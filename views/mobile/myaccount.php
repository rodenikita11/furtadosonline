</head>
<body>
 
   <div class="top-logo">
      <div class="container" style="padding:0;">
         <div class="row">
            <div class="col-md-12">
               <div class="col-xs-3">
                  <a href="javascript:void(0)" onclick="window.history.go(-1); return false;" >
                     <!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
                     <i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
                  </a>
               </div>
               <div class="col-xs-6 Checkout">My Profile</div>
               <div class="col-xs-3"></div>
            </div>
         </div>
      </div>
   </div>
   
   	<div class="Container container-fluid " id="detail">
   	 	<div id="billingdetail">
			<div class="col-sm-12" id="tabedit">
		      	<div class="panel panel-default" style="margin-top: 20px;">
		            <div class="panel-heading panleheade">
		               Personal Detail
		               <div id="edit" class="pull-right Edit" onclick="useredit()">Edit</div><!--"editprofile(<?php //echo $usrid;?>)"-->
		            </div>
					<table class="table " width="70%">
						<tbody>
							<tr style="border-bottom: 1px solid #f5f5f5;">
								<td class="Accountpr" width="20%"><b>Name</b></td>
								<td width="2%"> <b> :</b></td>
								<td width="48%" class="adtd" style="border-bottom: none"><?php echo ($custdtl[0]['title']) ? $custdtl[0]['title'] : ''; ?><?php echo ucfirst($custdtl[0]['firstname'].' '.$custdtl[0]['lastname'])?></td>
							</tr>
							<tr style="border-bottom: 1px solid #f5f5f5;">
								<td class="Accountpr"><b>Mobile No</b> </td>
								<td width="2%"> <b> :</b></td>
								<td class="adtd" style="border-bottom: none"><?php if(isset($custdtl[0]['telephone'])){ echo $custdtl[0]['telephone'];}?></td>
							</tr>
							<tr style="border-bottom: 1px solid #f5f5f5;">
								<td class="Accountpr"><b>Email Id</b> </td>
								<td width="2%"><b> :</b></td>
								<td class="adtd" style="border-bottom: none"><? if(isset($custdtl[0]['email'])){ echo $custdtl[0]['email'];}?></td>
							</tr>
							<tr style="border-bottom: 1px solid #f5f5f5;">
								<td class="Accountpr"><b>Gender</b></td>
								<td width="2%"><b> :</b></td>
								<td class="adtd" style="border-bottom: none"><? if($custdtl[0]['gender']== 'ma') { echo 'Male';} elseif ($custdtl[0]['gender']=='fe') { echo 'Female';}?></td>
							</tr>
							<tr style="border-bottom: 1px solid #f5f5f5;">
								<td class="Accountpr"><b>Date of Birth</b></td>
								<td width="2%"><b> :</b></td>
								<td class="adtd" style="border-bottom: none"><? if(isset($custdtl[0]['dob'])){ echo $custdtl[0]['dob'];}?></td>
							</tr>
						</tbody>
					</table>
		        </div>
	    	</div>
    	</div>
   	</div> 

    <div id="myprofile" style="display: none;">  
		<div class="Container container-fluid available_1 bg-1 text-center ">
			<div class="row">
				<div class="col-xs-12 Payment" >
					<b>Personal Detail</b>
				</div>
			</div>
		</div>
		 
		<form action="" method="post" name="userdtlform" id="userdtlform">
		<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <?// print_r($custdtl);die;?>
			         <div class="col-xs-4 "><b class="ft14">Email Id: *</b></div>
			         <div class="col-xs-8"><input type="text" name="emailid" id="emailid" value="<? if(isset($custdtl[0]['email'])){ echo $custdtl[0]['email'];}?>"></div>
			      </div>
			   </div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Title *</b></div>
			         <div class="col-xs-8 ">
			            <select name="title" id="title">
			               <option value="Select">---Select---</option>
			               <option value="Mr."<?php if(isset($custdtl[0]['title'])) { if($custdtl[0]['title']=="Mr."){ echo 'selected';}else {echo '';}} ?>>Mr.</option>
			               <option value="Mrs."<?php if(isset($custdtl[0]['title'])){ if($custdtl[0]['title']=="Mrs."){ echo 'selected';}else {echo '';}}?>>Mrs.</option>
			               <option value="Ms." <?php if(isset($custdtl[0]['title'])){if($custdtl[0]['title']=="Ms."){ echo 'selected';}else {echo '';}}?>>Ms.</option>
			            </select>
			         </div>
			      </div>
			   </div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">First Name: *</b></div>
			         <div class="col-xs-8"><input type="text" name="fname" id="fname" value="<? if(isset($custdtl[0]['firstname'])){ echo $custdtl[0]['firstname'];}?>"></div>
			      </div>
			   </div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Last Name: *</b></div>
			         <div class="col-xs-8"><input type="text" name="lname" id="lname" value="<? if(isset($custdtl[0]['lastname'])){ echo $custdtl[0]['lastname'];}?>"></div>
			      </div>
			   </div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Date of Birth: *</b></div>
			         <div class="col-xs-8"><input type="date" name="dob" id="dob"  max="<?php echo date('d/m/Y');?>" value="<? if(isset($custdtl[0]['dob'])){ echo $custdtl[0]['dob'];}?>"></div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Address: *</b></div>
			         <div class="col-xs-8"><textarea type="text" name="address" id="address" value="<? if(isset($custdtl[0]['useraddress'])){ echo $custdtl[0]['useraddress'];}?>"><? if(isset($custdtl[0]['useraddress'])){ echo $custdtl[0]['useraddress'];}?></textarea></div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Pin Code: *</b></div>
			         <div class="col-xs-8"><input type="text" name="pincode" id="pincode"  value="<?php if(isset($custdtl[0]['pincode'])){ echo $custdtl[0]['pincode'];}?>"></div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <? //print_r($states);die;?>
			         <div class="col-xs-4 "><b class="ft14">State: *</b></div>
			         <div class="col-xs-8">
			            <select name="state" id="state" onchange="citychange()">
			               <option value="Select">---Select---</option>
			               <?php for ($i=0 ; $i < count($states) ; $i++ ) { ?>
			               <option value="<?php echo $states[$i]['stateName'];?>" <?php if(isset($custdtl[0]['state'])){ 
			                  if($custdtl[0]['state']== $states[$i]['stateName'])
			                  {
			                    echo 'selected';
			                  }
			                  
			                  }?> data-id="<?php echo $states[$i]['id'];?>"><?php echo $states[$i]['stateName'];?></option>
			               <?php }?>
			            </select>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <input type="hidden" name="hiddencity" id="hiddencity" value="<?php echo $custdtl[0]['city'];?>">
			         <div class="col-xs-4 "><b class="ft14">City: *</b></div>
			         <div class="col-xs-8">
			            <select id="city" name="city">
			               <option value="Select">---Select---</option>
			            </select>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Country: *</b></div>
			         <div class="col-xs-8">
			            <select id="country" name="country">
			               <option value="India">India</option>
			            </select>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Telephone No.: *</b></div>
			         <div class="col-xs-8"><input type="text" name="telephone" id="telephone"  value="<?php if(isset($custdtl[0]['telephone'])){ echo $custdtl[0]['telephone'];}?>"></div>
			      </div>
			   </div>
			</div>
			<!--<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Fax No.:</b></div>
			         <div class="col-xs-8"><input type="text" name="fax" id="fax"  value="<?php //if(isset($custdtl[0]['fax'])){ echo $custdtl[0]['fax'];}?>"></div>
			      </div>
			   </div>
			</div>-->
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Products interested in:</b></div>
			         <?php  $interest=explode(",", $custdtl[0]['interest']);
			            // print_r($interest);die;
			            ?>
			         <div class="col-xs-8">
			         	<div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Guitars" value="Guitars" <?php if(isset($interest)){ if(in_array('Guitars',$interest)){ echo "checked"; }}?>> Guitars
			        </div>
			        <div class="col-xs-12 ft14" >
			            <input type="checkbox" name="interest[]" id="Piano"  value="Piano" <?php if(isset($interest)){ if(in_array('Piano',$interest)){ echo "checked"; }}?>> Piano
			        </div>
			        <div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Keyboards"  value="Keyboards" <?php if(isset($interest)){ if(in_array('Keyboards',$interest)){ echo "checked"; }}?> > Keyboards
			        </div>
			        <div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Drums"  value="Drums" <?php if(isset($interest)){ if(in_array('Drums',$interest)){ echo "checked"; }}?> > Drums
			        </div>
			        <div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Computer music"  value="Computer music" <?php if(isset($interest)){ if(in_array('Computer music',$interest)){ echo "checked"; }}?> > Computer music
			        </div>
			        <div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Bowed Strings"  value="Bowed Strings" <?php if(isset($interest)){ if(in_array('Bowed Strings',$interest)){ echo "checked"; }}?> > Bowed Strings
			        </div>
			        <div class="col-xs-12 ft14">
			            <input type="checkbox" name="interest[]" id="Wind Instruments"  value="Wind Instruments" <?php if(isset($interest)){ if(in_array('Wind Instruments',$interest)){ echo "checked"; }}?> > Wind Instruments
			        </div>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">Subscribe To:</b></div>
			         <div class="col-xs-8">
			            <input type="checkbox" name="Newsletter" id="Newsletter" value="0" <?php
			               if(isset($custdtl[0]['newsletterSubscribed']))
			                { 
			                  if($custdtl[0]['newsletterSubscribed']== 1){
			                    echo "checked";
			                }
			               }
			               ?> ><span class="ft14"> Newsletter</span>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
			   <div class="row">
			      <div class="col-xs-12" >
			         <div class="col-xs-4 "><b class="ft14">How did you hear about us?:</b></div>
			         <div class="col-xs-8">
			            <select name="hearAboutUs" id="hearAboutUs" >
			               <option value="">Select</option>
			               <option value="Online Search" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Online Search')echo "selected";}}?> >Online Search</option>
			               <option value="Facebook" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Facebook')echo "selected";}}?> >Facebook</option>
			               <option value="Magazine / Newspaper" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Magazine / Newspaper')echo "selected";}}?> >Magazine / Newspaper</option>
			               <option value="Stores" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Stores')echo "selected";}}?> >Stores</option>
			               <option value="Music Teacher" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Music Teacher')echo "selected";}}?> >Music Teacher</option>
			               <option value="Events" <?php if(isset($custdtl[0]['hearVia'])){{ if($custdtl[0]['hearVia']== 'Events')echo "selected";}}?> >Events</option>
			            </select>
			         </div>
			      </div>
			   </div>
			</div>
			<div class="emi_bottom Container container-fluid available_Save_Address bg-1 text-center ">
			   <div class="row">
			   	<div class="col-xs-1"></div>
			      <div class="col-xs-10 " >
				       <div class="col-xs-6">
				       	  <input type="submit" name="submit" id="savedetailadd" value="Save Details" class="saveaddr" style="padding: 8px 0px;width: 100%;font-size: 12px;text-transform: uppercase;color: white;" >
				       	   <img id='loader' src="<?php echo base_url(); ?>assets/images/preloader.gif" style='display: none;position: absolute;right: 107px;width: 14px;bottom: 7px;'>
				       </div> 
				       <div class="col-xs-6">
				       	<button name="cancle" id="cancleaddr" class="saveaddr" style="padding: 8px 0px;margin: 0px 4px;width: 100%;color: white;text-transform: uppercase;font-size: 12px;"> Cancel Details</button>
				       </div>
				      </div>
				      <div class="col-xs-1"></div>
			    </div>
			</div>
		<div class="clear"></div>
		<!--Save Address-->
		<div class="clear"></div>
		</form>
    </div>

   <!--Billing Address-->
    <div id="billingaddr" >
        <div class="Container container-fluid " id="billingdetail">
	      <div class="col-sm-12" id="tabedit">
	         <div class="panel panel-default">
	            <div class="panel-heading panleheade">
	               Billing Address
	               <div id="edit" class="pull-right Edit" onclick="editbillingaddr()" >Edit</div><!--"editprofile(<?php //echo $usrid;?>)"-->
	            </div>
	            <?php foreach($custdtl as $customer){?>
	            <div class="panel-body" style="padding: 15px;">
	               <h5><?php echo ucfirst($customer['firstname'].' '.$customer['lastname']);?></h5>
	               <p style="font-size:12px;"><?php echo $customer['useraddress'];?><br>
	                  <?php echo $customer['city'].",".$customer['state']; ?><br>
	                  <?php echo $customer['pincode']?>
	               </p>
	            </div>
	            <?}?>
	         </div>
	      </div>
	    </div>
  	</div>

  	<!--billing adress edit from-->
  	<div id="billingadd" style="display: none;">
		<div class="Container container-fluid available_1 bg-1 text-center ">
			<div class="row">
				<div class="col-xs-12 Payment" >
					<b>Edit Billing Address</b>
				</div>
			</div>
		</div>	
		<form action="" method="post" name="userdtlform1" id="userdtlform1">
		<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">Name</div>
						<div class="col-xs-8"><input type="text" name="custname" id="custname" placeholder="Abc Xyz"
						 value="<?php echo $custdtl[0]['firstname'].' '.$custdtl[0]['lastname']?>" readonly></div>
					</div>
				</div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">Mobile No</div>
						<div class="col-xs-8 "><input type="text" name="mobnumber" id="mobnumber"  value="<?php echo $custdtl[0]['telephone'];?>" readonly></div>
					</div>
				</div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">Address</div>
						<div class="col-xs-8"><textarea name="addr" id="addr"><?php echo $custdtl[0]['useraddress'];?></textarea></div>
					</div>
				</div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">Landmark</div>
						<div class="col-xs-8"><input type="text" name="landmark" id="landmark" value="<?php echo $custdtl[0]['landmark']?>"></div>
					</div>
				</div>
			</div>
			<!---Delivery -->
			<div class="Container container-fluid  bg-1 text-center emi_bottom ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">Pin Code</div>
						<div class="col-xs-8"><input type="text" name="pincode1" id="pincode1"  value="<?php echo $custdtl[0]['pincode']?>"></div>
					</div>
				</div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4 ">State</div>
						<div class="col-xs-8">
							<select name="state1" id="state1" onchange="citychange1();">
								<option value="Select">---Select---</option>
								<?php for ($i=0 ; $i < count($states) ; $i++ ) { ?>
								<option value="<?php echo $states[$i]['stateName'];?>" <?php if(isset($custdtl[0]['state'])){if($custdtl[0]['state']== $states[$i]['stateName']){ echo 'selected';}}?> data-id="<?php echo $states[$i]['id'];?>"><?php echo $states[$i]['stateName'];?></option>
								<?php }?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="Container container-fluid  bg-1 text-center emi_1 ">
				<div class="row">
					<div class="col-xs-12" >
						<input type="hidden" name="hiddencity1" id="hiddencity1" value="<?php echo $custdtl[0]['city'];?>">
						<div class="col-xs-4 ">City: *</div>
						<div class="col-xs-8">
							<select id="city1" name="city1">
							<option value="Select">---Select---</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<!--Save Address-->
			<div class="Container container-fluid available_Save_Address bg-1 text-center ">
				<div class="row">
					<div class="col-xs-12 " >
					  <div class="col-xs-6">
					  	 <input type="submit" name="submit"  value="Save Address" class="saveaddr" id="savebillingadd" style="padding: 8px 12px;"><img id='loader' src="<?php echo base_url(); ?>assets/images/preloader.gif" style='display: none;position: absolute;right: 107px;width: 14px;bottom: 7px;'>
					  </div>
					  <div class="col-xs-6">
					  	<button name="cancle" id="cancleaddr" class="savebillingadd" style="right: 177px;padding: 5px;background: #cd0029;    border: 1px solid #cd0029;">Cancel Address</button>
					  </div>	
					</div>
				</div>
			</div>
			
		</form>
	</div> 

	<?php foreach($shippingdtl as $shipping){ ?>
	    <div id="shippingadd">
			<div class="Container container-fluid" id="customeraddress_<?php echo $shipping['address_id']; ?>">
				<div class="col-sm-12" id="tabedit">
			       <div class="panel panel-default">
			          <div class="panel-heading panleheade">Shipping Address 
			          	<span style="float:right; margin-left:10px;">
			          		<a href="javascript: void(0)" onClick="deleteaddress(this, {addressid:<?php echo $shipping['address_id']; ?>, customerid: <?php echo $shipping['customer_id']; ?>})" data-id="address_<?php echo $shipping['address_id']; ?>" style="color:#000000 ! important">Delete</a>
			          	</span>
			          	<span style="float:right">
			          		<a href="javascript: void(0)" onClick="editaddress(this, {addressid:<?php echo $shipping['address_id']; ?>, customerid: <?php echo $shipping['customer_id']; ?>})" data-id="address_<?php echo $shipping['address_id']; ?>" style="color:#000000 ! important">Edit</a>
			          	</span>
			          </div>
			          <div class="panel-body" style="padding: 15px;" id="address_<?php echo $shipping['address_id']; ?>">
			             <h5><?echo $shipping['firstname'].' '.$shipping['lastname']; ?></h5>
			             <p><?echo $shipping['address_1']; ?> <br>
			                Land Mark - <?echo $shipping['landmark']; ?><br>
			                <?echo $shipping['postcode']; ?><br>
			             </p>
			          </div>
			       </div>
			   </div>
			</div>
		</div>
	<?}?>  
	<div>
        <div id="changepassword" class="Container container-fluid">
	      <div class="col-sm-12" id="tabedit">
	         <div class="panel panel-default">
	            <div class="panel-heading panleheade">
	               Change Password
	               <div id="edit" class="pull-right Edit" onclick="editchangepass()" >Change</div>
	            </div>
	         </div>
	      </div>
	    </div>
  	</div>
  	<div id="changepasswordfrm" style="display: none;">
  		<div class="Container container-fluid available_1 bg-1 text-center ">
			<div class="row">
				<div class="col-xs-12 Payment" >
					<b>Change Password</b>
				</div>
			</div>
		</div>	
       <form method='post' id='passwordfrmdtl' name='passwordfrmdtl' action="">
       	<div class="Container container-fluid  bg-1 text-center emi_1 ">	
         <table>
           <tbody>
              <tr>
                <td width="25%" class="Accountpr">Old Password</td>
                <td width="2%">:</td>
                <td width="65%" class="adtd"><input type="password" class="form-control" value=""  name="oldpassword" id="oldpassword"  style="border: 1px solid #ccc ! important;"></td>
              </tr>
              <tr>
                <td width="25%" class="Accountpr">New Password</td>
                <td width="2%">:</td>
                <td width="65%" class="adtd"><input type="password" class="form-control" value="" name="newpassword" id="newpassword" style="border: 1px solid #ccc ! important;"></td>
              </tr>
              <tr>
                <td width="25%" class="Accountpr">Confirm Password</td>
                <td width="2%">:</td>
                <td width="65%" class="adtd"><input type="password" class="form-control" value="" name="cpassword" id="cpassword" style="border: 1px solid #ccc ! important;"></td>
              </tr>
           </tbody>
         </table>
    	</div>
        
         <div class="Container container-fluid available_Save_Address bg-1 text-center ">
				<div class="row">
					<div class="col-xs-12 " >
					  <div class="col-xs-6">
					  	 <input type="submit" name="submit"  value="Save" class="saveaddr" id="submitpasswordusr" style="padding: 5px 12px; font-size: 17px; float: right;">
					  </div>
					  <div class="col-xs-6">
					  	<button name="cancle" id="cancleaddr" class="savebillingadd" style="right: 177px;padding: 5px;background: #cd0029;    border: 1px solid #cd0029;">Cancel</button>
					  </div>	
					</div>
				</div>
			</div> 
       </form>
    </div>
	
</body>
</html>
<style type="text/css">
   input#emailid,input#fname,input#lname,input#dob,textarea#address,input#pincode,input#telephone,input#fax,input#custname,input#mobnumber,textarea#addr,input#landmark,input#pincode1 {
    padding: 5px;
    width: 100%;
    font-size: 12px;
}
select#title,select#state,select#city,select#country,select#hearAboutUs,select#state1,select#city1 {
    width: 100%;
    padding: 5px;
    font-size: 12px;
    margin: 0px ! important;
}
   .error{
   color: red;
   font-size: 13px;
   }
</style>
<script type="text/javascript">
 jQuery(document).ready(function(){
  //Change Password info ========================
  jQuery('#submitpasswordusr').click(function(){
  	$.validator.addMethod(
            "regex",
            function(value, element, regexp) 
            {
                if (regexp.constructor != RegExp)
                    regexp = new RegExp(regexp);
                else if (regexp.global)
                    regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
            },
            "Please check your input."
    );
    jQuery('#passwordfrmdtl').validate({
      rules:{
          oldpassword:{
            required:true
          },
          newpassword:{
            required:true,
            regex: /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
          },
          cpassword:{
            equalTo:"#newpassword"
          }
        },
        messages:{
          oldpassword:{
            required:"Please enter your old password"
          },
          newpassword:{
            required:"Please enter your new password",
            regex: "Passwords must contain at least 8 characters, including uppercase, lowercase, special characters and numbers.",
          },
          cpassword:{
            equalTo:"Confirm password must match with new password"
          }
        },
		submitHandler:function(){
			 //var success=jQuery("#passwordfrmdtl").valid();
			 var newpassword=jQuery('#newpassword').val();
			 var oldpassword=jQuery('#oldpassword').val();

			 //if(success){
				jQuery('#loader').show();
				jQuery.ajax({
					type:'post',
					url:'<?php echo SITEURL;?>user/changepassword',
					dataType:'JSON',
					data:{newpassword:newpassword,oldpassword:oldpassword},
					success:function(result){
						jQuery('#loader').hide();
						res=eval(result);
						if(res.status == true)
						{
							alert(res.msg);  
							history.go(0); 
							window.location.href='<?php echo SITEURL; ?>user/';
							// jQuery('#login').modal('show');
						}else{
							alert(res.msg);
						}
					}
				});
			//}
		}
	});
    })
  });
 </script>
<script type="text/javascript">
	function editchangepass(){
		jQuery('#detail').hide();
		jQuery('#myprofile').hide();
		jQuery('#billingaddr').hide(); 
		jQuery('#shippingadd').hide();   
		jQuery('#changepassword').hide(); 
		jQuery('#changepasswordfrm').show();   	
    }

	function editaddress(ele, detailobj){
	    addressid=detailobj.addressid;
	    var arr=[];
	    var name, html;
	    var addr=jQuery(ele).data('id');
	    addrhtml=jQuery('#'+addr).html();
	    arr=addrhtml.split("</h5>");
	    rawname=jQuery.trim(arr[0]);
	    name=rawname.replace('<h5>', '');

	    addresses=arr[1].split('<br>');
	    rawaddress=jQuery.trim(addresses[0]);
	    adrress1=rawaddress.replace('<p>', '');

	    rawlandmark=addresses[1].split('-');
	    landmark=rawlandmark[1];

	    postcode=jQuery.trim(addresses[2]);

	    html='<form method="post" id="addressform_'+addressid+'"><label>Name: </label><input type="text" placeholder="Name" id="name_'+addressid+'" value="'+name+'" class="form-control" style="border: 1px solid #cccc;"><label style="margin-top: 14px;">Address: </label><input type="text"  id="address_1_'+addressid+'" class="form-control" placeholder="Address" value="'+adrress1+'" style="border: 1px solid #cccc;"><label style="margin-top: 14px;">Landmark: </label><input type="text" placeholder="Landmark" id="landmark_'+addressid+'" class="form-control" value="'+landmark+'" style="border: 1px solid #cccc;"><label style="margin-top: 14px;">Postcode: </label><input type="text" placeholder="Postcode" id="postcode_'+addressid+'" class="form-control" value="'+postcode+'"style="border: 1px solid #cccc;"><button type="button" class="form-control" onClick="updateaddress('+addressid+')"  style="background: #cc0028;margin-top: 10px;color: #fff;">Submit</button></form>';
	    jQuery('#'+addr).html(html);
	}

	function updateaddress(addressid){
	  name=jQuery('#name_'+addressid).val();
	  landmark=jQuery('#landmark_'+addressid).val();
	  postcode=jQuery('#postcode_'+addressid).val();
	  address_1=jQuery('#address_1_'+addressid).val();
	  jQuery.ajax({
	      type:'POST',
	      data:{name,address_1,landmark,postcode,addressid},
	      url:'<?php echo SITEURL; ?>user/updateaddress',
	      dataType:'json'
	  }).done(function(data){
	      if(data.error==false){
	          //window.location.href='<?php echo SITEURL; ?>user/myaccount#address_'+addressid;
	         updatedaddr='<h4>'+name+'</h4><p>'+address_1+'<br>Land Mark - '+landmark+'<br>'+postcode+'<br></p>';
	         jQuery('#address_'+addressid).html(updatedaddr);
	      }else{
	          bootbox.alert(data.message);
	      }
	  })
	}

	function deleteaddress(ele, detailsobject){
	    addressid=detailsobject.addressid;
	    customerid=detailsobject.customerid;

	    jQuery.ajax({
	        type:'POST',
	        data:{addressid, customerid},
	        url:'<?php echo SITEURL; ?>user/deleteaddress',
	        dataType:'json'
	    }).done(function(data){
	        if(data.error==false){
	            jQuery('#customeraddress_'+addressid).fadeOut('slow');
	        }else{
	            bootbox.alert(data.message);
	        }
	    })
	}

   function useredit(){
      //alert('hii');
		jQuery('#shippingadd').hide();   
		jQuery('#changepassword').hide(); 
		
		jQuery('#detail').hide();
		jQuery('#myprofile').show();
		jQuery('#billing').hide();
		jQuery("#billingaddr").css("display", "none");
		
    }
    jQuery('#cancleaddr').click(function(){
		window.location.reload()
    })
    function editbillingaddr(){
    	//window.location.href='<?php echo SITEMOBURL;?>product/addbillingaddr'
		jQuery('#shippingadd').hide();   
		jQuery('#changepassword').hide(); 
		
		jQuery('#detail').hide();
		jQuery('#myprofile').hide();
		jQuery('#billingaddr').hide(); 
		jQuery('#billingadd').show();   	
    }
    
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
  	//=======save billing address==========
  	jQuery('#savebillingadd').click(function(){
    	//alert('hiii1'); return false;
		jQuery.validator.addMethod('selectcheck', function (value) {
			return (value != 'select');
		}, "Please Select the State");

		jQuery('#userdtlform1').validate({
			rules:{
				state1:{
					selectcheck:true
				},
				city1:{
					selectcheck:true
				},
				custname:{
					required:true
				},
				addr:{
					required:true
				},
				landmark:{
					required:true
				},
				mobnumber:{
					required:true,
					number:true,
					minlength:10,
					maxlength:10
				},
				pincode:{
					required:true,
					number:true,
					minlength:6,
					maxlength:6
				}
			},
			messages:{
				custname:{
					required:"Please enter the Name" 
				},
				addr:{
					required:"Please enter the Address"
				} ,
				landmark:{
					required:"Please enter the Landmark"
				},
				mobnumber:{
					required:"This field is required",
					number:"This field require only numbers",
					minlength:"Please enter valid contact number",
					maxlength:"Please enter valid contact number"
				},
				pincode:{
					required:"This field is required",
					number:"This field require only numbers",
					minlength:"This field require 6 digit",
					maxlength:"This field require 6 digit"
				}
			},
			submitHandler:function(){

				jQuery('#loader').show();
        		jQuery('#savebillingadd').attr('disabled',true);
        		//console.log('vikram');return false; 
        		var model='useraccdtl';
				var address=jQuery('#address').val();
				var landmark=jQuery('#landmark').val();
				var state2=jQuery('#state1 option:selected').val();
				//var state2=jQuery('#state1').val();
				var city2=jQuery('#city1 option:selected').val();
				var pincode2=jQuery('#pincode1').val();
				jQuery.ajax({
					type:'post',
					url:'<?php echo SITEMOBURL?>user/updateuseraccdtl',
					dataType:'JSON',
					data:{address:address,landmark:landmark,state2:state2,city2:city2,model:model,pincode2:pincode2},
					success:function(result){
						jQuery('#loader').hide();
            			jQuery('#savebillingadd').attr('disabled',false);
						res=eval(result);
						if(res.status == true)
						{
							alert('Address added successfully');  
							history.go(0); 
						}else{
							alert('Error while adding address');
						}
					}
				});
			}

		});
    })
  	//=====================================
  	//save personal info ========================
  	jQuery('#savedetailadd').click(function(){
  	//	alert('hiii'); return false;
    jQuery.validator.addMethod('selectcheck1', function (value) {
      return (value != 'Select');
    }, "Please select one option");

    jQuery.validator.addMethod("emailvalid", function(value) {
      var r = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      return value.match(r);
    }, "This field contain valid email id ");


    jQuery('#userdtlform').validate({
      rules:{
        title:{
        selectcheck1:true
        },
        emailid:{
        emailvalid:true
        },
        state:{
        selectcheck1:true 
        },
        city:{
        selectcheck1:true 
        },
        country:{
        selectcheck1:true
        },
        fname:{
        required:true
        },
        lname:{
        required:true
        },
        telephone:{
        required:true,
        number:true,
        minlength:10,
        maxlength:10
        },
        address:{
        required:true
        },
        pincode:{
        required:true,
        number:true,
        minlength:6,
        maxlength:6
        },
        dob:{
        required:true,
        date:true
        },     
      },
      messages:{
        fname:{
        required:"Please enter the First Name" 
        },
        lname:{
        required:"Please enter the Last Name"
        },

        telephone:{
        required:"This field is required",
        number:"This field require only numbers",
        minlength:"Please enter valid contact number",
        maxlength:"Please enter valid contact number"
        },
        address:{
        required:"This field is required"
        },
        pincode:{
        required:"This field is required",
        number:"This field require only numbers",
        minlength:"This field require 6 digit",
        maxlength:"This field require 6 digit"
        },
        dob:{
        required:"This field is required",
        date:"please select date of birth"
        },
      },
      submitHandler:function(){
        jQuery('#loader').show();
        jQuery('.saveaddr').attr('disabled',true);
        jQuery.ajax({
          type:'post',
          url:'<?php echo SITEMOBURL?>user/updateuseraccdtl',
          dataType:'JSON',
          data:{formdata:jQuery('#userdtlform').serialize()},
          success:function(result){
            jQuery('#loader').hide();
            jQuery('.saveaddr').attr('disabled',false);
            res=eval(result);
            if(res.status == true)
            {
              alert(res.msg);
              window.location='<?php echo SITEMOBURL?>';
            }else{
              alert(res.msg);
            }
          }
        });
      }

    });
    })
  });
   
</script>
<script type="text/javascript">
   jQuery('#Newsletter').click(function(){
       if(jQuery('#Newsletter').is(':checked'))
       {
        jQuery('#Newsletter').val('1');
       }else{
         jQuery('#Newsletter').val('0'); 
       }
   
   });
</script>
<script type="text/javascript">
   function citychange(){
   
     var sid=jQuery('#state option:selected').data('id');
     
     jQuery.ajax({
       type:'POST',
       dataType:'JSON',
       url:'<?php echo SITEMOBURL?>user/getcities',
       data:{sid:sid},
       success:function(result)
       {
         res=eval(result);
         if(res.status== true)
         {
           var str='';
           var hidcity=jQuery('#hiddencity').val();
           
           for (var i = 0; i < (res.msg.length); i++) {
             
             var resultcity = res.msg[i]['cityName'].replace(/ /g, "_");
             if(hidcity != " " && hidcity== resultcity){
                 var sele='selected'; 
             }else{
                 var sele='';
             }
                     
             str+='<option value='+resultcity+' '+ sele+'>'+res.msg[i]['cityName']+'</option>';
           };
         jQuery('#city').html(str);
         }else{
           alert(res.msg);
         }
       }
     });
     return false;
   }
   function citychange1(){
   
     var sid=jQuery('#state1 option:selected').data('id');
     
     jQuery.ajax({
       type:'POST',
       dataType:'JSON',
       url:'<?php echo SITEMOBURL?>user/getcities',
       data:{sid:sid},
       success:function(result)
       {
         res=eval(result);
         if(res.status== true)
         {
           var str='';
           var hidcity=jQuery('#hiddencity1').val();
           
           for (var i = 0; i < (res.msg.length); i++) {
             
             var resultcity = res.msg[i]['cityName'].replace(/ /g, "_");
             if(hidcity != " " && hidcity== resultcity){
                 var sele='selected'; 
             }else{
                 var sele='';
             }
                     
             str+='<option value='+resultcity+' '+ sele+'>'+res.msg[i]['cityName']+'</option>';
           };
         jQuery('#city1').html(str);
         }else{
           alert(res.msg);
         }
       }
     });
     return false;
   }
</script>
<script type="text/javascript">
   jQuery(document).ready(function(){
     citychange();
     citychange1();
  //   function useredit(){
		// jQuery('#detail').hide();
  //     	jQuery('#myprofile').show();
  //   }
   });
   
   
</script>
<script type="text/javascript">
   $(function(){
       var dtToday = new Date();
       
       var month = dtToday.getMonth() + 1;
       var day = dtToday.getDate();
       var year = dtToday.getFullYear();
       if(month < 10)
           month = '0' + month.toString();
       if(day < 10)
           day = '0' + day.toString();
       
       var maxDate = year + '-' + month + '-' + day;
       
       $('#dob').attr('max', maxDate);
   });
</script>