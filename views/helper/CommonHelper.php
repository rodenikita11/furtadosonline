<?php
		/*
				AUTHOR : SANJAY YADAV
				CREATED : 19-Aug
				DESCRIPTION : Common function call on view
		*/
		class CommonHelper
		{
			static function get_payment_method($key=null)
			{
				$payment_type = array('COD'=>'Cash On Delivery','NBK'=>'Online Payment','PBC'=>'Online Payment','EMI'=>'Easy month instalment','RDM'=>'Point Redeemed','COUPON'=>'Coupon Points');
					if(!empty($key))
					{
						return $payment_type[$key];
					}
				return $payment_type;
			}
			
			/*
					NAME : return_part_of_string
					DESC : Return part of string with ...
			*/
			static function return_part_of_string($str,$count)
			{
						if(strlen($str) >($count+3))
							$return_str = substr($str, 0, $count) . '...';
						else
							$return_str = $str;
					return $return_str;
			}
			
			/*
					NAME : get_date_diff
					CREATED : 25/Sep
			*/
			static function get_date_diff($date_from,$date_to)
			{
				$date_from =date_create(date('Y-m-d',strtotime($date_from)));
				$date_to = date_create(date('Y-m-d',strtotime($date_to)));
				$obj = date_diff($date_from,$date_to);
				$dateDiff = $obj->format("%a");
				return $dateDiff;
			}
			
			/*
					NAME : is_date_in_between
					CREATED : 20/Dec
					DESC : Check whether current date is in between.
			*/
			static function valid_offer_date($date_from,$date_end)
			{
				$currentDate = date('Y-m-d');
					$currentDate=date('Y-m-d', strtotime($currentDate));; 
						if(!empty($date_end) && !empty($date_from))
						{
					$offerDateBegin = date('Y-m-d', strtotime($date_from));
					$offerDateEnd = date('Y-m-d', strtotime($date_end));
								if (($currentDate > $offerDateBegin) && ($currentDate < $offerDateEnd))
									{
										return true;
									}
									else
									{
										return false;
									}
						
						}
						else
						{
							return false;
						}
			
			}
		}
?>