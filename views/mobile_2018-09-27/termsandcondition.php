<div class="top-logo">
<div class="container" style="padding:0;">
<div class="row">
<div class="col-md-12">
<style>h4.tac{margin: 10px 1px 0px 0px;}</style>

<div class="col-xs-2">
<a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>

</div>
<div class="col-xs-7 Favourite_list">Terms and Condition</div>

<div class="col-xs-3"style="padding:0;">
  
 </div>
	</div>
</div>
</div>
</div>
<div class="Container container-fluid about-us">
<div class="row">
<div class="col-xs-12">
<h4 class="tac">Your Account</h4>
		<p>If you use this site you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and, you agree to accept responsibility for all activities that occur under your account or password. Furtadosonline.com and its affiliates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders at their sole discretion.
		</p>
		<h4 class="tac">Acceptance</h4>
		<p>By purchasing products and/or services from this site, you agree to be bound by and accept this agreement. Orders are not binding upon Furtadosonline until accepted by Furtados. We reserve the right to refuse service to anyone. Furtadosonline will indicate its acceptance of an order by acknowledging your order by reply email or by shipping the ordered items to you. Furtadosonline reserves the right to cancel or part ship orders as per its convenience and necessity.
		</p>
		<h4 class="tac">Pricing</h4>
		<p >Product descriptions, pricing, and availability can change quickly, Furtadosonline does not warrant the accuracy or completeness of the information provided on the site. If there are any changes in pricing, availability or transportation charges the same will be intimated to the user and requisite approval obtained, by e-mail or any other accepted means of communication, before the product is dispatched.
		</p>
		<h4class="tac">Limited Quantities</h4>
		<p>Some products available on promotion or as special deal may be limited in stock and are offered on a first come, first serve basis. Furtadosonline reserves the right to amend or discontinue any promotion that may be applicable without requirement to provide a reason for the same.
		</p>
		<h4 class="tac">Shipping Charges-Taxes and Title</h4>
		<p>Separate charges for shipping and handling will be shown on your order form. Orders placed on Saturdays, Sundays, or holidays will be processed on the next business day. As a security precaution, initial orders and orders shipping to alternate addresses may be held for extended verification. We reserve the right to make partial shipments, which will not relieve you of your obligation to pay for the remaining deliveries. All items purchased from Furtadosonline are made pursuant to a shipment contract. This means that the risk of loss and title for such items passes to you upon our delivery to the carrier. For more information on our shipping policies, including rates, delivery times, and delays, please refer to our Shipping Policy or call us at (022) 29206780 / 61415050.
		</p>
		<h4 class="tac">Billing & Payments</h4>
		<p>All items supplied will be accompanied by an official invoice. If payment for your order has been made in advance the order will be dispatched after receipt of the same. In case of credit card payment your credit card will be charged only at the time of dispatch of the order. To proceed with your order minimum purchase has to be more then Rs.100/-.
		</p>
		<h4 class="tac">Delivery Mode</h4>
		<p>All orders are processed and shipped within 10 to 12 working days, once payment is confirmed and the product is in stock. However, certain categories of products have different lead-times for delivery due to the nature of the product. If a product is delayed for an extended period of time the same will be informed to the user. Kindly note, due to certain regulatory requirements or carrier unavailability it may not be possible to supply products to certain areas and locations. You can check if your location is in a serviceable area by entering your pincode in the box designated for the same. If for any reason we are unable to deliver to your location we will advise you after you have placed your order.
		</p>
		<h4 class="tac">Road Permits</h4>
		<p>Requisite Sales Tax Forms / Road Permits / Entry Forms may be required for delivery to certain areas. Our dispatch team will be in touch with you if this is required for your area. Please note that responsibility for providing these forms lies with the user. We may be unable to dispatch the order if the requisite forms are not received by us in original. Inability to comply with requirement of the requisite form(s) in original will lead to cancellation of order and refund/return of advance payment, if received.
		</p>
		<h4 class="tac">Modes of Dispatch</h4>
		</p>All shipments are delivered to your doorstep. If for any reason we are unable to deliver to your doorstep you will be informed by us or the logistic/courier/postal company handlingthe delivery. Please note that while we may endeavour to have your shipment delivered at your door-step certain mitigating circumstances and occurrences may necessitate that the same may be required to be collected by you from a designated location. The following modes of dispatch are available:
		<br />
		Express Shipping - This mode is generally delivered by air and is the speediest mode of shipment. Some bulky and oversized items may not be deliverable by this mode.<br />
		Standard Shipping - This mode is generally delivered by road/rail and takes longer than Express shipping.<br />
		Custom Delivery Option - This mode is applicable for oversized and large items like Acoustic Pianos and Organs. A quote will be provided for the item keeping in mind the location of dispatch, etc. Only on acceptance of this quote and completion of the shopping process will the product be delivered.

		<h4 class="tac">Delivery Time</h4>
		<p>Transit times vary depending on the location and mode of dispatch opted for. To form an estimate of the amount of time you can expect to receive your items please add:<br /> Time to dispatch (varies from product to product and displayed on the product page) + Transit time. The Transit time is displayed (depending on your mode of shipment) in your shopping cart, Please note that transit time is based on best effort estimates and may be exceeded due to operational or logistic reasons which may or may not be within our control.
		</p>
		<h4 class="tac">Cancellation / Refund Policy</h4>
		<p>We at Furtados make every effort to fulfill all the orders placed with the help of all our group stores under the corporate entity - Furtados Music India Pvt Ltd. However, please note that there may be certain orders that we are unable to process and must cancel. Some situations that may result in your order being cancelled include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, or problems identified by our credit and fraud avoidance department. Furtados will communicate to you if all or any portion of your order is cancelled. In the unlikely event that your order is cancelled after your credit card has been charged, the said amount will be reversed in your Card Account.
		</p>
		<h4 class="tac">Refunds</h4>
		<p>In the event that a refund is necessitated the amount will be refunded in the same mode as the payment was made. If the payment was by Credit Card or Net-Banking, we will refund the Credit Card or Net-Banking account. Typically refunds are processed within 15 working days. Cheques will be sent only to the billing address of the customer.
		</p>
		<h4 class="tac">Returns Policy</h4>
		<p>We always endeavor to supply product in pristine condition. However, if, in the unlikely event that the product is received in a defective condition we request you to send back the product to us for replacement. Kindly send back the product only after receiving a return authorization from us. Items sent without a return authorization will not be accepted by us. We are unable to accept returns from customers/users under any other circumstances. Kindly verify that the item ordered by you is what you require when placing your order.
		</p>
		<h4 class="tac">Limited Warranty & Service</h4>
		<p>All the new products are sold with the manufacturer's limited warranty. The warranty period and service varies by manufacturer and product. Please see our Furtados Assurance Program for our service policy. We value your opinions. If you have comments or questions about our Terms & Privacy policy, please send a mail to
		<br />
		terms@furtadosonline.com or write to us at the following address.
		<br />
		Furtados Music (India) Pvt Ltd<br />
		201, Town Centre II,<br />
		Andheri � Kurla Road, Marol, Andheri (E), Mumbai-400059<br />
		Telephone: (022) 42875050 / 42875060 <br />
		</p>


</div>
</div>
</div>

<?php include('include/footer.php');?>