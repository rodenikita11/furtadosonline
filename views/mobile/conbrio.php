<style>
   p {
   font-size: 14px;
   text-align: justify;
   }
   .ptop {margin-top: 30px;}
</style>
<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px;">
          <div class="col-lg-12 outer-top-150" style="text-align:center">
              <img src="<?php echo WEBIMG; ?>banners/CB19 Competition Details_logo.jpg" width="50%" style="margin-top: 22px">
          </div>
         <div class="col-lg-12" style="margin-top: 20px;">
           <h1 style="color: #0088cc;text-transform: capitalize; font-size: 30px;text-align: center;">2019: More Schumania</h1>
            <p class="ptop">A happy coincidence for 2019 is that not only will it be the tenth edition of Con Brio, but it will also
              be the two-hundredth birth centenary of Clara Schumann, the wife of Robert Schumann who was
              the central figure of our inaugural Con Brio back in 2010, titled Schumania. So, we are pleased to
              have another round of Schumania, with the focus this time being on Clara and not Robert. Clara is
              undoubtedly one of the most famous women composers in history and we will celebrate her twohundredth birth centenary by programming the festival to include exclusively the works of women composers, many of them unduly underperformed. The competition will do the same.</p>
            
            <h3 style="color: #0088cc;">eligibility</h3>
            <p>The competition is open to all Indian nationals (including PIO and OCI card holders) who have
              resided in India for at least 7 months between January 1, 2018 and December 31, 2018. All entrants
              must be born on or after January 1, 1984. You will need to produce a valid passport for verification
              purposes. Previous first place winners of Con Brio and honorary Con Brio prize recipients may not
              enter the competition.</p>

            <h3 style="color: #0088cc;">prizes</h3>
            <p>First Prize <span style="margin-left:40px">Rs. 3,00,000 cash</span></p>
            <p>Second Prize <span style="margin-left:20px">Rs. 1,00,000 cash</span></p>
            <p>Third Prize <span style="margin-left:32px">Rs. 50,000 cash</span></p>
            <p>Semifinalists <span style="margin-left:20px">Rs. 25,000 cash each</span></p>

            <p class="ptop">In accordance with the fiscal policy, tax will be deducted at source and prize-winners will be issued a
              TDS certificate.</p>

            <p class="ptop">The adjudicators reserve the right to withhold the first and/or second prize if the level of the
              performance of the competitors does not meet the expected standard. This marks a shift in the
              ideology of Con Brio in that it is no longer adequate to be the best amongst the group of entries,
              but it is also essential to be a suitably high level musician and match the standard of our past
              winners. This is also reflected in our rolling selection process explained below.</p>

            <h3 style="color: #0088cc;">repertoire</h3>
            <p><span style="font-weight: bold; margin-right:50px">pre-screening</span>Clara Schumann – Impromptu in E major & one other work of your choice</p>
            <p><span style="font-weight: bold; margin-right:74px">semifinals</span>A programme lasting 5 to 8 minutes comprising works of your choice</p>
            <p><span style="font-weight: bold; margin-right:108px">finals</span>A programme lasting 7 to 10 minutes comprising works of your choice</p>

            <p class="ptop" style="font-weight: bold;">notes on the repertoire</p>
            <p>• All works of your choice must be by female composers <u>excluding</u> Clara Schumann</p>
            <p>• You may include individual movements/pieces from multi-movement works or collections</p>
            <p>• You may not repeat works from your semifinals programme in your finals programme but you may repeat the work of your choice from the pre-screening in the semifinals or finals.</p>
            <p>• In your semifinals and finals programme combined, you must cover the works of at least three different female composers</p>
            <p>• You may not use a page turner but are expected to memorize or use photocopies of short sections to ensure that the music is not interrupted by page turns.</p>

            <h3 style="color: #0088cc;">how to enter</h3>
            <p>To enter the competition, start by reading the video recording guidelines in this document. After
              you have prepared the pre-screening pieces to the best of your ability, record your performance of
              the pieces. Upload the clips (two separate clips, one for each piece) to a service such as YouTube,
              Vimeo, etc. You may keep the videos private, but ensure that they can be viewed in all countries
              simply with a link, without needing to login. From your primary email address, send an email to
              <a href="mailto:conbrio@furtados.in" style="color:#0088cc !important">conbrio@furtados.in</a> with a CC to <a href="mailto:parveshjava@me.com" style="color:#0088cc !important">parveshjava@me.com</a> with the subject “Con Brio 2019 Entry” and
              include the following five items in the body:</p>

              <p>1. Full name, as you would like it to appear in the programme and on certificates</p>
              <p>2. Date of birth</p>
              <p>3. Home city in India, i.e. the city where you consider yourself to be most from</p>
              <p>4. A scan of your passport data pages containing your photograph, name and date of birth</p>
              <p>5. Two links to the performances of the two pieces</p>

            <h3 style="color: #0088cc">rolling selection</h3>
            <p>This year we will <b>“provisionally select”</b> semi-finalists on the basis of the pre-screening repertoire.
              After you submit your entry, you will be informed whether you have been provisionally selected
              within 2 weeks. The last date for entering the competition will be <b>March 15, 2019</b>. However, late
              entries might be entertained if an adequate number of semifinalists have not been provisionally
              selected. If you wish to enter after March 15, 2019, please email us to check if we are still receiving
              new entries.</p>

            <p class="ptop">Once you are provisionally selected, you should proceed to prepare the semifinal and final
              repertoire, which you will also need to record and submit links to via email, no later than <b>June 15,
              2019</b>. Once your recordings are received, you will know within 2 weeks whether you have met the
              level required for the competition, in which case you will be a <b>“confirmed semifinalist”</b> and invited
              to Mumbai for the live rounds.</p>

            <h3>video recording guidelines</h3>
            <p>• Instrument: You must record your performances on an <u>acoustic piano</u>.</p>
            <p>• Quality: We are not extremely fussed about quality at this stage. You may use a video camera
              or a phone camera provided that the resolution is sufficiently high (minimum 360p) to capture
              the details of your face, your hands and the keyboard. Sound quality is also important and
              should not be distorted.</p>
            <p>• Editing: Editing is strictly prohibited. You may, however, clip the videos so that the leading and
              trailing silences are not very long. You are also advised strongly to record audio and video on
              the same device, as synchronization issues between sound and video will not be accepted
              under any circumstances. You must record every individual piece using a single camera, in <u>a
              single take and from a fixed position</u>.</p>
            <p>• Angle: We leave you to choose what angle you want for your video. However, it must include
              at least a side profile of your face and the entire keyboard from at least a 20º angle above the
              plane of the keys. Use the knowledge that the video is primarily to ensure authenticity of the
              recording and allow common sense to prevail. You may film from either the treble or bass side,
              but ensure that the image is not laterally inverted. Try to get the camera view to be as close to
              the keyboard as possible. Before you start the piece, please look directly into the camera to
              allow us a front view of your face.</p>
            <p>• Please use a mount of some kind for the recording device, such as a tripod, to ensure that the
              image is completely still and do not change the position of the device during the piece.</p>
            <p>• Make sure that no voices are heard on the recordings and please do not say anything in your
              recordings. </p>
            <p>• Here are some bad examples:</p>
            <p><a href="https://www.youtube.com/watch?v=CyHLWL_G6kY" target="_blank" style="color:#0088cc !important">https://www.youtube.com/watch?v=CyHLWL_G6kY</a> (Keyboard not in view)</p>
            <p><a href="https://www.youtube.com/watch?v=4ZiemNmklX0" target="_blank" style="color:#0088cc !important">https://www.youtube.com/watch?v=4ZiemNmklX0</a> (Too far, hence too much background)</p>
            <p>• Here are some good examples:</p>
            <p><a href="https://www.youtube.com/watch?v=d6kZ5XyJ9go" target="_blank" style="color:#0088cc !important">https://www.youtube.com/watch?v=d6kZ5XyJ9go</a></p>
            <p><a href="https://www.youtube.com/watch?v=g2kNBh2h44k" target="_blank" style="color:#0088cc !important">https://www.youtube.com/watch?v=g2kNBh2h44k</a></p>

            <h3 style="color: #0088cc">most important piece of advice</h3>
            <p class="ptop">
              When watching your recorded videos, if something is not quite right, you will have the urge to
              record again. This can go on endlessly as there is always scope for something getting better and the
              entire process can get very frustrating and draining. Ensure that you have a piece sufficiently well
              prepared before you record and set a reasonable limit on the number of times you will record a
              piece. Choose your best. There are no perfect recordings and a few minor slips here and there are
              not usually factors in the overall quality of the performance.</p>

            <h3><a href="mailto:conbrio@furtados.in" style="color: #0088cc !important">conbrio@furtados.in</a></h3>

            <p class="ptop">If you have any queries, please contact us via email and we will be happy to assist you with them.
              Always email <a href="mailto:conbrio@furtados.in" style="color: #0088cc !important">conbrio@furtados.in</a> and remember to CC the competition moderator at <a href="mailto:parveshjava@me.com" style="color: #0088cc !important">parveshjava@me.com</a>. We look forward to your participation in Con Brio 2019!</p>
         </div>       
      </div>
   </div>
</div>