<?php if($resp){
   foreach($resp as $key=>$val){//print_r($val);
?> 
<div class="row bb">
   <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
      <div class="product-item-holder size-big single-product-gallery small-gallery">
         <div class="single-product-gallery-item" id="slide1">
            <a data-lightbox="image-1" data-title="Gallery">
            <img class="img-responsive" alt="" src="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" data-echo="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" />
            </a>
         </div>
         <!-- /.single-product-gallery-item -->
      </div>
   </div>
   <div class='col-sm-6 col-md-9 product-info-block'>
      <div class="product-info">
         <h1 class="name"><?echo $val['proname'];?></h1>
         <div class="rating-reviews">
            <div class="row">
               <div class="col-sm-12">
                  <div class="reviews">
                  </div>
               </div>
            </div>
            <!-- /.row -->    
         </div>
         <!-- /.rating-reviews -->
         <div class="price-container info-container m-t-20">
            <div class="row">
               <div class="col-sm-6">
                  <div class="price-box m-1-26">
                     <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"></i> <?echo $this->libsession->priceformat($val['dicountprice']);?></span>
                     <?$stock=$val['isAvailable']>0 ? 'In stock' : 'Out of stock';
                        $style=$val['isAvailable']>0 ? 'value1' : 'value2' ?> 
                     <span class="<?php echo $style;?>"><?php echo $stock;?></span>
                  </div>
               </div>
            </div>
            <!-- /.row -->
         </div>
         <!-- /.price-container -->
         <div class="pricefilter">
            <span id="qtychange<?echo $val['qty']?>" style="font-size: 14px;">No of Quantity:<b> <?echo $val['qty']?>Qty.</b></span>
         </div>
         <?php if($val['couponid']){ ?>
            <div class="pricefilter">
               <span id="appliedcouponid<?echo $val['couponid']?>" style="font-size: 12px; color: #cc0028"><?php echo $val['couponcode']; ?> has been applied on this product</span>
            </div>
         <?php } ?>
         <!-- /.product-info -->
      </div>
   </div>
</div>
<?if(is_array($val['combo_product'])){ ?>
<h3 class="comt" style="width: 83%; margin: 0px 173px;">Combo Product</h3>
<?php //print_r($val['combo_product']);
   foreach($val['combo_product'] as $combokey=>$comboval){
   ?>
<div class="col-xs-12 col-sm-3 col-md-3 gallery-holder"></div>
<div class="col-xs-12 col-sm-9 col-md-9 gallery-holder">
   <div class="row bb">
      <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
         <div class="product-item-holder size-big single-product-gallery small-gallery">
            <div class="single-product-gallery-item" id="slide1">
               <a data-lightbox="image-1" data-title="Gallery">
               <img class="img-responsive" alt="" src="<? if(!isset($comboval['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $comboval['image'];} ?>" data-echo="<? if(!isset($comboval['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $comboval['image'];} ?>" />
               </a>
            </div>
            <!-- /.single-product-gallery-item -->
         </div>
      </div>
      <div class='col-sm-6 col-md-9 product-info-block'>
         <div class="product-info">
            <h1 class="name"><?echo $comboval['proname'];?></h1>
            <div class="rating-reviews">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="reviews">
                     </div>
                  </div>
               </div>
               <!-- /.row -->    
            </div>
            <!-- /.rating-reviews -->
            <div class="price-container info-container m-t-20">
               <div class="row">
                  <div class="col-sm-6">
                     <div class="price-box">
                        <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"></i> <?echo $this->libsession->priceformat(floor($comboval['dicountprice']));?></span>
                        <!--span class="value1">In Stock</span-->
                     </div>
                  </div>
               </div>
               <!-- /.row -->
            </div>
            <!-- /.price-container -->
            <div class="pricefilter">
               <span id="qtychange<?echo $comboval['qty']?>" style="font-size: 14px;">No of Quantity:<b> <?echo $comboval['qty']?>Qty.</b></span>
            </div>
            <!-- /.product-info -->
         </div>
      </div>
   </div>
</div>
<?php } } } ?> 

   
   <div class="col-lg-9 carting pull-right" style="margin-right: 34px;">
      <div class="col-lg-12" style="padding: 3px 0px 3px;">
         <?php if($giftcode){ ?>
         <div class="col-lg-12" style="color: #cc0028; font-weight: 700; text-align: right;"> 
            <?php echo $giftcode; ?> is successfully applied on this cart.
         </div>
         <?}else{?>
         <div class="col-lg-12" style="padding:10px;margin: 0px 8px;">
            <b class="col-lg-6"  style="padding:5px;font-size: 16px;color: #cc0028;">Gift card & coupon code</b>
            <div class="col-lg-6" style="padding-left: 6px;">
               <input type="text" placeholder="Enter code" value="" name="promocode" id="promocode" style="padding: 5px;">
               <input type="button" value="Apply" onclick="checkforgiftcode();" style="
border:  none;background: #cc0028;padding: 6px 10px;color: #fff;font-size: 14px;margin-left: -7px;width: 87px;">
            </div>
            
         </div>
         <?}?>
   </div>

   <div style="margin-left: 22px;">
      <div class="col-lg-3" style="padding: 5px 0px;font-size: 14px">
         <input type="radio" class="nadu" id="normal" name="shipmode" value="Normal" checked onclick="checkshippingcost()"> &nbsp; 
         <b>Normal Delivery </b>
      </div>
       <div class="col-lg-4" style="margin: 5px 0px;font-size: 14px">
         <div class="nadu">
            <input type="radio" class="nadu" id="express" name="shipmode" value="Express" onclick="checkshippingcost()"> &nbsp;<b>Express Delivery</b>
         </div>
      </div>
      <div class="col-lg-5" style="padding: 7px 0px;font-size: 14px">
         <b>Shipping Cost will be</b> : 
         <i class="fa fa-rupee" style="font-size:14px"></i>  <span class="shippingcost"></span>
      </div>

      </div>
     
      <div class="col-lg-5" style="margin-top: 10px;">
         <?php if($giftcode){ ?>
            <a href="<?php echo base_url(); ?>product/cancelcoupon" style="padding: 9px; background: #cc0028;">Click here to cancel the coupon<i class="fa fa-times-circle"></i></a>
         <?php } ?>
      </div>
       <div class="col-lg-5 pull-right">
         <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Cart Total</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($carttotalprice));?></span></div>
          <?php if($cartmaindiscount && !isset($giftcode)){ ?>
               <!--<div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Total amount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($carttotalprice));?></span></div>-->
           
               <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Discount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($cartmaindiscount));?></span></div>
            <?php } ?>
            <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px">
               <b>Delivery Charges</b> : 
               <i class="fa fa-rupee" style="font-size:14px"></i>  
               <span class="shippingcost"></span>
            </div>
            <?php if($giftcode){ ?>
                <!--<div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Total amount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($totalcart));?></span></div>-->

                <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Additional Discount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($totaldiscountedamount));?></span></div>
            <?php } ?>
            <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Order Total </b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span class="totalamt"><?echo $this->libsession->priceformat(floor($amount));?></span></div>
            <!--<div class="col-xs-6 Place_Order"><a href="<?php echo SITEMOBURL;?>product/distributepg">Proceed to Checkout</a></div>-->
            <br>
            <button class="btn btn btn-warning col-lg-4" onClick="movetonexttab()" style="width: 74%;margin-top: 5px;padding: 4px 7px; outline: none;text-align: center;">Place order</button>
      </div>

   </div>
   <div class="col-sm-12" style="padding-top: 10px; padding-bottom: 10px;">
         <b>* Note :</b><span> 10% discount on Books worth Rs.1,000/- and above *only applicable on non-discounted books</span>
         </div>
         <div class="col-sm-12">
         <b>* Note :</b><span> Delivery of product(s) is subject to availability of stock at the time the order is received. Furtados does not warranty the accuracy or    &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp; correctness of the information provided on the website.</span>
         </div>

<div class="clear"></div>
<?}else{?>
<div class="Favourite-no">
   <p>There are no products in your cart</p>
</div>
<?}?>    
<script type="text/javascript">
   jQuery(document).ready(function(){
      if(jQuery('input[type="radio"]').is(':checked'))
      {
         setTimeout(function(){
               checkshippingcost();
                  }, 1000);
      }
      function disableBack() { window.history.forward() }
   
          window.onload = disableBack();
          window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
   });
</script>
<script type="text/javascript">
   function checkshippingcost(){ 
      var shippingmode=jQuery("input[name='shipmode']:checked").val();
      jQuery.ajax({
         url:'<?php echo SITEMOBURL;?>product/checkshippingcost',
         type:'POST',
         data:{shippingmode:shippingmode},
         success:function(res)
         {
   
            result=jQuery.parseJSON(res);
            
            if(result.status== true){
               //alert(result.msg.total_shipping_cost);
               jQuery('.shippingcost').html(result.msg.total_shipping_cost);
               if(result.msg.showexp == 1 || result.msg.postState==0){
                  jQuery('.carto').show();
               }else{
                  jQuery('.carto').hide(); 
               }
               total_amt=<?echo $amount?>+result.msg.total_shipping_cost; 
               jQuery('.totalamt').html(total_amt);
            }else{
               jQuery('.shippingcost').html('0');   
            }
            
         }
      });
      
   }
   
</script>
<script>
   function checkforgiftcode(){
      
      var promocode=jQuery('#promocode').val();
       jQuery.ajax({
            url:'<?php echo SITEMOBURL;?>product/checkgiftcarddtl',
            type:'POST',
            data:{promocode:promocode},
            success:function(res){
               result=jQuery.parseJSON(res);
               if(result.status== false){ 
                  bootbox.alert(result.msg);
               }else{
                  palcefinalorder('coupon');
               }
               
            }
         });
      
      
      
   }
</script>

<style type="text/css">

span.value2 {
    color: #cc0028;
    font-size: 14px;
   }
</style>