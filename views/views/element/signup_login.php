<?php
		/*
				FILE NAME : signup_login.php
				CREATED : 4/Sep
				AUTHOR : SANJAY
				DESC : Sign Up / Login Popups.
				NOTE : Don't modify code , its working
		*/
?>
<script src="<? echo SITEJS?>jquery.easing-1.3.pack.js"></script>
<script src="<? echo SITEJS?>jquery.mousewheel-3.0.4.pack.js"></script>
<script src="<? echo SITEJS?>jquery.fancybox-1.3.4.pack.js"></script>
<link href="<?echo SITECSS?>jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css"> 
<style>
.clear{ clear:both;}
.top-sign-s{border:solid 1px #999; background-color:#fff; padding-bottom:10px;  margin-top:0px; font-family:'Raleway';}
.none{display:none;}
.switch{ border-bottom:solid 1px #CCCCCC;text-align:center; height:40px; line-height:40px; font-size:16px;}
.sign-input-box{width:270px; float:left; border-right: dotted 1px #ddd; margin-right:28px;}
.sign-divided-div{ float:left; margin-bottom:10px;}
.sign-divided-fb{width:260px; float:left;}
.sign-active-heading{font-size:24px; display:block; text-align:center; margin-bottom:20px; margin-top:10px; font-weight:bold; color:#fc5602;}
.custom-input-s{ margin-left:10px; border:1px solid #CCCCCC; background-color:#FFFFFF; display:block; height:30px; margin-bottom:10px; width:230px; padding-left:10px;   -moz-box-shadow: inset 0 0 5px #d9d7d7;
   -webkit-box-shadow: inset 0 0 5px #f2f2f2;box-shadow:         inset 0 0 5px #f2f2f2; border-radius:3px;}
.top-sign-s button{border:solid 1px #fc5602; margin-bottom:5px; width:230px; background-color:#fc5602; display:block; cursor:pointer; color:#fff; padding-bottom:5px; padding-top:5px; margin-bottom:5px; font-size:16px; text-align:center; margin-left:10px; font-weight:bold; border-radius:3px;}
.top-sign-s button:hover{border:solid 1px #fc5602; width:230px; background-color:#fc5602; display:block; color:#000; text-align:center;}
#fancybox-content, #fancybox-content:before, #fancybox-content:after
{
	box-sizing: inherit;
}
</style>
<a href="#signup_login_page" id="login_signup_popups"></a>
<div style="display:none;">
<div id="signup_login_page" style="box-sizing: unset;">
<!--Signup popups-->
<p id="flash_msg" class="flash_success"></p>
<div class="top-sign-s" id="signup_form" style="">
<div class="switch"><strong>Already have an account?</strong> <strong><a href="javascript:void(0);" onclick="show_hide('L');"> Login</a></strong></span>
</div>
<div class="clear"></div>
<div class="sign-active-heading">Create a new account</div>
<div class="sign-input-box">
<form id="userfrmlogin" method="post">
<input placeholder="Your name" id="fullname" name="fullname" title="Enter user name" class="custom-input-s" type="text"> 
<input placeholder="Enter email address" id="email_id" name="email" title="Enter a valid email address" class="custom-input-s" type="email" required>
<input name="password" id="password" placeholder="Password" title="Minimum 6 characters required" class="custom-input-s" type="password">
<input name="mobile" id="mobiled_num" placeholder="Mobile Number" title="Enter a valid mobile number" class="custom-input-s" type="tele"> 
<div class="gender-s" style="padding-left:12px;padding-bottom:4px;">
	<input name="gender" value="M" checked="checked" type="radio">Male&nbsp;&nbsp;&nbsp;<input name="gender" value="F"  type="radio">Female 
</div>
<button type="submit">CREATE MY ACCOUNT</button> 
</form>
</div>
<div class="sign-divided-div">
<a target="_parent" href="<?php echo $this->facebook->getLoginUrl(array('redirect_uri' => $this->config->item('redirecturl'), 'scope' => $this->config->item('scope'))); ?>"><img src="<?php echo SITEIMAGES; ?>fb.png" class="hand"/></a>
</div>
<div class="sign-divided-fb">
<a href="<?php echo $this->googleplus->getGoogleLoginUrl();?>"><img src="<?php echo SITEIMAGES;?>gg.png" class="hand"/></a>
<?php #$this->load->view('element/google_login');?> 
</div>
<div class="clear"></div>
</div>
<!--Login popups-->
<div class="top-sign-s" id="login_form">
<div class="switch">
<strong>New to Spidersmall? </strong><strong><a href="javascript:void(0);" onclick="show_hide('S');">Create a New Account</a></strong></span>
</div>
<div class="clear"></div>
<div class="sign-active-heading">Login to Your Account</div>
<div class="sign-input-box">
<form>
<input placeholder="Enter email address" id="login_email" title="Enter a valid email address" class="custom-input-s">
<input id="login_pwd" placeholder="Password" title="Minimum 6 characters required" class="custom-input-s" type="password">
<button type="button" onclick="buyerLogin();" class="button fill">Login</button>&nbsp;&nbsp;&nbsp;<span><a onclick="show_hide('F');" href="javascript:void(0);">Forgot Your Password?</a></span>
</form>
</div>
<div style="width:260px;float:right;">
	<div class="sign-divided-div">
	<a target="_parent" href="<?php echo $this->facebook->getLoginUrl(array('redirect_uri' => $this->config->item('redirecturl'), 'scope' => $this->config->item('scope'))); ?>"><img src="<?php echo SITEIMAGES;?>fb.png" class="hand"/></a>
	</div><br><br>
	<div class="sign-divided-fb">
	<a href="<?php echo $this->googleplus->getGoogleLoginUrl(); ?>"><img src="<?php echo SITEIMAGES;?>gg.png" class="hand"/></a>
	<?php #$this->load->view('element/google_login');?>
	</div>
</div>
	<div class="clear"></div>
</div>
<!--forgot password popups-->
<div class="top-sign-s" id="forgotpassword_form">
	<div class="switch">
		Don't have An Account? <em><strong><a href="javascript:void(0);" onclick="show_hide('S');">Create One!</a></strong></em>
	</div>
	<div class="clear"></div>
<div class="sign-active-heading" style="margin-bottom: 6px;margin-top: 4px;">Forgot Your Password?</div>
	<div class="sign-input-box" style="padding-left: 84px; width: 529px;">
	<span style="font-size: 11px;">Enter your Email Address here to receive a temporary password.</span>
<div>
	 <label>Your Email :</label>  <input placeholder="Enter your email address" type="text" id="forgot_email" title="Enter a valid email address" class="custom-input-s">
<button type="button" onclick="buyerForgotPassword();" class="button fill">Send Mail</button>
</div>
	</div>
	<div class="clear"></div>
</div>
</div>
</div>

<script type="text/javascript">
/*
		NAME : get_login_signup_pops 
		CREATED : 3/Sep
		AUTHOR : SANJAY
		DESC : Used to display signup pop-ups
*/
get_login_signup_pops = function(type){
	 jQuery('#flash_msg').hide(); 
		show_hide(type);
		jQuery("#login_signup_popups").click();
		jQuery("#fancybox-wrap").css({'padding':'0px'});
		jQuery("#fancybox-outer").css({'background-color':'grey'});
};

show_hide = function(type) {

		jQuery("#login_form").hide();
		jQuery("#signup_form").hide();
		jQuery("#forgotpassword_form").hide();
			if(type=="S")
			{
				jQuery("#signup_form").show();
			}
			else if(type=="L")
			{
				jQuery("#login_form").show();
			}
			else if(type="F")
			{
				jQuery("#forgotpassword_form").show();
			}
};

jQuery(document).ready(function(){
	jQuery("a#login_signup_popups").fancybox({
		//'height' : 600,
		'width': 592,
		'autoSize': true,
		padding : 0, background: 'none',
		autoDimensions : true,
	});
});

buyerSignUp = function(){
		var fullname = jQuery("#fullname").val();
		alert(fullname);
		var emailId = jQuery("#email_id").val();
		var passd = jQuery("#password").val();
		var mobiled_num = jQuery("#mobiled_num").val();
		var gender = jQuery("[name='gender']:checked").val();
			jQuery.ajax({
					url: APIURL+'customer/adduser',
					type:'post',
					data : {email:emailId,password:passd,telephone:mobiled_num,gender:gender,fullname:fullname},
			}).done(function(response){
					resp = jQuery.parseJSON(response);
						if(resp.status==1)
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_error').addClass('flash_success').show().fadeOut(2000,function(){ jQuery.fancybox.close();});
						}
						else
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_success').addClass('flash_error').show();
						}
			});
};

buyerLogin = function(){
	  
		var email = jQuery("#login_email").val();
		var pwd = jQuery("#login_pwd").val();
			jQuery.ajax({
					url: APIURL+'customer/login',
					type:'post',
					data : {email:email,password:pwd},
			}).done(function(response){
					resp = jQuery.parseJSON(response);
						if(resp.status==1)
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_error').addClass('flash_success').show().fadeOut(2000,function(){ jQuery.fancybox.close();});
							manageWishList(); //-- Update 27/Nov for managing cookies, Sanjay
							
							return false;
						}
						else
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_success').addClass('flash_error').show();
						}
			});
};

/*
		NAME : manageWishList
		CREATED : 27/Nov
		AUTHOR : SANJAY
*/
function manageWishList()
{
	var callBackFlag = false;
	jQuery.ajax({
				url : APIURL+'customer/ajax_getWishList?_'+Math.random(),
	}).done(function(resp){
			var response = jQuery.parseJSON(resp);
				if(response.status==1)
				{
					console.log(response.wishlist_csv);
					var found = false; 
					var wish_csv = response.wishlist_csv;
					console.log(wish_csv);
						if(!(!wish_csv))
						{
					wish_csv = wish_csv.split(",");
					jQuery.each(wish_csv ,function(index,proId){ console.log(index+" = "+proId);
							var proid=proId; 
							var cookie ='';
							cookie=jQuery.cookie('mywishlist');
							var items = cookie ? cookie.split(',') : new Array();
							jQuery.each(items,function(index,value) {
								if(proid==value){
										found = true; 
										return false;
									}	
								});
							if(found == false ){
									if(!cookie){
										newpro = proid;
									}else{
										newpro = cookie+','+proid;
									}
						jQuery.cookie('mywishlist',newpro,{ path:'/'});
						callBackFlag = true;
							}
					});
					}	
				}
		},function(){ 
							if(callBackFlag==true)
									{
							var wishlist_csv=jQuery.cookie('mywishlist');
							var url=APIURL+'customer/updatewishlist';
							if(wishlist_csv!="")
								{
								jQuery.ajax({
											type: "POST",
											url: url,
											data:{wishlist:wishlist_csv},
										}).done(function(resp){
												var response = jQuery.parseJSON(resp);
													if(response.status==1)
													{
														location.reload();
													}
										});
								}	
									}
									else
									{
										location.reload();
									}
					});
}

buyerForgotPassword = function(){
		var email = jQuery("#forgot_email").val();
		//alert(email);
		jQuery.ajax({
					url: APIURL+'customer/forgotpassword',
					type:'post',
					data : {email:email},
			}).done(function(response){
					resp = jQuery.parseJSON(response);
						if(resp.status==1)
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_error').addClass('flash_success').show().fadeOut(2000,function(){ jQuery.fancybox.close();});
						}
						else
						{
							jQuery("#flash_msg").html(resp.msg).removeClass('flash_success').addClass('flash_error').show();
						}
			});
};

jQuery(document).ready(function(){
//-- For login
		jQuery("#login_email,#login_pwd").each(function(){
					jQuery(this).live("keypress", function(e) {
							var code = (e.keyCode ? e.keyCode : e.which);
								if (code == 13) { buyerLogin(); }
						});
			});
//-- For sign up.
		jQuery("#email_id,#password,#mobiled_num").each(function(){
					jQuery(this).live("keypress", function(e) {
							var code = (e.keyCode ? e.keyCode : e.which);
								if (code == 13) { buyerSignUp(); }
						});
			});
			
//--- Forgot password
jQuery("#forgot_email").live("keypress", function(e) {
							var code = (e.keyCode ? e.keyCode : e.which);
								if (code == 13) { buyerForgotPassword(); }
						});
});

 jQuery(function() {
  jQuery.validator.addMethod("accept", function(value, element, param) {
  return value.match(new RegExp("." + param + "$"));
});
    // Setup form validation on the #register-form element
    jQuery("#userfrmlogin").validate({		
  
         ignore:[], 
        rules: {
          
            email: {
                required: true,
                email: true
            },
			
            password: {
                required: true,
                minlength: 6
            },
		
			mobile:{
				required:true,
				maxlength:10,
				minlength:10
			},
			fullname: {  
                required: true,
				accept: "[a-zA-Z]+",
				//lettersonly: true,
				minlength:3
            },
		
			
			gender:{
				 required: true
    				 
			}
            
        },
        
        // Specify the validation error messages
        messages: {
             fullname: {
                required: "Please enter your first name", 
                accept: "please enter  letters only",
            },
           
			
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long" 
            },
			
			mobile:"Please enter valid phone number", 
            email: "Please enter a valid email address", 
			
			gender:"please select gender"
          
        },
         submitHandler: function(){
			 buyerSignUp();
		 },
        
    });

 });
</script>