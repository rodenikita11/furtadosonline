<section>
            <div class="block ">
                <div class="container">

                    <div class="row">
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-gift fS16"></i> Featured Products
                            </div>
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="widget-block wow fadeInUp animated" data-wow-duration="1s">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="Effect,%20premium%20HTML5&amp;CSS3%20template_files/product5.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="widget-block wow fadeInUp animated" data-wow-duration="1s">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-3">
                                        <img class="img-responsive" src="Effect,%20premium%20HTML5&amp;CSS3%20template_files/product2.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-9">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Product name</a>
                                            <p class="product-price"><span>$330</span> $320.99</p>

                                        </div>
                                        <div class="product-rating">
                                            <div class="stars">
                                                <span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span><span class="star active"></span>
                                            </div>
                                            <a href="" class="review">8 Reviews</a>
                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-twitch fS16"></i> Our Store
                            </div> 
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="wow fadeInUp animated" data-wow-duration="1s">
                                <p>
                                    <span class="dropcap">L</span>orem 
ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
tempor incididunt ut labore et dolore magna aliqua. Duis aute irure 
dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 
nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in 
culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <blockquote>
                                    Duis aute irure dolor in 
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla 
pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa 
qui officia deserunt mollit anim id est laborum.
                                </blockquote>  
                            </div>

                        </article>
                        <article class="col-md-4">
                            <div class="widget-title">
                                <i class="fa fa-comments fS16"></i> Latest Posts
                            </div>
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="widget-block wow fadeInUp animated" data-wow-duration="1s">
                                <div class="row">
                                    <div class="col-md-4  col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="Effect,%20premium%20HTML5&amp;CSS3%20template_files/post-image-2.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8  col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Summer sale</a>

                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="widget-block wow fadeInUp animated" data-wow-duration="1s">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="Effect,%20premium%20HTML5&amp;CSS3%20template_files/post-image-4.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Call now</a>

                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                            <div style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="widget-block wow fadeInUp animated" data-wow-duration="1s">
                                <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-4">
                                        <img class="img-responsive" src="Effect,%20premium%20HTML5&amp;CSS3%20template_files/post-image-3.jpg" alt="" title="">   
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-8">
                                        <div class="block-name">
                                            <a href="#" class="product-name">Good day</a>

                                        </div>
                                        <p class="description">Lorem ipsum dolor sit amet, con sec tetur adipisicing elit.</p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </section>