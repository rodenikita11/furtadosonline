<?php
class Libprodtl
{
    public $ci;
    public function __construct()
    {
        $CI =& get_instance();
        $CI->load->model('Do_product');
        $CI->load->model('Do_common');
        $this->ci = $CI;
        
    }
    
	protected function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Libprodtl.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . json_encode($query).PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/admin_database_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	}
    public function inert_review($param)
    {
        if ($param) {
            if (is_int($param['id'])) {
                $res = $this->ci->Do_product->insert_review($param);
                
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
        
    }
    
    
    public function getsimiliar_prod($param)
    {
        
        if ($param) {
            if (is_int($param['id'])){
                $memkey='similar_products'.json_encode($param);

                $memCacheDate = $this->ci->libsession->getMemcache($memkey);
                if($memCacheDate){
                    return $memCacheDate;
                }

                $res= $this->ci->Do_product->getsimiliar_prod($param);
                //print_R($res); die;  
                $wishlist = $this->ci->libsession->getSession('wishlist');
                if ($res) {
                    foreach ($res as $row) {
                        //print_r($row);die;
                        if ($wishlist) {
                            $arrwishlist = explode(',', $wishlist);
                            if (in_array($row['proid'], $arrwishlist)) {
                                $prodata[$row['proid']]['wishlist'] = '1';
                            } else {
                                $prodata[$row['proid']]['wishlist'] = '0';
                            }
                        } else {
                            $prodata[$row['proid']]['wishlist'] = '0';
                        }
                        
                        $proname                        = preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
                        $proname                        = str_replace("--", "-", $proname);
                        $proname                        = strtolower(rtrim($proname, '-'));
                        $proid                          = $row['proid'];
                        $prodata[$row['proid']]['id']   = $row['proid'];
                        $prodata[$row['proid']]['url']  = '' . SITEMOBURL . '' . $proname . '/' . $proid . '';
                        $prodata[$row['proid']]['name'] = $row['proname'];
                        
                        if ($row['prothumbnail']) {
                            $prodata[$row['proid']]['img'] = str_replace('http:', 'https:', $row['prothumbnail']);
                        } else {
                            $prodata[$row['proid']]['img'] = SITEIMAGES . 'mobimg/noimage1.png';
                        }
                        
                        $prodata[$row['proid']]['price']         = $row['onlinePrice'];
                        $prodata[$row['proid']]['qty']           = $row['quantity'];
                        $prodata[$row['proid']]['discountprice'] = $row['discountPrice'];
                        $setprice                                = $row['onlinePrice'] - $row['discountPrice'];
                        $prodata[$row['proid']]['percentage']    = round($setprice / $row['onlinePrice'] * 100);
                        $prodata[$row['proid']]['callforprice']  = $row['callforprice'];
                        $prodata[$row['proid']]['isAvailable']   = $row['isAvailable'];
                        
                    }
                    $this->ci->libsession->setMemcache($memkey,$prodata);
                    //print_R($prodata); die;  
                    return $prodata;
                }
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
    }
    
    public function get_offerfilter($param){
        $memkey='get_offerfilter'.json_encode($param);
        //echo $memkey; die;
        $memCacheDate = $this->ci->libsession->getMemcache($memkey);
        if($memCacheDate){
            return $memCacheDate;
        }
        
        $res['filter_offer']= $this->ci->Do_product->getofferfilter($param);
        //echo '<pre>';
        //print_r($res['filter_offer']); die;
        if($res['filter_offer']){
            $i=0;
            foreach($res['filter_offer']['brand'] as $key => $val){
                $filterdata['getfilter']['brand'][$i]['brandid']= $val['brandid'];
                $filterdata['getfilter']['brand'][$i]['brandname']= $val['brandname'];
                $i++;
            }
            $filterdata['getfilter']['available']=$res['filter_offer']['available'][1]['offerid'];
            $filterdata['getfilter']['notavailable']=$res['filter_offer']['available'][0]['offerid'];
            $k= 0;
            foreach ($res['filter_offer']['category'] as $key => $val){
                if($val['catparent_id'] !=1 && $val['catparent_id']!=2 && $val['catparent_id']!=3 && $val['catparent_id']!=4){
                    $url=ADMINURL.'product/promotion/'.$param['id'].'/?categoryid='.$val['categoryid'];
                    $filterdata['filter']['category'][$val['catparentname']]['url']=ADMINURL.str_replace(array(' ', '&'), array('','-'), $val['catparentname']).'/'.$val['catparent_id'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['id']=$val['categoryid'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['name']=$val['catname'];
                    $filterdata['filter']['category'][$val['catparentname']]['sub'][$k]['url']=$url;
                    $k++;
                }
            }
            //echo '<pre>';
            //print_r($filterdata); die;
            $filterdata['getfilter']['minprice']= $res['filter_offer']['price'][0]['minamount'];
            $filterdata['getfilter']['maxprice']= $res['filter_offer']['price'][0]['maxamount'];

            foreach($res['filter_offer']['discount'] as $key=>$val){
                $filterdata['discount'][$val['discount']]=$val['discount'];
            }

            $this->ci->libsession->setMemcache($memkey,$filterdata);
            return $filterdata;
        }
    }
    
    public function getproductpromotiondtl($param){
        if ($param) {
            // print_r($param);
            // $param['id']=intval($param['id']);
            // //print_r($param['id']);
            if (is_int($param['id'])) {
                //$cacheparam['id']=$param['id'];
               // $memkey = 'libprodtl_promotion_'.$cacheparam.'_'.$param['start'];
                //echo $memkey; die;
                //error_log($memkey); 
                //$memCacheDate = $this->ci->libsession->getMemcache($memkey);
               /// if($memCacheDate){
                   // return $memCacheDate;
               // }
                $param['count']=true;
                $rescount=$this->ci->Do_product->getproductpromotiondtl($param);
                $total_rows=$rescount['prodata'][0]['cnt'];
                if($param['bookofthemonth']==1){
                    $config['base_url']= ($param['module']!='brandsfilters') ? ADMINURL.'product/bookofthemonth' : ADMINURL.$param['brandname'].'/'.$param['brandid'];
                }else{
                     $config["base_url"] = ADMINURL.'product/promotion/'.$param['id'];
                }
                $config['total_rows'] =$total_rows;
                $config['per_page'] = LIMIT;
                $config['uri_segment']=4;
                $config['enable_query_strings'] = true;
                if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

                $this->ci->pagination->initialize($config);
                $param['count']=false;
                $res = $this->ci->Do_product->getproductpromotiondtl($param);
                
                $wishlist = $this->ci->libsession->getSession('wishlist');
                if ($wishlist) {
                    $arrwishlist = explode(',', $wishlist);
                }
                $prodata['links']=$this->ci->pagination->create_links();
                //echo $prodata['links']; die;
                //echo '<pre>';
                //print_r($res['prodata']);die;
				// $this->log_fileDetails('getproductpromotiondtl', $res);
                if ($res['prodata']){
                    $prodata['promotionname']                  = $res['prodata'][0]['promotionname'];
                    foreach ($res['prodata'] as $row) {

                        if ($wishlist) {
                            if (in_array($row['proid'], $arrwishlist)) {
                                $prodata['product'][$row['proid']]['wishlist'] = '1';
                            } else {
                                $prodata['product'][$row['proid']]['wishlist'] = '0';
                            }
                        } else {
                            $prodata['product'][$row['proid']]['wishlist'] = '0';
                        }
                        
                        $proname= preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
                        $proname= str_replace("--", "-", $proname);
                        $proname= strtolower(rtrim($proname, '-'));
                        $proid= $row['proid'];
                        $prodata['product'][$row['proid']]['id']   = $proid;
                        $prodata['product'][$row['proid']]['url']  = '' . SITEMOBURL . '' . $proname . '/' . $proid . '';
                        $prodata['product'][$row['proid']]['name'] = $row['proname'];
                        if ($row['prothumbnail']) {
                            $prodata['product'][$row['proid']]['img'] = str_replace('http:', 'https:', $row['prothumbnail']);
                        } else {
                            $prodata['product'][$row['proid']]['img'] = SITEIMAGES . 'mobimg/noimage1.png';
                        }
                        $prodata['product'][$row['proid']]['promotiontitle']  = $row['title'];
                        $prodata['product'][$row['proid']]['promotionname']   = $row['name'];
                        $prodata['product'][$row['proid']]['promotionbanner'] = str_replace('http:', 'https:', $row['banner']);
                        $prodata['product'][$row['proid']]['logoimg']         = $row['logoimg'];
                        $prodata['product'][$row['proid']]['topMenuImage']    = $row['topMenuImage'];
                        $prodata['product'][$row['proid']]['price']           = $row['onlinePrice'];
                        $prodata['product'][$row['proid']]['qty']             = $row['quantity'];
                        $setprice                                  = $row['onlinePrice'] - $row['discountPrice'];
                        $prodata['product'][$row['proid']]['percentage']      = round($setprice / $row['onlinePrice'] * 100);
                        $prodata['product'][$row['proid']]['callforprice']    = $row['callforprice'];
                        $prodata['product'][$row['proid']]['discountprice']   = $row['discountPrice'];
                        $prodata['product'][$row['proid']]['avaliable']       = $row['isAvailable'];

                        /*$setprice=$row['onlinePrice']-$row['discountPrice'];
                        $prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
                        if($row['callforprice']=='NO'){
                            $percentage=round($setprice/$row['onlinePrice']*100);
                            if($percentage >0 && $percentage <=5){
                                $prodata['percentage']['0_5']='0 to 5%';
                            }else if($percentage >5 && $percentage <=10){
                                $prodata['percentage']['5_10']='5 to 10%';
                            }else if($percentage >10 && $percentage <=15){
                                $prodata['percentage']['10_15']='10 to 15%';
                            }else if($percentage >15 && $percentage <=20){
                                $prodata['percentage']['30_40']='30 to 40%';
                            }else if($percentage >40){
                                $prodata['percentage']['40_more']='40% and more';
                            }
                        }*/
                    }

                    //echo '<pre>';
                    //print_r($prodata); die;

                    $this->ci->libsession->setMemcache($memkey,$prodata);
                    return $prodata;
                }
            } else {
                echo "invalid data type of product id";
                exit;
            }
            
        }
    }
    
	public function getproductpromotiondtlX($param, $paramx){
	  		//echo 'under this';die('mara');
	       // $memkey = 'liburl_get_categorydetails'.json_encode($param);

	       // $memCacheDate = $this->ci->libsession->getMemcache($memkey);
	       // if($memCacheDate && false){
	         // return $memCacheDate;
	       // }
		   
	       /**** New Elastic Search Code ****/
			// user id check kar... and search id ke sath log kar de!
			if($param){
			   if(is_int($param['id'])){
			   		if(is_string($param['branddtl'])){
						$param['branddtl']=json_decode(str_replace('_', ',', $param['branddtl']));
					}
					if(is_string($param['search'])){
						$param['search']=$param['search'];
					}
					if(is_string($param['avaliable'])){
						$param['avaliable']=json_decode($param['avaliable']);
					}
					if($param['percentage']){
				 		$param['percentage']=json_decode($param['percentage']);
					}

					$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
					$url = "http://3.210.70.215:8080/listing/api/v1.0/0/promotion/".$param['start']."/".LIMIT.'/'.rawurlencode($param['id']).'.0?x=';
					$base_url = "http://3.210.70.215:8080/listing/api/v1.0/0/promotion/0/0/".rawurlencode($param['id']).'.0?x=';
					if( isset($paramx['branddtl']) || isset($paramx['available']) || isset($paramx['minprice']) ||
							isset($paramx['maxprice']) || isset($_SESSION['usrid']) || isset($paramx['catid']) || 
							isset($paramx['itemtype']) || isset($paramx['discount'])|| isset($paramx['sort'])
						){
							$url=$url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$paramx['minprice'].$paramx['maxprice'].$paramx['discount'].$paramx['sort'].$usrid;
							$base_url = $base_url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$usrid;
						}
					// var_dump($param, $paramx); 
					//echo $url; exit;
					$get_base_data = $this->ci->libapi->callAPI('GET', $base_url , false);
					$get_data = $this->ci->libapi->callAPI('GET', $url , false);
					//var_dump($url, $get_data); exit;
					$base_res = json_decode($get_base_data, true);
					$res1 = json_decode($get_data, true);
					$errors = $res['res']['errors'];
					
					$wishlist=$this->ci->libsession->getSession('wishlist');

					if($res1){
						if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
						$prodata['promotionname'] = $res1['data'][0]['promotionname'];
						$patterntocheck=array("/[\/\&%#',\"\$\s]/");
						$pattertobereplaced=array("-");
						$namereplacearray=array('--', '---', '----', '-----');
						$filteredname=preg_replace($patterntocheck, $pattertobereplaced, $param['name']);
						$securename=str_replace($namereplacearray, $pattertobereplaced, $filteredname);
						$config["base_url"] = ADMINURL.'product/promotion/'.$param['id'];
						$config['total_rows']=$res1['total'];
						$config['per_page']=LIMIT;
						$config['uri_segment']=4;
						$config['enable_query_strings']=TRUE;
						$this->ci->pagination->initialize($config);
						$prodata['links']=$this->ci->pagination->create_links();
						
						$model['count']=false;
						foreach($res1['data'] as $row){
							$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
							$proname=str_replace("--","-", $proname);
							$proname=strtolower(rtrim($proname,'-'));
							$prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$row['proid'].'';
							if($wishlist){
								$arrwishlist=explode(',',$wishlist);
								//print $row['proid'].'hiiiii';
								if(in_array($row['proid'],$arrwishlist)){
									$prodata['product'][$row['proid']]['wishlist']='1';
								}else{
									$prodata['product'][$row['proid']]['wishlist']='0';
								}
							}else{
								$prodata['product'][$row['proid']]['wishlist']='0';
							}
							$prodata['product'][$row['proid']]['id']=$row['proid'];
							$prodata['product'][$row['proid']]['url']=SITEMOBURL.''.$proname.'/'.$row['proid'].'';
							$prodata['product'][$row['proid']]['name']=$row['proname'];
							$prodata['product'][$row['proid']]['catname']=$row['third_catname'];
							$prodata['maincatname']=$row['maincatname'];

							$prodata['product'][$row['proid']]['onlineprice']=$row['onlineprice'];
							$prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
							$setprice = $row['onlineprice'] - $row['discountprice'];
							//echo $setprice;
							$prodata['product'][$row['proid']]['percentage'] = round($setprice / $row['onlineprice'] * 100);
							// $prodata['product'][$row['proid']]['callforPrice']=$row['callforprice']; -------------------------------> Not Found
							$image1=str_replace(SITEMOBILEURL,FILEBASEPATH,str_replace('http:','https:',$row['prothumbnail']));
							$prodata['product'][$row['proid']]['prothumbnail']= !empty(str_replace('http:','https:',$row['prothumbnail'])) ? str_replace('http:','https:',$row['prothumbnail']) : '';
							$prodata['product'][$row['proid']]['firstcatid']=$row['first_catid'];
							//$prodata[$row['proid']]['image']=$row['prothumbnail'];
							$prodata['product'][$row['proid']]['avaliable']=$row['isavailable'];
							$prodata['product'][$row['proid']]['third_catid']=$row['third_catid'];
							$prodata['product'][$row['proid']]['third_category']=$row['third_catname'];
							$prodata['product'][$row['proid']]['price']=$row['onlineprice'];
					
							// Data not found in search
							 $prodata['catparent_name']=$row['catparentname'];
							 $prodata['catmeta_description']=$row['catmeta_description'];
							 $prodata['catmeta_keyword']=$row['catmeta_keyword'];
							//Added by Pritish 12-06-2018
							$prodata['meta_title']=stripslashes($row['catmeta_title']);
							$prodata['meta_description']=stripslashes($row['catmeta_description']);
							//echo $prodata['meta_description']; die;
							$prodata['meta_keyword']=stripslashes($row['catmeta_keyword']);

							 $prodata['catparentid']=$row['catparentid'];
							 $r1=array(' ','&');
	              			 $r2=array('-','-');
							 $catparurl=str_replace($r1,$r2, $row['catparentname']);
							 $prodata['catparenturl']=ADMINURL.strtolower($catparurl).'/'.$row['catparentid'];
							 if($row['prothumbnail']){
								$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
							 }else{
								$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							 }
							 
							 if($row['callforprice']=='NO'){
								$percentage=round($setprice/$row['onlinePrice']*100);
								if($percentage >0 && 	$percentage <=10){
									$prodata['percentage']['1_10']='1 to 10%';
								}else if($percentage >10 && 	$percentage <=20){
									$prodata['percentage']['10_20']='10 to 20%';
								}else if($percentage >20 && 	$percentage <=30){
									$prodata['percentage']['20_30']='20 to 30%';
								}else if($percentage >30 && 	$percentage <=40){
									$prodata['percentage']['30_40']='30 to 40%';
								}else if($percentage >40 && 	$percentage <=50){
									$prodata['percentage']['40_50']='40 to 50%';
								}else if($percentage >50 && 	$percentage <=60){
									$prodata['percentage']['50_60']='50 to 60%';
								}else if($percentage >60 && 	$percentage <=70){
									$prodata['percentage']['60_70']='60 to 70%';
								}else if($percentage >70 && 	$percentage <=80){
									$prodata['percentage']['70_80']='70 to 80%';
								}else if($percentage >80 && 	$percentage <=90){
									$prodata['percentage']['80_90']='80% to 90%';
								}else if($percentage >90 && 	$percentage <=99){
									$prodata['percentage']['90_'.$percentage]='90% to '.$percentage.'%';
								}
							}

						} //For loop ends here
					} // res1 if condition ends here.

				  	/* Filters code starts here */
				  	$url_filters = $url."&filters=true";
						// {"third_catid":"1127","third_catname":"WOODWIND ACCESSORIES","first_catid":"2","second_catid":"111","first_catname":"Music Books","second_catname":"MISCELLANEOUS BOOKS"}
					$get_data_filters = $this->ci->libapi->callAPI('GET', $url_filters , false);
					//var_dump($url_filters,$get_data_filters);
					$res_filters = json_decode($get_data_filters, true);
					$cat_len = count($res_filters['data']['unique_cat']);
					// structuring categories to fit UI
					$x=0;
					for( $i=0; $i < $cat_len; $i++ ){
						$newarray['id']=$res_filters['data']['unique_cat'][$i]['third_catid'];
						$newarray['name']=$res_filters['data']['unique_cat'][$i]['third_catname'];

						if($param['module']=='brandsfilters'){
							$newarray['url']=SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
						}else if($param['module']=='promotionsfilters'){
							$newarray['url']=SITEURL.'product/promotion/'.$param['id'].'.0?x=&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'];
						}else{
							if($param['bookofthemonth']==1){
								$newarray['url']=SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
							}else{
								$newarray['url']=SITEURL.'product/search/?maincat='.$res_filters['data']['unique_cat'][$i]['first_catid'].'&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
							}
						}

						if($param['module']=='brandsfilters'){
							$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
						}else if($param['module']=='promotionsfilters'){
							// $newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] =SITEURL.'product/promotion/'.$param['id'].'.0?x=&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'];

						}else{
							if($param['bookofthemonth']==1){
								$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
							}else{
								// realx for now
								if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
									$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/search/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];	
								}
							}
						}
						if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
							$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['sub'][] = $newarray;
						}
						$x++;
					}
					// Structuring Brand to fit UI
					$brand_len = count($res_filters['data']['unique_brand']);
					for( $m=0; $m < $brand_len; $m++ ){
						$res_filters['data']['unique_brand'][$m]['brandid'] = $res_filters['data']['unique_brand'][$m]['id'];
					}
					//var_dump($newfilterarray,$res_filters);
					$prodata['brandfilter'] = $res_filters['data']['unique_brand'];
					$prodata['catfilter'] = $newfilterarray;
					$prodata['minprice']= $res_filters['min_amt'];
					$prodata['maxprice']= $res_filters['max_amt'];
					$prodata['mindiscount']= round( $res_filters['min_discperc'] );
					$prodata['maxdiscount']= round( $res_filters['max_discperc'] );
				
					//var_dump($prodata);die('mara');
					$this->ci->libsession->setMemcache($memkey,$prodata);
					return $prodata;
				}else{
					echo "invalid data type of category id"; exit;
				} // If int param check ends here

			} // if param check ends here
    }
	
    public function getpromotiondtl()
    {
        $res = $this->ci->Do_product->getallpromotions();
        // print_r($res);die;
        if ($res['prodata']) {
            foreach ($res['prodata'] as $row) {
                $prodata[$row['id']]['url']       = '' . SITEMOBURL . 'product/promotion/' . $row['id'] . '';
                $prodata[$row['id']]['title']     = $row['title'];
                $prodata[$row['id']]['img']       = str_replace('http:', 'https:', $row['bannerImage']);
                $prodata[$row['id']]['startDate'] = $row['startDate'];
                $prodata[$row['id']]['endDate']   = $row['endDate'];
                $prodata[$row['id']]['promotionofferDisplay']   = $row['promotionofferDisplay'];
            }
            //print_R($prodata); die; 
            return $prodata;
        }
    }
    
    public function getrecently_viewed($param)
    {
        //print_R($param); die;
        if ($param) {
            if (is_int($param['id'])) {
                // print_R($_COOKIE['proid']);  die;
                
                
                if (isset($_COOKIE['proid'])) {
                    $existedcookie = unserialize($cookieval);
                    print_R($existedcookie);
                    if (!in_array($existedcookie, $param['id'])) {
                        $existedcookie[] = $param['id'];
                        $existedcookie   = $existedcookie;
                        print_R($cookieval);
                        $cookieval = serialize($existedcookie);
                    }
                } else {
                    $cookieval = $param['id'];
                }
                setcookie('proid', $cookieval);
                
            }
        }
        
        
        
    }
    
    public function addreview($param)
    {
        
        if ((int) $param['pid']) {
            $param['pid'] = (int) $param['pid'];
            
            $res = $this->ci->Do_product->insert_review($param);
            
            if ($res) {
                return $res;
            }
        } else {
            echo "there is some problem with the parameter";
        }
        echo json_encode($data);
    }
    
    /*created by viki 28/2/2018*/
    public function addenquirydtl($param)
    {
        if ($param) {
            $param['productId'] = intval($param['productId']);
            if (is_int($param['productId'])) {
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $param['ip'] = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $param['ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $param['ip'] = $_SERVER['REMOTE_ADDR'];
                }
                //print_R($param);
                $res = $this->ci->Do_product->addenquirydtl($param);
                if ($res) {
                    return true;
                } else {
                    return false;
                }
            } else {
                echo "invalid data type of product id";
            }
            echo json_encode($data);
        }
        
    }
    public function pianoenquirydtl($param)
    {
        if ($param) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $param['ip'] = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $param['ip'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $param['ip'] = $_SERVER['REMOTE_ADDR'];
            }
            $res = $this->ci->Do_product->addpianoenquirydtl($param);
            if ($res) {
                return true;
            } else {
                return false;
            }
            echo json_encode($data);
        }
        
    }
    
    /*end viki 28/2/2018*/
}
?> 