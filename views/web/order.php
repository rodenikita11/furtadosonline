<!--  ============================================== -->
<div class="body-content" id="top-banner-and-menu">
<div class="container-fluid">
<div class="row">
<!-- ============================================== CONTENT ============================================== -->
<div class="col-xs-12 col-sm-12 col-md-12 outer-top-150">
<div class="container">
   <div class="row">
      <div class='col-md-10 sidebar'>
         <div class=" outer-top-ss">
            <div class="panel-group" id="accordion">
               <?php if(!$this->libsession->getSession('usrid')){ ?>
               <div class="panel panel-default" id="frstlogincheck">
                  <div class="panel-heading headingar loginfrist">
                     <h4 class="panel-title odertitle">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1. Login or Signup</a>
                     </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-sm-8 form-group" style="padding: 0px 20px;">
                              <input type="text" placeholder="Enter Email / Mobile Number" class="form-control" style="background: transparent;    padding: 6px 0px;">
                           </div>
                           <div class="col-sm-8 form-group" style="padding: 0px 20px;">
                              <input type="text" placeholder="Enter Password" class="form-control" style="background: transparent;    padding: 6px 0px;">
                           </div>
                           <div class="col-sm-8">
                              <!-- Indicates caution should be taken with this action -->
                              <button type="button" class="btn btn-warning">Continue</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            <?php } ?>
               <div class="panel panel-default">
                  <div class="panel-heading headingar loginfrist">
                     <h4 class="panel-title odertitle">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" onclick="shippingaddress();" id="shippingaddressdiv">2. Delivery address</a>
                     </h4>
                  </div>
                  <div id="collapsetwo" class="panel-collapse collapse">
                     <div class="panel-body">
                        <div class="row">
                           <div id="alladdress">
                              <b><p>Please wait while we get your saved addresses.. <img src="https://vignette.wikia.nocookie.net/lego/images/b/b4/Loading_Animation.gif/revision/latest/scale-to-width-down/480?cb=20120528032206" width="48"></p></b>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="panel panel-default">
                  <div class="panel-heading headingar">
                     <h4 class="panel-title odertitle">
                        <a class="disabled-checkout" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id="clickonordersummary" id="ordersummarydiv">3. Order SummAry</a>
                     </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                     <div class="panel-body" id="finalcart">
                        <b><p>Please wait while we generate your order.. <img src="https://vignette.wikia.nocookie.net/lego/images/b/b4/Loading_Animation.gif/revision/latest/scale-to-width-down/480?cb=20120528032206" width="48"></p></b>
                     </div>
                  </div>
               </div>
               <div class="panel panel-default">
                  <div class="panel-heading headingar">
                     <h4 class="panel-title odertitle">
                        <a class="disabled-checkout" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" id="collapsefourfour">4.  Payment Option</a>
                     </h4>
                  </div>
                  <div id="collapsefour" class="panel-collapse collapse">
                     <div class="panel-body">
                        <div class="row">
                           <div class="col-lg-11 col-md-12 col-sm-8 col-xs-9 bhoechie-tab-container">
                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bhoechie-tab-menu">
                                 <div class="list-group">
                                    <a href="<?php echo SITEMOBURL; ?>product/distributepg" class="list-group-item active text-left">
                                    Pay by Credit Card
                                    </a>
                                    <a href="<?php echo SITEMOBURL; ?>product/distributepg" class="list-group-item text-left">
                                    Pay by Debit Card
                                    </a>
                                    <a href="<?php echo SITEMOBURL; ?>product/distributepg" class="list-group-item text-left">
                                    Pay by Net Banking
                                    </a>
                                    <a href="<?php echo SITEMOBURL; ?>product/getneftpayment" class="list-group-item text-left">
                                    Pay by NEFT/RTGS
                                    </a>
                                    <a href="<?php echo SITEMOBURL;?>/product/getchequepayment" class="list-group-item text-left">
                                    Pay by Cheque/Demand Draft
                                    </a>
									<?//print_r($_SESSION);
									//if($_SESSION['total_amount'] > MIN_PRICE_FOR_EMI_5_BANK){?>
                                    <!--<a href="<?php echo SITEMOBURL;?>/product/hdfc_payment" class="list-group-item text-left">
                                    Pay in Installment (HDFC Bank)
                                    </a>-->
									<?//}?>
                                    <a href="<?php echo SITEMOBURL;?>/product/distributepg" class="list-group-item text-left">
                                    Pay by EMI
                                    </a>
									<a href="<?php echo SITEMOBURL; ?>product/distributepg" class="list-group-item text-left">
                                    Pay by Wallet
                                    </a>
									<a href="<?php echo SITEMOBURL; ?>product/distributepg" class="list-group-item text-left">
                                    Pay by UPI 
                                    </a>
                                 </div>
                              </div>
                              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 bhoechie-tab">
                                 <!-- flight section -->
                                 <div class="bhoechie-tab-content active">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay using Credit Card </p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          By clicking on the "Pay by Credit Card " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                          <br><br>
                                          <strong style="font-size: 12px;">Note: After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</strong>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;">
                                       Pay by Credit Card
                                       </button></a>
                                    </div>
                                 </div>
                                 <!-- train section -->
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay using Debit Card</p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          By clicking on the "Pay by Debit Card " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                          <br><br>
                                          <strong style="font-size: 12px;">Note: After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</strong>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/distributepg" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important;">
                                       Pay by Debit Card
                                       </button></a>
                                    </div>
                                 </div>
                                 <!-- hotel search -->
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay using Net Banking </p>
                                       <ul>
                                          <li style="padding-bottom:10px; font-size: 14px;font-weight:600;color: #2f2e2e;  margin-left:24px;  list-style-type: disc;">Select from more than 25 banks.</li>
                                       </ul>
                                       <p class="m1" style="font-weight:normal;margin:0;font-size: 12px">
                                          By clicking on the "Proceed to Payment" button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                          <br><br>
                                          After clicking on the "Proceed to payment" button, you will be directed to a secure gateway for payment (Techprocess). After completing the payment process, you will be redirected back to Furtadosonline.com
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/distributepg" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-top: 14px;color: #ffffff ! important;">
                                       Proceed to Payment
                                       </button></a>
                                    </div>
                                 </div>
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">NEFT / RTGS </p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          The NEFT/ RTGS should be in favor of “Furtados Music India Pvt Ltd.”  
                                          <br>
                                          Beneficiary A/c no:05922560004715<br>
                                          Beneficiary Add : 201, Town Centre II, Andheri – Kurla Road, Marol, <br>
                                          Andheri (E), Mumbai – 400059<br>
                                          Beneficiary Bank: HDFC Bank<br>
                                          Beneficiary IFSC Code : HDFC0000143 <br>
                                          By clicking on the "Order Now" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/getneftpayment" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important">
                                       Order Now
                                       </button></a>
                                    </div>
                                 </div>
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay by Cheque / Demand Draft</p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          Cheque / DD for the complete value (including shipping cost) should be drawn in favour of "Furtados Music India Pvt Ltd." Cheque / DD should be payable at par in Mumbai. Outstation cheques will not be accepted. Cheque / DD should be posted / couriered to our office address as follows:  
                                          <br>
                                          <b>Furtados Music India Pvt Ltd,<br>
                                          201, Town Centre II, Andheri – Kurla Road, Marol, Andheri (E)<br>
                                          Mumbai – 400 059 <br>
                                          Tel:(022) 42875050 / 42875060 <br>
                                          Fax:(022) 42875012 <br></b>
                                          By clicking on the "Order Now" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/getchequepayment" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important">
                                       Order Now
                                       </button></a>
                                    </div>
                                 </div>
                                 <!--<?php if($_SESSION['total_amount'] > MIN_PRICE_FOR_EMI_5_BANK){ ?>
                                 <div class="bhoechie-tab-content">
            								 <form method="post" action="<?php echo SITEMOBURL; ?>product/hdfc_payment">
                                                <div class="middle">
                                                   <p class="title2" style="text-align: center;border-bottom:none;     font-weight: 600;">Pay in easy Installment <span style="font-size:12px;">(HDFC credit card holder only)</span> <img src="https://www.furtadosonline.com/images/hdfc_small.png" alt="HDFC" border="0"> </p>
                                                   <p class="m1" style="margin:30px 0 10px 0px;font-weight: 600;">
                                                    <input type="radio" name="paymentType" id="paymentType" value="6" onclick="show_3_month_Emi();">   &nbsp; 3 Months EMI (<i class="fa fa-inr"></i><span id="amount_3_amount">
                                                     </span>  per month)<br/></p>
                                                     <div id="3_month_Emi" style="display:none;">
                                                       <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                                                     <b>Tenure </b>: 3 Months
                                                   </li>
                                                    <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                                                     <b> Easy EMI Finance Charge </b> : 14% P/a 
                                                   </li>
                                                   <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;" >
                                                   <b>EMI Payable Every Month </b>: <i class="fa fa-inr"></i> <span id="amount_3_amount1">
                                                     </span> 
                                                   </li>
                                                   <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;" >
                                                   <b>Total Amount Payable over tenure (with interest) : <i class="fa fa-inr"></i></b> 
                                                   <span id="amount_3_total"></span></li>
                                                  </div>
                                                    <p class="m1" style="margin:0px 0 10px 0;font-weight: 600;">
                                                     <input type="radio" name="paymentType" id="paymentType" value="7" onclick="show_6_month_Emi();">
                                                     &nbsp; 6 Months EMI (<i class="fa fa-inr"></i> <span id="amount_6_amount1">
                                                     </span>   per month)
                                                    </p>
                                                    <div id="6_month_Emi" style="display:none;">
                                                       <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                                                     <b>Tenure </b>:6 Months
                                                   </li>
                                                       
                                                    <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                                                     <b>Easy EMI Finance Charge </b>: 14% P/a 
                                                   </li>

                                                    <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                                                     <b>EMI Payable Every Month : <i class="fa fa-inr"></i></b> 
                                                     <span id="amount_6_amount"></span>
                                                   </li>
                                        
                                                    <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;padding-bottom: 10px">
                                                     <b>Total Amount Payable over tenure (with interest) : <i class="fa fa-inr"></i></b> 
                                                     <span id="amount_6_total"></span>
                                                   </li>
                                                   </div>
                                                    <p class="m1" style="margin:0px 0 10px 0;font-weight: 600;">
                                                       <input type="radio" name="paymentType" id="paymentType" value="16" onclick="paydirectly(this);">
                                                          &nbsp; Pay directly (<i class="fa fa-inr"></i>  <span id="total_amount">
                                                     </span>)
                                                     </p>
                                                     <p class="m1" style="font-weight:normal;font-size: 12px">By clicking on the "Pay by credit card / Debit card" button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a></p>
                                                     <p class="m1" style="font-weight:normal;font-size: 12px">Note: After clicking on "Pay" button you will be redirected to our HDFC payment gateway.  After completing the payment process, you will be redirected back to   Furtadosonline.com</p>
                                                      <button type="submit" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important;">
                                                   Proceed to Payment
                                                   </button></a>
                                                </div>
            									</form>
                                 </div>
                              <?php } ?>-->
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay by EMI</p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          
                                       </p>
                                       <!--<a href="<?php echo SITEMOBURL; ?>product/distributepg" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important">
                                       Pay By EMI
                                       </button></a>-->
                                       <form action="distributepg" method="POST">
                                          <div class="form-group">
                                             <label for="emibanks">Select Bank</label>
                                             <select class="form-control" id="emibanks" name="emibanks">
                                                <option value="">Select Bank</option>
                                                <?php foreach($emibanks as $val){ ?>
                                                   <option value="<?php echo $val['eb_id']?>"><?php echo $val['eb_name']; ?></option>
                                                <?php } ?>
                                             </select>
											 <p class="m1" style="font-weight:normal; font-size: 12px"><br>
											  By clicking on the "Pay by EMI" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
											  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
											  <br><br>
											  <strong style="font-size: 12px;">Note: After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</strong>
										   </p>
                                          </div>
                                          <div class="form-group">
                                             <button type="submit" name="submit" class="btn btn-warning pd delhe">PAY BY EMI</button>
                                          </div>
                                       </form>
                                    </div>
                                 </div>
								<!-- train section 2-->
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay using Wallet</p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          By clicking on the "Pay by Wallet" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                          <br><br>
                                          <strong style="font-size: 12px;">Note: After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</strong>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/distributepg" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important;">
                                       Pay by Wallet
                                       </button></a>
                                    </div>
                                 </div>
								 <!-- train section 2-->
                                 <div class="bhoechie-tab-content">
                                    <div class="middle" style="min-height:200px;">
                                       <p class="title" style="font-weight: bold;">Pay using UPI</p>
                                       <p class="m1" style="font-weight:normal; font-size: 12px">
                                          By clicking on the "Pay by UPI " button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                                          <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
                                          <br><br>
                                          <strong style="font-size: 12px;">Note: After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</strong>
                                       </p>
                                       <a href="<?php echo SITEMOBURL; ?>product/distributepg" <button type="button" class="btn btn-warning pd delhe" style="width: 32%;border:none;margin-bottom: 14px;color: #ffffff ! important;">
                                       Pay by UPI
                                       </button></a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .error {
   color: #cc0028;
   font-weight: 600;
   }
   /*  bhoechie tab */
   div.bhoechie-tab-container{
   z-index: 10;
   background-color: #ffffff;
   padding: 0 !important;
   border-radius: 4px;
   -moz-border-radius: 4px;
   border:1px solid #ddd;
   margin-top: 20px;
   margin-left: 15px;
   -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
   box-shadow: 0 0px 4px rgba(0,0,0,.175);
   -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
   background-clip: padding-box;
   opacity: 0.97;
   filter: alpha(opacity=97);
   }
   div.bhoechie-tab-menu{
   padding-right: 0;
   padding-left: 0;
   padding-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group{
   margin-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group>a{
   margin-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group>a .glyphicon,
   div.bhoechie-tab-menu div.list-group>a .fa {
   color: #5A55A3;
   }
   div.bhoechie-tab-menu div.list-group>a:first-child{
   border-top-right-radius: 0;
   -moz-border-top-right-radius: 0;
   }
   div.bhoechie-tab-menu div.list-group>a:last-child{
   border-bottom-right-radius: 0;
   -moz-border-bottom-right-radius: 0;
   }
   div.bhoechie-tab-menu div.list-group>a.active,
   div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
   div.bhoechie-tab-menu div.list-group>a.active .fa{
   background-color: #cc0028;
   border-color: #cc0028;
   color: #ffffff ! important;
   }
   div.bhoechie-tab-menu div.list-group>a.active:after{
   content: '';
   position: absolute;
   left: 100%;
   top: 50%;
   margin-top: -13px;
   border-left: 0;
   border-bottom: 13px solid transparent;
   border-top: 13px solid transparent;
   border-left: 10px solid #cc0028;
   }
   div.bhoechie-tab-content{
   background-color: #ffffff;
   /* border: 1px solid #eeeeee; */
   padding-left: 20px;
   padding-top: 10px;
   }
   div.bhoechie-tab div.bhoechie-tab-content:not(.active){
   display: none;
   }
</style>
<script type="text/javascript">
   function edit(){
      $('#delivery').hide();
      $('#editadd').show();
      jQuery('#userdtlform').show();
   }
</script>
<script>
   function checklogin(){
    
     jQuery.ajax({
         url:"<?php echo SITEMOBURL?>user/checklogin",
         type:'POST',
         success:function(result)
         {
      var response = jQuery.parseJSON(result);
      
           if(response.error==false){
      
    jQuery('#frstlogincheck').hide();
    shippingaddress();
    jQuery('#collapsetwo').collapse('in');
     }else{
      jQuery('#frstlogincheck').show();
     }
     
         }
   
     });
     return false;
    
    
   }
   
   function shippingaddress(){
    jQuery.ajax({
         url:"<?php echo SITEMOBURL?>product/checkoutuserdtl",
         type:'POST',
         success:function(result)
         {  
    
    jQuery('#collapsetwo').html(result);  
     
         }
   
   });
   return false;
     
    
   }
   
   function addformshow(){
   jQuery('#editadd').css('background', 'rgb(236, 229, 231)');
   jQuery('#editadd').show();
   //jQuery('body').scrollTo('#editadd');
   //jQuery('#alllistaddress').hide();
   window.location.hash = '#radio3';

   jQuery('#userdtlform').show();
   } 
   
   function palcefinalorder(module=false){
   window.scrollBy(0, 60);
   jQuery('#userdtlform').hide();
   jQuery('#clickonordersummary').click();
   jQuery('#shippingaddressdiv').click();
   jQuery('#clickonordersummary').removeClass('disabled-checkout');
   var addressid=jQuery("input[type=radio]:checked").data('aid');
   jQuery.ajax({
         url:"<?php echo SITEMOBURL; ?>product/placeFinalOrder",
         type:'POST',
		 data:{addressid:addressid},
         success:function(result)
         {
			var response = jQuery.parseJSON(result);
            if(response.error==false){
                jQuery('#amount_3_amount').html(response.emi_amount_3_amount);
                 jQuery('#amount_3_total').html(response.emi_amount_3_total);
         		 jQuery('#amount_3_amount1').html(response.emi_amount_3_amount);
                 jQuery('#amount_6_amount1').html(response.emi_amount_6_amount);
				 jQuery('#amount_6_amount').html(response.emi_amount_6_amount);
                 jQuery('#amount_6_total').html(response.emi_amount_6_total);
                jQuery('#total_amount').html(response.total_amount);
               getfinalcart();
               return false;
			}
		  else{
			   bootbox.alert(response.msg);
				jQuery('#clickonordersummary').removeClass('collapsed');
				jQuery('#collapseThree').removeClass('in');
				jQuery('#shippingaddressdiv').click();
				jQuery('#shippingaddressdiv').addClass('collapsed');
				jQuery('#collapsetwo').addClass('in');
				jQuery('#clickonordersummary').addClass('disabled-checkout');
			}
		  if(module=='coupon'){
			 /*jQuery('#shippingaddressdiv').removeClass('collapsed');
			 jQuery('#collapsetwo').removeClass('in');
			 jQuery('#clickonordersummary').addClass('collapsed');
			 jQuery('#collapseThree').addClass('in');*/
			 jQuery('#collapsetwo').toggle();
			 //jQuery('#collapseThree').toggle();
			 setTimeout(function(){
				//jQueery('#clickonordersummary').trigger('click');
				jQuery('#clickonordersummary').addClass('collapsed');
				jQuery('#collapseThree').addClass('in');
				jQuery('#collapseThree').attr('style', '');
			 }, 1000)
		  }
		  // window.location.href='<?php echo base_url(); ?>product/webcheckout';
		}
   
   });
   return false;
   }

   
   function getfinalcart(){
   console.log('here');
   jQuery.ajax({
      url:"<?php echo SITEMOBURL?>product/finalcart",
      type:'POST',
      success:function(result){
         jQuery('#finalcart').html(result);  
      }
   });
      return false;
   }
   
   jQuery(document).ready(function(){
    checklogin();
   });
   
</script>
<script type="text/javascript">
   jQuery('.Change_Address').click(function(){
      jQuery('#addr').prop('disabled',false);
      jQuery('#landmark').prop('disabled',false);
      jQuery('#mobnumber').prop('disabled',false);
      jQuery('#pincode').prop('disabled',false);
   
   });
</script>
<script type="text/javascript">
   function getcity(){
     sid=jQuery('#state1 option:selected').val();
     var str="";
     
     jQuery.ajax({
       type:'POST',
       url:'<?php echo SITEMOBURL?>user/getcities',
       data:{sid:sid},
       success:function(res){
         result=jQuery.parseJSON(res);
       if(result.error!=true){
         if(result.msg.length > 0)
         {
         for (var i =0; i < (result.msg).length; i++) {
             str+="<option value="+result.msg[i]['id']+">"+result.msg[i]['cityName']+"</option>";
           }
           
          }}else{
           str+="<option value='0'>Other City</option>"; 
         }
         jQuery('#citylist').html(str); 
       },
       error:function(){
         bootbox.alert('Something went wrong');
       }
     });
     return false;
   }
   
   
   function movetonexttab(){
   window.scrollBy(0, 100);
	   jQuery('#collapsefourfour').removeClass('disabled-checkout');
       jQuery('#collapsefourfour').click();
       jQuery('#clickonordersummary').click();
   }
</script>
<script type="text/javascript">
   $(document).ready(function() {
     $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
         e.preventDefault();
         $(this).siblings('a.active').removeClass("active");
         $(this).addClass("active");
         var index = $(this).index();
         $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
         $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
     });
   });
</script>
<script type="text/javascript" language="javascript">
   function show_3_month_Emi(){
     document.getElementById("3_month_Emi").style.display='block';
     document.getElementById("6_month_Emi").style.display='none';
   
     document.getElementById("idpaymentType").value=6; 
   }

   function show_6_month_Emi(){
      document.getElementById("6_month_Emi").style.display='block';
    document.getElementById("3_month_Emi").style.display='none';
     
    document.getElementById("idpaymentType").value=7; 
   }
   
   function paydirectly(){
	    document.getElementById("6_month_Emi").style.display='none';
    document.getElementById("3_month_Emi").style.display='none';
	   
   }
</script>