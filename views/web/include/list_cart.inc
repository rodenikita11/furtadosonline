
<div class="deatilsbg">
   <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
      <div class="product-item-holder size-big single-product-gallery small-gallery">
         <div class="single-product-gallery-item" id="slide1">
           <? $r1=array(',',' ','/','&');
               $r2=array('-','-','','');
               $url=ADMINURL.str_replace($r1, $r2, $val['proname']).'/'.$val['proid'];?> 
            <a href="<?php echo $url;?>" data-lightbox="image-1" data-title="Gallery">
            <img class="img-responsive" alt="" src="<?php if(!isset($val['image'])){ echo SITEIMAGES.'mobimg/noimage1.png';}else{echo $val['image'];}?>"/>
            
            </a>
         </div>
         <!-- /.single-product-gallery-item -->
      </div>
   </div>
   <div class='col-sm-6 col-md-9 product-info-block'>
      <div class="product-info">
         <h1 class="name nw"><a href="<?php echo $url;?>"><?php echo $val['proname'];?></a></h1>
         <div class="rating-reviews">
            <div class="row">
               <div class="col-sm-12">
                  <!-- <div class="reviews">
                     <a href="#" class="lnk">Item No.130034</a>
                  </div> -->
               </div>
            </div>
            <!-- /.row -->    
         </div>
         <!-- /.rating-reviews -->
         <div class="price-container info-container m-t-20">
            <div class="row">
               <div class="col-sm-6">
                  <div class="price-box m-l-33">
                        <span class="price m-r-12 t-r88"><i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat($val['dicountprice']);?></span>
                     <?php if($val['dicountprice'] < $val['price']){ ?>
                        <span class="price-strike m-r-12 trr"><i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat($val['price']); ?></span>
                     <? } ?>

                     <?php if($val[isAvailable]=='0'){ ?>
                      <span class="value1 red tre">out of Stock</span>
                       <?php }else{ ?>
                      <span class="value1 tre"> In Stock</span>
                       <? } ?>
                  </div>
               </div>
            </div>
            <!-- /.row -->
         </div>
         <!-- /.price-container -->
      </div>
      <!-- /.product-info -->
   </div>
   <!-- /.col-sm-7 -->
   <div class="pricefilter m-l-58">
      <div class="col-sm-2" style="padding-left: 0px;">
        <select id="qtychange_<?echo$key;?>" name="qtychange" class="form-control ft qtycart"  value="<?echo $val['qty']?>"  style="width:100%;" onchange="updatecart(<?echo $key;?>);" >
        <? for ($i=1;$i<=15;$i++){
      		 if($val['qty']== $i ){
      		  $selected="selected";
      		 }else{
      		    $selected="";
		    }?>
		 <option value="<?echo $i?>" <?echo $selected;?>><?echo $i?></option> 
		 <?}?>
        </select>
      </div>
      <div class="col-sm-3" style="padding-left: 0px;">
         <button type="button" class="btn btn-Move" onclick="addtowishlist(<?echo $key;?> , this);" data-addtowishlist="true">Move to Wishlist</button>
      </div>
      <div class="col-md-2" style="padding-left: 5px;">
         <div class="btn-group">
            <a href="javascript:void(0);" class="btn btn-inverse delet" onclick="removefromcart(<?echo $key;?>);"><i class="glyphicon glyphicon-trash"></i></a>
         </div>
      </div>
   </div>

   
   <!-- Combo -->
    <div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
      <?php if(is_array($val['combo_product']) && count($val['combo_product'] > 0)){ ?>
      <h3 class="comt">Combo Product</h3>

  		<?php foreach ($val['combo_product'] as $combokey => $comboval) { 
             include('cart_combolist.php'); 
         }
     
      } 
      ?>
    </div>
    <!-- Combo -->
</div>