<div class="top-logo">
<div class="container" style="padding:0;">
<div class="row">
<div class="col-md-12">

<div class="col-xs-2">
<a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>

</div>
<div class="col-xs-7 Favourite_list">Feedback</div>


	</div>
</div>
</div>
</div>

<div class="container" style="margin: 30px 12px 0px 12px;">
  
  <form class="form-horizontal" name="feedbackform" id="feedbackform">
    <div class="form-group">
      <label class="control-label col-xs-2 col-sm-2" for="email">Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="username" placeholder="Enter Name" name="username" onkeypress="return isNumber(event)">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Email Id:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="emailid" placeholder="Enter Email Id" name="emailid">
      </div>
    </div>
    <div class="form-group">
  <label class="control-label col-sm-2" for="pwd">Comment:</label>
  <div class="col-sm-10"> 
  <textarea class="form-control" rows="5" id="comment" name="comment" laceholder="Enter Comment"></textarea>
  </div>
</div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="button" name="submit" id="submit" class="btn btn-default saveaddr">Submit</button>
      </div>
    </div>
  </form>
</div>
<?php include('include/footer.php');?>

<script type="text/javascript">
 jQuery(document).ready( function(){

    var val=jQuery('#feedbackform').validate({

        rules:{
          username:{
            required:true
          },
          emailid:{
              email:true,
              required:true
          },
          comment:{
            required:true
          }
        },
        messages:{
          username:{
            required:"Please enter your Name"
          },
          emailid:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            },
          comment:{
             required:"Please enter your Comment"
          }
        }
    });

    jQuery('#submit').click(function() {
        var success=jQuery("#feedbackform").valid();
        if(success){
          
          var username = jQuery('#username').val();
          var emailid = jQuery('#emailid').val();
          var comment = jQuery('#comment').val();
        
            jQuery.ajax({
                url:'<?php echo SITEMOBURL;?>user/savefeedback',
                type: "POST",
                data:{username:username,emailid:emailid,comment:comment},
                success:function(result){
                    res=jQuery.parseJSON(result);
                    
                    if(res.status == true){
                        alert('Thank you for your feedback.');
                        jQuery('#username').val(' ');
                        jQuery('#emailid').val(' ');
                        jQuery('#comment').val(' ');
                   }else if(res.status == false){
                     alert('Some error occurred');
                     jQuery('#username').val(' ');
                     jQuery('#emailid').val(' ');
                     jQuery('#comment').val(' ');
                     
                    }
                }
            });
            return false;
        }
    });
});
</script>
<script type="text/javascript">
   function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 96 && charCode < 123) || (charCode > 64 && charCode < 91)) {
        return true;
    }
    return false;
}
</script>
