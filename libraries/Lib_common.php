<?
class Lib_common{
    public $ci;
    public function __construct() {
        $CI = & get_instance();
        $CI->load->library('libsession');
        $CI->load->library('libapi');
        $this->ci = $CI;
    }

    public function printr($data) {
        echo "<pre>";
           print_r($data);
        echo "</pre>";
     }

     public function getProductESresult($param, $paramx){
         $url = $param['url'];
         $base_url = $param['base_url'];
        if( isset($paramx['branddtl']) || isset($paramx['available']) || isset($paramx['minprice']) ||
					isset($paramx['maxprice']) || isset($_SESSION['usrid']) || isset($paramx['catid']) || 
					isset($paramx['itemtype']) || isset($paramx['discount'])|| isset($paramx['sort'])
				){
					$url=$url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$paramx['minprice'].$paramx['maxprice'].$paramx['discount'].$paramx['sort'].$usrid;
					$base_url = $base_url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$usrid;
				}
            /* $this->printr($param);
            $this->printr($paramx);
			echo $url; exit; */
			$get_base_data = $this->ci->libapi->callAPI('GET', $base_url , false);
			$get_data = $this->ci->libapi->callAPI('GET', $url , false);
			
			$base_res = json_decode($get_base_data, true);

			$res = json_decode($get_data, true);
            $errors = $res['res']['errors'];
            
            return array('res' => $res, 'base_res' => $base_res);
    }
}