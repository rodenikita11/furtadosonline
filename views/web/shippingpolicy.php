<style>
   p {
   font-size: 14px;
   text-align: justify;
   }
</style>
<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px;">
         <div class="col-lg-12 outer-top-150">
           <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;text-align: center;">Shipping Policy</h2>
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;"> Shipping Charges-Taxes and Title Policy</h2>
            <p> • Separate charges for shipping and handling will be shown on your order form, if applicable.</p>
            <p>• Orders placed on Saturdays, Sundays, or holidays will be processed on the next business day.
 </p>
            </p>
            <p>• As a security precaution, initial orders and orders shipping to alternate addresses may be held for extended verification.</p>
            <p>• We reserve the right to make partial shipments, which will not relieve you of your obligation to pay for the remaining deliveries. </p>
            <p>• All items purchased from Furtadosonline are made pursuant to a shipment contract. This means that the risk of loss and title for such items passes to you upon our delivery to the carrier. </p>
            <p>• For more information on our shipping policies, including rates, delivery times, and delays, please call us at <a href="tel:02242875050">+91 22 42875050/ 60.</a> </p>
         </div>
       
         <div class="col-lg-12 outer-top-xs">
           <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Billing & Payments</h2>
            <p>• All items supplied will be accompanied by an official GST invoice.</p>
            <p> •  If payment for your order has been made in advance the order will be dispatched after receipt of the same. </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
           <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Delivery Mode</h2>
            <p>Generally all orders are processed and shipped within 5-6 working days, once payment is confirmed and the product is in stock. However, certain categories of products have different lead-times for delivery due to the nature of the product. If the delivery of a product is delayed for an extended period of time the same will be informed to the user. Kindly note, due to certain regulatory requirements or carrier availability it may not be possible to supply products to certain areas and locations. We will advise you if the area is not serviceable after you have placed your order. Transit times vary depending on your location and mode of dispatch opted for.  
            </p>
         </div>
      </div>
   </div>
</div>
