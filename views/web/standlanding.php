<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stands for Instruments and Pro-Audio</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo WEBCSS;?>standlandingcss/style.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MGRL7HV');</script>
	<!-- End Google Tag Manager -->
	
	<!-- Global site tag (gtag.js) - Google Ads: 863719530 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-863719530"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'AW-863719530');
	</script>
</head>

<body>
    <div class="main">
        <div class="container-fluid header-red">
            <div class="stands">
                <div class="stands-txt"><span class="stands-1">Music</span><span class="center">Stands</span><span class="stands-2">For You</span></div>
                <h4 class="mt-3">REDUCE YOUR TOTAL COST OF OWNERSHIP</h4>
                <h4>worry free investment FOR MANY MANY YEARS!</h4>
            </div>
        </div>
        <div class="container">
            <div class="row py-lg-5 pt-3 pt-lg-0">
                <div class="col-12 col-md-6 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/konig-meyer/147" target="_blank">
                        <div class="brand-name">
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/konic1.png" alt="" class="brand-img">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/hercules/116" target="_blank">
                        <div class="brand-name">
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/brand2.png" alt="" class="brand-img">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row pb-lg-5 pb-3 pb-lg-0">
                <div class="col-12 col-md-6 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/nomad/187" target="_blank">
                        <div class="brand-name">
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/brand3.png" alt="" class="brand-img">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/magnum/164" target="_blank">
                        <div class="brand-name">
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/brand4.png" alt="" class="brand-img">
                        </div>
                    </a>
                </div>
            </div>
            <!-- Instruments -->
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <!--"https://www.furtadosonline.com/product/search/?maincat=1&searchText=konig%20and%20meyer,%20hercules,%20nomad,%20Magnum&level=3&catid=1100&catname=KEYBOARD%20ACCESSORIES"-->
					<a href="https://www.furtadosonline.com/keyboard-stands/10097?filter=&brand=[164,116,147,187]&sort=popular&searchText=" target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">keyboards stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/key.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                   <!--"https://www.furtadosonline.com/product/search?maincat=1&searchText=konig+and+meyer%2C+hercules%2C+nomad%2C+Magnum&level=3&catid=1082&catname=GUITAR+STANDS"-->
				   <a href="https://www.furtadosonline.com/guitar-stands/1082?filter=&brand=[164,116,147,187]&sort=popular&searchText=" target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">guitars stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/guitar.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
        <!--"https://www.furtadosonline.com/product/search/?maincat=1&searchText=konig%20and%20meyer,%20hercules,%20nomad,%20Magnum&level=3&catid=1075&catname=NOTATION%20STANDS"-->
				   <a href="https://www.furtadosonline.com/notation-stands/1075?filter=&brand=[164,116,147,187]&sort=popular&searchText=" target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">notation stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/notation.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/speaker-stands-mounts-rigs/10173?filter=&pricestart=15330&pricend=20493&avaliable=[1]&sort=popular&searchText="
                        target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">speaker stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/speaker.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row pt-lg-5">
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/product/search?filter=&avaliable=%5B1%5D&sort=relevence&searchText=mic+stand+konig+and+meyer%2C+hercules%2C+nomad%2C+Magnum&maincat=3"
                        target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">mic stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/misc.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/product/search/?maincat=1&searchText=konig%20and%20meyer,%20hercules,%20nomad,%20Magnum&level=3&catid=1127&catname=WOODWIND%20ACCESSORIES"
                        target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">wind instruments stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/wind.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/product/search/?maincat=1&searchText=konig%20and%20meyer,%20hercules,%20nomad,%20Magnum&level=3&catid=1118&catname=VIOLIN%20ACCESSORIES"
                        target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">Bowed strings stands</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/bowed.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-3 py-3 py-lg-0">
                    <a href="https://www.furtadosonline.com/product/search?maincat=0&searchText=205566%2C+209784%2C+209786%2C+211539%2C+212357%2C+137153&submit="
                        target="_blank">
                        <div class="instrument">
                            <div class="heading">
                                <h4 class="text-center text-uppercase mb-0">others</h4>
                            </div>
                            <img src="<?php echo SITEIMAGES; ?>standlandingimgs/others.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
		</div>
            <!-- End Instruments -->
            <!-- Yt -->
		<div class="terms py-2 py-lg-5">
			<h2 class="terms-heading text-center">
				Explore Product Videos
			</h2>
			<div class="container">
				<div class="row yt-vid pt-lg-5">
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/8eYyQZTviBw" frameborder="0"
							allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/product/search?filter=&brand=[12984]&avaliable=[1]&sort=relevence&searchText=konig+and+meyer+mic+stand&maincat=0  "> König & Meyer Tripod Mic Stands</a></center><h6>
					</div>
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/4upm40ngGdA?start=111"
							frameborder="0" allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/keyboard-stands/10097?filter=&brand=[12984]&avaliable=[1]&sort=popular&searchText=">König & Meyer Keyboard Stands</a></center><h6>
					</div>
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/WOQ54LflNZ8" frameborder="0"
							allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/guitar-stands/1082?filter=&brand=[12984]&avaliable=[1]&sort=popular&searchText= ">König & Meyer Guitar Stands</a></center><h6>
					</div>
				</div>
				<div class="row yt-vid pt-lg-5">
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/SQoOwcE5zHM?start=2"
							frameborder="0" allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/hercules-guitar-stand-gs432b-plus-black/215829">Hercules GS432B plus now in India!</a></center><h6>
					</div>
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/YsUkiWmdZ4k" frameborder="0"
							allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/hercules-smart-phone-holder-dg200b/209784">Hercules Multimedia Stand DG200B</a></center><h6>
					</div>
					<div class="col-12 col-md-12 col-lg-4 py-3 py-lg-0">
						<iframe width="420" height="315" src="https://www.youtube.com/embed/Ug442gicy08?start=8"
							frameborder="0" allowfullscreen>
						</iframe>
						<h6><center><a href="https://www.furtadosonline.com/hercules-keyboard-stand-ez-step-ks100b-210816/210816">Hercules - Keyboard Stand, EZ Step KS100B</a></center><h6>
					</div>  
				</div>
			</div>
	    </div>
        
    </div>
    <div id="terms&conditions" class="terms py-2 ">
            <h2 class="terms-heading text-center">
                TERMS & CONDITIONS of Warranty on Stands
            </h2>
            <div class="container">
                <div class="terms-txt pt-lg-5 pt-3">
                    <p class="font-weight-bold">Warranty is hereby extended for the following brands of stands, for the stated
                        duration:</p>
                    <p class="font-weight-bold py-lg-4 py-2">
                        Konig & Meyer - 5 years<br>
                        Hercules - 3 years<br>
                        Nomad - 2 years<br>
                        Magnum - 2 years<br>
                    </p>
                    <p>If at any time the stand malfunctions as a result of faulty materials or workmanship, Furtados
                        will repair, or arrange for repair of, the defect(s) as it deems appropriate at its sole
                        discretion. Furtados reserves the right to use materials regularly utilised at the time of
                        repair in the event that original materials are no longer available.</p>
                    <p>
                        In the unlikely event that your stand is destroyed, lost or damaged beyond repair, while in the
                        possession of Furtados for repair, Furtados will replace that stand with one of the same or most
                        similar style of a value not in excess of the original purchase price of your stand. Any
                        insurance covering the stand must be carried by the owner at owner’s expense.
                    </p>
                    <p>If warranty work is provided by Furtados the date of expiry of warranty will remain the same as
                        the original stand purchased and will not be further extended from the date of warranty work.
                        All replaced parts and/or stands will not be covered separately from the original stand covered
                        under warranty.
                    </p>
                    <p>
                        This warranty is extended to the original retail purchaser only and may not be transferred or
                        assigned to subsequent owners. Your proof of purchase or sales bill/receipmust accompany all
                        requests for warranty coverage and is a pre-condition for warranty. </p>
					<p>This warranty is valid
                        within the territorial borders of the Republic of India only. This warranty shall cease to be
                        valid if this stand is removed from the Republic of India
                    </p>
                    <p class="font-weight-bold">This warranty is subject to the following limitations. THIS WARRANTY DOES NOT COVER:</p>
                    <ol>
                        <li>Any stand that has been altered or modified in any way.</li>
                        <li>Screws, springs, pads, foam, and other parts subject to normal wear and tear.</li>
                        <li>Lights, bulbs, batteries, and any electrical consumable.</li>
                        <li>Any stand that has been damaged due to misuse, negligence, abuse, accident, or improper
                            operation, aberrant environmental conditions, abnormal operating conditions, strain or
                            inadequate
                            maintenance or care.</li>
                        <li>Product defects which are caused by the use of accessories, components or replacement parts
                            which are not original components.</li>
                        <li>Shipping damages of any kind.</li>
                        <li>Any stand that has been subjected to extremes of humidity or temperature.</li>
                        <li>Any stand damaged by exposure to insects, pests or like organisms.</li>
                        <li>Physical damage, internal or external of any kind.</li>
                        <li>Normal wear and tear, rusting, discolouration, stains or scratches.</li>
                        <li> Non-conformity to standard operating procedures as detailed in the owner's instruction
                            manual.</li>
                        <li>Any stand that has been purchased from an unauthorised dealer, or upon which unauthorised
                            repair or service has been performed.</li>
                        <li>Damage resulting out of excessive force and/or repetitive non-standard actions.</li>
                    </ol>
                    <p>Furtados makes no other express warranty of any kind whatsoever. All implied warranties,
                        including
                        warranties of merchantability and fitness for a particular purpose, exceeding the specific
                        provisions of this warranty, are hereby disclaimed and excluded from this warranty.</p>
                    <p>Furtados shall not be liable for any special, indirect, consequential, incidental or other
                        similar damages suffered by the purchaser or any third party, including, without limitation,
                        damages for loss of profits or business or damages resulting from use or performance of the
                        stands, whether in contract or in tort, even if the dealer or its authorised representative has
                        been advised of the possibility of such damages, and Furtados shall not be liable for any
                        expenses, claims, or suits arising out of or relating to any of the foregoing. All disputes are
                        subject to the jurisdiction of the courts in Mumbai.
                    </p>
                    <p class="font-weight-bold">How to Obtain Warranty Service:</p>
                    <p>In the event of malfunction of your stand, you should notify your NEAREST FURTADOS SHOWROOM
                        OR ONLINE STORE from which you purchased the stand. The owner must make available the stand at
                        the showroom premises along with proof of purchase (sales bill/receipt). Furtados disclaim liability for defects or damage caused by services performed by unauthorised persons or non-warranty service not performed by an authorized service representative.</p>
                    <p>Do not ship the stand, in whole or part, to Furtados, without written authorisation from Furtados. In the event the stand is to be shipped to Furtados, the owner must ship the stand, freight and insurance prepaid, to the designated address of Furtados or make available the same at Furtados’ premises. The repaired stand or part will be returned to you at the place of collection or, if required, by freight collect, insured.</p>
                    <p>When contacting Furtados, you must include a complete written description of the malfunction of the stand. You are not required to purchase non-warranty work in order to obtain service on materials
                        covered by this warranty. Following its inspection of the stand upon its arrival, Furtados will advise you of the approximate date of completion. The repaired stand or part will be returned to you at the place of collection.</p>
					<p>No representative or other person is authorised to assume for Furtados any liability except as stated in this warranty.</p>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
//window.location.hash = '#tnc';
</script>
</html>