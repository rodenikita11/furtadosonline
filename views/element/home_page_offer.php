<?php 
	$offerDetail = $this->Do_offer->getoffer();
		if(!empty($offerDetail)) { 
?>
		<section>
            <div class="block color-scheme-2 section-bg1">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">EXCLUSIVE <span>OFFERS</span></h1>

                        <div class="toolbar-for-light" id="nav-bestseller">
                            <a href="javascript:;" data-role="prev" class="prev" id="pre-offer"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:;" data-role="next" class="next" id="nxt-offer"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div id="owll-bestseller">
						<?php foreach($offerDetail as $keyecho=>$order_details) { ?>
                        <div class="text-center item home-right-mr-sc">
                            <article class="product light" style="height:254px;">
                                <a href="<?php echo $order_details['offer_target_url'];?>"><img class="img-overlay img-responsive" src="<?php echo $this->lib_storeuri->getprothumbnail(FILEBASEURL.IMGOFFERPATH.$order_details['file_name'],263);?>" alt="" title=""></a>
                             </article>
							 <h1 class="col-name fS16" style="height:30px;"><?php echo CommonHelper::return_part_of_string($order_details['offer_title'],30); ?></h1>
							<h2 class="col-name fS20 color1 font-fa1"><?php echo CommonHelper::return_part_of_string($order_details['offer_short_desc'],25);?></h2>
                        </div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </section>
		<script type="text/javascript">
		$(document).ready(function() {
				$("#owll-bestseller").owlCarousel();
		});
		 $("#nxt-offer").click(function(){
				$("#owll-bestseller").trigger('owl.next');
			});
			$("#pre-offer").click(function(){
				$("#owll-bestseller").trigger('owl.prev');
			});
		</script>
		<?php } ?>