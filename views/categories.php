<?php include('includes/head.php'); ?>
			<div class="content_wrapper">				
				<!-- feature product hot deals -->
				<section class="inner_page_sec">
					<div class="container">
						<div class="row">	
							<!-- bread crum -->
							<div class="col-lg-12 col-md-12 col-sm-12 bredcrum">
								<ul>
									<li><a href="">Home /</a></li>
									<li><a href="">Guitars & Bass</a></li>									
								</ul>
								<div class="clear"></div>
							</div>
							<!-- end bread crum -->
							<!-- left side bar-->						
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<!-- guitar bas -->
								<div class="left_categories">
									<h3>Guitars & Bass</h3>
									<ul>
										<li><a href="#">Acoustic Guitar</a></li>
										<li><a href="#">Electric Guitars</a></li>
										<li><a href="#">Classical Guitars</a></li>
										<li><a href="#">Basses</a></li>
										<li><a href="#">Guitar Amps</a></li>
										<li><a href="#">Guitar Accessories</a></li>
										<li><a href="#">Guitar Cables</a></li>
										<li><a href="#">Guitar Stands</a></li>
										<li><a href="#">Guitar Picfk-Ups</a></li>
										<li><a href="#">Pedals & Processors</a></li>
										<li><a href="#">Banjos & Ukeleles</a></li>
									</ul>
								</div>
								<div class="left_categories">
									<h3>Browse By Brands</h3>
									<ul>
										<li><a href="#">Alhambra</a></li>
										<li><a href="#">Alice</a></li>
										<li><a href="#">AMT</a></li>
										<li><a href="#">Applause</a></li>
										<li><a href="#">Ashton</a></li>
										<li><a href="#">B C Rich</a></li>
										<li><a href="#">Belcat</a></li>
										<li><a href="#">Bespeco (1)</a></li>
										<li><a href="#">Blackstar (1)</a></li>
										<li><a href="#">Blade (1)</a></li>
									</ul>
								</div>
								<!-- end guitar bas -->
								<div class="hot_deals">
									<h3 class="co_heading">Hot Deals</h3>
									<div class="owl-carousel">
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>												
											</ul>
										</div>
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>												
											</ul>
										</div>										
									</div>
								</div>
							</aside>
							<!-- end left side bar-->
							<!-- right side bar-->
							<article class="col-lg-9 col-md-9 col-sm-9">
								<!-- inn banner -->
								<section class="in_banner_1">
									<div class="in_banner_text">
										<h2>Guitars & Bass</h2>
										<img src="<?php echo ASSETURL; ?>images/img_31.jpg" alt="">
									</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a tellus maximus, maximus magna quis, porttitor urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed pharetra blandit metus. Mauris tempor metus malesuada lectus vestibulum sagittis. </p>									
								</section>
								<!-- end inn banner -->
								<!-- feature pro list -->
								<section class="landing_feature">
									<h2 class="co_heading">Featured Products</h2>
									<div class="owl-carousel_2">
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>										
									</div>
									<div class="clear"></div>
								</section>
								<!-- end feature pro list -->
								<!-- shop bey -->
								<section class="discount_cate_list">
									<h3 class="co_heading">Shop by Type</h3>
									<div class="owl-carousel_1">
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Education</span>
														<a href="#"><img src="<?php echo ASSETURL; ?>images/img_18.jpg" alt=""></a>
												</li>												
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Musician Essentials </span>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_19.jpg" alt=""></a>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Computer Music</span>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_20.jpg" alt=""></a>
												</li>
											</ul>
										</div>										
									</div>
								</section>
								<!-- End shop bey -->
								<!-- new arrivals -->
								<section class="featur_pr landing_fea">
									<h3 class="co_heading">New Arrivals</h3>
									<ul class="pr_list">
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>												
											</div>
											<a href="#">
												<div class="pr_img">
													<img alt="" src="<?php echo ASSETURL; ?>images/img_1.jpg">
												</div>
												<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><strong>22</strong> Reviews</span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>												
												<span class="onsale">ONSale</span>												
											</div>
											<a href="#">
												<div class="pr_img"><img alt="" src="<?php echo ASSETURL; ?>images/img_2.jpg"></div>
												<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>												
												<span class="free">free</span>												
											</div>
											<a href="#">
												<div class="pr_img"><img alt="" src="<?php echo ASSETURL; ?>images/img_3.jpg"></div>
												<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li class="last_child">
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>																								
											</div>
											<a href="#">
												<div class="pr_img"><img alt="" src="<?php echo ASSETURL; ?>images/img_4.jpg"></div>
												<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
									</ul>
									<div class="clear"></div>
								</section>
							<!-- end new arrivals -->
							<!-- Top Brands -->
								<section class="discount_cate_list Top_Brands">
									<h3 class="co_heading">Top Brands</h3>
									<div class="owl-carousel_3">
										<div class="item">
											<ul>
												<li>													
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_32.jpg" alt=""></a>
												</li>												
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>													
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_33.jpg" alt=""></a>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_34.jpg" alt=""></a>
												</li>
											</ul>
										</div>	
										<div class="item">
											<ul>
												<li>													
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_35.jpg" alt=""></a>
												</li>
											</ul>
										</div>										
									</div>
								</section>
								<!-- End Top Brands -->
								<!-- feature pro list -->
								<section class="landing_feature">
									<h2 class="co_heading">Featured Products</h2>
									<div class="owl-carousel_2">
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>												
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
											</ul>
										</div>										
									</div>
									<div class="clear"></div>
								</section>
								<!-- end feature pro list -->
							</article>
							<!--end  right side bar-->
						</div>
						<div class="clear"></div>
					</div>
				</section>
				<!-- end feature product hot deals -->	
						
			</div>
			<?php include('includes/footer.php'); ?>