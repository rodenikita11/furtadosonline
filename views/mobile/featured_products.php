<?php //echo '<pre>'; print_r($hotdeals); die;
if(is_array($hotdeals)){
	$resp['product']=$hotdeals['product'];
	$name = 'Hot Deals';
} else if(is_array($newarrivalbooks)){
	$resp['product']=$newarrivalbooks['product'];
	$name = 'Newly launched books';
} else if(is_array($newarrivals)){
	$resp['product']=$newarrivals['product'];
	$name = 'New Arrivals';
}else {
	$resp['product']=$resp['product'];
	$name = 'Featured Products';
}

if($_GET['brand']){
	$param['branddtl']=explode(',',$_GET['brand']);
	$param['branddtl']=str_replace( array('[',']') , ''  , $param['branddtl']);
}
if($_GET['avaliable']){
	$param['avaliable']=explode(',',$_GET['avaliable']);
	$param['avaliable']=str_replace( array('[',']') , ''  , $param['avaliable']);
}
?>
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
<div class="top-header-padding">
   <div class="row">
      <div class="col-xs-12">
         <div class="boxsort ">
            <div class="col-xs-6 short-product" data-toggle="modal" data-target="#myModalsort"><img src="<?echo SITEIMAGES;?>mobimg/icon/short.jpg" height="24px">Sort</div>
            <div class="col-xs-6 short-product"><a href="#" data-toggle="modal" data-target="#myModal"><img src="<?echo SITEIMAGES;?>mobimg/icon/filter.jpg" height="24px">Filters</a>
             <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTQiIGhlaWdodD0iMTQiIHZpZXdCb3g9IjAgMCAxNCAxNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPjxwYXRoIGQ9Ik03IDBhNyA3IDAgMSAxIDAgMTRBNyA3IDAgMCAxIDcgMCIgZmlsbD0iIzQxYjI4MyIvPjxwYXRoIGQ9Ik01LjQ5NiA5LjA2bDQuNTU4LTQuMDU2LjU4Mi41MTctNS4xNCA0LjU3M0wzIDcuODY5bC41ODItLjUxN0w1LjQ5NyA5LjA2IiBzdHJva2U9IiNmZmYiIHN0cm9rZS13aWR0aD0iLjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgZmlsbD0iI2ZmZiIvPjwvZz48L3N2Zz4=" class="flt"></div>
         </div>
      </div>
   </div>
</div> 
<div class="Container container-fluid bg-1 text-center search-res">
	<div class="col-xs-12 Drums_and_bass_top">
		<div class="col-xs-7 Drums_and_bass">
			<p class="view"><?php echo $name;?></p>
		</div>
		<!--<div class="col-xs-5">
			<?if($resp['product']){?>
				<p class="item"><?//echo count($resp['product']);?> items</p>
			<?}?>
		</div>-->
	</div>
</div>
<div class="Electric  bg-1 text-center">
	<div class="row">
		<div class="col-xs-12 flex-container" id="listdata">
			<?php if($resp['product']){
				foreach ($resp['product'] as $key=>$value){ 
					$value['onlinePrice']=$value['price'];
					$value['discountprice']=($value['discountPrice']) ? $value['discountPrice'] : $value['discountprice'];
					include('include/list_product.inc');
				}
			?>
				<div class="listingtab productpagination">
					<span><?php echo $resp['links']; ?></span>
				</div>
			<?php } else { ?>
			<div class="">No Products Found...</div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- -------------------------new filter modals------------------------------ -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content overflow">
      <?php if($filter){ ?>
      <div class="side-menu animate-dropdown outer-bottom-xs" id="subcate" >
        <?php 
            $i=99;
            foreach($filter as $key=>$val){ ?>
        <div class="head" style="padding: 9px 0px 5px 10px;background: #cd0029;color: #ffffff;margin: 0px 0px 10px 0px;border-bottom: 1px solid #66666696;">
          <i class="icon fa-fw"></i>FILTER
          <?php //echo strtoupper($key);?>
        </div>
        <nav class="yamm megamenu-horizontal">
			<li class="dropdown menu-item" style="font-size: 14px; padding: 8px 10px;width: 100%; border-bottom: 1px solid rgba(0, 0, 0, .15);position: relative; min-height: 45px;align-items: center; flex-wrap: wrap;">
				<a href="#"  data-toggle="collapse" data-target="#opencat" >Category
					<span class="pull-right">
					  <i class="fa fa-angle-down" aria-hidden="true" style="font-size:  25px;margin: -4px 5px 0px 0px;"></i>
					</span>
			   </a>
					
			  <ul class="nav collapse innercatsearch " style="height: auto !important; min-height: auto !important;" id="opencat">
				<div class="head" style="padding: 9px 0px 5px 10px;background: #cd0029;color: #ffffff;margin: 0px 0px 10px 0px;border-bottom: 1px solid #66666696;">
				  <i class="icon fa-fw"></i>
				  <?php echo ucwords($key);?>
				</div>
				  <? foreach($val as $index => $value){ $i++; 
						  $anchor=($value['sub']) ? 'javascript: void(0)' : $value['url'];
				  ?>
				<li class="dropdown menu-item" >
				  <a href="<?php echo $anchor; ?>"  onClick="subcat()"  data-toggle="collapse" data-target="#cat_<?php echo $i; ?>" >
					<?php echo ucwords(strtolower($index));?>
					<span class="pull-right">
					  <i class="fa fa-angle-down" aria-hidden="true" style="font-size:  25px;margin: -4px 5px 0px 0px;"></i>
					</span>
				  </a>
				  <ul class="nav collapse innercatsearch" style="height: auto !important; min-height: auto !important;" id="cat_<?php echo $i; ?>">
					<?foreach($value['sub'] as $index1 => $value1){ ?>
					<li class="dropdown menu-item">
					  <a href="<?php echo $value1['url'];?>"  onClick="subcat()"  >
						<?php echo ucwords(strtolower($value1['name']));?>
					  </a>
					</li>
					<?}?>
				  </ul>
				</li>
				<?}?>
			  </ul>
			</li>
        </nav>
        <?php } ?>
      </div>
      <?php } ?>
      <!-- extra filter starts -->
      <div class="" data-dismiss="modal">
        <a href="javascript:void(0)" class="flitclosebtn" onclick="closeNav()">&times;</a>
      </div>
      <div style="border-bottom: 1px solid rgba(0, 0, 0, .15);">
        <p class="">
          <label for="price" class="pricepr">Price range:</label>
          <!-- <p id="amount2"></p> -->
          <p id="amount"></p>
          <!--<input type = "text" id = "price" style = "border:0; color:#b9cd6d; font-weight:bold;">-->
        </p>
        <div id = "slider-range"></div>
        <input type="hidden" id="amount1" value="<?echo $_GET['pricestart']?>">
        <input type="hidden" id="amount2" value="<?echo $_GET['priceend']?>">
      </div>
          <ul>
            <!--<li class="_3d7x0n" onclick="optionshow('percentage')"><p class="_2-yGHU">Percentage</p><svg class="_2Zylr6"
            xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path></svg></li>-->
            <?php if(count($getfilter['brand'])>1){  ?>
              <li class="_3d7x0n " onclick="optionshow('brand')">
                <p class="_2-yGHU">Brand</p>
                <svg class="_2Zylr6"
                  xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
                </svg>
              </li>
           <?php } ?>
            
            <li class="_3d7x0n" onclick="optionshow('avaliable')">
              <p class="_2-yGHU">Availability</p>
              <svg class="_2Zylr6"
                xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
              </svg>
            </li>
            <li class="_3d7x0n" onclick="optionshow('discount')">
              <p class="_2-yGHU">Discount</p>
              <svg class="_2Zylr6"
                xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
              </svg>
            </li>
<!-- 

 -->          </ul>
          <div class="modal-footer">
            <div class="col-xs-2"></div>
            <div class="col-xs-4" id="clearattr">
              <button type="button" class="applybtn">Clear</button>
            </div>
            <div class="col-xs-4" id="getfilter">
              <button type="button" class="applybtn">Apply</button>
            </div>
            <div class="col-xs-2"></div>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
          </div>
          <!-- extra filters ends -->
      </div>
   </div>
</div>
<div id="myModaloption" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
        <div class="col-xs-12 brandhd" >
                  <div class="col-xs-10">
                 </div>
                  <div class="col-xs-2 close1">
                    <!-- <button type="button" class="clearbtn" data-dismiss="modal">CLEAR</button> -->
                  </div>
               </div>
         <div class="Container container-fluid bg-1 text-center mainatt emi_1 brandlister overflow "  style="display:none;">
            <div class="row">
               <div class="clear"></div>
               <div class="overflowflit">   
               <?foreach($getfilter['brand'] as $key => $val){
                 if($val['brandname'] != '0'){   
               ?>
               <div class="emi_2  bg-2 text-center   ">
                  <div class="row ">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 search"><?echo $val['brandname'];?></div>
                           <?if(in_array($val['brandid'],$param['branddtl'])){
                              $selected="checked";}else{
                              $selected='';
                           }?>
                        <div class="col-xs-2">
                           <b><input id="checkbox-<?echo $val['brandid']?>" class="custom-control-input attributebrand" name="brandradio-2" type="checkbox" value="<?echo $val['id']?>" <?echo $selected;?>>
                              <label for="brandradio-2" class="checkbox-custom-label"></label>
                           </b>
                        </div>
                     </div>
                  </div>
               </div>
               
               <?}}?>
               </div>
               <div class="Container container-fluid  fit">
                  <div class="row">

                     <div class="col-xs-4" >
                        <input type="button" value="Done" onclick="goback('brand');" class="applybtn">
                     </div>
                     <div class="col-xs-4" >
                        <input type="button" value="Close" onclick="goback();" class="applybtn">
                     </div>
                     <?php if(!empty($param['branddtl'])){ ?>
                     <div class="col-xs-4" >
                        <input type="button" value="Clear" onclick="clearbrand();" class="applybtn">
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>

<!----------------------------------------------Discount----------------------------------------------------------- -->
         <div class="Container container-fluid bg-1 text-center mainatt emi_1 discount "  style="display:none;">
            <div class="row">
               <div class="clear"></div>
               <div class="">   
               <?php $discount=$getfilter['maxdisc']!=''  ? 'block' : 'none' ?>
                  <div class="side-menu animate-dropdown outer-bottom-xs border-bottom: 1px solid rgba(0, 0, 0, .15);" style="margin-bottom: 2px !important; display: <?php echo $discount;?>">
                  <a class="collapsed" data-toggle="collapse" href="#collapseDiscounts" role="button"  aria-controls="collapseDiscounts" style = "color : black !important;">
                     <div class="head" id="scrollbar"><i class="icon  fa-fw"></i>Discount
                        <? if( count($param['discountdetail'])>0  && !empty($discountdetailer[0]) ){?>
                        <span class="pull-right"><a href="javascript:void(0)" onclick="cleardiscount();" style="font-size: 12px;color: #fff;" >Clear </a></span>
                        <?}?>
                     </div>
                  </a>
                  <div style="border-bottom: 1px solid rgba(0, 0, 0, .15);">
                     <p class="">
                        <!-- <label for="price" class="pricepr">Price range:</label> -->
                        <p id="amount2Disc"></p>
                        <p id="amountDisc"></p>
                        <br>
                        <!--<input type = "text" id = "price" style = "border:0; color:#b9cd6d; font-weight:bold;">-->
                     </p>
                     <div id = "slider-rangeDisc"></div>
                     <input type="hidden" id="amount1Disc" value="<?echo explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']))[0]?>">
                     <input type="hidden" id="amount2Disc" value="<?echo explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']))[1]?>">
                  </div>                  
                    
                  </div>
               </div>
               <div class="Container container-fluid  fit">
                  <div class="row">

                     <div class="col-xs-4" >
                        <input type="button" value="Done" onclick="filterall('discount');" class="applybtn">
                     </div>
                     <div class="col-xs-4" >
                        <input type="button" value="Close" onclick="goback();" class="applybtn">
                     </div>
                     <?php if(!empty($param['discountdetail'])){ ?>
                     <div class="col-xs-4" >
                        <input type="button" value="Clear" onclick="cleardiscount();" class="applybtn">
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>

         <div class="Container container-fluid  bg-1 text-center mainatt emi_1 avaliable "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 ">Availability</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <div class="Container container-fluid  bg-1  text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 ">Available</div>
                        <? if(in_array(1,$param['avaliable'])){
                           $selected1="checked";}else{
                           $selected1='';
                           }?>
                        <div class="col-xs-2"><b><input id="checkbox-1" class="custom-control-input attributeavaliable" name="avalRadio-3" type="radio" value="1" <?echo $selected1;?>>
                           <label for="avalRadio-3" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 ">Not Available </div>
                        <?  if(in_array(0,$param['avaliable'])){
                           $selected2="checked";}else{
                             $selected2='';
                             }?>
                        <div class="col-xs-2"><b><input id="checkbox-0" class="custom-control-input attributeavaliable" name="avalRadio-3" type="radio" value="0" <?echo $selected2;?>>
                           <label for="checkbox-4" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- <input type="button" value="Done" onclick="goback('aval');" style=" background: #cd0029 !important;text-shadow: none;color: #fff;
               border: none; padding:5px 5px;width: 25%;text-transform: uppercase;font-size: 14px;margin-left: 3%;"> -->
               <div class="Container container-fluid  fit">
                  <div class="row">

                     <div class="col-xs-4" >
                        <input type="button" value="Done" onclick="goback('aval');" class="applybtn">
                     </div>
                     <div class="col-xs-4" >
                        <input type="button" value="Close" onclick="goback();" class="applybtn">
                     </div>
                     <?php if(!empty($param['avaliable'])){ ?>
                     <div class="col-xs-4" >
                        <input type="button" value="Clear" onclick="clearavail();" class="applybtn">
                     </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
         <!---attribute-->
         
         <? 
            //print_R($getfilter['attr']);die;
          foreach($getfilter['attgrp'] as $key=>$val){?>
          <div class="Container container-fluid  bg-1 text-center mainatt emi_1 <?echo str_replace(' ','',$val['name']);?> "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 "><?echo $val['name']?></div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <?
              foreach ($val['att'] as $key1 =>$valatt){?>  
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 "><? echo $valatt['attname'];?></div>
                        <div class="col-xs-2"><b><input id="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom attribute" name="checkbox-<?echo $valatt['id'];?>" type="checkbox" value="<?echo $valatt['id'];?>" <? //echo $selected1;?>>
                           <label for="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <?}?>
               <input type="button" value="Done" onclick="goback('');" style=" background: #cd0029 !important; text-shadow: none; color: #fff;    border: none;
                  padding: 1em 2em;">
            </div>
          </div>
         <?}?>  
         <!--- end---->

      </div>
   </div>
</div>
<!--sorting popup-->
<?php include('include/sortpop.php');?>
<?php include('include/footer.php');?>

<!-- -------------------------new filter modals ends ------------------------------ -->
<style type="text/css">
   p.view {
    text-transform: uppercase;
    font-size: 14px;
    margin-top: 1px;
   }

</style>
<!-- ---------------------New filter modals jQuery start----------- -->
<script>
   var start2=0;
   var filterType = [];
       jQuery(function() {
         let min = <?php
                     if(isset($getfilter['minprice'])){
                        if($getfilter['minprice'] >= 1){
                           echo $getfilter['minprice'];
                        }
                        else{
                           echo 1;
                        }
                     }
                     else if($_GET['pricestart'] >= 1){
                        echo $_GET['pricestart'];
                     }
                     else{
                        if($_GET['pricend'] >= 1 || $getfilter['maxprice'] >= 1){
                           echo 1;
                        }
                        else{
                           echo 0;
                        }
                     }
                     ?>;
         let max = <?php
                     if($getfilter['maxprice'] >= 1){
                        echo $getfilter['maxprice'];
                     }
                     else if($_GET['pricend'] >= 1){
                        echo $_GET['pricend'];
                     }
                     else{
                        echo 0;
                     }
                     ?>;

         let valMin = <?php
                     if($_GET['pricestart'] >= 1){
                        echo $_GET['pricestart'];
                     }
                     else if($getfilter['minprice'] >= 1){
                        echo $getfilter['minprice'];
                     }
                     else{
                        echo 0;
                     }
                     ?>;
         let valMax= <?php
                     if($_GET['pricend'] >= 1){
                        echo $_GET['pricend'];
                     }
                     else if($getfilter['maxprice'] >= 1){
                        echo $getfilter['maxprice'];
                     }
                     
                     else{
                        echo 0;
                     }
                     ?>;

         jQuery( "#slider-range" ).slider({
            range: true,
            min: min,
            max: max,
            values: [ valMin, valMax ],
              slide: function( event, ui ) {
                 //$( "#price" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                 jQuery( "#amount2" ).hide();
                 jQuery( "#amount" ).show();
                 jQuery( "#amount" ).html( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
                 jQuery( "#amount1" ).val(ui.values[ 0 ]);
                 jQuery( "#amount2" ).val(ui.values[ 1 ]); 
              }
           });
           //$( "#price" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );
   
           jQuery( "#amount" ).html( "Rs." + jQuery( "#slider-range" ).slider( "values", 0 ) +
    " - Rs." + jQuery( "#slider-range" ).slider( "values", 1 ) );

           /**
           *           D  I  S  C  O  U  N  T
            */

            let tempMinDisc = <?php
                              if(isset($_REQUEST['discount'])){
                                 echo explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']))[0];
                              }
                              else{
                                 echo 0;
                              }
                              ?>;
          let tempMaxDisc = <?php 
                              if(isset($_REQUEST['discount'])){
                                 echo explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']))[1];
                              }
                              else{
                                 echo 0;
                              }
                              ?>;
          let filterMinDisc = tempMinDisc;
          let filterMaxDisc = tempMaxDisc;
          let getfilterMinDisc = <?php
                                    if($getfilter['mindisc'] >= 1){
                                       echo $getfilter['mindisc'];
                                    }
                                    else{
                                       echo 0;
                                    }
                                 ?>;
          let getfilterMaxDisc = <?php
                                    if($getfilter['maxdisc'] >= 1){
                                       echo $getfilter['maxdisc'];
                                    }
                                    else{
                                       echo 0;
                                    }
                                 ?>;
          let minDisc = 0;
            if(getfilterMinDisc >= 1){
               minDisc =  getfilterMinDisc;
            }
            else if(filterMinDisc >= 1){
               minDisc =  filterMinDisc;
            }
            else{
               if(filterMaxDisc >= 1 || getfilterMaxDisc >= 1){
                  minDisc =  1;
               }
               else{
                  minDisc =  0;
               }
            }
         let maxDisc = 0;
            if(getfilterMaxDisc >= 1){
               maxDisc = getfilterMaxDisc;
            }
            else if(filterMaxDisc >= 1){
               maxDisc = filterMaxDisc;
            }
            else{
               maxDisc = 0;
            }

         let valMinDisc = 0;
            if(filterMinDisc >= 1){
               valMinDisc = filterMinDisc;
            }
            else if(getfilterMinDisc >= 1){
               valMinDisc = getfilterMinDisc;
            }
            else{
               valMinDisc = 0;
            }
         let valMaxDisc= 0;
            if(filterMaxDisc >= 1){
               valMaxDisc = filterMaxDisc;
            }
            else if(getfilterMaxDisc >= 1){
               valMaxDisc = getfilterMaxDisc;
            }
            
            else{
               valMaxDisc = 0;
            }

         jQuery( "#slider-rangeDisc" ).slider({
            range: true,
            min: minDisc,
            max: maxDisc,
            values: [ valMinDisc, valMaxDisc ],
              slide: function( event, ui ) {
                 //$( "#price" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                 jQuery( "#amount2Disc" ).hide();
                 jQuery( "#amountDisc" ).show();
                 jQuery( "#amountDisc" ).html( ui.values[ 0 ] + "% - " + ui.values[ 1 ] + "%");
                 jQuery( "#amount1" ).val(ui.values[ 0 ]);
                 jQuery( "#amount2Disc" ).val(ui.values[ 1 ]); 
              }
           });
           //$( "#price" ).val( "$" + $( "#slider-rangeDisc" ).slider( "values", 0 ) + " - $" + $( "#slider-rangeDisc" ).slider( "values", 1 ) );
   
           jQuery( "#amountDisc" ).html(  jQuery( "#slider-rangeDisc" ).slider( "values", 0 ) +
    "% - " + jQuery( "#slider-rangeDisc" ).slider( "values", 1 ) + "%" );

        });
   
   jQuery('#clearattr').bind('click',function(){
      clearAll();
   });
   
   jQuery('#getfilter').bind('click',function(){
      filterall('pricerange');
   });
   
</script>
<script>

   function clearAll(){
      jQuery('.attributebrand').prop("checked", false);
      jQuery('.attrdisc').prop("checked", false);
      jQuery('.attributeavaliable').prop("checked", false);
      
      filterall('clearAll');  
   }

   function clearbrand(){
    jQuery('.attributebrand').prop("checked", false);
    filterall('brand');
   }

   function cleardiscount(){
      let urlParams = new URLSearchParams(window.location.search);

      urlParams.delete('discount');

      console.log(urlParams);
      let curURI=window.location.pathname;

      window.location.href=curURI+'?'+urlParams.toString();
    /* jQuery('.attrdisc').prop("checked", false);
    filterall('discount');  */
   
   }
   
   function clearavail(){
    jQuery('.attributeavaliable').prop("checked", false);
    filterall('aval');
   }

   function optionshow(ele){
   jQuery('#myModal').modal('hide');
   jQuery('#myModaloption').modal('show');
   jQuery('.mainatt').hide(); 
   if(ele=='brand'){
      // jQuery('.avaliable').hide();  
     jQuery('.brandlister').show();  
   }else if(ele=='avaliable'){
     //jQuery('.brandlister').hide();  
     jQuery('.avaliable').show();  
   } if( ele == 'discount' ){
      jQuery('.discount').show();
   }else{
   /// Query('.em_1').hide(); 
   jQuery('.'+ele+'').show();  
     //jQuery('.avaliable').hide();  
     //jQuery('.brandlister').hide();   
   }
   
   
   }
   
   var filterType = '';
   function goback(type=''){
      if(type == 'brand'){
         filterall('brand');
         filterType = 'brand';
      }
      if(type == 'aval'){
         filterall('aval');
         filterType = 'aval';
      }
      if(type == 'discount'){
         filterall('discount');
         filterType = 'discount';
      }
      jQuery('#myModaloption').modal('hide'); 
      jQuery('#myModal').modal('show');   
   }
</script>
<!-- ---------------------New filter modals jQuery end----------- -->