<?php
class Libsearch{
             public $ci;
          public function __construct(){
             $CI = & get_instance();
             $CI->load->model('Do_product');
			 $CI->load->model('Do_common');
			 $CI->load->library('libsession');
			 $this->ci = $CI;
		}


    public function prouctsearch($param){
			 if(!empty($param['search'])){
			 	$cacheparam['search']=$param['search'];
                $memkey = 'libsearch_search'.json_encode($cacheparam);
                //echo $memkey; die;
                $memCacheDate = $this->ci->libsession->getMemcache($memkey);

                if($memCacheDate){
                  return $memCacheDate;
               	}
               	$totalrowcount=$this->ci->Do_product->searchtotalcount($param);
               	$total_rows=$totalrowcount[0]['cnt'];
               	if($param['bookofthemonth']==1){
               		$config['base_url']= ($param['module']!='brandsfilters') ? ADMINURL.'product/bookofthemonth' : ADMINURL.$param['brandname'].'/'.$param['brandid'];
               	}else{
               	$config['base_url']= ($param['module']!='brandsfilters') ? ADMINURL.'product/search' : ADMINURL.$param['brandname'].'/'.$param['brandid'];
               }
			$config['total_rows']=$total_rows;
			$config['per_page']=LIMIT;
			$config['uri_segment']=3;
			$config['enable_query_strings']=TRUE;

               /*	$config['base_url']= ($param['module']!='brandsfilters') ? ADMINURL.'product/search' : ADMINURL.$param['brandname'].'/'.$param['brandid'];
           		$config['total_rows']=$total_rows;
				$config['per_page']=LIMIT;
				$config['uri_segment']=3;
				$config['enable_query_strings']=TRUE;*/
				if(count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

				$this->ci->pagination->initialize($config);
				$prodata['links']=$this->ci->pagination->create_links();
				//	var_dump($param);
				$res=$this->ci->Do_product->productsearch($param);
				//print $res; print_r($res); die;
		        $wishlist=$this->ci->libsession->getSession('wishlist');
		        //print_r($wishlist);
					if($res){
						foreach($res as $row){
							$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
							$proname=str_replace("--","-", $proname);

							$proname=strtolower(rtrim($proname,'-'));
							$prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$row['proid'].'';
							if($wishlist){
								$arrwishlist=explode(',',$wishlist);
								//print $row['proid'].'hiiiii';
								if(in_array($row['proid'],$arrwishlist)){
									$prodata['product'][$row['proid']]['wishlist']='1';
								}else{
									$prodata['product'][$row['proid']]['wishlist']='0';
								}
							}else{
								$prodata['product'][$row['proid']]['wishlist']='0';
							}
							$prodata['product'][$row['proid']]['id']=$row['proid'];
							$prodata['product'][$row['proid']]['name']=$row['proname'];
							$prodata['product'][$row['proid']]['onlineprice']=$row['onlineprice'];
							$prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
							$setprice = $row['onlineprice'] - $row['discountprice'];
							//echo $setprice;
							$prodata['product'][$row['proid']]['percentage'] = round($setprice / $row['onlineprice'] * 100);
							$prodata['product'][$row['proid']]['callforPrice']=$row['callforprice'];
							$image1=str_replace(SITEMOBILEURL,FILEBASEPATH,str_replace('http:','https:',$row['prothumbnail']));
							$prodata['product'][$row['proid']]['image']= str_replace('http:','https:',$row['prothumbnail']);
							$prodata['product'][$row['proid']]['firstcatid']=$row['firstcatid'];
							//$prodata[$row['proid']]['image']=$row['prothumbnail'];
							$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
							$prodata['product'][$row['proid']]['third_catid']=$row['third_catid'];
							$prodata['product'][$row['proid']]['third_category']=$row['third_category'];
						}
					return $prodata;
				}
				 //print_R($res); die;
				    foreach($res as  $row){
							$prodata['brand'][$row['brandid']]['id' ]=$row['brandid'];
							$prodata['brand'][$row['brandid']]['name' ]=$row['brandname'];
						}
						$i=0;$j=0;
						foreach($res as $row1){
							if($row1['isAvailable']==0){

								$i++;
							}else{
								$j++;
							}
						}

						$prodata['avalibale']['cnt']=	$j;
						$prodata['notavaliable']['cnt']=	$i;
						$resprice=$this->ci->Do_product->getsearchprice($param);
						//print_R($resprice); die;
						$prodata['minprice']=$resprice[0]['minprice'];
						$prodata['maxprice']=$resprice[0]['maxprice'];
				  //

				$this->ci->libsession->setMemcache($memkey,$prodata);		
				return  $prodata;
			}
		}

		public function insertsearchterm($param){
			if($param['search']){
				$res=$this->ci->Do_product->insertsearchterm($param);
			}
		}


		public function getcount($param){
			if(!empty($param['search'])){
				return $this->ci->Do_product->searchproductcount($param);
			}
			return false;
		}

		/*public function catfilter($param){
			if(!empty($param['search']) || $param['module']){
				$res=$this->ci->Do_product->catfilter($param);
				$newfilterarray=array();
				foreach($res as $key=>$val){
					//if($key){
						$newarray['id']=$val['third_catid'];
						$newarray['name']=$val['third_catname'];
						$newarray['url']=SITEURL.'product/search/?searchText='.$param['search'].'&level=3&catid='.$val['third_catid'].'&catname='.$val['third_catname'];

						$newfilterarray[$val['first_catname']][$val['second_catname']]['url'] = SITEURL.'product/search/?searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$val['second_catname'];
						$newfilterarray[$val['first_catname']][$val['second_catname']]['sub'][] = $newarray;
					//}
				}
				return $newfilterarray;
			}else{
				//returns nothing
			}
		}*/

		public function catfilter($param){
			if(!empty($param['search']) || $param['module']){
				$res=$this->ci->Do_product->catfilter($param);
				$newfilterarray=array();
				foreach($res as $key=>$val){
					//if($key){
						$newarray['id']=$val['third_catid'];
						$newarray['name']=$val['third_catname'];
						if($param['module']=='brandsfilters'){
							$newarray['url']=SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&level=3&catid='.$val['third_catid'].'&catname='.$val['third_catname'];
						}else{
								if($param['bookofthemonth']==1){
							$newarray['url']=SITEURL.'product/bookofthemonth/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&level=3&catid='.$val['third_catid'].'&catname='.$val['third_catname'];
						}else{
							$newarray['url']=SITEURL.'product/search/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&level=3&catid='.$val['third_catid'].'&catname='.$val['third_catname'];
						}
						}

						if($param['module']=='brandsfilters'){
							$newfilterarray[$val['first_catname']][$val['second_catname']]['url'] = SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$val['second_catname'];
						}else{
							if($param['bookofthemonth']==1){
							$newfilterarray[$val['first_catname']][$val['second_catname']]['url'] = SITEURL.'product/bookofthemonth/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$val['second_catname'];
						}else{
							$newfilterarray[$val['first_catname']][$val['second_catname']]['url'] = SITEURL.'product/search/?maincat='.$param['maincat'].'&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$val['second_catname'];	
						}
						}
						$newfilterarray[$val['first_catname']][$val['second_catname']]['sub'][] = $newarray;
					//}
				}
				return $newfilterarray;
			}else{
				//returns nothing
			}
		}

		public function brandfilter($param){
			if(!empty($param['search'])){
				$res=$this->ci->Do_product->brandfilter($param);
				return $res;
			}else{
				return false;
			}
		}

		// public function get_searchfilter($param){

		// 	$res['filter_search'] = $this->ci->Do_product->getsearch_filter($param); 
		// 	//print_r($res['filter_clerance']); die;
		// 	if($res['filter_search']){
		// 		$i = 0;
		// 		foreach ($res['filter_search']['brand'] as $key => $val) {
		// 			$filterdata['caturl']   = ADMINURL.str_replace(array(' ', '&'), array('','-'), $val['catname']).'/'.$val['categoryid'];
		// 			$filterdata['category'] = $val['catname'];

		// 			$filterdata['brand'][$i]['brandid']    = $val['brandid'];
		// 			$filterdata['brand'][$i]['brandname']  = $val['brandname'];
		// 			$filterdata['brand'][$i]['brandcount'] = $val['brandcount'];
		// 			$i++;
		// 		}
		// 		$j = 0;
		// 		foreach ($res['filter_search']['available'] as $key => $val) { //print_r($val);
		// 			$filterdata['available'][$j]['clearanceid']    = $val['clearanceid'];
		// 			$filterdata['available'][$j]['count']    = $val['count'];
		// 			$j++;
		// 		}
		// 			$filterdata['minprice'] = $res['filter_search']['price'][0]['minamount'];
		// 			$filterdata['maxprice'] = $res['filter_search']['price'][0]['maxamount'];
		// 	}
		// 	return $filterdata;
	
		// }

		/*created By Viki 17/02/0218 */
		public function brandsearch($param){
			if(!empty($param)){
				//print_r($param);die;
			$res=$this->ci->Do_product->getbranddetail($param);
			//print_r($res);
			foreach ($res as $val) {
				//print_r($val);die;
				$brand['brandname']=$val['brandname'];
				$brand['brandcnt']=$val['probrandcnt'];
			}
			//print_r($brand);
			return $brand;
		    }else{

		    }
		}
		/*created By Viki 17/02/0218: End */
		public function autosuggest($param){
			//print_R($param); die;
			$res=$this->ci->Do_product->autosuggest($param);
			if($res){
			  foreach($res as $row){
				$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['second_catname']);
				$proname=str_replace("--","-", $proname) ;
				$proname=strtolower(rtrim($proname,'-'));
				$proname1=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['third_catname']);
				$proname1=str_replace("--","-", $proname1) ;
				$proname1=strtolower(rtrim($proname1,'-'));
				$brandname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['brandname']);
				$brandname=str_replace("--","-", $brandname) ;
				$brandname=strtolower(rtrim($brandname,'-'));
				$prodata[$row['second_catid']]['id']=$row['second_catid'];
				$prodata[$row['second_catid']]['name']=$row['second_catname'];
				$prodata[$row['second_catid']]['url']=SITEMOBURL.''.$proname.'/'.$row['second_catid'].'?search='.$param['search'].'';
                $prodata[$row['third_catid']]['id']=$row['third_catid'];
				$prodata[$row['third_catid']]['name']=$row['third_catname'];
				$prodata[$row['third_catid']]['url']=SITEMOBURL.''.$proname1.'/'.$row['third_catid'].'?search='.$param['search'].'';

			  }
			}
             //print_R($prodata); die;
			return  $prodata;

		}

		public function websearch($param){
		 	//print_r($param);exit;
			if($param){
				$result=$this->ci->Do_common->productbrandcat($param);

				return $result;

			}else{

				echo "Please pass the search parameter"; die;
			}
		}
}
?>
