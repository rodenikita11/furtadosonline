<style type="text/css">
    .head {
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    color: #ffffff;
    font-size: 16px;
    font-family: 'Open Sans', sans-serif;
    padding: 12px 17px;
    text-transform: capitalize;
    background-color: #cc0028;
    border: 1px solid #cc0028;
    font-weight: 700;
    letter-spacing: 0.5px;
    border-bottom: 1px #cc0028 solid;
    border-radius: 2px;
    }
    .cart .action {
        margin-left: 30px ! important;  
    }
    div#slider-range {
    width: 70%;
    margin: 10px 24px 21px;
    }
    .pricepd{
    padding: 0px 0px 3px;
    }
    .pdr{
    padding: 5px 12px;
    }
    .hot-deals .custom-carousel .owl-controls .owl-prev {
    top: -10px;
    }
    .custom-carousel .owl-controls .owl-prev {
    position: absolute;
    width: 30px;
    height: 30px;
    left: 233px;
    }
    .hot-deals .custom-carousel .owl-controls .owl-next {
    top: -10px;
    }
    .custom-carousel .owl-controls .owl-next {
    position: absolute;
    width: 30px;
    height: 30px;
    right: -4px;
    }
    #discount{
    overflow: auto !important;
    height: auto !important; 
    min-height: auto !important;
    }
    .nano { background: #bba; width: 500px; height: 500px; }
    .nano .nano-content { padding: 10px; }
    .nano .nano-pane   { background: #888; }
    .nano .nano-slider { background: #111; }
</style>

<!-- ============================================== HOT DEALS ============================================== -->

<?php if($banner_category['hotdeal_banner']){ ?>
<div class="sidebar-widget hot-deals wow fadeInUp " style="padding: 0px;">
<h3 class="section-title head"><i class="icon  fa-fw"></i>Other Hot Deals</h3>
  <!-- <h3 class="section-title" style="text-transform: capitalize;">hot deals</h3> -->
  <? foreach($banner_category['hotdeal_banner'] as $hotdeal_banners ){?>
     <div class="item">
        <div class="products">
           <div class="hot-deal-wrapper">
              <div class="image">
                <a href="<?php echo $hotdeal_banners['url'];?>" target="_blank"> 
                 <img  <?if(isset($hotdeal_banners['imgpath'])){?> src="<?php echo $hotdeal_banners['imgpath'];?>" <?}else{?> src="<?php echo WEBIMG?>banners/Noimage (1).jpg" <?}?> alt=""></a>
              </div>
           </div>
           <!-- /.hot-deal-wrapper -->
        </div>
     </div>
     <?}?>
</div>
<?php } ?>
<!-- ============================================== HOT DEALS: END ============================================== -->
