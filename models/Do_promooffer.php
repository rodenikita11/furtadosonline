<?php
	class Do_promooffer Extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			
	        $this->load->database();
		}
		
		public function getComboProducts($param){
			//print_r($param); die;
			
			$reletedproduct = "SELECT cpm.*,pm.isAvailable as parentavl , pm.* ,((pm.onlinePrice/100)*(100-cpm.offer_perc)) as combo_price,((pm.onlinePrice/100)*(cpm.offer_perc)) as combo_saving FROM `combo_product_masters` cpm LEFT JOIN product_master pm ON pm.proid=cpm.`product_id` WHERE (`main_product` in(".$param['id'].") or `main_category` in(".$param['catid'].")) and cpm.status=1";
			//echo $reletedproduct; die;

			$res=$this->db->query($reletedproduct);
			if($res){
			  $res1=$res->result_array();
			  return $res1;
			}else{
				return false;
			} 
			
			
		}
		public function valid_offer_date($date_from,$date_end)
		{
			$currentDate = date('Y-m-d');
			$currentDate=date('Y-m-d', strtotime($currentDate));; 
				if(!empty($date_end) && !empty($date_from))
				{
					$offerDateBegin = date('Y-m-d', strtotime($date_from));
					$offerDateEnd = date('Y-m-d', strtotime($date_end));
					 if (($currentDate > $offerDateBegin) && ($currentDate < $offerDateEnd))
						{
						  return true;
						}
						else
						{
						  return false;
						}
				
				}
				else
				{
					return false;
				}

		}
		
		function getavailableComboProduct($param)
		{
			$reletedproduct = "SELECT cpm.combo_pro_id, b.id AS prosellerid, a.proid, a.prosku , a.proname,a.prothumbnail,b.price,cpm.offer_amount AS combo_offer_amount, b.sellersku, CONCAT(c.contactperson1_firstname,' ',c.contactperson1_lastname) AS name1,pd.percentage, b.seller_id,d.category_id AS cid FROM product_master a JOIN product_seller b ON a.proid=b.proid LEFT JOIN agent_seller c ON b.seller_id = c.id JOIN product_to_category d ON a.proid=d.proid LEFT JOIN product_discount pd ON a.proid=pd.proid LEFT OUTER JOIN combo_product_masters cpm ON cpm.product_id=b.proid WHERE cpm.status='A' AND cpm.offer_amount <= ".$this->dbc->escape($param['amount_less_than'])." GROUP BY b.proid ORDER BY RAND() LIMIT ".$param['limit'];

			$res=$this->db->query($reletedproduct);
			if($res){
			  $res1=$res->result_array();
			  return $res1;
			}else{
				return false;
			} 
	   }

}

?>
