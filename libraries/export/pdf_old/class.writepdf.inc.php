<?php
require('../fpdf.php');

class PDF extends FPDF
{
// Page header
function Header()
{
	// To be implemented in your own inherited class
	foreach($glbResp["keys"] as $key=>$value) {
		$this->Cell(40,7,$value,1,0,'C',true);
	}
	$pdf->Ln();
}

// Page footer
function Footer()
{
	// Position at 1.5 cm from bottom
	$this->SetY(-15);
	// Arial italic 8
	$this->SetFont('Arial','',8);
	// Page number
	$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}
?>
