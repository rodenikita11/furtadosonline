 <section>
            <div class="block color-scheme-2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <article style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="payment-service wow fadeInLeft" data-wow-duration="1s">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-thumbs-up"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Safe Payments</h3>
                                        <p>All your money is secure and safe with spidersmall.com</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="payment-service wow fadeInUp" data-wow-duration="1s">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-truck"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">Free Shipping</h3>
                                        <p>Save even more with Free Shipping on orders.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4">
                            <article style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;" class="payment-service wow fadeInRight" data-wow-duration="1s">
                                <a href="#"></a>
                                <div class="row">
                                    <div class="col-md-4 text-center">
                                        <i class="fa fa-fax"></i>
                                    </div>
                                    <div class="col-md-8">
                                        <h3 class="color-active">24/7 Support</h3>
                                        <p> 24/7 customer engagement for service</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>