<style type="text/css">
    .clickable {
        cursor: pointer;
    }
    
    .panel-heading span {
        margin-top: -20px;
        font-size: 15px;
    }
    .panel-primary>.panel-heading {
    color: #fff;
    background-color: #cc0028;
    border-color: #cc0028;
}
</style>

<div class="container outer-top-150">
 <h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Career</h2>
 <?php foreach($joblist as $row) {?>
    <div class="row" style="margin-left: -10px">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><b> <?php echo $row['title'];?></b></h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <table class="table" width="100%">
                        <tr>
                            <th>Profile </th>
                            <td>:</td>
                            <td><?php echo $row['profile'];?>.</td>

                        </tr>
                        <tr>
                            <th>Key Skills </th>
                            <td>:</td>
                            <td><?php echo str_replace('-', '<br>', $row['keySkills']);?>.</td>

                        </tr>
                        <tr>
                            <th>Qualification </th>
                            <td>:</td>
                            <td><?php echo $row['qualification'];?></td>

                        </tr>
                        <tr>
                            <th>Experience </th>
                            <td>:</td>
                            <td><?php echo $row['experience'];?></td>

                        </tr>
                        <tr>
                            <th>Location </th>
                            <td>:</td>
                            <td><?php echo $row['location'];?></td>

                        </tr>
                        <tr>
                            <th>Salary </th>
                                <td>:</td>
                                <td><?php echo $row['salary'];?></td>

                        </tr>
                        <tr>
                            <th>Post Date </th>
                            <td>:</td>
                            <td><?php echo date('F j, Y ',strtotime($row['postDate']));?></td>
                            <!-- Dec 11, 2017 -->
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <a href="mailto:careers@furtadosonline.com?subject=Application for post of Sales Representative- Lajpat Nagar (Delhi)" class="btn" style="background: #cc0028;color: #fff ! important;"><strong>Apply for this Job</strong></a></td>

                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?}?>
   

    

</div>

<script type="text/javascript">
    $(document).on('click', '.panel-heading span.clickable', function(e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        }
    })
</script>