<style>
   p {
   font-size: 14px;
   text-align: justify;
   }
</style>
<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px">
         <div class="col-lg-12 outer-top-150">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;text-align: center;">Assurance Program</h2>
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;"> Piano Tuning</h2>
            <p>We will provide 2 FREE piano tuning sessions within the first 12 months of purchase.</p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
             <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 10px;">Terms and Conditions of Warranty (This Warranty Is Valid In India Only)</h2>
            <p>• Any new product purchased has a warranty to be free from defects in material and workmanship for the number of years specified on the warranty, subject to limitations contained in the warranty.</p>

            <p>• The warranty is extended to the original retail purchaser only and may not be transferred or assigned to subsequent owners.</p>

            <p>• The customer has to produce the original Furtados Assurance Certificate which should have the date of purchase and/or the invoice, failing which the warranty would not be honoured.</p>

            <p>• If at any time the product purchased malfunctions as a result of faulty material or workmanship, Furtados will repair the defect/s or replace the product as it deems appropriate at its sole discretion.</p>

            <p>• The product will have to be produced at the retail showroom for repair or shipped to our service centre, freight pre-paid by the customer. If the product is to be shipped back to the owner after repair the freight charges will need to be borne by the owner. Insurance for the product during transit – to and fro – is highly recommended. Furtados will not be liable for any damage that may occur in transit and delivery of the product.</p>

            <p>• Furtados reserves the right to use substitute materials at the time of repair in the event that the original materials are no longer available.</p>

            <p>• In the case of an unlikely event where the product is destroyed, lost or damaged beyond repair, while in the possession of Furtados, it will be replaced with one of the same or most similar style of a value not in excess of the original purchase price of the product. Any insurance covering the product including but not limited to the Collector’s Item value insurance, must be carried by the owner at the owner’s expense.</p>
            <p>• Furtados makes no other express warranty of any kind whatsoever. All implied warranties, including warranties of merchantability and fitness for a particular purpose, exceeding the specific provisions of this warranty are hereby disclaimed and excluded from this warranty.</p>
            <p>• Furtados shall not be liable for any special, indirect, consequential, incidental or other similar damages suffered by the purchaser or any third party, including without limitation, damages for loss of profits or business or damages resulting from use or performance of the product, whether in contract or in tort, even if Furtados or its authorized representative has been advised of the possibility of such damages. Furtados shall not be liable for any expenses, claims, or suits arising out of or relating to any of the foregoing.</p>
            <p>• Furtados will not be liable for any irregularities in service in the case of a warranty which is dependent on a third party.</p>
            <p>• Furtados shall not be liable or be deemed to be in default for any delay or failure in performance resulting directly or indirectly from causes beyond its reasonable control including delay in repairing due to non-availability of component and/or accessory especially vis-a-vis the import of supplies or if the company is prevented from performing its functions under the warranty.</p>
            <p>• All disputes will be subject to the exclusive jurisdiction of Mumbai, India, only.</p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
             <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 10px;">The Warranty Will Not Apply In The Following Situations (Limitations Of Warranty)</h2>

            <p>• Any product that has been altered or modified in any way or upon which the serial number has been tampered with or altered.</p>

            <p>• Any product whose warranty form has been altered or upon which false information has been given.</p>

            <p>• Any product that has been damaged due to misuse, negligence, accident or improper operation.</p>

            <p>• The subjective issue of tonal characteristics.</p>

            <p>• Any repair work carried out/ tampered by unauthorized personnel/agencies.</p>

            <p>• Shipping damages of any kind.</p>

            <p>• Any product that has been subjected to extremes of humidity or temperature.</p>

            <p>• Normal wear and tear (For e.g. keypads, springs for wind instruments, drum lugs and pedals, worn frets, worn machine heads, worn plating, string replacement, scratched pick guards, LCD display, casing, moving parts, tube valves or damages to or discoloration of the product finish for any reason)</p>

            <p>• Essential consumables required for the proper functioning of the product including but not limited to guitar strings, wind instrument reeds and drum heads supplied with/ as part of the product are not covered under any warranty.</p>

            <p>•	More than one replacement of rubber pads and switches. Rubber pads and switches will be replaced free of charge for one time within the warranty period.</p>

            <p>• Any factory installed electronics in acoustic instruments, after a period of 1 year following the original date of purchase.</p>

            <p>• Any product damaged by exposure to insects, pests or like organisms.</p>

            <p>• Any form of damage resulting from fire, water, unregulated or defective power supply, riot, mishandling, commercial use or any act of nature and use contrary to operating instructions specified in the user manual supplied with the equipment.</p>

            <p>• Damage resulting out of excessive force and/or repetitive non-standard actions.</p>
         </div>
      </div>
   </div>
</div>