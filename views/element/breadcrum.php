<?php 
$param['category'] = $this->catid;
$res = $this->Do_category->getcatparentage($param);
?>
<?php if($res){ ?>
<div class="block-breadcrumb">
    <ul class="breadcrumb">
        <li><a href="<?php echo SITEURL?>">Home</a></li>
			<?php foreach($res as $row){ ?>
         <li><a href="<?php echo $row['url']?>"><?php echo ucfirst(strtolower($row['name'])); ?></a></li>
			<?php } ?>
         <!--<li class="active">Products grid</li>-->
    </ul>
</div>
<?php } ?>