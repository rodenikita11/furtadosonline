 <?$name=$this->libsession->getSession('usrname'); ?>
  <!-- ============================================== Newsletter ============================================== -->
     <section class="Newsletter">
       <div class="container">
          <div class="content clearfix">
            <div class="col-md-2"></div>
             <div class="col-md-4">
               <img class="img-responsive MT-25" src="<?php echo WEBIMG;?>banners/mail.png" alt="">
               <div class="Subnew">
               <h2>Subscribe Our Newsletter</h2>
               <p>Get regular updates by email.</p>
              </div>
             </div>
             <div class="col-md-4">
              <div class="Subscribe-btn">
              <form action="#" id="newsletterform" method="post">
               <input type="email" placeholder="Enter Email ID" id="emailnl" name="emailnl">
               <button type="submit" class="thm-btn">Register</button>
             </form>
             </div>
           </div>
             <div class="col-md-2"></div>
          </div>
       </div>
   </section>

    <!-- ============================================== Newsletter End  ============================================== -->

<section style="clear: both;" class="info-boxes">
<div class="copyright-bar" style="border-top: none;">
    <div class="container">
      <div class="col-xs-12 col-sm-3 no-padding">
        <div class="info-box">
               <div class="row">
                  <a href="<?echo SITEMOBURL;?>user/paymenthelp"><div class="col-xs-12">
                   <h4 class="info-box-heading green">Get payment support &#8595;</h4>
                  </div></a>
               </div>

            </div>
      </div>
       <div class="col-xs-12 col-sm-3 no-padding">
        <div class="info-box">
               <div class="row">
                  <div class="col-xs-12">
                   <a href="mailto:response@furtadosonline.com"><h4 class="info-box-heading green" style="text-transform: lowercase;font-weight: 400;font-size: 14px;">response@furtadosonline.com</h4>
                   </a>
                  </div>
               </div>

            </div>
      </div>
      <div class="col-xs-12 col-sm-6 no-padding">
        <div class="clearfix payment-methods">
          <h4 class="info-box-heading green col-md-5">We Accept All Leading Credit/Debit Cards</h4>
            <!--<ul>
           
          <li><img src="<?php echo WEBIMG;?>payments/2.png" alt="" style="width:40px;"></li>
            <li><img src="<?php echo WEBIMG;?>payments/3.png" alt=""></li>
            <li><img src="<?php echo WEBIMG;?>payments/4.png" alt=""></li>
            <li>
            <img src="<?php echo WEBIMG;?>payments/5.png" alt=""></li>
             <li>
             <img src="<?php echo WEBIMG;?>payments/6.png" alt="">
           </li>

          </ul>-->
          <img src="<?php echo WEBIMG;?>payments/Payment.png">
        </div>
        <!-- /.payment-methods -->
      </div>
    </div>
  </div>
</section>
<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3 p-l">
          <div class="module-heading">
            <h4 class="module-title">Offer</h4>
          </div>
          <!-- /.module-heading -->


          <div class="module-body">
            <ul class='list-unstyled'>
              <!--<li class="first"><a title="Instruments" href="#">Instruments</a></li>
              <li><a title="Misuc Books" href="#">Misuc Books</a></li>
              <li><a title="Pro Audio" href="#">Pro Audio</a></li>-->
              <li><a title="Clearance" href="<?php echo SITEMOBURL?>product/getcleranceproduct" title="Clearance">Clearance</a></li>
              <!--<li class="last"><a title="Gift Card" href="<?php echo SITEMOBURL?>product/giftcart" title="Gift Card">Gift Card</a></li>-->
              <li class="last"><a title="Offer Zone" href="<?php echo SITEMOBURL?>product/offerzone" title="Offer Zone">Offer Zone</a></li>
            </ul>
          </div>
          <!-- /.module-body -->
        </div>
        <!-- /.col -->

        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Information</h4>
          </div>
          <!-- /.module-heading -->

          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a href="<?php echo SITEMOBURL?>user/aboutus" title="About Us">About Us</a></li>
              <li><a href="<?php echo SITEMOBURL?>user/contactus" title="Contact Us">Contact Us </a></li>
              <li><b><a href="<?php echo SITEMOBURL?>user/job" title="Career">Career</a></b></li>
              <li><a href="<?php echo SITEMOBURL?>user/pianoenquiry" title="Piano Enquiry">Piano Enquiry</a></li>
              <!--<li class="last"><a href="#" title="Feedback">Feedback</a></li>
              <li class="last"><a href="#" title="Sitemap">Sitemap</a></li>-->
            </ul>
          </div>
          <!-- /.module-body -->
        </div>
        <!-- /.col -->

        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Other Portals</h4>
          </div>
          <!-- /.module-heading -->

          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a title="Furtados Artists" href="http://furtadosartists.com/" title="Furtados Artists">Furtados Artists</a></li>
              <li><a title="High Furtados" href="http://www.highfurtados.com/" title="High Furtados">High Furtados</a></li>
              <li><a title="Furtados School Of Music" href="http://www.furtadosschoolofmusic.com/" title="Furtados School Of Music">Furtados School Of Music</a></li>
              <li><a href="<?php echo SITEMOBURL?>blog/" title="Blog">Blog</a></li>

            </ul>
          </div>
          <!-- /.module-body -->
        </div>
        <!-- /.col -->

        <div class="col-xs-12 col-sm-6 col-md-3 p-r">
          <div class="module-heading">
            <h4 class="module-title">Policies</h4>
          </div>
          <!-- /.module-heading -->

          <div class="module-body">
            <ul class='list-unstyled'>
              <li class="first"><a href="<?php echo SITEMOBURL?>user/shippingpolicy" title="Shipping Policy">Shipping Policy </a></li>
              <li><a href="<?php echo SITEMOBURL?>user/returnpolicy" title="Return Policy ">Return Policy </a></li>
              <li><a href="<?php echo SITEMOBURL?>user/privacypolicy" title="Privacy Policy ">Privacy Policy </a></li>
              <li><a href="<?php echo SITEMOBURL?>user/assuranceprogram" title="Assurance Program">Assurance Program</a></li>
              <li class=" last"><a href="<?php echo SITEMOBURL?>user/termsandcondition" title="Terms & Conditions">Terms & Conditions</a></li>
            </ul>
          </div>
          <!-- /.module-body -->
        </div>
      </div>
    </div>

    <div class ="container">
    	<div class="row">
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

    			<a class="maincat" href="#">MUSICAL INSTRUMENT</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>pianos/212">Piano</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>acoustic-pianos/1106">Acoustic Pianos</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>digital-pianos/1107">Digital Pianos</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>piano-accessories/1128">Piano Accessories</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>guitars-bass/206">Guitars & Bass</a>	
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-accessories/1078">Guitar Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-amps/1079">Guitar Amps</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-cables/1080">Guitar Cables</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-pick-ups/1081">Guitar Pick-ups</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-stands/1082">Guitar Stands</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>pedals-processors/1083">Pedals & Processors</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>banjos-ukeleles/1085">Banjos & Ukeleles</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>basses/1086">Basses</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>classical-guitars/1087">Classical Guitars</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>electric-guitars/1088">Electric Guitars</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>keyboards-dmi/209">Keyborad & Dmi</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>keyboard-accessories/1100">Keyboard Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>keyboard-amps/1101">Keyboard Amps</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>keyboards/1102">Keyboard</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>modules/1103">Modules</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>synths-etc/1104">Synths, Etc</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>organs/1129">Organs</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>drums-percussion/202">Drums & Percussion</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>cymbals/1062">Cymbals</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>digital-drums/1063">Digital Drums</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>drum-accessories/1064">Drums Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>drum-hardware/1065">Drums Hardware</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>drum-heads/1066">Drum Heads</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>drum-sets/1067">Drum Sets</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>drum-sticks/1068">Drum Sticks</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>percussion/1069">Percussion</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>marching-percussion/1131">Marching Percussion</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>tuned-percussion/1132">Tuned Percussion</a>
					<a class="subcat" href="<?php echo SITEMOBURL?>product/promotion?id=456#dropdown-EDUCATION">Education</a>
						<a class="subsubcat" href="<?php echo SITEMOBURL?>education-accessories/1071">Education Accessories</a>
						<a class="subsubcat" href="<?php echo SITEMOBURL?>education-instruments/1072">Education Instrument</a>
						<a class="subsubcat" href="h<?php echo SITEMOBURL?>metronomes/1074">Metronomes</a>
						<a class="subsubcat" href="<?php echo SITEMOBURL?>notation-stands/1075">Notation Stands</a>
						<a class="subsubcat" href="<?php echo SITEMOBURL?>pitch-pipes-tuners/1076">Pitch Pipes & Tuners</a>

    				<a class="subcat" href="<?php echo SITEMOBURL?>bowed-strings/214">Bowed Strings</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>cello-bass-accessories/1113">Cello & Bass Accessories</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>cellos-double-bass/1115">Cellos & Double Bass</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>viola-accessories/1116">Viola Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>viola/1117">Viola</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>violin-accessories/1118">Violin Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>violins/1119">Violins</a>

    				<a class="subcat" href="<?php echo SITEMOBURL?>wind-instruments/216">Wind Instruments</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>brass/1124">Brass</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>brass-accessories/1125">Brass Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind/1126">Woodwind</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind-accessories/1127">Woodwind Accessories</a>

    				
    				<a class="subcat" href="<?php echo SITEMOBURL?>harmonicas-accordions/215">Harmonics & Accordions</a>	
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>accordions/1121">Accordions</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>harmonicas/1122">Harmonicas</a>

    				<a class="subcat" href="<?php echo SITEMOBURL?>indian-instruments/208">Indian Instrument</a>	
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>electronic-indian-instrument/1092">Electronics Indian Instrument</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>harmoniums/1093">Harmoniums</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>indian-instrument-accessories/1094">Indian Instrument Accessories</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>indian-percussion/1095">Indian Percussion</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>indian-string-instruments/1096">Indian String Instruments</a>
    					<a class="subsubcat" href="h<?php echo SITEMOBURL?>indian-wind-instruments/1097">Indian Wind Instruments</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>tablas/1098">Tablas</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>computer-music/201">Computer Music</a>
    			<!--	<a class="subcat" href="<?php echo SITEMOBURL?>religious/301">Religious</a>-->
    				<a class="subcat" href="<?php echo SITEMOBURL?>instrument-bundles/219">Instrument Bundles</a>	
    				<a class="subcat" href="<?php echo SITEMOBURL?>audio-cds/218">Audio CDs</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>musician-essentials/217">Musician Essentials</a>
    				<!--<a class="subcat" href="<?php echo SITEMOBURL?>music-gifts/205">Music Gifts</a>-->
    				<a class="subcat" href="<?php echo SITEMOBURL?>other-fretted-instruments/207">Other Fretted Instruments</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>mandolins-accessories/1090">Mandolins & Accessories</a>
    					<br><br>
    					
    				<!--musical category finish-->

    			<a class="maincat" href="#">MUSICAL BOOKS</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>miscellaneous-books/111">Miscellaneous Books</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>music-magazines/112">Music Magazines</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>repertoire-albums/113">Repertoire & Albums</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>classical/1049">Classical</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>collections-folios/1050">Collection & Follos</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>movie-stage/1051">Movie & Stage</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>personalities-groups/1052">Personalities & Groups</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>voice/114">Voice</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>choral0/1054">Choral0</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>voice-technique/1055">Voice Technique</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>voice-tutors/1056">Voice Tutors</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>wind/115">Wind</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>brass/1058">Brass</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>harmonica/1059">Harmonica</a>
    					<a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind/1060">Woodwind</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>keyboard/110">Keyword</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>indian-instruments/109">Indian Instruments</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>bowed-strings/101">Bowed Strings</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>drums-percussion/102">Drums & Percussion</a>	




    				<!--musical category finish-->
    				<br><br>
    					

    			<a class="maincat" href="#">PRO AUDIO</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>amplifiers/401">Amplifiers</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>speakers/402">Speakers</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>software/403">Software</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>stage-monitors/404">Stage Monitors</a>
    			<!--	<a class="subcat" href="<?php echo SITEMOBURL?>apple-products/416">Apple Products</a>-->
    				<a class="subcat" href="<?php echo SITEMOBURL?>dj/417">Dj</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>pro-audio-bundles/418">Pro-audio Bundles</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>live-sound/419">Live Sounds</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>home-studio/420">Home Studio</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>home-sound/415">Home Sound</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>portable-recording-devices/414">Portable Recording Devices</a>
    				<a class="subcat" href="<?php echo SITEMOBURL?>desktop-monitors/405">Desktop Monitors</a>


    			   <!--musical category finish-->

    		</div>
    	</div>

    </div>


    <div class="copyright-bar">
    <div class="container">
      <div class="col-xs-12 col-sm-2s no-padding social" style="margin-bottom: 12px;">
      <h6 class="stay">stay connected</h6>
        <ul class="link">
          <li class="fb pull-left"><a target="_blank" rel="nofollow" href="https://www.facebook.com/FurtadosMusic" title="Facebook"></a></li>
          <li class="tw pull-left"><a target="_blank" rel="nofollow" href="https://twitter.com/furtados" title="Twitter"></a></li>
          <li class="googleplus pull-left"><a target="_blank" rel="nofollow" href="https://www.instagram.com/furtadosmusic/" title="Instagram"></a></li>
          <li class="youtube pull-left"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/furtadosmusicindia" title="Youtube"></a></li>
           <li class="pintrest pull-left"><a target="_blank" rel="nofollow" href="https://plus.google.com/u/0/+furtadosmusicindia" title="googleplus"></a></li>
        </ul>

      </div>
    <p style=" text-align:  center;">Copyright © 2018 Furtadosonline.com</p>

    </div>
  </div>
  </div>

</footer>
<div id="loading" style="display:none;">
                <img src="<?echo SITEIMAGES?>loading_gif.gif" style="width:40px;" />  
           </div>
<!--modal popup of forgotpassword-->
   <?require_once (__DIR__ .'/forgotpassword.php');?>
   <!--modal popup of forgotpassword:end -->
<!-- ============================================================= FOOTER : END============================================================= -->
  </div>
         <!-- /.homebanner-holder-->
      <!-- ============================================== CONTENT : END ============================================== -->
<!-- JavaScripts placed at the end of the document so the pages load faster -->

      <!-- <script src="<?php echo WEBJS;?>bootstrap-hover-dropdown.min.js"></script>-->
      <script src="<?php echo WEBJS;?>owl.carousel.min.js"></script>
      <script src="<?php echo WEBJS;?>echo.min.js"></script>
      <script src="<?php echo WEBJS;?>jquery.easing-1.3.min.js"></script>
      <script src="<?php echo WEBJS;?>bootstrap-slider.min.js"></script>
      <script src="<?php echo WEBJS;?>jquery.rateit.min.js"></script>
      <script type="text/javascript" src="<?php echo WEBJS;?>lightbox.min.js"></script>
      <script src="<?php echo WEBJS;?>bootstrap-select.min.js"></script>
      <script src="<?php echo WEBJS;?>wow.min.js"></script>
      <script src="<?php echo WEBJS;?>scripts.js"></script>
      <script src="<?php echo WEBJS;?>jquery.magnify.js"></script>
      <script src="<?php echo WEBJS;?>jquery-ui.min.js"></script>
      <script src="<?php echo WEBJS;?>jquery.spzoom.js"></script>
      <script src="<?php echo WEBJS;?>bootbox.min.js"></script>
      <script src="<?php echo WEBJS;?>common.js"></script>
      <script src="<?php echo WEBJS;?>xzoom.js"></script>
      <script src="<?php echo WEBJS;?>jquery.nanoscroller.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

   </body>
</html>
<style type="text/css">
  ul.product.ui-menu.ui-widget.ui-widget-content.ui-front{
    display: block !important;
  }
  ul.brand.ui-menu.ui-widget.ui-widget-content.ui-front{
   display: block !important; 
  }
  ul.category.ui-menu.ui-widget.ui-widget-content.ui-front{
    display: block !important; 
  }
  ul.ui-menu.ui-widget.ui-widget-content.ui-front{
    display: block !important; 
  }

  a.subcat{
        border-left: 1px solid grey !important;
    padding-left: 4px !important;
    font-size: 12px !important;
}
a.subsubcat{ border-left: 1px solid grey !important;
    padding-left: 4px !important;
    font-size: 12px !important; }
 
</style>
<script type="text/javascript">

  var moduleid='<?php echo $_GET['id'];?>';
  var offer = '<?php echo $this->uri->segment(2);?>';
  var promourl=window.location.href;
  //alert(promourl);

  function displayPassDiv(){
    //alert('hiii');
    jQuery('#forgotpasswordpop').modal('show');
    jQuery('#login').modal('hide'); 
//document.getElementById("idFormInform").style.display = "block";
}
 jQuery('#inform').validate({
        rules:{
            emailid:{
                email:true,
                required:true
            }
        },
        messages:{
            emailid:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            }
        },
    });

    jQuery('#forgotpassword').click(function(){
    jQuery('#forgotpassword').attr('disabled','disabled');     
        var success=jQuery("#inform").valid();
        if(success){
        //var emailid = document.getElementById('emailid').value;
       var emailid=jQuery('#emailid').val();
  //alert(emailid);
        jQuery.ajax({
                    url:'<?php echo SITEMOBURL?>user/saveforgotpassword',
                    type:'POST',
                    dataType:'JSON',
                    data:{emailid:emailid},
                    success:function(result){
                       // res=eval(result);
                        //console.log(result); return false;
                        jQuery('#forgotpassword').removeAttr("disabled");
                        if(result.status== true)
                        {
                          //console.log('df');
                            jQuery('#forgotpasswordpop').hide();
                            bootbox.alert('Thank you. You will receive a mail for password change shortly',function(){
                              jQuery('#login').modal('show');
                             // window.location.reload();
                            });
                            
                            //jQuery('#login').modal('show');
                           // jQuery('#emailid').val(' ');
                            //window.history.go(1);
                        //window.location.reload();
                        }else{
                            bootbox.alert('Sorry you are not registered with us or kindly enter the valid email Id & Password.',function(){
                             // window.history.go(-1);
                              jQuery('#forgotpasswordpop').modal('show');
                              jQuery('#emailid').val(' ');
                              jQuery('#myModal').modal('hide');  
                            });
                            
                        }
                    }
                });
       /* var baseurl = '<?echo SITEMOBURL?>';
            jQuery.ajax({
                
                type: "POST",
                url: baseurl+'user/saveforgotpassword',
                data:{emailid:emailid},
                success:function(result){

                    res=jQuery.parseJSON(result);
                    console.log(res);
                    if(res.status == true){
                        bootbox.alert('Thank you. You will receive a mail for password change shortly');
      jQuery('#forgotpasswordpop').model('hide');
                        jQuery('#emailid').val(' ');
      
                        jQuery('#myModal').modal('hide');
                   }else if(res.status == false){
         
                     bootbox.alert('Sorry you are not registered with us or kindly enter the valid email Id & Password.');
         jQuery('#forgotpasswordpop').hide();
         window.history.go(-1);
                     jQuery('#emailid').val(' ');
         jQuery('#myModal').modal('hide');

                    }
                }
            });*/
        }
    });
   jQuery('#newsletterform').validate({
        rules:{
            emailnl:{
                email:true,
                required:true
            }
        },
        messages:{
            emailnl:{
                email:'Please Enter valid EmailID',
                required:'Please Enter EmailID'
            }
        },
        submitHandler:function(form)
        {
            var valid=jQuery('#newsletterform').valid();

            var email=jQuery('#emailnl').val();
            if(valid){
                jQuery.ajax({
                    url:'<?php echo SITEMOBURL?>user/newslettersubmit',
                    type:'POST',
                    dataType:'JSON',
                    data:{email:email},
                    success:function(result){
                        res=eval(result);
                        if(res.status== true)
                        {
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val('');
                              window.location.reload();  
                            });
                            
                        }else{
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val();  
                            });
                            
                        }
                    }
                });
            }
        }
    });
  function addtobundle(ele){
  proid=ele;
  jQuery.ajax({
    url:'<?php echo SITEMOBURL?>product/addtobundle',
    type:'POST',
    data:{proid:proid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
           bootbox.alert('Product successfully added to bundle cart',function(){
           window.location.reload(); 
           });
           
        }else{
         bootbox.alert('Error while adding product to bundle cart');

        }

    }

  });

}
function checkloginforeview()
  {
    jQuery.ajax({
      url:'<?php echo SITEMOBURL?>product/checkloginforeview',
      type:'POST',
      success:function(data){   
        var response = jQuery.parseJSON(data);
          console.log(response); 

        if(response.status==false){
          //jQuery('#review').modal('hide');
          bootbox.alert('You need to login to add Review',  function(){
            

            jQuery('#login').modal('show');
          //  window.location.reload();
            
          })
           
              //jQuery('#postreviews').click();
          }else if(response.status==true){
            
           jQuery('#review').modal('show');
      }
    }
    });
  }
function removefromwishlist(ele, removefromcart=false) {

  var pid=ele;
 // alert(pid); //return false;
 // var segmenturl=<?php echo $this->uri->segment(2);?>
 // console.log(segmenturl);
    jQuery.ajax({
    url:'<?php echo SITEMOBURL?>user/removefromwishlist',
    type:'POST',
    data:{pid:pid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
          if(!removefromcart){
              bootbox.alert('Product Removed Successfully From Shortlist', function(){
                <?if($this->uri->segment(2)=='user'){?>
                 window.location.href='<?php echo SITEMOBURL?>user/myaccount?name=wishlist';
                 <?}else{?>
                 window.location.reload(); 
                  <?}?>
              });
          }else{
              window.location.reload();
          }
          // window.location.reload();
        }else{
         bootbox.alert('Error in removing  product from Shortlist');
        }

    }

  });


}
 
 var checkviewcartadded=false;

function gotoviewcart(ele, isavailable, element=false){
  //alert('hii');
  var module='buynow';
   if(isavailable){
    addtocart(ele, element, module);
      //console.log(checkviewcartadded);
       if(checkviewcartadded){
          window.location.href="<?php echo SITEMOBURL;?>product/viewcart";
       }
      <? /*jQuery.ajax({
          type:'POST',
          url:'<?php echo SITEMOBURL; ?>product/buynow',
          dataType:'json',
          data:{module}
       }).done(function(data){
        //console.log(data);
          if(data.error==true){
              bootbox.hideAll();
              jQuery('#login').modal('show');
          }else{
            window.location.href="<?php echo SITEMOBURL;?>product/viewcart";
          }
       })
   }else{
       bootbox.alert('This product is not available currently, try again later');
   }*/?>
 }
}

function addtocart(ele, elementobj=false, module=false){
  //alert('hii');
  jQuery('.catallpro').unbind('click');
  proid=ele;
  removewishlist=jQuery(elementobj).data('removewishlist');
  //alert(proid);
  sid =jQuery(elementobj).data('sid');
  if(sid){
    sid=sid;
  }else{
    sid=0;
  }
  jQuery.ajax({
    url:'<?php echo SITEMOBURL?>product/storeproid',
    type:'POST',
    data:{proid:proid,sid:sid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
          if(module=='buynow'){
              checkviewcartadded=true;
              return true;
          }
          bootbox.alert('Product successfully added to cart', function(){
            if(removewishlist){
              removefromwishlist(proid, removewishlist);
            }
          });

           //return false;
          // window.location.reload();
           ajax_getCartCount();
           ajax_wishlistcount();
        }else{
         bootbox.alert('Error while adding product to cart');

        }

    }

  });

}


function addtowishlist(ele, wishlistobj=false ){
  jQuery('.catallpro').unbind('click');
  pid=ele;
  addtowishlist=jQuery(wishlistobj).data('addtowishlist');
  jQuery.ajax({
    url:'<?php echo SITEMOBURL?>user/wishlist',
    type:'POST',
    data:{pid:pid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
           bootbox.alert('Product successfully added to shortlist', function(){
            window.location.reload();
           });
           if(addtowishlist){
           removefromcart(pid, addtowishlist);
         }
          // window.location.reload();
            ajax_getCartCount();
           ajax_wishlistcount();
        }else if(response.login=='false'){
           bootbox.alert('You need to login to create Shortlist',  function(){
              jQuery('#login').modal('show');
           });
        }else{
         bootbox.alert('Error while adding product to Shortlist');

        }

    }

  });
}
function ajax_wishlistcount(){
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>user/getwishlistcount',
           type:'POST'

        }).done(function(resp){
          if(resp){
          var response = jQuery.parseJSON(resp);
           // bootbox.alert(response.cnt);
            if(response.error==false)
            {
             if(response.cnt > 0){
                 jQuery('#wishlistcount').html(response.cnt);
             }
            }
            else
            {
                if(response.cnt > 0){
                    jQuery('#wishlistcount').html(response.cnt);
              }
            }
         }
          return false;
      });
}
 jQuery('#submitsearch').click(function(){
          var search=jQuery('#search').val();

          jQuery.ajax({
              url:"<?php echo SITEMOBURL?>product/search",
              type:'POST',

              data:{search:search},
              success:function(result)
              {
               // window.location.href="<?php echo SITEMOBURL?>home/search1";
              }

          });
          return false;
        });
function opentab (id) {
  jQuery('.sn-tab').removeClass('active');
  jQuery('.actclass'+id).addClass("active");
  jQuery('.sn-tab-content').removeClass('active');
  jQuery('.snt-shop'+id).addClass("active");

}
    jQuery(document).ajaxStart(function(){
      jQuery(".overlay").show();
     });

    jQuery(document).ajaxComplete(function(){
       jQuery(".overlay").hide();
    });
jQuery('#submit11').click(function(){
    var search=jQuery('#search').val();
    //bootbox.alert(search);
    jQuery.ajax({
        url:"<?php echo SITEMOBURL?>product/search",
        type:'POST',

        data:{search:search},
        success:function(result)
        {
         // window.location.href="<?php echo SITEMOBURL?>home/search1";
        }

    });
    return false;
  });
  function isNumber(evt) {
        //alert('nbjj');
       //console.log(evt);
    evt = (evt) ? evt : window.event;
    console.log(evt);
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)  || (charCode >= 48 && charCode <= 57) || charCode == 32) {
      //alert('great');
        return true;
    }
    evt.preventDefault();
    return false;
}
function ajax_getCartCount(){
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>product/getcartcount',
           type:'POST',

        }).done(function(resp){
         if(jQuery.trim(resp)){
         // console.log(resp);
          var response = jQuery.parseJSON(resp);
            if(response.status==1)
            {
             if(response.cartcount > 0){
                   jQuery('#currentitemcount').html(response.cartcount);
                 }

            }
            else
            {
                  if(response.cartcount > 0){
                     jQuery('#currentitemcount').html(response.cartcount);
                   }
            }
         }
          return false;
      });
}
function updatecart(ele){
   var pid=ele;
    var qty= jQuery('#qtychange_'+ele).val();
    //alert(qty); return false;
    jQuery.ajax({
      url:'<?php echo SITEMOBURL?>product/updatecart',
      type:'POST',
      data:{qty:qty,pid:pid},
      success:function(data)
      {   var response = jQuery.parseJSON(data);

         if(response.error=='false'){
              bootbox.alert('Cart updated successfully', function(){
                window.location.reload();
              });
              //window.location.reload();
            }else{
             bootbox.alert('Error in updating  product to cart');
            }

      }

   });
}
/*function updatecombocart(ele) {
   var pid=ele;
    var qty= jQuery('#qtychange_'+ele).val();
    //alert(qty); return false;
    jQuery.ajax({
      url:'<?php// echo SITEMOBURL?>product/updatecart',
      type:'POST',
      data:{qty:qty,pid:pid},
      success:function(data)
      {   var response = jQuery.parseJSON(data);
          console.log(response);
         if(response.error=='false'){
              bootbox.alert('Cart updated successfully', function(){
                //str+='<i class="fa fa-rupee" style="font-size:14px"></i>'+response.user.amount;
                jQuery('#updateprice').replaceWith('<i class="fa fa-rupee" style="font-size:14px"></i>'+response.user.amount);
              });

              //window.location.reload();
            }else{
             bootbox.alert('Error in updating  product to cart');
            }

      }

   });
}*/
function removefromcart(ele, removetocart=false){

   var pid=ele;

    jQuery.ajax({
      url:'<?php echo SITEMOBURL?>product/removefromcart',
      type:'POST',
      data:{pid:pid},
      success:function(data)
      {   var response = jQuery.parseJSON(data);

         if(!removetocart){
         if(response.error=='false'){
              bootbox.alert('Product removed successfully', function(){
             window.location.reload();
           });
              //window.location.reload();
            }else{
             bootbox.alert('Error in removing  product from cart');
            }
        }
      }

   });
}


function removefromcombocart(proid, comboproid){
   var pid=proid;
   var comboproid=comboproid;
    jQuery.ajax({
      url:'<?php echo SITEMOBURL?>product/removefromcombocart',
      type:'POST',
      data:{pid:pid, combo_id:comboproid},
      success:function(data)
      {   var response = jQuery.parseJSON(data);

         if(response.error=='false'){
              alert('Product removed successfully');
              window.location.reload();
            }else{
             alert('Error in removing  product from cart');
            }

      }

   });


}

function getbundlecart(){

   jQuery.ajax({
        url:"<?php echo SITEMOBURL?>product/viewbundlecart",
        type:'POST',
        success:function(result)
        {

      jQuery('#profile').html(result);

        }

    });
    return false;

}
function ajax_wishlistcount(){
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>user/getwishlistcount',
           type:'POST'

        }).done(function(resp){
          if(resp){
          var response = jQuery.parseJSON(resp);
           // bootbox.alert(response.cnt);
            if(response.error==false)
            {
             if(response.cnt > 0){
                 jQuery('#wishlistcount').html(response.cnt);
             }
            }
            else
            {
                if(response.cnt > 0){
                    jQuery('#wishlistcount').html(response.cnt);
              }
            }
         }
          return false;
      });
}
jQuery(document).ready(function(){

jQuery('img').error(function(){
  console.log('error image');
    jQuery(this).attr('src', '<?php echo SITEMOBURL?>beta/assets/images/mobimg/noimage1.png');
}); 
//check if the user comes from change password
setTimeout(function(){
  headerelementset=jQuery('header').data('flashmessage');
  if(headerelementset==true){
    jQuery('#login').modal('show');
  }
}, 3000)
 
var name='<?echo $name;?>'  ;
if(name){
 jQuery('#mainloginicon').attr('style', 'width:137px'); 
}else{
  jQuery('#mainloginicon').attr('style', 'width:143px'); 
}
    ajax_getCartCount();
    ajax_wishlistcount();
  jQuery('#loginform').validate({
        rules:{
            email:{
                email:true,
                required:true
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            },
            password:{
                required:"Please enter Password"
            }
        },
    });

    jQuery('#loginsubmit').click(function() {
      //alert('hii');
      jQuery('#loginsubmit').children('img').show();
      jQuery('#loginsubmit').attr('disabled',true);
        var success=jQuery("#loginform").valid();
        //alert(success);
             if(success){
        var backurl=document.referrer;
                jQuery.ajax({
                    url:'<?php echo SITEMOBURL?>user/userlogin',
                    type:'POST',
                    dataType:'JSON',
                    data:jQuery('#loginform').serialize(),
                    success:function(result){
                        res=eval(result);
                        console.log(res);
                        jQuery('#loginsubmit').children('img').hide();
                        jQuery('#loginsubmit').attr('disabled',false);
        
                        if(res.status == true)
                        {
                         if(backurl=='<?php echo SITEMOBURL?>product/buynow' || backurl=='<?php echo SITEMOBURL?>product/checkoutuserdtl'){
                         window.location.href="<?php echo SITEMOBURL?>product/viewcart";
                         }else{
               //alert()
                         jQuery('#mainloginicon').attr('style', 'width:137px');  
                          window.location.reload();
                          //window.location.href="<?php echo SITEMOBURL?>";
                         }
                        }else{
                          jQuery('#login').hide();
                          bootbox.alert(res.msg,function(){
                            jQuery(".input").val(""); 
                            jQuery('#login').show(); 
                          });
                        }
                    }
                });
            }
        });

    });
function Signup(){
  jQuery('#login').hide();
jQuery(document).ready(function(){

    jQuery('#registerform').validate({
        rules:{
            uname:{
                required:true,
        minlength:2,
                maxlength: 30
            },
            remail:{
                email:true,
                required:true
            },
            rpassword:{
                required:true
            },
            cpassword:{
                 equalTo:"#rpassword"
            },
            state:{
                 required:true
            },
            city:{
                 required:true
            },
            phoneno:{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            address1:{
                 required:true
            }
        },
        messages:{
            uname:{
                required:"Please enter Name",
        minlength:"Name should be less than 2 character",
        
            },
            remail:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            },
            password:{
                required:"Please enter Password"
            },
            cpassword:{
                equalTo:"Confirm password must match with Password"
            },
            state:{
                required:"Please enter your State"
            },
            city:{
                required:"Please enter your City"
            },
            phoneno:{
                required:"Please enter your Phone No"
            },
            address1:{
                required:"Please enter your Address"
            }
        },
    });

    jQuery('#registeruser').click(function(){
             
             var success=jQuery("#registerform").valid();
             if(success){
                    jQuery('#loader').show();
                    jQuery('#registeruser').attr('disabled',true);
                    name=jQuery('#uname').val();
                    email=jQuery('#remail').val();
                    password=jQuery('#rpassword').val();
                    state=jQuery('#signupstate option:selected').val();
                    city=jQuery('#signupcity option:selected').val();
                    phoneno=jQuery('#phoneno').val();
                    address1=jQuery('#address1').val();
                    // address2=jQuery('#address2').val();

                jQuery.ajax({
                     url:'<?php echo SITEMOBURL?>user/usersignup',
                    type:'POST',
                    dataType:'json',
                    data:{name:name,email:email,password:password,state:state,city:city,phoneno:phoneno,address1:address1},
                    success:function(result){
                        jQuery('#loader').hide();
                        jQuery('#registeruser').attr('disabled',false);
                         if(result.error=='false'){
                             jQuery('#signup').hide();
                             bootbox.alert('user registered successfully' ,function(){
                              jQuery('#login').show();
                             });
                        }else if(result.error=='true'){
                             bootbox.alert(result.message);
                        }else if(result.error=='exits'){

                            bootbox.alert('EmailId Already Exits',function(){
                              jQuery('#remail').val('');
                            });
                        }
                    }
                });
            }
        });

    });
jQuery('#signup').on('hidden.bs.modal', function () {
  //bootbox.alert('fhii');
  jQuery('.modal-backdrop').hide();
})
}

//Fancy Scroll
jQuery('#scrollbar').scrollbar();

// var nodata=true;
   
//    var scrollval=800;
//    jQuery(document).ready(function () {
//     jQuery('body').attr('data-scrollload', <?php echo $start; ?>);
//       jQuery(window).scroll(function () {
      
//           if (jQuery(this).scrollTop() > scrollval) {
//         //nodata=true;
//               jQuery('.scrollup').fadeIn();
//               scrollval+=500;
//               if(nodata==true){
           
//                showproducts();
//          }
//           } else {
//               jQuery('.scrollup').fadeOut();
//          }
//       });
//    })  
 // function showproducts(){
 //     ///start=<?php echo $start; ?>;
 //     startlast=jQuery('#appendid').last().data('scrollload');
 //     start=jQuery('body').attr('data-scrollload');
 //     //alert(start);
 //     jQuery(".preloader").show();
 //     jQuery('#listdata').append('<span class="preloader col-md-12" style="text-align: center;"><img src="<?echo WEBIMG;?>ajax-loader.gif" style="width: 40px;"></span>');
 //     var url= '<?//echo //$_SERVER['REQUEST_URI'] ;?>';
 //     jQuery.ajax({
 //     type:"post",
 //     url:url,
 //     data:{start:start},
 //     success: function(data){
 //       console.log(data); //return false;
 //       if(data){
 //         jQuery(".preloader").remove();
 //         if(startlast==start){
 //           nodata=false;
 //           jQuery('#listdata').append('No more records to show ');
 //           return false;
 //         }else{
 //           jQuery('#listdata').append(data);
 //         }
 //     }
 //   }
 //   });
 //   }
//scroll function:End

function test(){
var f1 = document.getElementById("twotabsearchtextbox");
var f2 = document.getElementById("twotabsearchtextbox1");
f2.value = f1.value;
}
function test1(){
var f1 = document.getElementById("twotabsearchtextbox");
var f2 = document.getElementById("twotabsearchtextbox1");
f1.value = f2.value;
}
function test3(){
var f1 = document.getElementById("select_drop");
var f2 = document.getElementById("select_drop1");
f2.value = f1.value;
}
function test4(){
var f1 = document.getElementById("select_drop");
var f2 = document.getElementById("select_drop1");
f1.value = f2.value;
}


</script>
<script>
jQuery(document).ready(function(){
   curURL=jQuery(location).attr('href');
      //urlid = ele;
   console.log(curURL); 
 var $loading = jQuery('#loading').hide();
                   //Attach the event handler to any element
                  
  
  
  
})

          
jQuery( document ).ajaxStart(function() {
  jQuery('#loading').show();
});

jQuery( document ).ajaxStop(function() {
  jQuery('#loading').hide();
});

//filter left sidebar and sort
function filterall(){
    var searchtext='<?php echo $_GET['searchText']?>';
    var appendquerystr='';
    if(searchtext){
        appendquerystr='?searchText='+searchtext;
    }else{
      productsearch='';
    }
    //console.log(appendquerystr);
     curURI=window.location.pathname+appendquerystr;
      //console.log(curURI); return false;
      //alert(curURI);
     var jstr = '';
     var jstr1='';
     jQuery(".attribute:checked").each(function(){
       if(jstr==''){
         jstr += '[' + jQuery(this).val();
       }else{
         jstr += ',' + jQuery(this).val();
       }
     });
       jQuery(".attributebrand:checked").each(function() {
         if(jstr==''){
           jstr += '&brand=[' + jQuery(this).val();
         }else{
           jstr += ',' + jQuery(this).val();
         }
     });
        jQuery(".attributepercentage:checked").each(function() {
       if(jstr==''){
         jstr += '&percentage=[' + jQuery(this).val();
       }else{
         jstr += ',' + jQuery(this).val();
       }
     });
       jQuery(".attributeavaliable:checked").each(function() {

       if(jstr1==''){
         jstr1 += '&avaliable=[' + jQuery(this).val();
       }else{
         jstr1 += ',' + jQuery(this).val();
       }

     });
     if(jstr!=''){ jstr += ']'; }
     if(jstr1!=''){  jstr1 += ']';}

     //console.log(min+'===='+max);
     if(min && max){
        var min = jQuery( "#slider-range" ).slider( "values", 0 );
        var max = jQuery( "#slider-range" ).slider( "values", 1 );
       jstr += '&pricestart='+min;
       jstr += '&pricend='+max;
     }
     <?php //$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));?>
      //console.log(jstr1); return false;
     var sort = $( "#prosuctsort_opt option:selected").val();
     if(sort==undefined){sort = 'popular';}
     //console.log('jstr-----'+jstr);
     //console.log('jstr1-----'+jstr1); return false;
     if(offer == 'promotion'){
        //console.log(<?php echo $_GET['id'];?>); return false; 
         window.location.href=promourl+'&filter='+jstr+jstr1+"&sort="+sort;
     }else{
        if(jstr && jstr1){
            window.location.href=curURI+'?filter='+jstr+jstr1+"&sort="+sort;
           }else if(jstr){
             window.location.href=curURI+'?filter='+jstr+"&sort="+sort;
            }else if(jstr1){
             window.location.href=curURI+'?filter='+jstr1+"&sort="+sort;
           }else{
                window.location.href=curURI+"?sort="+sort;   
           }
      }
    }
 
jQuery(document).mouseup(function (e){
    var container = jQuery("#suggestions");

    if (!container.is(e.target)
    && container.has(e.target).length === 0)
    {
    container.hide();
    }
});
</script>