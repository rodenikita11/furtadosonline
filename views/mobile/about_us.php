<div class="top-logo">
<div class="container" style="padding:0;">
<div class="row">
<div class="col-md-12">

<div class="col-xs-2">
<a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>

</div>
<div class="col-xs-7 Favourite_list">About Us</div>

<div class="col-xs-3"style="padding:0;">
  
 </div>
	</div>
</div>
</div>
</div>


<div class="Container container-fluid about-us">
<div class="row">
<div class="col-xs-12">

<i><b>'Think Music, Think Furtados' </b> is an apt description for the music house of Furtados. Established in 1865, Furtados has always been the vanguard in the field of Western music education and knowledge in India. From print music to digital equipment, having a complete spectrum of musical instruments in the Western and Indian categories, Furtados has evolved into a one-stop shop for the musicoriented. Be it the beginner-student, the tutor or the virtuoso. </i><br/><br/>
<p>
At the core of Furtados' business motto is 'Trusted Service'. In a pan-India customer survey conducted in the metros and other major cities, three factors characterised Furtados' services – Quality, Reliability and Trust. 
<br/><br/>
Furtados has grown into a household name in music in Mumbai, Pune, Delhi, Punjab, Bangalore, Ahmedabad, Mangalore, Dimapur and Goa, and is a familiar name to almost all musicians across the country. Its growing stature in the field has seen the organisation spread its roots throughout the country, through a growing retail network and over 350 dealer outlets. Furtados has re-invented itself from a family-managed to a professionally-managed business enterprise, to meet the challenges and demands of the times. <br/><br/>

In its changing personality, Furtados has progressively widened its range and scope of services, and presently holds a record number of international dealerships for top-of-the-line brands in music. Instruments are imported from all across the globe including the USA, Canada, the UK, Germany, the Netherlands, Denmark, Italy, France, Japan, Korea, Taiwan, Indonesia, China, Malaysia, Vietnam, Hong Kong and Sri Lanka. Music books are imported from virtually every major music publisher. <br/><br/>

Furtados has formed a joint venture, Furtados Music (India) Pvt Ltd, with another giant in the music industry, Musee Musical Pvt Ltd, which has a strong retail, distribution and teaching presence in southern India. The joint venture is focused on expanding Furtados' retail presence across India, with state-of-the-art music showrooms, concept stores and other retail offerings. <br/><br/>

Since 2007, the Furtados net has spread far and wide. There has been a flurry of new showroom openings across the length and breadth of the country. As of May 2014, the Furtados network totals 20 showrooms. <br/><br/>

Furtados has kept pace with technology, in all its business endeavours. A full-scale ERP (Enterprise Resource Planning) with a front end retail POS has been implemented across all showrooms and all operations. Furtados is very proud to have taken this momentous step – marching into the digital world, to emerge as the country's first and only fully-computerised music provider. <br/><br/>

www.furtadosonline.com is the quintessential MI e-commerce site for India, and is already emerging as a watering hole for both dealers and retail customers. <br/><br/>

In January 2011, Furtados launched a publishing joint venture with one of the world's leading music book publishers, Music Sales Ltd, UK. The new company, Music Sales Furtados (India) Pvt Ltd, is focused on publishing Indian editions of best-selling Music Sales books, especially those dealing with education (tutors, technique books, etc), as also on developing local content by identifying and bringing into print music composers of Indian origin residing in India. <br/><br/>

In its 149 years of existence, Furtados has left a trail of not just satisfied but delighted customers. And this is possible because of its progressive and professional management that drives a team of committed employees, numbering over 200 country-wide. The management and sales team at Furtados truly believe in the new positioning, that they exist for the music-lover, at every step of their musical journey. The management is a fine blend of youth and experience, with drive and enthusiasm. Not only does it make available world-class products at affordable prices but also proactively invests in the development of musical knowledge through teacher-training workshops, master classes, clinics, demonstrations and exhibitions, and the sponsoring of local talent and artistes. <br/><br/>

In one such manifestation, on 17th April, 2011, Furtados inaugurated a brand new Steinway Model D concert grand piano which it has acquired for concert/event rentals in Mumbai and Western India, in a gala concert performance by Steinway Artist Yuja Wang. This is the first instance in 40 years that a new Steinway concert grand has been acquired in India for concerts and performances.<br/>


</p>
</div>
 </div>
 </div>
 <!-------2 img--------------->
 
<?php include('include/footer.php');?>
