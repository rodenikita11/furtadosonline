<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('Libsession');
		$this->load->library('Lib_storeuri'); 
		$this->load->library('Feildvalidation');
		$this->load->library('Liburl');
		$this->load->library('Libmenu'); 
		$this->load->library('Libprodtl');
		$this->load->library('Libpromooffer'); 
		$this->load->library('Lib_home');
		$this->load->library('Libcart'); 
		$this->load->library('Liblogin');
		$this->load->library('Libsearch');
		$this->load->library('Libwishlist');
		$this->load->library('Liborder'); 
		$this->load->library('Libshippingcost');
		$this->load->library('Libmailer');
		$this->load->library('Giftcoupon_validator');
	}
}
?>
