<div class="col-xs-3">
	<div class="my-accounts-main-heading-s">My Accounts</div>
<!--row-->
    <div class="my-accounts-sub-heading-s">ORDER</div>
    <div class="my-accounts-ul">
    	<ul>
        	<li><a href="<?php echo site_url('customer/mainorder');?>">My Order</a></li>
			<li><a href="<?php echo site_url('customer/refundProducts');?>">Refund tracker</a></li>
			<li><a href="#">E-Gift Voucher</a></li>
			<li><a href="<?php echo site_url('customer/my_coins');?>">My Coins</a></li>

        </ul>
    </div>
<!--end row-->

<!--row-->
    <div class="my-accounts-sub-heading-s">MY STUFF</div>
    <div class="my-accounts-ul">
    	<ul>
        	<li><a href="<?php echo site_url('customer/wishlist');?>">My Whishlist</a></li>
			<li><a href="<?php echo site_url('offer/showmygiftcard');?>">My Gift Card</a></li>
			<li><a href="<?php echo site_url('offer/showmypoints');?>">My reedem points</a></li>
        </ul>
    </div>
<!--end row-->

<!--row-->
    <div class="my-accounts-sub-heading-s">Settings</div>
    <div class="my-accounts-ul">
    	<ul>
        	<li><a href="<?php echo site_url('customer/profileSetting');?>">Personal Information</a></li>
            <li><a href="<?php echo site_url('customer/showpassword');?>">Change Password</a></li>
            <li><a href="#">Addresses</a></li>
        	<li><a href="#">Profile Settings</a></li>
            <li><a href="#">Update Email</a></li>
            <li><a href="#">Deactivate Account</a></li>
        	<li><a href="#">Personal Information</a></li>
            <li><a href="#">Notifications</a></li>
            <li><a href="#">Email Preferences</a></li>
        </ul>
    </div>
<!--end row-->
</div>