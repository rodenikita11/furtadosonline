<style type="text/css">
    .head {
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    color: #ffffff;
    font-size: 16px;
    font-family: 'Open Sans', sans-serif;
    padding: 12px 17px;
    text-transform: capitalize;
    background-color: #cc0028;
    border: 1px solid #cc0028;
    font-weight: 700;
    letter-spacing: 0.5px;
    border-bottom: 1px #cc0028 solid;
    border-radius: 2px;
    }
    .cart .action {
        margin-left: 30px ! important;  
    }
    .product .cart{
        margin-left: -58px;
    }
    div#slider-range {
    width: 70%;
    margin: 10px 24px 21px;
    }
    .pricepd{
    padding: 0px 0px 3px;
    }
    .pdr{
    padding: 5px 12px;
    }
    .hot-deals .custom-carousel .owl-controls .owl-prev {
    top: -10px;
    }
    .custom-carousel .owl-controls .owl-prev {
    position: absolute;
    width: 30px;
    height: 30px;
    left: 233px;
    }
    .hot-deals .custom-carousel .owl-controls .owl-next {
    top: -10px;
    }
    .custom-carousel .owl-controls .owl-next {
    position: absolute;
    width: 30px;
    height: 30px;
    right: -4px;
    }
    #discount{
    overflow: auto !important;
    height: auto !important; 
    min-height: auto !important;
    }
    .nano { background: #bba; width: 500px; height: 500px; }
    .nano .nano-content { padding: 10px; }
    .nano .nano-pane   { background: #888; }
    .nano .nano-slider { background: #111; }
</style>
<? //print '<pre>'; print_r($filter); print_R($filter1);?>
<?$brands=[]; foreach($filter['brand'] as $key => $val){ ?>
    
        <?if(in_array($val['brandid'],$param['branddtl'])){
          $brands[] = $val['brandid'];
        }else{
          $selected='';
        }?>
<?} $brands = implode(',',$brands); ?>
  <div class="side-menu animate-dropdown outer-bottom-xs" id="subcate" >
    <div class="head"><i class="icon  fa-fw"></i> Category
    <? if(count($_GET['categoryid'])>0){?>
      <span class="pull-right"><a href="javascript:void(0)" onclick="clearcategory();" style="font-size: 12px;color: #fff ! important;padding: 5px;" >Clear </a></span>
    <?}?></div>
    <nav class="yamm megamenu-horizontal">
      <ul class="nav " style="max-height: auto !important">
        <?if($filter['categorydtl']){
          ?>
       
          <?php foreach($filter['categorydtl'] as $key => $val){?>
             
              <li class="dropdown menu-item"> <a href="<?php echo base_url();?>product/<?php echo $current_path ?>?categoryid=<?php echo $val['categoryid']?>&filter=&pricestart=<?echo $_GET['pricestart'] > 1 ? $_GET['pricestart'] : $filter['minprice']?>&pricend=<?php echo $_GET['pricend'] > 1 ? $_GET['pricend'] : $filter['maxprice'] ?>&available=&sort=<? echo $_GET['sort']?>&brand=<?php if(!empty($brands)){ ?>[<? echo $brands; ?>] <?php } ?>" ><?php echo $val['category'];?>(<?php echo $val['productcount'];?>)</a></li>
          <?}?>
         
        <?} else {?>
          <?php foreach($filter['categorydtl'] as $key => $val){?>
              <li class="dropdown menu-item"> <a href="<?php echo $val['caturl']?> 
                                                       &filter=
                                                       &pricestart=<?echo $_GET['pricestart'] > 1 ? $_GET['pricestart'] : $filter['minprice']?>
                                                       &pricend=<?php echo $_GET['pricend'] > 1 ? $_GET['pricend'] : $filter['maxprice'] ?>
                                                       &available=
                                                       &sort=<? echo $_GET['sort']?>
                                                       &brand=[<?php echo $param['branddtl']; ?>]
                                                      " ><?php echo $val['category'];?>(<?php echo $val['productcount'];?>)</a></li>
          <?}?>
        <?}?>
      </ul>
       <!-- /.nav -->
    </nav>
    <!-- /.megamenu-horizontal -->
  </div>
  
  <!--====================================================================================================-->

  <div class="sidebar-module-container">
      <div class="sidebar-filter">
           <div class="side-menu animate-dropdown outer-bottom-xs">
              <div class="head" id="scrollbar"><i class="icon  fa-fw"></i>Browse By Brands
              <? if(count($param['branddtl'])>0){?>
                
               <span class="pull-right"><a href="javascript:void(0)" onclick="clearbrand();" style="font-size: 12px;color: #fff ! important;padding: 5px;" >Clear </a></span>
               <?}?>
              </div>
              <input type="text" class="form-control" style="width: 100% !important;padding: 6px 17px !important;font-size: 14px;" onkeyup="find()" name="brandsearch" id="brandsearch" name="x" placeholder="Search Brands...">
              <nav class="yamm megamenu-horizontal">
                 <ul class="nav overflow " id='bname'>
                    <?foreach($filter['brand'] as $key => $val){ ?>
                      <li class="dropdown menu-item " data-id="<?echo strtolower($val['brandname']);?>" id='bnameinner'>
                        <a href="#" id="getfilter"   data-id="<?echo $val['brandid'];?>">
                          <?if(in_array($val['brandid'],$param['branddtl'])){
                            $selected="checked";
                          }else{
                            $selected='';
                          }?>
                          <label for="checkbox-2" class="checkbox-custom-label"></label>
                          <input id="checkbox-<?echo $val['brandid']?>" class="checkbox-custom attributebrand" onchange="filterall();" name="checkbox-2" type="checkbox" value="<?echo $val['brandid']?>" <?echo $selected;?>>
                          <?echo $val['brandname'];?>(<?echo $val['brandcount']; ?>)
                        </a>
                      </li>
                    <?}?>
                    <!-- /.menu-item -->
                 </ul>
                 <!-- /.nav -->
              </nav>
              <!-- /.megamenu-horizontal -->
           </div>
           <!-- /.side-menu -->
           <!-- ================================== TOP NAVIGATION : END ================================== -->
       
      
           <!-- ================================== AVAILABILITY ================================== -->
           <?//$avalibalenot=count($getfilter['avalibale'])>0 ? 'block' : 'none' ?>
            <div class="side-menu animate-dropdown outer-bottom-xs" >
              <div class="head"><i class="icon  fa-fw"></i>Availability
                <? if(count($param['avaliable'])>0){
                  ?>
                   <span class="pull-right"><a href="javascript:void(0)" onclick="clearavail();" style="font-size: 12px;color: #fff ! important;padding: 5px;" >Clear </a></span>
                <?} ;?>
                
              </div>
              <nav class="yamm megamenu-horizontal">
                <ul class="nav ">
             <?php // var_dump($filter['available']);die; ?>
                  <?if(!$filter){?>
                    <? if(in_array(1,$param['avaliable'])){
                      $selected1="checked";}else{
                      $selected1="";
                    }?>
                    <? if(in_array(0,$param['avaliable'])){
                      $selected2="checked";}else{
                      $selected2="";
                    }?>  
                    <!-- <?if(empty($param['avaliable'])){
                      $selected1="checked";
                      $selected2="checked";
                    }?>  -->
                    <?if($filter['available'][0]['clearanceid'] == 0){
                       
                     
                      $clearancename1 = 'Available';
                    } else {
                      $clearancename1 = 'Available';
                    }
                    if ($filter['available'][1]['clearanceid'] == 1) {
                      $clearancename2 = 'Not Available';
                    } else {
                      $clearancename2 = 'Not Available';
                    }?>
                  <?} else {
                    
                    ?>
                    <? if(in_array(1,$param['avaliable'])){
                      $selected1="checked";}else{
                      $selected1="";
                    }?>
                    <? if(in_array(0,$param['avaliable'])){
                      $selected2="checked";}else{
                      $selected2="";
                    }?>  
                    <!-- <?if(empty($param['avaliable'])){
                      $selected1="checked";
                      $selected2="checked";
                    }?> --> 
                    <?if($filter['available'][1]['offerid'] == 1 ){
                      $clearancename1 = 'Available';
                    } else {
                      $clearancename1 = 'Available';
                    }
                    if ($filter['available'][0]['offerid'] == 0) {
                      $clearancename2 = 'Not Available';
                    } else {
                      $clearancename2 = 'Not Available';
                    }?>
                  <?}?>

                  <?php //aaina
                  foreach($filter['available'] as $avl){
                  //var_dump($avl['clearanceid']);die;

                  if($avl['clearanceid'] == 1){

                    $x = $avl['count'];
                  //  var_dump($x);die;
                  }
                  else{
                    $y = $avl['count'];
                   // var_dump($y);die;
                  }
                }
                ?>

                  <li class="dropdown menu-item"> <a href="#" id="getfilter"  >
                    <input id="checkbox-1" class="checkbox-custom attributeavaliable" name="checkbox-3" onchange="filterall();" type="checkbox" value="1" 
                      <?echo $selected1;?> >
                      <?php //var_dump($filter['available']);die; ?>
                    <label for="checkbox-3" class="checkbox-custom-label"></label>
                    <?php echo $clearancename1;?>(<?echo $x ? $x : 0 ; ?>)</a>
                  </li>
                  <li class="drop//down menu-item"> <a href="#" id="getfilter"  >
                    <input id="checkbox-1" class="checkbox-custom attributeavaliable" name="checkbox-3" onchange="filterall();" type="checkbox" value="0" 
                      <?echo $selected2;?> >
                    <label for="checkbox-3" class="checkbox-custom-label"></label>
                    <?php echo $clearancename2;?>(<?echo $y ? $y : 0 ;?>)</a>
                  </li>
                </ul>
              </nav>
            </div>
           <!-- /.side-menu -->
           <!-- ================================== AVAILABILITY : END ================================== -->
           <!-- ================================== PRICE SLIDER ================================== -->
          <div class="side-menu animate-dropdown outer-bottom-xs pricepd">
              <div class="head"><i class="icon  fa-fw"></i>Price Range
            
               <? if(count($param['minprice'])>0){
                 
                ?>
                    <span class="pull-right"><a href="javascript:void(0)" onclick="clearprice1();" style="font-size: 12px;color: #fff ! important;" >Clear </a></span>
               <?}    ?></div>
              <input type="text" id="amount" readonly style="border:0; color:#343333; font-weight:bold;margin: 9px 62px 0px; font-size: 14px;">
              <div id="slider-range"><button onclick='filterall();' class="gobtn "> GO </button></div>
              <!-- /.megamenu-horizontal -->
           </div>
          <?/*php $discount=$res['percentage']!=''  ? 'block' : 'none' ?>
          <div class="side-menu animate-dropdown outer-bottom-xs" style="display: <?php echo $discount;?>">
              <div class="head" id="scrollbar"><i class="icon  fa-fw"></i>Discounts
                  <? if(count($param['discountdetail'])>0){?>
                    <span class="pull-right"><a href="javascript:void(0)" onclick="cleardiscount();" style="font-size: 12px;color: #fff;" >Clear </a></span>
                  <?}?>
              </div>
              <nav class="yamm megamenu-horizontal">
                 <ul class="nav overflow" id="discount">
                  <?php //print '<pre>'; print_r($param['discountdetail']);?>
                    <?foreach($res['percentage'] as $key => $val){ ?>
                    <li class="dropdown menu-item">
                       <a href="#" id="getfilter"   data-id="<?echo $key;?>">
                        <?php if($param['discountdetail']==$key){
                          $selected="checked";}else{
                          $selected='';
                          }?>
                      
                       <input id="checkbox-<?echo $key;?>" class="checkbox-custom attrbrand"  name="checkbox-2" type="checkbox" data-id="<?echo $key;?>" value="<?echo $key;?>" <?echo $selected;?>>
                       <?echo $val;?></a>
                    </li>
                    <?}?>
                    <!-- /.menu-item -->
                 </ul>
                 <!-- /.nav -->
              </nav>
              <!-- /.megamenu-horizontal -->
           </div*/?> 
      </div>
  </div>

<!--====================================================================================================-->

<!-- ============================================== HOT DEALS ============================================== -->
<!---<div class="sidebar-widget hot-deals wow fadeInUp " style="padding: 0px;">
<h3 class="section-title head"><i class="icon  fa-fw"></i>Other Hot Deals</h3>
  <? foreach($banner_category['hotdeal_banner'] as $hotdeal_banners ){?>
     <div class="item">
        <div class="products">
           <div class="hot-deal-wrapper">
              <div class="image">
                <a href="<?php echo $hotdeal_banners['url'];?>" target="_blank"> 
                 <img  <?if(isset($hotdeal_banners['imgpath'])){?> src="<?php echo $hotdeal_banners['imgpath'];?>" <?}else{?> src="<?php echo WEBIMG?>banners/Noimage (1).jpg" <?}?> alt=""></a>
              </div>
           </div>
           /.hot-deal-wrapper 
        </div>
     </div>
     <?}?>
</div>--->
<!-- ============================================== HOT DEALS: END ============================================== -->
<script>


   jQuery( function() {
     jQuery( "#slider-range" ).slider({
       range: true,
       min: <?echo $filter['minprice'];?>,
       max: <?php echo $filter['maxprice']?>,
       values: [ <?echo $_GET['pricestart'] > 1 ? $_GET['pricestart'] : $filter['minprice']?>, <?php echo $_GET['pricend'] > 1 ? $_GET['pricend'] : $filter['maxprice'] ?> ],
       slide: function( event, ui ) {
         var amount= jQuery( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
         //console.log(amount);
       }
     });
     jQuery( "#amount" ).val( "Rs." + jQuery( "#slider-range" ).slider( "values", 0 ) +
       " - Rs." + jQuery( "#slider-range" ).slider( "values", 1 ) );
   } );
  

   jQuery(document).ready(function () {
     function subcat(){
       //jQuery('#subcate').hide();
     }

     <? if($_GET['sort']){ ?>
       jQuery("#prosuctsort_opt").val('<? echo $_GET['sort']?>')
     <? } ?>
     jQuery(".nano").nanoScroller();
   });

  function clearbrand(){
    jQuery('.attributebrand').prop("checked", false);
    filterall();
  }
 function cleardiscount(){
    jQuery('.attrbrand').prop("checked", false);
    filterall(); 
   
 }
  
   jQuery('.attrbrand').click(function() {
    //var url='furtadosv2.lumeg.in';
    curURI=window.location.pathname;
    window.location.href=curURI+'?discount='+jQuery(this).val();
     
  });
  
  function clearavail(){
    jQuery('.attributeavaliable').prop("checked", false);
    filterall();
  }

  function clearprice1(){

    console.log(window.location.search);
    let urlParams = new URLSearchParams(window.location.search);
 
    let avl = urlParams.get('avaliable');
     console.log(avl);
    urlParams.delete('pricestart');
    urlParams.delete('pricend');
    urlParams.delete('avaliable');

    console.log(urlParams);
   let curURI=window.location.pathname;
   // alert(curURI);
   window.location.href=curURI+'?'+urlParams.toString()+'&avaliable='+avl;

    
  }
  function clearcategory(){
    // console.log('hello aaina');
    let curURI=window.location.pathname;
    window.location.href=curURI
   // alert(window.location.href);
  }

 function find(){
    var brandsearch1=jQuery('#brandsearch').val().toLowerCase();

    if(brandsearch1 !=''){
     // console.log(brandsearch1);
      jQuery('#bname li').hide();
      jQuery('[data-id^='+brandsearch1+']').show();
      jQuery('.overflow').css('height','auto');
      jQuery('.overflow').css('min-height','auto');
    }else{
      jQuery('#bname li').show();
      jQuery('.overflow').css('height','400px');
      jQuery('.overflow').css('min-height','400px');
    }
}
</script>   
