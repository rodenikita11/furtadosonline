<div class="row">
   <div class="col-md-8" style="border: 1px solid #f2f2f2;">
<?php if($resp){
   foreach($resp as $key=>$val){//print_r($val);
?> 
<div class="row bb">
   <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
      <div class="product-item-holder size-big single-product-gallery small-gallery">
         <div class="single-product-gallery-item" id="slide1">
            <a data-lightbox="image-1" data-title="Gallery">
            <img class="img-responsive" alt="" src="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" data-echo="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" />
            </a>
         </div>
         <!-- /.single-product-gallery-item -->
      </div>
   </div>
   <div class='col-sm-6 col-md-9 product-info-block'>
      <div class="product-info">
         <h1 class="name"><?echo $val['proname']; ?></h1>
         <div class="rating-reviews">
            <div class="row">
               <div class="col-sm-12">
                  <div class="reviews">
                  </div>
               </div>
            </div>
            <!-- /.row -->    
         </div>
         <!-- /.rating-reviews -->
         <div class="price-container info-container m-t-20">
            <div class="row">
               <div class="col-sm-6">
                  <div class="price-box m-1-26">
                     <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"></i> <?echo $this->libsession->priceformat($val['dicountprice']);?></span>
                        <?php if($val['dicountprice'] < $val['price']){ ?>
                        <span class="price-strike m-r-12 trr"><i class="fa fa-rupee" style="font-size:14px"></i><?php echo $val['price']; ?></span>
                        <?php } ?>
                        <?php $stock=$val['isAvailable']>0 ? 'In stock' : 'Out of stock';
                        $style=$val['isAvailable']>0 ? 'value1' : 'value2' ?> 
                     <span class="<?php echo $style;?>"><?php echo $stock;?></span>
                  </div>
               </div>
            </div>
            <!-- /.row -->
         </div>
         <!-- /.price-container -->
         <div class="pricefilter">
            <span id="qtychange<?echo $val['qty']?>" style="font-size: 14px;">No of Quantity:<b> <?echo $val['qty']; ?>Qty.</b></span>
         </div>
         <?php if($val['couponid']){ ?>
            <div class="pricefilter">
               <span id="appliedcouponid<?echo $val['couponid']?>" style="font-size: 12px; color: #cc0028">Coupan Applied: <?php echo $val['couponcode']; ?> </span>
            </div>
         <?php } ?>
         <!-- /.product-info -->
      </div>
   </div>
</div>
<?if(is_array($val['combo_product'])){ ?>
<h3 class="comt" style="width: 83%; margin: 0px 173px;">Combo Product</h3>
<?php //print_r($val['combo_product']);
   foreach($val['combo_product'] as $combokey=>$comboval){
   ?>
<div class="col-xs-12 col-sm-3 col-md-3 gallery-holder"></div>
<div class="col-xs-12 col-sm-9 col-md-9 gallery-holder">
   <div class="row bb">
      <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
         <div class="product-item-holder size-big single-product-gallery small-gallery">
            <div class="single-product-gallery-item" id="slide1">
               <a data-lightbox="image-1" data-title="Gallery">
               <img class="img-responsive" alt="" src="<? if(!isset($comboval['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $comboval['image'];} ?>" data-echo="<? if(!isset($comboval['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $comboval['image'];} ?>" />
               </a>
            </div>
            <!-- /.single-product-gallery-item -->
         </div>
      </div>
      <div class='col-sm-6 col-md-9 product-info-block'>
         <div class="product-info">
            <h1 class="name"><?echo $comboval['proname'];?></h1>
            <div class="rating-reviews">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="reviews">
                     </div>
                  </div>
               </div>
               <!-- /.row -->    
            </div>
            <!-- /.rating-reviews -->
            <div class="price-container info-container m-t-20">
               <div class="row">
                  <div class="col-sm-6">
                     <div class="price-box">
                        <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"></i> <?echo $this->libsession->priceformat(floor($comboval['dicountprice']));?></span>
                        <!--span class="value1">In Stock</span-->
                     </div>
                  </div>
               </div>
               <!-- /.row -->
            </div>
            <!-- /.price-container -->
            <div class="pricefilter">
               <span id="qtychange<?echo $comboval['qty']?>" style="font-size: 14px;">No of Quantity:<b> <?echo $comboval['qty']?>Qty.</b></span>
            </div>
            <!-- /.product-info -->
         </div>
      </div>
   </div>
</div>
<?php } } } ?>
   
<?}else{?>
<div class="Favourite-no">
   <p>There are no products in your cart</p>
</div>
<?}?>
</div>

<div class="col-md-4" style="border:1px solid #f2f2f2;padding-left: 0px;padding-right: 0px;">
      <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;padding: 10px 13px; background: #f5f5f5;"><b>Order Summary</b></div>
      <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;border-bottom: 1px solid #f5f5f5; padding: 10px 13px"><b>Cart Total</b> : &nbsp;<span style="float:right"><i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($carttotalprice));?></span></span></div>
       <?php if($cartmaindiscount && !isset($giftcode)){ ?>
            <!--<div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Total amount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($carttotalprice));?></span></div>-->
        
            <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;    border-bottom: 1px solid #f5f5f5; padding: 10px 13px"><b>Discount</b> : &nbsp;<span style="float:right"><i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($cartmaindiscount));?></span></span></div>
         <?php } ?>
         <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;    border-bottom: 1px solid #f5f5f5; padding: 10px 13px;">
            <b>Delivery Charges</b> : 
            <span style="float:right;"><i class="fa fa-rupee" style="font-size:14px"></i>  
            <span class="shippingcost"><?php echo $this->libsession->priceformat($total_courier_charges); ?></span></span>
         </div>
         <?php if($giftcode){ ?>
             <!--<div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;margin-left: 10px; padding: 5px 13px"><b>Total amount</b> : &nbsp;<i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($totalcart));?></span></div>-->

             <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px;    border-bottom: 1px solid #f5f5f5; padding: 5px 13px"><b>Additional Discount</b> : &nbsp;<span style="float:right;"><i class="fa fa-inr" aria-hidden="true"></i><span><?echo $this->libsession->priceformat(floor($totaldiscountedamount));?></span></div></span>
         <?php } ?>
         <div class="col-lg-12 Change_Address duoadd" style="font-size: 15px; padding: 5px 13px; background: #f5f5f5;"><b>Order Total </b> : &nbsp;
         <span style="float:right;"><i class="fa fa-inr" aria-hidden="true"></i><span class="totalamt"><?echo $this->libsession->priceformat(floor($amount));?></span></span></div>
         <!--<div class="col-xs-6 Place_Order"><a href="<?php echo SITEMOBURL;?>product/distributepg">Proceed to Checkout</a></div>-->
         <br>
      
</div>
</div>

<div class="row">
    <div class="col-md-12" style="margin-right: 34px; border: 1px solid #ccc;">
      <div class="col-lg-12" style="padding: 0px 0px 0px;">
         <?php if($giftcode){ ?>
         <div class="col-lg-12" style="color: #cc0028; font-weight: 700; text-align: right;"> 
            <div class="col-lg-12">
           Coupan Applied: <?php echo $giftcode; ?> 
         <?php if($giftcode){ ?>
            <span>
               <a href="<?php echo base_url(); ?>product/cancelcoupon" style="padding: 4px 9px; background: #cc0028; color:#ffffff !important;">Cancel Coupon<i class="fa fa-times-circle"></i></a>
            </span>
         <?php } ?>
         </div>
         </div>
         <div class="col-lg-12" style="padding:10px;margin: 0px 8px;">

             <div class="col-lg-2" style="padding: 5px 0px;font-size: 14px">
         <input type="radio" class="nadu" id="normal" name="shipmode" value="Normal" checked onchange="checkshippingcost()"> &nbsp; 
         <b>Normal Delivery </b>
      </div>

       <!---<div class="col-lg-3" style="margin: 5px 0px;font-size: 14px">
         <div class="nadu">
            <input type="radio" class="nadu" id="express" name="shipmode" value="Express" onchange="checkshippingcost()"> &nbsp;<b>Express Delivery</b>
         </div>
      </div>-->
   </div>
         <?}else{?>
         <div class="col-lg-12" style="padding:10px;margin: 0px 8px;">

             <div class="col-lg-2" style="padding: 5px 0px;font-size: 14px">
         <input type="radio" class="nadu" id="normal" name="shipmode" value="Normal" checked onchange="checkshippingcost()"> &nbsp; 
         <b>Normal Delivery </b>
      </div>

       <!--<div class="col-lg-3" style="margin: 5px 0px;font-size: 14px">
         <div class="nadu">
            <input type="radio" class="nadu" id="express" name="shipmode" value="Express" onchange="checkshippingcost()" disabled> &nbsp;<b>Express Delivery</b>
         </div>
      </div>-->
            <div class="col-lg-3" style="margin: 5px 0px;font-size: 14px">
               <b style="color: #cc0028;">
              Gift card & coupon code
             </b>
            </div>
            
            <div class="col-lg-4" >
               <input type="text" placeholder="Enter code" value="" name="promocode" id="promocode" style="padding: 5px; ">
               <input type="button" value="Apply" onclick="checkforgiftcode();" style="
                border:  none;background: #cc0028;padding: 6px 10px;color: #fff;font-size: 14px;">
            </div>


            
         </div>
         <?}?>
   </div>

   <div style="margin-left: 22px;">
      
      
      <!--<div class="col-lg-5" style="padding: 7px 0px;font-size: 14px">
         <b>Shipping Cost will be</b> : 
         <i class="fa fa-rupee" style="font-size:14px"></i>  <span class="shippingcost"></span>
      </div>-->

      </div>
     
      
   </div>
</div>
<div class="row" style="margin-top:10px;">
   <!--<div class="col-sm-12" style="padding-top: 10px; padding-bottom: 10px;">
   <b>* Note :</b><span> 10% discount on Books worth Rs.1,000/- and above *only applicable on non-discounted books</span>
   </div>-->
   <div class="col-sm-12">
   <b>* Note :</b><span> Delivery of product(s) is subject to availability of stock at the time the order is received. Furtados does not warranty the accuracy or    &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp;   &nbsp; correctness of the information provided on the website.</span><br><br>
   <!--<span><b>* Due to the uncertainty regarding the All India transporter Strike scheduled to begin on 20th July dispatch & delivery of products may be delayed. We apologies for any inconvenience that this may cause and thank you for your understanding.</b></span>-->
   </div>

<div class="clear"></div>
</div>

<div class="row">
   <div class="col-md-8">

   </div>

   <div class="col-md-4">
      <button class="btn btn btn-warning col-lg-4" onClick="movetonexttab()" style="width: 74%;margin-top: 5px;padding: 4px 7px; outline: none;text-align: center;">Place order</button>
   </div>
</div>
<script type="text/javascript">
   jQuery(document).ready(function(){
      if(jQuery('input[type="radio"]').is(':checked'))
      {
         setTimeout(function(){
            //checkshippingcost();
         }, 1000);
      }
      function disableBack() { window.history.forward() }
   
          window.onload = disableBack();
          window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
   });
</script>
<script type="text/javascript">
   function checkshippingcost(){
      var shippingmode=jQuery("input[name='shipmode']:checked").val();
      jQuery.ajax({
         url:'<?php echo SITEMOBURL;?>product/placeFinalOrder',
         type:'POST',
         data:{shippingmode:shippingmode},
         success:function(res){
            result=jQuery.parseJSON(res);
            
            if(result.status== true){
               //alert(result.msg.total_shipping_cost);
               jQuery('.shippingcost').html(result.msg.total_shipping_cost);
               if(result.msg.showexp == 1 || result.msg.postState==0){
                  jQuery('.carto').show();
               }else{
                  jQuery('.carto').hide(); 
               }
               total_amt=<?echo $amount?>+result.msg.total_shipping_cost; 
               jQuery('.totalamt').html(total_amt);
            }else{
               jQuery('.shippingcost').html('0');   
            }
            
         }
      });
   }
   
</script>
<script>
   function checkforgiftcode(){
      
      var promocode=jQuery('#promocode').val();
       jQuery.ajax({
            url:'<?php echo SITEMOBURL;?>product/checkgiftcarddtl',
            type:'POST',
            data:{promocode:promocode},
            success:function(res){
               result=jQuery.parseJSON(res);
               if(result.status== false){ 
                  bootbox.alert(result.msg);
               }else{
                  palcefinalorder('coupon');
               }
               
            }
         });
      
      
      
   }
</script>

<style type="text/css">

span.value2 {
    color: #cc0028;
    font-size: 14px;
   }
</style>