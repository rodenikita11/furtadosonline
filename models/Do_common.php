<?php
class Do_common extends CI_Model{
   function __construct(){
		parent:: __construct();
		$this->load->database();
	}
	
	public function getallmenu(){
		//$select ="SELECT cm1.catid, cm1.catname, cm1.catimage, cm1.catlevel, cm1.catparent_id, cm1.catsort_order,cm1.catstatus,cm2.catname as level1,cm2.catid as levelid1,cm2.catimage as catimg,cm2.catparent_id as parentid,cm3.catname as level2,cm3.catid as levelid2,cm4.catname as level3,cm4.catid as levelid3 FROM `category_master`as cm1 join category_master as cm2 on cm1.catid=cm2.catparent_id join category_master cm3 on cm2.catid=cm3.catparent_id  join category_master as cm4 on cm3.catid=cm4.catparent_id";g
		//remove where to see apple products and religious category
		$select="SELECT 
		cm1.catid, cm1.catname, cm1.catimage, cm1.catlevel, cm1.catparent_id, cm1.catsort_order,cm1.catstatus,
		
		cm2.catname as level1, cm2.priority as p1, cm2.catid as levelid1, cm2.catimage as catimg, cm2.catparent_id as parentid,
		cm3.catname as level2, cm3.priority as p2, cm3.catid as levelid2,
		cm4.catname as level3, cm4.priority as p3, cm4.catid as levelid3 
		FROM `category_master`as cm1 
		JOIN category_master as cm2 on cm1.catid=cm2.catparent_id 
		LEFT JOIN category_master cm3 on cm2.catid=cm3.catparent_id  
		LEFT JOIN category_master as cm4 on cm3.catid=cm4.catparent_id 
		WHERE cm2.catname!='APPLE PRODUCTS' AND cm2.catname!='RELIGIOUS' AND cm2.catname!='INSTRUMENT BUNDLES'
		ORDER BY cm1.catparent_id ASC";
		//echo $select; die;
		
		$res=$this->db->query($select);
		return $res1=$res->result_array();
	}
	
	public function getfilter($param){
		if($param['cid']){
			 //$query="select bm.brandname,bm.brandid,(select count(proid) from product_to_category where category_id='".$param['cid']."') as procnt ,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid and pc.category_id='".$param['cid']."') as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and pc.category_id='".$param['cid']."') as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and pc.category_id='".$param['cid']."') as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where pc.category_id='".$param['cid']."' GROUP by bm.brandid"; 

			//$query="select bm.brandname,bm.brandid,(select count(proid) from product_to_category where FIND_IN_SET('".$param['cid']."',category_path)) as 'procnt' ,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid and FIND_IN_SET('".$param['cid']."',pc.category_path)) as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and FIND_IN_SET('".$param['cid']."',category_path)) as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and FIND_IN_SET('".$param['cid']."',category_path)) as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where FIND_IN_SET('".$param['cid']."',category_path) GROUP by bm.brandid";

			// $query="select bm.brandname,bm.brandid,(select count(proid) from product_to_category where MATCH(category_path) AGAINST(".$param['cid'].")) as 'procnt' ,(SELECT count(DISTINCT(pm.proid)) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid and MATCH(pc.category_path) AGAINST(".$param['cid'].")) as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and MATCH(category_path) AGAINST(".$param['cid'].")) as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and MATCH(category_path) AGAINST(".$param['cid'].")) as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where MATCH(category_path) AGAINST(".$param['cid'].") GROUP by bm.brandid";
			
			$query0 = "SELECT bm.brandname,bm.brandid from brand_master bm join product_master pm on bm.brandid=pm.brandid 
									join product_to_category pc on pm.proid=pc.proid where MATCH(category_path) AGAINST(".$param['cid'].") GROUP by bm.brandid";
			$res0=$this->db->query($query0);
			$res0 = $res0->result_array($query0);  
			
			$procnt = "SELECT count(proid) as procnt from product_to_category where MATCH(category_path) AGAINST(".$param['cid'].")";
			$res1=$this->db->query($procnt);

			$probrandcnt = "SELECT count(DISTINCT(pm.proid)) as probrandcnt from product_master pm join product_to_category pc on pc.proid=pm.proid join  brand_master bm on bm.brandid=pm.brandid 
									where pm.brandid=bm.brandid and MATCH(pc.category_path) AGAINST(".$param['cid'].")";
			$res2=$this->db->query($probrandcnt);

			$proavailablecnt = "SELECT count(pm.proid) as proavailablecnt from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and MATCH(category_path) AGAINST(".$param['cid'].")";
			$res3=$this->db->query($proavailablecnt);

			$pronotavailablecnt = "SELECT count(pm.proid) as pronotavailablecnt from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and MATCH(category_path) AGAINST(".$param['cid'].")";
			$res4=$this->db->query($pronotavailablecnt);
			

			// $res=$this->db->query($query);
			if($res){
			 return $res1=$res->result_array($query);  
		   	}
		}
	}

	public function product_searchfilter(){
		
				
				 $query="select bm.brandname,bm.brandid,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid ) as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 ) as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 ) as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid  GROUP by bm.brandid";
				 
				 $res=$this->db->query($query);
					if($res){
					 return $res1=$res->result_array($query);  
				   }
			
		
	}
	
	/*function getpricerange($param){ //Old function with call for price is not being considered
	        if($param['cid']){
			   $sql = "SELECT  min(a.discountprice) as minprice , max(a.discountprice) as maxprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid where a.proactive=1 and a.callforprice='NO' and FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."'";  
			  echo $sql; die;
			
			$res=$this->db->query($sql);
			if($res){
		     	return $res1=$res->result_array($sql);
		      } 
			}
	
	}*/

	function getpricerange($param){ //Old function with call for price is not being considered
	        if($param['cid']){
			   $sql = "SELECT  min(a.discountprice) as minprice , max(a.discountprice) as maxprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid where a.proactive=1  and FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."'";  
			  //echo $sql; die;
			
			$res=$this->db->query($sql);
			if($res){
		     	return $res1=$res->result_array($sql);
		      } 
			}
	
	}

	/*function getpricerange($param){
			$qstr='';
	        if($param['cid']){
			   $sql = "SELECT  min(a.discountprice) as minprice , max(a.discountprice) as maxprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid LEFT JOIN brand_master bm ON a.brandid=bm.brandid LEFT JOIN review rm ON a.proid=rm.proid where a.proactive=1 and a.callforprice='NO' and (FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."')";
			
			if($param['search']){
		      	$qstr.=" and (match(a.proname) against ('".$param['search']."*' in boolean mode) OR match(a.proid) against ('".$param['search']."*' in boolean mode))";
		    }

		    if($param['branddtl'] ){
		    	$param['branddtl']=explode(',', str_replace(array('[', ']'), '', $param['branddtl']));
		       	$param['branddtl'] =implode($param['branddtl'],',');
		      	$qstr.=" and bm.brandid in(".$param['branddtl'].")";

		    }
		    if($param['avaliable'] ){
		    	$param['avaliable']=explode(',', str_replace(array('[', ']'), '', $param['avaliable']));
		       	$param['avaliable'] =implode(',', $param['avaliable']);
		      	$qstr.=" and a.isAvailable in(".$param['avaliable'].")";
			}
		     
		    if($param['minprice'] and $param['maxprice']){
		      	$qstr.=" and a.discountPrice BETWEEN  '".$param['minprice']."' AND    '".$param['maxprice']."'";
		    }

		    if(($param['start'] || $param['start']==0) && !$param['count']){
		        $qstr.=" GROUP BY a.proid ";
		    }
			
			$sql=$sql.$qstr;
			//echo $sql; die;
			$res=$this->db->query($sql);
			if($res){
		     	return $res1=$res->result_array($sql);
		      } 
			}
	
	}*/

	function getpercentage($param){
	        if($param['cid']){
	        	//print_r($param); die;
			   $sql = "SELECT DISTINCT(percentage),id,Name FROM `promotionMaster` WHERE tagCat='".$param['cid']."' group by percentage order by percentage asc";  
			
			$res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      } 
			}
	
	}
	function getattribute($param){
		  if($param['cid']){
		   $sql ="select ag.*,am.* from category_master cm join product_to_category pc on cm.catid=pc.category_id join product_master pm on pm.proid=pc.proid join product_attribute_mapping pam on pm.proid=pam.proid join attribute_master am on am.attriid=pam.attriid join attribute_group ag on ag.attrigrpid=am.attrigrpid where pc.category_id='".$param['cid']."' and ag.attrgrpfilter='1'";
		 $res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      } 
			}
	   	
	}
	
	public function getsubcat($param){
		if($param['cid']){
		  $sql ="select catid,catname from category_master where catparent_id=".$param['cid']."";
		 $res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      }else{
				  return false;
			  } 
			}
		
		
	}
	/*created by viki 19/2/2018*/
	public function getcat($param){
		if($param['cid']){
		  $sql ="SELECT cm1.catname as maincat,cm2.catname as subcat FROM `category_master` cm1 join category_master cm2 on cm1.catid=cm2.catparent_id where cm2.catid= ".$param['cid']."";
		 $res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      }else{
				  return false;
			  } 
			}
	}
	public function getcatname($param){
		if($param['catid']){
			$select="SELECT cm1.catname as maincat,cm1.catid FROM `category_master` cm1 join category_master cm2 on cm1.catid=cm2.catparent_id where cm2.catid= ".$param['catid']." ";
			$res=$this->db->query($select);
			$res1=$res->result_array($res);
			if(!empty($res1[0]['catid'])){
				$select1="SELECT catname,catid FROM category_master WHERE catparent_id='".$res1[0]['catid']."'";
				$res2=$this->db->query($select1);
				$res3=$res2->result_array($res2);
			}
			$res4['maincategory']=$res1;
			$res4['subcategory']=$res3;
			if($res4){
				return $res4;
			}else{
				return false;
			}
		}
	}
	
	/*created by viki 19/2/2018:*/
	
	public function getbreadcrum($param){
		//print_r($param); die;
		if($param['proid'] && $param['name']=='category'){
		  $sql ="SELECT DISTINCT(category_path) from product_to_category  where category_id='".$param['proid']."'";
		} elseif ($param['name']== 'categorymain') {
		  $sql = "SELECT DISTINCT(category_path) from product_to_category where find_in_set('".$param['proid']."',category_path)";
		} else {
		  $sql ="SELECT pc.category_path,pm.proname from product_to_category pc join product_master pm  on pc.proid=pm.proid where pm.proid='".$param['proid']."'";
		}
		//print ($sql); die;
		 $res=$this->db->query($sql);
			if($res){
				
		     	 $res1=$res->result_array($sql);
		     	 //print_r($res1); die;
				 if($res1){
					 $maincatname['mainproname']=$res1[0]['proname'];
					 $allcatid=explode(',',$res1[0]['category_path']);
					 $allcat=array_filter($allcatid);
					 //print_r($allcat);
					if(!empty($allcat)){
					 	for ($i=0; $i <count($allcat) ; $i++) { 
					 		$select="select catname,catid from category_master where catid=".$allcat[$i]."";
						    $res2=$this->db->query($select);
						    $res3=$res2->result_array($select);
						    if($res3){

							  $maincatname['name'][$i]=$res3[0]['catname']; 
							  $maincatname['id'][$i]=$res3[0]['catid'];  
						    }
						    if($res3[0]['catid'] == $param['proid']){
						    	break; //Dont add nodes below the given category	
						    }
						}
						//print_r($maincatname);die;
					     return $maincatname;
					}
				}
			}
		}

	/*brand of particular category created by viki 4/5/2018*/
	
	public function getbrandname($param){
		$select="SELECT bm.brandname, bm.brandid FROM `product_to_category` ptc join product_master pm on ptc.proid=pm.proid  join brand_master bm on pm.brandid=bm.brandid where find_in_set('".$param['catid']."',ptc.category_path) group by bm.brandname";
		$res=$this->db->query($select);
		$res1=$res->result_array($select);
		return $res1;
		//print '<pre>';print_r($res1); die('viki');
	}	

	public function productbrandcat($param){
	#print_R($param);exit;
	/* $probrandcat="select SQL_CACHE pm.proid as 'proid',
bm.brandid as 'brandid',
GROUP_CONCAT(pm.proname,':',pm.proid SEPARATOR '|') as 'product_name',
bm.brandname as 'brand_name',
(select GROUP_CONCAT(distinct catname,',',catid SEPARATOR '|') from category_master where FIND_IN_SET(catid,pm.categorypath)) 
as 'category_name'
from product_master pm join brand_master bm on pm.brandid=bm.brandid where match(pm.proname) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE)  OR match(pm.proTags) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(bm.brandname) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(pm.leastCategoryName) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(pm.productname_search) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR pm.proid= '".addslashes($param['search'])."' order by match(pm.proname)
against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) DESC,pm.proname DESC limit 10";*/

$probrandcat="select SQL_CACHE pm.proid as 'proid',
GROUP_CONCAT(bm.brandid SEPARATOR '|') as 'brandid',
GROUP_CONCAT(pm.proname,':',pm.proid SEPARATOR '|') as 'product_name',
GROUP_CONCAT(bm.brandid,':',bm.brandname SEPARATOR '|') as 'brand_name',
(select GROUP_CONCAT(distinct catname,',',catid SEPARATOR '|') from category_master where FIND_IN_SET(catid,pm.categorypath)) 
as 'category_name'
from product_master pm left join brand_master bm on pm.brandid=bm.brandid where match(pm.proname) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE)  OR match(pm.proTags) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(bm.brandname) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(pm.leastCategoryName) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR match(pm.productname_search) against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) OR pm.proid= '".addslashes($param['search'])."' order by match(pm.proname)
against(concat('".addslashes($param['search'])."','*') IN BOOLEAN MODE) DESC,pm.proname DESC limit 10";

//echo $probrandcat; die;

	$probrandcat_sql=$this->db->query($probrandcat);
	$probrandcatrow=$probrandcat_sql->result_array($probrandcat);

	if(!empty($probrandcatrow)){
		foreach ($probrandcatrow as $key => $val){
			# code...
			//print_R($probrandcatrow);
			$product[]=explode('|',$val['product_name']);;
			$brand[]=explode('|', $val['brand_name']);
			$brand_id[]=explode('|', $val['brandid']);
			$category[]=explode('|',$val['category_name']);
		}	
	}else{
		echo "NO DATA Found";
	}
		$brand=array_unique($brand[0]);
		$brand_id=array_unique($brand_id[0]);
		//$category=array_unique($category);
		//$product=array_unique($product);
	
		$res=array();
		$res['product']=$product;
		$res['brand']=$brand;
		$res['category']=$category;
    	$res['brand_id']=$brand_id;
		if(!empty($res)){
			return $res;
		}else{
			return false;
		}
	}
	
	
	public function addbannercount($param){
		//print_r($param); die;
		$upadte="UPDATE banner_master SET clickcount = clickcount + 1 WHERE id = '".$param['id']."' LIMIT 1"; 
		//print $update; die;
		$res=$this->db->query($upadte);
		if($res){ 
			
			return $res;
		}else{
			
			return false;
		}
		
		
	}
	public function getstaticdata($param){
		
		$upadte="select * from staticMaster  WHERE id = ".$param['id']." LIMIT 1"; 
		$res=$this->db->query($upadte);
		if($res){ 
			
			return $res->result_array();
		}else{
			
			return false;
		}
		
		
	}

	public function getjobdetails() {
		$select = "SELECT * FROM `jobMaster` where 1 AND status=1 order by id desc";	//limit 0,4 
		$res=$this->db->query($select);
		if($res){ 
			
			return $res->result_array();
		}else{
			
			return false;
		}
	}

	public function brandsmeta($param){
		$select="SELECT seo_title, seo_metakeyword, seo_metadesc, seo_footerdescription FROM brand_master WHERE brandid=".$param['brandid']."";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getfilterbrand($param){
		$select="SELECT bm.brandname,bm.brandid FROM brand_master bm LEFT JOIN product_master pm on bm.brandid=pm.brandid JOIN product_to_category pc on pm.proid=pc.proid where FIND_IN_SET('".$param['cid']."',category_path) GROUP by bm.brandid";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getfilteravailable($param){
		$select="SELECT count(pm.proid) proavailablecnt from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and FIND_IN_SET('".$param['cid']."',category_path)";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getfilternotavailable($param){
		$select="SELECT count(pm.proid) pronotavailablecnt from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and FIND_IN_SET('".$param['cid']."',category_path)";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getfilterprocnt($param){
		$select="SELECT count(proid) procnt from product_to_category where FIND_IN_SET('".$param['cid']."',category_path)";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getfilterminprice($param){
		$select="SELECT  min(a.discountprice) as minprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid where a.proactive=1 and a.callforprice='NO' and FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."'";

		$res=$this->db->query($select);
		return $res->result_array($res);
	}

	public function getfiltermaxprice($param){
		$select="SELECT  max(a.discountprice) as maxprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid where a.proactive=1 and a.callforprice='NO' and FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."'";

		$res=$this->db->query($select);
		return $res->result_array($res);
	}

	public function getfilterbrandcount($param){
		$select="SELECT bm.brandid, count(pm.proid) probrandcnt from product_master pm LEFT JOIN brand_master bm ON pm.brandid=bm.brandid join product_to_category pc on pc.proid=pm.proid where FIND_IN_SET('".$param['cid']."',pc.category_path) GROUP BY bm.brandid";
		$res=$this->db->query($select);
		return $res->result_array();
	}

	public function getbrands(){
		$select="SELECT brandid , brandname FROM brand_master";
		$res=$this->db->query($select);
		return $res->result_array();
	}
}
?>