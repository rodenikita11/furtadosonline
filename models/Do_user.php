<?php
class Do_user extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tableName  = 'customer';
        $this->primaryKey = 'customer_id';
    }
	
	private function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Do_user.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . $query.PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/database_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	}

    //=============USER_DETAIL===============
    public function getusrdtl($param = false){
        if ($param['email'] && $param['password']) {
            $select = "select customer_id, registrationtype,wishlist,firstname, lastname, email, status, telephone, gender from customer where email='" . $param['email'] . "' and  password ='" . $param['password'] . "' and status='1'";
            //echo $select; 
            $res3 = $this->db->query($select);
            if ($res3) {
                $res1 = $res3->result_array();
                return $res1;
            }

        } else {
            echo "email or password missing ";
            exit;

        }
    }
    //=======================================

    //============changepassword by viki 14/3/2018====
    public function changepassword($param)
    {
		$this->load->library('encryption');
		$param['id'] = $this->encryption->decrypt($param['id']);
        if ($param['id']) {
            $select = "SELECT customer_id FROM customer WHERE customer_id='" . $param['id'] . "' AND  password='" . md5($param['oldpassword']) . "'";
            //print $select;
            $res    = $this->db->query($select);
            $res1   = $res->result_array();
            //print_r($res1); die;
            if ($res1) {
                $update = "UPDATE `customer` SET password='" . md5($param['newpassword']) . "' WHERE customer_id='" . $res1[0]['customer_id'] . "'";
                //print $update;
                $res2   = $this->db->query($update);
                // print_r($res2);  die;
                if ($res2) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        }
    }
    //============End changepassword===============

    //============Insert New User==================
    public function insertuser($param)
    {
        $param['date_added'] = date("Y-m-d H:i:s");
        if ($param['email'] && $param['password'] && $param['name']) {
            $select = "select customer_id from customer where email='" . $param['email'] . "'";
            $res3   = $this->db->query($select);
            $res1   = $res3->result_array();
            // print_r($res1); die;
            $cnt    = count($res1);
            if ($cnt > 0) {
                return 3;
            } else {

                $select = " insert into  customer set firstname='" . $param['name'] . "',email ='" . $param['email'] . "', password='" . $param['password'] . "', city='" . $param['city'] . "',state='" . $param['state'] . "',telephone='" . $param['phoneno'] . "',useraddress='" . $param['address1'] . "',status='1', date_added='" . $param['date_added'] . "' , registrationtype='1' ";
                $res4   = $this->db->query($select);
                if ($res4) {
                    return true;
                }

            }


        } else {
            echo "parameter missing";
            exit;

        }

    }
    //=============================================

    //============Get Cities=======================
    public function getcities($param)
    {
        $selectcities = "SELECT id,cityName FROM `cityMaster` WHERE stateId=" . $param['sid'];

        $res = $this->db->query($selectcities);
        return $res2 = $res->result_array();
    }
    //=============================================

    //==============Get States=========================
    public function getStates()
    {
        $selectstate = "SELECT id,stateName FROM `stateMaster`";
        $res1        = $this->db->query($selectstate);
        //print_r($this->db); die;
        return $res2 = $res1->result_array();

    }
    //==================================================

    //===========FOR USER ACCOUNT==================
    public function updateuseraccdtl($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        //print_r($param);die;
        if ($param['model'] == 'useraccdtl') {
            $update = "UPDATE customer SET useraddress='" . $param['address'] . "',landmark='" . $param['landmark'] . "',state='" . $param['state2'] . "',city='" . $param['city2'] . "',pincode='" . $param['pincode2'] . "'";
        } else {


            if (!empty($param['interest'])) {
                $interest = implode(',', $param['interest']);
            }
            if (!empty($param['dob'])) {
                $dob = date('Y-m-d', strtotime($param['dob']));
            }
            //print_r($param);
            $update = "UPDATE customer SET title='" . $param['title'] . "',firstname='" . $param['fname'] . "',lastname='" . $param['lname'] . "',dob='" . $dob . "',telephone='" . $param['telephone'] . "',fax='" . $param['fax'] . "',interest='" . $interest . "',gender='" . $param['gender'] . "',newsletterSubscribed='" . $param['Newsletter'] . "',hearVia='" . $param['hearAboutUs'] . "' ";

            // if (!empty($param['email'])) {

            //     $update .= " ,email='" . $param['email'] . "'";
            // }
            if (!empty($param['pincode'])) {

                $update .= " ,pincode='" . $param['pincode'] . "'";
            }
            if (!empty($param['state1'])) {

                $update .= " ,state='" . $param['state1'] . "'";
            }
            if (!empty($param['state2'])) {

                $update .= " ,state='" . $param['state2'] . "'";
            }
            if (!empty($param['state'])) {

                $update .= " ,state='" . $param['state'] . "'";
            }
            if (!empty($param['city1'])) {

                $update .= " ,city='" . $param['city1'] . "'";
            }
            if (!empty($param['city2'])) {

                $update .= " ,city='" . $param['city2'] . "'";
            }
            if (!empty($param['city'])) {

                $update .= " ,city='" . $param['city'] . "'";
            }
            if (!empty($param['country'])) {

                $update .= " ,country='" . $param['country'] . "'";
            }
        }
        $update .= " WHERE 1 AND customer_id=" . $param['usrid'];
        //print $update;die;
        $res4 = $this->db->query($update);
        if ($res4) {
            return true;
        }
    }
    //============================================

    //============for shipping address============
    public function updateuser($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        $name = explode(" ", $param['username']);

        $update = "insert into  customer_shipping_address SET firstname='" . $name[0] . "',lastname='" . $name[1] . "', contact_num='" . $param['mobnumber'] . "',address_1='" . $param['addr'] . "',landmark='" . $param['landmark'] . "',postcode='" . $param['pincode'] . "',state='" . $param['state'] . "', company='', company_id='', tax_id='', address_2='', city='" . $param['city'] . "',customer_id=" . $param['usrid'] . ",created=now()";
        //print $update; die;
        $res4   = $this->db->query($update);
        if ($res4) {
            return true;
        }

    }
    //==============================================

    //============Add Shipping Address==============
    public function addshippingaddress($param)
    {
        $select = "SELECT address_id,firstname,lastname,customer_id,contact_num,address_1,landmark,postcode,city,state FROM customer_shipping_address WHERE address_id=" . $param['addressid'];
        $res    = $this->db->query($select);
        if ($res) {
            $res1 = $res->result_array();
            $shipping_address_position = $res1[0]['address_id']; // $shipping_address_position will be 1 extra. Anything more then 
            $order_id = !empty($this->libsession->getSession('orderinvoice_no'))?$this->libsession->getSession('orderinvoice_no'):'null';
        
            if($_SESSION['billing_address_key']){
                $billing_id = $_SESSION['billing_address_key'];
                $insert = "INSERT INTO `customer_billing_address` SET address_id=$billing_id, order_id= $order_id, customer_id='" . $res1[0]['customer_id'] . "',firstname='" . $res1[0]['firstname'] . "',lastname='" . $res1[0]['lastname'] . "',contact_num='" . $res1[0]['contact_num'] . "',address_1='" . $res1[0]['address_1'] . "',landmark='" . $res1[0]['landmark'] . "',postcode='" . $res1[0]['postcode'] . "',city='" . $res1[0]['city'] . "',state='" . $res1[0]['state'] . "' ON DUPLICATE KEY UPDATE order_id=$order_id, address_1='" . $res1[0]['address_1'] . "',city='" . $res1[0]['city'] . "',state='" . $res1[0]['state'] . "' ";
            }
            else{
                $insert = "INSERT INTO `customer_billing_address` SET order_id= $order_id, customer_id='" . $res1[0]['customer_id'] . "',firstname='" . $res1[0]['firstname'] . "',lastname='" . $res1[0]['lastname'] . "',contact_num='" . $res1[0]['contact_num'] . "',address_1='" . $res1[0]['address_1'] . "',landmark='" . $res1[0]['landmark'] . "',postcode='" . $res1[0]['postcode'] . "',city='" . $res1[0]['city'] . "',state='" . $res1[0]['state'] . "' ON DUPLICATE KEY UPDATE order_id=$order_id, address_1='" . $res1[0]['address_1'] . "',city='" . $res1[0]['city'] . "',state='" . $res1[0]['state'] . "' ";
            }
            // echo $insert; exit;
            $res3 = $this->db->query($insert);
            if ($res3) {
                $res_i = $this->db->query('SELECT LAST_INSERT_ID() as id;');
                $res_i = $res_i->result_array();
                $_SESSION['billing_address_key'] = $res_i[0]['id'];
                return true;
            } else {
                return false;
            }

        }
    }
    //==============================================

    //============Save Billing Address==============
    public function savebillingaddress($param = false)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        $name = explode(" ", $param['custname']);

        $insert = "INSERT INTO `customer_billing_address` SET customer_id='" . $param['usrid'] . "', firstname='" . $name[0] . "',lastname='" . $name[1] . "',contact_num='" . $param['mobnumber'] . "',address_1='" . $param['addr'] . "',landmark='" . $param['landmark'] . "',postcode='" . $param['pincode'] . "',state=" . $param['state'] . ",city=" . $param['city'] . "";

        $res3 = $this->db->query($insert);
        if ($res3) {
            return true;
        } else {
            return false;
        }


    }
    //================================================

    //===========User Add Details=====================
    public function getuseraddrdtl($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        $select = "SELECT address_id, firstname, lastname, customer_id,contact_num,address_1,landmark,postcode FROM customer_shipping_address WHERE customer_id=" . $param['usrid']." ORDER BY address_id DESC";

        $res = $this->db->query($select);
        if ($res) {
            $res1 = $res->result_array();

            return $res1;
        }

    }
    //================================================

    //================User Data=======================
    public function getuserdata($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        $select = "select email,title,firstname,lastname,telephone,dob,useraddress,pincode,fax,interest,newsletterSubscribed,hearVia,city,state,country,gender,landmark from customer where customer_id='" . $param['usrid'] . "'";
        $raw_order_history = $this->db->query($select);
        if ($raw_order_history) {
            $order_history = $raw_order_history->result_array();
            return $order_history;
        } else {
            return false;
        }
    }
    //=================================================

    public function getshippingid($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        $select = "SELECT address_id FROM customer_shipping_address WHERE customer_id=" . $param['usrid'] . " order by address_id desc limit 1";
        $res    = $this->db->query($select);
        if ($res) {
            $res1 = $res->result_array();

            return $res1;
        }


    }

    //for facebook user
    public function checkUser($data = array()){
        $this->db->select('*');
        $this->db->from($this->tableName);

        //$this->db->where(array('fb_oauth_provider'=>$data['fb_oauth_provider'],'fb_oauth_uid'=>$data['fb_oauth_uid']));
        $this->db->where(array(
            'email' => $data['email']
        ));

        $prevQuery = $this->db->get();

        $prevCheck  = $prevQuery->num_rows();
        $prevResult = $prevQuery->row_array();
        if ($prevCheck > 0) {


            $data['fb_modified'] = date("Y-m-d H:i:s");
            $update= "UPDATE `customer` SET `firstname` ='" . $data['firstname'] . "', `lastname` ='" . $data['lastname'] . "',
            `fb_oauth_provider` ='" . $data['fb_oauth_provider'] . "', `fb_oauth_uid` = '" . $data['fb_oauth_uid'] . "', `fb_modified` = '" . $data['fb_modified'] . "'  WHERE `customer_id` = '" . $prevResult['customer_id'] . "'";

            $this->db->query($update);


            $prevResult['userID'] = $prevResult['customer_id'];
			//encrypt the userid.
			$this->load->library('encryption');
			$prevResult['userID'] = $this->encryption->encrypt($prevResult['userID']);
			
            $data['userID']       = $prevResult['userID'];
            $data['wishlist']     = $prevResult['wishlist'];
            $data['usradd']       = $prevResult['useraddress'];
            $data['usrnum']       = $prevResult['telephone'];

        } else {

            $data['date_added']  = date("Y-m-d H:i:s");
            $data['fb_modified'] = date("Y-m-d H:i:s");
            $query               = "INSERT INTO `customer` set `fb_oauth_provider`='" . $data['fb_oauth_provider'] . "', `fb_oauth_uid`='" . $data['fb_oauth_uid'] . "', `firstname`='" . $data['firstname'] . "', `lastname`='" . $data['lastname'] . "', `email`='" . $data['email'] . "', `gender`='" . $data['gender'] . "', `date_added`='" . $data['date_added'] . "', `fb_modified`='" . $data['fb_modified'] . "' , registrationtype='1',status='1' ";

            $this->db->query($query);
			$this->log_fileDetails('checkUser', $query);
        
			
            $data['userID']   = $this->db->insert_id();
			//encrypt the userid.
			$this->load->library('encryption');
			$data['userID'] = $this->encryption->encrypt($data['userID']);
			
            $data['wishlist'] = $prevResult['wishlist'];
            $data['usradd']   = $prevResult['useraddress'];
            $data['usrnum']   = $prevResult['telephone'];
        }

        return $data;
    }

    //for gmail user
    public function checkgmailUser($data = array())
    {

        $this->db->select('*');
        $this->db->from($this->tableName);

        //$this->db->where(array('fb_oauth_provider'=>$data['fb_oauth_provider'],'fb_oauth_uid'=>$data['fb_oauth_uid']));
        $this->db->where(array(
            'email' => $data['email']
        ));

        $prevQuery = $this->db->get();

        $prevCheck  = $prevQuery->num_rows();
        $prevResult = $prevQuery->row_array();
        if ($prevCheck > 0) {

            $data['fb_modified'] = date("Y-m-d H:i:s");
            //$update = $this->db->update($this->tableName,$data,array('customer_id'=>$prevResult['customer_id']));
            $update              = "UPDATE `customer` SET  `firstname` ='" . $data['firstname'] . "', `lastname` ='" . $data['lastname'] . "',
            `fb_modified` = '" . $data['fb_modified'] . "' WHERE `customer_id` = '" . $prevResult['customer_id'] . "' AND registrationtype='1' ";

            $this->db->query($update);

            $prevResult['userID'] = $prevResult['customer_id'];
			//encrypt the userid.
			$this->load->library('encryption');
			$prevResult['userID'] = $this->encryption->encrypt($prevResult['userID']);
			
            $data['userID']       = $prevResult['userID'];
            $data['wishlist']     = $prevResult['wishlist'];
            $data['usradd']       = $prevResult['useraddress'];
            $data['usrnum']       = $prevResult['telephone'];


        } else {

            $data['date_added']       = date("Y-m-d H:i:s");
            $data['fb_modified']      = date("Y-m-d H:i:s");
            // $data['registrationtype'] = 1;
			// $data['status'] = 1;
            // $insert = $this->db->insert($this->tableName, $data);
			
			$query = "INSERT INTO `customer` set `fb_oauth_provider`='" . $data['fb_oauth_provider'] . "', `fb_oauth_uid`='" . $data['fb_oauth_uid'] . "', `firstname`='" . $data['firstname'] . "', `lastname`='" . $data['lastname'] . "', `email`='" . $data['email'] . "', `gender`='" . $data['gender'] . "', `date_added`='" . $data['date_added'] . "', `fb_modified`='" . $data['fb_modified'] . "' , registrationtype='1',status='1' ";
            $this->db->query($query);
			$this->log_fileDetails('checkgmailUser', $query);
			
            $data['userID']   = $this->db->insert_id();
			if($data['userID'] > 0){
				$this->load->library('encryption');
				$data['userID'] = $this->encryption->encrypt($data['userID']);
			}
            $data['wishlist'] = $prevResult['wishlist'];
            $data['usradd']   = $prevResult['useraddress'];
            $data['usrnum']   = $prevResult['telephone'];
        }

        return $data;
    }


    public function generateRandomPassword()
    {
        // $charset = "abcdefghijklmnopqrstuvwxyz";
        // $charset .= "0123456789";
        // for ($i = 0; $i < 9; $i++) {
        //     $key .= $charset[(mt_rand(0, (strlen($charset) - 1)))];
        // }
        // return $key;

        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.
            '0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
        $str = '';
        for ($i=0; $i < 9; $i++)
            $str .= $chars[(mt_rand(0, (strlen($chars) - 1)))];
        return $str;
    }

    public function saveforgotpassword($param)
    {
        //print 'zzzz';print_R($param);
        $emailid  = addslashes(trim(strip_tags($param['emailid'])));
        //print_r($emailid); die;
        #$emailid="pritishko@gmail.com";
        $select   = "SELECT customer_id,title,firstname,lastname FROM customer WHERE email = '" . $emailid . "' LIMIT 1";
        $res      = $this->db->query($select);
        #print $res->num_rows;
        $maildata = array();
        if ($res) {
            if ($res->num_rows() == 1) {
                $res1 = $res->result_array();
                //print_R($res1);

                $id        = stripslashes($res1[0]['customer_id']);
                $title     = stripslashes($res1[0]['title']);
                $firstName = stripslashes($res1[0]['firstname']);
                $lastName  = stripslashes($res1[0]['lastName']);
                $name      = $title . " " . ucfirst($firstName) . " " . ucfirst($lastName);
                $encCode   = $this->generateRandomPassword();

                #print 'Customer_id'.$id;

                if ($id != "" && $id != "0") {
                    $updatePassSql = " UPDATE customer SET password = '" . md5($encCode) . "' WHERE customer_id =" . $id . " LIMIT 1"; //exit();
                    $updatePass    = $this->db->query($updatePassSql);

                    $affected_rows = $this->db->affected_rows();

                    if ($affected_rows == 1) {
                        #print $html;
                        $maildata['firstname'] = $firstName;
                        $maildata['name']      = $name;
                        $maildata['enccode']   = $encCode;
                        $maildata['emailid']   = $emailid;


                    }


                }
            }
        }
        if (!empty($maildata)) {
            #print_R($maildata);exit;
            return $maildata;
        } else {
            return false;
        }

    }

    public function insertnewsletter($param = false)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        //print_R($param['email']);die;
        if ($param['email']) {
            $match = "SELECT distinct(email) FROM newsletterusers WHERE email='" . $param['email'] . "'";
            //print $match;die;
            $res   = $this->db->query($match);
            $res1  = $res->result_array();
            // print_R($res1);die;
            if ($res1[0]['email'] == $param['email']) {
                //  echo 'hii';
                return $res1;
                //exit;
            } else {

                $select = "INSERT INTO newsletterusers SET userid='" . $param['usrid'] . "', status='1', email='" . $param['email'] . "', insertDate=now(), updateDate=now(), ipAddress='" . $param['ipaddress'] . "'";

                $res3 = $this->db->query($select);

                if ($res3) {

                    return $this->db->insert_id($res3);
                } else {
                    return false;
                }
            }
        } else {
            echo "email missing ";
            exit;

        }
    }
    function updatenewsletter($id)
    {
        $update = "UPDATE `newsletterusers` SET isSent='1' WHERE id='" . $id . "'";
        $res    = $this->db->query($update);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function checkpincode($param = false)
    {
        $select = "SELECT pincode FROM pincodeMaster WHERE pincode=" . $param['pincode'] ." and status=1";
        $res    = $this->db->query($select);
        $res2   = $res->result_array();
        if (count($res2) > 0) {
            return $res2;
        } else {
            return false;
        }
    }

    public function getmapdtl($param = false)
    {
        $select = "SELECT * FROM `storeMaster` WHERE status='1'";
        if ($param) {
            $select .= " and storeid=" . $param['id'];
        } else {
            $select .= " order by storeName";
        }
      // print_r($select);
        $res  = $this->db->query($select);
        $res2 = $res->result_array();
        //print_r($res2);
        if (is_array($res2) && count($res2) > 0) {
            return $res2;
        } else {
            return false;
        }

    }

    public function getevento($param = false)
    {

        $select = "SELECT * FROM `eventMaster` WHERE status='1' and storeId ='" . $param['id'] . "'";
        //echo $select;

        $res  = $this->db->query($select);
        $res2 = $res->result_array();
        if ($res2) {
            return $res2;
        } else {
            return false;
        }

    }



    public function getmap($param = false)
    {

        $select = "SELECT googleMapLink FROM `storeMaster` WHERE 1";
        if ($param) {
            $select .= " and id=" . $param['id'];
        }

        $res  = $this->db->query($select);
        $res2 = $res->result_array();
        if ($res2) {
            return $res2;
        } else {
            return false;
        }

    }


    public function savefeedback($param = false)
    {

        $select = "INSERT INTO feedbackMaster SET full_name = '" . $param['username'] . "', commentText = '" . $param['comment'] . "', emailid = '" . $param['emailid'] . "', feedbackType = 1, ipAddress   = '" . $param['ip'] . "', status = 1, insertDate = NOW()";

        $res = $this->db->query($select);

        if ($res) {
            return true;
        } else {
            return false;
        }

    }

    public function savesessionhistory($param)
    {
		$this->load->library('encryption');
		$param['usrid'] = $this->encryption->decrypt($param['usrid']);
        #print_R($param);die;
        foreach ($param['cart'] as $val) {
            $productid .= $val['pid'];
            $productid .= ',';

            $quantity .= $val['qty'];
            $quantity .= ',';

        }
        $productid = rtrim($productid, ',');
        $quantity  = rtrim($quantity, ',');

        $savesesshis = "INSERT into sessionHistory set userId='" . $param['usrid'] . "',productId='" . $productid . "',quantity='" . $quantity . "',lastUpdated = NOW(), mail_sent='0', is_visited='0'";

        $res = $this->db->query($savesesshis);



    }


    public function updateaddress($param)
    {
        $lastname = ($param['lastname']) ? "lastname='" . $param['lastname'] . "', " : "";
        $update   = "UPDATE customer_shipping_address SET firstname='" . $param['firstname'] . "', " . $lastname . " address_1='" . $param['address'] . "', landmark='" . $param['landmark'] . "', postcode='" . $param['postcode'] . "' WHERE address_id=" . $param['addressid'] . "";
        $res      = $this->db->query($update);
        if ($res) {
            return true;
        } else {
            return false;
        }
    }


    public function deleteaddress($param)
    {
        $delete = "DELETE FROM customer_shipping_address WHERE address_id=" . $param['addressid'] . " AND customer_id=" . $param['customerid'] . "";
        $res    = $this->db->query($delete);
        return ($res) ? true : false;
    }

    public function pincodavailabilitycheck($param){
        $select="SELECT * FROM pincodeMaster WHERE pincode='".$param['pincode']."' AND stateId='".$param['state']."' and status=1";
        $res=$this->db->query($select);
        $results=$res->result_array();
        return count($results);
    }

    public function newsletterunsub($param){
        $delete="DELETE FROM newsletterusers WHERE email='".$param['email']."'";
        return $this->db->query($delete);
    }
}
?>