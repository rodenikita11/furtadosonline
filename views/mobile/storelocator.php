
<style type="text/css">
  .panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
    text-transform: uppercase;
    font-weight: 600;
}
.panel {
    margin-bottom: 2px;
  }
  
</style>
<div class="top-logo">
   <div class="container" style="padding:0;">
      <div class="row">
         <div class="col-md-12">
            <div class="col-xs-2">
               <a href="#" onclick="window.history.go(-1); return false;" >
                  <!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
                  <i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
               </a>
            </div>
            <div class="col-xs-4 Favourite_list">Store Detail</div>
         </div>
      </div>
   </div>
</div>
<!-- store locator contianer starst -->
<div class="w100per">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="listing-box w100per no-shadow">
        <div class="panel panel-default" style="width: 92%;">
          <div class="panel-heading"><?php echo $map[0]['storeName'];?></div>
          <div class="panel-body">
              <p><?php echo $map[0]['storeAddress'];?></p>
         <?php if (isset($map[0]['googleMapLink'])) { 
			$storename=strtolower(str_replace(array('----', '---', '--'), '-', preg_replace('/(\,|\s|\-)/i', '-', $map[0]['storeName'])));
		 ?>
         <p class="last-row w100per"><a href="<?php echo SITEMOBURL;?>store/<?php echo $storename ?>/<?php echo $map[0]['storeId'];?>" class="pull-left"> 
		 <!--<img src="<?php echo SITEIMAGES;?>mobimg/map-icn.jpg">-->
		 <span class="gmap">Google Map</span></a></p>
         <?php } ?>
          <div class="media-left"><img src="<?php echo SITEIMAGES;?>mobimg/clock.jpg" alt=""></div>
          </div>
          <?php if($map[0]['telePhone1']){ ?>
                    <p style="padding-left: 10px"><b>Phone:</b> <span style="padding-left: 22px;"><?php echo $map[0]['telePhone1'];?></span></p>
                  <?php } ?>
                  <?php if($map[0]['telePhone2']){ ?>
                    <p style="padding-left: 10px"><b>Phone 2:</b> <span style="padding-left: 10px;"><?php echo $map[0]['telePhone2'];?> </span></p>
                  <?php } ?>
                  <p style="padding-left: 10px"><b>Timing:</b> <span <span style="padding-left:18px;"><?php echo ($map[0]['time']) ? $map[0]['time'] : 'N/A'; ?></span></p>
                 <!--<p style="padding-left: 10px"><b>Closed On:</b><span <span style="padding-left:4px;"><?php echo ($map[0]['closedOn']) ? $map[0]['closedOn'] : 'N/A'; ?> </span> </p>
                  <p style="padding-left: 80px" class="media-heading"><?php  if(isset($map[0]['telePhone1'])) { ?><a href="tele:<?php echo $map[0]['telePhone1']; ?>"><?php echo $map[0]['telePhone1'];?></a><? } else{
                     echo "Not available"; 
                     }
                     if(isset($map[0]['telePhone2'])){  ?> <a href="tele:<?php echo $map[0]['telePhone2'];?>"><?php echo "/ ".$map[0]['telePhone2'];?></a><? }?>
                   </p>-->
        </div>
        
         
         
      </div>
   </div>
   <?php if(!empty($map[0]['storeImage'])){ ?>
   <div class="store-image col-xs-12 col-sm-12 col-md-12 col-lg-12 bdr-top">
      <div class="store-slider">
         <img src="<?php echo SITESTORE;?>/<?php echo $map[0]['storeImage'];?>" alt="">
      </div>
   </div>
   <? } ?>
</div>
<!-- store locator contianer finish -->
<script>
   function initMap() {
     // Create a map object and specify the DOM element for display.
     var map = new google.maps.Map(document.getElementById('map'), {
       center: {lat: -34.397, lng: 150.644},
       scrollwheel: false,
       zoom: 8
     });
   }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfjn9LcHReUmiN89okGovW9XCqJEO-3-E&callback=initMap"
   async defer></script>
<?php include("include/footer.php"); ?>