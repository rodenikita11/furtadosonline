        <?php $bannerOfferDetails=$this->Do_banner->subbanner(); 
				if(!empty($bannerOfferDetails)) { 
		?>
		<section>
            <div class="block color-scheme-2 section-bg1">
                <div class="container">
                    <div class="header-for-light">
                        <h1 class="wow fadeInRight animated" data-wow-duration="1s">BEST  <span>OFFERS</span></h1>

                        <div class="toolbar-for-light" id="nav-bestseller">
                            <a href="javascript:;" data-role="prev" class="prev"><i class="fa fa-angle-left"></i></a>
                            <a href="javascript:;" data-role="next" class="next"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div id="owl-bestseller"  class="owl-carousel">
						<?php foreach($bannerOfferDetails as $bofferKey=>$bofferVal) { ?>
                        <div class="text-center item home-right-mr-sc">
                            <article class="product light">
                                <a href="<?php echo $bofferVal['img_link'];?>"><img class="img-overlay img-responsive" src="<?php echo $this->lib_storeuri->getprothumbnail($bofferVal['img_path'],251);?>" alt="" title=""></a>
                             </article>
                        </div>
						<?php } ?>
                    </div>
                </div>
            </div>
        </section>
		<?php } ?>