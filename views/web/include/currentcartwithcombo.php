<style type="text/css"> .modal-lg {width: 950px ! important;} 
   /*.combooverflow {
   background: #fff;
   min-height: 500px;
   height: 500px;
   overflow: auto;
   width: 100%;
   }*/
   .combooverflow {
   height: 500px ! important;
   min-height: 500px ! important;
   overflow: auto;
   }
</style>
<div id="myTabContent" class="tab-content overflow2 combooverflow">
   <div role="tabpanel" class="tab-pane fade in active " id="home" aria-labelledby="home-tab">
      <?php
         if(count($res) > 0){
         	$totalprice=0;
         	foreach($res as $key=>$val){
         		$princetoshow=($val['dicountprice'] < $val['price']) ? $val['dicountprice'] : $val['price'];
         		$pricetostrike=($val['dicountprice'] < $val['price']) ? $val['price'] : '';
         		$totalprice+=$princetoshow;
         		?>
      <div class="deatilsbg">
         <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
            <div class="product-item-holder size-big single-product-gallery small-gallery">
               <div class="single-product-gallery-item" id="slide1">
                  <a data-lightbox="image-1" data-title="Gallery">
                  <img class="img-responsive" alt="" src="<?php echo ($val['image']) ? $val['image'] : ''.SITEIMAGES.'mobimg/noimage1.png'; ?>" data-echo="<?php echo ($val['image']) ? $val['image'] : ''.SITEIMAGES.'mobimg/noimage1.png'; ?>">
                  </a>
               </div>
               <!-- /.single-product-gallery-item -->
            </div>
         </div>
         <div class='col-sm-6 col-md-9 product-info-block'>
            <div class="product-info">
               <h1 class="name"><?php echo $val['proname']; ?></h1>
               <div class="rating-reviews">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="reviews">
                           <a href="#" class="lnk"></a>
                        </div>
                     </div>
                  </div>
                  <!-- /.row -->    
               </div>
               <!-- /.rating-reviews -->
               <div class="price-boxst">
                  <span class="price m-r-12" id="updateprice"> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo number_format($princetoshow); ?></span>
                  <?php if($val['dicountprice'] < $val['price']){ ?>
                  <span class="price-strike m-r-12 "> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo number_format($pricetostrike); ?></span>
                  <?php } ?>
                  <span class="value1">In Stock</span>
               </div>
            </div>
            <!-- /.product-info -->
         </div>
         <!-- /.col-sm-7 -->
         <?/*div class="pricefilter m-l-98" >
            <div class="col-sm-2">
               <select class="form-control ft" id="qtychange_<?php echo $val['proid']; ?>" onChange="updatecombocart(<?php echo $val['proid']; ?>)" style="    padding: 6px 15px ! important;">
         <option value="1" selected>1</option>
         <option value="2">2</option>
         <option value="3">3</option>
         <option value="4">4</option>
         <option value="5">5</option>
         </select>
      </div>
      <div class="col-sm-2">
         <button type="button" class="btn btn-Move">Move to Favourite</button>
      </div>
      <div class="col-md-4">
         <div class="btn-group" onClick="removefromcart(<?php echo $val['proid']; ?>);">
            <a href="javascript:void(0)" class="btn btn-inverse disabled delet"><i class="glyphicon glyphicon-trash"></i></a>
         </div>
      </div>
      </div*/?>
   </div>
   <?php if(is_array($val['combo_product'])){ ?>
   <h4 class="comboh2">Combo Products</h4>
   <?php foreach($val['combo_product'] as $combokey=>$comboval){ 
      $comboprincetoshow=($comboval['dicountprice'] < $comboval['price']) ? $comboval['dicountprice'] : $comboval['price'];
      $combopricetostrike=($comboval['dicountprice'] < $comboval['price']) ? $comboval['price'] : '';
      $totalprice+=$comboprincetoshow;
      ?>
   <div class="col-md-3 "></div>
   <div class="deatilsbg col-md-9">
      <div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
         <div class="product-item-holder size-big single-product-gallery small-gallery">
            <div class="single-product-gallery-item" id="slide1">
               <a data-lightbox="image-1" data-title="Gallery">
               <img class="img-responsive" alt="" src="<?php echo ($comboval['image']) ? $comboval['image'] : ''.SITEIMAGES.'mobimg/noimage1.png'; ?>" data-echo="<?php echo ($comboval['image']) ? $comboval['image'] : ''.SITEIMAGES.'mobimg/noimage1.png'; ?>">
               </a>
            </div>
            <!-- /.single-product-gallery-item -->
         </div>
      </div>
      <div class='col-sm-6 col-md-9 product-info-block'>
         <div class="product-info">
            <h1 class="name"><?php echo $comboval['proname']; ?></h1>
            <div class="rating-reviews">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="reviews">
                        <a href="#" class="lnk"></a>
                     </div>
                  </div>
               </div>
               <!-- /.row -->    
            </div>
            <!-- /.rating-reviews -->
            <div class="price-boxst">
               <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"> </i><?php echo number_format($comboprincetoshow); ?></span>
               <?php if($comboval['dicountprice'] < $comboval['price']){ ?>
               <span class="price-strike m-r-12 "> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo number_format($combopricetostrike); ?></span>
               <?php } ?>
               <span class="value1">In Stock</span>
               <div>
                  <?/*div class="btn-group Tl" onClick="removefromcombocart(<?php echo $val['proid']?>, <?php echo $comboval['proid']; ?>)">
                  <a href="javascript:void(0)" class="btn btn-inverse disabled delet"><i class="glyphicon glyphicon-trash"></i></a>
                  </div*/?>
               </div>
            </div>
            <!-- /.price-container -->
         </div>
         <!-- /.product-info -->
      </div>
      <!-- /.col-sm-7 -->
   </div>
</div>
<?php } } ?>
<?php } ?>
<div class="totl">
   <div class=" col-lg-2 inner-top-md-10"></div>
   <div class=" col-lg-3 inner-top-md-10">
      <table cellpadding="10" cellspacing="10" width="100%" border="0">
         <tr>
            <td>
               <h4><b style="font-weight:900">Total</h4>
               </b>
            </td>
            <td> : </td>
            <td>
               <h4 style="font-weight: 700;"> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo number_format($totalprice); ?></h4>
            </td>
         </tr>
      </table>
   </div>
   <div class=" col-lg-3 inner-top-md-10 pull-right"> 
      <a href="<?php echo SITEMOBURL; ?>product/viewcart"><button type="button" class="btn orderbtn">View Cart</button></a>
   </div>
</div>
<?php } ?>
</div>
</div>
<style>
   div {color: #000;}
</style>
<script>
   function removefromcombocart(proid, comboproid){
      var pid=proid;
      var comboproid=comboproid;
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/removefromcombocart',
         type:'POST',
         data:{pid:pid, combo_id:comboproid},
         success:function(data)
         {   var response = jQuery.parseJSON(data);
           
            if(response.error=='false'){
                 alert('Product  removed successfully'); 
                 window.location.reload();
               }else{
                alert('Error in removing  product from cart'); 
               }
            
         }
   
      });
      
       
   }
   
   function removefromcart(ele) {
   	
      var pid=ele;
      
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/removefromcart',
         type:'POST',
         data:{pid:pid},
         success:function(data)
         {   var response = jQuery.parseJSON(data);
           
            if(response.error=='false'){
                 alert('Product  removed successfully'); 
                 window.location.reload();
               }else{
                alert('Error in removing  product from cart'); 
               }
            
         }
   
      });
      
       
   }
</script>