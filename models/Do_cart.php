<?php

 class Do_cart extends CI_Model{
	
	function __construct(){
		parent:: __construct();
		$this->load->database();
	}

	function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Do_cart.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . $query.PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/database_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	}

    function addtocart($param){
		if($_SESSION['cart']!=''){
			$cart=$_SESSION['cart'];	
		}
		$cart[$param['pid']]['pid']=$param['pid'];
		$cart[$param['pid']]['qty']=$param['qty'];
		
		if($param['sid']){
		 $cart[$param['pid']]['sid']=$param['sid'];	
		}
		if($param['selected_combo_pro_id']){
			 $cart[$param['pid']]['combo_product'][$param['selected_combo_pro_id']]['pid']=$param['selected_combo_pro_id'];	
             $cart[$param['pid']]['combo_product'][$param['selected_combo_pro_id']]['qty']=$param['qty'];				 
		}
		
		$_SESSION['cart']=$cart;
        //print_R($_SESSION); die;		
		
	    return true;
	}
	
	function getcurrentcart($params=null){
		if($_SESSION['cart']){
			//echo 'something';
			//echo '<pre>';
			//print_r($_SESSION['cart']); die;
			 //print_r($_SESSION['cart']); die; 
			foreach($_SESSION['cart'] as $key=>$value){
				//echo 'value';
				//print_r($value); die;
				//print_R($value['combo_product']); die;
                if($value['sid']){
				   $select="select cpm.productId as proid, cpm.productName as proname, cpm.onlinePrice, cpm.clearanceMRP as discountprice, cpm.review, cpm.productImage1 as prothumbnail, cpm.productImage2, cpm.productImage3, cpm.productImage4, cpm.clearanceId, cpm.clearancePercentage, cpm.clearanceMRP, cpm.serialid, cpm.clearanceAvailable, cpm.isAvailable, bm.brandid, pc.category_path FROM clerance_product_master cpm LEFT JOIN product_to_category pc ON cpm.productid=pc.proid LEFT JOIN brand_master bm ON cpm.brandid=bm.brandid where cpm.productId='".$key."' and serialid='".$value['sid']."'";    
				}else{
					$select="select pm.proweight, pm.proid, pm.onlineprice, pm.discountprice, pm.proname, pm.prothumbnail, pm.isAvailable, pc.category_id, bm.brandid, pc.category_path, isBook, isPro from product_master pm join product_to_category pc on pc.proid=pm.proid LEFT JOIN brand_master bm ON pm.brandid=bm.brandid WHERE pm.proid='".$key."'";
				}
				$res=$this->db->query($select);
                $this->log_fileDetails('getcurrentcart', $select);
				$res2=$res->result_array();
                   
				if($res2){
					$row=$res2[0];
					$result[$key]['proid']=$row['proid'];
					$result[$key]['proname']=$row['proname'];
					$result[$key]['image']=str_replace('http:', 'https:', $row['prothumbnail']);
					$result[$key]['price']=$row['onlineprice'];
					$result[$key]['weight']=$row['proweight'];
					$result[$key]['stock']=$row['proweight'];
					$result[$key]['catid']=$row['category_id'];
					$result[$key]['isAvailable']=$row['isAvailable'];
					$result[$key]['category_path']=$row['category_path'];
					$result[$key]['brandid']=$row['brandid'];
					// $result[$key]['onlineprice']=$row['onlineprice'];
					$result[$key]['qty']=$value['qty'];
					if($row['discountprice']!='' && !isset($value['couponid'])){
						$result[$key]['dicountprice']=$row['discountprice'];
						$discount=$row['onlineprice']-$row['discountprice'];
						$dis_per=($discount/$row['onlineprice'])*100;
						$dis_per= ceil($dis_per);
						$result[$key]['precentage']=$dis_per;
					}else{
						$result[$key]['dicountprice']=$value['dicountprice'];
						$discount=$row['onlineprice']-$value['dicountprice'];
						$dis_per=($discount/$value['onlineprice'])*100;
						$dis_per= ceil($dis_per);
						$result[$key]['precentage']=$dis_per;
					}

					if($row['discountprice']!=''){
						$result[$key]['amount']=$result[$key]['dicountprice'] * $value['qty']; //this is final amount
						$result[$key]['amountcart']=$row['discountprice'] * $value['qty'];
					}else{
						$result[$key]['amount']=$result[$key]['onlineprice']*$value['qty']; //this is final amount
						$result[$key]['amountcart']=$row['onlineprice'] * $value['qty'];
					}
					//print_r($value['combo_product']); 
					if(is_array($value['combo_product'])){
						//print_r($value['combo_product']);echo 'iii';
						$comboarrar = $this->addcombo($value['combo_product'],$row['category_id'],$row['proid']);
						if($comboarrar){
							$result[$key]['combo_product']= $comboarrar;
							//print_r($result[$key]['combo_product']);die;
						}
					}

					if($value['couponcode'] && 	!empty($value['couponcode']) && $params['module']!='cancelcoupon'){
						$result[$key]['couponapplied']=$value['couponapplied'];
						$result[$key]['couponid']=$value['couponid'];
						$result[$key]['couponcode']=$value['couponcode'];
						$result[$key]['coupontype']=$value['coupontype'];
						$result[$key]['coupondiscount']=$value['coupondiscount'];
						$result[$key]['amountdeducted']=$value['amountdeducted'];
						$result[$key]['couponappliedbool']=$value['couponappliedbool'];
						$result[$key]['coupondisctype']=$value['coupondisctype'];
						$result[$key]['offertype']=$value['offertype'];
						/*$result['coupondata']['couponapplied']=$value['couponapplied'];
						$result['coupondata']['couponid']=$value['couponid'];
						$result['coupondata']['couponcode']=$value['couponcode'];
						$result['coupondata']['coupontype']=$value['coupontype'];
						$result['coupondata']['coupondiscount']=$value['coupondiscount'];
						$result['coupondata']['amountdeducted']=$value['amountdeducted'];*/
					}
					//set the main type for the product
					if($row['isBook']=='1' && $row['isPro']=='0'){
						$result[$key]['maintype']='2';
					}else if($row['isBook']=='0' && $row['isPro']=='1'){
						$result[$key]['maintype']='3';
					}else if($row['isBook']=='0' && $row['isPro']=='0'){
						$result[$key]['maintype']='1';
					}
				}
			}
        }
        //echo 'results';
        return $result;
    }
             
	
	function addcombo($param,$catid,$proid){
		//print_R($param); die;
		 foreach($param as $row){
		 	$result[$row['pid']]['qty']+=$row['qty'];
	        $select="select  pm.proweight,pm.proid,pm.onlineprice,pm.discountprice,pm.proname,pm.prothumbnail,pm.isAvailable ,((pm.onlinePrice/100)*(100-cpm.offer_perc)) as combo_price,((pm.onlinePrice/100)*(cpm.offer_perc)) as combo_saving from product_master pm join combo_product_masters cpm on cpm.product_id=pm.proid where pm.proid =".$row['pid']." and (find_in_set(".$catid.",main_category) or main_product in(".$proid."))";
	        //echo $select; die;
		 $res=$this->db->query($select);
				
				
				
				$res2=$res->result_array();
                   
				if($res2){
						$row=$res2[0];
							
						 $result[$row['proid']]['proid']=$row['proid'];
						 $result[$row['proid']]['proname']=$row['proname'];
						 $result[$row['proid']]['image']=$row['prothumbnail'];
						 $result[$row['proid']]['price']=$row['onlineprice'];
						 $result[$row['proid']]['weight']=$row['proweight'];
						 $result[$row['proid']]['stock']=$row['proweight'];
						 $result[$row['proid']]['onlineprice']=$row['onlineprice'];
						 //$result[$row['proid']]['qty']=$row['onlineprice'];
							
						 // $result[$row['proid']]['qty']=$row['qty'];
						 
						 if($row['combo_price']!=''){
							 
							  $result[$row['proid']]['dicountprice']=$row['combo_price'];
							  

						  }
						  
						 if($row['discountprice']!=''){
								$result[$row['proid']]['amount']=$result[$key]['combo_price']*$value['qty'];
						}else{
							$result[$row['proid']]['amount']=$result[$key]['combo_price']*$value['qty'];
						}
			
			}	
		 }	
		// print_r($result);die;
				  return $result;
		
	}
	function addtobundle($param){
		//print_R($param);
		if($_SESSION['final_bundle']!=''){
			$bundlecart=$_SESSION['final_bundle'];	
		}
		$bundlecart[$param['pid']]['pid']=$param['pid'];
		$bundlecart[$param['pid']]['qty']=$param['qty'];
		
		$_SESSION['final_bundle']=$bundlecart; 

		
	    return true;
	}
	
	function getbundlecart($params=null){
			
		if($_SESSION['final_bundle'])
        {	
			 //print_r($_SESSION['bundlecart']); 
			
			foreach($_SESSION['final_bundle'] as $key=>$value){
                  if($value['sid']){
				   $select="select productId as proid, productName as proname,onlinePrice,clearanceMRP as discountprice,review,productImage1 as prothumbnail,productImage2,productImage3,productImage4, clearanceId,clearancePercentage,clearanceMRP,serialid,
		           clearanceAvailable isAvailable FROM clerance_product_master where productId='".$key."' and serialid='".$value['sid']."'";    
				  }else{
				 $select="select proweight,proid,onlineprice,discountprice,proname,prothumbnail,isAvailable  from  product_master WHERE proid='".$key."'"; 
				  }				 
				$res=$this->db->query($select);
				
				
				$res2=$res->result_array();
                  //print_R($res2); 
				if($res2){
					$row=$res2[0];
					
						
					 $result[$key]['proid']=$row['proid'];
					 $result[$key]['proname']=$row['proname'];
					 $result[$key]['image']=$row['prothumbnail'];
					 $result[$key]['price']=$row['onlineprice'];
					 $result[$key]['weight']=$row['proweight'];
					 $result[$key]['stock']=$row['proweight'];
					 // $result[$key]['onlineprice']=$row['onlineprice'];
						
					  $result[$key]['qty']=$value['qty'];
					 
					 if($row['discountprice']!=''){
						 
						  $result[$key]['discountprice']=$row['discountprice'];
						  $discount=$row['onlineprice']-$row['discountprice'];
						  $dis_per=($discount/$row['onlineprice'])*100;
						 
						  $dis_per= ceil($dis_per);
						  $result[$key]['precentage']=$dis_per;

					  }
                      
					 if($row['discountprice']!=''){
							$result[$key]['amount']=$row['discountprice']*$value['qty'];
					}else{
						$result[$key]['amount']=$row['onlineprice']*$value['qty'];
					}

					
					}
                
					
			
				
			
				

		  }
		
		
        }
       
            
                  return $result;
	}
	
	public function getquote($param){
		$this->load->library('encryption');
		$usrid = $this->encryption->decrypt($_SESSION['usrid']);
			
		  $ins_sql = "INSERT INTO userBundleMaster 
					  SET bdName = '".$_SESSION['usrname']."', 
						bdEmail = '".$_SESSION['usremail']."', 
						bdContact = '".$_SESSION['usrnum']."', 
						bdPincode = '".$_SESSION['pincode']."', 
						bd_Comment = '".$param['comment']."', 
						userId = '".$usrid."',
						ipAddress = '".$param['ip']."',
						insertDate = NOW(),
						status = 1";  

              $res=$this->db->query($ins_sql);
             if($res){  			  
               $bundleId = $this->db->insert_id();
				 //print_R($_SESSION['bundlecart']);
                if($bundleId!=0){
					foreach($_SESSION['final_bundle'] as $key=>$value){
					 $select="select onlinePrice, discountPrice from product_master where proid='".$key."'";	
					$res=$this->db->query($select);
					if($res){
					 $res1=$res->result_array();
					 if($res1[0]['onlinePrice'] >$res1[0]['discountPrice']){
                     $bundleprice['bundle_price']	=$res1[0]['discountPrice'];			 
					}else if($res1[0]['onlinePrice'] == $res1[0]['discountPrice']){
						$bundleprice['bundle_price']	=$res1[0]['onlinePrice'];	
					}else{
						
						$bundleprice['bundle_price']	=$res1[0]['onlinePrice'];	
					}
				$insertDetailsSql = "INSERT INTO userBundleDetails
									  SET bundleId = ".$bundleId.",
										  productId = ".$key.",
										  productPrice = '".$bundleprice['bundle_price']."',
										  quantity = '".$value['qty']."',  
										  status = 1,
										  ipAddress = '".$param['ip']."',
										  insertDate = NOW()";  
				$insertDetails = $this->db->query($insertDetailsSql);
				
				
				// Insert into adminBundleDetails Details Table
			$ins_sql2 = "INSERT INTO userbundleAdminDetails
						SET bundleId = '".$bundleId."',
							productId = '".$key."',
							productPrice = '".$bundleprice['bundle_price']."',
							quantity = '".$value['qty']."',
							status = 1,
							insertDate = NOW(),
							ipAddress = '".$param['ip']."'"; 
				 $ins_res = $this->db->query($ins_sql2);
                 if($insertDetails && $ins_res){
                 	
					 return true;
					 
				 } 				 
					}
					
				}				
             }
	            $data['bundleId']=$bundleId;
	         	$data['usrname']=$_SESSION['usrname'];
	         	$data['usremail']=$_SESSION['usremail'];
	         	$data['comment']=$param['comment'];

         	return $data;
          }
        }
		
		public function getbundlecartdetail($param){
			 $select="SELECT * FROM `userBundleMaster` WHERE bdEmail = '".$_SESSION['usremail']."' AND id = '".$param['bundleid']."' LIMIT 1";
			 $res = $this->db->query($select);
			 if($res){
				$res1=$res->result_array();
                if($res1){
					$selBundle_sql = "SELECT pm.proname, pm.onlinePrice, pm.discountPrice, pm.prothumbnail, pm.proweight, pm.promotionId, pm.clearanceId, pm.clearanceMRP,bm.productId,bm.productPrice,bm.quantity
                    FROM userbundleAdminDetails as bm, product_master as pm
                    WHERE bm.productId = pm.proid AND bm.bundleId = ".$param['bundleid']." AND bm.status = 1";
					 $res2 = $this->db->query($selBundle_sql);
					 if($res2){
						 $res3=$res2->result_array();
						 return $res3;
						 
					 }
				}
			 }
			
		} 
		
		public function checkgiftcode($param){
		   	$param['order_id'] =$_SESSION['orderinvoice_no'];
			$select="SELECT * FROM `order_giftcard_details` WHERE user_email_id = '".$param['emailid']."' AND gift_card_id = '".$param['giftcardid']."' LIMIT 1";
			$res = $this->db->query($select);
			if($res->num_rows() > 0){
				return true;
			}else{
				$today=date('Y-m-d');
				$select1="select * from coupon where code='".$param['giftcardid']."' and date_start <='".$today."' and date_end >= '".$today."' LIMIT 1";
				$res1 = $this->db->query($select1);
				if($res1->num_rows() > 0){
					$alldata=$res1->result_array();
					$coupontype=$alldata[0]['type'];
					$select2="select customer_id from customer where email='".$param['emailid']."'";
					$resselect=$this->db->query($select2);
					if($resselect){
						//$resselectdata=$this->db->query($resselect);
						$resselectdata1=$resselect->result_array();
						$cutomer_id=$resselectdata1[0]['customer_id'];
					}

					if($coupontype==1){
						if($alldata[0]['uses_customer']){
							$param['emailid']==$alldata[0]['uses_customer'];
							 $couponhistory="select * from  coupon_history where coupon_id='".$param['giftcardid']."' and customer_id=".$cutomer_id." and  date_added  between ".$param['date_start']."  and ".$param['date_end'].""; 
							$couponhistorydtl = $this->db->query($couponhistory);
							if ($couponhistorydtl->num_rows() > 0){
								return $mesg['mesg']="The coupon is valid for one time use only";
							}else{
								
								if($alldata[0]['product_id']=='1' && $alldata[0]['category_id']=='1' &&  $alldata[0]['brand_id']=='1'){
									return $alldata;
								}
								else{
									$selectallpro="select op.product_id  from  order_product op join product_master pm on op.product_id=pm.proid join brand_master bm on pm.brandid=bm.brandid  join product_to_category pc on pc.proid=pm.proid where order_id=".$param['order_id'].""; 
									if($alldata[0]['category_id']!='1'){
										$selectallpro.=" and find_in_set (pc.category_id,'".$alldata[0]['category_id']."')";
								    }
									if($alldata[0]['product_id']!='1'){
										$selectallpro.=" or find_in_set (pm.proid,'".$alldata[0]['product_id']."')";
									}
									if($alldata[0]['brand_id']!='1'){
										$selectallpro.=" or find_in_set (pm.brandid,'".$alldata[0]['brand_id']."')";
									} 
									$respro = $this->db->query($selectallpro);
									if ($couponhistorydtl->num_rows() > 0){
										return $alldata;
										
									}else{
										return $mesg['mesg']="The coupon is not  valid on this order";
									}
								}
							}
						}else{
							return $mesg['mesg']="The cupon code is not valid for this user";
						}  
					  }else if($coupontype==2){
						
						  $customercount= $alldata[0]['uses_total'];
						  //echo $customercount; die;
						  if($customercount){
						     $couponhistory="select * from  coupon_history where coupon_id='".$param['giftcardid']."' and  customer_id=".$cutomer_id." and  date_added  between '".$alldata[0]['date_start']."'  and '".$alldata[0]['date_end']."'"; 
							$couponhistorydtl = $this->db->query($couponhistory);
							if ($couponhistorydtl->num_rows() > 0){
								return $mesg['mesg']="The coupon is valid for one time use only";
							}else{
								if($alldata[0]['product_id']=='1' && $alldata[0]['category_id']=='1' &&  $alldata[0]['brand_id']=='1'){
									
									return $alldata;
								}
								else{
									 $selectallpro="select op.product_id  from  order_product op join product_master pm on op.product_id=pm.proid join brand_master bm on pm.brandid=bm.brandid  join product_to_category pc on pc.proid=pm.proid where order_id=".$param['order_id'].""; 
									if($alldata[0]['category_id']!='1'){
										$selectallpro.=" and find_in_set (pc.category_id,'".$alldata[0]['category_id']."')";
								    }
									if($alldata[0]['product_id']!='1'){
										$selectallpro.=" or find_in_set (pm.proid,'".$alldata[0]['product_id']."')";
									}
									if($alldata[0]['brand_id']!='1'){
										$selectallpro.=" or find_in_set (pm.brandid,'".$alldata[0]['brand_id']."')";
									} 
									echo $selectallpro; //die;
									$respro = $this->db->query($selectallpro);
									if ($respro->num_rows() > 0){
										return $alldata;
										
									}else{
										return $mesg['mesg']="The coupon is not  valid on this order";
									}
								}
							}
						  
						  }else{
							   return $mesg['mesg']="The cupon code is not valid for this user";
							  
						  }
						  
						  
					  }else if($coupontype==3){
						  $selectallpro="select op.product_id  from  order_product op join product_master pm on op.product_id=pm.proid join brand_master bm on pm.brandid=bm.brandid  join product_to_category pc on pc.proid=pm.proid where order_id=".$param['order_id'].""; 
									if($alldata[0]['category_id']!='1'){
										$selectallpro.=" and find_in_set (pc.category_id,'".$alldata[0]['category_id']."')";
								    }
									if($alldata[0]['product_id']!='1'){
										$selectallpro.=" or find_in_set (pm.proid,'".$alldata[0]['product_id']."')";
									}
									if($alldata[0]['brand_id']!='1'){
										$selectallpro.=" or find_in_set (pm.brandid,'".$alldata[0]['brand_id']."')";
									} 
									$respro = $this->db->query($selectallpro);
									if ($couponhistorydtl->num_rows() > 0){
										return $alldata;
										
									}else{
										return $mesg['mesg']="The coupon is not  valid on this order";
									}
						  
						  
						  
					  }else if($coupontype==4){
						  $selectorderamount="select total_amount from order_master where orderinvoice_no=".$param['order_id']."";
						  $respro = $this->db->query($selectorderamount);
							if ($respro->num_rows() > 0){
										$resamt= $respro->result_array();
										if($resamt[0]['total_amount']>=$alldata[0]['total']){

											  /*$selectallpro="select op.product_id  from  order_product op join product_master pm on op.product_id=pm.proid join brand_master bm on pm.brandid=bm.brandid  join product_to_category pc on pc.proid=pm.proid where order_id=".$param['order_id'].""; 
												if($alldata[0]['category_id']!='1'){
													$selectallpro.=" and find_in_set (pc.category_id,'".$alldata[0]['category_id']."')";
												}
												if($alldata[0]['product_id']!='1'){
													$selectallpro.=" or find_in_set (pm.proid,'".$alldata[0]['product_id']."')";
												}
												if($alldata[0]['brand_id']!='1'){
													$selectallpro.=" or find_in_set (pm.brandid,'".$alldata[0]['brand_id']."')";
												}*/
												$selectallpro="select op.product_id  from  order_product op join product_master pm on op.product_id=pm.proid join brand_master bm on pm.brandid=bm.brandid  join product_to_category pc on pc.proid=pm.proid where order_id=".$param['order_id'].""; 
												if($alldata[0]['category_id']!='1'){
													$selectallpro.=" and find_in_set (pc.category_id,'".$alldata[0]['category_id']."')";
												}
												if($alldata[0]['product_id']!='1'){
													$selectallpro.=" or pm.proid='all'";
												}
												if($alldata[0]['brand_id']!='1'){
													$selectallpro.=" or pm.brandid='all'";
												}

												//echo $selectallpro; die;
												$respro = $this->db->query($selectallpro);
												if ($respro->num_rows() > 0){
													return $alldata;
													
												}else{
													return $mesg['mesg']="The coupon is not  valid on this order";
												}
											
										}else{
											
											return $mesg['mesg']="The minimum amount required is Rs.".$alldata[0]['total']."";
										}
										
									}else{
										return $mesg['mesg']="The coupon is not  valid on this order";
									}
						
						  
					}
				}
			}
		}
		
		public function getgiftcodevalue($param){
			 $select="SELECT * FROM `order_giftcard_details` WHERE user_email_id = '".$param['emailid']."' AND gift_card_id = '".$param['giftcardid']."' and  validity >=  NOW() and gift_card_redemption='0' LIMIT 1 ";
			 $res = $this->db->query($select);
			 if($res){
				$res1=$res->result_array();
				
				return $res1;
			 }else{
				 
				return false; 
			 }
			
			
		}

	public function validategiftcoupon($param){
		if($param['promocode'] && $param['user_email']){
			$selectcoupon="SELECT * FROM coupon WHERE DATE(date_start) <= '".$param['curdate']."' AND DATE(date_end) >='".$param['curdate']."' AND status='1' AND code='".$param['promocode']."'";
			$res=$this->db->query($selectcoupon);
			if($res->num_rows() > 0){
				return $res->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
		//return $data;
	}

	public function couponusagecount($param){
		$select="SELECT COUNT(orderid)cnt FROM order_master WHERE coupon_or_gift_code='".$param['couponcode']."' AND DATE(	orderdate_added) >='".$param['startdate']."' AND DATE(orderdate_added) <='".$param['enddate']."'";
		$res=$this->db->query($select);
		$results=$res->result_array();
		return $results[0]['cnt'];
	}

	public function removefromcart($param){
		$delete="DELETE FROM order_product WHERE order_id=".$param['orderinvoice_no']." AND product_id=".$param['pid']."";
		$res=$this->db->query($delete);
		if($res){
			//now manage main order total amount
			$amount=0;
			$finalamount=0;
			$shipping_charges=0;
			$select="SELECT onlinePrice, discountPrice FROM product_master WHERE proid=".$param['pid']."";
			$resselect=$this->db->query($select);
			$resultsselect=$resselect->result_array();
			$onlineprice=$resultsselect[0]['onlinePrice'];
			$discountprice=$resultsselect[0]['discountPrice'];
			//$amount=($onlineprice > $discountprice) ? $discountprice : $onlineprice;
			$update="UPDATE order_master SET total_amount=".$amount.", total_courier_charges=".$shipping_charges.", final_amount_to_pay=".$finalamount." WHERE orderinvoice_no=".$param['orderinvoice_no']."";
			$resupdate=$this->db->query($update);
			return true;
		}
		return false;
	}
}
?>
