 <?
 class Lib_home{
	        public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_common');
             $CI->load->model('Do_home');
             $CI->load->library('libsession');
			 $CI->load->library('libapi');
			 $CI->load->library('lib_filters');
			 $CI->load->library('lib_common');
             $this->ci = $CI;
          }



         public function get_home_dtl(){


           $memkey = 'lib_home_get_home_dtl';
           $memCacheDate = $this->ci->libsession->getMemcache($memkey);
           if($memCacheDate){
             return $memCacheDate;
           }



       $res['banner']=$this->ci->Do_home->gethome_mobilebanner();
       $res['webbanner']=$this->ci->Do_home->gethome_desktopbanner();
	     $res['featured_cat']=$this->ci->Do_home->gethome_featuredcat();
	     $res['hotdeal_banner']=$this->ci->Do_home->gethotdeal_banner();
	     $res['featured_pro']=$this->ci->Do_home->gethome_featuredproduct();


       $wishlist=$this->ci->libsession->getSession('wishlist');


       if($res['featured_pro']){
				   foreach($res['featured_pro'] as $row){
					    if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 //print_r($arrwishlist);
						 if(in_array($row['proid'],$arrwishlist)){
							 $prodata['product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$row['proid']]['wishlist']='0';
					 }
					//echo $row['prothumbnail'];
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;

					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					 $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];

					 if($row['prothumbnail']){

					        $prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
					        //$prodata['product'][$row['proid']]['img']=str_replace('http', 'https', $row['prothumbnail']);
					    }else{
						    $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
					    }

					//echo $prodata[$row['proid']]['img'];die;
					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
						$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
				   }
				 }

				 if($res['featured_cat']){
				   foreach($res['featured_cat'] as $row){

					 $catname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['catname']);
					 $catname=str_replace("--","-", $catname) ;
					 $catname=strtolower(rtrim($catname,'-'));

					 $catid=$row['catid'];
					 $prodata['category'][$row['catid']]['id']= $row['catid'];
					 //$prodata['category'][$row['catid']]['url']=''.SITEURL.''.$catname.'/'.$row['catid'].'';
					 $prodata['category'][$row['catid']]['url']=''.SITEMOBURL.''.$catname.'/'.$row['catid'].'';
                     $prodata['category'][$row['catid']]['name']=$catname;
                     if($_SERVER['REMOTE_ADDR']=='111.91.17.62'){
						 //echo $row['catimage']; die;
					}

					           if($row['catimage']){

							 $prodata['category'][$row['catid']]['img']=(stripos($row['catimage'], 'http:')!==false) ? str_replace('http:', 'https:', $row['catimage']) : $row['catimage'];
							 }else{
							   $prodata['category'][$row['catid']]['img']=SITEIMAGES.'mobimg/noimage1.png';;
							 }

				   }
				 }

				 if($res['banner']){
					 foreach($res['banner'] as $row){
					 $prodata['banner'][$row['id']]['imgpath']=(stripos($row['img_path'], 'http:')!==false) ? str_replace('http:', 'https:', $row['img_path']) : $row['img_path'];
					 $prodata['banner'][$row['id']]['url']=(stripos($row['img_link'], 'http:')!==false) ? str_replace('http:', 'https:', $row['img_link']) : $row['img_link'];
                     $prodata['banner'][$row['id']]['sortorder']=$row['img_order'];

			    	}
			  }
  			  if($res['webbanner']){
  					 foreach($res['webbanner'] as $row){
  					 $prodata['webbanner'][$row['id']]['imgpath']= (stripos($row['img_path'], 'http:')!==false) ? str_replace('http:', 'https:', $row['img_path']) : $row['img_path'];
  					 $prodata['webbanner'][$row['id']]['url']=(stripos($row['img_link'], 'http:')!==false) ? str_replace('http:', 'https:', $row['img_link']) : $row['img_link'];
                       $prodata['webbanner'][$row['id']]['sortorder']=$row['img_order'];

  			    	}
  			  }

			    if($res['hotdeal_banner']){
  					 foreach($res['hotdeal_banner'] as $row){
  					 $prodata['hotdeal_banner'][$row['id']]['imgpath']=(stripos($row['image'], 'http:')!==false) ? str_replace('http:', 'https:', $row['image']) : $row['image'];
  					 $prodata['hotdeal_banner'][$row['id']]['url']=(stripos($row['url'], 'http:')!==false) ? str_replace('http:', 'https:', $row['url']) : $row['url'];
                       $prodata['hotdeal_banner'][$row['id']]['sortorder']=$row['priority'];

  			    	}
			    }
        $this->ci->libsession->setMemcache($memkey,$prodata);
			  return $prodata;
	}

	public function get_clerancefilter($param=false){
		$res['filter'] = $this->ci->Do_home->getclerance_filter($param);

		$result = $this->filter($res);
		//print_r($result); die;
		return $result;

	}

	public function get_clerance($param=false){//this function is created by Sandeep on 2017-12-14 because clearnace products shouldn't be clubbed by product id as each product has separate serial id

      $memkey = 'lib_home_get_clerance';
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate && false){
        return $memCacheDate;
      }




    $res['prod_clerance']=$this->ci->Do_home->getclerance_product($param);
		//print_R($res['prod_clerance']);
		$res['banner']=$this->ci->Do_home->get_clearncebanner();
		$wishlist=$this->ci->libsession->getSession('wishlist');
		if($res['prod_clerance']){
					$i=0;
				   foreach($res['prod_clerance'] as $row){
				   	//print_r($row);
					 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 //print_r($arrwishlist);
						 if(in_array($row['productId'],$arrwishlist)){
							 $prodata['product'][$i]['wishlist']='1';
						 }else{
							 $prodata['product'][$i]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$i]['wishlist']='0';
					 }
					//echo $row['prothumbnail'];
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['productName']);
					 $proname=str_replace("--","-", $proname) ;

					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['productId'];
					 $prodata['product'][$i]['id']= $proid;
					 $prodata['product'][$i]['url']=str_replace('http:', 'http:', ''.SITEMOBURL.''.$proname.'/'.$proid.'?sid='.urlencode($row['serialid']).'');
                     $prodata['product'][$i]['name']= $row['productName'];

					if($row['productImage1']){
						 $pathchk=FILEBASEPATH.'uploads/clerance/'.$row['productImage1'];
						//echo $pathchk;
						 if($pathchk){
						   $prodata['product'][$i]['img']=str_replace('http:', 'http:', GENERALURL.'uploads/clerance/'.$row['productImage1']);
						 }else{
							$prodata['product'][$i]['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }

					}else{
						$prodata['product'][$i]['img']=SITEIMAGES.'mobimg/noimage1.png';
					}
					//echo $prodata[$row['proid']]['img'];die;
					$prodata['product'][$i]['price']=$row['onlinePrice'];
					$setprice=$row['onlinePrice']-$row['clearanceMRP'];
					$prodata['product'][$i]['percentage']= round($setprice/$row['onlinePrice']*100);

					$prodata['product'][$i]['discountprice']=$row['clearanceMRP'];
					$prodata['product'][$i]['serialid']=$row['serialid'];
					$prodata['product'][$i]['clearanceAvailable']=$row['clearanceAvailable'];
				   	$i++;
				   }
				 }
				$prodata['banner'][0]=(stripos($res['banner'][0]['bannerImage'], 'http:')!==false) ? str_replace('http:', 'https:', $res['banner'][0]['bannerImage']) : $res['banner'][0]['bannerImage'];
				$prodata['banner'][1]=(stripos($res['banner'][0]['logoImage'], 'http:')!==false) ? str_replace('http:', 'https:', $res['banner'][0]['logoImage']) : $res['banner'][0]['logoImage'];

        $this->ci->libsession->setMemcache($memkey,$prodata);
		    return $prodata;

	}

	//===========================================================================

	public function get_featuredproductfilter($param=false){
		$res['filter'] = $this->ci->Do_home->getfeaturedproduct_filter($param);
		$result = $this->filter($res);
		return $result;

	}

	//===========================================================================

	//===========================================================================

	function filter($res){
		//print_r($res); die;
		if($res['filter']){
			$i = 0;
			foreach ($res['filter']['brand'] as $key => $val) {
				$filterdata['brand'][$i]['brandid']    = $val['brandid'];
				$filterdata['brand'][$i]['brandname']  = $val['brandname'];
				$filterdata['brand'][$i]['brandcount'] = $val['brandcount'];
				$i++;
			}
			$j = 0;
			foreach ($res['filter']['available'] as $key => $val) { //print_r($val);
				$filterdata['available'][$j]['clearanceid'] = $val['clearanceid'];
				$filterdata['available'][$j]['count']       = $val['count'];
				$j++;
			}
			$k = 0;
			foreach ($res['filter']['category'] as $key => $val) {
				//$filterdata['categorydtl'][$k]['caturl']   = ADMINURL.'product/featuredproducts?categoryid='.$val['category_id'];
				$filterdata['categorydtl'][$k]['category'] = $val['catname'];
				$filterdata['categorydtl'][$k]['categoryid'] = $val['category_id'];
				$filterdata['categorydtl'][$k]['productcount'] = $val['productcount'];
				$k++;
			}
				$filterdata['minprice'] = $res['filter']['price'][0]['minamount'];
				$filterdata['maxprice'] = $res['filter']['price'][0]['maxamount'];

			foreach($res['filter']['discount'] as $key=>$val){
					$filterdata['discount'][$val['discount']]=$val['discount'];
			}
		}
		return $filterdata;
	}

	//===========================================================================

	 public  function getfeaturedproduct($param=false){
	 	//$memkey='featured';
	 	$param['count']=true;
	 	$rescount=$this->ci->Do_home->gethome_featuredproduct($param);
	 	$total_rows=$rescount[0]['cnt'];
	 	$config["base_url"] = ADMINURL.'product/featuredproducts';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");

		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();
	 	$param['count']=false;
		$res['featured_pro']=$this->ci->Do_home->gethome_featuredproduct($param);
		
			 $wishlist=$this->ci->libsession->getSession('wishlist');
			 $i=0;
			 if($res['featured_pro']){
				   foreach($res['featured_pro'] as $row){
					    if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 //print_r($arrwishlist);
						 if(in_array($row['proid'],$arrwishlist)){
							 $prodata['product'][$i]['wishlist']='1';
						 }else{
							 $prodata['product'][$i]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$i]['wishlist']='0';
					 }
					//echo $row['prothumbnail'];
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;

					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$i]['id']= $proid;
					 $prodata['product'][$i]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$i]['name']=$row['proname'];

					 if($row['prothumbnail']){
						 	//$image1=str_replace("http://furtadosonline.com/",FILEBASEPATH,$row['prothumbnail']);
						 	$image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
						 	$image1=str_replace("uploads","upload",$image1);
							//echo 	$image1; die;

						 	//if(file_exists($image1)){
						      $prodata['product'][$i]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];

							// }else{
							//	$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							// }
					 }else{
							$prodata['product'][$i]['img']=SITEIMAGES.'mobimg/noimage1.png';
					 }
					//echo $prodata[$row['proid']]['img'];die;
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$i]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['product'][$i]['price']=$row['onlinePrice'];
					$prodata['product'][$i]['callforprice']=$row['callforprice'];
					$prodata['product'][$i]['avaliable']=$row['isAvailable'];
					//$prodata['product'][$i]['discountPrice']=$row['discountPrice'];
					$prodata['product'][$i]['discountprice']=$row['discountPrice'];
					$i++;
				   }
				 }
				//print_r($prodata); echo 'hiii';	 die;
		        return $prodata;

	}
	public function getfeaturedproductX($param, $paramx){
		$memkey = 'lib_home_newarrival'.json_encode($param);//.json_encode($param);
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//echo 'under memcache';
			//var_dump($memCacheDate);
			// return $memCacheDate;
		}
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8080/listing/api/v1.0/1/featured_home/".$param['start']."/".$param['limit']."/0.0?".$paramx['available'].$paramx['sort'];
		$base_url = "http://3.210.70.215:8080/listing/api/v1.0/1/featured_home/".$param['start']."/".$param['limit']."/0.0?".$paramx['available'].$paramx['sort'];
		//var_dump($url);	//die();	
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.'product/featuredproducts';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$param['isFilters']=true;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
					return $prodata;
	  }
	}	   
	
	
	 public function get_hot_deal(){
		 $res['featured_pro']=$this->ci->Do_home->gethome_featuredproduct();
		//print_r($res['featured_pro']); echo 'hiii';
			 $wishlist=$this->ci->libsession->getSession('wishlist');
			 //echo 	 $wishlist; die;
			 if($res['featured_pro']){
				   foreach($res['featured_pro'] as $row){
					    if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 //print_r($arrwishlist);
						 if(in_array($row['proid'],$arrwishlist)){
							 $prodata['product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$row['proid']]['wishlist']='0';
					 }
					//echo $row['prothumbnail'];
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;

					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					 $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];

					 if($row['prothumbnail']){
						 	$image1=str_replace("http://furtadosonline.com/",FILEBASEPATH,$row['prothumbnail']);
							// echo 	$image1; die;
					 if(file_exists($image1)){

					      $prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
						 }else{
							$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }
					 }else{
							$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }
					//echo $prodata[$row['proid']]['img'];die;
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
						$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
				   }
				 }

		        return $prodata;



	 }

	 public function newarrival($param=false){

				 $memkey = 'lib_home_newarrival'.json_encode($param);//.json_encode($param);
         $memCacheDate = $this->ci->libsession->getMemcache($memkey);
         if($memCacheDate){
         	//echo 'under memcache';
         	//var_dump($memCacheDate);
           	return $memCacheDate;
         }
         	//echo 'under DB';
         	$param['count']=true;
         	$rescount=$this->ci->Do_home->gethome_newarrival($param);
         	$total_rows=$rescount[0]['cnt'];
         	$config["base_url"] = ADMINURL.'product/newarrivals';
			$config['total_rows'] =$total_rows;
			$config['per_page'] = LIMIT;
			$config['uri_segment']=3;
			$config['enable_query_strings'] = TRUE;
			if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
			$this->ci->pagination->initialize($config);
			$prodata['links']=$this->ci->pagination->create_links();

			$param['count']=false;
		    $res1=$this->ci->Do_home->gethome_newarrival($param);
			$wishlist=$this->ci->libsession->getSession('wishlist');
        	if($res1){
					if($res1){

					 foreach($res1 as $row){
						if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($row['proid'],$arrwishlist)){
							 $prodata['product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$row['proid']]['wishlist']='0';
					 }
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
					  $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];
                     $prodata['product'][$row['proid']]['catname']=$row['catname'];
					 $prodata['maincatname']=$row['catname'];
					 if($row['prothumbnail']){
						 //echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;

							//$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
							$image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
						 	$image1=str_replace("uploads","upload",$image1);
							// echo 	$image1; die;
					 // if(file_exists($image1)){
							 //echo "hi"; die;
							$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
						 // }else{
							// $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						 // }
					 }else{

						$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';

					 }
					//echo $prodata[$row['proid']]['img'];die;
					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];

					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];


					}
          $this->ci->libsession->setMemcache($memkey,$prodata,600);
					return $prodata;
			}
		   }
	   }

	   ////////////////////////////// Elastic Search Functions for HotDeals , New Arrival , and New Arrival Books. /////////////////////////////
	public function newarrivalX($param, $paramx){
		$memkey = 'lib_home_newarrivalbooks'.json_encode($param);;
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//print_r($memCacheDate); die;
		  // return $memCacheDate;
		}
		
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8080/listing/api/v1.0/1/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		$base_url = "http://3.210.70.215:8080/listing/api/v1.0/1/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		//var_dump($url);	die();	
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.'product/newarrivals';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$param['isFilters']=true;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
			return $prodata;
		}
	}
		
	   public function newarrivalX_old($param='', $paramx=''){

		$memkey = 'lib_home_newarrival'.json_encode($param);//.json_encode($param);
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//echo 'under memcache';
			//var_dump($memCacheDate);
			// return $memCacheDate;
		}

		// $rescount=$this->ci->Do_home->gethome_hotdeal($param);
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8000/listing/api/v1.0/1/general/".$param['pagestart']."/".$param['limit']."/0?orderby=proadddate&ordertype=desc";
		
		$base_url = "http://3.210.70.215:8000/listing/api/v1.0/1/general/".$param['pagestart']."/".$param['limit']."/0?orderby=proadddate&ordertype=desc";
		
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.'product/newarrivals';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
					return $prodata;
	  }
	}	   
	public function hotdealsX($param, $paramx){
		$memkey = 'lib_home_hotdeal'.json_encode($param);;
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//print_r($memCacheDate); die;
		  // return $memCacheDate;
		}
		
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8080/listing/api/v1.0/1/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		$base_url = "http://3.210.70.215:8080/listing/api/v1.0/1/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		//var_dump($url);	die();	
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.'product/hotdeals';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$param['isFilters']=true;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
			return $prodata;
		}
	}
	   public function hotdealsX_old($param=false){


        $memkey = 'lib_home_hotdeal'.json_encode($param);
        /*if($param['module']!='homehotdeals'){
        	echo $memkey; die;
        }*/
        $memCacheDate = $this->ci->libsession->getMemcache($memkey);
        if($memCacheDate){
          // return $memCacheDate; commented for now
        }
        if($pagestart){
			$param['pagestart']=$pagestart;
		}else{
			$param['pagestart']=0;
		}
		// $rescount=$this->ci->Do_home->gethome_hotdeal($param);
		// hit url and get response
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
							$url = "http://3.210.70.215:8000/listing/api/v1.0/1/general/0/1/0?min_perc=15&max_perc=99&isavailable=1".rawurlencode($param['search'])."/".$param['pagestart']."/".LIMIT.'?x=';
							$base_url = "http://3.210.70.215:8000/listing/api/v1.0/1/general/".$param['pagestart']."/".LIMIT."/0?min_perc=15&max_perc=99&isavailable=1";
							if( isset($paramx['branddtl']) || isset($paramx['available']) || isset($paramx['minprice']) ||
									isset($paramx['maxprice']) || isset($_SESSION['usrid']) || isset($paramx['catid']) || 
									isset($paramx['itemtype']) || isset($paramx['discount'])|| isset($paramx['sort'])
								){
									$url=$url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$paramx['minprice'].$paramx['maxprice'].$paramx['discount'].$paramx['sort'].$usrid;
									$base_url = $base_url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$usrid;
								}
							// var_dump($param, $paramx); 
							// echo $url; exit;
							$get_base_data = $this->ci->libapi->callAPI('GET', $base_url , false);
							// $get_data = $this->ci->libapi->callAPI('GET', $url , false);
							
							$base_res = json_decode($get_base_data, true);

							// $res = json_decode($get_data, true);
							$errors = $res['res']['errors'];
		// var_dump($base_res['data']); die('ara');

		$total_rows=$base_res['total'];
	 	$config["base_url"] = ADMINURL.'product/hotdeals';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		// $param['count']=false;
		// $res1=$this->ci->Do_home->gethome_hotdeal($param);
		$wishlist=$this->ci->libsession->getSession('wishlist');

		if($base_res['data']){
			foreach($base_res['data'] as $row){
				if($wishlist){
					$arrwishlist=explode(',',$wishlist);
				   if(in_array($row['proid'],$arrwishlist)){
						$prodata['product'][$row['proid']]['wishlist']='1';
					}else{
						$prodata['product'][$row['proid']]['wishlist']='0';
					}
				}else{
					 $prodata['product'][$row['proid']]['wishlist']='0';
				}
				$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
				$proname=str_replace("--","-", $proname) ;
				$proname=strtolower(rtrim($proname,'-'));
				$proid=$row['proid'];
				$prodata['product'][$row['proid']]['id']= $proid;
			// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
				$prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
				$prodata['product'][$row['proid']]['name']=$row['proname'];
				$prodata['product'][$row['proid']]['catname']=$row['catname'];
				$prodata['maincatname']=$row['catname'];
				if($row['prothumbnail']){
					//echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;

					//$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
					$image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
					$image1=str_replace("uploads","upload",$image1);
					// echo 	$image1; die;
				// if(file_exists($image1)){
						//echo "hi"; die;
					$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
					// }else{
					// $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
					// }
				}else{

				$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
			}

			//echo $prodata['product'][$row['proid']]['img'];die;
			$prodata['product'][$row['proid']]['price']=$row['onlineprice'];
			$prodata['product'][$row['proid']]['onlinePrice']=$row['onlineprice'];
			$prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
			$setprice=$row['onlineprice']-$row['discountprice'];
			$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlineprice']*100);
	$prodata['product'][$row['proid']]['callforprice']=$row['callforprice']; // - not found
			$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
	$prodata['product'][$row['proid']]['qty']=$row['quantity']; // not found
	$prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
	$prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
	$prodata['product'][$row['proid']]['review']=$row['rvcnt'];
	$prodata['product'][$row['proid']]['rating']=$row['overallrating'];
			$prodata['product'][$row['proid']]['brandid']=$row['brandid'];

			}

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
          	return  $prodata;
		}
	   } // hotdealsX ends

	public function newarrivalbooksX($param, $paramx){
		$memkey = 'lib_home_newarrivalbooks'.json_encode($param);;
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//print_r($memCacheDate); die;
		  // return $memCacheDate;
		}
		
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8080/listing/api/v1.0/2/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		$base_url = "http://3.210.70.215:8080/listing/api/v1.0/2/general/".$param['start']."/".LIMIT."/0.0?".$paramx['available'].$paramx['sort'];
		//var_dump($url);	die();	
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.'product/newarrivalbooks';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$param['isFilters']=true;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
			return $prodata;
		}
	}

	   public function newarrivalbooksX_old($param=false){

		$memkey = 'lib_home_newarrivalbooks'.json_encode($param);;
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//print_r($memCacheDate); die;
		  // return $memCacheDate;
		}

		if($pagestart){
			$param['pagestart']=$pagestart;
		}else{
			$param['pagestart']=0;
		}
		// $rescount=$this->ci->Do_home->gethome_hotdeal($param);
		// hit url and get response
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8000/listing/api/v1.0/2/general/0/20/0?orderby=proadddate&ordertype=desc".rawurlencode($param['search'])."/".$param['pagestart']."/".LIMIT.'?x=';
		$base_url = "http://3.210.70.215:8000/listing/api/v1.0/2/general/".$param['pagestart']."/".LIMIT."/0?orderby=proadddate&ordertype=desc&isavailable=1";
		if( isset($paramx['branddtl']) || isset($paramx['available']) || isset($paramx['minprice']) ||
				isset($paramx['maxprice']) || isset($_SESSION['usrid']) || isset($paramx['catid']) || 
				isset($paramx['itemtype']) || isset($paramx['discount'])|| isset($paramx['sort'])
			){
				$url=$url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$paramx['minprice'].$paramx['maxprice'].$paramx['discount'].$paramx['sort'].$usrid;
				$base_url = $base_url.$paramx['itemtype'].$paramx['catid'].$paramx['branddtl'].$paramx['available'].$usrid;
			}
		// var_dump($param, $paramx); 
		// echo $url; exit;
		$get_base_data = $this->ci->libapi->callAPI('GET', $base_url , false);
		// $get_data = $this->ci->libapi->callAPI('GET', $url , false);
		
		$base_res = json_decode($get_base_data, true);

		// $res = json_decode($get_data, true);
		$errors = $res['res']['errors'];


		/**
		 * *******************
		 * ***************
		 * ***********
		 */
 
			$param['count']=true;
			// $rescount=$this->ci->Do_home->newarrivalbooks($param);
 
		 $total_rows=$base_res['total'];
		 $config["base_url"] = ADMINURL.'product/newarrivalbooks';
		 $config['total_rows'] =$total_rows;
		 $config['per_page'] = LIMIT;
		 $config['uri_segment']=3;
		 $config['enable_query_strings'] = TRUE;
		 if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		 $this->ci->pagination->initialize($config);
		 $prodata['links']=$this->ci->pagination->create_links();
		 
 
			$param['count']=false;
		//  $res1=$this->ci->Do_home->newarrivalbooks($param);
		 $wishlist=$this->ci->libsession->getSession('wishlist');
			   if($base_res['data']){
					 if($base_res['data']){
 
					  foreach($base_res['data'] as $row){
						 if($wishlist){
						  $arrwishlist=explode(',',$wishlist);
						 if(in_array($row['proid'],$arrwishlist)){
							  $prodata['product'][$row['proid']]['wishlist']='1';
						  }else{
							  $prodata['product'][$row['proid']]['wishlist']='0';
						  }
					  }else{
						   $prodata['product'][$row['proid']]['wishlist']='0';
					  }
					  $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					  $proname=str_replace("--","-", $proname) ;
					  $proname=strtolower(rtrim($proname,'-'));
					  $proid=$row['proid'];
					  $prodata['product'][$row['proid']]['id']= $proid;
					 // $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
					   $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
					  $prodata['product'][$row['proid']]['name']=$row['proname'];
					  $prodata['product'][$row['proid']]['catname']=$row['catname'];
					  $prodata['maincatname']=$row['catname'];
					  if($row['prothumbnail']){
						  //echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;
 
							 //$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
							  $image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
							  $image1=str_replace("uploads","upload",$image1);
							 // echo 	$image1; die;
					  // if(file_exists($image1)){
							  //echo "hi"; die;
							 $prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
						  // }else{
							 // $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						  // }
					  }else{
 
						 $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
 
					  }
					 //echo $prodata[$row['proid']]['img'];die;
					 $prodata['product'][$row['proid']]['price']=$row['onlineprice'];
					 $prodata['product'][$row['proid']]['onlinePrice']=$row['onlineprice'];
					 $prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
					 $setprice=$row['onlineprice']-$row['discountprice'];
					 $prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlineprice']*100);
				$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
					 $prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
				$prodata['product'][$row['proid']]['qty']=$row['quantity'];
				$prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
				$prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
				$prodata['product'][$row['proid']]['review']=$row['rvcnt'];
				$prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
 
 
					 }
		   $this->ci->libsession->setMemcache($memkey,$prodata,600);
		   return  $prodata;
			 }
			}
		  }	   


	   //////////////////////////////////////////////////// End elastic search functions.///////////////////////////////////////

	    public function hotdeals($param=false){


        $memkey = 'lib_home_hotdeal'.json_encode($param);
        /*if($param['module']!='homehotdeals'){
        	echo $memkey; die;
        }*/
        $memCacheDate = $this->ci->libsession->getMemcache($memkey);
        if($memCacheDate){
          return $memCacheDate;
        }
        $param['count']=true;
		$rescount=$this->ci->Do_home->gethome_hotdeal($param);
		$total_rows=$rescount[0]['cnt'];
	 	$config["base_url"] = ADMINURL.'product/hotdeals';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$res1=$this->ci->Do_home->gethome_hotdeal($param);
		$wishlist=$this->ci->libsession->getSession('wishlist');

       	if($res1){
					//if($res1){

					 foreach($res1 as $row){
						 if($row['discountPrice'] != '0') {
							if($wishlist){
							$arrwishlist=explode(',',$wishlist);
							if(in_array($row['proid'],$arrwishlist)){
								$prodata['product'][$row['proid']]['wishlist']='1';
							}else{
								$prodata['product'][$row['proid']]['wishlist']='0';
							}
						}else{
							$prodata['product'][$row['proid']]['wishlist']='0';
						}
						$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
						$proname=str_replace("--","-", $proname) ;
						$proname=strtolower(rtrim($proname,'-'));
						$proid=$row['proid'];
						$prodata['product'][$row['proid']]['id']= $proid;
						// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
						$prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
						$prodata['product'][$row['proid']]['name']=$row['proname'];
						$prodata['product'][$row['proid']]['catname']=$row['catname'];
						$prodata['maincatname']=$row['catname'];
						if($row['prothumbnail']){
							//echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;

								//$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
								$image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
								$image1=str_replace("uploads","upload",$image1);
								// echo 	$image1; die;
						// if(file_exists($image1)){
								//echo "hi"; die;
								$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
							// }else{
								// $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							// }
						}else{

							$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						}

						//echo $prodata['product'][$row['proid']]['img'];die;
						$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
						$prodata['product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
						$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
						$setprice=$row['onlinePrice']-$row['discountPrice'];
						$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
						$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
						$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
						$prodata['product'][$row['proid']]['qty']=$row['quantity'];
						$prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
						$prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
						$prodata['product'][$row['proid']]['review']=$row['rvcnt'];
						$prodata['product'][$row['proid']]['rating']=$row['overallrating'];
						$prodata['product'][$row['proid']]['brandid']=$row['brandid'];

					  }
					}

          $this->ci->libsession->setMemcache($memkey,$prodata,600);
          return  $prodata;
			    //}
		   }
	   }
	   public function newarrivalbooks($param=false){

       $memkey = 'lib_home_newarrivalbooks'.json_encode($param);;
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
       	//print_r($memCacheDate); die;
         return $memCacheDate;
       }

       	$param['count']=true;
       	$rescount=$this->ci->Do_home->newarrivalbooks($param);

       	$total_rows=$rescount[0]['cnt'];
	 	$config["base_url"] = ADMINURL.'product/newarrivalbooks';
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();
		

       	$param['count']=false;
		$res1=$this->ci->Do_home->newarrivalbooks($param);
		$wishlist=$this->ci->libsession->getSession('wishlist');
        	  if($res1){
					if($res1){

					 foreach($res1 as $row){
						if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($row['proid'],$arrwishlist)){
							 $prodata['product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$row['proid']]['wishlist']='0';
					 }
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
					  $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];
                     $prodata['product'][$row['proid']]['catname']=$row['catname'];
					 $prodata['maincatname']=$row['catname'];
					 if($row['prothumbnail']){
						 //echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;

							//$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
					 		$image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
						 	$image1=str_replace("uploads","upload",$image1);
							// echo 	$image1; die;
					 // if(file_exists($image1)){
							 //echo "hi"; die;
							$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
						 // }else{
							// $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
						 // }
					 }else{

						$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';

					 }
					//echo $prodata[$row['proid']]['img'];die;
					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
						$prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];


					}
          $this->ci->libsession->setMemcache($memkey,$prodata,600);
          return  $prodata;
			}
		   }
		 }
		 
		 public function get_hotdealsfilter($param=false){
			$res['filter'] = $this->ci->Do_home->gethotdeals_filter($param);
			$result = $this->filter($res);
			return $result;
		}

		public function get_newarrivalsfilter($param=false){
			$res['filter'] = $this->ci->Do_home->getnewarrivals_filter($param);
			$result = $this->filter($res);
			return $result;
		}
		
	public function brandsX($param, $paramx){
		$memkey = 'lib_home_newarrivalbooks'.json_encode($param);;
		$memCacheDate = $this->ci->libsession->getMemcache($memkey);
		if($memCacheDate){
			//print_r($memCacheDate); die;
		  // return $memCacheDate;
		}
		
		$param['limit'] = $param['limit'] ? $param['limit'] : LIMIT;
		$usrid = isset($_SESSION['usrid']) ? '&user='. $_SESSION['usrid'] : '';
		$url = "http://3.210.70.215:8080/listing/api/v1.0/0/brand/".$param['start']."/".LIMIT."/".$param['brandid'].".0?x=";
		$base_url = "http://3.210.70.215:8080/listing/api/v1.0/0/brand/".$param['start']."/".LIMIT."/".$param['brandid'].".0?x=";
		//var_dump($url); die();	
		$param['url'] = $url;
		$param['base_url'] = $base_url;

		$resES = $this->ci->lib_common->getProductESresult($param, $paramx);
		$res = $resES['res'];
		$base_res = $resES['base_res'];
		//var_dump($res); die();	

		$param['count']=true;
		$rescount=$res;
		$total_rows=$res['total'];
		$config["base_url"] = ADMINURL.$param['brandname'].'/'.$param['brandid'];
		$config['total_rows'] =$total_rows;
		$config['per_page'] = LIMIT;
		$config['uri_segment']=3;
		$config['enable_query_strings'] = TRUE;
		if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");
		
		$this->ci->pagination->initialize($config);
		$prodata['links']=$this->ci->pagination->create_links();

		$param['count']=false;
		$param['isFilters']=true;
		if($res['data']){

			$res_prodata = $this->ci->lib_filters->getFormattedProducts($res);
			// setting values
			$prodata = array_merge($prodata, $res_prodata);
			// filters logic
			if($param['isFilters']){
				$param['url'] = $url;
				$filters = $this->ci->lib_filters->getCommonFilters($param);
				$prodata = array_merge($prodata, $filters);
			}

			$prodata['minprice']= $base_res['min_amt'];
			$prodata['maxprice']= $base_res['max_amt'];
			$prodata['mindiscount']= round( $base_res['min_discperc'] );
			$prodata['maxdiscount']= round( $base_res['max_discperc'] );

			$this->ci->libsession->setMemcache($memkey,$prodata,600);
			return $prodata;
		}
	}
}
 ?>
