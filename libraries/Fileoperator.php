<?php

	/**
	* fileoperator.php
	*/
	class Fileoperator
	{
		
		function uploadfile($temp,$destination,$filename){
             $filename = preg_replace(array("/\s+/", "/[^-\.\w]+/"),  array("_", ""),  trim($filename));echo $filename;
             return $moved = move_uploaded_file ($temp,$destination.$filename);
        }
                
        function destinationcreator($pathtocreate)
        {
                $dirToCreate = $pathtocreate;
				$pathToCreate = FILEBASEPATH;
                $getDir = array();
                if(strstr($dirToCreate,"/"))
                {
                        $getDir = explode("/",$dirToCreate);
                }
                $dirCnt = count($getDir);

                for($i=0;$i<$dirCnt;$i++)
                {
                        if($getDir[$i])
                        {
                            $pathToCreate = $pathToCreate."/".$getDir[$i];
                             $pathToCreate = str_replace('//','/',$pathToCreate);
                              if(!is_dir($pathToCreate))	
                                if(!mkdir($pathToCreate,0777))
                                   die($pathToCreate);
                        }
                }
                //echo ($pathToCreate); 
				return $pathToCreate;
        }

		//$types -> allowed file types
		function upload_file($file, $dest = '', $var, $maxsize = '99999', $types = array(), $errors = array('nodata' => 'No uploaded file data', 'empty' => 'Empty uploaded file data', 'toolong' => 'Filename is too long', 'invalidpath' => 'Upload path is invalid', 'pathwrite' => 'Upload path is NOT writable', 'nofile' => 'Not an uploaded file', 'invalidtype' => 'Invalid file type', 'toosmall' => 'File is too small', 'toobig' => 'File is too big', 'exists' => 'File already exists', 'failed' => 'Upload failed')) {

			
			
			if (empty($dest)) {
				$dest = './' . $file['name'];
			}
			if (!isset($file)) {
				return $errors['nodata'];
			}
			if (empty($file)) {
				return $errors['empty'];
			}
			if (strlen($file['name']) > 60) {
				return $errors['toolong'];
			}
			if ($file['error'] != 0) {
				return $file['error'];
			}
			if (!is_dir(dirname($dest))) {
				return $errors['invalidpath'];
			}
			if (!is_writeable(dirname($dest))) {
				return $errors['pathwrite'];
			}
			if (!is_uploaded_file($file['tmp_name'])) {
				return $errors['nofile'];
			}
			if (!self::check_file_types($file['name'], $types)) {
				return $errors['invalidtype'];
			}
			if ($file['size'] == 0) {
				return $errors['toosmall'];
			}
			if ($file['size'] > $maxsize) {
				return $errors['toobig'];
			}
			if (file_exists($dest)) {
				return $errors['exists'];
			}
			//if (!@copy($file['tmp_name'][$var], $dest)) {
			
			if(!move_uploaded_file ($file['tmp_name'],$dest)) {
				return $errors['failed'];
			}

			
			return "true";
		}

		function check_file_types($file, $types = array()) {

			if (!empty($types)) {
				foreach($types as $type) {
					if (strstr($type, '/')) {
						if (strtolower(mime_content_type($file)) == strtolower($type)) {
						    return TRUE;
						}
					} else {
						if (strtolower(self::get_ext($file)) == strtolower($type)) {
						    return TRUE;
						}
					}
				}
			}
		}

		function get_ext($file) { //returns .ext, ie: .jpg
			if (strstr($file, '.')) {
				$ext = substr($file, strrpos($file, '.'));
				return $ext;
			}

		}

       function getfileicon($file){
	   
	 
        $file =new SplFileInfo($file);
	    $ext= $file->getExtension();

		
	    if($ext=="png" || $ext=="jpg" || $ext=="gif")
		   {
		     $path= '<img src="'.base_url().'static/web/images/icon/imageicon.png" width="50" height="50"/>';
		   }
		else if($ext=="xls" || $ext=="xlsx" || $ext=="csv")
		   {
		     $path= '<img src="'.base_url().'static/web/images/icon/excelicon.png" width="50" height="50"/>';
		   }
		else if($ext=="pdf")
		   {
		     $path= '<img src="'.base_url().'static/web/images/icon/pdficon.png" width="50" height="50"/>';
		   }
		else if($ext=="docx")
		   {
		    echo "Attachment(s)";
			 $path= '<img src="'.base_url().'static/web/images/icon/wordicon.png" width="50" height="50" />';
		   }
		else
		   {
		     $path= '<img src="'.base_url().'static/web/images/icon/othericon.png" width="50" height="50" />';
		   }
		
		return $path;
	   
	   }




	}
?>
