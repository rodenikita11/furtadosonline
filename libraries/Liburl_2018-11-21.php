<?php
class Liburl{
             public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_product');
			  $CI->load->model('Do_common');
			  $CI->load->library('pagination');
			  $CI->load->library('libsession');
             $this->ci = $CI;

         }

		 public function getinfotype($param){
			 //var_dump($param);
			 if($param){
				 if(is_int($param['id'])){
					 //die('once');
					 $res =$this->ci->Do_product->getinfotype($param);

					 return $res;

				 }else{
					 echo "invalid data type of product id"; exit;
				 }

			 }

		 }

		public function get_productdetail($param){

       $memkey = 'liburl_get_productdetail'.json_encode($param);
      //echo $memkey; die;
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
         return $memCacheDate;
       }

			if(isset($param['pid'])){
				$param['id']=intval($param['pid']);
			}
           if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];

				if($param['sid']){

					$model['sid']=$param['sid'];
				}
				if(is_string($param['filterdtl'])){
					$model['allattribute']=implode(',',$param['filterdtl']);
                    //print_R($model['allattribute']); die;
				}
			     $res=$this->ci->Do_product->getprodtl($model);
				//print_r($res); die;
				 if($res){
					// print_r($res['prodata']); echo 'vikram';
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $res['prodata'][0]['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));

					$productName = stripslashes($res['prodata'][0]['proname']);
					 $proid=$res['prodata'][0]['proid'];
					 $prodata['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
					 $prodata['proid']=$res['prodata'][0]['proid'];
                     $prodata['name']=$res['prodata'][0]['proname'];
					 $prodata['img']=str_replace('http:', 'https:', $res['prodata'][0]['prothumbnail']);
					 $prodata['embedCode']=$res['prodata'][0]['embedCode'];
					  if($res['prodata'][0]['prothumbnail']){
						  $bigImage = str_replace('_100x75.','_350x350.',$res['prodata'][0]['prothumbnail']);
						 // echo  $bigImage; die;
					 	 $prodata['img']=$this->getprothumbnail($bigImage,DWIMGDTL);
					 }else{
						$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
					 }
					$wishlist=$this->ci->libsession->getSession('wishlist');
					 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						 if(in_array($proid,$arrwishlist)){
							 $prodata['wishlist']='1';
						 }else{
							$prodata['wishlist']='0';
						 }
					 }else{
						  $prodata['wishlist']='0';
					 }
					 $prodata['onlinePrice']=$res['prodata'][0]['onlinePrice'];
					 $prodata['qty']=$res['prodata'][0]['quantity'];
					 $prodata['discountPrice']=$res['prodata'][0]['discountPrice'];
					 $setprice=$res['prodata'][0]['onlinePrice']-$res['prodata'][0]['discountPrice'];
					$prodata['discountpercentage']= round($setprice/$res['prodata'][0]['onlinePrice']*100);

					 $prodata['shortspecification']=$res['prodata'][0]['prodescription'];
					 $prodata['longdesc']=$res['prodata'][0]['prolongspecification'];
					 $prodata['brandid']=$res['prodata'][0]['brandid'];
					 //echo '<pre>'; print_r($res['prodata']); die;
					 $prodata['brandname']=$res['prodata'][0]['brandname'];
					 $prodata['brandimg']=str_replace('http:', 'https:', $res['prodata'][0]['brandimage']);
					 $prodata['brandurl']=SITEURL.''.$prodata['brandname'].'/'.$prodata['brandid'];
					 $prodata['branddescription']=$res['prodata'][0]['branddescription'];
					 $prodata['catname']=$res['prodata'][0]['catname'];
					 $prodata['catid']=$res['prodata'][0]['category_id'];
					 $prodata['catparent_id']=$res['prodata'][0]['catparent_id'];
					 $prodata['revcnt']=$res['prodata'][0]['rvcnt'];
					 $prodata['overallrating']=$res['prodata'][0]['overallrating'];
					 $prodata['quantity'] = $res['prodata'][0]['quantity'];
                     $prodata['avaliable'] = $res['prodata'][0]['isAvailable'];
					 $prodata['callforprice'] = $res['prodata'][0]['callforprice'];
					 for($i=0;$i<count($res['proimage']);$i++){
						 $prodata['images'][$i]['pro_img_id']=$res['proimage'][$i]['pro_img_id'];
						 $prodata['images'][$i]['proimagetitle']=$res['proimage'][$i]['proimagetitle'];

						 $image=str_replace('_100x75.','_350x350.',$res['proimage'][$i]['pro_image']);
						 //echo $image; die;
						 $prodata['images'][$i]['pro_image']=str_replace('http:', 'https:', $image);
						 $prodata['images'][$i]['pro_img_sort_order']=$res['proimage'][$i]['pro_img_sort_order'];
					 }
                   //	print_R($prodata['images']); die;
					 $prodata['review']=$res['rewview'];
				 }
				 //print_r($prodata);die;

				if($res['prodata'][0]['isBook']){
					$prodata['meta_title']='Buy '.$this->getPlainText($productName).' Book Online in India | Furtados';
					$prodata['meta_description'] = 'Furtados : Buy '.$this->getPlainText($productName).' Book Online in India at Best Prices.';
					$prodata['meta_keyword']=$this->getPlainText($productName);
				}else{
					$prodata['meta_title']='Buy '.$productName.' Online in India at Best Price | Furtados';
					$prodata['meta_description']='Find the best '.$productName.' Online in India at competitive prices. ✓Best Prices ✓Quick Delivery ✓Quality Assured';
					$prodata['meta_keyword']=$this->getPlainText($productName);
				}

           $this->ci->libsession->setMemcache($memkey,$prodata);
				return $prodata;
			}else{
           echo "invalid data type of product  id"; exit;
         }

		   }

      }

	public function getPlainText($para){
		$old_pattern = array("/[^a-zA-Z0-9. ]/", "/_+/", "/_$/");
		$new_pattern = array("");
		$newPara = preg_replace($old_pattern, $new_pattern , $para);
		$newPara = ucwords(strtolower($newPara));

		return $newPara;
	}

	   public function get_categorydetails($param){

       $memkey = 'liburl_get_categorydetails'.json_encode($param);

       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
         return $memCacheDate;
       }


        if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				$model['count']=$param['count'];
				$model['start']=$param['start'];
				if(is_string($param['filterdtl'])){
					$model['allattribute']=json_decode($param['filterdtl']);

				}
				if(is_string($param['branddtl'])){
					$model['branddtl']=json_decode(str_replace('_', ',', $param['branddtl']));

				}
				if(is_string($param['search'])){
					$model['search']=$param['search'];

				}
				if(is_string($param['avaliable'])){
					$model['avaliable']=json_decode($param['avaliable']);

				}
				if(is_string($param['sort'])){
					$model['sort']=$param['sort'];

				}
				if($param['minprice'] && $param['maxprice']){
					$model['minprice']=$param['minprice'];
					$model['maxprice']=$param['maxprice'];
				}
				if($param['percentage']){
				 	$model['percentage']=json_decode($param['percentage']);
				}
				 $res=$this->ci->Do_product->getcatdtl($model);
				//echo '<pre>';
				//print_R($res); die;
				 $total_rows=$res[0]['count'];

					$config = array();
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");



					$config['total_rows'] =$total_rows;
					$config['per_page'] = LIMIT;
					$config['uri_segment'] = 4;
					$this->ci->pagination->initialize($config);
                    $prodata['links']=$this->ci->pagination->create_links();
					$model['count']=false;
					$res1=$this->ci->Do_product->getcatdtl($model);
					$wishlist=$this->ci->libsession->getSession('wishlist');

				 if($res1){
					if($res1['product']){

					 foreach($res1['product'] as $row){
					 	//print '<pre>'; print_r($row);die;
							if($wishlist){
							 $arrwishlist=explode(',',$wishlist);
								if(in_array($row['proid'],$arrwishlist)){
									 $prodata['product'][$row['proid']]['wishlist']='1';
								}else{
									 $prodata['product'][$row['proid']]['wishlist']='0';
								}
							}else{
								  $prodata['product'][$row['proid']]['wishlist']='0';
							}
						 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
						 $proname=str_replace("--","-", $proname) ;
						 $proname=strtolower(rtrim($proname,'-'));
						 $proid=$row['proid'];
						 $prodata['product'][$row['proid']]['id']= $proid;
						// $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
						 $prodata['product'][$row['proid']]['url']=str_replace('http:', 'https:', ''.SITEMOBURL.''.$proname.'/'.$proid.'');
	                     $prodata['product'][$row['proid']]['name']=$row['proname'];
	                     $prodata['product'][$row['proid']]['catname']=$row['catname'];
						 $prodata['maincatname']=$row['catname'];
						// $prodata['maincaturl']=$prodata['product'][$row['proid']]['url'];
						 $prodata['catparent_name']=$row['catparentname'];
						 $prodata['catmeta_description']=$row['catmeta_description'];
						 $prodata['catmeta_keyword']=$row['catmeta_keyword'];
						//Added by Pritish 12-06-2018
						$prodata['meta_title']=stripslashes($row['catmeta_title']);
						$prodata['meta_description']=stripslashes($row['catmeta_description']);
						//echo $prodata['meta_description']; die;
						$prodata['meta_keyword']=stripslashes($row['catmeta_keyword']);


						 $prodata['catparentid']=$row['catparentid'];
						 $r1=array(' ','&');
              			 $r2=array('-','-');
						 $catparurl=str_replace($r1,$r2, $row['catparentname']);
						 $prodata['catparenturl']=ADMINURL.strtolower($catparurl).'/'.$row['catparentid'];

							 if($row['prothumbnail']){
								$prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
							 }else{
								$prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
							 }

					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					if($row['callforprice']=='NO'){
						$percentage=round($setprice/$row['onlinePrice']*100);
						if($percentage >0 && 	$percentage <=10){
							$prodata['percentage']['0_10']='0 to 10%';
						}else if($percentage >10 && 	$percentage <=20){
								$prodata['percentage']['10_20']='10 to 20%';
						}else if($percentage >20 && 	$percentage <=30){
								$prodata['percentage']['20_30']='20 to 30%';
						}else if($percentage >30 && 	$percentage <=40){
								$prodata['percentage']['30_40']='30 to 40%';
						}else if($percentage >40){
								$prodata['percentage']['40_more']='40% and more';
						}
					}

					$prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];

					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
					 $prodata['product'][$row['proid']]['brandname']=$row['brandname'];
					 $prodata['product'][$row['proid']]['avaliable']=$row['isAvailable'];
					 $prodata['seo_metadesc']=$row['seo_metadesc'];
					 $prodata['seodescription']=$row['seodescription'];

					}
				   }
					if($res1['featured_product']){
						//print_r($res1['featured_product']); die;
					 foreach($res1['featured_product'] as $row){
					 	//print_r($row);
						 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($row['proid'],$arrwishlist)){
							 $prodata['featured_product'][$row['proid']]['wishlist']='1';
						 }else{
							 $prodata['featured_product'][$row['proid']]['wishlist']='0';
						 }
					 }else{
						  $prodata['featured_product'][$row['proid']]['wishlist']='0';
					 }
					  $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['featured_product'][$row['proid']]['id']= $proid;
					 $prodata['featured_product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
		           $prodata['featured_product'][$row['proid']]['name']=$row['proname'];
		           $prodata['featured_product'][$row['proid']]['catname']=$row['catname'];
		           if($row['prothumbnail']){
								$prodata['featured_product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
					 }else{
								$prodata['featured_product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
					}
					$prodata['featured_product'][$row['proid']]['price']=$row['onlinePrice'];
					$prodata['featured_product'][$row['proid']]['onlinePrice']=$row['onlinePrice'];
					$setprice=$row['onlinePrice']-$row['discountPrice'];
					$prodata['featured_product'][$row['proid']]['percentage']= round($setprice/$row['onlinePrice']*100);
					$prodata['featured_product'][$row['proid']]['discountprice']=$row['discountPrice'];
					$prodata['featured_product'][$row['proid']]['callforprice']=$row['callforprice'];
					$prodata['featured_product'][$row['proid']]['avaliable']=$row['isAvailable'];
					$prodata['featured_product'][$row['proid']]['qty']=$row['quantity'];

					}
				  }
					if($res1['cat_banner']){
					 foreach($res1['cat_banner'] as $row){
						 if($row['imgpath']){
					  $prodata['cat_banner'][$row['id']]['imgpath']=str_replace('http:', 'https:', $row['imgpath']);
				  }else{
					 $prodata['cat_banner'][$row['id']]['imgpath']=ADMINURL.'/assets/images/cat-banner-1.jpg' ;
					 }
					 $prodata['cat_banner'][$row['id']]['url']=$row['img_link'];
                     $prodata['cat_banner'][$row['id']]['sortorder']=$row['img_order'];

			    	}
				  }
					if($res1['cat_bannerweb']){

					 	//$image1=str_replace(SITEMOBURL,FILEBASEPATH,$res1['cat_bannerweb'][0]['img_path']);
						//echo $image1; die;
						if($res1['cat_bannerweb'][0]['img_path']){
							$prodata['cat_bannerweb']['imgpath']= str_replace('http:', 'https:', $res1['cat_bannerweb'][0]['img_path']);
						}else{
							//$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
						}

					 $prodata['cat_bannerweb']['url']=str_replace('http:', 'https:', $res1['cat_bannerweb'][0]['img_link']);
                     $prodata['cat_bannerweb']['sortorder']=$res1['cat_bannerweb'][0]['img_order'];


				 }else{
					  //$prodata['cat_bannerweb']['imgpath']= ADMINURL.'/assets/web/images/cat-banner.jpg';
					 $prodata['cat_bannerweb']['url']='';
                     $prodata['cat_bannerweb']['sortorder']=1;



					 }

           $this->ci->libsession->setMemcache($memkey,$prodata);
				   return $prodata;

			   }else{


					 echo "invalid data type of category id"; exit;

			   }

		   }

      }
     }

	  public function getbranddtl($param){
		    if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				$model['count']=$param['count'];
				$model['start']=$param['start'];
				if(is_string($param['filterdtl'])){
					$model['allattribute']=json_decode($param['filterdtl']);

				}
			     $res=$this->ci->Do_product->getbrnaddtl($model);

				 $total_rows=$res[0]['count'];

					$config = array();
					if (count($_GET) > 0) $config['suffix'] = '?' . http_build_query($_GET, '', "&");



					$config['total_rows'] =$total_rows ;
					$config['per_page'] = LIMIT;
					$config['uri_segment'] = 4;
					$this->ci->pagination->initialize($config);
                    $prodata['links']=$this->ci->pagination->create_links();
					 $model['count']=false;
					 $res1=$this->ci->Do_product->getbrnaddtl($model);

				 if($res1){

					 foreach($res1 as $row){

					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$row['proid'];
					 $prodata['product'][$row['proid']]['id']= $proid;
					 $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
                     $prodata['product'][$row['proid']]['name']=$row['proname'];
					 if($row['prothumbnail']){
					 $prodata['product'][$row['proid']]['img']=str_replace('http:', 'https:', $row['prothumbnail']);
					 }else{
						$prodata['product'][$row['proid']]['img']=IMAGEDEFAULT;
					 }

					$prodata['product'][$row['proid']]['price']=$row['onlinePrice'];

					$prodata['product'][$row['proid']]['discountprice']=$row['discountPrice'];


					 $prodata['product'][$row['proid']]['qty']=$row['quantity'];
					 $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
					 $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
					 $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
					 $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
					 $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
					 $prodata['product'][$row['proid']]['brandname']=$row['brandname'];

					}


				 }
				  return $prodata;
			   }else{

					 echo "invalid data type of category id"; exit;

			   }

		   }


	  }

        public function getfilter($param){

          $memkey = 'liburl_getfilter'.json_encode($param);
          $memCacheDate = $this->ci->libsession->getMemcache($memkey);
          if($memCacheDate){
            return $memCacheDate;
          }

			if($param){
				  if(is_int($param['cid']) ){
					  $param['cid']=$param['cid'];
					  $result=$this->ci->Do_common->getfilter($param);

				      $resprice=$this->ci->Do_common->getpricerange($param);
				      $data['respercentage']=$this->ci->Do_common->getpercentage($param);
				     //print '<pre>'; print_R($resatt); die;
					  $resatt=$this->ci->Do_common->getattribute($param);

						foreach($result as  $row){
							$data['brand'][$row['brandid']]['id' ]=$row['brandid'];
							$data['brand'][$row['brandid']]['name' ]=$row['brandname'];
							$data['brand'][$row['brandid']]['probrandcnt' ]=$row['probrandcnt'];
						}

						foreach($result as $row1){
						 	$data['avalibale']['cnt']=	$row1['proavailablecnt'];
						 	$data['notavaliable']['cnt']=	$row1['pronotavailablecnt'];
						  	$data['procnt']['cnt']=$row['procnt'];
						}
						//print_r($resatt); die;
						foreach($resatt as $rowatt){
							$data['attgrp'][$rowatt['attrigrpid']]['id']=$rowatt['attrigrpid'];
                            $data['attgrp'][$rowatt['attrigrpid']]['name']=$rowatt['attrigrpname'];
						    $data['attgrp'][$rowatt['attrigrpid']]['att'][$rowatt['attriid']]['id']=$rowatt['attriid'];
							$data['attgrp'][$rowatt['attrigrpid']]['att'][$rowatt['attriid']]['attname']=$rowatt['attriname'];
						}

						$data['minprice']=$resprice[0]['minprice'];
						$data['maxprice']=$resprice[0]['maxprice'];

            		$this->ci->libsession->setMemcache($memkey,$data);

					  return $data;
				}else{
					 echo "invalid data type of attribute id"; exit;
				}
			}



		}

		public function getprothumbnail($imgpath,$size){
			if($imgpath && $size){
				return THUMBPATH.'?img='.$imgpath.'&w='.$size;
			}else{
				echo "please specify the size and path for the image"; exit;
			}
		}

	     public function pro_clerance_dtl($param){
			//print_r($param);
           if($param){
			   if(is_int($param['id'])){
				$model['id']=$param['id'];
				if($param['sid']){

					$model['sid']=$param['sid'];
				}
				if(is_string($param['filterdtl'])){
					$model['allattribute']=implode(',',$param['filterdtl']);
                    //print_R($model['allattribute']); die;
				}
			     $res=$this->ci->Do_product->pro_clerance_dtl($model);
			     //print_r($res); echo 'hiii';
				$wishlist=$this->ci->libsession->getSession('wishlist');
				 if($res){
					 if($wishlist){
						 $arrwishlist=explode(',',$wishlist);
						if(in_array($res[0]['productId'],$arrwishlist)){
							 $prodata['product'][$res[0]['productId']]['wishlist']='1';
						 }else{
							 $prodata['product'][$res[0]['productId']]['wishlist']='0';
						 }
					 }else{
						  $prodata['product'][$res[0]['productId']]['wishlist']='0';
					 }
					 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $res[0]['productName']);
					 $proname=str_replace("--","-", $proname) ;
					 $proname=strtolower(rtrim($proname,'-'));
					 $proid=$res[0]['productId'];
					 $prodata['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
					 $prodata['proid']=$res[0]['proid'];
                     $prodata['name']=$res[0]['productName'];
					// $prodata['img']=$res[0]['productImage1'];
                      $pathchk=FILEBASEPATH.'uploads/clerance/'.$res[0]['productImage1'];
					  if($res[0]['productImage1']){
						  if(file_exists($pathchk)){
							$prodata['img']=GENERALURL.'uploads/clerance/'.$res[0]['productImage1'];
						  }else{
							$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }
					 }else{
							$prodata['img']=SITEIMAGES.'mobimg/noimage1.png';
						 }

					 $setprice=$res[0]['onlinePrice']-$res[0]['clearanceMRP'];
					 $devideprice=$setprice/$res[0]['onlinePrice'];
					 $prodata['discountpercentage']= round($devideprice*100);
					 $prodata['onlinePrice']=$res[0]['onlinePrice'];
					 $prodata['qty']=$res[0]['quantity'];
					 $prodata['discountPrice']=$res[0]['clearanceMRP'];

					 $prodata['shortspecification']=$res[0]['proshortspecification'];
					 $prodata['longdesc']=$res[0]['prolongspecification'];
					 $prodata['brandid']=$res[0]['brandid'];
					 $prodata['brandname']=$res[0]['brandname'];
					 $prodata['brandimg']=$res[0]['brandimage'];
					 $prodata['catname']=$res[0]['catname'];
					 $prodata['catid']=$res[0]['category_id'];
					 $prodata['catparent_id']=$res[0]['catparent_id'];
					 $prodata['revcnt']=$res[0]['rvcnt'];
					 $prodata['overallrating']=$res[0]['overallrating'];
					 $prodata['quantity'] = $res[0]['quantity'];
					 $prodata['clearanceId'] = $res[0]['clearanceId'];
					 $prodata['serialid']=$res[0]['serialid'];
					 if($res[0]['clearanceAvailable']==1){
						 $prodata['avaliable'] =1;
					 }else{
						 $prodata['avaliable'] =0;
					 }
						  $pathchk=FILEBASEPATH.'uploads/clerance/'.$res[0]['productImage1'];
                      if($res[0]['productImage1']){
						 if(file_exists($pathchk)){
						 $prodata['images'][1]['pro_image']=GENERALURL.'uploads/clerance/'.$res[0]['productImage1'];
						$prodata['images'][1]['pro_image']=str_replace('_80x60','_250x190',$prodata['images'][1]['pro_image']);
						  }else{
							 $prodata['images'][1]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
					  }else{
							 $prodata['images'][1]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
						  $pathchk1=FILEBASEPATH.'uploads/clerance/'.$res[0]['productImage2'];
						  //echo 		$res[0]['productImage3'];
					  if($res[0]['productImage2']){
						  if(file_exists($pathchk1)){
							$prodata['images'][2]['pro_image']=GENERALURL.'uploads/clerance/'.$res[0]['productImage2'];
							$prodata['images'][2]['pro_image']=str_replace('_80x60','_250x190',$prodata['images'][2]['pro_image']);
						  }	else{
							 $prodata['images'][2]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }
					  }else{
							 $prodata['images'][2]['pro_image']=SITEIMAGES.'mobimg/noimage1.png';
						  }

					 $prodata['review']=$res['rewview'];
				 }
				#print_R($prodata); die;
				   return $prodata;
			   }else{


					 echo "invalid data type of product  id"; exit;

			   }

		   }

      }

	  public function getsubcat($param){

      $memkey = 'liburl_getsubcat'.json_encode($param);
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate){
        return $memCacheDate;
      }

		  if($param['cid']){
			  $res=$this->ci->Do_common->getsubcat($param);
			        if($res ){
						foreach($res as $row){
						 $catname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['catname']);
						 $catname=str_replace("--","-", $catname) ;
						 $catname=strtolower(rtrim($catname,'-'));
						 $prodata['category'][$row['catid']]['id']= $row['catid'];
						 //$prodata['category'][$row['catid']]['url']=''.SITEURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['url']=''.SITEMOBURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['name']=$row['catname'];
						}
					}

		  }
      $this->ci->libsession->setMemcache($memkey,$prodata);
		  return  $prodata;

	  }
	  /*viki 19/2/2018*/
	  public function getcat($param){

      $memkey = 'liburl_getcat'.json_encode($param);
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate){
        return $memCacheDate;
      }


		  if($param['cid']){
			  $res=$this->ci->Do_common->getcat($param);

			        if($res ){
						foreach($res as $row){
						 $subcat=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['subcat']);
						 $maincat=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['maincat']);
						 $subcat=str_replace("--","-", $subcat) ;
						 $maincat=str_replace("--","-", $maincat) ;
						 $subcat=strtolower(rtrim($subcat,'-'));
						 $maincat=strtolower(rtrim($maincat,'-'));
						 $prodata['category'][$row['catid']]['id']= $row['catid'];
						 //$prodata['category'][$row['catid']]['url']=''.SITEURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['url']=''.SITEMOBURL.''.$catname.'/'.$row['catid'].'';
						 $prodata['category'][$row['catid']]['subcat']=$row['subcat'];
						 $prodata['category'][$row['catid']]['maincat']=$row['maincat'];
						}
					}

		  }
      $this->ci->libsession->setMemcache($memkey,$prodata);
		  return  $prodata;

	  }

	  public function addbannercount($param){
		  if($param){
		  	//print_r($param); die;
			  $res=$this->ci->Do_common->addbannercount($param);
			  if($res){
				  return true;
			  }else{

				  return false;
			  }
			}


	  }

	   public function getstaticdata($param){
		   $res=$this->ci->Do_common->getstaticdata($param);
			  if($res){
				  return $res;
			  }else{

				  return false;
			  }

	   }

	  /*viki 07/04/2018*/

		public function getjobdetails() {
			$res = $this->ci->Do_common->getjobdetails();
			//print_r($res); die('hisss');
				if($res) {
					return $res;
				} else {
					return false;
				}
		}

		public function brandsmeta($param){
			$res = $this->ci->Do_common->brandsmeta($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilterbrand($param){

			$res=$this->ci->Do_common->getfilterbrand($param);
			$rescount=$this->ci->Do_common->getfilterbrandcount($param);
			if($res){
				$brands=array();
				foreach($res as $val){
					$brands[$val['brandid']]['id']=$val['brandid'];
					$brands[$val['brandid']]['name']=$val['brandname'];
					foreach($rescount as $brandsval){
						if($val['brandid']==$brandsval['brandid']){
							$brands[$val['brandid']]['probrandcnt']=$brandsval['probrandcnt'];
						}
					}
				}
				return $brands;
			} else {
				return false;
			}
		}

		public function getfilteravailable($param){
			$res=$this->ci->Do_common->getfilteravailable($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilternotavailable($param){
			$res=$this->ci->Do_common->getfilternotavailable($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfilterprocnt($param){
			$res=$this->ci->Do_common->getfilterprocnt($param);
			if($res) {
				return $res;
			} else {
				return false;
			}	
		}

		public function getfilterminprice($param){
			$res=$this->ci->Do_common->getfilterminprice($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		public function getfiltermaxprice($param){
			$res=$this->ci->Do_common->getfiltermaxprice($param);
			if($res) {
				return $res;
			} else {
				return false;
			}
		}

		
	}