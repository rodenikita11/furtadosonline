<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller
{
    
    function __construct(){
        parent::__construct();
        $this->load->library('user_agent');
    }
    
    public function index($param1=false, $param2=false){
    	/*echo '<pre>';
    	print_r($this->libsession->getallsessions()); die;*/
        $name1= $this->uri->segment('1');
        $name= str_replace("-", " ", $name1);
        //print $name; die;
        $id=$this->uri->segment('2');
        //$param['catid']=intval(206);
        
        //  print_R($id);  
        if ($name && $id){
            $data['menus'] = $this->libmenu->get_dropdownmenu();
            $data['signupstate']     = $this->liblogin->getsignupStates();
            $param['id'] = intval($id);
            $param['name']=$name;
            //print_r($_GET);
            
            if($_GET['filter']){
                $param['filterdtl'] = $_GET['filter'];
            }
            if ($_GET['search']) {
                $param['search'] = $_GET['search'];
            }
            if ($_GET['brand']) {
                $param['branddtl'] = $_GET['brand'];
            }
            if ($_GET['avaliable']) {
                $param['avaliable'] = $_GET['avaliable'];
            }
            if ($_GET['sort']) {
                $param['sort'] = $_GET['sort'];
            }
            if ($_GET['pricestart']) {
                $param['minprice'] = $_GET['pricestart'];
            }
            if ($_GET['pricend']) {
                $param['maxprice'] = $_GET['pricend'];
            }
            if ($_GET['percentage']) {
                $param['percentage'] = $_GET['percentage'];
            }
            //print_r($param); die;
            // print_R($catname);
            $getdtl = $this->liburl->getinfotype($param);
            //echo '<pre>';
            //print_r($getdtl); die;
            if($_SERVER['REMOTE_ADDR']=='162.158.50.90'){
                //die($getdtl['type']);
            }

            if ($_GET['sid']){
                $getdtl['type'] = 'product';
            }
            
            if ($name1 == 'giftcard'){
                if ($_REQUEST['start']){
                    $param['start'] = $_REQUEST['start'] + LIMIT;
                } else {
                    $param['start'] = '0';
                }

                $param['cid']            = intval($id);
                $data['banner_category'] = $this->lib_home->get_home_dtl();
                $data['giftcarddetail']  = $this->liburl->get_categorydetails($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
               //echo $mobile; die;
                if (!empty($mobile)) {
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/offercard", $data);
                } else {
                    
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/offercard", $data);
                    $this->load->view('web/include/footer', $data);
                }
            } else if ($name1 == 'promotion'){
                $param['id']              = intval($id);
                $data['promotionproduct'] = $this->libprodtl->getproductpromotiondtl($param);
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/promotion_products", $data);
            } else if ($getdtl['type'] == 'product'){
                $data['seomodule'] = 'product';
                $data['seoname']   = $this->uri->segment(1);
                $data['seoid']     = $this->uri->segment(2);
                
                $param['sid'] = urldecode($_GET['sid']);
                // print_r($param['sid']); echo 'hiii123456';
                if (empty($param['sid'])){
                    $data['res']= $this->liburl->get_productdetail($param);
                    $param['catid'] = $data['res']['catid'];
                    //print '<pre>';print_r($data['res']); die;
                    $data['comboproducts']= $this->libpromooffer->getComboProductDetails($param);
                    //print_R($data['comboproducts']); die;
                    
                } else {
                    $data['res'] = $this->liburl->pro_clerance_dtl($param);
                }
                if ($data['res']['discountPrice'] >= MIN_PRICE_FOR_EMI_5_BANK){
                    $data['res']['emi_month']  = $this->pmt(13, 1, $data['res']['discountPrice']);
                    $data['res']['idMinPrice'] = $data['res']['emi_month'];
                }
                //print_r($data); die;
                $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param);
                $data['similar_product'] = $this->libprodtl->getsimiliar_prod($param);
                $data['res2']            = $this->libprodtl->getrecently_viewed($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
                $data['sid']           	 = urldecode($_GET['sid']);
                if (!empty($mobile)){
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/product_detail", $data);
                } else {
                    $param['name']         = 'categorydetail';
                    $data['productId']     = $id;
                    
                    $data['categorynames'] = $this->libmenu->getprodtlbredacrum($param);
                    $data['res2']          = $this->liburl->get_categorydetails($param);
                    $data['resp']          = $this->lib_home->get_clerance();
            
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/category_detail", $data);
                    $this->load->view('web/include/footer', $data);
                }
                
            } else if ($getdtl['type'] == 'category'){
                $data['seoname'] = $this->uri->segment(1);
                if ($_REQUEST['start']) {
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }
                $param['percentage1'] = $param['percentage'];
                
                //print_r($data['categorynames']);die; 
                
                $param['cid']   = $param['id'];
                $data['seoid']  = $param['cid'];
                $param['count'] = true;
                // print_r($param);die;
                //print_r($param); die;
                $data['res']= $this->liburl->get_categorydetails($param);
                $data['secondsubcat'] = $this->libmenu->get_dropdownmenu();
                
                //$data['getfilter'] = $this->liburl->getfilter($param);
                $data['getfilter']['brand']=$this->liburl->getfilterbrand($param);
                $filteravailable=$this->liburl->getfilteravailable($param);
                $data['getfilter']['avalibale']['cnt']=$filteravailable[0]['proavailablecnt'];
                $filternotavailable=$this->liburl->getfilternotavailable($param);
                $data['getfilter']['notavaliable']['cnt']=$filternotavailable[0]['pronotavailablecnt'];
                $filterprocnt=$this->liburl->getfilterprocnt($param);
                $data['getfilter']['procnt']['cnt']=$filterprocnt[0]['procnt'];
                $filterminprice=$this->liburl->getfilterminprice($param);
                $data['getfilter']['minprice']=$filterminprice[0]['minprice'];
                $filtermaxprice=$this->liburl->getfiltermaxprice($param);
                $data['getfilter']['maxprice']=$filtermaxprice[0]['maxprice'];
                
                $data['getsubcat'] = $this->liburl->getsubcat($param);
                $data['getcat']    = $this->liburl->getcat($param);
                $data['start']     = $param['start'];
                $mobile            = $this->agent->is_mobile();
                $desktop           = $this->agent->is_browser();

                $brands=array();
                foreach($data['res']['product'] as $resval){
                    $brands[$resval['brandid']]=$resval['brandname'];
                }
                $brandsstring=implode(',', $brands);
                //$catbrands=from here it is pending
                $catseoname=str_replace('-', ' ', $this->uri->segment(1));
                //$data['metadescription']="Find your next ".$catseoname." from our extensive selection online with all the major brands like ".$brandsstring." in India. Best Prices , Quick Delivery & 100% genuine ".$catseoname."";
                $data['metadescription']=$data['res']['meta_description'];
                //echo $data['res']['meta_description']; die;

                
                if ($param['start'] > 0){
                    
                    if(!empty($mobile)){
                        $this->load->view("mobile/ajax_productlist", $data);
                    } else {
                        $this->load->view("web/include/ajax_productlist", $data);
                    }
                } else {
                    
                    $mobile  = $this->agent->is_mobile();
                    $desktop = $this->agent->is_browser();
                    
                    if (!empty($mobile)){
                        $this->load->view('mobile/include/header', $data);
                        $this->load->view("mobile/category_listing", $data);
                    } else {
                        //print_r($data); die;
                        $param['id']              = intval($id);
                        $catName                 = $this->uri->segment(1);
                        $data['catname']         = $catName;
                        $catId                   = $this->uri->segment(2);
                        $data['catid']           = $catId;
                        $data['maincaturl']      = ADMINURL . $catName . '/' . $catId;
                        //echo "hi"; die;    
                        //print '<pre>'; print_r($data);die; 
                        //print_r($param);
                        $param['categoryname']   = $data['catname'];
                        $param['categoryno']     = $catId;
                        $data['getbrandname']    = $this->libmenu->getbrandname($param);
                        $param['name']           = 'categorymain';
                        $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param);
                        $data['getcatname']      = $this->libmenu->getcatname($param);
                        //print_r($data['categorynames']);
                        $data['banner_category'] = $this->lib_home->get_home_dtl();
                        $this->load->view('web/include/header', $data);
                        $this->load->view("web/category", $data);
                        $this->load->view('web/include/footer', $data);
                    }
                }
            } else if ($getdtl['type'] == 'brand'){
                if ($_REQUEST['start']) {
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }

                if($_REQUEST['catid']){
                  $param['catid']=$_REQUEST['catid'];
                }
                if($_REQUEST['level']){
                  $param['level']=$_REQUEST['level'];
                }
                if($_REQUEST['searchText']){
                    $param['search']=addslashes($_REQUEST['searchText']);
                }else{
                    $param['search']=addslashes($_REQUEST['search']);
                }
                if($_REQUEST['catname']){
                    $param['catname']=$data['catname']=$_REQUEST['catname'];
                }
                if($_REQUEST['maincat']){
                    $param['maincat']=$_REQUEST['maincat'];
                }
                if($_REQUEST['filter']){
                    $param['filterdtl']=$_REQUEST['filter'];
                }
                if($_REQUEST['brand']){
                    $param['branddtl']=$_REQUEST['brand'];
                }
                if($_REQUEST['avaliable']){
                    $param['avaliable']=$_REQUEST['avaliable'];
                }
                if($_REQUEST['sort']){
                    $param['sort']=$_REQUEST['sort'];
                }
                if($_REQUEST['pricestart']){
                    $param['minprice'] =$_REQUEST['pricestart'];
                }
                if($_REQUEST['pricend']){
                    $param['maxprice']=$_REQUEST['pricend'];
                }
                
                $param['brandid']=intval($id);
                //$param['search']=$param['brandname']=$name1;
                $param['search']=$param['brandname']=$data['brandname']=$name1;
                //echo $param['start']; die;
                $data['module']=$param['module']='brandsfilters';
                $catfilter=$this->libsearch->catfilter($param);
                //echo '<pre>';
                //print_r($catfilter); die;
                $data['filter']=$catfilter;
                $data['res']= $this->libsearch->prouctsearch($param);
                //print_r($data['res']); die;
                $data['resp']['product']=$data['res']['product'];

                $metacats=array();
                foreach($data['resp']['product'] as $brandmeta){
                    $metacats[$brandmeta['third_catid']]=$brandmeta['third_category'];
                }
                $brandsmeta=$this->liburl->brandsmeta($param);
                $metacatsstring=implode(',', $metacats);

                $data['metadescription']="Wide range of  musical instruments including ".$metacatsstring." from ".$param['brandname']." is now avaliable at upto 35% discount with Best Prices , Quick Delivery & 100% genuine product";
                /*if(!empty($mobile)){
                    //print '<pre>';print_r($data['hotdeals']); die;
                    $data['banner']   = $this->lib_home->get_home_dtl();
                    $data['category'] = $data['banner'];//$this->lib_home->get_home_dtl();
            
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view('mobile/product_searching',$data);
                }else{
                    //echo "hi"; die;
                    $data['leftsidefalse']=true;
                    $data['webbanner']       = $this->lib_home->get_home_dtl();
                    $data['banner_category'] = $data['webbanner'];   // $this->lib_home->get_home_dtl();
                    //print_r($data); die;
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/searchproduct",$data);
                    $this->load->view('web/include/footer', $data);
                }*/

                if($param['start'] > 0){
                    if (!empty($mobile)){
                         $this->load->view('mobile/ajax_productsearch',$data);
                    }else{
                        $this->load->view("web/include/ajaxsearchproductlist", $data);
                    }
                }else{
                    if (!empty($mobile)){
                        $this->load->view('mobile/include/header',$data);
                        $this->load->view('mobile/product_searching',$data);
                    }else{
                        $data['leftsidefalse']=true;
                        $data['banner_category']=$data['webbanner']=$this->lib_home->get_home_dtl();
                        $this->load->view('web/include/header',$data);
                        $this->load->view("web/searchproduct",$data);
                        $this->load->view('web/include/footer',$data);
                    }
                }
            }
        } else {
            $param['module']         = 'homehotdeals';
            $data['clerance']        = $this->lib_home->get_clerance();
            $data['newarrivals']     = $this->lib_home->newarrival($param);
            $data['hotdeals']        = $this->lib_home->hotdeals($param);
            $data['signupstate']     = $this->liblogin->getsignupStates();
            //print '<pre>'; print_r($data['states']); die('hiii');
            $data['newarrivalbooks'] = $this->lib_home->newarrivalbooks();
            // print_R($data['newarrivals']); die('hiiii123');
            $data['menus']           = $this->libmenu->get_dropdownmenu();
            //echo '<pre>';
            //print_r($data['menus']); die;
            $mobile                  = $this->agent->is_mobile();
            $desktop                 = $this->agent->is_browser();
            //$mobile=true;
            //echo '<pre>';
            //print_r($data['menus']); die;
            if(!empty($mobile)){
                //print '<pre>';print_r($data['hotdeals']); die;
                $data['banner']   = $this->lib_home->get_home_dtl();
                $data['category'] = $data['banner'];//$this->lib_home->get_home_dtl();
        
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/home", $data);
            }else{
                //echo "hi"; die;
                $data['webbanner']       = $this->lib_home->get_home_dtl();
                $data['banner_category'] = $data['webbanner'];   // $this->lib_home->get_home_dtl();
                //echo '<pre>';
                //print_r($data['banner_category']); die;
                $this->load->view('web/include/header', $data);
                $this->load->view('web/index', $data);
                $this->load->view('web/include/footer', $data);
            }
        }
    }
    
    public function priceFormat($amt){
        setlocale(LC_MONETARY, 'en_IN');
        return money_format('%!.0n', floor($amt)) . '/-';
    }
    
    public function pmt($apr, $term, $loan)
    {
        $term   = $term * 12;
        $apr    = $apr / 1200;
        $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
        return $amount;
    }
}
?>