<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Furtados</title>
    <!-- Bootstrap core CSS -->
 
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo SITECSS;?>ie10-viewport-bug-workaround.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo SITECSS;?>font-awesome.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <link href="<?php echo SITECSS;?>bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="<?php echo SITECSS;?>responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo SITECSS;?>custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">-->
  </head>

  <body>   
  <div class="mobile-container">
   <div class="100per">
    <!-- header starts -->
       <header>
       <div class="container">
       <!-- left section starts -->
        <div class="col-xs-7 col-sm-7 col-md-7 pl0">
         <div class="col-xs-1 col-sm-1 col-md-1 nav-icon pl0"><a href="#" Onclick="window.history.go(-1); return false;"><i class="fa fa-angle-left"></i></a></div>
         <!-- logo starts -->
          <div class="col-xs-11 col-sm-11 col-md-11 pl0 pr0"><a href="<?php echo SITEMOBURL;?>"><img class="img-responsive" src="<?php echo SITEIMAGES?>mobimg/logo.jpg" alt=""></a></div>
         <!-- logo finsih -->
        </div>
       <!-- left section finish -->
       <!-- right section starts -->
        <div class="col-xs-5 col-sm-5 col-md-5 pl0 pr0">
         <!--<span class="pull-right">
          <a href="javascript:;" class="pull-right col-xs-4 col-sm-4 col-md-4"><img src="<?php echo SITEIMAGES?>mobimg/search.jpg" alt=""></a>
          <a href="javascript:;" class="pull-right col-xs-4 col-sm-4 col-md-4"><img src="<?php echo SITEIMAGES?>mobimg/cart.jpg" alt="car"></a>
          <a href="javascript:;" class="pull-right col-xs-4 col-sm-4 col-md-4"><img src="<?php echo SITEIMAGES?>mobimg/myaccount.jpg" alt=""></a>
         </span>-->
        </div>
       <!-- right section finsih -->
         </div>
       </header>
   </div>
  </div>
    <!-- header finsih -->
     <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
   <!--  <script src="js/bootstrap.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo SITEJS;?>ie10-viewport-bug-workaround.js"></script>
  <script src="<?php echo SITEJS;?>custom.js"></script>
    <!-- for swatches popup starts -->
    <!-- JavaScript Includes -->
  <script src="transition.js"></script>
  <script src="modal.js"></script>
  </body>
</html>


