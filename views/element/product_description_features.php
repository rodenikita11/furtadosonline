<div class="container mrt40">
<div class="row">
	<div class="col-md-12">
	   <div class="box-border block-form">
			<!-- Nav tabs -->
			<ul class="nav nav-pills  nav-justified">
				<li class="active"><a href="#description" data-toggle="tab">Description</a></li>
				<li><a href="#additional" data-toggle="tab">Specification</a></li>
				<li><a href="#fetured" data-toggle="tab">Features</a></li>
				<li><a href="#review" data-toggle="tab">Review</a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<input type="hidden" value="<?php echo $result1[0]['proid'];?>" name="hidden_proId" id="hidden_proId" readonly="readonly"/>
				<div class="tab-pane active" id="description">
					<br>
					<h3>Product Details</h3>
					<hr>
					<?php echo $result1[0]['prodescription'];?> 
				</div>
				<div class="tab-pane" id="additional">
					<br>
					<h3>Product Specification</h3>
					<hr>
					<div class="row">
						<?php echo $result1[0]['prolongspecification'];?> 
					</div>

				</div>
				<div class="tab-pane" id="review">
					<br>
					<div class="row">
						<div class="col-md-12">
							<h3>Buyers Review(s)</h3>
							<hr>
							<?php $param['pid']=$result1[0]['proid']; 
								$result2=$this->Do_review->getproductreview($param);
							?>
							<?if($result2){?>
							<?php foreach($result2 as $row){ ?>
							<div class="review-header">
								<h5><?php echo $row['reviewauthor'];?></h5>
								<div class="product-rating">
									<div class="stars">
										<?for($i=1;$i<=5;$i++){
										if($i<=$row['reviewrating']){?>
											<span class="st-active"><i class="fa fa-star st-active"></i></span>
										<?php } else{?>
											<span class="st-active"><i class="fa fa-star-o st-active"></i></span>
										<?php }}?>
									</div>
								</div>
								<small class="text-muted"><?echo $row['reviewdate'];?></small>
							</div>
							<div class="review-body">
								<p><?php echo $row['reviewtext'];?></p>
							</div>
							<hr>
							<?php } } else { ?>
							<p>No review is available, be the first one to review it.</p>
							<?php } ?>
						</div>
					</div>
				   <?php echo $this->load->view('element/review_write_popups.php');?>
				</div>
			<div class="tab-pane" id="fetured">
					<br>
					<h3>Product Features</h3>
					<hr>
				   <?php if(!empty($result1['attgrp'])) { 
				   foreach($result1['attgrp'] as $key=>$value){ ?>
					<div class='attr'>
						<div class="attrgrp" ><? echo $key?></div>
							<?php foreach($value as $key1=>$value1){ $attributes.=$value1.', '; }?>
						<div class="attname"><? echo rtrim($attributes,", "); unset($attributes);?></div>
					</div>
					<div class="clear"></div>
					<?} } else { ?>
					<div class='attr'>
						No features for this product.
					</div>
					<?php }?>
		   </div>
			</div>
		</div>
	</div>
</div>
</div>