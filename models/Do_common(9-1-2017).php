<?php
class Do_common extends CI_Model{
   function __construct(){
		parent:: __construct();
		$this->load->database();
	}
	
	public function getallmenu(){
		 //$select ="SELECT cm1.catid, cm1.catname, cm1.catimage, cm1.catlevel, cm1.catparent_id, cm1.catsort_order,cm1.catstatus,cm2.catname as level1,cm2.catid as levelid1,cm2.catimage as catimg,cm2.catparent_id as parentid,cm3.catname as level2,cm3.catid as levelid2,cm4.catname as level3,cm4.catid as levelid3 FROM `category_master`as cm1 join category_master as cm2 on cm1.catid=cm2.catparent_id join category_master cm3 on cm2.catid=cm3.catparent_id  join category_master as cm4 on cm3.catid=cm4.catparent_id";
		 $select="SELECT cm1.catid, cm1.catname, cm1.catimage, cm1.catlevel, cm1.catparent_id, cm1.catsort_order,cm1.catstatus,cm2.catname as level1,cm2.catid as levelid1,cm2.catimage as catimg,cm2.catparent_id as parentid,cm3.catname as level2,cm3.catid as levelid2,cm4.catname as level3,cm4.catid as levelid3 FROM `category_master`as cm1 join category_master as cm2 on cm1.catid=cm2.catparent_id left join category_master cm3 on cm2.catid=cm3.catparent_id  left join category_master as cm4 on cm3.catid=cm4.catparent_id order by cm2.catsort_order asc";
		
		$res=$this->db->query($select);
		return $res1=$res->result_array();
		
		
	}
	
		public function getfilter($param){
				if($param['cid']){
					 // $query="select bm.brandname,bm.brandid,(select count(proid) from product_to_category where category_id='".$param['cid']."') as procnt ,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid and pc.category_id='".$param['cid']."') as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and pc.category_id='".$param['cid']."') as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and pc.category_id='".$param['cid']."') as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where pc.category_id='".$param['cid']."' GROUP by bm.brandid"; 

					 $query="select bm.brandname,bm.brandid,(select count(proid) from product_to_category where FIND_IN_SET('".$param['cid']."',category_path)) as 'procnt' ,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid and FIND_IN_SET('".$param['cid']."',pc.category_path)) as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 and FIND_IN_SET('".$param['cid']."',category_path)) as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 and FIND_IN_SET('".$param['cid']."',category_path)) as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where FIND_IN_SET('".$param['cid']."',category_path) GROUP by bm.brandid";
					$res=$this->db->query($query);
					if($res){
					 return $res1=$res->result_array($query);  
				   }
				 
					
			}
			

	}
	public function product_searchfilter(){
		
				
				 $query="select bm.brandname,bm.brandid,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid ) as 'probrandcnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=1 ) as 'proavailablecnt',(SELECT count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid where isAvailable=0 ) as 'pronotavailablecnt'  from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid  GROUP by bm.brandid";
				 
				 $res=$this->db->query($query);
					if($res){
					 return $res1=$res->result_array($query);  
				   }
			
		
	}
	
	function getpricerange($param){
	        if($param['cid']){
			   $sql = "SELECT  min(a.discountprice) as minprice , max(a.discountprice) as maxprice FROM product_master a join product_to_category b on a.proid = b.proid  left join category_master  c on b.category_id=c.catid where a.proactive=1 and FIND_IN_SET('".$param['cid']."',b.category_path) OR  c.catparent_id='".$param['cid']."'";  
			
			$res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      } 
			}
	
	}
	function getattribute($param){
		  if($param['cid']){
		   $sql ="select ag.*,am.* from category_master cm join product_to_category pc on cm.catid=pc.category_id join product_master pm on pm.proid=pc.proid join product_attribute_mapping pam on pm.proid=pam.proid join attribute_master am on am.attriid=pam.attriid join attribute_group ag on ag.attrigrpid=am.attrigrpid where pc.category_id='".$param['cid']."' and ag.attrgrpfilter='1'";
		 $res=$this->db->query($sql);
			if($res){
				
		     	return $res1=$res->result_array($sql);
		      } 
			}
	   	
	}
}
?>
