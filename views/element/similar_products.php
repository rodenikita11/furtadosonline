<?php  #--Created by sanjay , 22/Sep
	$params['proid']= $this->proid;
	$params['limit'] = 14;
	$params['start'] = 0;
	$reletedPro = $this->Do_product->getrelatedproduct($params);
		if(!empty($reletedPro)) { ?>
<div class="container pdl mrt10">
<div class="row">
 <div class="col-md-12">
	                    <div class="widget-title" style="margin-top:0px;">
                          <i class="fa fa-thumbs-up"></i> Similar Products
                        </div>
						<?php foreach($reletedPro as $proSellerId=>$proSellerVal) { 
						?>
						<div class="col-md-3 mrt10">
                            <div class="widget-block">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="<?php echo $proSellerVal['proURL']; ?>"><img class="img-responsive" src="<? echo $this->lib_storeuri->getprothumbnail($proSellerVal['image'],80);?>" alt="<?php echo $proSellerVal['proname'];?>" title="<?php echo $proSellerVal['proname'];?>"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="block-name">
                                            <a href="<?php echo $proSellerVal['proURL']; ?>" class="product-name"><?php echo CommonHelper::return_part_of_string($proSellerVal['proname'],45); ?></a>
                                            <p class="product-price">
											<?if($proSellerVal['marketprice']>$proSellerVal['finalprice']){?>
											<span><i class="fa fa-inr"></i> <?php echo $proSellerVal['marketprice']; ?></span> <i class="fa fa-inr"></i><?php echo $proSellerVal['finalprice'];?>
											<?php }else { ?>
											<?php echo $proSellerVal['finalprice']; ?> 
											<?php }?>
											</p>
                                        </div>
                                        <div class="product-rating">
                                            <?php 
											$param['pid']=$proSellerVal['proid'];
											$proReview = $this->Do_review->getreviewcount($param);
												$limit=5;
												$fraction=$limit-$proReview[0]['average'];
											?>
											<div class="stars">
                                                    <?php for($i=1;$i<=5;$i++){ ?>
															<?php if($i<=$proReview[0]['average']) { ?>
															<span class="st-active"><i class="fa fa-star st-active"></i></span>
															<?php } else {?>
															<span class="st-active"><i class="fa fa-star-o st-active"></i></span>
													<?php } } ?>
                                                </div>
                                            <a href="javascript:void(0);" class="review"><?php echo round($proReview[0]['average']);?> Reviews</a>
                                        </div>
                                        <p class="description"><?php echo strip_tags(CommonHelper::return_part_of_string($proReview['proshortspecification'],20));?></p>
                                    </div>
                                </div>
                            </div>
						</div>
						<?php } ?>
 </div>
</div>
</div>
<?php } ?>