<?php


 class Libcart{
	      public $ci;
          public function __construct(){
             $CI = & get_instance();
             $CI->load->model('Do_cart');
                          //$CI->load->library('Lib_storeuri');
			 $this->ci = $CI;
             
         }

		public function addtocart($param){
			 if((int)$param['pid']){
				$param['pid']=(int)$param['pid'];
				if($param['qty']){
					$param['qty']=$param['qty'];
				 }else{
					$param['qty']=1;
				}
				if($param['sid']){
					$param['sid']=$param['sid'];
				}
				if($param['selected_combo_pro']){
					
					$param['selected_combo_pro']=$param['selected_combo_pro'];
				}
				//print_r($param);
				 $res['addtocart']= $this->ci->Do_cart->addtocart($param);

					if($res){
					  	$res['currentcart']=$this->ci->Do_cart->getcurrentcart($params); 
						if($this->ci->libsession->getSession('cartreplica')){
							$cartreplica['cartreplica']=$res['currentcart'];
							$this->ci->libsession->setSession($cartreplica);
						}
					  	return $res['currentcart']; 
				    }	
				}else{
				    echo "there is some problem with the parameter";
				}
            		
		}
		
		public function removefromcart($param){
	        $pid = $param['pid'];
			if(isset($_SESSION['cart'][$pid])){
				unset($_SESSION['cart'][$pid]);
				$res['currentcart']=$this->ci->Do_cart->getcurrentcart();
				$cartreplica['cartreplica']=$res['currentcart'];
				$this->ci->libsession->setSession($cartreplica);
				if($param['orderinvoice_no']){
					$this->ci->Do_cart->removefromcart($param);
				}
				$data['error']='false';
			}else{
				$data['error']='true';
			}
			return $data;
		}
		public function removefromcombocart($param){
			// print_R($param); die;
			         $pid = $param['pid'];
					 $comboid=$param['combo_id'];
						if($_SESSION['cart'][$pid]['combo_product'][$comboid])
						{
							unset($_SESSION['cart'][$pid]['combo_product'][$comboid]);
							$data['error']='false';
							
						}
						else
						{
							$data['error']='true';
							
						}
			 return $data;
		}
		public function getcurrentcart($param=false){
			 $res['currentcart']=$this->ci->Do_cart->getcurrentcart($params); 
			 return $res['currentcart']; 
		}
		
		public function updatecart($param){
			if((int)$param['pid']){
				
				$param['pid']=(int)$param['pid'];
				if($param['qtychange']){
					$param['qty']=$param['qtychange'];
				 }
                     
				 $res['addtocart'] = $this->ci->Do_cart->addtocart($param);

					if($res){
					  $res['currentcart']=$this->ci->Do_cart->getcurrentcart($params); 
					  //print_R($res); die; 
					  return $res['currentcart']; 
				    }	
				}else{
				    echo "there is some problem with the parameter";
				}   
			
			
		}
		public function updatebundlecart($param){
			if((int)$param['pid']){
				
				$param['pid']=(int)$param['pid'];
				if($param['qtychange']){
					$param['qty']=$param['qtychange'];
				 }
                     
				 $res['addtobundlecart'] = $this->ci->Do_cart->addtobundle($param);

					if($res){
					  $res['bundlecart']=$this->ci->Do_cart->getbundlecart($params); 
					  //print_R($res); die; 
					  return $res['bundlecart'];  
				    }	
				}else{
				    echo "there is some problem with the parameter";
				}   
			
			
		}
		
		public function addtobundle($param){
            
			 if((int)$param['pid']){
				$param['pid']=(int)$param['pid'];
				if($param['qty']){
					$param['qty']=$param['qty'];
				 }else{
					$param['qty']=1;
				}
				

				 $res['addtobundle'] = $this->ci->Do_cart->addtobundle($param);

					if($res){
					  $res['bundlecart']=$this->ci->Do_cart->getbundlecart($params); 
					  
					  return $res['bundlecart']; 
				    }	
				}else{
				    echo "there is some problem with the parameter"; 
				}	
		}

		

		public function getbundlecart($param=false){
			 $res['bundlecart']=$this->ci->Do_cart->getbundlecart($params); 
			 return $res['bundlecart']; 
			
		}
		
		public function addquote($param){
			if (!empty($_SERVER['HTTP_CLIENT_IP']))  
				{
				  $param['ip']=$_SERVER['HTTP_CLIENT_IP'];
				}
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   
				{
				  $param['ip']=$_SERVER['HTTP_X_FORWARDED_FOR'];
				}
			else
				{
				  $param['ip']=$_SERVER['REMOTE_ADDR'];
				}

			$res=$this->ci->Do_cart->getquote($param);
			
             if($res== true){
             	
				return $res; 
				 
			 }else{
				 return false;  
			 } 			
			
		}
		
		public function getbundledetail($param){
			if($param){
				
				$res=$this->ci->Do_cart->getbundlecartdetail($param);
				if($res){
							 foreach ($res as $row) {
							 
							 
							 $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
							 $proname=str_replace("--","-", $proname) ;
							 $proname=strtolower(rtrim($proname,'-')); 
							 $proid=$row['productId'];
							 $prodata[$row['productId']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
		                     $prodata[$row['productId']]['name']=$row['proname'];
							 if($row['prothumbnail']){
							 $prodata[$row['productId']]['img']=$row['prothumbnail'];
							 }else{
								$prodata[$row['productId']]['img']=SITEIMAGES.'mobimg/noimage1.png'; 
							 }
							 
							 
							 $prodata[$row['productId']]['price']=$row['onlinePrice'];
							 if($_SESSION['final_bundle'][$row['productId']]){
								 $prodata[$row['productId']]['qty']=$_SESSION['final_bundle'][$row['productId']]['qty']; 
							 }else{
							 $prodata[$row['productId']]['qty']=$row['quantity'];
							 }
							 $prodata[$row['productId']]['discountPrice']=$row['discountPrice'];
							 $prodata[$row['productId']]['proweight']=$row['proweight'];
							  if($row['onlineprice'] > $row['discountPrice'] ){
							  $prodata[$row['productId']]['amount']=$row['discountPrice']*$prodata[$row['productId']]['qty'];
							}else{
								$prodata[$row['productId']]['amount']=$row['onlinePrice']*$prodata[$row['productId']]['qty'];
							}
							$prodata[$row['productId']]['bundleprice']=$row['productPrice'];
							
			                
						}
						    
					       $_SESSION['final_bundle']=$prodata;
							return $prodata;
				}
			}else{
				
				echo "parameter missing"; die;
			}
			
		}
		
		public function promocodecheck($param){
			if($param['user_email']){
				$param['emailid']=$param['user_email'];
				$param['giftcardid']=$param['promocode'];
				$res=$this->ci->Do_cart->checkgiftcode($param);
				if($res==1){
					$res1=$this->promocodeapply($param);
					return $res1;
				}else if(!is_array($res)){
					return $res;
				}else if(!empty($res)){
					$promodetails['coupon_code_id']=$res[0]['coupon_id'];
					$promodetails['code']=$res[0]['code'];
					$promodetails['total_card_value']=$res[0]['discount'];
					$promodetails['validity']=$res[0]['date_end'];
					return $promodetails; 
				}else{
					return false;
				}
			}
		}
		
		public function promocodeapply($param){
			if($param['user_email']){
				$param['emailid']=$param['user_email'];
				$param['giftcardid']=$param['promocode'];
				$res=$this->ci->Do_cart->getgiftcodevalue($param);
				if($res){
					$promodetails['gift_card_id']=$res[0]['id'];
					$promodetails['code']=$res[0]['gift_card_id'];
					$promodetails['total_card_value']=$res[0]['gift_card_value'];
					$promodetails['validity']=$res[0]['validity'];
					$promodetails['order_id']=$res[0]['order_id'];
					return $promodetails; 
				}else{
					
					return false;
				}
				
				
			}
			
			
			
		}

           		
}				
?>
