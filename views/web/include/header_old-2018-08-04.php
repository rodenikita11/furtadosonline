<style type="text/css">.container {padding-left:15px;}</style>
<?include('head.php'); $searchval=$_GET['search'];?>
<body class="cnt-home scrollbar" data-scrollload="">
   <!-- ============================================== HEADER ============================================== -->
   <header class="header-style-1" data-flashmessage="<?php echo $this->session->flashdata('changepassword'); ?>">
      <!-- ============================================== TOP MENU ============================================== -->
      <div class="navbar navbar-fixed-top">
      <div class="top-bar animate-dropdown">
         <div class="container">
            <div class="header-top-inner">
               <div class="cnt-account">
                  <ul class="list-unstyled">
                     <li><a href="tel: 022 - 4287 5050/60">Customer Services :  022 - 4287 5050/60</a></li>
                     <li>
                        <a>10 AM - 6:30 PM (Mon to Sat)</a>
                       </li>
                     <!--<li class="TO"><a href="<?php echo SITEMOBURL?>user/trackorder"><img class="Mr-6" src="<?php echo WEBIMG;?>track.png">Track Order</a></li>-->
                  </ul>
               </div>
               <!-- /.cnt-account -->
               <!-- /.cnt-cart -->
               <div class="clearfix"></div>
            </div>
            <!-- /.header-top-inner -->
         </div>
         <!-- /.container -->
      </div>

      <!-- /.header-top -->
      <!--<div class="topmar animate-dropdown" style="background: #cc0028; color: #fff; font-size: 15px; padding-top: 3px; padding-bottom: 3px;">
         <div class="">
            <div class="header-top-inner">
             <marquee behavior="scroll" direction="left"><i>Due to the uncertainty regarding the All India Transporters Strike, scheduled to begin on 20th July, despatch & delivery of products may be delayed. We apologies for any inconvenience that this may cause and thank you for your understanding.</i></marquee>

               <div class="clearfix"></div>
            </div>
         </div>
      </div>-->
      <!-- ============================================== TOP MENU : END ============================================== -->
      <div class="main-header">
         <div class="container">
            <div class="row" style="margin-left: -5px">
               <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                  <!-- ============================================================= LOGO ============================================================= -->
                  <div class="logo"> <a href="<?php echo SITEURL;?>"> <img src="<?php echo WEBIMG;?>logo.png" alt="logo"> </a> </div>
                  <!-- /.logo -->
                  <!-- ============================================================= LOGO : END ============================================================= -->
               </div>
               <!-- /.logo-holder -->
               <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder" style="z-index:9999">
                  <!-- /.contact-row -->
                  <!-- ============================================================= SEARCH AREA ============================================================= -->
                  <div class="search-area">
                     <form method="GET" action="<?php echo SITEMOBURL?>product/search"  class="formset">
                        <div class="control-group">
                           <!--<input type="text"  value='<? echo $_GET["searchText"]?>' autocomplete="off" name="searchText" id="twotabsearchtextbox" placeholder="Search Products" onkeyup="suggestKeywords(this.value, 1);" value="<?php echo $searchText; ?>" oninput="test()"/>-->
                           <input type="text"  value='<? echo $_GET["searchText"]?>' autocomplete="off" name="searchText" id="twotabsearchtextbox" placeholder="Search Products" value="<?php echo $searchText; ?>" oninput="test()"/>
                        <div class="suggestionsBox" id="suggestions" style="display: none;" >
                            <div class="suggestionList" id="suggestionsList"></div>
                            <div style="clear:both;"></div>
                        </div>
                           <button class="search-button" href="javascript:viod(0)" name="submit" id="submit" type="submit" style="padding: 12px 12px 12px"></button>
                        </div>
                     </form>
                  </div>
                  <!-- /.search-area -->
                  <!-- ============================================================= SEARCH AREA : END ============================================================= -->
               </div>
               <!-- /.top-search-holder -->
               <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
                  <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
                  <div class="cnt-block">
                     <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small" style="margin-right: 3px;">
                           <a href="<?echo SITEMOBURL?>product/viewcart" class="dropdown-toggle" data-hover="dropdown" style="position: relative;top: 0px;left: 2px;" id="headercart"><i class="icon fa fa-shopping-bag" style="color: #cc0028; font-size: 14px"></i>&nbsp;<span class=" margin-left value" id="currentitemcount"></span></a>
                        </li>
                        <li class="dropdown dropdown-small" style="margin-right: 2px;">
                           <?if($this->libsession->isSetSession('usrid')){?>
                           <a href="<?echo SITEMOBURL?>user/myaccount?name=wishlist" class="dropdown-toggle" data-hover="dropdown" style="position:  relative;top: 1px;left: 1px;" > <i class="icon fa fa-heart" id="heart" style="color: #cc0028;font-size: 14px"></i>&nbsp;<span class=" margin-left value" id="wishlistcount"></span></i></a>
                           <?}else{?>
                           <a href="#login" class="dropdown-toggle" data-hover="dropdown" data-toggle="modal" style="position:  relative;top: 1px;left: 1px;">
                           <i class="icon fa fa-heart" style="color: #cc0028;font-size: 14px"></i>&nbsp;<span class=" margin-left value" id="wishlistcount"></span></i></a>
                           <?}?>
                        </li>
                        <li class="dropdown dropdown-small" id="mainloginicon" style="height: 44px;">
                           <?if(!$this->libsession->isSetSession('usrid')){?>
                           <p style="text-align: center;"><a href="#login" class="dropdown-toggle" data-hover="dropdown" data-toggle="modal"><span class="value">Log In</span> &nbsp;&nbsp;<i class="fa fa-user" aria-hidden="true" style="color: #cc0028;font-size: 14px"></i></a></p>
                           <?}else{?>
                           <?$user=explode(' ', trim($this->libsession->getSession('usrname')));?>
                           <a href="#" class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" style=" margin-left: 14px;" ><span class="value">Hi,<?php echo substr($user[0],0,8);?></span><i class="fa fa fa-sort-desc" aria-hidden="true" style="position: relative;top: -3px;left: 12px"></i></a>
                           <ul class="dropdown-menu">
                              <li> <a href="<?php echo SITEMOBURL?>user/myaccount">My Account</a></li>
                              <!-- <li> <a href="<?php echo SITEMOBURL?>product/getorderbyuser">My Order</a> -->
                              <li><a href="<?php echo SITEMOBURL?>user/logout">Log Out</a></li>
                           </ul>
                           <?}?>
                        </li>
                     </ul>
                     <!-- /.list-unstyled -->
                  </div>
               </div>
           <input type="hidden" autocomplete="off" name="searchText" id="twotabsearchtextbox1" placeholder="Search Products" onkeyup="suggestKeywords(this.value, 1);" value="<?php echo $searchText; ?>" oninput="test1()"/>
               <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
               <!-- /.top-cart-row -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.main-header -->
      <!-- ============================================== NAVBAR ============================================== -->
      <?php include('left_menu.php'); ?>
      <!-- /.header-nav -->
      <!-- ============================================== NAVBAR : END ============================================== -->

</div>
   </header>
   <!-- Modal HTML Log IN -->
   <?//echo  __DIR__; //die;?>
   <?require_once ( __DIR__ .'/login.php');?>
   <!--Modal Log In:End HTML -->
   <!-- Modal SIGN UP HTML -->
   <?include (__DIR__ .'/signup.php');?>
   <!-- Modal SIGN UP:END HTML -->
   <!-- Modal emi HTML -->
   <?require_once (__DIR__ .'/emi.php');?>
   <!-- Modal emi:END HTML -->
   <!--modal popup of review-->
   <?include (__DIR__ .'/reviewpopup.php');?>
   <!--modal popup of review:end -->

   <!-- ============================================== HEADER : END ============================================== -->
   <style type="text/css">
       #toTop {
    padding: 8px 13px;
    background: #cd2327;
    color: #fff;
    position: fixed;
    bottom: 0;
    right: 5px;
    z-index: 999;
    display: none;
    margin-right: 10px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
  }
   </style>
<script>
function suggestKeywords(inputString, type){
    if(inputString.length <= 1) {
      $('#suggestions').fadeOut();
    }else{


      //alert(search_type);
      $('#searchText').addClass('load');
      //var term=jQuery('#search').val()
      $.ajax({
        type: "GET",
        //url: PREFIX_RUN+'autosuggest.php',
        url: "<?php echo SITEMOBURL?>product/autosuggestweb",
        data:  {term:inputString},
        success: function(data){ //alert(data);
          if(data == ''){
            $('#suggestions').fadeOut();
            $('#suggestionsList').hide('fast');
          }else if(data.length > 0) {
            $('#suggestions').fadeIn();
            $('#suggestionsList').show('fast');
            $('#suggestionsList').html(data);
            //$('#searchText').removeClass('load');

            var temp = data.split('|');
            $('#catid').val(temp[0]);
            $('#brandid').val(temp[1]);

            //
          }
        }
      });

    }
  }//suggestKeywords()
</script>
<script type="text/javascript">
  jQuery(document).ready(function(){
      /*Top button Script*/
 jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()) {
        jQuery('#toTop').fadeIn();
    } else {
        jQuery('#toTop').fadeOut();
    }
});

jQuery("#toTop").click(function () {
   jQuery("html, body").animate({scrollTop: 0}, 1000);
});
/*Top button Script*/
  })
</script>