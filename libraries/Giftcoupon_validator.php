<?php
class Giftcoupon_validator{
	public $ci, $curdate, $curdateobject, $curuser, $cartdata, $totalamount, $filteredproducts, $couponcond, $coupon, $amounaftercoupon, $cartreplica;
	private $param;
	public function __construct(){
		$CI = & get_instance();
		$CI->load->model('do_cart');
		$this->ci = $CI;
		$this->curdateobject=new DateTime();
		$this->curdate=$this->curdateobject->format('Y-m-d');
		$this->curuser=$this->ci->libsession->getSession('usrid');
	}

	public function validate($param=array()){
		$param['curdate']=$this->curdate;
		$coupon=array();
		$this->param=$param;
		//get gift card or coupon details
		$coupondata=$this->ci->do_cart->validategiftcoupon($param);
		$couponcount=count($coupondata);
		if($couponcount > 0){
			$categorycheck=false;
			$brandcheck=false;
			$productcheck=false;
			$statuscheck=false;
			$expirycheck=false;
			$validproduct=false;

			$coupon['code']=$coupondata[0]['code'];
			$coupon['coupon_id']=$coupondata[0]['coupon_id'];
			$coupon['name']=$coupondata[0]['name'];
			$coupon['user_ids']=$coupondata[0]['user_ids'];
			$coupon['maintype']=$coupondata[0]['maintype'];
			$coupon['category_id']=$coupondata[0]['category_id'];
			$coupon['product_id']=$coupondata[0]['product_id'];
			$coupon['coupon_desc']=$coupondata[0]['coupon_desc'];
			$coupon['discount_type']=$coupondata[0]['discount_type'];
			$coupon['discount']=$coupondata[0]['discount'];
			$coupon['type']=$coupondata[0]['type'];
			$coupon['brand_id']=$coupondata[0]['brand_id'];
			$coupon['startdate']=$coupondata[0]['date_start'];
			$coupon['enddate']=$coupondata[0]['date_end'];
			$coupon['status']=$coupondata[0]['status'];
			$coupon['usescount']=$coupondata[0]['uses_total'];
			$coupon['total']=$coupondata[0]['total'];
			$coupon['offertype']='coupon';
			$this->minamountbarrier=$coupon['total'];

			$this->coupon=$coupon;
			$couponstartdateobj=new DateTime($this->coupon['startdate']);
			$couponenddateobj=new DateTime($this->coupon['enddate']);

			$couponstartdate=$couponstartdateobj->format('Y-m-d');
			$couponenddate=$couponenddateobj->format('Y-m-d');

			$this->cartdata=$this->ci->libcart->getcurrentcart();
			$this->cartreplica=$this->cartdata;
			$this->ci->libsession->setSession(array('cartreplica'=>$this->cartreplica));
			
			//First do the generic check and then go by the types
			$modified=array();
			$couponprobrands=($this->coupon['brand_id']!='') ? (($this->coupon['brand_id']=='all') ? 'all' : explode(',', $this->coupon['brand_id'])) : '';
			$couponproducts=($this->coupon['product_id']=='all') ? 'all' : explode(',', $this->coupon['product_id']);
			foreach($this->cartdata as $key=>$val){
				$categorycheck=false;
				$brandcheck=false;
				$productcheck=false;
				$statuscheck=false;
				$expirycheck=false;

				//discounted products should be skipped from coupon code
				if($val['dicountprice'] == $val['price']){
					//check if any of the product exists in the same same category as coupon category
					$cartprocats=explode(',', $val['category_path']);
					if($this->coupon['maintype']==$val['maintype'] && (in_array($this->coupon['category_id'], $cartprocats) || $this->coupon['category_id']=='all' || $this->coupon['category_id']==0 || $this->coupon['category_id']==4)){
						$categorycheck=true;
					}
					//check if the product brand is available for coupon
					if(in_array($val['brandid'], $couponprobrands) || $couponprobrands=='all'){
						$brandcheck=true;
						if($couponproducts=='all'){
							$productcheck=true;
						}else if(in_array($val['proid'], $couponproducts)){
							$productcheck=true;
						}
					}else{
						//check if the product existst in the coupon product list
						if(in_array($val['proid'], $couponproducts) || $couponproducts=='all'){
							$productcheck=true;
						}
					}
					//check the status of the coupon
					if($coupon['status']==1){
						$statuscheck=true;
					}
					//check expiry of the coupon
					if($this->curdate >= $couponstartdate && $this->curdate <= $couponenddateobj){
						$expirycheck=true;
					}

					if($categorycheck && $brandcheck && $statuscheck && $expirycheck && $productcheck){
						$modified[]=$val;
					}
				}//discounted products should be skipped from coupon code
			}

			if(empty($modified)){
				$data['error']=true;
				$data['data']=false;
				$data['message']='This coupon can be aplied on selected products, brands and categories.';
				return $data;
			}

			$this->filteredproducts=$modified;
			$this->settotalcartamount();
			/*Now check typewise coupons
			Types of coupons
			1. Userwise
			2. Usage count
			3. Special
			4. Mimimum purchase amount
			*/
			switch($coupondata[0]['type']){
				case '1':
					$this->userwisecoupon();
					break;

				case '2':
					$this->usagecount();
					break;

				case '3':
					$this->specialcoupon();
					break;

				case '4':
					$this->minimumamountcoupon();
					break;

				default:
					$this->defaultcoupon();
			}

			if($this->couponcond){
				$applyres=$this->applycoupon();
				$rewriteres=$this->rewritecart();

				if($applyres && $rewriteres){
					$data['error']=false;
					$data['data']=$this->ci->libcart->getcurrentcart();
					$data['message']='cart returned';
					$data['coupon_code_applied']=$this->coupon['code'];
					$data['coupon_code_value']=$this->coupon['discount'];
					$data['coupon_id']=$this->coupon['coupon_id'];
					$data['validity']=$this->coupon['enddate'];
				}else{
					$data['error']=true;
					$data['data']=false;
					$data['message']='Error occurred while applying coupon, try again later';
				}
			}else{
				$data['error']=true;
				$data['data']=false;
				$data['message']='Not a valid coupon';
			}
		}else{
			//This block can be used for GIFTCARD
			$data['error']=true;
			$data['data']=false;
			$data['message']='No data output';
		}
		return $data;
	}

	public function settotalcartamount(){
		if($this->filteredproducts){
			$finalamount=0;
			foreach($this->filteredproducts as $key=>$val){
				$finalamount+=$val['amount'];
			}
			$this->totalamount=$finalamount;
		}
	}

	public function userwisecoupon(){
		$couponusers=($this->coupon['user_ids']=='all') ? $this->coupon['user_ids'] : explode(',',$this->coupon['user_ids']);
		$this->couponcond=(in_array($this->curuser, $couponusers) || $this->coupon['user_ids']=='all') ? true : false;
	}

	public function usagecount(){
		//get total count of coupon uses
		//move this query to the model
		$param['couponcode']=$this->coupon['code'];
		$param['startdate']=$this->coupon['startdate'];
		$param['enddate']=$this->coupon['enddate'];
		$usedcount=$this->ci->do_cart->couponusagecount($param);
		$this->couponcond=($usedcount < $this->coupon['usescount']) ? true : false;
	}

	public function specialcoupon(){
		$this->couponcond=($this->curdate > $this->coupon['startdate'] && $this->curdate < $this->coupon['enddate']) ? true : false;
	}

	public function minimumamountcoupon(){
		//the amount column in the database might change
		$this->couponcond=($this->minamountbarrier <= $this->totalamount) ? true : false;
	}

	public function defaultcoupon(){
		return false;
	}

	private function rewritecart(){
		$modified=array();
		foreach($this->cartdata as $key=>$val){
			foreach($this->filteredproducts as $index=>$value){
				if($val['proid']==$value['proid']){
					$modified['cart'][$val['proid']]=$value;
					break;
				}else{
					$modified['cart'][$val['proid']]=$val;
				}
			}
		}
		$this->ci->libsession->setSession($modified);
		return true;
	}

	private function applycoupon(){
		//rewrite the filter products array
		$remainingamount=$this->coupon['discount'];
		$modproducts=array();
		if($this->coupon['discount_type']=='amt'){
			foreach($this->filteredproducts as $key=>$val){
				$remainingamount=$remainingamount-$val['amount'];
				if($remainingamount > 0){
					$val['amountdeducted']=$val['amount'];
					$val['remain']=$remainingamount;
					$val['coupon']=true;
					$val['couponapplied']=($remainingamount <= 0) ? 'full' : 'partial';
					$val['couponid']=$this->coupon['coupon_id'];
					$val['couponcode']=$this->coupon['code'];
					$val['coupontype']=$this->coupon['type'];
					$val['coupondiscount']=$this->coupon['discount'];
					$val['couponappliedbool']=true;
					$val['coupondisctype']=$this->coupon['discount_type'];
					$val['dicountprice']=0;
					$val['offertype']=$this->coupon['offertype'];
				}else if($remainingamount < 0){
					$val['amountdeducted']=$val['amount']-($remainingamount * -1);
					$val['remain']=$remainingamount;
					$val['couponapplied']=($remainingamount <= 0) ? 'full' : 'partial';
					$val['couponid']=$this->coupon['coupon_id'];
					$val['couponcode']=$this->coupon['code'];
					$val['coupontype']=$this->coupon['type'];
					$val['coupondiscount']=$this->coupon['discount'];
					$val['couponappliedbool']=true;
					$val['coupondisctype']=$this->coupon['discount_type'];
					$val['dicountprice']=$remainingamount * - 1;
					$val['offertype']=$this->coupon['offertype'];
					$remainingamount=0;
				}
				$modproducts[]=$val;
			}
		}else{
			foreach($this->filteredproducts as $key=>$val){
				$amounttobeded=($val['dicountprice'] * $this->coupon['discount'] / 100);
				$remainingamount=$val['dicountprice']-$amounttobeded;
				$val['amountdeducted']=$amounttobeded;
				$val['remain']=$remainingamount;
				$val['coupon']=true;
				$val['couponapplied']=($remainingamount <= 0) ? 'full' : 'partial';
				$val['couponid']=$this->coupon['coupon_id'];
				$val['couponcode']=$this->coupon['code'];
				$val['coupontype']=$this->coupon['type'];
				$val['coupondiscount']=$this->coupon['discount'];
				$val['couponappliedbool']=true;
				$val['coupondisctype']=$this->coupon['discount_type'];
				$val['dicountprice']=$remainingamount;
				$val['offertype']=$this->coupon['offertype'];
				$modproducts[]=$val;
			}
		}
		$this->filteredproducts=$modproducts;
		return true;
	}

	/*public function cancelcoupon(){
		$couponlesscart=$this->ci->libsession->getSession('cartreplica');
		$rewritecart['cart']=$couponlesscart;
		$this->ci->libsession->deleteSession('coupon_code_applied');
		$this->ci->libsession->deleteSession('coupon_code_value');
		$this->ci->libsession->deleteSession('coupon_id');
		$this->ci->libsession->setSession($rewritecart);
		return true;
	}*/

	public function cancelcoupon($param){
		$couponlesscart=$this->ci->do_cart->getcurrentcart($param);
		$rewritecart['cart']=$couponlesscart;
		$this->ci->libsession->deleteSession('coupon_code_applied');
		$this->ci->libsession->deleteSession('coupon_code_value');
		$this->ci->libsession->deleteSession('coupon_id');
		$this->ci->libsession->setSession($rewritecart);
		return true;
	}
}
?>