<!--pop-up for forget password start-->
<div id="forgotpasswordpop" class="modal fade">
<div class="modal-dialog width-400">
   <div class="modal-content">
      <div class="modal-header">
          <h3 class="modal-title" style="    text-align: center;
    text-transform: capitalize;
    color: #cc0028;
    font-size: 22px;
    padding: 5px 0px;">Forgot your password ?</h3>
         <div class="modal-body" style="height: 185px;padding: 10px;">
             <button type="button" class="close" data-dismiss="modal" style="position: relative;
    top: -44px;">&times;</button>
            <div class="col-lg-12 ">
                  <p>In order to receive your access code by email, please enter the email address you provided during the registration process. 
                  <p>
                  <form name="formInformMe" id="inform">
                     <input name="emailid" id="emailid" type="text" maxlength="100" value="" size="30" placeholder="Enter your email address" class="sign_in_form" style="margin-top: 14px;
                        padding: 5px 5px 5px 5px; width: 100%; outline: none;
                        " />
                  </form>
               
            </div>
            
         </div>
         <div class="modal-footer">
              <button type="button" id="forgotpassword" class="btn btn-danger grooverbtn text-center">Send</button>
         </div>
      </div>
   </div>
</div>
<!--pop-up for forget password end-->