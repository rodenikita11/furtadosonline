<style type="text/css">
label#landline-error,label#mobile-error,label#fullname-error,label#time-error {
    color:red;
    font-weight: 400;
}
   h2.offerh2 {
   font-size: 18px;
   text-align: center;
   margin: 8px 0px 10px;
   }
   .m-b-10{
   margin-bottom: 20px;
   }
   .form-control {
    padding: 6px 7px ! important;
    background: transparent;
    border: none;
    border: 1px solid #ccc ! important;
    width: 100%;
    font-size: 12px;
}
label{
  font-size: 14px;
}
    .enquiry_form{
    width:100%; 
    top: 100%;
    left: 0px;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 12px;
    font-weight: 600;
    background-color: #f4f5f9;
   /* background-color: rgba(255, 255, 255, 0.84);*/
    border: 1px solid #ccc;
    border-radius: 4px;
    
   }
   p.enquiry_txt {
    font-size: 14px;
   }
</style>
<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
              <div class="row" style="margin-left: 6px;">
               <h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;"> Pianos Enquiry</h2>
               <p class="enquiry_txt">Thank you for your interest in Pianos.</p>
               <br>
               <p class="enquiry_txt">Furtados is India's leading Piano retailer with a range that encompasses beginner student models to the exclusive Steinway range.
                  Pianos are available in different types and sizes. The main types are Acoustic Pianos &amp; Digital Pianos. Acoustic Pianos is also available in Grand and Upright models.
               </p>
               <br>
               <p class="enquiry_txt">Please enter the details in the below form &amp; our Piano Specialist will contact you for your requirement shortly. We assure you of our best service at all time.</p>
               <br>
           

               <div class="col-lg-12 well">
                  <div class="row" style="margin-left: -33px;">
                    <div class="col-sm-6">
                     <form name="pianoenquiry" id="pianoenquiry" method="POST">
                        <div class="col-sm-12" style="background: #f9f9f9;padding: 12px;">
                           <div class="form-group">
                                 <label>First Name</label>
                                 <input type="text" name="fullname" id="fullname" placeholder="Enter First Name Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Landline Number</label>
                                 <input type="text" name="landline" id="landline" placeholder="Enter Landline Number Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Mobile Number</label>
                                 <input type="text" name="mobile" id="mobile" placeholder="Enter Mobile Number Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Email Address</label>
                                 <input type="text" name="emailid" id="emailid" placeholder="Enter Email Address Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Which is the best time we can contact you?</label>
                                 <input type="text" placeholder="Best Time.." name="time" id="time" class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Type of Piano interested in:</label>
                                 <select class="enquiry_form" id="interest" name="interest" >
                                    <option value="Not Selected">Please Select</option>
                                    <option value="Grand Piano">Grand Piano</option>
                                    <option value="Upright Piano">Upright Piano</option>
                                    <option value="Digital Piano">Digital Piano</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Budget:</label>
                                 <select class="enquiry_form" id="budget" name="budget" >
                                    <option value="Not Selected">Please Select</option>
                                    <option value="Upto 1 lakh">Upto 1 lakh</option>
                                    <option value="1 lakh - 2 lakhs">1 lakh - 2 lakhs</option>
                                    <option value="2 lakhs - 5 lakhs">2 lakhs - 5 lakhs</option>
                                    <option value="5 lakhs &amp; above">5 lakhs &amp; above</option>
                                 </select>
                              </div>
                           
                           <input type="submit" name="submit" class="btn btn-danger" id="enquiry" value="Send"> 
                           <img id='loader' src="<?php echo base_url(); ?>assets/images/preloader.gif" style='display: none;position: absolute;right: 352px;width: 21px;bottom: 19px;'>                   
                        </div>
                     </form>
                  </div>
               
                 <div class="col-sm-6">
                 <img src="<?php echo base_url(); ?>assets/web/images/piano.jpg">
                 </div>

    
               </div>
            </div>
            <!-- ========================================= SECTION – HERO : END========================--> 
         </div>
           </div>
      </div>
   </div>
</div>
<script type="text/javascript">
   jQuery('#pianoenquiry').on('click',function(){
   
   jQuery.validator.addMethod("lettersonly", function(value, element){
       return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Please enter letters only");
   jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != 'select');
    }, "Please Select the field");
      //alert('HII');
      jQuery('#pianoenquiry').validate({
         rules:{
            fullname:{
               required:true,
               lettersonly:true,
               minlength: 2
            },
            mobile:{
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            },
            emailid:{
               required:true,
               email:true
            },
            time:{
               required:true
            },
            landline:{
                number:true,
                minlength:11,
                maxlength:11
            },
            interest:{
                selectcheck:true
            },
            budget:{
                selectcheck:true
            }
         },
         messages:{
            fullname:{
               required:"This field is required",
               minlength: jQuery.validator.format("At least {0} characters required!")
            },
            mobile:{
               required:"This field is required"
                     },
            emailid:{
               required:"This field is required",
               email:"Please enter valid email id"
            },
            time:{
               required:"Please Enter Best Time to contact"
            },
            landline:{
                minlength:jQuery.validator.format("At least {0} characters required!"),
                maxlength:jQuery.validator.format("At least {0} characters required!")
            }
            
         },
          submitHandler:function(form, e){
        submitformdetails(form, e);
      }
      });
   function submitformdetails(form, e) {
      //alert('hiii');
        jQuery('#loader').show();
        jQuery('#pianoenquiry').attr('disabled',true);
        var fullname= jQuery('#fullname').val();
        var mobile= jQuery('#mobile').val();
        var email=jQuery('#emailid').val();
        var landline=jQuery('#landline').val();
        var time =jQuery('#time').val();
        var interest=jQuery('#interest option:selected').val();
        var budget=jQuery('#budget option:selected').val();
        //alert(budget);return false;
        jQuery.ajax({
               url:'<?php echo ADMINURL;?>product/pianoenquirydtl',
               type: "POST",
               data: {fullname:fullname,mobile:mobile,email:email,time:time,landline:landline,interest:interest,budget:budget},
               success: function (result) {
                jQuery('#loader').hide();
                jQuery('#pianoenquiry').attr('disabled',false);
                  res=jQuery.parseJSON(result);
                  //console.log(res); return false;
               if(res.status == 'erroroccured'){  
                  jQuery('.error').html(res.error);
               }else if(res.status == true){
                  jQuery("input[type=submit]").attr("disabled", "disabled");
                  bootbox.alert(res.msg);
                  setTimeout(function(){
                  window.location.reload(1);
                  }, 7000);
               }else{
                  bootbox.alert(res.msg);
                }
               }
           });

          e.preventDefault();
      }
   });
</script>