<style>
p {
    font-size: 14px;
    text-align: justify;
}
</style>
<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder inner-top-md-20">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
               <div class="wide-banners wow fadeInUp outer-bottom-xs">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="col-lg-5">
                           <img src="<?php echo WEBIMG;?>staticpage/comingsoon.jpg" style="width:100%">
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
