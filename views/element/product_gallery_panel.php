<?php
		/*
				AUTHOR : SANJAY
				CREATED : 27/Aug
				DESC : Product Image magnification panel.
				NOTE : Its on working mode, don't modify it.
		*/
?>
<script src="<? echo SITEJS;?>jquery.simpleGallery.js" type="text/javascript"></script>
<script src="<? echo SITEJS?>jquery.simpleLens.js" type="text/javascript"></script>
<link href="<?echo SITECSS?>jquery.simpleLens.css" rel="stylesheet"/>
<?php if(!empty($result1['image'])) { ?>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 det_img">
<div class="gallery-container" id="demo">
	<div class="plug_container">
	<div class="btnprev" style="position: absolute; top: 5px; left: 31px;">
		<i class="fa fa-sort-asc" style="color: #fc5602;cursor: pointer;font-size: 36px;"></i>
	</div>
	<div class="btnnext" style="position: absolute; top: 375px; left: 31px;">
		<i class="fa fa-sort-desc" style="color: #fc5602;cursor: pointer;font-size: 36px;"></i>
	</div>
	<div class="thumbnails-container" id="my_carousel">
		<ul style="list-style:none; padding:0px; margin:0px; ">
		<?php foreach($result1['image'] as $key=>$value){?>
	<li style="margin-top:2px; border:solid 1px #eee; text-align:center; padding-top:2px;">
	<a href="#" class="thumbnail-wrapper" style="padding-right:3px;" data-lens-image="<?php echo $value;?>" data-big-image="<?php echo $this->lib_storeuri->getprothumbnail($value,400);?>"><img src="<?php echo $this->lib_storeuri->getprothumbnail($value,66);?>"></a></li>
	<?php } ?>
		</ul>
	</div>
		<div class="big-image-container">
				<a class="lens-image" data-lens-image="<?php echo $result1[0]['prothumbnail'];?>"> <img src="<?php echo $this->lib_storeuri->getprothumbnail($result1[0]['prothumbnail'],400);?>" class="big-image"></a>
		</div>
	</div>
</div>
</div>
<?php } ?>
<script>
jQuery(document).ready(function(){
jQuery('#demo .thumbnails-container img').simpleGallery({
loading_image: "<?php echo SITEIMAGES?>icons/loader.gif",
});
 
jQuery('#demo .big-image').simpleLens({
loading_image: "<?php echo SITEIMAGES?>icons/loader.gif",
	});
});
</script>

<script type="text/javascript">
/*
		Script for carousel activity
		CREATED : 19/Sep
		AUTHOR : SANJAY
*/
jQuery(document).ready(function(){
		var step = 1; 
		var current = 0; 
		var maximum = jQuery('#my_carousel ul li').size(); 
		var visible = 4; 
		var speed = 200; 
		var liSize = 87;
		var carousel_height = 360;
		
		var ulSize = liSize * maximum;   
		var divSize = liSize * visible; 
	//-- Basic initialization.	
jQuery('#my_carousel ul').css("width", "84px").css("position", "absolute");
jQuery('#my_carousel').css("width","85px").css("height", carousel_height+"px").css("visibility", "visible").css("overflow", "hidden").css("position", "relative").css("margin-top", "30px"); 
	//-- Navigation click activity
		jQuery('.btnnext').click(function() { 
			if(current + step < 0 || current + step > maximum - visible) {return; }
			else {
				current = current + step;
				jQuery('#my_carousel ul').animate({top: -(liSize * current)}, speed, null);
			}
			return false;
		});
 
		jQuery('.btnprev').click(function() { 
			if(current - step < 0 || current - step > maximum - visible) {return; }
			else {
				current = current - step;
				jQuery('#my_carousel ul').animate({top: -(liSize * current)}, speed, null);
			}
			return false;
		});
	
});
</script>