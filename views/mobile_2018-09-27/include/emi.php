<div id="emi" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content" style="background-color: #f2f3f5;">
         
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="background: #fffefe;width:  30px;height:  30px;border-radius: 50px; margin-top: -20px;margin-right: -8px;    opacity: 10;">&times;</button>
         <div class="modal-body" style="height: auto;">
           
             <!-- ============================================== according Tab ============================================== -->
            <div class="container" style="width: 90%;margin-top: 33px;">
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="1" href="#collapse1" class="panel-title expand" style="color: #ffffff">
                              <div class="right-arrow pull-right">+</div>
                              Axis bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_axis.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_1"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="2" href="#collapse2" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              ICICI Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_icici.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_2"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="3" href="#collapse3" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              HSBC Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_hsbc.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_3"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="4" href="#collapse4" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              Kotak Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_kotak.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_4"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="5" href="#collapse5" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              Indusind Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_indulnd.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_5"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="6" href="#collapse6" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              HDFC Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="https://www.furtadosonline.com/upload/images/EMI_hdfc.jpg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_6"></div>
                        </div>
                     </div>
                  </div>
                  <div class="PT5"><img src="https://www.furtadosonline.com/upload/images/bank_strip.jpg" alt="Bank"></div>
                  <div class="PT5 g_12">
                     <ul>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">Select EMI options from 6 banks- available for online purchase.</li>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">EMI amount is calculated on total value of the order at the time of payment.</li>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">For availing the EMI payment option the Minimum Order Value is Rs.5,000/-.</li>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">A processing fee may be charged for availing EMI payment option.</li>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">In an instance of refund in an EMI transaction, interest already billed in a particular transaction will not be refundable under any circumstances.</li>
                        <li style="margin-left: 15px;list-style-position: initial;list-style-type: disc;">EMI options are only available on Credit Cards</li>
                     </ul>
                  </div>
               </div>
            </div>
            <!-- ============================================== according Tab ============================================== -->
               
            </div>
         </div>
      </div>
   </div>
<script type="text/javascript">
   function bankemi(ele){
      var bid = jQuery(ele).data('bankid');
      jQuery.ajax({  
        type: "POST",  
        url: "<?php echo SITEMOBURL?>product/productwiseemi",  
        data: 'bank='+bid+'&amount=<?php echo $res['discountPrice']; ?>',  
        success: function(data) {
            $('#emidiv_'+bid).html(data);
               
        }  
      });   
      //alert(bid); return false;
   }
</script>