<style>
p {
    font-size: 14px;
    text-align: justify;
}
</style>
<!--<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px;">
         <div class="col-lg-12 outer-top-150">
         	<h3 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Privacy Policy</h3>
            <p>The security of personal information about you is our priority. We protect this information by
				maintaining physical, electronic, and procedural safeguards that meet applicable law. We train our
				employees in the proper handling of personal information. We do not sell or rent your personal
				information to third parties for their marketing purposes without your explicit consent. Please read
				this privacy policy to learn more about the ways in which we use and protect your personal
				information. </p>
         </div>
		  
		  <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> How we use your Information</h3>
            <p>Furtados uses your personal information to provide you with services and to help us better understand your needs and interests. Specifically, we use your information to help you complete a transaction or order, to communicate with you, to provide service and support, to update you on services and benefits and to personalise promotional offers. Occasionally, we may also use your information to contact you for market research regarding Furtados products or services. Credit card information is used only for payment processing and fraud prevention. Furtados has built for itself a reputation of trust worthiness with a strong emphasis on quality and customer service, business processes, management structures, and technical systems that cross borders. Our privacy practices are designed to provide protection for your personal information, all over the world.

  		  </p>
			
           
            
         </div>
		
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> Access to and Accuracy of your information</h3>
            <p> Furtados strives to keep your personal information accurate. We have implemented technology, management processes and policies to maintain customer data accuracy. We will provide you with access to your information, including making reasonable efforts to provide you with online access and the opportunity to change your information. To protect your privacy and security, we will also take reasonable steps to verify your identity, such as a password and user ID, before granting access to your data.</p>
                      
         </div>
		 
		 
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> Links to non-Furtados websites</h3>
            <p> The Furtadosonline website may provide links to third-party websites for your convenience and information. If you access those links, you will leave the Furtados website. Furtados does not control those sites or their privacy practices, which may differ from ours. We do not endorse or make any representations about third-party websites. The personal data you choose to give to third parties is not covered by the Furtados Privacy Statement. We encourage you to review the privacy policy of any company before submitting your personal information. Some third-party companies may choose to share their personal data with Furtados; that sharing is governed by that third-party company's privacy policy.</p>
                      
         </div>
		 
		 
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> Children's Privacy</h3>
            <p>We encourage children and minors (under the age of 18 years) to use our site for information and material. However, if you are under the age of 18, you cannot purchase any product on the site. If you wish to purchase a product on the site such, only your legal guardian or parents who have registered as users of the site can make the purchase.</p>
                      
         </div>
		 
		
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">Keeping Your Information Secure</h3>
            <p> 
Furtados is committed to protecting the information provided by you. To prevent unauthorized access or disclosure, to maintain data accuracy, and to ensure the appropriate use of the information, furtadosonline.com has in place appropriate physical and managerial procedures to safeguard the information we collect.
 </p>
                      
         </div>
		 
		 
		 
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> Security</h3>
            <p> 
Furtados Secure Shopping Guarantee protects you every time you shop at Furtados so that you never have to worry about the safety of your credit card information. We use the industry standard encryption protocol known as Secure Socket Layer (SSL) to keep your order information secure. This security measure is working when you see either the symbol of an unbroken key or closed lock (depending on your browser) on the bottom of your browser window. Credit card numbers are used only for processing payment and are not used for other purposes. We guarantee that every transaction you make at furtadosonline.com will be safe and secure.
 
 </p>
                      
         </div>
		 
		   
		   <div class="col-lg-12">
		     <h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;"> Modifications to Privacy Statement</h3>
            <p> 
The privacy practices of this statement apply to our services available under the domain of <b><a href="https://www.furtadosonline.com/"> www.furtadosonline.com.</a></b> By visiting our website you agree to be bound by the terms and conditions of this Privacy Policy. If you do not agree, please do not use or register with our site. Furtadosonline may modify this privacy statement at any juncture. If a material change is made to this Privacy Policy and/or the way we use our users' personally identifiable information, then we will post prominent notice of the nature of such change on the first page of this Privacy Policy. You are responsible for reviewing this Privacy Policy periodically to ensure that you are aware of any changes to it. Your continued use of the site indicates your assent to any changes to this Privacy Policy.
 </p>
                      
         </div>
		 
      </div>
   </div>
</div>-->

<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px;">
         <div class="col-lg-12 outer-top-150">
         	<h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Privacy Policy</h2>
			<p>
				This <strong>Privacy Policy </strong>sets out how    <strong> Furtados Music India Private Limited</strong> uses and protects
				any information that you give the company when you use the website
				<a href="https://www.furtadosonline.com/">
					<strong>https://www.furtadosonline.com/</strong>
				</a>
				<strong>.</strong>
				The Company is committed to protecting your privacy. This Privacy Policy
				explains how the Company collects, uses and shares your personal
				information. This Privacy Policy applies to your use of Furtados as further
				described in the Company’s Terms and Conditions available at  <a href="https://www.furtadosonline.com/termsandcondition"> <strong>https://www.furtadosonline.com/termsandcondition</strong>.</a>
			</p>
			</br>
			<p>
				By using the Website, you agree to be bound by the terms and conditions of
				this Privacy Policy. You hereby expressly consent to the processing of your
				personal information in accordance with this Privacy Policy. Your personal
				information may be stored, processed or transmitted in India or any other
				part of the world, where laws regarding storing, processing or transmitting
				of personal information may be less restrictive than the laws in your
				country. If you do not agree to the terms of this Privacy Policy, please do
				not use or access the Website.
			</p>
			</br>
			<p>
				By mere use of or access to Website, you expressly consent to the Company’s
				use and disclosure of your personal information in accordance with this
				Privacy Policy. This Privacy Policy is incorporated into and subject to the
				Terms and Conditions.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				I) TYPES OF INFORMATION THE COMPANY COLLECTS
			</h3>
			<p>
				The Company collects personal information from you in a variety of ways
				when you use Website, such as: <em></em>
			</p>
			</br>
			<p>
				<strong><em>i) </em></strong>
				<em><strong>Account</strong></em>
				<strong>. </strong>
				The Company collects, and associates with your account information, your
				name, your birth date, email address, phone number, profile details,
				gender, address and other personally identifiable information and in case
				of entities, entity name, business details, address, email address, phone
				number etc. If you create an account by logging in with your credentials
				from your third party account with certain social networking sites (such as
				Twitter, LinkedIn, Google<sup>+</sup> or Facebook), the Company may receive
				information about you from such social networking sites, in accordance with
				the terms of use and privacy policy of that social networking site. The
				Company works with such social networking site’s application protocol
				interface (API) in a way that allows you to authorize the Company to access
				your account on that social networking site on your behalf. If you consent
				to the Company to share your information with these social networking sites
				(e.g., post public content on your wall/profile page), the Company will
				share information with them in accordance with your privacy choices. Once
				the Company shares your information with these social networking sites, the
				terms and policies of these sites, and not this Privacy Policy, will apply
				to their use of your information.<em></em>
			</p>
			</br>
			<p>
				<a name="_Hlk81219609">
					<em><strong>ii) </strong></em>
					<em><strong>Payment.</strong></em>
				</a>
				<em>
					When you make a payment on the Website through third party payment
					gateways, the Company may collect basic information about the payment
					such as payment preferences, payment instrument /mode details,
					transaction amount and other details. Such basic details are only
					stored until the payment is successfully completed and for records of
					the Company as per the applicable law. For more information how these
					third party payment providers use your information, please read their
					respective privacy policies.
				</em>
			</p>
			</br>
			<p>
				<strong>iii) </strong>
				<em><strong>Website Related Use</strong></em>
				<strong>. </strong>
				When you use the Website, the Company stores, processes and transmits your
				files (like images, videos, emails, feedback, postings, comments, log file
				information from browser, software etc., metadata and other data) and
				information related to them. If you give the Company access to your
				contacts, the Company will store those contacts on its servers for you to
				use. This will make it easy for you to do things like share your data, send
				emails, and invite others to use the Website.
			</p>
			</br>
			<p>
				<em><strong>iv) </strong></em>
				<em><strong>Usage</strong></em>
				. The Company collects information about your business area, expertise and
				preferences as per the data input by you. Your devices (depending on their
				settings) may also transmit location information to the Company.<em></em>
			</p>
			</br>
			<p>
				<strong>v) </strong>
				<em><strong>Cookies and other Technologies</strong></em>
				<strong>. </strong>
				The Company uses technologies like cookies and pixel tags to provide,
				improve, protect and promote Furtados. For example, cookies help the
				Company with things like remembering your username for your next visit,
				understanding how you are interacting with Furtados, and improving it based
				on that information. The Company uses software tools to collect information
				about how you interact with Furtados, such as mouse clicks, touch gestures,
				drags, hover-overs, response times, errors, page views and length of visits
				to certain pages. You can set your browser to not accept cookies, but this
				may limit your ability to use Furtados. The Company may ask its other
				partners to serve ads or services to your computer systems or other
				devices, which may use cookies or similar technologies placed by the
				Company or the third party. The Company does not control use of such
				technologies used by advertisers and partners and it expressly disclaims
				responsibility for information collected through them.
			</p>
			</br>
			<p>
				<strong>vi) </strong>
				<strong><em>Analytic Services.</em></strong>
				The Company may use third party analytics services such as Google
				Analytics, KISSmetrics and Comscore, to collect information about how you
				use and interact with Furtados. Such third party analytics services may use
				cookies to gather information such as the pages you visited, your IP
				address, a date/time stamp for your visit and which website or hyperlink
				referred you to Furtados. The Company uses these analytics services to
				analyze how people use Furtados, to improve them, to customize the
				content/features users see/use based upon their interests and to send email
				to users who are not actively using Furtados. For more information about
				how these entities use this information, please refer to their privacy
				policies made available on their respective websites.
			</p>
			</br>
			<p>
				<strong>vii) </strong>
				<strong><em>Information Collected from Third Parties</em></strong>
				<strong>. </strong>
				The Company may receive information about you from third parties that offer
				hyperlinks to the Website on their website and/or products and/or services.
				These third parties may supply the Company with your information. The
				Company may add this information to the information it has already
				collected from you via Furtados in order to improve the products and/or
				services the Company provides and/or its website.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				II) HOW THE COMPANY USES YOUR INFORMATION
			</h3>
			<p>
				<em><strong>i) </strong></em>
				<em><strong>General Use. </strong></em>
				<em>
					In general, the information you submit to the Company is used either to
					respond to requests that you make, or to aid the Company in serving you
					better. The Company may use your information in the following ways:
				</em>
				<em></em>
			</p>
			</br>
			<p>
				<em>(a) </em>
				<em>Purchase and delivery of products and services;</em>
				<em></em>
			</p>
			<p>
				<em>(b) </em>
				<em>
					to send you a welcome e-mail and to verify ownership of the e-mail
					address provided when your user account was created;
				</em>
				<em></em>
			</p>
			<p>
				<em>(c) </em>
				<em>to identify you as a user in the Company’s system;</em>
				<em></em>
			</p>
			<p>
				<em>(d) </em>
				<em>to provide access to Furtados;</em>
				<em></em>
			</p>
			<p>
				<em>(e) </em>
				<em>to facilitate the creation of and secure your user account;</em>
				<em></em>
			</p>
			<p>
				<em>(f) </em>
				<em>to provide improved administration of </em>
				Furtados<em>;</em><em></em>
			</p>
			<p>
				<em>(g) </em>
				<em>to notify you about updates to </em>
				Furtados<em>;</em><em></em>
			</p>
			<p>
				<em>(h) </em>
				<em>
					to improve and customize the quality of experience when you interact
					with
				</em>
				Furtados<em>;</em><em></em>
			</p>
			<p>
				<em>(i) </em>
				<em>
					to send you administrative e-mail notifications, such as security or
					support and maintenance advice;
				</em>
				<em></em>
			</p>
			<p>
				<em>(j) </em>
				<em>to engage with or contact inactive users of </em>
				Furtados<em>;</em><em></em>
			</p>
			<p>
				<em>(k) </em>
				<em>
					to direct certain content and advertisements to you so that you are
					more likely to see content and advertisements that are relevant to you;
				</em>
				<em></em>
			</p>
			<p>
				(l) to analyze the data submitted by you;
			</p>
			<p>
				<em>(m) </em>
				<em>to send offers and promotional materials related to </em>
			Furtados<em>and those of third parties for marketing and other purposes.</em>    <em></em>
			</p>
			</br>
			<p>
				<strong><em>ii) </em></strong>
				The Company may also use and share aggregated or de-identified information
				for any purpose and in any manner. This aggregated or de-identified data
				that the Company shares may include non-personally identifiable data that
				the Company creates using your personal information by excluding
				information that makes the data personally identifiable.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				III) HOW THE COMPANY SHARES YOUR INFORMATION
			</h3>
			<p>
				The Company may share information as per the terms of this Privacy Policy;
				however, the Company shall endeavor that such use of information does not
				create hindrance on your use of Furtados. The Company does not share your
				personal information with third parties other than as follows:<em></em>
			</p>
			</br>
			<p>
				<strong><em>i) </em></strong>
				<em><strong>Others working for the Company</strong></em>
				<strong>.</strong>
				The Company uses certain trusted third parties to help the Company provide,
				improve, protect, and promote Furtados (such as third-party analytics tools
				to help the Company measure traffic and usage trends for Furtados). These
				third parties will access your information only to perform tasks on the
				Company’s behalf and in compliance with this Privacy Policy. Company also
				shares the information with its vendors and third party service provider
				for providing various services including but not limited to delivering the
				products and in making payment for the purchases. These third parties will
				access your information to perform tasks on the Company’s behalf and in
				compliance with this Privacy Policy.<em></em>
			</p>
			</br>
			<p>
				<em><strong>ii) </strong></em>
				<em><strong>Social Media.</strong></em>
				<em>
					You may access third-party social media platforms through your account
					and/or allow Company to share your data with third-party social media
					platforms (including on the accounts/pages/timelines etc. on such
					third-party social media platforms), for example, by linking your
					account with a third-party social media platform or by sending/sharing
					status updates or posts to/on your third-party social media platform
					account. Any data shared with third-party social media platforms will
					be governed by such third-party social media platforms’ privacy policy.
				</em>
			</p>
			</br>
			<p>
				<em><strong>iii) </strong></em>
				<em><strong>Third Parties.</strong></em>
				<em>
					The Company may also share certain information such as cookie data with
					third-party advertising partners. This information would allow
					third-party ad networks to, among other things, deliver targeted
					advertisements that they believe will be of most interest to you.
				</em>
				<em></em>
			</p>
			</br>
			<p>
				<em><strong>iv) </strong></em>
				<em><strong>Generally with your Consent.</strong></em>
				<em>
					Any information or content that you voluntarily disclose for posting on
				</em>
				Furtados
				<em>
					may become available to the public, as controlled by any applicable
					privacy settings that you set. To change your privacy settings on
				</em>
				Furtados
				<em>
					, you may change your profile setting. Once you have shared the
					content/data or made it public, that content/data may be re-shared by
					others.
				</em>
				<em></em>
			</p>
			</br>
			<p>
				<em><strong>v) </strong></em>
				<em><strong>Enforcing Rights.</strong></em>
				<em>
					The Company may also disclose personal information to enforce its
					policies, respond to claims that a posting or other content violates
					others’ rights, or protects anyone’s rights, property or safety.
				</em>
				<em></em>
			</p>
			</br>
			<p>
				<strong>vi) </strong>
				<em><strong>Law &amp; Order</strong></em>
				<strong>.</strong>
				The Company may disclose your information to third parties if it determines
				that such disclosure is reasonably necessary to: (a) comply with the laws;
				(b) protect any person from death or serious bodily injury; (c) prevent
				fraud or abuse of its name, brands, trademarks or such other rights
				belonging to it or its other users; or (d) protect its rights.
			</p>
			</br>
			<p>
				Stewardship of your data is critical to the Company and a responsibility
				that the Company embraces. The Company believes that its users’ data should
				receive the same legal protections regardless of whether it’s stored on its
				servers or on your home computer systems or devices. The Company will abide
				by the following principles when receiving, scrutinizing and responding to
				government requests for its users’ data:
			</p>
			</br>
			<p>
				(a) Be transparent,
			</p>
			<p>
				(b) Fight blanket requests,
			</p>
			<p>
				(c) Protect all users, and
			</p>
			<p>
				(d) Provide trusted services.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				IV) STORING OF INFORMATION <em></em>
			</h3>
			<p>
				<strong><em>i) </em></strong>
				<em><strong>Retention</strong></em>
				<strong>.</strong>
				<a name="_Hlk81221229">
					The Company will retain your information for as long as it needs it and
					for the purpose which it was collected.
				</a>
			</p>
			</br>
			<p>
				<strong><em>ii) </em></strong>
				<strong><em>No warranty. </em></strong>
				The Company uses commercially reasonable safeguards to help keep the
				information collected through Furtados secure and take reasonable steps
				(such as requesting a unique password) to verify your identity before
				granting you access to your account. However, no method of transmission
				over the internet, or method of electronic storage, is 100% secure and so
				the Company cannot fully ensure or guarantee the security of any
				information you transmit to the Company or guarantee that information on
				Furtados may not be accessed, disclosed, altered, or destroyed by any
				person.
			</p>
			</br>
			<p>
				<strong><em>iii) </em></strong>
				<em><strong>Around the world</strong></em>
				<strong>.</strong>
				The Company may store, process and transmit information in locations around
				the world - including those outside your country. Information may also be
				stored locally on the devices you use to access Furtados. By registering
				for and using Furtados, you consent to the transfer of information to any
				part of the world in which the Company, its affiliates or service providers
				maintain facilities and the use and disclosure of information about you
				thereto as described in this Privacy Policy.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				V) CHANGES TO YOUR INFORMATION AND DELETION
			</h3>
			<p>
				You may change certain of your personal information by editing your user
				account or by sending the Company an e-mail at response@furtados.in or
				postal mail addressed to the Company. You are responsible for maintaining
				the secrecy of your unique password and account information, and for
				controlling access to emails between you and the Company, at all times. The
				Company is not responsible for any third party functionality, privacy or
				security policies which you are bound by. Please do your part to help the
				Company. When you don’t wish to continue availing services the Company
				provides, you may request to delete the information the Company has about
				you by writing to the Company on the email address mentioned above. The
				Company will delete your information stored on the Company’s servers save
				and except retentions in compliances with the applicable laws and for the
				purpose of audits. Company maintains the confidentiality till the
				information is retained. Company is not responsible for the information
				retained by any third party.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				VI) CHILDREN INFORMATION
			</h3>
			<p>
				Use of the Website is available only to a duly constituted legal entity or
				an adult individual who is competent to contract under the prevailing laws
				of India. Persons who are deemed "incompetent to contract" within the
				meaning of the Indian Contract Act, 1872 are not eligible to use the
				Website. If your age is below 18 years old, your parents or legal guardians
				can transact on your behalf, if they are registered users.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				VII) THIRD PARTY WEBSITES
			</h3>
			<p>
				Links on the Company to third-party websites are provided solely as a
				convenience to you. If you use these links, you will leave the Website. The
				Company is not obligated to review such third-party websites, does not
				control such third-party websites, and is not responsible for any such
				third-party websites (or the products, services, or content available
				through the same). Thus, the Company does not endorse or make any
				representations about such third-party websites, any information, software,
				products, services, or materials found there or any results that may be
				obtained from using them. If you decide to access any of the third-party
				websites linked to from the Website, you do this entirely at your own risk.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				VIII) CHANGES
			</h3>
			<p>
				The Company may share some or all of your information in connection with or
				during negotiation of any merger, financing, acquisition or dissolution,
				collaboration, transaction or proceeding involving sale, transfer,
				divestiture, or disclosure of all or a portion of its business or assets or
				in other business arrangements the company may enter into. In the event of
				an insolvency, bankruptcy, or receivership, your information may also be
				transferred as a business asset. If another entity acquires the Company or
				its business or assets, that entity will possess all your information
				collected by the Company and will assume the rights and obligations
				regarding your information as described in this Privacy Policy.
			</p>
			</br>
			<p>
				The Company may change this Privacy Policy from time to time. If the
				Company makes any changes to this Privacy Policy, the last modified Privacy
				Policy shall be applicable to you.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				IX) COMMUNICATIONS
			</h3>
			<p>
				The Company may periodically send you free newsletters and promotional
				communications. When you receive newsletters or promotional communications
				from the Company, you may “opt-out” of these emails by following the
				unsubscribe instructions provided in the email you receive or by contacting
				the Company at response@furtados.in Despite your indicated email
				preferences, the Company may send you notices of any updates to its Privacy
				Policy, Terms and Conditions or other administrative emails.
			</p>
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				X) GRIEVANCE OFFICER
			</h3>
			<p>
				Any question regarding this Privacy Policy or information collection, use
				and disclosure practices or if you would like to: access, correct, amend or
				delete any personal information the Company have about you, you may contact
				our grievance officer at glend@furtados.in the Company will use reasonable
				efforts to respond promptly to any requests, questions or concerns, which
				you may have regarding our use of your personal information.
			</p>
			</br>
			<p>
				The Company welcomes your comments or questions regarding this Privacy
				Policy. Please e-mail the Company or contact the Company at the following
				address:
			</p>
			</br>
			<p>
				Attn: Glen D’Souza
			</p>
			<p>
				Email: <a href="mailto:glend@furtados.in">glend@furtados.in</a>
			</p>
			<p>Address: Furtados Music India Pvt Ltd</p>
			<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 7/8 Dwarka Ashish, Jambulwadi</p>
			<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Kalbadevi Road, Mumbai 400 002</p>
				
			</br>
			<h3 style="color:#cc0028;text-transform: capitalize;font-size: 24px;">
				XI) GOVERNING LAW AND JURISDICTION
			</h3>
			<p>
				This Privacy Policy shall be governed by Indian Laws and Courts in Mumbai
				shall have exclusive jurisdiction to resolve a dispute.
			</p>
		 </div>
		 
      </div>
   </div>
</div>