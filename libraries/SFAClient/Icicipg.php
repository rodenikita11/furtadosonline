<?php
	class Icicipg
	{
		/*
				NAME : testpay
		*/
		public function testpay($paymentParams)
		{
			if(empty($paymentParams)) return false;
			#print_r($paymentParams); exit;
				include("Sfa/BillToAddress.php");
				include("Sfa/CardInfo.php");
				include("Sfa/Merchant.php");
				include("Sfa/MPIData.php");
				include("Sfa/ShipToAddress.php");
				include("Sfa/PGResponse.php");
				include("Sfa/PostLibPHP.php");
				include("Sfa/PGReserveData.php");
			
			$oMPI = new MPIData();
			$oCI =	new	CardInfo();
			$oPostLibphp = new PostLibPHP();
			$oMerchant		=	new	Merchant();
			$oBTA			=	new	BillToAddress();
			$oSTA			=	new	ShipToAddress();
			$oPGResp		=	new	PGResponse();
			$oPGReserveData =	new PGReserveData();

			$oMerchant->setMerchantDetails("96030119","00002116","00002116",$paymentParams['orderip'],'TXN'.strtoupper(substr(sha1(rand()), 0, 4)),'ID'.$paymentParams['orderid'],SITEURL."initorder/testPayReply","POST","INR",'INV'.$paymentParams['orderinvoice_no'],"req.Sale",$paymentParams['final_amount_to_pay'],"",date('Y-m-d h:m:s'),"true",time(),$paymentParams['offer_type'],'SHIP'.$paymentParams['shipping_master_id']);

			
			$oBTA->setAddressDetails("CID","Tester",$paymentParams['contact_person'],$paymentParams['address_1'],$paymentParams['landmark'],$paymentParams['city'],$paymentParams['state'],$paymentParams['postcode'],"IND","tester@soft.com");

			$oSTA->setAddressDetails($paymentParams['contact_person'],$paymentParams['address_1'],$paymentParams['landmark'],$paymentParams['city'],$paymentParams['state'],$paymentParams['postcode'],"IND","sad@df.com");

			$oPGResp = $oPostLibphp->postSSL($oBTA,$oSTA,$oMerchant,$oMPI,$oPGReserveData);
					if($oPGResp->getRespCode()=='000'){
								$url=$oPGResp->getRedirectionUrl();
								$resp['status']=1;
								$resp['url']=$url; #-- Get redirect url
						}else{
									$resp['status']=0;
									$resp['msg'] = "Error Code:".$oPGResp->getRespCode()."<br>Error Message:".$oPGResp->getRespMessage()."<br>";
									/*
									print "Error Occured.<br>";
									print "Error Code:".$oPGResp->getRespCode()."<br>";
									print "Error Message:".$oPGResp->getRespMessage()."<br>";
									*/
							}
			return $resp;
		}
		
		/*
			NAME : testpayresponse
		*/
		public function testpayresponse()
		{
		include("Sfa/EncryptionUtil.php");
		 $strMerchantId="96030119";
		 $astrFileName= PG_KEY;
		 #$astrFileName= $_SERVER['SCRIPT_FILENAME'];
		 $astrClearData;
		 $ResponseCode = "";
		 $Message = "";
		 $TxnID = "";
		 $ePGTxnID = "";
	     $AuthIdCode = "";
		 $RRN = "";
		 $CVRespCode = "";
		 $Reserve1 = "";
		 $Reserve2 = "";
		 $Reserve3 = "";
		 $Reserve4 = "";
		 $Reserve5 = "";
		 $Reserve6 = "";
		 $Reserve7 = "";
		 $Reserve8 = "";
		 $Reserve9 = "";
		 $Reserve10 = "";

		 if($_POST){
			if($_POST['DATA']==null){
				$resp['status']=0;
				$resp['pg_response'] = "null is the value";
			}
			 $astrResponseData=$_POST['DATA'];
			 $astrDigest = $_POST['EncryptedData'];
			 $oEncryptionUtilenc = 	new 	EncryptionUtil();
			 $astrsfaDigest  = $oEncryptionUtilenc->getHMAC($astrResponseData,$astrFileName,$strMerchantId);

			if (strcasecmp($astrDigest, $astrsfaDigest) == 0) {
			 parse_str($astrResponseData, $output);
			 if( array_key_exists('RespCode', $output) == 1) {
			 	$ResponseCode = $output['RespCode'];
			 }
			 if( array_key_exists('Message', $output) == 1) {
			 	$Message = $output['Message'];
			 }
			 if( array_key_exists('TxnID', $output) == 1) {
			 	$TxnID=$output['TxnID'];
			 }
			 if( array_key_exists('ePGTxnID', $output) == 1) {
			 	$ePGTxnID=$output['ePGTxnID'];
			 }
			 if( array_key_exists('AuthIdCode', $output) == 1) {
			 	$AuthIdCode=$output['AuthIdCode'];
			 }
			 if( array_key_exists('RRN', $output) == 1) {
			 	$RRN = $output['RRN'];
			 }
			 if( array_key_exists('CVRespCode', $output) == 1) {
			 	$CVRespCode=$output['CVRespCode'];
			 }
					if($_POST['DATA']!=null){
				$resp['status']=1;
				$resp['pg_response'] = json_encode($output);
				$resp['Message'] = $Message;
				$resp['ResponseCode'] = $ResponseCode;
				$resp['AuthIdCode'] = $AuthIdCode;
				$resp['RRN'] = $RRN;
				$resp['TxnID'] = $TxnID;
				$resp['ePGTxnID'] = $ePGTxnID;
				$resp['CVRespCode'] = $CVRespCode;
					}
			}
		 }
		 else{
				$resp['status']=0;
				$resp['pg_response'] = 'No response from payment gateway';
		 }
				/*
				print "<h6>Response Code:: $ResponseCode <br>";
				print "<h6>Response Message:: $Message <br>";
				print "<h6>Auth ID Code:: $AuthIdCode <br>";
				print "<h6>RRN:: $RRN<br>";
				print "<h6>Transaction id:: $TxnID<br>";
				print "<h6>Epg Transaction ID:: $ePGTxnID<br>";
				print "<h6>CV Response Code:: $CVRespCode<br>";
				*/
			return $resp;
		}
		
		/*
					//---------- GIFT CARD PAYMENT
		*/
		/*
				NAME : giftcardpay
		*/
		public function giftcardpay($paymentParams)
		{
			if(empty($paymentParams)) return false;
			#print_r($paymentParams); exit;
				include("Sfa/BillToAddress.php");
				include("Sfa/CardInfo.php");
				include("Sfa/Merchant.php");
				include("Sfa/MPIData.php");
				include("Sfa/ShipToAddress.php");
				include("Sfa/PGResponse.php");
				include("Sfa/PostLibPHP.php");
				include("Sfa/PGReserveData.php");
			
			$oMPI = new MPIData();
			$oCI =	new	CardInfo();
			$oPostLibphp = new PostLibPHP();
			$oMerchant		=	new	Merchant();
			$oBTA			=	new	BillToAddress();
			$oSTA			=	new	ShipToAddress();
			$oPGResp		=	new	PGResponse();
			$oPGReserveData =	new PGReserveData();

			$oMerchant->setMerchantDetails("96030119","00002116","00002116",$paymentParams['orderip'],'TXN'.strtoupper(substr(sha1(rand()), 0, 4)),'ID'.$paymentParams['orderid'],SITEURL."offer/secureGiftCard","POST","INR",'INV'.$paymentParams['orderinvoice_no'],"req.Sale",$paymentParams['final_amount_to_pay'],"",date('Y-m-d h:m:s'),"true",time(),$paymentParams['offer_type'],'SHIP'.$paymentParams['shipping_master_id']);

			
			$oBTA->setAddressDetails("CID","Tester",$paymentParams['contact_person'],$paymentParams['address_1'],$paymentParams['landmark'],$paymentParams['city'],$paymentParams['state'],$paymentParams['postcode'],"IND","tester@soft.com");

			$oSTA->setAddressDetails($paymentParams['contact_person'],$paymentParams['address_1'],$paymentParams['landmark'],$paymentParams['city'],$paymentParams['state'],$paymentParams['postcode'],"IND","sad@df.com");

			$oPGResp = $oPostLibphp->postSSL($oBTA,$oSTA,$oMerchant,$oMPI,$oPGReserveData);
					if($oPGResp->getRespCode()=='000'){
								$url=$oPGResp->getRedirectionUrl();
								$resp['status']=1;
								$resp['url']=$url; #-- Get redirect url
						}else{
									$resp['status']=0;
									$resp['msg'] = "Error Code:".$oPGResp->getRespCode()."<br>Error Message:".$oPGResp->getRespMessage()."<br>";
									/*
									print "Error Occured.<br>";
									print "Error Code:".$oPGResp->getRespCode()."<br>";
									print "Error Message:".$oPGResp->getRespMessage()."<br>";
									*/
							}
			return $resp;
		}
	}