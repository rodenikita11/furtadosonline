<section>
            <div class="second-page-container" style="padding-top:130px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
						
                            <?php $this->load->view('element/breadcrum');?>
                            <div class="block-product-detail">
                                <div class="row">
                                    <?php 
										$data['result1'] = $result1;
										$this->load->view('element/product_gallery_panel.php',$data);
									?>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 san">
									<input type="hidden" id="current_pro_id" value="<?php echo $result1[0]['prosellerid'];?>" readonly="readonly">
									<?php if(empty($result1[0]['promodiscount'])) { ?>
											<input type="hidden" value="<?php echo (($result1['finalprice']!=$result1[0]['price']?$result1['finalprice']:$result1[0]['price']));?>" id="current_pro_price" readonly="readonly">
									<?php } else { ?>
											<input type="hidden" value="0" id="current_pro_price" readonly="readonly">
									<?php } ?>
                                        <div class="product-detail-section">
                                            <h3 id='promainid' data-seller='<?php echo $param['sellerid'];?>' style="padding-left:40px;">
												<? echo $result1[0]['proname']?>
											</h3>
                                            <?php 
												$param['pid']=$result1[0]['proid'];
												$proReview = $this->Do_review->getreviewcount($param);
												$limit=5;
												$fraction=$limit-$proReview[0]['average'];
											?>
											<div class="product-rating" style="padding-left:40px;">
                                                 <div class="stars">
                                                    <?php for($i=1;$i<=5;$i++){ ?>
															<?php if($i<=$proReview[0]['average']) { ?>
															<span class="st-active"><i class="fa fa-star st-active"></i></span>
															<?php } else {?>
															<span class="st-active"><i class="fa fa-star-o st-active"></i></span>
													<?php } } ?>
                                                </div>
                                                <a href="javascript:void(0);" class="review" style="padding-left:32px;"> <?php echo round($proReview[0]['average']);?> Review(s)</a> 
                                            </div>
                                            <div class="product-information" style="padding-left: 40px;">
                                                <div class="clearfix">
                                                    <label class="pull-left">Product Code:</label><?echo $result1[0]['prosku']?>
                                                </div>
									<?php #-- Update by sanjay, 10/sep
										
										$attrParam['pro_seller_id'] = $result1[0]['prosellerid'];
										$getProductSizeAttr = $this->Do_product->getProductSizeAttrById($attrParam);
										
										if(!empty($getProductSizeAttr)) { 
									?>
													<div class="clearfix">
														<label class="pull-left">Select Size :</label>
															<select name="size" onchange="select_size(this.value);" class="form-control">
																<option value="" selected="selected">--select--</option>
															   <?php foreach($getProductSizeAttr as $optKey=>$optVal) {  ?>
															   <option value="<?php echo $optVal['attr_id'];?>"><?php echo $optVal['attribute_value'];?></option>
															   <?php } ?>
															</select>
															<input type="hidden" id="selected_option" value="" readonly="readonly"/>
													</div>
												<script type="text/javascript">
													function select_size(option_val)
													{
														console.log(option_val);
															if(option_val!='')
															{
																jQuery("#selected_option").val(option_val);
															}
															else
															{
																jQuery("#selected_option").val('');
															}
													}
												</script>
											<?php } ?>
                                                <div class="clearfix">
                                                    <label class="pull-left">Availability:</label>
                                                    <p><?php echo (($result1[0]['quantity']>0?"Available":"<span style='color:red;'>Out of stock</span>"))?></p>
                                                </div>
                                                <div class="clearfix">
                                                    <label class="pull-left">Description:</label>
                                                    <div class="description" title="" style="padding-left:80px;"><ul style='list-style-type: none;'><li><i class="fa fa-check color1"></i> <?php 
															$srt =  CommonHelper::return_part_of_string(strip_tags($result1[0]['proshortspecification']),200); 
															//print_r($srt); die;
															if($srt){
																
																echo  str_replace('~|',' </li><li><i class="fa fa-check color1"></i> ',trim($srt));
															}

														?></li></ul></div>
                                                </div>
                                                <div class="clearfix">
                                                    <label class="pull-left">Best Price:</label>
													<?php if(!empty($result1['finalprice'])) { ?>
															<?php if($result1['marketprice'] > $result1['finalprice']) { ?>
																<p class="product-price"><span class="fS13 text-hides"><i class="fa fa-inr color2 fS13"></i><?php echo $result1[0]['marketprice']?></span>&nbsp;&nbsp;<i class="fa fa-inr fS18 color1"></i><span class="fS24 color1"> <?php echo $result1['finalprice']?></span></p>
															<?php } else { ?>
																<p class="product-price">
																<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1"><?php echo $result1['finalprice']?></span>
																</p>
															<?php } ?>
													<?php $totalprice = $result1['finalprice']; ?>
													<? } else { ?>
													<p class="product-price">
																<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1">Unable to retrieve price</span>
													</p>
													<?}?>
													
													<?php if($result1['offerprice'] > 0 && empty($result1[0]['promodiscount'])) { ?>
													 <div class="clearfix">
														<label class="pull-left">Offer Price:</label>
															<p class="product-price">
																<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1"><?php echo $result1['offerprice']?></span>
															</p>
													</div>
													<?php $totalprice = $result1['offerprice']; 
													}
													?>
													
													<?php if($result1[0]['promodiscount']) { ?>
														<div class="clearfix">
															<label class="pull-left">Lucky Price:</label>
																<p class="product-price">
																	<? if(($result1[0]['promoprice']>0)){ ?>
																		<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1">
																			<? echo $result1[0]['promoprice']; ?>
																		</span>	
																	<? }else{ ?>
																		<span class="fS18 color1">Free</span>
																	<?}?>
																	
																</p>
														</div>
														<div style="color:green;font-style: italic;margin-bottom: 10px;">Congratulations!!! Its "<?=$result1[0]['promotext']?>" and you are getting additional <?= $result1[0]['promodiscount']?>% discount.</div>
													<?php 	$totalprice = $result1[0]['promoprice']; 
													} 
													?>
                                                </div>
												
												 <div class="clearfix">
                                                    <label class="pull-left">Points:</label>
                                                   <?php if(!empty($result1[0]['pro_points'])) { ?>
												    <p class="product-price">
														<span id="f_amnt"><?php echo $result1[0]['pro_points']?> Redeemable Points</span>
													</p>
													<?php } else { ?>
													 <p class="product-price"><i class="fa fa-inr"></i>
														<span id="f_amnt">No Redeemable Points</span>
													</p>
													<?php } ?>
                                                </div>
												
                                                <div class="shopping-cart-buttons">
                                                   <?php if($result1[0]['quantity']>0) { ?>
												   <a href="javascript:void(0);" onclick="addProductToCart();" class="shoping"><i class="fa fa-shopping-cart"></i>  Add to Cart</a>
												   <?php } ?>
                                                    <a href="javascript:void(0);" class="addtowishlist" data-id="<?php echo $result1[0]['prosellerid'];?>" title="Add to Wishlist"><i class="fa fa-heart"></i></a>
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<? echo current_url();?>" title="Share on Facebook" target = '_blank' ><i class="fa fa-facebook fS18"></i></a>
													 <a href="#" title="Share on Twitter"><i class="fa fa-twitter fS18"></i></a>
                                                </div>
			<?php if($result1[0]['quantity']>0) { #-- If not out of stock.. ?>
			<?php if($result1[0]['giftwrapping']=='yes' && empty($magic_click_product)) { #-- by sanjay, 8/Dec ?>
					<div class="mrt10 color2">
					<input type="checkbox" class="is_giftwraped" name="is_giftwraped" value="Y" id="is_giftwraped_<?php echo $result1[0]['prosellerid'];?>"/><span>&nbsp;&nbsp;Gift Wrap available for 
							<?php if($result1[0]['wrappingprice']>0) { ?>
					<i class="fa fa-inr ruppe1 color2" style="font-size:13px;"></i><?php echo $result1[0]['wrappingprice'];?>/quantity
							<?php } else { ?>
								free.
			<?php } ?>
					</span>
					</div>
					<?php } ?>
					<?php if($result1[0]['is_cod']=='1' && empty($magic_click_product)) { ?>
					

					<script type="text/javascript">

					</script>
					<?php } ?>
					
				<?php } #-- If not out of stock..?>
										<?php if(!empty($availableProducts)) { ?>
										<div class="dt-available">
											<h3 class="color1">ALSO AVAILABLE</h3>
											<ul>
											<?php foreach($availableProducts as $availableKey => $availableVal){ ?>
												<a href="<?php echo $availableVal['proURL'];?>"><li><img src="<?php echo $this->lib_storeuri->getprothumbnail($availableVal['prothumbnail'],35);?>"></li></a>
											<?php } ?>
											</ul>
										</div>
										<?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
						<div class="detail-view-brand" style="margin-top:-20px;">
							<!--brand-->
							<div class="heading">BRAND</div>
							<div class="brand-det">
								<div class="brand-img">
									<img src="<?php echo $this->lib_storeuri->getprothumbnail(FILEBASEURL.IMGBRANDPATH.$result1[0]['brandthumbnail'],65);?>">
								</div>
								<div class="brand-dt-name"><?php echo $result1[0]['brandname'];?></div> 
							</div>
							<!--end barnd-->
							<!--seller-->
							<div class="seller-heading">SELLER NAME</div>
							<div class="seller-name"><span class="fS16 color1"><?php echo $result1[0]['name'];?></span><br>
							<span class="shipping">Shipping in <?php echo $result1[0]['deliverytime'];?> Business Day</span>
							<span class="color1 fS13">Check for COD & Availability</span><br>
							<span><input data-id="<?php echo $result1[0]['prosellerid'];?>" id="buyers_pincode" type="number" minlength=6 maxlength=6   value="<?php echo $result1['pincode'];?>" placeholder="pincode"/>
								<button  id="get_availibility_trigger" >Verify</button>
							</span>
							
							<div style="padding-top: 10px; width: 160px;" class="color1 fS13" id="courier_charges_status"></div>
							<div style="padding-left: 12px;padding-top: 10px;text-align: left;width: 217px;" class="color1 fS13" id="COD_status"></div>
							
							<a href="<?php echo site_url('Cms/refund_cancel');?>" target="_blank"><u>View Return Policy</u></a>
							</div>
							
                    </div>
                </div>  
            </div>
        </section>
		<script type="text/javascript">
		addProductToCart = function(){
				var proId = jQuery("#current_pro_id").val();
				var quantity = jQuery("#pro_quantity").val();
				var product_size = '';
				//-- update by sanjay, 10-Sep
				if(jQuery("#selected_option").length>0)
				{
					 product_size = jQuery("#selected_option").val();
						if(product_size=="" || typeof product_size=='undefined')
									{
						bootbox.alert("Please select the  size of the product, before adding."); 
						return false;
								}
			}
			 //--update by sanjay, 10-Sep
				var is_gift_wraped = isGiftWraped(proId); //-- update on 8/Dec
				jQuery.ajax({
					url:APIURL+'spiderCart/ajax_addtocart?_'+Math.random(),
					type:'POST',
					data:{new_product_id:proId,quantity:quantity,is_gift_wraped:is_gift_wraped,product_size:product_size},
			}).done(function(resp){
					//return false; //--Temp
					var response = jQuery.parseJSON(resp);
					if(response.status==1)
					{
						//injectpopup(response.add_to_cart_html);
						bootbox.dialog({
							title: "Your Cart",
							message: response.add_to_cart_html,
							size:'large',
							className:'makeitlarge',
						});
						ajax_getCartCount();
					}
			});
		};
		
		get_amount_by_quantity = function(){
				var proPrice = jQuery("#current_pro_price").val();	var quantity = jQuery("#pro_quantity").val();	var max_quantity = jQuery("#pro_quantity").attr('max');
						if(parseInt(quantity)>parseInt(max_quantity))
						{
							quantity = max_quantity;
							jQuery("#pro_quantity").val(quantity);
						}
				total_amount = proPrice*quantity;
				jQuery("#f_amnt").html(total_amount);
				return false;
		};
		
		jQuery(document).ready(function(){
			jQuery('#get_availibility_trigger').bind('click',function() {get_courier_charges();});
			jQuery("#buyers_pincode").live("keypress", function(e) { var code = (e.keyCode ? e.keyCode : e.which);	if (code == 13) {jQuery("#get_availibility_trigger").click();}	});
			if(jQuery("#buyers_pincode").val()){jQuery("#get_availibility_trigger").click();}
		});
		</script>
<style>
a:hover {
  color: red;
  position: relative;
}

a[title]:hover:after {
  content: attr(title);
  padding: 0px 10px;
  height:40px;
  line-height:40px;
  color: #fc5602;
  position: absolute;
  font-size:12px;
  right: -1px;
  top: -44px;
  white-space: nowrap;
  z-index: 99999px;
  background:#fff;
  border:solid 1px #fc5602;
}
</style>