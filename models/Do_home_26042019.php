<?php
class Do_home extends CI_Model{
   function __construct(){
		parent:: __construct();
		$this->load->database();
	}

	public function gethome_mobilebanner(){
		 $select="select img_path,img_link,img_order,id from banner_master where displayType='HomeMobile' and status=1 and status=1 and end_date >= curdate() and start_date <= curdate() order by img_order asc limit 0,6 ";
		 $res=$this->db->query($select);

		 if($res){
		 return $res1=$res->result_array();

		}else{
			echo "there is some problem while retriving the data"; die;
		 }
	}

	public function gethome_desktopbanner(){
		 $select="select img_path,img_link,img_order,id,start_date,end_date from banner_master where displayType='Flash' and status=1 and end_date >= curdate() and start_date <= curdate() order by img_order asc limit 0,6 ";
		 $res=$this->db->query($select);

		 if($res){
		 return $res1=$res->result_array();

		}else{
			echo "there is some problem while retriving the data"; die;
		 }
	}

	public function gethome_featuredcat(){
	 $select="select catid,catname,catimage from category_master where  cat_home_featured=1 and catstatus=1 order by catsort_order asc limit 0,6";
		 $res=$this->db->query($select);
		 if($res){
		 return $res1=$res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		 }
	}


	public function getcat_banner(){
		$select="select * from banner_master where displayType='MobileCategory' and status=1 limit 0,4";
		 $res=$this->db->query($select);
		 if($res){
		 return $res1=$res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		 }

	}
	public function gethotdeal_banner(){
		$select="select * from hotDealMaster where status=1 order by id desc limit  0,6";
		 $res=$this->db->query($select);
		 if($res){
		 return $res1=$res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		 }

	}

	public function gethome_featuredproduct($param=false){
		//print
		/*brand*/
		if($param['branddtl']){
			$param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
            $whr1.=" and pm.brandid in(".$param['branddtl'].")";
        }
        /*available*/
		if($param['avaliable']){
			$param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
			//  echo $param['avl'].'hiii1';
			$whr2.=" and pm.isAvailable in(".$param['avaliable'].")";
		}
		/*category*/
        if($param['categoryid']) {
        	$whr3 .= " and pc.category_id = '".$param['categoryid']."'";
        }
		/*price*/
        if($param['minprice'] and $param['maxprice']){
	    	$whr4.=" and pm.discountPrice >= '".$param['minprice']."' and pm.discountPrice <= '".$param['maxprice']."'";
        }

        if($param['count']===true){
        	$select="select COUNT(pm.proid) cnt from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes' AND pm.isAvailable=1 ".$whr3." ".$whr1." ".$whr2." ".$whr4."";
		}else{
			$select="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, 0 as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes'   ".$whr3." ".$whr1." ".$whr2." ".$whr4."  order by pm.isAvailable  DESC ";
			//$select.= " group by pm.proid";
			if(isset($param['start'])){
				$select.=" LIMIT ".$param['start'].", ".LIMIT."";
			}
		}
		$res=$this->db->query($select);

		if($res){
		 return $res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		}
	}


	public function gethome_hotdeal($param=false){
  
  		if($param['count']==true){
  			// $select="select COUNT(DISTINCT(pm.proid)) cnt from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where  pm.isAvailable=1 and  pm.callforprice='NO'";	
			$select="select cnt from summary where type='hot_deals' and cnt != 0";
		}else{
  			$select="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, (((onlinePrice - discountPrice) / onlinePrice) * 100) as discdiff, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, 0 as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where  pm.isAvailable=1 and  pm.callforprice='NO'   GROUP BY pm.proid  order by pm.isAvailable  DESC ";
  			if($param['module']=='homehotdeals'){
				$select = " SELECT proname, proid, isAvailable, prothumbnail, onlinePrice, discdiff, discountPrice, callforprice,
                                                proshortspecification, prolongspecification, brandid, category_id, catname, catparent_id,
                                                minprice, maxprice, rvcnt, overallrating, brandname from summary where type='hot_deals' and cnt = 0 ";
				$select.="limit 5";
			}else{
				$select.="limit ".$param['start'].", ".LIMIT."";
			}
  		}
		$res=$this->db->query($select);
		//print_r($res);die;
		if($res){
		 	return $res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		}


	}
	
	public function gethome_newarrival($param=false){
		if($param['count']){
			// $select="select COUNT(DISTINCT(pm.proid)) cnt from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid where pm.isAvailable=1 AND pm.isBook=0 AND pm.isPro=0 AND pm.prothumbnail IS NOT NULL AND DATE(proadddate) > DATE_SUB(CURDATE(), INTERVAL 1 YEAR)";
				$select="select cnt from summary where type='new_arrival'  and cnt != 0";
		}else{
			$select="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.isAvailable=1  AND pm.isBook=0 AND pm.isPro=0 AND pm.prothumbnail IS NOT NULL AND DATE(proadddate) > DATE_SUB(CURDATE(), INTERVAL 1 YEAR) GROUP BY pm.proid order by pm.isAvailable  DESC ";
			if($param['module']=='homehotdeals'){
				$select = "select proname, proid, isAvailable, prothumbnail, onlinePrice, discountPrice, callforprice,
                                                proshortspecification, prolongspecification, brandid, category_id, catname, catparent_id, 
                                                minprice, maxprice, rvcnt, overallrating, brandname from summary where type='new_arrival'  and cnt = 0";
			 	$select.=" LIMIT 5";
			}else{
				if($param['start']){
					$select.=" LIMIT ".$param['start'].", ".LIMIT."";
				}else{
					$select.=" LIMIT ".LIMIT."";
				}
			}
		}
		
		$res=$this->db->query($select);
		if($res){
		 	return $res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		}
	}

	public function newarrivalbooks($param){
		if($param['count']===true){
			// $select="select COUNT(pm.proid) cnt from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.isAvailable=1 and  cm.catparent_id=2 AND pm.isBook=1";
			$select = "SELECT cnt from summary where type='new_arrival_books'  and cnt != 0";
		}else{
			$select="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.isAvailable=1 and  cm.catparent_id=2 AND pm.isBook=1  order by pm.isAvailable  DESC " ;
			if($param['module']=='homehotdeals' || !isset($param['start'])){
				$select = "select proname, proid, isAvailable, prothumbnail, onlinePrice, discountPrice, callforprice, proshortspecification,
                                                prolongspecification, brandid, category_id, catname, catparent_id, minprice, maxprice, rvcnt, overallrating,
												brandname from summary where type='new_arrival_books' and cnt = 0 ";
				$select.=" LIMIT 20";
			}else{
				$select.=" LIMIT ".$param['start'].", ".LIMIT."";
			}
		}

		$res=$this->db->query($select);
		if($res){
		 	return $res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		}
	}

	public function getclerance_product($param){
		$select ="select cpm.productId, cpm.productName,cpm.onlinePrice,cpm.clearanceMRP,cpm. review,cpm.productImage1,cpm.productImage2,cpm.productImage3,cpm.productImage4, cpm.clearanceId,cpm.clearancePercentage,cpm.clearanceMRP,cpm.serialid,
		    cpm.clearanceAvailable FROM clerance_product_master cpm LEFT join product_master pm on cpm.productId=pm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_to_category pc on pm.proid=pc.proid left join review rm on cpm.productid = rm.proid where cpm.clearanceId='1' ";

		if($param['branddtl']){
			$param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
            $select.=" and cpm.brandid in(".$param['branddtl'].")";

         }
          if($param['avaliable']){
            $param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
          //  echo $param['avl'].'hiii1';
            $select.=" and cpm.clearanceAvailable in(".$param['avaliable'].")";

         }
        if($param['categoryid']) {
        	$select .= " and categoryid = '".$param['categoryid']."'";
        }

        if($param['minprice'] and $param['maxprice']){
	    	$select.=" and cpm.clearanceMRP >= '".$param['minprice']."' and cpm.clearanceMRP <= '".$param['maxprice']."'";

        }
        	$select.=" group by cpm.productId ";

        if($param['sort'] =='popular' ){
			$select .=" order by rm.overallrating desc ";
		}else if($param['sort'] =='lowprice' ){
			$select .=" order by cpm.clearanceMRP asc ";
		}
		else if($param['sort'] =='highprice'){
			$select .=" order by cpm.clearanceMRP desc ";
		}else{
			$select .=" order by rm.overallrating desc, pm.isAvailable desc ";
		}
		//print $select; die;

    //print_r($this->db); die;

    $res=$this->db->query($select);

    $res1=$res->result_array();
		//print_R($res1); die;

		if($res1){
      return $res1;
		}else{
			//echo "there is some problem while retriving the data"; die;
		 }

	}

	public function getclerance_filter($param=false){
		//die('hiii');
		/*brand*/
		if($param['branddtl']){
			$param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
            $whr1.=" and cpm.brandid in(".$param['branddtl'].")";
        }
        /*available*/
		if($param['avaliable']){
			$param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
			//  echo $param['avl'].'hiii1';
			$whr2.=" and clearanceAvailable in(".$param['avaliable'].")";
		}
		/*category*/
        if($param['categoryid']) {
        	$whr3 .= " and categoryid = '".$param['categoryid']."'";
        }
		/*price*/
        if($param['minprice'] and $param['maxprice']){
	    	$whr4.=" and clearanceMRP >= '".$param['minprice']."' and clearanceMRP <= '".$param['maxprice']."'";
        }
        /*category*/
		$selectcat = "select cpm.categoryid,cm.catname,count(cpm.productid) as productcount from clerance_product_master cpm join category_master cm on cpm.categoryid=cm.catid where clearanceId = 1 ".$whr3." ".$whr1." ".$whr2." GROUP by cpm.categoryid ";
		//echo $selectcat; die;
		$res4 = $this->db->query($selectcat);
		$res2['category'] = $res4->result_array();
		/*brand*/
		$select = "select cpm.brandid,COUNT(cpm.brandid) as brandcount, bm.brandname  from  clerance_product_master cpm join brand_master bm on cpm.brandid=bm.brandid  where clearanceId = 1 ".$whr3."  ".$whr1." ".$whr2."  GROUP BY cpm.brandid";
		//print $select;
		$res  = $this->db->query($select);
		$res2['brand']=$res->result_array();
		/*available*/
		$select1 = "SELECT case cpm.clearanceAvailable when 1 then '1'   when 2 then '2'  end as 'clearanceid',count(cpm.clearanceAvailable) as 'count'FROM `clerance_product_master` cpm join brand_master bm on cpm.brandid = bm.brandid where clearanceId = 1 ".$whr3." ".$whr2." ".$whr1." group by clearanceAvailable ";
		$res1  = $this->db->query($select1);
		//print '<br>';print $select1; die;
		$res2['available']=$res1->result_array();
		/*price*/
		$select2 = "SELECT  MIN(clearanceMRP) as minamount ,MAX(clearanceMRP) as maxamount FROM `clerance_product_master` where clearanceId = 1 ".$whr4."  ";
		$res3  = $this->db->query($select2);
		$res2['price']=$res3->result_array();

		if($res2){
		  return $res2;
		}else{
			echo "there is some problem while retriving the data"; die;
		 }

	}

	public function getfeaturedproduct_filter($param=false){
		//die('hiii');
		/*brand*/
		if($param['branddtl']){
			$param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
            $whr1.=" and pm.brandid in(".$param['branddtl'].")";
        }
        /*available*/
		if($param['avaliable']){
			$param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
			//  echo $param['avl'].'hiii1';
			$whr2.=" and pm.isAvailable in(".$param['avaliable'].")";
		}
		/*category*/
        if($param['categoryid']) {
        	$whr3 .= " and pc.category_id = '".$param['categoryid']."'";
        }
		/*price*/
        if($param['minprice'] and $param['maxprice']){
	    	$whr4.=" and pm.discountPrice >= '".$param['minprice']."' and pm.discountPrice <= '".$param['maxprice']."'";
        }
        /*category*/
		$selectcat = "select pc.category_id,cm.catname,count(pm.proid) as productcount from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes' ".$whr3." ".$whr1." ".$whr2." GROUP by pc.category_id";
		$res4 = $this->db->query($selectcat);
		$res2['category'] = $res4->result_array();
		/*brand*/
		$select = "select pm.brandid,COUNT(pm.brandid) as brandcount,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes' and pm.isAvailable=1 ".$whr3."  ".$whr1." ".$whr2." GROUP BY pm.brandid ";
		//print $select;
		$res  = $this->db->query($select);
		$res2['brand']=$res->result_array();
		/*available*/
		$select1 = "select case pm.isAvailable when 1 then '1'   when 0 then '0'  end as 'clearanceid',count(pm.isAvailable) as 'count' from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes'  ".$whr3." ".$whr2." ".$whr1." group by pm.isAvailable ";
		$res1  = $this->db->query($select1);
		//print '<br>';print $select1; die;
		$res2['available']=$res1->result_array();
		/*price*/
		$select2 = "select MIN(pm.discountPrice) as minamount ,MAX(pm.discountPrice) as maxamount  from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid where pm.homeFeatured='Yes' ".$whr4."  ";
		$res3  = $this->db->query($select2);
		$res2['price']=$res3->result_array();

		if($res2){
		  return $res2;
		}else{
			echo "there is some problem while retriving the data"; die;
		 }

	}

	public function get_clearncebanner(){
		 $select="select * from clearanceMaster where status='1'";
		 $res=$this->db->query($select);
		if($res){
		 return $res1=$res->result_array();
		}else{
			echo "there is some problem while retriving the data"; die;
		 }

	}
}
?>
