<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Furtados</title>
    <link rel="stylesheet" href="<?php echo WEBCSS;?>bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo WEBCSS;?>hapinesssalestyle.css">
</head>

<body>

    <div class="container-fluid">
        <div class="header header-bg mt-lg-4 mt-3"></div>
        <div class="row my-3">
            <div class="col-12 col-lg-7 first-div" style="border:none">
                <a href="https://www.furtadosonline.com/product/promotion/906"><div class="col border korg-key">
                </div></a>
                <div class="row m-0 mt-2">
                    <div class="col border mr-lg-2" style="background: url(<?php echo SITEIMAGES; ?>3%201.jpg);
                    background-repeat: no-repeat;
                    background-size: 100% 100%;">
                    	<a href="https://www.furtadosonline.com/gibson-electric-guitar-es-les-paul-ebony-eslp16ebnh1/212556">
                            <div class="granada" ></div>   
                        </a>
                    </div>
                    <div class="col border" style="background: url(<?php echo SITEIMAGES; ?>3%202.jpg);
                    background-repeat: no-repeat;
                               background-size: 100% 100%;"> 
                               <a href="https://www.furtadosonline.com/mapex-drum-set-storm-series-5-pcs-hybrid-with-hardware-camphor-wood-grain-st5295fic/214429">
                        	<div class="col" style="height: 100%;"></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="row m-0 mt-2 mt-lg-0">
                    <div class="col border second-div" style="background: url(<?php echo SITEIMAGES; ?>3%203.jpg);
                    background-repeat: no-repeat;
                    background-size: 100% 100%;"> 
                        <a href="https://www.furtadosonline.com/korg-digital-piano-g1b-air-brown/216515">
                        	<div class="second-div"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-red mb-3 px-4 pt-3">
            <h4>Terms & Conditions</h4>
            <div class="row">
                <!-- <div class="image"> <img src="<?php echo SITEIMAGES; ?>T&C_landing page.jpg" width="100%" alt="" class="img-responsive" style=""> </div> -->
                <div class="col-12 col-lg-6">
                    <ul style="list-style: disc;" class="pl-2">
                        <li> This offer is available on select products only.</li>
                        <li> This offer expires at 11:59 p.m., May 03, 2020. Complete payment for the product needs to be made  before expiration of the offer. Furtados reserves the right to extend or curtail the offer, depending on  the Covid-19 related lockdown mandated by the Government.</li>
                        <li> We will refund all money in full only if any item ordered sells out and is not available by the time  the money is received.</li>
                    </ul>
                </div>
                <div class="col-12 col-lg-6">
                    <ul style="list-style: disc;" class="pl-2">
                        <li> All orders placed will be shipped after the lock-down is lifted and shipping of products is allowed.  
                            As  there will be a huge backlog of deliveries across all logistics services, there is a possibility of some  delay
                            in customers receiving the purchased products after the lock-down is lifted, but every effort will  be made  
                            to expeditiously despatch all orders as soon as possible.
                         </li>
                        <li> This offer cannot be clubbed with any other existing offers.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row bottom-row mb-lg-4 m-lg-0">
            <div class="col-12 col-lg-3">
                <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1106"><img src="<?php echo SITEIMAGES; ?>acoustic_pianos.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
                <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1107"><img src="<?php echo SITEIMAGES; ?>digital_photos.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
                <a href="https://www.furtadosonline.com/keyboards-dmi/209?filter=&discount=[20,30]&sort=popular&searchText="><img src="<?php echo SITEIMAGES; ?>electronic_keyboards.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1084"> <img src="<?php echo SITEIMAGES; ?>acoustic_guitar.jpg" alt=""></a>
            </div>
        </div>
        <div class="row bottom-row mb-lg-4 m-lg-0">
            <div class="col-12 col-lg-3">
                <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1088"><img src="<?php echo SITEIMAGES; ?>electric_basses.jpg?" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1087"> <img src="<?php echo SITEIMAGES; ?>clasic-guitar.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1086"> <img src="<?php echo SITEIMAGES; ?>basses.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/drums-percussion/202?filter=&discount=[21,30]&sort=popular&searchText="> <img src="<?php echo SITEIMAGES; ?>drums_percussion.jpg" alt=""></a>
            </div>
        </div>
        <div class="row bottom-row mb-lg-4 m-lg-0">
            <div class="col-12 col-lg-3">
                <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1079"><img src="<?php echo SITEIMAGES; ?>guitar_amps.jpg" alt=""></a>
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1119"> <img src="<?php echo SITEIMAGES; ?>violin_winds.jpg?" alt=""></a>
               <!-- https://www.furtadosonline.com/bowed-strings/214?filter=&discount=[20,30]&sort=popular&searchText= -->
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906/?categoryid=1126"> <img src="<?php echo SITEIMAGES; ?>wind-instrument.jpg" alt=""></a>
               <!-- https://www.furtadosonline.com/wind-instruments/216?filter=&discount=[20,30]&sort=popular&searchText= -->
            </div>
            <div class="col-12 col-lg-3">
               <a href="https://www.furtadosonline.com/product/promotion/906"> <img src="<?php echo SITEIMAGES; ?>explore-square.jpg" alt=""></a>
            </div>
        </div>
    </div>
</body>

</html>