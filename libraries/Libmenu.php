<?php
class Libmenu{
             public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_common');
             $CI->load->library('libsession');
             $this->ci = $CI;

         }

		 public function get_dropdownmenu(){

       $memkey = 'libmenu_get_dropdownmenu';
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
       	//	print_r($memCacheDate); die;
        	return $memCacheDate;
       }
       		$res=$this->ci->Do_common->getallmenu();
			foreach($res as $key=>$val){

				$newarray[$val['catid']][$val['catname']][$val['levelid1']]['name']=$val['level1'];
					$catnameurl1=preg_replace('/[^A-Za-z0-9\-]/', '-', $val['level1']);
					$catnameurl1=str_replace("---","-", $catnameurl1);
					$catnameurl1=preg_replace('/-+/', '-',$catnameurl1);
					 $catnameurl1=strtolower(rtrim($catnameurl1,'-'));

					 $catnameurl2=preg_replace('/[^A-Za-z0-9\-]/', '-', $val['level2']);
					 $catnameurl2=str_replace("---","-", $catnameurl2);
					  $catnameurl2=preg_replace('/-+/', '-', $catnameurl2);
					 $catnameurl2=strtolower(rtrim($catnameurl2,'-'));

					$catnameurl3=preg_replace('/[^A-Za-z0-9\-]/', '-', $val['level3']);
					$catnameurl3=str_replace("---","-", $catnameurl3);
					$catnameurl3=preg_replace('/-+/', '-',$catnameurl3);
					$catnameurl3=strtolower(rtrim($catnameurl3,'-'));

				if($val['level1']=='GIFT CARDS'){
					$newarray[$val['catid']][$val['catname']][$val['levelid1']]['url']=''.SITEMOBURL.'product/giftcart';
				}else{
					$newarray[$val['catid']][$val['catname']][$val['levelid1']]['url']=''.SITEMOBURL.''.$catnameurl1.'/'.$val['levelid1'].'';
				}

				$newarray[$val['catid']][$val['catname']][$val['levelid1']]['image']=$val['catimage'];
				$newarray[$val['catid']][$val['catname']][$val['levelid1']]['priority']=$val['p1'];
				$newarray[$val['catid']][$val['catname']][$val['levelid1']]['name'] = $val['level1'];
				if($val['level2']){
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']]['priority'] = $val['p2'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']]['name'] = $val['level2'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']]['name']=$val['level2'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']]['priority']=$val['p2'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']]['url']=''.SITEMOBURL.''.$catnameurl2.'/'.$val['levelid2'].'';
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']][$val['levelid3']]['url']=''.SITEMOBURL.''.$catnameurl3.'/'.$val['levelid3'].'';
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']][$val['levelid3']]['name']=$val['level3'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']][$val['levelid3']]['priority']=$val['p3'];
					$newarray[$val['catid']][$val['catname']][$val['levelid1']][$val['level1']][$val['levelid2']][$val['level2']][$val['levelid3']]['url']=''.SITEMOBURL.''.$catnameurl3.'/'.$val['levelid3'].'';
				}
			}
			$newarray[1]['Musical Instruments'] = $this -> SortByKeyValue($newarray[1]['Musical Instruments'], 'priority');
			$newarray[2]['Music Books'] = $this -> SortByKeyValue($newarray[2]['Music Books'], 'priority');
			$newarray[3]['Pro Audio'] = $this -> SortByKeyValue($newarray[3]['Pro Audio'], 'priority');
			// echo json_encode($newarray); die;
			// echo '<pre>' . var_export($newarray, true) . '</pre>'; exit;
			$this->ci->libsession->setMemcache($memkey,$newarray);
			return $newarray;
		}

		public function SortByKeyValue($data, $sortKey, $sort_flags=SORT_ASC)
		{
			$x = $data;
			array_multisort(array_column($x, 'priority'), SORT_ASC, $x);
			foreach($x as $key => $value){
				$innerKey = $value['name'];
				$innerCats = $value[$innerKey];
				array_multisort(array_column($innerCats, 'priority'), SORT_ASC, $innerCats);
				$x[$key][$innerKey] = $innerCats;
				foreach($innerCats as $k3 => $v3){
					$innerKeyV3 = $v3['name'];
					// echo 'Key: '. $key .', innerKey: '.$innerKey. ', k3: '. $k3 . ', innerKeyV3: '. $innerKeyV3;
					// exit;
					$innerCatsV3 = $v3[$innerKeyV3];
					$temp_prio = $innerCatsV3['priority'];
					$temp_name = $innerCatsV3['name'];
					$temp_url = $innerCatsV3['url'];
					unset($innerCatsV3['priority']);
					unset($innerCatsV3['name']);
					unset($innerCatsV3['url']);
					array_multisort(array_column($innerCatsV3, 'priority'), SORT_ASC, $innerCatsV3);
					$innerCatsV3['priority'] = $temp_prio;
					$innerCatsV3['name'] = $temp_name;
					$innerCatsV3['url'] = $temp_url;
					// echo json_encode($innerCatsV3);
					// echo '..................................................................................................';
					$x[$key][$innerKey][$k3][$innerKeyV3] = $innerCatsV3;
				}

			}
			// echo json_encode($x); die;
			// echo '<pre>' . var_export($x, true) . '</pre>'; exit;

			return $x; // array_values() added for identical result with multisort*
		}




		/*viki created on 3/5/2018*/
		public function getprodtlbredacrum($param){
      $memkey = 'liburl_getprodtlbredacrum'.json_encode($param);
      $memCacheDate = $this->ci->libsession->getMemcache($memkey);
      if($memCacheDate){
        return $memCacheDate;
      }

		 	 $param['proid']=$param['id'];
			 if($param['proid']){

				$res=$this->ci->Do_common->getbreadcrum($param);

        $this->ci->libsession->setMemcache($memkey,$prodata);

				return $res;
			 }
		 }

		 public function getbrandname($param){

       $memkey = 'liburl_get_getbrandname'.json_encode($param);
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
         return $memCacheDate;
       }


		 	$burl=ADMINURL.$param['categoryname'].'/'.$param['categoryno'].'?'.'brand=[';
		 	$param['catid']=$param['cid'];
		 	if($param['catid']){
		 		$results=array();
		 		$res=$this->ci->Do_common->getbrandname($param);
		 		foreach ($res as $key => $val) {
		 			$results['brandname']=$val['brandname'];
		 			$results['brandurl']=$burl.$val['brandid'].']';
		 			//print_r($val['brandname']);
		 			$result[]=$results;
		 		}



		 		if($result){
          $this->ci->libsession->setMemcache($memkey,$result);
		 			return $result;
		 		}

		 	}
		 }

		 public function getsubcatname($param){
	       $memkey = 'lib_home_getsubcatname'.json_encode($param);
	       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
	       if($memCacheDate){
	         return $memCacheDate;
	       }
		 	$param['catid']=$param['cid'];
		 	if(!empty($param['catid'])){
		 		$res=$this->ci->Do_common->getsubcat($param);
		 		//print_r($res);
		 		$this->ci->libsession->setMemcache($memkey,$res);

		 		return $res;
		 	}
		 }
		/*viki: end*/
	

		 public function getcatname($param){
       $memkey = 'lib_home_getcatname'.json_encode($param);
       $memCacheDate = $this->ci->libsession->getMemcache($memkey);
       if($memCacheDate){
         return $memCacheDate;
       }

		 	$param['catid']=$param['cid'];
		 	if(!empty($param['catid'])){
		 		$res=$this->ci->Do_common->getcatname($param);
		 		//print_r($res);
		 		$res['maincatname']=preg_replace('/[^A-Za-z0-9\-]/', '-', $res['maincategory'][0]['maincat']);
		 		$res['maincatid']=$res['maincategory'][0]['catid'];
		 		$res['maincaturl']=ADMINURL.$res['maincatname'].'/'.$res['maincatid'];

        $this->ci->libsession->setMemcache($memkey,$res);

		 		return $res;
		 	}
		 }
		/*viki: end*/
	}
?>
