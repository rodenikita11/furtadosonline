 <!--<form role="form" method="post" action="#">-->
 <?php if($this->Do_customer->isUserLogin()){ 
	$param['proid']=$this->proid;
	$param['custid']=$this->spidersession->getSession('buyer_id');
	$reviewid=$this->Do_customer->hasnotgivenreview($param);
	if(empty($reviewid))
	{
 ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="inputTitle" class="control-label">Title:</label>
                                                        <div>
                                                            <input type="text" class="form-control" id="inputTitle">
                                                        </div>
                                                    </div>
                                                </div>
                                                
												<!--<div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputCompany" class="control-label">Name:</label>
                                                        <div>
                                                            <input type="text" class="form-control" id="inputName">
                                                        </div>
                                                    </div>
                                                </div>-->
												
                                                <div class="col-md-12">
                                                    <div class="form-group">
														<label for="inputReview" class="control-label">Your Review:</label>
                                                        <textarea id="inputReview" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label  class="control-label">Your Rate:</label>
                                                       
                                                    </div>
													 <input id="input-21a" value="0" type="number" class="rating" data-symbol="*" min=0 max=5 step=0.5 data-size="xs" >
													 <input id="r_score" value="0" type='hidden'>
													 
													<script>
													jQuery(document).ready(function () {
														$('#input-21a').rating({
															  min: 0,
															  max: 5,
															  step: 1,
															  size: 'lg',
															  showClear: false,
															  showCaption: false,
														   });
														$("#input-21a").on("rating.change", function(event, value, caption) {
															jQuery("#r_score").val(value);
														});
													});
												</script>
                                                </div>
                                            </div>
		<div id="review_alert"></div>
               <input type="button" onclick="writeMyReview();" class="btn-default-1" id="addreview" value="Add Review">
<script type="text/javascript">
writeMyReview = function(){
		var loginStatus = jQuery("#user_login_status").val();
		console.log(loginStatus);
			if(loginStatus=='Y')
			{
				var r_proId = jQuery("#hidden_proId").val();
				var r_title = jQuery("#inputTitle").val();
				var r_review = jQuery("#inputReview").val();
				var r_score = jQuery("#r_score").val();
				console.log(r_title+" "+r_review+" "+r_score);
				
				/* Pritish */
				if(r_title==""||r_review=="")
				{
					jQuery("#review_alert").html('Please enter details.').css({'color':'red'}); 
					return false;
				}
						if(r_score!='')
						{
				jQuery.ajax({
						url : APIURL+"review/ajax_addreview?_"+Math.random(),
						data : {r_proId:r_proId,r_title:r_title,r_review:r_review,r_score:r_score},
						type:'POST',
				}).done(function(resp){
						var response = jQuery.parseJSON(resp);
							if(response.status==1)
							{
								jQuery("#review_alert").html(response.msg).css({'color':'green'}); 
								clearReviewField(); 
								jQuery("#addreview").attr('onclick','javascript:void(0);');
								return false;
							}
							else
							{
								jQuery("#review_alert").html(response.msg).css({'color':'red'}); return false;
							}
				});
					}
					else
					{
						jQuery("#review_alert").html('Please select rating.').css({'color':'red'}); return false;
					}
					
				return false;
			}
			else
			{
				jQuery("#review_alert").html('Please login to continue'); return false;
			}
};

clearReviewField = function(){
		jQuery("#inputName").val('');
		jQuery("#inputTitle").val('');
		jQuery("#inputReview").val('');
		jQuery("#addreview").remove();
};
</script>
<?php }
	else
	{
		echo "<span>You have  already reviewed this product !</span>";
	}
}
else
{
echo '<span>Please <a href="javascript:void(0);" onclick="get_login_signup_pops(\'L\');" style="color: #fc5602; border: solid 1px #ccc;">login</a> to review</span>';
}
?>
                     <!--</form>-->
