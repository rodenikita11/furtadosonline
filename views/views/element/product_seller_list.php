<?php 
$param['proid']=$result1[0]['proid'];
$param['cid'] = $param['cid'];
$res1=$this->Do_product->othersellers($param);
if($res1){ ?>
	<div class="container mrt40">
		<div class="row">
		  <div class="col-md-12 font-fa1" style="padding-right:29px;">
		  <h3>Other Sellers</h3>
			  <div class="seller_list_scroll">
				<div class="sell_scroll_in">
				  <div class="row dt-col-height1">
					  <div class="dt-col-1">SELLER</div>
					  <!--<div class="dt-col-2 fS18">*</div>-->
					  <div class="dt-col-3">STOCK QTY</div>
					  <div class="dt-col-4">DELIVERY</div>
					  <div class="dt-col-5">GIFT WRAP</div>
					  <div class="dt-col-6">DELIVERY CHARGES</div>
					  <div class="dt-col-7">RATE</div>
					  <div class="dt-col-8">BUYING OPTION</div>
				  </div>
				  <?php foreach($res1 as $key=>$value) { //print_r($res1); die; ?>
				  <div class="row dt-col-height2">
					  <a href="<?php echo $value['proURL'];?>">
					  <div class="dt-col-1"><?php echo $value['shopname'];?></div>
					  <!--<div class="dt-col-2 color4">Demo</div>-->
					  <div class="dt-col-3"><?php echo (($value['quantity']>0?'Available':'Out of stock'));?></div>
					  <div class="dt-col-4">Delivery in <?echo $value['deliverytime']?> days.</div>
					  <div class="dt-col-5">Yes</div>
					  <?php if($value['free_home_delivery']!='Y'){ ?>
						<div class="dt-col-6"><span rel="<?php echo $value['prosellerid'];?>" id="dynamic_courier_charges<?php echo $value['prosellerid'];?>" class="dynamic_courier" data-o-seller='<?php echo $value['seller_id'];?>' data-o-pro='<?php echo $value['prosellerid'];?>'>N/A</span></div>
					  <?php } else { ?>
						<div class="dt-col-6">Free home delivery</div>
					  <?php } ?>
					  </a>
					  <div class="dt-col-7"><i class="fa fa-inr fS14 color2"></i>
						<?php echo ((isset($value['finalprice']) && $value['finalprice']!=$value['price'])?$value['finalprice']:$value['price']);?>
					  </div>
					  <div class="dt-col-8"><button class="product2cart" data-id="<?php echo $value['prosellerid'];?>"><i class="fa fa-shopping-cart"></i>  Add to Cart</button></div>
				  </div>
				  <?php } ?>
				  </div>
			  </div>
		  </div>
		</div>
	</div>
<?php } ?>