<table id="summary" style="width:100%;border-collapse:collapse;"> 
           <tbody>
            <tr> 
             <td style="vertical-align:top;font-size:13px;line-height:18px;font-family:Arial, sans-serif;"> <h3 style="font-size:18px;color:rgb(204, 102, 0);font-weight:normal;">Hello, <?php echo $current_cart_details[0]['customer_firstname'].' '.$current_cart_details[0]['customer_lastname'];?></h3> 
			 <p style="font:12px/ 16px Arial, sans-serif;">Congratulations!!! You have successfully redeem points.</p>
			 <p style="font:12px/ 16px Arial, sans-serif;">Thank you for shopping with us.</p>
			 </td> 
            </tr>
            <tr> 
             <td style="vertical-align:top;font-size:13px;line-height:18px;font-family:Arial, sans-serif; padding-bottom: 10px;"> 
				<p>Your order details for Order Id: <?php echo $current_cart_details[0]['om_invoicenum'];?></p>
				<table border="1" style="border-style:solid;border-width:1px;border-color:#eeeaea;margin:15px 0 0;">
					<tr>
						<th>#</th>
						<th style="width:250px;">Name</th>
						<th>Quantity</th>
						<th>Point</th>
						<th>Sub-total</th>
						<th>Date</th>
					</tr>
					<?php  $counter=0;
					foreach($current_cart_details as $order=>$orderDetails) { ?>
						<tr>
							<td><?php echo ++$counter;?></td>
							<td style="width:250px;"><?php echo $orderDetails['op_product_name'];?></td>
							<td><?php echo $orderDetails['op_product_quantity'];?></td>
							<td><?php echo number_format((float)$orderDetails['op_product_price'], 2, '.', '');?></td>
							<td><?php echo number_format((float)$orderDetails['op_total_price'], 2, '.', '');?></td>
							<td><?php echo date('d/M/Y',strtotime($orderDetails['op_created']));?></td>
						</tr>
					<?php } ?>
				</table>
			 </td> 
            </tr>
			<tr style="border-style: solid; border-color: rgb(235, 219, 235);">
				<td>
					<table>
						<tr>
							<td style="padding:10px;margin:0px;">
								<p style="padding:0px;margin:0px;"><b>Shipping Details</b></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_contact_person'];?></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_address'];?></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_landmark'];?></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_state'];?></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_city'];?></p>
								<p style="padding:0px;margin:0px;"><?php echo $current_cart_details[0]['shipping_postcode'];?></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
           </tbody>
</table>