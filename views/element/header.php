<script src="<?php echo SITEJS?>new-menu.js" type="text/javascript" ></script>
<link rel="stylesheet" href="<?echo SITECSS?>new-menu.css" type="text/css" media="screen" />
<style>
.header-bg-n{background:#000; width:100%;}
.menu-bg-n{background:#fc5602; width:100%;}
.new-top-search{ background-image: url("../assets/images/search-box.png"); background-position: left center; background-repeat: no-repeat; border: medium none; display: block; float: left; height: 25px; padding-left: 30px; padding-right: 10px; width:380px; border-right:solid 1px #000; color:#fc5602; margin-top:0px; margin-bottom:10px; margin-top:8px;}
.new-top-btn { border: medium none; color: #000; cursor: pointer; display: block; height: 25px; width: 102px; background-color:#fff; font-size:14px; font-weight:500; margin-top:0px; float:left; margin-top:8px;}
.new-top-btn:hover{border: medium none; color: #fff; cursor: pointer; display: block; height: 25px; margin-top:0px; width: 102px; background:#fc5602; margin-top:8px;}
.signin{background: none repeat scroll 0 0 #fff; color: #000; font-size:14px; display: block; height: 25px; line-height: 25px; text-align: center; width: 76px; float:left; margin-top:8px;}
.signin:hover{background: none repeat scroll 0 0 #fc5602; color: #fff; display: block;font-size: 14px; height: 25px; line-height: 25px; text-align: center; width: 76px; margin-top:8px;}
.cart-n{background-image: url("../assets/images/cart-n.png"); background-position: left center; background-repeat: no-repeat; width:40px; font-size:18px; text-align:center; color:#fff; float:right; margin-right:-35px; height:35px; margin-left:20px; margin-top:-5px;}




ul#topnavs {
	margin: 0; padding: 0;
	float: left;
	width: 100%;
	list-style: none;
	position: relative;
	font-size: 1.1em;
	background:url(images/topnav_s.gif) repeat-x;
}
ul#topnavs li {
	float: left;
	margin: 0; padding: 0;
}
ul#topnavs li a {
	padding: 10px 15px;
	display: block;
	color: #000;
	text-decoration: none;
}
ul#topnavs li a:visited {
	padding: 10px 15px;
	display: block;
	color: #;
	text-decoration: none;
}
ul#topnavs li a:hover {
	padding: 10px 15px;
	display: block;
	color: #000;
	text-decoration: none;
	z-index:99999;
}
ul#topnavs li a:active {
	padding: 10px 15px;
	display: block;
	color: #000 !important;
	text-decoration: none;
}
ul#topnavs li:hover {  }
ul#topnavs li ul {
	float: left;
	padding: 15px 0;
	position: absolute;
	left: 0; 
	display: none;
	width: 100%;
	background: #fff;
	border-left: solid #fc5602 1px;
	border-top:none;
	border-right:none;
	color: #000 !important;
	-moz-border-radius-bottomright: 5px;
	-khtml-border-radius-bottomright: 5px;
	-webkit-border-bottom-right-radius: 5px;
	-moz-border-radius-bottomleft: 5px;
	-khtml-border-radius-bottomleft: 5px;
	-webkit-border-bottom-left-radius: 5px;
}
ul#topnavs li:hover ul { display: block; list-style:none; margin:0px; padding:0px; color:#000; }
ul#topnavs li ul li a { display: inline-block; width:233px; color:#000; border-bottom:solid 1px #fc5602; border-right:solid 1px #fc5602; margin-bottom:0px; height:65px; }
ul#topnavs li ul li a span{width:150px; float:left; vertical-align:middle;}
ul#topnavs li ul li a img{float:;}
ul#topnavs li ul li a:hover {text-decoration: none; color:#fc5602 !important;}
</style>
<script type="text/javascript">
$(document).ready(function() {
	
$("ul#topnavs li").hover(function() { //Hover over event on list item
	$(this).css({ 'background' : '#fff url(topnav_active.gif) repeat-x'}); //Add background color + image on hovered list item
	$(this).find("ul").show(); //Show the subnav
} , function() { //on hover out...
	$(this).css({ 'background' : 'none'}); //Ditch the background
	$(this).find("ul").hide(); //Hide the subnav
});
	
});
</script>
<div class="header-bg-n">
<!-- logo search sign cart-->
	<?php $this->load->view('element/promotional_details');#-- Update by sanjay, 14/Oct?>
	<div class="container" style="width:1140px;">
    	<div class="span12" style="padding-top:6px;">
        	<div class="span3">
            	<a class="hand" href="<?echo SITEURL?>"><img src="<?echo SITEIMAGES?>logo-n.png" width="100" /></a>
            </div>
            <div class="span6">
            	<form method='get' action='<?php echo SITEURL?>products/category'>
                    <input type="text" value='<?=$_GET['query']?>' placeholder="Search for Product " class="new-top-search" name='query' id='search' />
                    <button class="new-top-btn">SEARCH</button>
                </form>
            </div>
            <div class="span3" style="margin-left:-12px;">
            	<div>
				<?php if(!$this->Do_customer->isUserLogin()){ ?>
				<a href="javascript:void(0)" onclick="get_login_signup_pops('L');"><span class="signin">SIGN IN</span></a>
				<?php } ?>
				<?php if($this->Do_customer->isUserLogin()){ ?>
					<?php $this->load->view('element/user_short_cut'); #-- Update, Sanjay 14/oct/2014?>
				<?php } ?>
				</div>
                <div style="float:left; margin:3px 21px; text-align:center;"><a target="_blank" href="<?php echo SPIDER_SELLER;?>Registration/showregistration" style="color:#fff;">Sale Your<br/> Products</a></div>
                <div class="left" style="margin-left:8px; margin-right:11px; margin-top:10px;"><a href="#modal" id="modal_trigger"><img src="<?echo SITEIMAGES?>message-n.png" width="22" /></a></div>
                <div class="left" style="margin-top:9px;"><a href="#"><img src="<?echo SITEIMAGES?>contact-n.png" width="22" /></a></div>
               <a class="right" style="margin-top:7px;" onclick="showCartSummary();" href="javascript:void(0);"> <div class="cart-n" style="background-image:url(<?echo SITEIMAGES?>cart-n.png)"><span id="currentitemcount"></span></div></a>
            </div>
            <div class="clear" style="clear:both;"></div>
        </div>
    </div>
<!--end logo search sign cart-->
<div class="clear"></div>
<!--menuabr-->
<div class="menu-bg-n">
	<div class="container">
<ul id="topnavs" style="z-index:9999999 !important;">
<?php $res=$this->Do_category->getmaincategory();
    foreach ($res as $row){ ?>
	<li><a href="<?echo $row['url']?>"><?echo $row['name']?></a>
     <? $result=$this->Do_category->category();
				if(!empty($result))
		  foreach ($result as $key=>$val){
		                   if($row['name']==$val['catname']){ ?>
							<?php if($val['subcat']){  ?>
									<ul style="z-index:9999999;">
 							<?php
													
													foreach($val['subcat'] as $keyname=>$valuename){ ?>
							<li id="menu-item-1"><a href="<?echo $valuename['url']?>"><span><?echo $keyname?></span> <img style="max-height:45px;" src="<?php echo $this->lib_storeuri->getprothumbnail($valuename['image'],45);?>"<?/*echo SITEIMAGESadidas.jpg"*/?>/></a></li>
													<?php } ?>
								</ul>
							</li>
									<?php } ?>
								<?php } } } ?>
	</ul>			 	
    </div>
</div>
<!--end menuabr-->
</div>

<script>
  jQuery(function() {
    jQuery("#search" ).autocomplete({
	  minLength: 2,
      delay: 0,
      source: APIURL+"ajax/searchsuggest",
	  select: function (event, ui) { window.location = ui.item.url;  }
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( item.label + "<br><span class='autocategory'>" + item.category + "</span>" )
        .appendTo( ul );
    };
  });
</script>
