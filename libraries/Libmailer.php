<? class Libmailer{
	    public $ci;
        public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_common');
             $CI->load->model('Do_home');
             $CI->load->model('Do_order');
             $CI->load->library('libsession');
             $CI->load->library('libgetprice');
			 //$CI->load->library('Mailclass');
			 $CI->load->library('Falconide');
			 $this->ci = $CI;
        }

		 
		public function registermail($param){
            $from = array(WEB_SENDER => DOMAIN_NAME);
	        $to = array($param['email'] => $param['name'], 'registration@furtadosonline.com'=>DOMAIN_NAME);

            $subject = "Registration at ".DOMAIN_NAME;
			
          
		    $mailer_string .=MAILER_HEADER;
			//$mailer_string .= "<tr>";
			//$mailer_string .= "<td style='padding:20px 20px; color:#464F4B; font-family:Arial; font-size:12px; background-color:#ff;'>";
			$mailer_string .= "<p>Dear ".strtoupper($title." ".$param['name']." ").",</p>";
			$mailer_string .= "<p>Thank you for registering at <a href='".DOMAIN_PATH."' target='_blank' style='color:#464F4B;font-size:12px;'><label style='color:#464F4B;font-size:12px;cursor:pointer;'>".DOMAIN_NAME."</label></a></p>";
			$mailer_string.="<p>At Furtados, we have a wide range of musical instruments and music books. You can avail of our special offers by clicking here.</p>";
			$mailer_string .="<p>Happy browsing and hope you have a wonderful shopping experience with us.</p>";
			$mailer_string .= "<p style='color:#464F4B;'>For feedback/ errors during your registration on furtadosonline.com, contact us on <a href='mailto:".RESPONSE_MAIL."' style='color:#464F4B;'>".RESPONSE_MAIL."</a></p>";
		    $mailer_string .=MAILER_FOOTER;
			//echo $mailer_string;die;
			$emailobj=new Falconide();
			$recipients=$emailobj->sendmail($param['email'], $subject, $mailer_string);
		}

        public function  budleenquirymail($param){
        	
			
			$from = array($param['usremail'] => $param['usrname']);
			$to = array('allmails@furtadosonline.com','snehamulajkar@gmail.com');
           
			$subject = "Bundle Product link - Furtados";							// SEND MAIL
		
			$html = MAILER_HEADER;
			$html .= "<p>Dear ".$param['usrname'].",</p>";
			$html .= "<p>Greetings from Furtadosonline.</p>";
			$html .= "<p>Congratulations on getting the best offer from us on your bundle request.<p>";
			$html .= "<p>Please click on the link below to view & proceed with your bundle order.</p>";
			//$html .= "<p><b>Use this link :</b><a href= ".PREFIX_RUN."bundleShoppingCart.php?bundleId=".base64_encode($bundleId).">".PREFIX_RUN."bundleShoppingCart.php?bundleId=".base64_encode($bundleId)."</a><p>";
			$html .= "<p><b>Use this link :</b><a href= ".PREFIX_RUN."bundleShoppingCart.php?bundleId=".base64_encode($param['bundleId']).">".PREFIX_RUN."bundleShoppingCart.php?bundleId=".base64_encode($param['bundleId'])."</a><p>";
			
			$html .= "<p>You can avail the EMI option for your bundle order.<p>";
			$html .= "<b>Thank you for your association with us.</b><br><br>";
			
			//$html .= "<p>".$reply_feedback_db."</p>";
			$html .= MAILER_FOOTER;
			$emailobj=new Mailclass($param['usremail'], $subject, $html);
			$recipients=$emailobj->sendmail();
		}

       public function feedbackdelivery(){
	 	   $message_string = MAILER_HEADER;
		   $message_string .= " <tr>
							<td style='padding:5px 5px; color:#464F4B; font-family:Arial; font-size:12px; font-weight:normal;'>
							<br>Hi," ;
			$message_string .= "<br><br> Here is  delivery feedback for furtadosonline.com <br><br>";
	        $message_string .= "<b>Order No. : </b>".stripslashes($orderId)."<br><br> ";
			$message_string .= "<b>Name : </b>".$_SESSION['fullName']." <br><br>";
			$message_string .= "<b>Email : </b>".$userEmail." <br><br>";
			
	    	for($i=1; $i<=$ratingstar; $i++){$nunStar .= "<img src=".SITEIMAGES."emailer_images/star.jpg />";} 		
	        $message_string .= "<b>Rate Delivery experience :</b> " .$nunStar." <br><br>";
			$message_string .= "<b>Was the package delivered when? : </b> ".$userFeed." <br><br>";
			$message_string .= "<b>Comment : </b> ".$CommentText;
			//$message_string .= "<br><br><b>IP Address : </b>".getIPAddress();
		    $message_string .= MAILER_FOOTER;
			
			
			//require_once "lib/swift_required.php";
			$from = array(WEB_SENDER => DOMAIN_NAME);
			$to = array(PRODUCT_FEEDBACK_EMAIL => DOMAIN_NAME);	//PRODUCT_FEEDBACK_EMAIL
		 //  $to = array('junedhmd9@gmail.com' => 'juned');
						  
			$subject = 'Delivery feedback for  order no.  : '.$orderId;			
	   }
	   
	   public function postreview($param=false){
	   	
		    $from = array(WEB_SENDER => DOMAIN_NAME);
			$to = array('product_review@furtadosonline.com'=>DOMAIN_NAME); //'dsom26@gmail.com'=>DOMAIN_NAME,;
			//$to = array('dsom26@gmail.com'=>'juned');

			$subject =  ucfirst($param['usrname']). ' has submitted review for ' .$param['proname'].'('.$param['pid'].')';
		    $mailer_string .=MAILER_HEADER;
			$mailer_string .= "<p>Dear,</p>";
			$mailer_string .= "<p>".ucfirst($param['usrname'])." has submitted review  for ".$param['proname']."(".$param['pid'].")</p>";
			$mailer_string .= "<p>Title: <b>".$param['reviewtitle1']."</b></p>";
			$mailer_string .= "<p>Over All: <b>".$param['ratingme']." star</b></p>";
			//$mailer_string .= "<p>Features : <b>".$Features_star."</b></p>";
			//$mailer_string .= "<p>Quality : <b>".$Quality_star."</b></p>";
			//$mailer_string .= "<p>Value: <b>".$Value_star."</b></p>";
			//$mailer_string .= "<p>Experience With Product : <b>".$experience."</b></p>";
			//$mailer_string .= "<p>My Background :<b>".$text_ackground."</b></p>";
			$mailer_string .= "<p>My Review : <b>".$param['reviewmessage']."</b></p>";
			$mailer_string .=MAILER_FOOTER;
			$emailobj=new Mailclass($param['usremail'], $subject, $mailer_string);
			$recipients=$emailobj->sendmail();
		   
	   }
	   public function account_update($param=false){
	   		
	   		$from = array(WEB_SENDER => DOMAIN_NAME);

	        $to = array($param['email'] => $param['fname'], 'registration@furtadosonline.com'=>DOMAIN_NAME);
		   	$mailer_string = MAILER_HEADER;
			$mailer_string .= "<p>Dear ".strtoupper($param['title']." ".$param['fname']." ".$param['lname']).",</p>";
			$mailer_string .= "<p>Your personal details have been modified on <a href='".DOMAIN_PATH."' target='_blank' style='cursor: pointer;'><b>".DOMAIN_NAME."</b></a>,as requested by you.</p>";
			$mailer_string .= "<p style='color:#464F4B;'>If you have not modified your profile and are receiving this mail in error we request you to inform us immediately by e-mailing us at <a href='mailto:".RESPONSE_MAIL."' style='color:#464F4B;'>".RESPONSE_MAIL."</a></p>";
			$mailer_string.= MAILER_FOOTER; 
				
			$subject = "Modification in personal details on ".DOMAIN_NAME;
			// $emailobj=new Mailclass($param['emailid'], $subject, $mailer_string);
			// $recipients=$emailobj->sendmail();
		   $emailobj=new Falconide();
			$recipients=$emailobj->sendmail($param['email'], $subject, $mailer_string);
		   
	   }
	 
	   public function orderdetails($param=false){
		   	$mailerMessage = MAILER_HEADER;
			$mailerMessage .= '<br>Dear '.strtoupper($param[0]['customer_name']).',<br><br>Thank you for your order on furtadosonline.com.<br><br>You can avail of our special offers by clicking <a href="'.ROOTURL.'product/offerzone">here</a>.<br><br>Details of your order are as follows:<br><br>
						<table width="100%" border="1" cellpadding="0" cellspacing="0" style="border:1px solid rgb(204,204,204);border-collapse:collapse;padding:10px;font-size:12px;font-weight:normal;color:#464f4b;">
							  <tbody><tr style="background-color:#f9f9f9">
								<td colspan="2" style="padding:10px; background:url('.SITEIMAGES.'emailer_images/grid2.jpg) repeat-x; color:#FFF; text-transform:uppercase;"><b>Order Details</b></td>
							  </tr>
							  <tr>
								<td style="padding:10px;">Order Id : </td>
								<td style="padding:10px;"><b>'.$param[0]['orderId'].'</b></td>
							  </tr>
							  <tr>
								<td style="padding:10px">Date : </td>
								<td style="padding:10px"><b>'.date($param[0]['insertDate']).'</b></td>
							  </tr>
							  <tr>
								<td width="29%" style="padding:10px">Customer Name : </td>
								<td width="71%" style="padding:10px"><b>'.ucfirst($param[0]['customer_name']).'</b></td>
							  </tr>
							  <tr>
								<td style="padding:10px">Payment Mode : </td>
								<td style="padding:10px"><b>'.$param[0]['orderpayment_method'].'</b></td>
							  </tr>
							  <!--<tr>
								<td style="padding:10px" valign="top">Shipping Mode : </td>
								<td style="padding:10px"><b>'.$param[0]['shipping_mode'].'</b></td>
							 </tr>-->
							 <tr>
								<td style="padding:10px" valign="top">Shipping Address : </td>
								<td style="padding:10px"><b>'.$param[0]['address_1'].'</b><br>
									<p>'.$param[0]['cityName'].'</p>
									<p>'.$param[0]['stateName'].' - '.$param[0]['postcode'].'</p>
								</td>
							  </tr>
							</tbody></table><br>
							<table cellpadding="5" cellspacing="0" width="100%" class="print_final"  border="1" style="border:1px solid rgb(204,204,204);border-collapse:collapse;padding:10px;font-size:12px;font-weight:normal;color:#464f4b;">
                 <tr style="background:url('.SITEIMAGES.'emailer_images/grid2.jpg) repeat-x; color:#FFF; text-transform:uppercase;font-size:12px;">
                    <td width="10%" height="30" style="color:#fff;border-bottom:none;">Sr. No.</td>
                    <td width="8%" style="color:#fff;border-bottom:none;">Image</td>
                    <td width="40%" style="color:#fff;border-bottom:none;">Product</td>
                    <td width="2%" style="color:#fff;border-bottom:none;">Quantity</td>
                    <td width="25%" align="right" style="padding-right:10px;color:#fff;border-bottom:none;">Price</td>
                    <td width="25%" align="right" style="padding-right:10px;color:#fff;border-bottom:none;">Total</td>
                 </tr>';
			$cnt = 1;
			
			//foreach($param as $arrPid => $arrPidDetails){
			for ($i=0; $i <count($param) ; $i++) { 
			
			
				
				//$selPPSql = "SELECT promotionId, onlinePrice, clearanceId, clearanceMRP FROM product_master WHERE proid = ".$param[$i]['product_id']." AND status = 1 LIMIT 1";
				$product_id=$param[$i]['product_id'];
				
				$result=$this->ci->Do_order->getproiddtl($product_id);
				
				/*$selP = mysqli_query($selPPSql);
				$selPRow = $selP->fetch_array();*/
				//print_r($result);die;
				if(!empty($result)){

					//$selPRow = $selP->fetch_array();
					
					$promotionId = $result[0]['promotionId'];
					$clearanceId = $result[0]['clearanceId1'];
					//$clearanceId2 = $result[0]['clearanceId2'];
					$serialid = $result[0]['serialid'];
					//$onlinePrice = $result[0]['onlinePrice'];
					
					$onlinePrice = $this->ci->libgetprice->priceFormat($result[0]['onlinePrice']);
				}else{
					$promotionId = 0;
				}
			 $siteUrl=SITEMOBURL;
			$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $param[$i]['name']); 
			$proname=str_replace("--","-", $proname) ;			 
			$proname=strtolower(rtrim($proname,'-'));
				
				//$tarUrl = PREFIX_RUN.'product/'.getURLText($arrPidDetails['name']).'-'.$arrPid.'.html';
				/*if($clearanceId2 != 'null' || $clearanceId2 ==1){
					
					$tarUrl = $siteUrl.''.$proname.'/'.$param[$i]['product_id'].'?sid='.$serialid.'';
					echo $tarUrl;
					if($clearanceId2 == 1){
						$shippingstatus = 1;	
						$pShippingMsg = '* Shipping Cost Applicable'; $isShippingMsg = 1;
					}else{
						$shippingstatus = 2;	
					}
					
					$clSql=$this->ci->Do_order->getclearencelogo($product_id);
					
					print_r($clSql);die;
					if(!empty($clSql)){
						$clRow = $cl->fetch_array();

						$promoLogo = str_replace('_80x60.','_40x36.',stripslashes($clRow['logoImage']));
						$promoLogoImage = CLEARANCE_IMAGE_DB.$promoLogo;
					}
					
					$promoLink = 'clearance.php';
					$promoId = $clearanceId;
				}else if($promotionId != 0){
					
					$today = date('Y-m-d H:i:s');
					
					$promoSql=$this->ci->Do_order->getpromotiondtl($promotionId);
					
					print_r($promoSql);die;
					if(!empty($promoSql)){
						
						$promoName = stripslashes($promoRow['Name']);
						$promoLogo = str_replace('_80x60.','_150x30.',stripslashes($promoRow['logoImage']));
						$promoLogoImage = PROMOTION_IMAGE_DB.$promoLogo;
						$shippingstatus = stripslashes($promoRow['shippingstatus']);	// 1 : include, 2 : exclude
						switch($shippingstatus){
							case 1 : $pShippingMsg = '* Shipping Cost Applicable'; $isShippingMsg = 1; break;
							default : $pShippingMsg = ''; break;
						}
					}
					$promoLink = 'promotion.php';
					$promoId = $promotionId;
				}else{
					$promoName = ''; $promoLogo	= ''; $shippingstatus = 0; $pShippingMsg = ''; $promoLink = '';  $promoId = '';
				}*/
               
         
          if(!isset($param[$i]['image']))
          {
          	$img=SITEURL.'assets/images/mobimg/noimage1.png';
          }else{
          	$img=$param[$i]['image'];
          }


        

        $mailerMessage .= '<tr align="center">
                <td>'.$cnt++.']</td>
                <td><a href="'.$siteUrl.''.$proname.'/'.$param[$i]['product_id'].'"><img src='.$img.' alt="'.$param[$i]['name'].'"/></a></td>
                <td align="left"><a href="'.$siteUrl.''.$proname.'/'.$param[$i]['product_id'].'">'.stripslashes($param[$i]['name']).'</a>
                	<br/><br/>
		             <p style="font-size:10px;"> 
         	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td  align="left" valign="middle" style="border-bottom:none;font-size: 10px;"> Item # : '.$param[$i]['name'].'</td>
	           </tr>
			    <tr>
					<td align="left" style="border-bottom:none;">';
		if($promoLogo != ''){ 
			$mailerMessage .= '<a href="'.DOMAIN_PATH.$promoLink.'?id='.$promoId.'"><img src="'.$promoLogoImage.'" border="0" alt="'.$promoName.'" /></a>';
		} 
			$mailerMessage .= '</td>
	           </tr>
              <tr><td style="font-size:11px;color:#C02D2D;border-bottom:none;">'.$pShippingMsg.'</td></tr>
            </table>

        		 </p>
                </td>
                <td>'.$param[$i]['quantity'].'</td>
				<td align="right">';

		if($promotionId == 0 && $clearanceId == 0 && $isBundleId == 0 ){
            		$mailerMessage .= '<img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($param[$i]['price']);
            	}else{ 
            		$mailerMessage .= '<strike><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($param[$i]['onlinePrice']).'</strike><br><label style="color:#b90000"><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($param[$i]['price']).'</label>';	
            	}
					$mailerMessage .= '</td>
                    <td align="right"><b><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($param[$i]["price"]*$param[$i]["quantity"]).'</b></td>
                 </tr>';
                 $total_courier_charges=$param[$i]['total_courier_charges'];
		
		}//for close

 
		$subTotal=0;
		for ($j=0; $j <count($param) ; $j++) { 
		 	$subTotalold+=$param[$j]['total'];
		 } 
		 
		$mailerMessage .= '<tr align="center" style="font-weight:bold;">
			<td align="right" colspan="5">Sub Total : </td>
			<td align="right"><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($subTotalold).'</td>
		 </tr>
		 <tr align="center" style="font-weight:bold;">
			<td align="right" colspan="5">Total Shipping Cost : </td>
			<td align="right"><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/>'.$this->ci->libgetprice->priceFormat($total_courier_charges).'</td>
		 </tr>';
		
		if($bookDiscountTotalPrice != 0 && $bookDiscountTotalPriceFlag == 1){ 		

		$mailerMessage .= '
		<tr align="center" style="font-weight:bold;">
                    <td align="right" colspan="5">10% discount on books – Books offer :<br /><label style="font-size:11px;">*only applicable on non-discounted books</label></td>
                    <td align="right">- <img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($bookDiscountTotalPrice).'</td>
                 </tr>';
			} 
		 
		 $subTotalnew=$subTotalold+$total_courier_charges;
		$mailerMessage .= '
                 <tr align="center" style="font-weight:bold;">
                    <td align="right" colspan="5">Total Amount : </td>
                    <td align="right"><img src="'.SITEIMAGES.'emailer_images/rupee.jpg" alt="Rs."/> '.$this->ci->libgetprice->priceFormat($subTotalnew).'</td>
                 </tr>        
	          </table>
							 </td>
						  </tr></table>';
						  

	//if($shippingDetailsArr['formDetails'] != ''){ 
	/*if($param[0]['address_1'] != ''){ 
        $mailerMessage .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td style="padding-bottom: 20px;padding-left: 5px;color: #464f4b;font-family: Arial;font-size: 12px;"><b>Form Required : '.$param[0]['address_1'].'</b></td>
          </tr>
        </table>';
	}*/

						  
		/*$mailerMessage .= '<tr><td align="left" colspan= "2" style="padding-left:5px;font-size: 12px;"><b>Please Note:</b></br></br></td></tr>
		                    <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders placed with Credit Card option will be charged only when your Order is shipped. </br></br></td></tr>
		                   <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders placed with Cheque/DD option would be processed only after realisation.</br></br></td></tr>
						   <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;"> Orders placed with NEFT / RTGS option would be processed after the amount is credited to Furtados Music India Pvt. Ltd. Account.</br></br></td></tr>
						   <tr><td align="left" colspan= "2" style=" padding-top:5px;padding-left:5px;  color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders including items "Available on Special Order" would be notified through mail if delays are being caused beyond Processing + Shipping time.</br></br></td></tr>
						   <tr><td align="left" colspan= "2" style="padding-top:5px; padding-right: 20px; padding-left: 5px;color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;"> Certain Indian States require Road Permit  Forms/ Declaration/ Affidavit or ID proofs to be supplied by the Buyer to ship productsto their location eg. High Value products, Guitars, Keyboards, Cymbals, Drums, etc. (Dimensions or Weight). </br></br></td></tr>
		                 <tr><td align="left" colspan= "2" style="padding-top:5px; padding-left: 5px;color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Prices are subject to change.</br></br></td></tr>
						 <tr><td align="left" colspan= "2" style="padding-top:5px; padding-left: 5px;color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">For Debit card payments your account is immediately debited when placing your order. However, we receive your money in our bank account only once the order is shipped.</td></tr>
						  <tr><td align="left" colspan= "2" style="padding-top:10px; padding-left:5px; color: #464f4b; font-family:Arial; font-size:12px; background-color:#fff; font-weight:normal;"><a href="'.SITEMOBURL.'shippingPolicy.php">Click here for  Shipping Policy</a></td></tr>
						  <tr><td align="left" colspan= "2" style="padding-top:10px; padding-left:5px; padding-bottom:10px; color:#464f4b; font-family: Arial; font-size:12px; background-color:#fff; font-weight:normal;"><a href="'.SITEMOBURL.'returnPolicy.php">Click here for Refund Policies</a></td></tr>';*/

		$mailerMessage .= '<tr><td align="left" colspan= "2" style="padding-left:5px;font-size: 12px;"><b>Please Note:</b></br></br></td></tr>
		                    <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders placed using a Cheque/ Demand Draft will be processed only after realisation.</br></br></td></tr>
		                   <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders placed using NEFT/ RTGS transactions will be processed only after the amount has been credited to Furtados Music India Pvt. Ltd. bank account.</br></br></td></tr>
						   <tr><td align="left" colspan= "2" style="padding-top:5px;padding-left:5px; color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Orders placed by selecting the “Available on Special Order” category will be notified via mail if the delay is beyond the estimated Shipping time.</br></br></td></tr>
						   <tr><td align="left" colspan= "2" style=" padding-top:5px;padding-left:5px;  color: #464f4b;font-family: Arial;font-size: 12px;background-color: #fff;font-weight:normal;">Prices are subject to change.</br></br></td></tr>
						  <tr><td align="left" colspan= "2" style="padding-top:10px; padding-left:5px; color: #464f4b; font-family:Arial; font-size:12px; background-color:#fff; font-weight:normal;"><a href="'.SITEMOBURL.'shippingPolicy.php">Click here for  Shipping Policy</a></td></tr>
						  <tr><td align="left" colspan= "2" style="padding-top:10px; padding-left:5px; padding-bottom:10px; color:#464f4b; font-family: Arial; font-size:12px; background-color:#fff; font-weight:normal;"><a href="'.SITEMOBURL.'returnPolicy.php">Click here for Refund Policies</a></td></tr>';
                       
		 	 //$mailerMessage .= $MAILER_FOOTER_800; //exit(); 
			//echo $mailerMessage; exit();
			
			$from = array(WEB_SENDER => DOMAIN_NAME);
			//$to = array(GENERIC_EMAIL  => DOMAIN_NAME, $emailId =>$customerName,' weborder@furtadosonline.com'  => DOMAIN_NAME);
			$to = array($param[0]['customer_email']=>$param[0]['customer_name'], 'weborder@furtadosonline.com'=>DOMAIN_NAME, 'response@furtadosonline.com'=>DOMAIN_NAME);			
			
			//$subject = "Details of your order  on ".DOMAIN_NAME." Order No - ".$orderNumber;  
			//$subject = "Details of your order  on ".DOMAIN_NAME." Order No - ".$param[0]['orderId'];  
			$subject="Your ".DOMAIN_NAME." Order No ".$param[0]['orderId']." has been confirmed.";
			//require_once "lib/swift_required.php";
			
			/*$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
			$transport->setUsername($mailUser);
			$transport->setPassword($mailPass);
			$swift = Swift_Mailer::newInstance($transport);
			
			$message = new Swift_Message($subject);
			$message->setFrom($from);
			$message->setBody($mailerMessage, 'text/html');
			$message->setTo($to);
			//$message->setBcc(array('allmails@furtadosonline.com')); commented by Sandeep on 16-5-16. Will be uncommented as soon as new Email pai is integrated
			$message->addPart($text, 'text/plain');*/
			//echo $mailerMessage;die;
			$emailobj=new Falconide();
			$recipients=$emailobj->sendmail($param[0]['customer_email'], $subject, $mailerMessage);
			date_default_timezone_set('Asia/Kolkata');
			error_log("\n ".date('Y-m-d H:i:s')."Sending Order confirmed mail-".print_r($recipients,1),3,'/webapps/furtadosonline/logs/my-errors.log');  
			#print 'Customer emaivl '.$param[0]['customer_email'];
			
			#echo 'Mail 1 '.$recipients;
			if(!empty($param[0]['customer_email'])){
				$emailobj1=new Falconide();
				$recipients1=$emailobj1->sendmail('weborder@furtadosonline.com', $subject, $mailerMessage);

				#echo 'Mail 2 '.$recipients1;
				
				$emailobj2=new Falconide();
				$recipients2=$emailobj2->sendmail('response@furtadosonline.com', $subject, $mailerMessage);

				$emailobj3=new Falconide();
				$recipients3=$emailobj3->sendmail('orders@furtadosmusic.com', $subject, $mailerMessage);
			}
			#echo 'Mail 3 '.$recipients2;

		   	#exit;
		   
	   }
}
?>