<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Furtados</title>
    <!-- Bootstrap core CSS -->
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../mobile-site/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="../mobile-site/css/font-awesome.css">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <link href="../mobile-site/css/bootstrap.min.css" rel="stylesheet">
     <!--<link rel="stylesheet" type="text/css" href="responsive.css">-->
    <link rel="stylesheet" type="text/css" href="../mobile-site/css/custom.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--<link rel="stylesheet" href="/maps/documentation/javascript/demos/demos.css">-->
  </head>

  <body>
  <!--<div class="mobile-container">-->
  <!--<div class="">-->
   <div class="100per">
    <!-- header starts -->
     <header>
     <div class="container">
     <!-- left section starts -->
      <div class="col-xs-12 col-sm-12 col-md-12 pl0">
        <a class="tex pull-left" style="margin-left:0;">Store Locator</a>
        <a href="#" class="pull-right"><img src="../mobile-site/images/cross.jpg" class="img-responsive" alt=""></a>
      </div>
     <!-- left section finish -->
       </div>
     </header>