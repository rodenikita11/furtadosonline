<?php //print '<pre>'; print __DIR__;?>
<div id="emi" class="modal fade">
   <div class="modal-dialog">
      <div class="modal-content bgclr">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <div class="modal-body heightauto">
            <!-- ============================================== according Tab ============================================== -->
            <div class="container wdm">
               <div class="panel-group" id="accordion">
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="1" href="#collapse1" class="whitef panel-title expand" >
                              <div class="right-arrow pull-right">+</div>
                              Axis bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_axis.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_1"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="2" href="#collapse2" class="whitef panel-title expand">
                              <div class="right-arrow pull-right">+</div>
                              ICICI Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_icici.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_2"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="3" href="#collapse3" class="whitef panel-title expand">
                              <div class="right-arrow pull-right">+</div>
                              HSBC Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_hsbc.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_3"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="4" href="#collapse4" class="whitef panel-title expand">
                              <div class="right-arrow pull-right">+</div>
                              Kotak Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_kotak.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_4"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="5" href="#collapse5" class="whitef panel-title expand">
                              <div class="right-arrow pull-right">+</div>
                              Indusind Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_indulnd.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_5"></div>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading bg28">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="6" href="#collapse6" class="whitef panel-title expand">
                              <div class="right-arrow pull-right">+</div>
                              HDFC Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_hdfc.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_6"></div>
                        </div>
                     </div>
                  </div>
                        <!---sbi-->
                         <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="7" href="#collapse7" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              SBI Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse7" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_sbi.png&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_7"></div>
                        </div>
                     </div>
                  </div>

                  <!--end sbi-->
                  <!---Yes Bank-->
                         <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="8" href="#collapse8" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              Yes Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse8" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_yesbank.png&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_8"></div>
                        </div>
                     </div>
                  </div>

                  <!--end yes bank-->
                  <!---Rbl Bank-->
                         <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="9" href="#collapse9" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              RBL Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse9" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_rblbank.png&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_9"></div>
                        </div>
                     </div>
                  </div>

                  <!--end RBL bank-->

                        <!---Amex Bank-->
                         <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="10" href="#collapse10" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              American Express CC
                           </h4>
                        </a>
                     </div>
                     <div id="collapse10" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_amex.png&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_10"></div>
                        </div>
                     </div>
                  </div>

                  <!--end Amex bank-->

                        <!---Standard Charted Bank-->
                         <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="11" href="#collapse11" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              Standard Charted Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse11" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_standardcharted.jpeg&#9;" border="0" style="margin-bottom: 10px;">
                           <div id="emidiv_11"></div>
                        </div>
                     </div>
                  </div>

                  <!--end Standard Charted bank-->

                  <!---Bank Of Baroda-->
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="12" href="#collapse12" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              Bank Of Baroda
                           </h4>
                        </a>
                     </div>
                     <div id="collapse12" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_Bank_Of_Baroda.png&#9;" border="0" style="margin-bottom: 10px; width: 90px;">
                           <div id="emidiv_12"></div>
                        </div>
                     </div>
                  </div>
                  <!--end Bank Of Baroda-->
				  <!---CITI Bank-->
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="13" href="#collapse13" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              CITI Bank
                           </h4>
                        </a>
                     </div>
                     <div id="collapse13" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_citibank.png" border="0" style="margin-bottom: 10px; width: 90px;">
                           <div id="emidiv_13"></div>
                        </div>
                     </div>
                  </div>
                  <!--end CITI Bank-->
				  <!---HDFC Debit Card-->
                  <div class="panel panel-default">
                     <div class="panel-heading" style="background:#cc0028">
                        <a href="#">
                           <h4 data-toggle="collapse" data-parent="#accordion" onclick="bankemi(this)" data-bankid="14" href="#collapse14" class="panel-title expand" style="color: #ffffff;">
                              <div class="right-arrow pull-right">+</div>
                              HDFC Debit Card
                           </h4>
                        </a>
                     </div>
                     <div id="collapse14" class="panel-collapse collapse">
                        <div class="panel-body">
                           <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_hdfc.jpg&#9;" border="0" class="mb10">
                           <div id="emidiv_14"></div>
                        </div>
                     </div>
                  </div>
                  <!--end HDFC Debit Card-->
				  
                 <!-- <div class="PT5"><img src="<?php echo SITEURL; ?>upload/images/emiimages/bank_strip.jpg" alt="Bank">
				  <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_citibank.png" style="height: 40px;">
				  </div>
                  <div class="PT5">
                     <img src="<?php echo SITEURL; ?>upload/images/emiimages/bank_strip_2.jpg" alt="Bank">
                     <img src="<?php echo SITEURL; ?>upload/images/emiimages/EMI_Bank_Of_Baroda.png&#9;" style="height: 29px;">
                  </div>-->
                  <div class="PT5 g_12">
                     <ul>
                        <!--<li class="slectemi">Select EMI options from 12 banks- available for online purchase.</li>A processing fee may be charged for availing EMI payment option.-->
                        <li class="slectemi">EMI amount is calculated on total value of the order at the time of payment.</li>
                        <li class="slectemi">For availing the EMI payment option the Minimum Order Value is Rs.5,000/-.</li>
                        <li class="slectemi">A processing fee may be charged by your bank for availing EMI option.</li>
                        <li class="slectemi">In an instance of refund in an EMI transaction, interest already billed in a particular transaction will not be refundable under any circumstances.</li>
                        <li class="slectemi">EMI options are only available on Credit Cards</li>
                     </ul>
                  </div>
               </div>
            </div>
            <!-- ============================================== according Tab ============================================== -->
         </div>
      </div>
   </div>
</div>
<style type="text/css">
   .bgclr{background-color: #f2f3f5;}.heightauto{height: auto;}.wdm{width: 90%;margin-top: 33px;}.bg28{background:#cc0028}.whitef{color: #ffffff;}.mb10{margin-bottom: 10px;}.slectemi{margin-left: 15px;list-style-position: initial;list-style-type: disc;}
</style>
<script type="text/javascript">
   function bankemi(ele){
      var bid = jQuery(ele).data('bankid');
      jQuery.ajax({  
        type: "POST",  
        url: "<?php echo SITEMOBURL?>product/productwiseemi",  
        data: 'bank='+bid+'&amount=<?php echo $res['discountPrice']; ?>',  
        success: function(data) {
            $('#emidiv_'+bid).html(data);
               
        }  
      });   
      //alert(bid); return false;
   }
</script>