<!-- find-stores Container -->
<script src="<?php echo SITEJS;?>newwishlist.js" type="text/javascript"></script>
<script src="<?php echo WEBJS;?>bootbox.min.js"></script>  
<div class="find-stores">
   <div class="row">
      <div class="col-xs-12">
         <a href="<?echo SITEMOBURL;?>user/storelisting"><img src="<?php echo SITEIMAGES.'mobimg/find_store.jpg';?>" width="100%"></a>
      </div>
   </div>
</div>
<!-- logo Container -->
<div class="Container container-fluid bg-1 text-center" style="margin-top:10px">
   <div class="row">
      <div class="col-xs-12">
        <div class="col-xs-2"></div>
         <div class="col-xs-4">
            <a href="https://fsmbuddy.com/" target="_blank">
            <img src="<?php echo SITEIMAGES.'mobimg/icon/FSMB1.jpg';?>" width="90%" >
            </a>
         </div>
         <div class="col-xs-4">
            <a href="http://www.furtadosschoolofmusic.com/" target="_blank">
            <img src="<?php echo SITEIMAGES.'mobimg/icon/fsm1.jpg';?>" width="100%">
            </a>
         </div>
      </div>
      <div class="col-xs-2"></div>
   </div>
</div>
<!-- blog img 
<div class="Container container-fluid blog">
   <div class="row">
      <div class="col-xs-12 ">
         <a href="https://www.furtadosonline.com/blog/" target="_blank">
         <img src="<?php echo SITEIMAGES.'mobimg/icon/blog.jpg';?>" width="100%">
         </a>
      </div>
   </div>
</div>-->
<!-- music img -->
<div class="container pdl-pdr">
   <div class="col-md-12 bg-one">
      <div class="content-grid-text" style="padding-bottom: 10px !important;">
         <!-- <h3 style="margin-left: 26px">Customer Service <a href="tel:02242875050" style="color:#fff !important;">022 - 42875050 / 60</a></h3>
          <h3 style="margin-left: 18px: padding-top:2px">Books Enquiry <a href="tel:02266225423" style="color:#fff !important;">022 - 66225423</a></h3> -->
         <!-- <h3 style="margin-left: 26px">Customer Service <a href="tel:+91 91520 04939" style="color:#fff !important;">+91 91520 04939</a></h3> -->
         <h3 style="margin-left: 26px;padding-top:2px; display: none;">Books Enquiry <a href="tel:+91 91520 04939" style="color:#fff !important;">+91 91520 04939</a></h3>
          <h3 style="margin-left: 26px; padding-top:2px;">Customer Service 
				<a href="tel: +91 91520 04939 " style="color:#fff !important;"> +91 91520 04939</a>
				<a href="tel: 022 42875060" style="color:#fff !important;">| +91 (022) 4287 5060/5000</a>
				
		  </h3>
		 <!--<h3 style="margin-left: 26px"><a href="tel:+91 93206 44558" style="color:#fff !important;">+91 93206 44558</a></h3>-->
          <h5 style="color:#fff !important;margin-left: 26px; padding-top:2px;">Mon - Sat : 10.00 AM to 5.30 PM </h5>
         <p> <a href="mailto:response@furtadosonline.com " target="_top" style="color:#fff !important;">response@furtadosonline.com </a> </p>
         <div class=" ser-grid">    
            <a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-headphones"> </a>
         </div>
      </div>
   </div>
   <div class="clearfix"> </div>
</div>
<!------------text--------------->
<!--<div class="container form container-fluid">
   <div class="row">
      <div class="col-xs-12 ">
         <h3>Get Regular Updates By Email.</h3>
        
         <div class="header-search">
            <form action="#" id="newsletterform" method="post">
               <input type="search" name="email" placeholder="Enter Email ID" required="" id="email">
               <button type="submit" class="btn btn-default" aria-label="Left Align" style="background: #cd0029 ! important; ">
                  <p class="go"> Go</p>
               </button>
            </form>
         </div>
      </div>
   </div>
</div>-->
<div class="stripbanner">
	<div class="row">
		<div class="col-xs-12">
			<a href="https://forms.zohopublic.com/furtados/form/BasicSignup/formperma/CLnzQosVA-gZWsqO21vyeY5pLMF6RClDyjARx61gq1s" target="_blank">
				<img width="100%" src="<?php echo WEBIMG;?>banners/newsletter_bar.jpg">
			</a>
		</div>
	</div>
</div>
<!-------end------------->
<!-- Follow us on Container -->
<div class="container-fluid text-center">
   <!--removed by sandesh bg-2-->
   <div class="row">
      <div class="col-xs-12 follow-us" style="padding-right: 18px; !important">
         <h3>Stay Connected  </h3>
      </div>
   </div>
</div>
<!-- Follow us on icon -->
<div class="container container-fluid">
   <div class="row">
    <div class="col-xs-3"></div>
      <div class="col-xs-6" style="padding-left:0px;margin-left: 2px;">
         <div class="col-xs-2 social"><a href="https://www.facebook.com/FurtadosMusic" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/fb.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2 social"><a href="https://www.youtube.com/user/furtadosmusicindia" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/vi.jpg';?>" width="100%"></a></div>
		 <div class="col-xs-2 social"><a href="https://twitter.com/furtados" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/tw.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2  social"><a href="https://www.instagram.com/furtadosmusic" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/ins.jpg';?>" width="100%"></a></div>
         <!-- <div class="col-xs-2  social"><a href="https://plus.google.com/u/0/+furtadosmusicindia" target="_blank"><img src="<?php echo SITEIMAGES; ?>mobimg/icon/google-plus.png" width="100%"></a></div> -->
         <div class="col-xs-2 social"></div>
      </div>
      <div class="col-xs-3"></div>
   </div>
</div>
<!-- about us  Container -->
<div class="Container container-fluid about">
   <div class="row">
      <div class="col-xs-12 ">
		 <div class="col-xs-2">
            <h4 class="fotrhead">OFFER</h4>
			<!--<p><a class="footabout" title="Clearance" href="<?php echo SITEMOBURL?>product/getcleranceproduct" title="Clearance">Clearance</a></p>-->
            <p><a class="footabout" title="Offer Zone" href="<?php echo SITEMOBURL?>product/offerzone" title="Offer Zone">Offer Zone</a></p>
         </div>
         <div class="col-xs-4 about-bottom">
            <h4 class="fotrhead">Information </h4>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>aboutus">About us</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>contactus" title="Contact Us">Contact Us </a></p>
			<p><a class="footabout" href="<?php echo SITEMOBURL?>job">Careers</a></p>
            <!--<p><a class="footabout" href="<?echo SITEMOBURL;?>feedback">Feedback</a></p>-->
            <p><a class="footabout" href="<?php echo SITEMOBURL?>pianoenquiry" title="Piano Enquiry">Piano Enquiry</a></p>
         </div>
         <div class="col-xs-3 furtados">
          <h4 class="fotrhead">POLICIES</h4>
			<p><a class="footabout" href="<?php echo SITEMOBURL?>shippingpolicy" title="Shipping Policy">Shipping Policy</a></p>
			<p><a class="footabout" href="<?php echo SITEMOBURL?>returnpolicy" title="Return Policy ">Return Policy</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>privacypolicy" title="Privacy Policy ">Privacy Policy</a></p>
			<p><a class="footabout" href="<?php echo SITEMOBURL?>assuranceprogram" title="Assurance Program">Assurance Program</a></p>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>termsandcondition">Terms and Conditions</a></p>
         </div>
		 <div class="col-xs-3">
           <h4 class="fotrhead">OTHER PORTALS</h4>
           <!--<p><a class="footabout" href="http://furtadosartists.com/">Furtados Artists</a></p>-->
		   <p><a class="footabout" href="http://www.highfurtados.com/">High Furtados</a></p>
           <p><a class="footabout" title="Furtados School Of Music" href="http://www.furtadosschoolofmusic.com/" title="Furtados School Of Music">Furtados School Of Music</a></p>
		   <p><a class="footabout" href="https://www.furtadosonline.com/blog/" title="Blog">Blog</a></p>
         </div>
      </div>
   </div>
</div>
<div class="accordion" id="accordionCategories">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn collapsed" style="width: 100%;" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
          All Categories
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionCategories">
      <div class="card-body">
               <div class="Container container-fluid about">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <br>
                        <a class="maincat" href="#">MUSICAL INSTRUMENT</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>pianos/212">Piano</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>acoustic-pianos/1106">Acoustic Pianos</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>digital-pianos/1107">Digital Pianos</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>piano-accessories/1128">Piano Accessories</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>guitars-bass/206">Guitars &amp; Bass</a>	
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-accessories/1078">Guitar Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-amps/1079">Guitar Amps</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-cables/1080">Guitar Cables</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-pick-ups/1081">Guitar Pick-ups</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>guitar-stands/1082">Guitar Stands</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>pedals-processors/1083">Pedals &amp; Processors</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>banjos-ukeleles/1085">Banjos &amp; Ukeleles</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>basses/1086">Basses</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>classical-guitars/1087">Classical Guitars</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>electric-guitars/1088">Electric Guitars</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>keyboards-dmi/209">Keyborad &amp; Dmi</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>keyboard-accessories/1100">Keyboard Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>keyboard-amps/1101">Keyboard Amps</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>keyboards/1102">Keyboard</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>modules/1103">Modules</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>synths-etc/1104">Synths, Etc</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>organs/1129">Organs</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>drums-percussion/202">Drums &amp; Percussion</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>cymbals/1062">Cymbals</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>digital-drums/1063">Digital Drums</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>drum-accessories/1064">Drums Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>drum-hardware/1065">Drums Hardware</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>drum-heads/1066">Drum Heads</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>drum-sets/1067">Drum Sets</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>drum-sticks/1068">Drum Sticks</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>percussion/1069">Percussion</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>marching-percussion/1131">Marching Percussion</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>tuned-percussion/1132">Tuned Percussion</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>product/promotion?id=456#dropdown-EDUCATION">Education</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>education-accessories/1071">Education Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>education-instruments/1072">Education Instrument</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>metronomes/1074">Metronomes</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>notation-stands/1075">Notation Stands</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>pitch-pipes-tuners/1076">Pitch Pipes &amp; Tuners</a>

                        <a class="subcat" href="<?php echo SITEMOBURL?>bowed-strings/214">Bowed Strings</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>cello-bass-accessories/1113">Cello &amp; Bass Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>cellos-double-bass/1115">Cellos &amp; Double Bass</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>viola-accessories/1116">Viola Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>viola/1117">Viola</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>violin-accessories/1118">Violin Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>violins/1119">Violins</a>

                        <a class="subcat" href="<?php echo SITEMOBURL?>wind-instruments/216">Wind Instruments</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>brass/1124">Brass</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>brass-accessories/1125">Brass Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind/1126">Woodwind</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind-accessories/1127">Woodwind Accessories</a>

                        
                        <a class="subcat" href="<?php echo SITEMOBURL?>harmonicas-accordions/215">Harmonics &amp; Accordions</a>	
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>accordions/1121">Accordions</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>harmonicas/1122">Harmonicas</a>

                        <a class="subcat" href="<?php echo SITEMOBURL?>indian-instruments/208">Indian Instrument</a>	
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>electronic-indian-instrument/1092">Electronics Indian Instrument</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>harmoniums/1093">Harmoniums</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>indian-instrument-accessories/1094">Indian Instrument Accessories</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>indian-percussion/1095">Indian Percussion</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>indian-string-instruments/1096">Indian String Instruments</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>indian-wind-instruments/1097">Indian Wind Instruments</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>tablas/1098">Tablas</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>computer-music/201">Computer Music</a>
                     <!--	<a class="subcat" href="<?php echo SITEMOBURL?>religious/301">Religious</a>-->
                        <a class="subcat" href="<?php echo SITEMOBURL?>instrument-bundles/219">Instrument Bundles</a>	
                        <a class="subcat" href="<?php echo SITEMOBURL?>audio-cds/218">Audio CDs</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>musician-essentials/217">Musician Essentials</a>
                        <!--<a class="subcat" href="<?php echo SITEMOBURL?>music-gifts/205">Music Gifts</a>-->
                        <a class="subcat" href="<?php echo SITEMOBURL?>other-fretted-instruments/207">Other Fretted Instruments</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>mandolins-accessories/1090">Mandolins &amp; Accessories</a>
                           <br><br>
                           
                        <!--musical category finish-->

                     <a class="maincat" href="#">MUSICAL BOOKS</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>miscellaneous-books/111">Miscellaneous Books</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>music-magazines/112">Music Magazines</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>repertoire-albums/113">Repertoire &amp; Albums</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>classical/1049">Classical</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>collections-folios/1050">Collection &amp; Follos</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>movie-stage/1051">Movie &amp; Stage</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>personalities-groups/1052">Personalities &amp; Groups</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>voice/114">Voice</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>choral0/1054">Choral0</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>voice-technique/1055">Voice Technique</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>voice-tutors/1056">Voice Tutors</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>wind/115">Wind</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>brass/1058">Brass</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>harmonica/1059">Harmonica</a>
                           <a class="subsubcat" href="<?php echo SITEMOBURL?>woodwind/1060">Woodwind</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>keyboard/110">Keyword</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>indian-instruments/109">Indian Instruments</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>bowed-strings/101">Bowed Strings</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>drums-percussion/102">Drums &amp; Percussion</a>	




                        <!--musical category finish-->
                        <br><br>
                           

                        <a class="maincat" href="#">PRO AUDIO</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>amplifiers/401">Amplifiers</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>speakers/402">Speakers</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>software/403">Software</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>stage-monitors/404">Stage Monitors</a>
                     <!--	<a class="subcat" href="<?php echo SITEMOBURL?>apple-products/416">Apple Products</a>-->
                        <a class="subcat" href="<?php echo SITEMOBURL?>dj/417">Dj</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>pro-audio-bundles/418">Pro-audio Bundles</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>live-sound/419">Live Sounds</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>home-studio/420">Home Studio</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>home-sound/415">Home Sound</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>portable-recording-devices/414">Portable Recording Devices</a>
                        <a class="subcat" href="<?php echo SITEMOBURL?>desktop-monitors/405">Desktop Monitors</a>


                        <!--musical category finish-->

                     <br><br>
                        <!--<a class="maincat" href="javascript:void(0)">Brands</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>yamaha/136'>Yamaha</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>korg/69'>Korg</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>hercules/56'>hercules</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>casio/19'>casio</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>planet-waves/92'>planet waves</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>laney/73'>laney</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>vOX/130'>VOX</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>gibson/47'>Gibson</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>epiphone/36'>Epiphone</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>granada/53'>Granada</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>java/64'>Java</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>prs/96'>PRS</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>fender/42'>Fender</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>hohner/61'>Hohner</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>boss/16'>Boss</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>levys/138'>Levys</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>cort/11508'>Cort</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>mighty-bright/150'>Mighty Bright</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>roland/107'>Roland</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>fishman/43'>Fishman</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>mapex/80'>mapex</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>sennheiser/157'>Sennheiser</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>nux/51336'>NUX</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>akg/3'>AKG</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>alhambra/4'>Alhambra</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>aubert/7'>Aubert</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>b-c-rich/8'>B C Rich</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>behringer/11'>Behringer</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>belcat/12'>Belcat</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>bespeco/13'>Bespeco</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>beyerdynamic/15'>Beyerdynamic</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>boston/17'>Boston</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>cakewalk/18'>Cakewalk</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>cherub/22'>Cherub</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>eMG/145'>EMG</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>ernie-ball/37'>Ernie Ball</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>essex/40'>Essex</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>evans/41'>Evans</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>studio-master/1'>Studio master</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>b-steiner/9'>B steiner</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>behringer/11'>Behringer</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>belcat/12'>Belcat</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>beyerdynamic/15'>Beyerdynamic</a>
                     <a class="subcat" href='<?php echo SITEMOBURL; ?>beyerdynamic/15'>Beyerdynamic</a>
                     <div class="brandlist" style="word-break: break-all;"></div>-->
                  </div>
               </div>

            </div>
     
      </div><!-- card body ends -->
    </div>
  </div>
</div>

<div class="accordion" id="accordionBrands">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn collapsed" style="width: 100%;" type="button" data-toggle="collapse" data-target="#collapseBrands" aria-expanded="false" aria-controls="collapseBrands">
          All Brands
        </button>
      </h5>
    </div>

    <div id="collapseBrands" class="collapse" aria-labelledby="headingOne" data-parent="#accordionBrands" style="height: 0px;" aria-expanded="false">
      <div class="card-body">
               <div class="Container container-fluid about">
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                       <br>
                     <div class="brandlist" style="word-break: break-all;"></div>
                  </div>
               </div>

            </div>
     
      </div><!-- card body ends -->
    </div>
  </div>
</div>
<!-- Footer -->
<footer class="bg-4 text-center footer-copy">
   <p style="font-size: 12px">Copyright @ <?php echo date('Y'); ?> Furtadosonline.com</p>
    <?require_once (__DIR__ .'/emi.php');?>
</footer>
</body>
</html>
<style type="text/css">label#email-error {color: #cd0029;font-size: 12px;position: relative;right: 114px;}</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16232700-1"></script>
<script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());
   
   gtag('config', 'UA-16232700-1');
</script> 
<script type="text/javascript">
jQuery(document).ready(function(){
   jQuery.ajax({
          url:'<?php echo SITEMOBURL?>product/getbrands',
          dataType:'JSON'
      }).done(function(data){
          str='<a class="maincat paddingrightclass" href="javascript:void(0)">Brands</a>';
          for(var i=0; i < data.length; i++){
              if(data[i]['brandname'].substr(0, 2)!='DB'){
                  var brandname=data[i]['brandname'].toLowerCase().replace(/[&\s|']/mig, '-').replace(/---/gmi, '-');  
              }else{
                  var brandname=data[i]['brandname'].replace(/[&\s|']/mig, '-').replace(/---/gmi, '-');  
              }
              
              str+='<a class="subcat paddingrightclass" href="<?php echo SITEMOBURL; ?>'+brandname+'/'+data[i]['brandid']+'">'+data[i]['brandname']+'</a>';
          }
          jQuery('.brandlist').html(str);
      })
});

   jQuery('#newsletterform').validate({
       rules:{
           email:{
               email:true,
               required:true
           }
       },
       messages:{
           email:{
               email:'Please Enter valid Email ID',
               required:'Please Enter Email ID'
           }
       },
       submitHandler:function(form){
           var valid=jQuery('#newsletterform').valid();
           
           var email=jQuery('#email').val();
           if(valid){
               jQuery.ajax({
                   url:'<?php echo SITEMOBURL?>user/newslettersubmit',
                   type:'POST',
                   dataType:'JSON',
                   data:{email:email},
                   success:function(result){
                     res=eval(result);
                        if(res.status== true)
                        {
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val('');
                              window.location.reload();  
                            });
                            
                        }else{
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val();  
                            });
                            
                        }
                   }
               });
           }
       }
   });
</script>
<script>
   jQuery(document).ready(function(){
      jQuery.ajax({
            url:'<?php echo SITEMOBURL?>product/getbrands',
            dataType:'JSON'
         }).done(function(data){
            str='<a class="maincat paddingrightclass" href="javascript:void(0)">Brands</a>';
            for(var i=0; i < data.length; i++){
               if(data[i]['brandname'].substr(0, 2)!='DB'){
                     var brandname=data[i]['brandname'].toLowerCase().replace(/[&\s|']/mig, '-').replace(/---/gmi, '-');  
               }else{
                     var brandname=data[i]['brandname'].replace(/[&\s|']/mig, '-').replace(/---/gmi, '-');  
               }
               
               str+='<a class="subcat paddingrightclass" href="<?php echo SITEMOBURL; ?>'+brandname+'/'+data[i]['brandid']+'">'+data[i]['brandname']+'</a>';
            }
            jQuery('.brandlist').html(str);
         })
   });

   jQuery(document).scroll(function(){
    curscrollpos=jQuery(document).scrollTop();
    if(curscrollpos < 10){
        jQuery('#stickyheader').hide();
        jQuery('#mainheader').show();
		jQuery('#pfilters').attr('style','');
        return false;
    }else{
        jQuery('#mainheader').hide();
        jQuery('#stickyheader').show();
		jQuery('#pfilters').attr('style','position: fixed;z-index: 999 !important;bottom: 0px;');
    }
   })
   function addtowishlist(ele, wishlistobj=false ){
  jQuery('.catallpro').unbind('click');
  pid=ele;
  addtowishlist=jQuery(wishlistobj).data('addtowishlist');
  jQuery.ajax({
    url:'<?php echo SITEMOBURL?>user/wishlist',
    type:'POST',
    data:{pid:pid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
           // bootbox.alert('Product successfully added to wishlisht', function(){
            // window.location.reload();
           // });
		   bootbox.alert({
				message: 'Product successfully added to wishlisht',
				callback: function(){ window.location.href="<?php echo SITEMOBURL?>user/getwishlist"; },
				buttons: {
				  ok: {
					label: 'Go To Wishlisht'
				  }
				}
			 });
           if(addtowishlist){
           removefromcart(pid, addtowishlist);
         }
          // window.location.reload();
            ajax_getCartCount();
           ajax_wishlistcount();
        }else if(response.login=='false'){
           // bootbox.alert('You need to login to create Shortlist',  function(){
              // window.location.href="<?php echo SITEMOBURL?>user";
           // });
		   bootbox.alert({
				message: 'You need to login to create wishlisht',
				callback: function(){ window.location.href="<?php echo SITEMOBURL?>user"; },
				buttons: {
				  ok: {
					label: 'Login'
				  }
				}
			 });
        }else{
         bootbox.alert('Product Already In wishlisht');

        }

    }

  });
}
/**
 * Complete filterall package below
 */
var moduleid='<?php echo $_GET['id'];?>';
var offer = '<?php echo $this->uri->segment(2);?>';
var promourl=window.location.href;
var promourl=window.location.href.split('?')[0];

//filter left sidebar and sort
function filterall(type=''){
   let maincat = '';
    if(getUrlParameter('maincat')){
      maincat = '&maincat=' + getUrlParameter('maincat');
    }
    var searchtext='<?php echo urlencode($_GET['searchText'])?>';
    var appendquerystr='';
    if(searchtext){
        appendquerystr='?searchText='+searchtext;
    }else{
      productsearch='';
    }
    let search_string = '&searchText='+searchtext;
    //console.log(appendquerystr); return false;
     curURI=window.location.pathname+appendquerystr;
     //console.log(appendquerystr); return false;
     curURI='<?php echo ROOTURL; ?>'+appendquerystr;
      //alert(curURI);
      //console.log(curURI); return false;
    curURI=window.location.href;
    
    let categoryid = '';
     if(getUrlParameter('catid')){
        categoryid = '&catid='+getUrlParameter('catid');
     }
     var jstr='';
     var jstr1='';
     jQuery(".attribute:checked").each(function(){
       if(jstr==''){
         jstr += '[' + jQuery(this).val();
       }else{
         jstr += ',' + jQuery(this).val();
       }
     });
     if(type == 'brand' || getUrlParameter('brand')){
        jQuery(".attributebrand:checked").each(function() {
          if(jQuery(this).val() != ''){
              if(jstr==''){
                jstr += '&brand=[' + jQuery(this).val();
              }else{
                jstr += ',' + jQuery(this).val();
              }
          }
        });
        // if(jQuery(".attributebrand:checked").val() != undefined && jQuery(".attributebrand:checked").val() != ''){
          // jstr += '&brand=[' + jQuery(".attributebrand:checked").val();
        // }
     }
      jQuery(".attributepercentage:checked").each(function() {
        if( jQuery(this).val() != '' ){
          if(jstr == ''){
            jstr += '&percentage=[' + jQuery(this).val();
          }else{
            jstr += ',' + jQuery(this).val();
          }
        }
      });
      if(type == 'aval' || getUrlParameter('avaliable')){
        jQuery(".attributeavaliable:checked").each(function() {
          if( jQuery(this).val() != '' ){
            if(jstr1==''){
              jstr1 += '&avaliable=[' + jQuery(this).val();
            }else{
              jstr1 += ',' + jQuery(this).val();
            }
          }
        });
      }
     if(jstr!=''){ jstr += ']'; }
     if(jstr1!=''){  jstr1 += ']';}

     let discarr = '';

      if(type == 'discount' || getUrlParameter('discount')){
          let disc=[];

          var min = jQuery( "#slider-rangeDisc" ).slider( "values", 0 );
          var max = jQuery( "#slider-rangeDisc" ).slider( "values", 1 );
          // ------commented this code becuase if min i.e min dis is 0 then it treated as null so jstr is not form ---------//
		  // if(min != '' && max != ''){
            // jstr += '&discount='+"["+min+","+max+"]";
          // }
		  if(min !== '' && max !== ''){
			  jstr += '&discount='+"["+min+","+max+"]";
		  }
		  		
          /* if(jQuery('.attrdisc:checked').val() != undefined && jQuery('.attrdisc:checked').val() != ''){
            // get the id as things are tucked up
            let minmax = jQuery('.attrdisc:checked').data('id');
            discarr='&discount='+"["+minmax+"]";
          } */
      }
          // window.location.href=curURI+'?discount='+discarr;
      //});

     //console.log(min+'===='+max);
     //if(min && max){
      if( type != 'clearAll' && (type == 'pricerange' || getUrlParameter('pricestart') || getUrlParameter('pricend')) ){
        var min = jQuery( "#slider-range" ).slider( "values", 0 );
        var max = jQuery( "#slider-range" ).slider( "values", 1 );
        if(min != ''){
          jstr += '&pricestart='+min;
        }
        if(max != ''){
          jstr += '&pricend='+max;
        }
      }
     //}
     <?php //$uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));?>
      //console.log(jstr1); return false;
     var sort = $( "#prosuctsort_opt option:selected").val();
     if(sort==undefined){sort = 'relevence';}
     //console.log('jstr-----'+jstr);
     //console.log('jstr1-----'+jstr1); return false;
     if(offer == 'promotion'){
        //console.log(promourl); return false;
        //console.log(<?php echo $_GET['id'];?>); return false; 
       window.location.href=promourl+'?filter='+jstr+jstr1+"&sort="+sort;
       var currenturl=window.location.href;
       var spliturl=currenturl.split('?');
       var appendstr=(spliturl.length > 1) ? '&' : '?&';
       //console.log(curURI+appendstr+'filter='+jstr+jstr1+"&sort="+sort); return false;
       //window.location=curURI+appendstr+'filter='+jstr+jstr1+"&sort="+sort;
     }else{
      //console.log('here-----'+curURI); return false;
      var splitpath=window.location.pathname.split('/');
      if(false){  //splitpath.length > 3 bypass done
          var pathname=splitpath[0]+'/'+splitpath[1]+'/'+splitpath[2]+'/';
      }else{
          var pathname=window.location.pathname;
      }
      //console.log('search stringgggggg: ');
      //console.log(search_string);
        if(jstr && jstr1){
           //console.log(window.location);
           //window.location.href=curURI+'?filter='+jstr+jstr1+"&sort="+sort;
           window.location.href=window.location.origin+pathname+'?filter='+jstr+jstr1+"&sort="+sort+categoryid+discarr+search_string+maincat;
           }else if(jstr){
            window.location.href=window.location.origin+pathname+'?filter='+jstr+"&sort="+sort+categoryid+discarr+search_string+maincat;
            // window.location.href=curURI+'?filter='+jstr+"&sort="+sort+maincat;
           }else if(jstr1){
             window.location.href=window.location.origin+pathname+'?filter='+jstr1+"&sort="+sort+categoryid+discarr+search_string+maincat;
            // window.location.href=curURI+'?filter='+jstr1+"&sort="+sort+maincat;
           }else if(discarr){
            window.location.href=window.location.origin+pathname+"?filter="+discarr+search_string+maincat;
           }else{
              window.location.href=window.location.origin+pathname+"?sort="+sort+categoryid+discarr+search_string+maincat;
             //window.location.href=curURI+"?sort="+sort;
           }
      }
}

/* header me dala....
    var getUrlParameter = function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}; */
/**
 * Filter all package ends here
 */
</script>
<style type="text/css">
  .modal-dialog { top: 24%;}.modal-footer {background: #f5f5f5;padding: 5px;}
  div#myModaloption .modal-dialog {top: 0;}
   
</style>
