 <link href="<?php echo SITECSS?>ubislider.min.css" rel="stylesheet" type="text/css" media="all">



<div class="w100per slider">
   <div class="customcontainer">
      <p class="w100per">
         <?if($res['wishlist'] ==0){?>
         <a href="#" class="pull-right" onclick="addtowishlist('<?php echo $this->uri->segment(2);?>');"><i class="fa fa-heart-o heartwish"></i></a>
         <?}else{?>
         <a href="#" class="pull-right" onclick="removefromwishlist('<?php echo $this->uri->segment(2);?>');"><i class="fa fa-heart heartwish"></i></a>
         <?}?>
      </p>
      <!-- <?php if($res['discountPrice'] < $res['onlinePrice']){ ?>
      <div class="ofr">
         <div class="ofrtxt">ON OFFER</div>
      </div>
   <?php } ?> -->
      <!-- image slider starts -->
      <!--<div class="w100per image-slider"><img src="images/slider-image.jpg"></div>-->

         <div class="row">
            <div class="clearfix">
				<?php if($res['discountPrice'] < $res['onlinePrice']){ ?>
					<div class=""> 
					  <span class="off" style="color: #fff !important;"><?php echo $res['discountpercentage']; ?>% OFF</span>
				   </div>
				<?php } ?>
                  <div class="ubislider-image-container" data-ubislider="#slider4" id="imageSlider4"></div>
                      <div id="slider4" class="ubislider">
                          <a class="arrow prev"></a>
                          <a class="arrow next"></a>
                          <ul id="gal1" class="ubislider-inner">

                           <?php
                              $i=0;
                              //echo '<pre>';
                              //print_r($res['images']); die;
                              if($res['images']){
                                 foreach($res['images'] as $key=>$val){
                                 if($i < 5){ 
                                 if($val['pro_image']){
                                    if(stripos($val['pro_image'], '_350x350')){
                                          $val['pro_image_new']= str_replace("_350x350", "", stripslashes($val['pro_image']));
                                          $val['pro_image_new']=str_replace("processedimage", "originalimage",  stripcslashes($val['pro_image_new']));
                                    }else if(stripos($val['pro_image'], '_525x400')){
                                       $val['pro_image_new']= str_replace("_525x400", "", stripslashes($val['pro_image']));
                                       $val['pro_image_new']=str_replace("processedimage", "originalimage",  stripcslashes($val['pro_image_new']));
                                    }else{
                                          $val['pro_image_new']=str_replace("_250x190", "", stripslashes($val['pro_image']));
                                    }
                                 }else{
                                    $val['pro_image_new']= SITEIMAGES.'mobimg/noimage1.png';   
                                 }
                                 ?>
                              <li> 
                                 <a> 
                                    <img class="product-v-img" src="<?php if(!isset($val['pro_image'])){ echo SITEIMAGES.'mobimg/noimage1.png';}else{echo stripcslashes($val['pro_image']);}?>"> 
                                 </a> 
                              </li>
                              <?php }
                              $i++;
                                 }
                              } ?>
                          </ul>
                      </div>
                  </div>
            </div>
      

      

      <!-- image slider finish -->
      <div class="_1AtHd0">
         <div class="_1lkA0W">
            <span  data-id="<?php echo $res['name'];?>"><?php echo $res['name'];?></span>

          <!-- rating and reviews section starts -->
            <div class="w100per ratingall" style="padding-bottom: 0px; display: <?php echo ($value['rating'] && $value['rating'] > 0) ? '' : 'none'; ?>">
              <div class="txftdos ml">
               <span class="ratnig">
                  <div class="rates">
                     <div class="ratningbg" style="background-color: rgb(55, 190, 95);">
                        <span><?php echo ($res['overallrating'] && $res['overallrating'] > 0) ? $res['overallrating'] : 0; ?></span>
                        <span>
                        <img class="starbg" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1MTIiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgMzA2IDMwNiI+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTE1MyAyMzAuNzc1bDk0LjM1IDY4Ljg1LTM1LjctMTEyLjIgOTQuMzUtNjYuM0gxOTEuMjVMMTUzIDYuMzc1bC0zOC4yNSAxMTQuNzVIMGw5NC4zNSA2Ni4zLTM1LjcgMTEyLjJ6Ii8+PC9zdmc+">
                        </span>
                     </div>
                     <span class="f8RGEP"><?php echo ($res['revcnt']) ? $res['revcnt'] : 0; ?> reviews</span>
                  </div>
               </span>
               </div>
            </div>
           <!-- rating and reviews section fisnih -->
           <br>
           <!--price -->
            <div class="catdetails">
               <?php if($res['callforprice']=='YES' || $res['third_catid']==1106){ ?>
                  <div class="_1p4t8M">
                     <span class="_1kL5kY">Call for Price</span>
                  </div>
               <?php }else{ ?>
                  <span class="catsubt" style="font-size: 16px;"><i class="fa fa-rupee" style="font-size:16px;margin-right: 2px;"></i><?php echo ($res['discountPrice'] < $res['onlinePrice']) ? $res['discountPrice'] : $res['onlinePrice']; ?></span>
                  <?php if($res['discountPrice'] < $res['onlinePrice']){ ?>
                  <span style="display: inline-block;">   
                     <span class="offamt"><?php echo $res['onlinePrice']; ?></span>
                     <span class="offeroff"><?php echo $res['discountpercentage']; ?>%</span>
                  </span>
               <?php }} ?>
            </div>
           <!--price -->
            <div>
				<?php if($res['callforprice']=='YES' || $res['idMinPrice']==0){ ?>
					 <!--   <b class=" emipro" >   
						   <i class=""> </i>NO EMI AVAILABLE
						   <i class="fa angle-right"></i>
						</b>-->
				 <?}else{?>
					<span class="catsubt"><img class="_3_PqJz _3SH_nI" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMiIgaGVpZ2h0PSIxMiIgdmlld0JveD0iMCAwIDEyIDEyIj4KICAgIDxwYXRoIGZpbGw9IiMxNkJFNDgiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTExLjY0NiA1Ljc0OGwtNS40LTUuNEExLjE5MiAxLjE5MiAwIDAgMCA1LjQgMEgxLjJDLjU0IDAgMCAuNTQgMCAxLjJ2NC4yYzAgLjMzLjEzMi42My4zNTQuODUybDUuNCA1LjRjLjIxNi4yMTYuNTE2LjM0OC44NDYuMzQ4LjMzIDAgLjYzLS4xMzIuODQ2LS4zNTRsNC4yLTQuMkMxMS44NjggNy4yMyAxMiA2LjkzIDEyIDYuNmMwLS4zMy0uMTM4LS42MzYtLjM1NC0uODUyek0yLjYyNSAzLjc1QTEuMTIzIDEuMTIzIDAgMCAxIDEuNSAyLjYyNWMwLS42MjIuNTAyLTEuMTI1IDEuMTI1LTEuMTI1czEuMTI1LjUwMyAxLjEyNSAxLjEyNVMzLjI0OCAzLjc1IDIuNjI1IDMuNzV6Ii8+Cjwvc3ZnPgo=">
						
						<b class=" emipro" id="emi1" data-toggle="modal" data-target="#emi">      
						   <u>EMI from </u><i class="fa fa-rupee" style="font-size: 12px; color: black;"></i><?php echo $this->libsession->priceformat($res['idMinPrice']);?>.</b>
					<!--<i class="fa angle-right"></i>	</b>-->
				 <?}?>
				 <?php if(($res['onlinePrice'] >=5000 || $res['discountPrice']>=5000) && ($res['onlinePrice'] <=500000 || $res['discountPrice']<=500000) && $res['avaliable']==1){ ?>
					</br><b>0% EMI Available</b></span>
				<?php } ?>
             </div>
         </div>
      </div>
      <div style=" margin-left: -5px;margin-right: -5px;border-bottom: 1px solid #eae5e5;border-top: 1px solid #eae5de;padding: 10px 12px;box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 1px 0px;">
         <span data-toggle="modal" data-target="#myModalshae">
            <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1MTIiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiI+PHBhdGggZmlsbD0iI2ExYTFhMSIgZD0iTTUxMiAzMmMwIDY0LTk2IDE5Mi0zMjAgMTkyVjk2TDAgMjg4bDE5MiAxOTJWMzUyYzE3Ni40MzggMCAzMjAtMTQzLjU2MiAzMjAtMzIweiIvPjwvc3ZnPg==" width="17px" class="_1YFjIw">  Share
         </span>
      </div>
     
   </div>
   <!-- delivery section finsih -->
</div>
</div>
<!-- slider finsih -->
<!-- add to bundle section starts -->
<?if($res['callforprice']!='Yes'){?>
<!--<div class="w100per bundle">
   <div class="container">
      <a href="javascript:void(0)" onclick="addtobundle(<?php echo $this->uri->segment(2);?>);">Add to Bundle</a> 
      <span><a href="#">(View in cart)</a></span>
   </div>
   </div>-->
<?}?>
<!-- add to bundle section fisnih -->
<div class="_3nUOC4" style="clear: both;">
   <div>
      <div class="_3IP9uq">
         <span class="_144yPn">
            <img class="_22upO3" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAMAAADW3miqAAAA1VBMVEUAAACMjIyHh4eIiIiIiIiHh4eJiYnAwMCHh4eIiIiIiIiJiYmLi4uNjY2IiIiIiIiIiIiIiIiJiYmIiIiJiYmWlpaIiIiIiIiIiIiJiYmIiIiOjo6IiIiLi4uIiIiHh4f///+oqKjNzc2bm5v8/Pzb29ujo6OJiYny8vK0tLS6urqYmJj6+vq3t7ecnJz4+Pjr6+vm5ubIyMi9vb2vr6+goKCSkpKOjo6Li4v09PTn5+fj4+Pf39/R0dHDw8Orq6vFxcXX19fl5eW/v7+xsbGdnZ2VlZV3GR2gAAAAH3RSTlMANYBy+/ZJA7iokXwlGOvLnZhsXi0J3Yd4aVEk7kAP0phvgAAAAXJJREFUeF7F01dz2kAUQGFkCZkSMGCDa3KuCrX34p76/39SNrvIrALxU2Z8nnRnvtmrGWkzH1P9plgoXPn1d8ht3kOXrZb+ZVwPNq3FYtlW7Py4qUD/aSCqcbiCk2OmCMNYdo1f4MhZFx5DsXohe/heOUYadOmas1YAlL9YpgTNFJIQ3ScLnbOO02h7iE4IJI2kf4Cq/EhQEBvUAuDSdd3aqUGXyUlDGD1oFJGEa9A1vwwazNswHKuHDUlkzdJb+Cam+yXrjshXrM7MwjMCSQpDZUfomtPpM3ClkQ+hWH3H1FE+gOyFVvmUesZC21WysA4sJoZMA2wkTeBaLwRgOZ/NehH7XsM/jZJfh/f6WNQmXfsQrR/lvm+RTVMmkYXuAF4l/dXman60/oSS45R5UugBPju6BjOFJuA7zun+NrRikS6N3Zzn50CkRzlj5UPUa0FthxyPvpop/n1DgcrbXPMA8mqVnVPIVX37zhZylZvM/+43bgdgQRWJVP8AAAAASUVORK5CYII=">
         </span>
         <div class="_1a8Thq"><span><span style="position: relative;" id="deliverto">Deliver to </span><span class="_3wIlNg" id="pincodewrapper"><?php echo ($this->libsession->getSession('pincode')) ? $this->libsession->getSession('pincode') : '<input type="text" placeholder="Enter Pincode" name="newpincdefield" id="newpincdefield" class="form-control" style="width: 75% !important;position:  relative;bottom: 9px;left: 65px;height:  30px;border-radius: 2px; display: none;">'; ?></span><span id="response" style="margin-left: 60px;"></span></span></div>
         <div class="_3GkmWz"><button class="yOYByF" style="padding: 10px 10px 9px; font-size: 12px; line-height: 10px; vertical-align: middle; color: rgb(40, 116, 240);" id="changepin">change</button></div>
      </div>
   </div>
</div>

<div style="box-shadow: rgba(0, 0, 0, 0.3) 0px 1px 1px 0px; margin: 0px 0px 16px;">
   <div>
      <div class="cwz6gx">
         <div class="_3aKFMv"><!--<b>Product:</b>--><span style="color: <?php echo ($res['avaliable']=='1') ? '#37be5f' : '#cd0029'; ?>;"><?php echo ($res['avaliable']=='1') ? 'Available' : 'Not Available' ?></span></div>
         <ul class="_1OF1pc">
            <li>
               <!-- <div class="RyV-EM">Ships in 2 - 3 business days</div> 
			   All products will be shipped after the completion of the lockdown and renewal of shipping services.-->
               <div class="RyV-EM">Due to regular revisions by our logistics service providers as well as in Government regulations and directives, delivery may be delayed depending on your location.</div>
            </li>
         </ul>
         <ul class="QGSGCA">
		 <!--(Delivery time may vary with shipping address)-->
            <li class="I3tH7a">Deliveries are currently slower than usual.</li>
         </ul>
      </div>
   </div>
</div>


<!-- four icons starts -->
<div class="w100per four-icons">
   <div class="customcontainer">
      <div class="row">
         <div class="col-xs-12  flex-container">
            <div class="col-xs-3 topimg">
               <a href="<?php echo SITEMOBURL;?>user/shippingpolicy">
                  <img src="<?php echo SITEIMAGES.'mobimg/icon/free_delivery.jpg';?>" width="100%" style='max-height:150px'>
                  <p>Home <br>Delivery</p>
               </a>
            </div>
            <div class="col-xs-3 topimg">
               <a href="<?php echo SITEMOBURL;?>user/assuranceprogram">
                  <img src="<?php echo SITEIMAGES.'mobimg/icon/assuranceP.jpg';?>" width="100%" style='max-height:150px'>
                  <p>Assurance<br>Programm</p>
               </a>
            </div>
            <div class="col-xs-3 topimg">
               <a href="<?php echo SITEMOBURL;?>user/paymenthelp">
                  <img src="<?php echo SITEIMAGES.'mobimg/icon/options.jpg';?>" width="100%" style='max-height:150px'>
                  <p>Payment<br>options</p>
               </a>
            </div>
            <div class="col-xs-3 topimg">
               <a href="javascript:void(0)">
                  <img src="<?php echo SITEIMAGES.'mobimg/icon/RP1.jpg';?>" width="100%" style='max-height:150px'>
                  <p>Return<br>Policy</p>
               </a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- four icons finsih -->
<!-- Add to cart and   Buy now  -->
<div class="w100per functional-button">
   <?if($res['callforprice']!='YES') {?>   
   <?if(($res['avaliable']) == 1) {?> 
   <div class="col-xs-6 col-sm-6 col-md-6 text-uppercase text-center pr0 pl0">
      <a href="JavaScript:Void(0);" id="adtocart" data-sid="<?php echo $sid; ?>"
         onclick="addtocart(<?php echo $this->uri->segment(2);?>, this);">Add to cart</a>
   </div>
   <?}else{?>
   <div class="col-xs-6 col-sm-6 col-md-6 text-uppercase text-center pr0 pl0 disabled">
      <a href="JavaScript:Void(0);" class="bgwhite" style="color: #838383 ! important;">Add to cart</a>
   </div>
   <?}?>
   <?if(($res['avaliable']) == 1){?>
   <div class="col-xs-6 col-sm-6 col-md-6 text-uppercase text-center pr0 pl0 active" >
      <a href="JavaScript:Void(0);" class="active" data-sid="<?php echo $sid; ?>" 
         onclick="gotoviewcart(<?php echo $this->uri->segment(2);?>, this);">Buy Now</a>
   </div>
   <?}else{?>
   <div class="col-xs-6 col-sm-6 col-md-6 text-uppercase text-center pr0 pl0 disabled">
      <a href="JavaScript:Void(0);" class="bgred" style="color: #838383 ! important;">Buy Now</a>
   </div>
   <?}
      }?>
</div>
<!-- add to cart and buy now section finsih -->
<!-- product detail and specification  -->
<div class="w100per noborder tab-button">
   <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
      <ul id="myTab" class="nav nav-tabs1" role="tablist">
         <li role="presentation" class="active">
            <a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true" class="text-uppercase text-center pr0 pl0">Product details</a>
         </li>
         <li role="presentation">
            <a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours" class="text-uppercase text-center pr0 pl0">Specifications</a>
         </li>
		 <li role="presentation">
			<a href="#tab6default" data-toggle="tab" style="display: <?php echo !empty($res['embedCode']) ? 'block' : 'none' ;?>">Video</a>
		 </li>
      </ul>
      <div id="myTabContent" class="tab-content">
         <div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
            <div class="w100per desc ptb15" style=" border-bottom: 1px solid #e2e2e2;">
              <!-- <h4>Description:</h4>-->
               <p class=""><?php 
                  if($res['shortspecification']){
                     echo $res['shortspecification'];
                  } else {
                     echo "Product description not available";
                  } ?>
               </p>
            </div>
         </div>
		 <div class="tab-pane fade" id="tab6default" style="background-color: #fff;">		<!--display: <?php //echo !empty($res['embedCode']) ? 'block' : 'none' ;?>-->
			  <?php echo ($res['embedCode']) ? '<iframe width="100%" height="315" style="margin-top: 10px;" src="https://www.youtube.com/embed/'.$res['embedCode'].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>': 'No Video found';?>
		</div>
		<div class="tab-pane fade" id="tours">
            <div class="w100per desc ptb15" style=" border-bottom: 1px solid #e2e2e2;">
              <!-- <h4>Specifications:</h4>-->
               <p>
					<?php 
					  if($res['mfgLocation']){
						 echo "Country of Origin : ".ucwords(strtolower($res['mfgLocation']))."";
					  }
					 ?>
					
					<?php 
					  if($res['longdesc']){
						 //echo $res['longdesc'];
					  }else{
						 //echo "Product specifications are not available";
					  }
					 ?>
               </p>
            </div>
         </div>
      
      </div>
   </div>
</div>
<?if($res['review']){?>
<div class="w100per user-reviews" >
   <div class="container">
      <h4 class="heading">Reviews</h4>
      <?php 
         for ($i=0; $i <count($res['review']) ; $i++) { 
            if($i > 2){
                  $stylestr='style="display:none"';
                  }else{
                  $stylestr='';
            }?>
      <div class="w100per review review4to10" <?php echo $stylestr; ?> >
         <p class="w100per ratingall">  
            <?php 
               $starNumber1 = $res['review'][$i]['reviewrating'];
                  if(strpos($starNumber1, '.')){
                     $rawnumber1 = explode('.', $starNumber1);
                     $number1    = $rawnumber1[0];
                     $decnumber1 = $rawnumber1[1];
                  }else{
                   $number1 = $starNumber1;
                  }
               for ($j=0; $j <5 ; $j++) {
                  if($j< $number1){
                   ?>
            <i class="fa fa-star" style="color:#ffd700;"></i>
            <?php } elseif ($decnumber1 && $decnumber1==5) { ?>
            <i class="fa fa-star-half-full " style="color:#ffd700;"></i>
            <?php 
               $decnumber1++;
               } else {?>
            <i class="fa fa-star-o"></i>
            <?}
               }?>
         </p>
         <h4 class="w100per granite"><?php echo ucfirst($res['review'][$i]['reviewtitle']);?></h4>
         <p class="sterotype"><?php echo $res['review'][$i]['reviewtext'];?></p>
         <p>
            <?php
               $date=$res['review'][$i]['reviewdate_modified'];
               $date1=date("d M Y", strtotime($date));
               ?>
            <span class="sterotypss"><b><?php echo ucfirst($res['review'][$i]['reviewauthor']);?></b></span>   | <span class="sterotypss"><?php echo $date1;?></span>
         </p>
      </div>
      <?php  } ?>
      <div class="w100per" style="padding-bottom: 10px;border-bottom: 1px solid #cac6c6;">
         <?php if(count($res['review']) > 3) { ?>
         <a href="JavaScript:void(0)" class="more showmoewreviews" onclick="morereview()"> More Reviews <i class="fa fa-angle-right"></i></a>
         <?php } ?>
         <a href="javascript:void(0)" class="more pull-right addreview" onclick="checkloginforeview()"> Write A Review <i class="fa fa-angle-right"></i></a>
         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none" id="postreviews"></button>
      </div>
   </div>
</div>
<? }else{?>
<div class="w100per">
   <a href="javascript:void(0)" class="more pull-right addreview" onclick="checkloginforeview()"> Write A Review <i class="fa fa-angle-right"></i></a>
   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" style="display: none" id="postreviews"></button>
</div>
<? }?>
<!-- product details and  specification finsih -->
<!-- similar products starts -->
<?if($similar_product ){?>
<div class="bg-1 Similar-products text-center">
   <div class="row">
      <div class="col-xs-12">
         <div class="col-xs-7 instruments">
            <p>Similar products</p>
         </div>
      </div>
   </div>
</div>
<div class="Electric  bg-1 text-center">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <?php foreach($similar_product as $key => $value) { 
            $value['onlinePrice']=$value['price'];
            //print '<pre>'; print_r($value); die;
            include('include/list_product.inc');
         }?>
      </div>
   </div>
</div>
<!-- product listing finsih -->
</div>
</div>
<?}?>
<div id="reviewhieghtdetail"></div>
<!-- similar products finish -->
<!-- reviews section starts -->
<!-- reviews section finish -->
<div class="modal fade" id="myModal" role="dialog" style="margin-top: 80px;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!--<h4 class="modal-title"><?php //echo $_SESSION['usrname'];?></h4>-->
            <h4 class="modal-title"> Write Review </h4>
         </div>
         <div class="modal-body">
            <!--main box-->
            <div class="row">
               <div class="col-sm-12 col-md-12">
                  <div class="panel ">
                     <div class="panel-body">
                        <form  name="form1">
                           <input type="hidden" name="hiddenrating" id="hiddenrating" value="2">
                           <input type="hidden" name="productid" id="productid" value="<?php echo $this->uri->segment(2);?>">
							<div id="rateYo"></div>
                           <br>
                           <input type="text" class="form-control" value="" id="reviewtitle1" name="reviewtitle1" placeholder="Review title"/><br>
                           <textarea class="form-control counted" id="reviewmessage" name="message" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                           <button type="button" class="btn btn-info" id="postreviewdtl" name="postreviewdtl">Post Review</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include('include/footer.php');?>

  <!-- The Modal -->
  <div class="modal" id="myModalshae">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal body -->
        <div class="modal-body" style="padding: 0px;">
         <div class="_2zfhep _1D_OfP">
            <div class="_369Wx8">Share this product</div>
            <div class="yZ3gN9">
               <div class="_1eNmOR">
                  <ul>
                     <li class="_1WjAa2">
                        <a class="_5vz6FV" title='Share <?php echo $res['name'];?> On Facebook' href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $res['url'];?>&t=<?php echo $res['name'];?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"target="_blank" title="Share on Facebook">
                           <img class="_2oABMi" width="30px" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1OTYiIGhlaWdodD0iNTk1LjI4MSIgdmlld0JveD0iMTIzLjQ0NSAwIDU5NiA1OTUuMjgxIj48Y2lyY2xlIGZpbGw9IiMzQzVBOUEiIGN4PSI0MjAuOTQ1IiBjeT0iMjk2Ljc4MSIgcj0iMjk0LjUiLz48cGF0aCBmaWxsPSIjRkZGIiBkPSJNNTE2LjcwNCA5Mi42NzdoLTY1LjI0Yy0zOC43MTQgMC04MS43NzcgMTYuMjgzLTgxLjc3NyA3Mi40MDIuMTkgMTkuNTU0IDAgMzguMjgxIDAgNTkuMzU3SDMyNC45djcxLjI3MWg0Ni4xNzR2MjA1LjE3N2g4NC44NDZWMjk0LjM1M2g1Ni4wMDJsNS4wNjgtNzAuMTE3aC02Mi41MzFzLjEzOS0zMS4xOTEgMC00MC4yNDljMC0yMi4xNzcgMjMuMDc2LTIwLjkwNyAyNC40NjMtMjAuOTA3IDEwLjk4MiAwIDMyLjMzMi4wMzIgMzcuODE0IDBWOTIuNjc3aC0uMDMyeiIvPjwvc3ZnPg==">
                           <span class="_2MM1zz">Facebook</span>
                        </a>
                     </li>
                     <li class="_1WjAa2">
                        <a class="_5vz6FV" title='Share <?php echo $res['name'];?>  On Twitter' href="https://twitter.com/share?url=<?php echo $res['url'];?>&via=TWITTER_HANDLE&text=<?php echo $res['name'];?>"
                                  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                                  target="_blank" title="Share on Twitter">
                           <img class="_2oABMi" width="30px" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1OTYiIGhlaWdodD0iNTk1LjI4MSIgdmlld0JveD0iMTIzLjQ0NSAwIDU5NiA1OTUuMjgxIj48Y2lyY2xlIGZpbGw9IiMyREFBRTEiIGN4PSI0MjAuOTQ0IiBjeT0iMjk2Ljc4MSIgcj0iMjk0LjUiLz48cGF0aCBmaWxsPSIjRkZGIiBkPSJNNjA5Ljc3MyAxNzkuNjM0Yy0xMy44OTEgNi4xNjQtMjguODExIDEwLjMzMS00NC40OTggMTIuMjA0IDE2LjAxLTkuNTg3IDI4LjI3NS0yNC43NzkgMzQuMDY2LTQyLjg2YTE1NC43OCAxNTQuNzggMCAwIDEtNDkuMjA5IDE4LjgwMWMtMTQuMTI1LTE1LjA1Ni0zNC4yNjYtMjQuNDU2LTU2LjU1MS0yNC40NTYtNDIuNzczIDAtNzcuNDYxIDM0LjY3NS03Ny40NjEgNzcuNDczIDAgNi4wNjQuNjgyIDExLjk4IDEuOTk2IDE3LjY2LTY0LjM4OS0zLjIzNi0xMjEuNDc0LTM0LjA3OS0xNTkuNjg0LTgwLjk0NS02LjY3MiAxMS40NDYtMTAuNDkxIDI0Ljc1NC0xMC40OTEgMzguOTUzIDAgMjYuODc1IDEzLjY3OSA1MC41ODcgMzQuNDY0IDY0LjQ3N2E3Ny4xMjIgNzcuMTIyIDAgMCAxLTM1LjA5Ny05LjY4NnYuOTc5YzAgMzcuNTQgMjYuNzAxIDY4Ljg0MiA2Mi4xNDUgNzUuOTYxLTYuNTExIDEuNzg1LTEzLjM0NCAyLjcxNi0yMC40MTMgMi43MTYtNC45OTggMC05Ljg0Ny0uNDcyLTE0LjU4NC0xLjM2NCA5Ljg1OSAzMC43NjkgMzguNDcgNTMuMTY2IDcyLjM2MyA1My43OTktMjYuNTE1IDIwLjc4NS01OS45MjUgMzMuMTc0LTk2LjIxMiAzMy4xNzQtNi4yNSAwLTEyLjQyNy0uMzcyLTE4LjQ5MS0xLjEwNCAzNC4yOTEgMjEuOTg4IDc1LjAwNiAzNC44MjQgMTE4Ljc1OSAzNC44MjQgMTQyLjQ5NiAwIDIyMC40MjctMTE4LjA1MiAyMjAuNDI3LTIyMC40MjggMC0zLjM2MS0uMDc0LTYuNjk3LS4yMzYtMTAuMDIxYTE1Ny44MzUgMTU3LjgzNSAwIDAgMCAzOC43MDctNDAuMTU3eiIvPjwvc3ZnPg==">
                           <span class="_2MM1zz">Twitter</span>
                        </a>
                     </li>
                     <!--<li class="_1WjAa2">
                        <a class="_5vz6FV" title='Share <?php echo $res['name'];?> On Google Plus' href="https://plus.google.com/share?url=<?php echo $res['url'];?>"
                                  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
                                  target="_blank" title="Share on Google+">
                           <img class="_2oABMi" width="30px" src="<? echo WEBIMG;?>shareicon/google.png">
                           <span class="_2MM1zz">Google+</span>
                        </a>
                     </li>-->
					 <li class="_1WjAa2">
                        <a class="_5vz6FV" title='Share <?php echo $res['name'];?> On Whatsapp' href="https://api.whatsapp.com/send?text=<?php echo $res['url'];?>"
                                  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
                                  target="_blank" title="Share on Whatsapp">
                           <img class="_2oABMi" width="30px" src="<? echo WEBIMG;?>shareicon/w.png">
                           <span class="_2MM1zz">Whatsapp</span>
                        </a>
                     </li>
                     <li class="_1WjAa2">
                        <a class="_5vz6FV" title='Share <?php echo $res['name'];?> On Pinterest' href="http://pinterest.com/pin/create/button/?url=<?php echo $res['url'];?>&description=<?php echo $res['name'];?>"
                                  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
                                  target="_blank" title="Share on Pinterest+">
                           <img class="_2oABMi" width="30px" src="<? echo WEBIMG;?>shareicon/pinterest.png">
                           <span class="_2MM1zz">Pinterest</span>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="V9_1rM">
               <div class="_27R1Ir">
                  <div class="_1G9wwo" class="close" data-dismiss="modal">cancel</div>
               </div>
            </div>
         </div>
        </div>
      </div>
    </div>
  </div>
<!-- The Modal -->


<script src="<?php echo SITEJS;?>jssor.slider-24.1.5.min.js" type="text/javascript"></script>
<style>
   .soicalnew.p0 li {
   display: inline-block;
   padding-left: 5px;
   float: right;
   }
   .off {
   background: #12b312;
   color: #000;
   padding: 2px 3px;
   border-radius: 4px;
   }
   .slider input {
   display: block !important;
   }
   /* jssor slider bullet navigator skin 05 css */
   /*
   .jssorb05 div           (normal)
   .jssorb05 div:hover     (normal mouseover)
   .jssorb05 .av           (active)
   .jssorb05 .av:hover     (active mouseover)
   .jssorb05 .dn           (mousedown)
   */
   .jssorb05 {
   position: absolute;
   }
   .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
   position: absolute;
   /* size of bullet elment */
   width: 16px;
   height: 16px;
   background: url('<?php echo SITEIMAGES;?>mobimg/b05.png') no-repeat;
   overflow: hidden;
   cursor: pointer;
   }
   .jssorb05 div { background-position: -7px -7px; }
   .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
   .jssorb05 .av { background-position: -67px -7px; }
   .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
   /* jssor slider arrow navigator skin 12 css */
   /*
   .jssora12l                  (normal)
   .jssora12r                  (normal)
   .jssora12l:hover            (normal mouseover)
   .jssora12r:hover            (normal mouseover)
   .jssora12l.jssora12ldn      (mousedown)
   .jssora12r.jssora12rdn      (mousedown)
   .jssora12l.jssora12lds      (disabled)
   .jssora12r.jssora12rds      (disabled)
   */
   .jssora12l, .jssora12r {
      display: block;
      position: absolute;
      /* size of arrow element */
      width: 30px;
      height: 46px;
      cursor: pointer;
      background: url('<?php echo SITEIMAGES;?>mobimg/a12.png') no-repeat;
      overflow: hidden;
   }
   .jssora12l { background-position: -16px -37px; }
   .jssora12r { background-position: -75px -37px; }
   .jssora12l:hover { background-position: -136px -37px; }
   .jssora12r:hover { background-position: -195px -37px; }
   .jssora12l.jssora12ldn { background-position: -256px -37px; }
   .jssora12r.jssora12rdn { background-position: -315px -37px; }
   .jssora12l.jssora12lds { background-position: -16px -37px; opacity: .3; pointer-events: none; }
   .jssora12r.jssora12rds { background-position: -75px -37px; opacity: .3; pointer-events: none; }
   #response{margin-left:65px;}
</style>
<!--  JavaScript
       ================================================== -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script type="text/javascript" src="https://cdn.jsdelivr.net/elevatezoom/3.0.8/jqueryElevateZoom.js"></script>
   <script src="<?php echo SITEJS;?>ubislider.min.js" type="text/javascript"></script>
    <script src="<?php echo SITEJS;?>scripts.js" type="text/javascript"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
  
        <script type="text/javascript">
		$(document).ready(function(){
			$("#rateYo").rateYo({
			   rating:0,
			   fullStar: true,
			   onSet: function (rating, rateYoInstance) {
				 $('#hiddenrating').val(rating);
		   
			   }
			});
		});

          $('#slider4').ubislider({
              arrowsToggle: true,
              type: 'ecommerce',
              hideArrows: true,
              autoSlideOnLastClick: true,
              modalOnClick: true,
              
          }); 
          $('#slider5').ubislider({
            bullets: true
          }); 
        </script>
<script type="text/javascript">
   jQuery(document).ready(function($){
      var jssor_html5_AdWords_SlideoTransitions = [
         [{b:-1,d:1,o:-1,rY:-120},{b:2600,d:500,o:1,rY:120,e:{rY:17}}],
         [{b:-1,d:1,o:-1},{b:1480,d:20,o:1},{b:1500,d:500,y:-20,e:{y:19}},{b:2300,d:300,x:-20,e:{x:19}},{b:3100,d:300,o:-1,sY:9}],
         [{b:-1,d:1,o:-1},{b:2300,d:300,x:20,o:1,e:{x:19}},{b:3100,d:300,o:-1,sY:9}],
         [{b:-1,d:1,sX:-1,sY:-1},{b:0,d:1000,sX:2,sY:2,e:{sX:7,sY:7}},{b:1000,d:500,sX:-1,sY:-1,e:{sX:16,sY:16}},{b:1500,d:500,y:20,e:{y:19}}],
         [{b:1000,d:1000,r:-30},{b:2000,d:500,r:30,e:{r:2}},{b:2500,d:500,r:-30,e:{r:3}},{b:3000,d:3000,x:70,y:40,rY:-20,tZ:-20}],
         [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
         [{b:-1,d:1,o:-1,r:30},{b:1000,d:1000,o:1}],
         [{b:-1,d:1,o:-1},{b:2780,d:20,o:1},{b:2800,d:500,y:-70,e:{y:3}},{b:3300,d:1000,y:180},{b:4300,d:500,y:-40,e:{y:3}},{b:5300,d:500,y:-40,e:{y:3}},{b:6300,d:500,y:-40,e:{y:3}},{b:7300,d:500,y:-40,e:{y:3}},{b:8300,d:400,y:-30}],
         [{b:-1,d:1,c:{y:200}},{b:4300,d:4400,c:{y:-200},e:{c:{y:1}}}],
         [{b:-1,d:1,o:-1},{b:4300,d:20,o:1}],
         [{b:-1,d:1,o:-1},{b:5300,d:20,o:1}],
         [{b:-1,d:1,o:-1},{b:6300,d:20,o:1}],
         [{b:-1,d:1,o:-1},{b:7300,d:20,o:1}],
         [{b:-1,d:1,o:-1},{b:8300,d:20,o:1}],
         [{b:2000,d:1000,o:-0.5,r:180,sX:4,sY:4,e:{r:5,sX:5,sY:5}},{b:3000,d:1000,o:-0.5,r:180,sX:-4,sY:-4,e:{r:6,sX:6,sY:6}}],
         [{b:-1,d:1,o:-1,c:{m:-35.0}},{b:0,d:1500,x:150,o:1,e:{x:6}}],
         [{b:-1,d:1,o:-1,c:{y:35.0}},{b:0,d:1500,x:-150,o:1,e:{x:6}}],
         [{b:-1,d:1,o:-1},{b:6500,d:2000,o:1}],
         [{b:-1,d:1,o:-1},{b:2000,d:1000,o:0.5,r:180,sX:4,sY:4,e:{r:5,sX:5,sY:5}},{b:3000,d:1000,o:0.5,r:180,sX:-4,sY:-4,e:{r:6,sX:6,sY:6}},{b:4500,d:1500,x:-45,y:60,e:{x:12,y:3}}],
         [{b:-1,d:1,o:-1},{b:4500,d:1500,o:1,e:{o:5}},{b:6500,d:2000,o:-1,r:10,rX:30,rY:20,e:{rY:6}}]
       ];
   
       var jssor_html5_AdWords_options = {
         //$AutoPlay: 1,
         $Idle: 1600,
         $SlideDuration: 400,
         $SlideEasing: $Jease$.$InOutSine,
         $CaptionSliderOptions: {
           $Class: $JssorCaptionSlideo$,
           $Transitions: jssor_html5_AdWords_SlideoTransitions
         },
         $ArrowNavigatorOptions: {
           $Class: $JssorArrowNavigator$,
           $ChanceToShow: 1
         },
         $BulletNavigatorOptions: {
           $Class: $JssorBulletNavigator$,
           $ActionMode: 2
         }
       };
   
       var jssor_html5_AdWords_slider = new $JssorSlider$("jssor_html5_AdWords", jssor_html5_AdWords_options);
     
     jQuery('.frist').addClass('active');
     
   }); 
   function addtocart(ele, element=false){
     proid=ele;
     sid =jQuery(element).data('sid');
     if(sid){
       sid=sid;
     }else{ 
       sid=0;
     }
     //console.log(sid); return false;
     jQuery.ajax({
       url:'<?php echo SITEMOBURL?>product/storeproid',
       type:'POST',
       data:{proid:proid,sid:sid},
       success:function(data)
       {   var response = jQuery.parseJSON(data);
           
         if(response.error=='false'){
              alert('Product successfully added to cart');  
              ajax_getCartCount();
           }else{
            alert('Error while adding product to cart');  
             
           }
         
       }
   
     });
     
   }
   
   function changepincode(){
     
     jQuery('.pin').hide();  
     jQuery('.showpincode').show();
     jQuery('.shipmesg').hide();  
   }
   
   function checkpincode(){
     var pincode=jQuery('#pincode').val();
     if(pincode){
        jQuery.ajax({
               url :'<?php echo SITEMOBURL?>user/checkpincode', 
              type:'POST',
              data:{pincode:pincode},
           success:function(resp){
                 
             response = jQuery.parseJSON(resp);
              console.log(response);
             if(response.status== true){ 
              
               jQuery('.showpincode').hide(); 
               jQuery('.pin').show(); 
            jQuery('.pin').html('Change Pincode'); 
               jQuery('#pincode').val(''); 
               jQuery('.shipmesg').show(); 
               jQuery('.shipmesg').html('Delivery to '+pincode+' is available');
             }else{
               jQuery('.showpincode').hide(); 
               jQuery('.pin').show();
            jQuery('.pin').html('Change Pincode'); 
               jQuery('#pincode').val('');  
               jQuery('.shipmesg').show(); 
               jQuery('.shipmesg').html('Delivery to '+pincode+' is not available');
             }
           }
         });   
       }else{
         alert('Please enter your pincode'); 
         return false;
       }
     }
   
</script>
<script>
   function ajax_getCartCount(){
     jQuery.ajax({
             url :'<?php echo SITEMOBURL?>product/getcartcount', 
            type:'POST',
           
         }).done(function(resp){
           var response = jQuery.parseJSON(resp);
             if(response.status==1)
             {
                 if(response.cartcount > 0){  
                    jQuery('#currentitemcount').html(response.cartcount); 
                  }
             }
             else
             {
                  if(response.cartcount > 0){  
                      jQuery('#currentitemcount').html(response.cartcount); 
                    } 
             }
           return false;
       });
   }
   
</script>
<script type="text/javascript">
   function gotoviewcart(ele, element=false)
   {
     addtocart(ele, element);

     window.setTimeout(function () {
        location.href="<?php echo SITEMOBURL;?>product/buynow"
    }, 2000);

     // window.location.href="<?php echo SITEMOBURL;?>product/buynow"
   }
</script>
<script>
   function addtobundle(ele){
     proid=ele;
     jQuery.ajax({
       url:'<?php echo SITEMOBURL?>product/addtobundle',
       type:'POST',
       data:{proid:proid},
       success:function(data)
       {   var response = jQuery.parseJSON(data);
           
         if(response.error=='false'){
              alert('Product successfully added to bundle cart');  
              window.location.reload();
           }else{
            alert('Error while adding product to bundle cart');  
             
           }
         
       }
   
     });
     
   }
   
     function checkloginforeview()
     {
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/checkloginforeview',
         type:'POST',
         success:function(data)
         {   var response = jQuery.parseJSON(data);
             console.log(response);
           if(response.status==true){
             
                 jQuery('#postreviews').click();
             }else{
                window.location.href='<?php echo SITEMOBURL?>user/';
             }
           
         }
   
       });
     }
   
	// $(function () {
		// console.log("In rating");
     // $("#rateYo").rateYo({
       // rating:2,
       // fullStar: true,
       // onSet: function (rating, rateYoInstance) {
         // jQuery('#hiddenrating').val(rating);
   
       // }
     // });
   // });  
   function morereview()
   {
     jQuery('.review4to10').css('display','block');
     jQuery('.showmoewreviews').css('display','none');
     
   }
   function isNumberKey(evt)
          {
          
         
             var charCode = (evt.which) ? evt.which : evt.keyCode;
              var a = [];
             if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
             {
                return false;
             }else{
                 
                 var k = evt.which;
                 for (i = 48; i < 58; i++)
                     a.push(i);
                 if (!(a.indexOf(k)>=0)){
                     return false;
                   }else{
                     return true;        
                   }
               
             }
          }
   
</script>
<script type="text/javascript">
   jQuery ('#reviews1').on('click',function(){
      //alert('hiii');
        var review=jQuery('#reviewhieghtdetail').innerHeight();
        //console.log(review); return false;
        var reviewexact=review;
        jQuery('html, body').animate({
         scrollTop: reviewexact//450
        }, 1000);
     })
     jQuery('#postreviewdtl').click(function(){
     
       reviewmessage=jQuery('#reviewmessage').val();
       reviewtitle1=jQuery('#reviewtitle1').val();
       ratingme=jQuery('#hiddenrating').val();
       proid=jQuery('#productid').val();
       proname=jQuery('.proname').attr('data-id');
       jQuery('#myModal').modal('hide');
       jQuery(".overlay").show();
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/addreview',
         type:'POST',
         data:{proid:proid,reviewmessage:reviewmessage,ratingme:ratingme,reviewtitle1:reviewtitle1,proname:proname},
         success:function(data1)
         {   
           //jQuery(".overlay").show();
           var response = jQuery.parseJSON(data1);
           if(response.error=='false'){
                alert('Review submitted successfully');  
                window.location.reload();
             }else{
              alert('Error while adding review to product');  
               
             }
           
         }
     });
     });
     jQuery('#emi1').on('click',function(){
     //alert('hiii');
         jQuery('#emi').modal('show');
     }) 
     jQuery('.emi2').on('click',function(){
         jQuery('#emi').modal('hide');
     }) 

   jQuery('#changepin').on('click', function(){
      var button=jQuery(this);
      var btntext=button.text();
      jQuery('#deliverto').css('top', '15px');
      jQuery('#newpincdefield').css('display', 'block');
      jQuery(button).text('Apply');      
      if(btntext=='Apply'){
         jQuery('#response').text('');
         var pincode=jQuery('#newpincdefield').val();
         if(pincode==''){
            jQuery('#response').html('Please enter Pincode');
            return false;
         }
         jQuery.ajax({
            type:'POST',
            data:{pincode},
            url:'<?php echo SITEMOBURL?>user/checkpincode',
            dataType:'JSON'
         }).done(function(data){
            jQuery('#changepin').css('margin-top', '-18px');
            if(data.status==true){
               var message='Delivery to '+pincode+' is available';
               jQuery('#response').html(message);
            }else{
               var message='Delivery is not available';
               jQuery('#response').html(message);
            }
         })
      }
   })
</script>