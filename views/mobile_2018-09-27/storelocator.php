<div class="top-logo">
   <div class="container" style="padding:0;">
      <div class="row">
         <div class="col-md-12">
            <div class="col-xs-2">
               <a href="#" onclick="window.history.go(-1); return false;" >
                  <!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
                  <i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
               </a>
            </div>
            <div class="col-xs-4 Favourite_list">Store Detail</div>
         </div>
      </div>
   </div>
</div>
<!-- store locator contianer starst -->
<div class="store-locator w100per">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="listing-box w100per no-shadow">
         <h3><?php echo $map[0]['storeName'];?></h3>
         <p><?php echo $map[0]['storeAddress'];?></p>
         <?php if (isset($map[0]['googleMapLink'])) { ?>
         <p class="last-row w100per"><a href="<?php echo SITEMOBURL?>user/map/<?php echo $map[0]['storeId'];?>" class="pull-left"> <img src="<?php echo SITEIMAGES;?>mobimg/map-icn.jpg"><span class="gmap">Google Map</span></a></p>
         <?php } ?>
         <div class="media w100ner">
            <div class="media-left"><img src="<?php echo SITEIMAGES;?>mobimg/clock.jpg" alt=""></div>
            <div class="media-body">
               <b>
                   <h3> <?php echo $map[0]['storeName'];?></h3>
                  <p><?php echo $map[0]['storeAddress'];?></p>
                  <?php if($map[0]['telePhone1']){ ?>
                    <p>Phone: <?php echo $map[0]['telePhone1'];?></p>
                  <?php } ?>
                  <?php if($map[0]['telePhone2']){ ?>
                    <p>Phone 2: <?php echo $map[0]['telePhone2'];?></p>
                  <?php } ?>
                  <p>Timing: <?php echo ($map[0]['time']) ? $map[0]['time'] : 'N/A'; ?></p>
                  <p>Closed On: <?php echo ($map[0]['closedOn']) ? $map[0]['closedOn'] : 'N/A'; ?></p>
               </b>
            </div>
         </div>
         <div class="media w100ner">
            <div class="media-left"><img src="<?php echo SITEIMAGES;?>mobimg/phone.jpg" alt=""></div>
            <div class="media-body">
               <b>
                  <p class="media-heading"><?php  if(isset($map[0]['telePhone1'])) { ?><a href="tele:<?php echo $map[0]['telePhone1']; ?>"><?php echo $map[0]['telePhone1'];?></a><? } else{
                     echo "Not available"; 
                     }
                     if(isset($map[0]['telePhone2'])){  ?> <a href="tele:<?php echo $map[0]['telePhone2'];?>"><?php echo "/ ".$map[0]['telePhone2'];?></a><? }?></p>
               </b>
            </div>
         </div>
      </div>
   </div>
   <?php if(isset($map[0]['storeImage'])){ ?>
   <div class="store-image col-xs-12 col-sm-12 col-md-12 col-lg-12 bdr-top">
      <div class="store-slider">
         <img src="<?php echo SITESTORE;?>/<?php echo $map[0]['storeImage'];?>" alt="">
      </div>
   </div>
   <? } ?>
</div>
<!-- store locator contianer finish -->
<script>
   function initMap() {
     // Create a map object and specify the DOM element for display.
     var map = new google.maps.Map(document.getElementById('map'), {
       center: {lat: -34.397, lng: 150.644},
       scrollwheel: false,
       zoom: 8
     });
   }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfjn9LcHReUmiN89okGovW9XCqJEO-3-E&callback=initMap"
   async defer></script>
<?php include("include/footer2.php"); ?>