<style>
p { font-size: 14px; text-align: justify;}
.abouthead{text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;}
.pdt{padding-left: 0px;padding-right: 0px; text-align:center}
.widt54{width: 54%}.widt50{width: 50%}.widt60{width: 60%}
.pd{padding-left: 0px;padding-right: 0px;}
.mt50{margin-top: 50px;}.clorcc{color:#cc0028 !important}.clr20{color:#cc0028; font-size: 20px;}.clrcc{color:#cc0028}
</style>
<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150 inner-bottom-20">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
               <div class="wide-banners wow fadeInUp">
                  <div class="row">
                    <h2 class="abouthead">About Us</h2>
                     <div class="col-xs-12">
                        <div class="col-lg-4 pdt">
                           <img src="<?php echo WEBIMG;?>staticpage/about.jpg" class="widt54">
                        </div>
                        <div class="col-lg-8 pd">
						              <p>
                           <b>'Think Music, Think Furtados' </b></p>
                           <p>Established in 1865, Furtados has always been a leader in the field of Western music education and knowledge in India. From musical instruments, accessories to print music and digital equipment, we have a complete spectrum of musical instruments in the Western and Indian categories.</p>
                           <p>Furtados has grown into a household name in the music business and are spread across Mumbai, Pune, Ahmedabad, Delhi, Chandigarh, Bengaluru, Mangaluru, Panjim and Margao in Goa. With a growing retail network (17 showrooms) and over 350 dealer outlets, Furtados holds a record number of international dealerships for top-of-the-line brands in music. Instruments are imported from all across the globe including the USA, Canada, UK, Germany, Netherlands, Denmark, Italy, France, Japan, Korea, Taiwan, Indonesia, China, Malaysia, Vietnam, Hong Kong and Sri Lanka. Music books are imported from almost every major music publisher.</p>
							         </div>
                     </div>
                     
					           <div class="col-lg-12">
                          <p></p>
					               <p><b>Our Core Beliefs</b></p>
                         <p>At the core of Furtados’ business motto is <b>‘Trusted Service’</b>. In a pan-India customer survey conducted in the metros and other major cities, three factors characterised Furtados’ services – <b>Quality</b>, <b>Reliability</b> and <b>Trust</b>. Our legacy of over 150 years has helped us build a service standard that is second to none. Furtados has over the years been a trusted partner for providing musical instruments to musicians and institutions across the country – partnerships which continue today and for the future.</p>
                         <br>
                         
                         <p><b>Our Music Support</b></p>
                         <p>Furtados firmly believes in educating, guiding and supporting music lovers at every step and pro-actively invests in the development of musical knowledge through teacher-training workshops, master classes, clinics, demonstrations and exhibitions and sponsoring of local talent and artists. Furtados also invests in community development and supports several organisations across the country, including school and college events, musical activity in religious and charitable organisations, festivals as well as social upliftment projects involving a musical element.</p>
                         <br>

                         <p><b>The Piano Experts:</b></p>
                         <p>Our experience and skills acquired over the vast number of years has allowed us to advise you on the most suitable piano for your home or institutional use. In June 2012, the Furtados Institute of Piano Technology commenced operations in Mumbai, involving a two year course covering tuning, regulating, repairing, rebuilding and restoring, which was designed, administered and imparted by two accomplished professionals in the trade from the UK, who moved to India for the duration of the project. Eighteen tuner-technicians were awarded diplomas in 2014 and now serve not just Furtados showrooms but piano customers all around the country. Today, the Furtados Piano Restoration Centre in Sewri, in south-west Mumbai has a team of technicians of international standards who can maintain your piano in pristine condition for a lifetime of use. We are the exclusive dealer for Steinway & Sons, the most preferred piano in the world. We also offer you a choice of brands and models to suit your taste, preference and budget.</p>
                         <br>
<!--is headed by <a href="http://www.steinwaypianos.in/services/DJ-Smallman-Profile/"><u>D J Smallman</u></a> from the UK, and provides -->
                         <p><b>The Furtados Assurance</b></p>
                         <p>A purchase from Furtados entitles you to a unique Assurance Card which provides warranties on all products that you buy. <a href="<?php echo ROOTURL; ?>user/assuranceprogram/"><u>The Furtados Assurance</u></a> is a guarantee that you will benefit from a service network that is reliable and trustworthy.</p>
                         <br>

                         <p><b>Gear for Hire</b></p>
                         <p>We provide the best of music gear on hire for all major music performances, concerts and events. Our extensive range enables us to provide even the most discerning of artists the gear that they are looking for.</p>
                         <br>
                    </div>
                    <br>
                    <div class="col-xs-12">
                        <div class="col-lg-3 text-center">
                           <img src="<?php echo WEBIMG;?>staticpage/fsm.jpg" style="width:50%">
                        </div>
                        <div class="col-lg-9">
                          
                           <p><a href="http://www.furtadosschoolofmusic.com/"><b><u>Furtados School of Music</u></b></a> was launched in June 2011 to provide quality and organised music education to students and aspiring musicians across India. With the help of a path breaking international curriculum for the Piano, Keyboard, Guitar, Drums and Violin, we have been able to bring music to and currently teach over 50,000 students in over 190+ points of presence. In 2018, we have partnered with The Narsee Monjee Institute of Management Studies (NMIMS), a deemed university, to launch India’s first under graduate degree programme in contemporary music. A first in the country.</p>
                       </div>
                     </div>

                     <div class="col-xs-12 mt50">
                        <div class="col-lg-3 text-center">
                           <img src="<?php echo WEBIMG;?>staticpage/high.jpg" class="widt50">
                        </div>
                        <div class="col-lg-9">
                          
                           <p><a href="http://highfurtados.com"><b><u>High Furtados</u></b></a> HIGH – A Furtados Venture – brings music learning to the doorstep of every music aspirant. With the HIGH app, you now get to choose teachers as per your convenience, pick up lessons from our extensive music curriculum, music backing tracks that help you improvise your skills and much more.</p>
                           
                       </div>
                     </div>
                     <div class="col-lg-12 ">
                        <p></p>
                        <p></p>
                        <p></p>
                        <br><br>
                        <p><b>Discover, Choose & Learn Music on the HIGH App</b></p>
                        <div class="col-lg-12 col-lg-offset-4">
                              <a href="https://itunes.apple.com/US/app/id1203611105?mt=8"><img src="<?php echo WEBIMG;?>staticpage/itunes.jpg" style="width:12%"></a>
                              <a href="https://play.google.com/store/apps/details?id=com.furtados.highapp&hl=en"><img src="<?php echo WEBIMG;?>staticpage/play_store.jpg" style="width:12%"></a>
                        </div>
                    </div>

                    <div class="col-lg-12 ">
                        <p></p>
                        <p></p>
                        <p></p>
                        <p><b>Social Media</b></p>
                        <p>Furtados is actively present on <a href="https://www.facebook.com/FurtadosMusic/" class="clorcc">Facebook</a>, <a href="https://www.instagram.com/furtadosmusic/" class="clorcc">Instagram</a> and <a href="https://twitter.com/furtados/" class="clorcc">Twitter</a>.The Furtados <a href="https://youtube.com/furtadosmusicindia" class="clorcc">Youtube</a> channel upholds our belief in sharing the joy of music among the ever increasing community of musicians. You can monitor our workshops, clinics, demos and other events on our Furtados Social Media.</p>
                    </div>

                    <div class="col-lg-12 col-lg-offset-4" ><!--This is pending-->
                        <ul class="link list-group">
                          <li class="fb pull-left list-group-item"><a target="_blank" rel="nofollow" href="https://www.facebook.com/FurtadosMusic" title="Facebook"><i class="fa fa-facebook-square"></i></a></li>

                          <li class="tw pull-left list-group-item"><a target="_blank" rel="nofollow" href="https://twitter.com/furtados" title="Twitter"><i class="fa fa-twitter-square clr20"></i></a></li>

                          <li class="googleplus pull-left list-group-item"><a target="_blank" rel="nofollow" href="https://www.instagram.com/furtadosmusic/" title="Instagram"><i class="fa fa-instagram clr20"></i></a></li>

                          <li class="youtube pull-left list-group-item"><a target="_blank" rel="nofollow" href="https://www.youtube.com/user/furtadosmusicindia" title="Youtube"><i class="fa fa-youtube clr20"></i></a></li>

                           <li class="pintrest pull-left list-group-item"><a target="_blank" rel="nofollow" href="https://plus.google.com/u/0/+furtadosmusicindia" title="googleplus"><i class="fa fa-google-plus-square clr20"></i></a></li>
                        </ul>
                    </div>

                    <div class="col-lg-12">
                        <p></p><p></p><br><br>
                        <div class="col-lg-7 text-center">
                            <img src="<?php echo WEBIMG;?>staticpage/brands.jpg" class="widt60">
                        </div>

                        <div class="col-lg-5">
                          <p></p>
                          <p></p>
                          <p></p>
                          <p></p>
                          <p><b>Our Brands</b></p>
                          <p>Furtados is proud to represent the most respected International Brands in all categories. We retail over 100 Brands and offer our customers the widest choice of instruments.</p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                          <p></p><br><br>
                          <p><b>India’s Leading Musical Instruments Company</b></p>
                          <p><b>Over 100 Brands</b></p>
                          <p><b>Shop @ Furtados Showrooms</b></p>
                          <p>We retail the entire range of musical instruments and music books from our network of showrooms across the country. Our customers can try out their favourite instrument in acoustically treated demo rooms under the guidance of trained and experienced product specialists. A purchase from Furtados is an assurance of continuous service and support across our countrywide network of showrooms.</p>

                          <p>WEST: MUMBAI | PUNE | GOA</p><!--AHMEDABAD |  -->
                          <p>NORTH: CHANDIGARH </p>
                          <!-- <p>NORTH-EAST: DIMAPUR | DELHI</p> -->
                          <p>SOUTH: HYDERABAD | MANGALURU | PUDUCHERRY | BENGALURU | CHENNAI</p>
                          <hr>
                          <!--<p>AHMEDABAD</p>
                          <p><span class="clrcc">&#9632;</span> S-19, AlphaOne, Vastrapur, Ahmedabad-380054 Tel: 079-40067704</p>
                          <p></p>
                          <hr>-->
                          <p>BENGALURU</p>
                          <p><span class="clrcc">&#9632;</span> 116 /C-1, 5th Block, Koramangala, Bengaluru-560095 Tel: 080-41104304</p>
                          <p></p>
                          <hr>
                          <p>CHANDIGARH</p>
                           <p><span class="clrcc">&#9632;</span> Ground Floor, S.C.F. - 21, Sector 7C (Inner Market), Chandigarh-160019 Tel: 0172-4371675</p>
                          <p></p>
                          <hr>
                          <p>CHENNAI</p>
                           <p><span class="clrcc">&#9632;</span> Musee Musical, 73, Anna Salai, Chennai-600002, Tel: 044-4556660</p>
                          <p></p>
                          <hr>
                          <!--<p>DELHI</p>
                           <p><span class="clrcc">&#9632;</span> B-14, Basement, Lajpat Nagar II, New Delhi-110024 Tel: 011-29814585, 46105145</p>
                          <p><span style="color:#cc0028">&#9632;</span> M-110, 1st Floor, DLF Place, Near Hard Rock Cafe, Saket, New Delhi-110017 Tel: 011-46170900</p>
                          <p></p>
                          <hr>
                           <p>DIMAPUR</p>
                           <p><span class="clrcc">&#9632;</span> AIKO Building,  No-9. Opp. Super Market, Dimapur-797112, Nagaland Tel: 09862563139</p>
                          <p></p> -->
                    		<hr>
                    	  <p>GOA</p>
                           <p><span class="clrcc">&#9632;</span> Shop No.12, Navelcar Arcade, Dr. A B Road, Panjim-403001 Tel: 0832-2223278</p>
                           <p><span class="clrcc">&#9632;</span> A120-A123, Reliance Trade Center, Margao-403601 Tel: 0832-2711226</p>
                          <p><span class="clrcc">&#9632;</span> Shop No. 14, Grace Estate, Margao-403601 Tel: 0832-2715719/ 2714827</p>
                          <p></p>
                          <hr>
                          <p>HYDERABAD</p>
                           <p><span class="clrcc">&#9632;</span> Musee Musical, Shop No. 7- 10, Babu Khan Mall, Sowmaji Guda Circle, Hyderabad Tel: 040-66101535</p>
                          <p></p>
                          <hr>
						  <p>MANGALURU</p>
                          <p><span class="clrcc">&#9632;</span> Gr. Flr, Willys Center, Balmatta, Mangaluru-575001 Tel: 0824-4262032/ 4255977</p>
                          <p></p>
                          <hr>
                          <p>MUMBAI</p>
                          <p><span class="clrcc">&#9632;</span> Jer Mahal, Dhobitalao, Mumbai-400002 Tel: 022-66225454</p>
                          <p><span class="clrcc">&#9632;</span> 540-544, Kalbadevi Road, Mumbai-400002 Tel: 022-66225417</p>
                          <p><span class="clrcc">&#9632;</span> Subedar House, Gr. Flr., Next to Sujay Hospital, Andheri(W), Mumbai-400049 Tel: 022-26211801</p>
                          <p><span class="clrcc">&#9632;</span> Shop no. 161 & 162, Galleria Shopping Mall, Powai, Mumbai-400076 Tel: 022-40155892</p>
                           <p><span class="clrcc">&#9632;</span> Shop No.4, Madhavkunj Apartments, A-Wing, 270, Chamunda Circle, Borivali (W), Mumbai-400092. Tel: 022-67081096/97</p>
                          <p></p>
                    		<hr>
                          <p>PUDUCHERRY</p>
                           <p><span class="clrcc">&#9632;</span> Musee Musical, 396, Bharathi Street, Puducherry-605001 Tel: 0413-2223533</p>
                          <p></p>
                          <hr>
                          <p>PUNE</p>
                          <p><span class="clrcc">&#9632;</span> CAMP-NUCLEUS MALL, Unit No.G8/G9, Gr.Fl,Church Rd, Nucleus Mall, Pune Camp, Pune-411001, INDIA Phone : 8446000623,8446000629</p>
                         <p><span class="clrcc">&#9632;</span> Shop No.G1-A,Upper Ground Floor, Sr No 1, Siddh Icon, Baner Road, Pune-411045  Tel: 020-27293508/9</p>
                          <p></p>
                          <hr>
                          <!-- <p>SECUNDERABAD</p>
                         <p><span class="clrcc">&#9632;</span> Musee Musical, 63, Swapnalok Complex, S.D. Road, Secunderabad-500003 Tel: (040) 27811674</p>
                          <p></p><hr> -->
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
