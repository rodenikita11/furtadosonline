<?php
  if (!defined('BASEPATH')) exit('No direct script access allowed');

  class Tracking extends MY_Controller{
    public $ci;
    function __construct(){
        parent::__construct();

        $CI = & get_instance();
        $CI->load->library('libapi');
		$this->ci = $CI;
    }

    public function index(){
        // die('mara');
    }

    public function productclick(){
        $search_id = $_SESSION['search_log_id'];

        $page_nos = $_POST['page_nos'] != '' ? $_POST['page_nos'] : 1;
        $product_id = $_POST['product_id'];

        if( !empty($search_id) && !empty($product_id) ){

            $url = "http://3.210.70.215:8080/search/api/v1.0/user_click/$search_id/$page_nos/$product_id";
            $get_data = $this->ci->libapi->callAPI('GET', $url , false);
                        
            $res = $get_data;
            echo '{ "status": "Success", "message":'.$res.' }';
        }
        else{
            echo '{ "status": "Failure", "message":"Not searching for anything" }';
        }
    }
		
 }
?>