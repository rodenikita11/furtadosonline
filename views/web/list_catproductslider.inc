<style type="text/css">

.item.item-carousel {
    margin: 3px 0px 0px;
}
.featured-product .products {padding: 14px 12px 0px;}
     /*carousel control button*/
.sidebar-widget .custom-carousel .owl-controls {right:0px}

.custom-carousel .owl-controls {
  position: absolute;
  right: -20px;
  top: -32px;
  width: 100%;
  display: block;
}
.custom-carousel .owl-controls .owl-prev {
  position: absolute;
  width: 20px;
  height: 20px;
  top: 0px;
  left: 784px;
  -webkit-transition: all linear 0.2s;
  -moz-transition: all linear 0.2s;
  -ms-transition: all linear 0.2s;
  -o-transition: all linear 0.2s;
  transition: all linear 0.2s;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background: #dddddd ! important;
}
.custom-carousel .owl-controls .owl-prev:before {
  color: #fff;
  content: "\f104";
  font-family: fontawesome;
  font-size: 13px;
  left: 7px;
  position: absolute;
  top: 2px;
}

.custom-carousel .owl-controls .owl-next {
  position: absolute;
  width: 20px;
  height: 20px;
  top: 0px;
  right: 20px;
  -webkit-transition: all linear 0.2s;
  -moz-transition: all linear 0.2s;
  -ms-transition: all linear 0.2s;
  -o-transition: all linear 0.2s;
  transition: all linear 0.2s;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background: #dddddd ! important;
}
.custom-carousel .owl-controls .owl-next:before {
  content: "\f105";
  font-family: fontawesome;
  color: #fff;
  font-size: 13px;
  left: 7px;
  position: absolute;
  top: 2px;
}
.logo-slider .owl-controls .owl-prev,
.logo-slider .owl-controls .owl-next {
  top: -57px;
  display:none
}
.tag {
    top: -5% ! important;
}
.featured-product {
  background: transparent;
}
.featured-product .home-owl-carousel {
    padding-left: 0px;
}
h3.section-title {
    padding-left: 5px ! important;
}
.text-center{text-align: center;}.imgwdt{width: auto;height: 143px;}
.adclr{color: #ccc; display: none;margin-top: 13px;}.clr{color: #cc0028;}.clrc{color: #ccc;}.fn10{font-size: 10px;}.hp{height: 50px; padding: 2px 12px;}.pdl5{padding-left: 5px;}.fn9{font-size: 9px;}.pd6{padding-left: 6px;}.fn6l{font-size: 9px;margin-left: 6px;}
.hp{height: 50px;padding: 2px 3px;}.mt0{margin-top: 0px !important;}.mt1{margin-top: 1px !important;}
   </style>
<? //echo '<pre>'; print_r($val); die;?>
 <?if(isset($val['onlineprice'])){
  $val['price']=$val['onlineprice'];
 }
if(isset($val['image'])){ $val['img']=$val['image'];}?> 
<?php $word=str_word_count($val['name']) <= 6 ? 'mt0' : 'mt1';?>

<div class="item item-carousel catallpro" data-url="<?php echo $val['url']; ?>">
   <div class="products margin-right0">
      <div class="product">
         <div class="product-image">
            <div class="image text-center"> <a href="<? echo $val['url'];?>" alt='<? echo $val["name"] ?>' title='<? echo $val["name"] ?>'><img  src="<?php echo $val['img'];?>" class="imgwdt" alt=""></a> </div>
            <!-- /.image -->
            <? if($val['percentage'] > 0){?>
            <div class="offer"> <img src="<?echo SITEIMAGES?>offer.png"></div>
            <?}?>
            <?if($val['clearanceAvailable']> 0){?>
            <div class="tag hot lnk wishlist"><a class="add-to-cart" href="#" alt='<? echo $val["name"] ?>' title='<? echo $val["name"] ?>'> <i class="icon fa fa-heart adclr"></i> </a></div>
            <?}else if($val['wishlist']> 0){?>
            <div class="tag hot lnk wishlist"><a class="add-to-cart" href="#" alt='<? echo $val["name"] ?>' title='<? echo $val["name"] ?>'> <i class="icon fa fa-heart"  ></i> </a></div>
            <?}else if(isset($val['discountPrice'])){?>
            <div class="tag hot lnk wishlist"><a class="add-to-cart" href="#" alt='<? echo $val["name"] ?>' title='<? echo $val["name"] ?>'> <i class="icon fa fa-heart adclr" ></i> </a></div>
            <?}else{?>
            <div class="tag hot lnk wishlist"><a class="add-to-cart" href="#" alt='<? echo $val["name"] ?>' title='<? echo $val["name"] ?>'> <i class="icon fa fa-heart clrc"></i> </a></div>
            <?}?>
         </div>
         <!-- /.product-image -->
         <div class="product-info text-left">
            <h3 class="name"><a href="<? echo $val['url'];?>"><? echo substr($val['name'], 0,55).'....';?></a></h3>
            <?php $viki=$val['rating']>0 ? 'block' : 'none' ?>
               <div class="rating">
               <div class="star-rating" title="70%">
                  <div class="back-stars">
                     <?php //echo $val['rating'];?>
                     <?php
                        $starNumber=$val['rating'];
                        if(strpos($starNumber, '.')){
                          $rawnumber=explode('.', $starNumber);
                          $number=$rawnumber[0];
                          $decnumber=$rawnumber[1];
                        }else{
                          $number=$starNumber;
                        }
                         for ($i=0; $i <5 ; $i++) {
                          if($i < $number){
                            ?>
                     <i class="fa fa-star bg-star" aria-hidden="true" style="display: <?php echo $viki;?>"></i>
                     <?}elseif($decnumber && $decnumber==5){ ?>
                     <i class="fa fa-star-o " aria-hidden="true" style="display: <?php echo $viki;?>"></i>
                     <?php
                        $decnumber++;
                        }else{?>
                     <i class="fa fa-star bg-star1" aria-hidden="true" style="display: <?php echo $viki;?>"></i></a>
                     <?}
                        }
                        ?>
                  </div>
               </div>
            </div>
            <?if($val['callforprice']=='YES'){?>
            <div class="product-price productt1 text-center" >
               <div class="col-xs-12 prize-color"><a href='tel:02242875050'>Call for Price</a></div>
            </div>
            <?}else{?>
            <?if(isset($val['discountprice'])){?>
            <?if($val['price'] ==  $val['discountprice']){?>
            <div class="product-price productt1 text-center">
               <span class="price"> <i class="fa fa-inr"></i> <? echo $this->libsession->priceformat($val['price']);?></span>
            </div>
            <?}?>
            <?if($val['price'] >  $val['discountprice']){?>
            <div class="product-price" >
               <span class="price"> <i class="fa fa-inr"></i> <? echo $this->libsession->priceformat($val['discountprice']);?> </span><span></span>
               <span class="price-before-discount"><i class="fa fa-inr"></i> <? echo $this->libsession->priceformat($val['price']);?></span>
                <?if($val['percentage']){?>
               <span class="price off fn10"><?php echo $val['percentage']; ?>%</span>
               <?}?>
            </div>
            <?}}else{?>
            <?if($val['price'] >  $val['discountPrice']){?>
            <div class="product-price" >
               <span class="price"> <i class="fa fa-inr"></i> <? echo $this->libsession->priceformat($val['discountPrice']);?> </span><span></span>
               <span class="price-before-discount"><i class="fa fa-inr"></i> <? echo $this->libsession->priceformat($val['price']);?></span>
                <?if($val['percentage']){?>
               <span class="price off fn10"><?php echo $val['percentage']; ?>%</span>
               <?}?>
            </div>
            <?}}}?>
            <!-- /.product-price -->
         </div>
         <!-- /.product-info -->
         <div class="cart clearfix animate-effect efftct2">
            <div class="action">
               <?php if($val['callforprice']=='YES' || $val['avaliable'] ==0){ ?>
               <p class="add2cart"><a class="viewdetails" href="<? echo $val['url'];?>" title="Viewdetails">View Details</a></p>
              <? }else{ ?>
                <p class="add2cart"><a class="add-to-cart" href="JavaScript:Void(0);" title="Addtocart" onclick="addtocart('<?php echo $val['id']?>');">Add to cart</a></p>
              <? } ?>
               <ul class="list-unstyled">
                  <li class="lnk wishlist hp " >
                     <?if($val['callforprice']=='YES' ){?>
                     <a class="add-to-cart"  title="Wishlist" onclick="addtowishlist('<?echo $val['id'];?>');">
                            <i class="fa fa-heart pdl5" aria-hidden="true"></i> 
                            <div class="fn9">+ Wishlist</div>
                         </a>
                     <?}else{?>
                         <?if($val['wishlist'] >0 || $val['clearanceAvailable']>0){?>
                         <a class="add-to-cart" href="#" title="cart" onclick="addtocart('<?echo $val['id'];?>');">
                            <i class="icon fa fa-shopping-bag" aria-hidden="true"></i> 
                            <div class="fn6l"> + Cart</div>
                         </a>
                         <?}else{?>
                         <a class="add-to-cart"  title="Wishlist" onclick="addtowishlist('<?echo $val['id'];?>');">
                            <i class="fa fa-heart pdl5" aria-hidden="true"></i> 
                            <div class="fn9">+ Shortlist</div>
                         </a>
                         <?}?>
                     <?}?>
                  </li>
                  <!--  <li class="lnk wishlist"> <a class="add-to-cart" href="<? echo $val['url'];?>" title="exchange"> <i class="icon fa fa-exchange"></i> </a> </li> -->
                  <li class="lnk wishlist hp">
                     <a class="add-to-cart" href="<? echo $val['url'];?>" title="View Details">
                        <i class="icon fa fa-eye pd6"></i>
                        <div class="fn9">View Details</div>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <!-- /.cart -->
      </div>
      <!-- /.product -->
   </div>
   <!-- /.products -->
</div>