<div class="top-logo">
   <div class="container" style="padding:0;">
      <div class="row">
         <div class="col-md-12">
            <div class="col-xs-2">
               <a href="#" onclick="window.history.go(-1); return false;">
                  <!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
                  <i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
               </a>
            </div>
            <div class="col-xs-9 Favourite_list">
               <span class="mycartnew">My Cart (<?echo $cnt?>) </span> 
               <!-- | 
                  <span class="mubundlenew" style="opacity: 0.5;"><a href="<?echo SITEMOBURL;?>product/viewbundlecart" class="Favourite_list">Bundle Cart (<?echo $bundlecnt;?>)</a> </span> -->
            </div>
            <div class="col-xs-1" style="padding:0;">
               <!--<div class="col-xs-12">
                  <div class="col-xs-4 hidden-xs"></div>
                    <div class="col-xs-4 "><a href="#"><img src="img/icon/go.png" width="100%" ></a></div>
                   <div class="col-xs-4  hidden-xs"></div>

                  </div>-->

            </div>
         </div>
      </div>
   </div>
</div>
<div style="font-weight:bold;margin: 15px;/* width: 80%; */text-align: justify;">Please note: Due to Limited Quantities of Trinity College & ABRSM Books only 1 quantity, per book, per user/user address will be supplied. Any additional quantities will be cancelled and if advance money received will be refunded for the balance. Thank you for your understanding.</div>
<?php
   $totalamount=0;
   $totaldiscount=0;
   if($resp){
      foreach($resp as $key=>$val){ 
         $totalamount+=$val['dicountprice'] * $val['qty'];
         if($val['precentage']){
            $totaldiscount+=($val['price'] - $val['dicountprice']) * $val['qty'];
         }
         ?>

   <div class="listingtab">
     <!-- <div style="font-weight:bold;margin: 15px;/* width: 80%; */text-align: justify;">Deliveries are currently slower than usual. Due to regular revisions by our logistics service providers as well as in Government regulations and directives, delivery may be delayed depending on your location.</div>-->
   
      <div class="">
         <div class="ptc"  id="<?echo $key;?>">
            <table width="100%" class="listtb">
               <tbody>
                  <tr>
                     <td width="90px" class="listtd" style="border-right: 1px solid #ffffff" data-id="<?php echo $val['proid']?>">
                        <div class="listimg" style="width: 90px; height: 120px;">
                           <img src="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" 
                              width="100%">
                        </div>
                     </td>
                     <td style="border-right: 1px solid #ffffff">
                        <div class="dtext" style="-webkit-line-clamp: 2; -webkit-box-orient: vertical;">
                           <div>
                              <div class="titlecat" style="margin-top: -5px;"><?echo $val['proname'];?></div>
                           </div>
                        </div>
                        <!--<div class="txftdos ml">
                           <span class="ratnig">
                              <div class="rates">
                                 <div class="ratningbg" style="background-color: rgb(55, 190, 95);">
                                    <span><?php echo $val['ratings']; ?></span>
                                    <span>
                                    <img class="starbg" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1MTIiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgMzA2IDMwNiI+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTE1MyAyMzAuNzc1bDk0LjM1IDY4Ljg1LTM1LjctMTEyLjIgOTQuMzUtNjYuM0gxOTEuMjVMMTUzIDYuMzc1bC0zOC4yNSAxMTQuNzVIMGw5NC4zNSA2Ni4zLTM1LjcgMTEyLjJ6Ii8+PC9zdmc+">
                                    </span>
                                 </div>
                                 <span class="f8RGEP"> (<?php echo $val['review']; ?>)</span>
                              </div>
                           </span>
                        </div>-->
                        <div class="catdetails" style="margin-bottom: 5px;">
                           <span class="catsubt">
                           <i class="fa fa-rupee" style="font-size:12px;margin-right: 2px;"></i>
                           <?echo $this->libsession->priceformat($val['dicountprice']);?>
                           </span>
                        </div>
                        <span class="">
                           <?if($val['stock']=='0'){?>
                           <p class="stock"  style="padding-bottom: 8px;">Not Avaliable</p>
                           <?}else{?>
                           <p class="stock"  style="padding-bottom: 8px;">In Stock</p>
                           <?}?>
                        </span>
                        <span class="">
                           <div class="col-xs-2">Qty</div>
                           <div class="col-xs-3">
                              <?$qunty=!empty($val['qty']) ? $val['qty'] : '' ;?>
                              <select id="qtychange_<?echo $key;?>" name="qtychange" class="qtych"  value="<?echo $qunty;?>"  style="width:100%; background: #ffffff;box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);font-family: Roboto Medium,Roboto-Medium,Droid Sans,HelveticaNeue-Medium,Helvetica Neue Medium,sans-serif-medium;border: 1px solid #efefef70;padding: 3px 0px;border-radius: 3px;" onchange="updatecart(<?echo $key;?>);" >
                                 <option <?php if($qunty == 1) echo "selected"; ?> value="1">1</option>
                                 <option <?php if($qunty == 2) echo "selected"; ?> value="2">2</option>
                                 <option <?php if($qunty == 3) echo "selected"; ?> value="3">3</option>
                                 <option <?php if($qunty == 4) echo "selected"; ?> value="4">4</option>
                                 <option <?php if($qunty == 5) echo "selected"; ?> value="5">5</option>
                                 <option <?php if($qunty == 6) echo "selected"; ?> value="6">6</option>
                                 <option <?php if($qunty == 7) echo "selected"; ?> value="7">7</option>
                                 <option <?php if($qunty == 8) echo "selected"; ?> value="8">8</option>
                                 <option <?php if($qunty == 9) echo "selected"; ?> value="9">9</option>
                                 <option <?php if($qunty == 10) echo "selected"; ?> value="10">10</option>
                                 <option <?php if($qunty == 11) echo "selected"; ?> value="11">11</option>
                                 <option <?php if($qunty == 12) echo "selected"; ?> value="12">12</option>
                                 <option <?php if($qunty == 13) echo "selected"; ?> value="13">13</option>
                                 <option <?php if($qunty == 14) echo "selected"; ?> value="14">14</option>
                                 <option <?php if($qunty == 15) echo "selected"; ?> value="15">15</option>
                              </select>
                           </div>
                        </span>
                     </td>
                     <td width="35px" style="border-right: 1px solid #ffffff">
                        <div class="">
                           <div>
                              <div class="crtdelet">
                                 <div class="col-xs-4 delete"  onclick="removefromcart(<?echo $key;?>);">
                                    <img class="BfsEA4" alt="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjNjE2MTYxIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTQxMy43IDEzMy40Yy0yLjQtOS00LTE0LTQtMTQtMi42LTkuMy05LjItOS4zLTE5LTEwLjlsLTUzLjEtNi43Yy02LjYtMS4xLTYuNi0xLjEtOS4yLTYuOC04LjctMTkuNi0xMS40LTMxLTIwLjktMzFoLTEwM2MtOS41IDAtMTIuMSAxMS40LTIwLjggMzEuMS0yLjYgNS42LTIuNiA1LjYtOS4yIDYuOGwtNTMuMiA2LjdjLTkuNyAxLjYtMTYuNyAyLjUtMTkuMyAxMS44IDAgMC0xLjIgNC4xLTMuNyAxMy0zLjIgMTEuOS00LjUgMTAuNiA2LjUgMTAuNmgzMDIuNGMxMSAuMSA5LjggMS4zIDYuNS0xMC42bS0zNC4zIDQyLjZoLTI0Ni44Yy0xNi42IDAtMTcuNCAyLjItMTYuNCAxNC43bDE4LjcgMjQyLjZjMS42IDEyLjMgMi44IDE0LjggMTcuNSAxNC44aDIwNy4yYzE0LjcgMCAxNS45LTIuNSAxNy41LTE0LjhsMTguNy0yNDIuNmMxLTEyLjYuMi0xNC43LTE2LjQtMTQuNyIvPjwvc3ZnPg==" src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjNjE2MTYxIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTQxMy43IDEzMy40Yy0yLjQtOS00LTE0LTQtMTQtMi42LTkuMy05LjItOS4zLTE5LTEwLjlsLTUzLjEtNi43Yy02LjYtMS4xLTYuNi0xLjEtOS4yLTYuOC04LjctMTkuNi0xMS40LTMxLTIwLjktMzFoLTEwM2MtOS41IDAtMTIuMSAxMS40LTIwLjggMzEuMS0yLjYgNS42LTIuNiA1LjYtOS4yIDYuOGwtNTMuMiA2LjdjLTkuNyAxLjYtMTYuNyAyLjUtMTkuMyAxMS44IDAgMC0xLjIgNC4xLTMuNyAxMy0zLjIgMTEuOS00LjUgMTAuNiA2LjUgMTAuNmgzMDIuNGMxMSAuMSA5LjggMS4zIDYuNS0xMC42bS0zNC4zIDQyLjZoLTI0Ni44Yy0xNi42IDAtMTcuNCAyLjItMTYuNCAxNC43bDE4LjcgMjQyLjZjMS42IDEyLjMgMi44IDE0LjggMTcuNSAxNC44aDIwNy4yYzE0LjcgMCAxNS45LTIuNSAxNy41LTE0LjhsMTguNy0yNDIuNmMxLTEyLjYuMi0xNC43LTE2LjQtMTQuNyIvPjwvc3ZnPg==" style="width: 20px; height: 20px;">
                                 </div>
                                 <div class="lw3t_m">
                                    <div class="_3yFKkI"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>

<?}?>

<div class="_2xM545" data-aid="Cart-Final-Price-Breakup-Container">
   <div class="_29c-ns">
      <div class="_5wF10A">PRICE DETAILS</div>
      <div class="_1S3CAu">
         <table class="_2Grs28" data-aid="Cart-Final-Price-TABLE">
            <tbody>
               <tr>
                  <td class="_2gTpg8 _3IJaz8" style="border-right: none;border-bottom: none;">
                     <!-- react-text: 142 -->Price (<!-- /react-text --><!-- react-text: 143 --><?php echo COUNT($resp); ?><!-- /react-text --><!-- react-text: 144 --> item<!-- /react-text --><!-- react-text: 145 --><!-- /react-text --><!-- react-text: 146 -->)<!-- /react-text -->
                  </td>
                  <td class="_2gTpg8 _2lcKV2" style="border-right: none;border-bottom: none;"><span><i class="fa fa-rupee" style="font-size:12px;margin-right: 2px;"></i><?php echo $totalamount; ?></span></td>
               </tr>
               <!--<tr>
                  <td class="_2gTpg8 _3IJaz8" style="border-right: none;border-bottom: none;">Delivery</td>
                  <td class="_2gTpg8 _2lcKV2" style="border-right: none;border-bottom: none;"><span class="_3krGW8">Free</span></td>
               </tr>-->
               <tr class="QJ8IwB">
                  <td class="_2gTpg8 _3IJaz8" style="border-right: none;border-bottom: none;">Amount Payable</td>
                  <td class="_2gTpg8 _2lcKV2" style="border-right: none;border-bottom: none;"><span><i class="fa fa-rupee" style="font-size:12px;margin-right: 2px;"></i><?php echo $totalamount; ?></span></td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="_2mBVJt">
         <?php if($totaldiscount > 0){ ?>
            <!-- react-text: 158 -->You will save <!-- /react-text --><span><i class="fa fa-rupee" style="font-size:12px;margin-right: 2px;"></i><?php echo $totaldiscount; ?></span><!-- react-text: 160 --> on this order<!-- /react-text -->
         <?php } ?>
      </div>
   </div>
</div>
<div class="_26Y-TN">
   <img class="_1dYf55" src="http://img1a.flixcart.com/www/linchpin/checkout/shield-3ca85446.svg" alt="Trust Marker">
   <div class="ukjWuG">
      <div class="_2TfNoy">Safe and Secure Payments.100% Authentic products.</div>
      <div class="_2TfNoy"></div>
   </div>
</div>

<div class="_3ZGHaV">
   <div class="_67Fvi4 _2vocLi">
      <div class="_1B09cx">
         <div class="_2uOlTL">
            <div class="_1NpipM">
               <div class="_1O573a" data-aid="Cart-Price-Summary"><span>Total <i class="fa fa-inr" aria-hidden="true" style="font-size: 18px;color: #222222;margin-right: 4px"></i><?echo $this->libsession->priceformat($amount);?></span></div>
               
            </div>
            <div class="_8tG1gW" data-aid="Cart-CONTINUE_Button">
               <div class="_2TWPj6">
                  <!-- react-empty: 177 -->
                  <div class="_1C3neO _2h9Zp6 _1y96ch"><a href="<?php echo SITEMOBURL;?>product/checkoutuserdtl" style="color: #ffffff">CONTINUE</a></div>
                  <!-- react-empty: 179 -->
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
 <?} else {?> 
   <div class="Favourite">
		<div class="Favourite-no" style="background: white;height: 0; margin: 200px 25px;">
			<center><p>There are currently no products in the Cart.</p></center>
		</div> 
	</div>
   <!--<div class="Favourite">
      <div class="Favourite-no">
         <p>There are currently no products in the Cart !!</p> 
      </div> 
   </div>
   <div class="col-xs-12">
   No product present in the cart !!
   </div>-->
   <? }?>

<?/* First Container 
   <div style="padding-right:0px !important;padding-left:0px !important;">
      <div class="row">
         <div class="col-xs-12" >
            <div class="flex-container ">
               <div class="col-xs-6 Change_Address">Total &nbsp;<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?//echo $this->libsession->priceformat($amount);?></div>
               <div class="col-xs-6 Place_Order"><a href="<?php //echo SITEMOBURL;?>product/checkoutuserdtl">CONTINUE</a></div>
            </div>
         </div>
      </div>
   </div>
   <?} //else {?> 
   <div class="Favourite">
      <div class="Favourite-no">
         <p>   There are currently no products in the Wishlist !!</p> 
      </div> 
   </div>
   <div class="col-xs-12">
   No product present in the wishlist !!
   </div>
   <? }?>*/?>
<script>
   function updatecart(ele) {
      var pid=ele;
       var qty= jQuery('#qtychange_'+ele).val();
      // console.log(pid+' next '+qty); return false;
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/updatecart',
         type:'POST',
         data:{qty:qty,pid:pid},
         //alert(data); 
         success:function(data) {
         //console.log(data); return false;  
          var response = jQuery.parseJSON(data); 
           
            if(response.error=='false'){ 
                 alert('Cart updated successfully'); 
                 window.location.reload();
               }else{
                alert('Error in updating  product to cart');   
               }
            
         }
   
      });
      
       
   }
   function removefromcart(ele) {
      var pid=ele;
      
       jQuery.ajax({
         url:'<?php echo SITEMOBURL?>product/removefromcart',
         type:'POST',
         data:{pid:pid},
         success:function(data)
         {   var response = jQuery.parseJSON(data);
           
            if(response.error=='false'){
                 alert('Product  removed successfully'); 
                 window.location.reload();
               }else{
                alert('Error in removing  product from cart'); 
               }
            
         }
   
      });
      
       
   }
</script>
<script type="text/javascript">
   jQuery('.shownow').click(function(){
      jQuery('.sh-btn-menu').click();
   })
</script>
<script>
   // jQuery( document ).ready(function() {
   //  jQuery('.qtych').val('<?echo $val['qty']?>');
   // });
</script>