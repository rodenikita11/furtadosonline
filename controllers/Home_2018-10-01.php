<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller
{

    function __construct(){
        parent::__construct();
        $this->load->library('user_agent');
    }

    public function index($param1=false, $param2=false){
        $name1 = $this->uri->segment('1');
        $name  = str_replace("-", " ", $name1);
        $id    = $this->uri->segment('2');
        if ($name && $id) {
            $data['menus'] = $this->libmenu->get_dropdownmenu(); //Cached
            $param['id'] = intval($id);

            if ($_GET['filter']){
                $param['filterdtl'] = $_GET['filter'];
            }
            if ($_GET['search']) {
                $param['search'] = $_GET['search'];
            }
            if ($_GET['brand']) {
                $param['branddtl'] = $_GET['brand'];
            }
            if ($_GET['avaliable']) {
                $param['avaliable'] = $_GET['avaliable'];
            }
            if ($_GET['sort']) {
                $param['sort'] = $_GET['sort'];
            }
            if ($_GET['pricestart']) {
                $param['minprice'] = $_GET['pricestart'];
            }
            if ($_GET['pricend']) {
                $param['maxprice'] = $_GET['pricend'];
            }
            if ($_GET['percentage']) {
                $param['percentage'] = $_GET['percentage'];
            }
            $getdtl = $this->liburl->getinfotype($param);
            if ($_GET['sid']){
                $getdtl['type'] = 'product';
            }

            if ($name1 == 'giftcard'){
                if ($_REQUEST['start']){
                    $param['start'] = $_REQUEST['start'] + LIMIT;
                } else {
                    $param['start'] = '0';
                }

                $param['cid']            = intval($id);
                $data['banner_category'] = $this->lib_home->get_home_dtl(); //Cached
                $data['giftcarddetail']  = $this->liburl->get_categorydetails($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
               //echo $mobile; die;
                if (!empty($mobile)) {
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/offercard", $data);
                } else {

                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/offercard", $data);
                    $this->load->view('web/include/footer', $data);
                }
            } else if ($name1 == 'promotion') {
                $param['id']              = intval($id);
                $data['promotionproduct'] = $this->libprodtl->getproductpromotiondtl($param);
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/promotion_products", $data);
            } else if ($getdtl['type'] == 'product'){
                if(!$param['start']){
                    $param['start']=0;
                }
                
                $data['seomodule'] = 'product';
                $data['seoname']   = $this->uri->segment(1);
                $data['seoid']     = $this->uri->segment(2);

                $param['sid'] = urldecode($_GET['sid']);
                if (empty($param['sid'])){
                    $data['res']           = $this->liburl->get_productdetail($param);
                    $param['catid']        = $data['res']['catid'];
                    $data['comboproducts'] = $this->libpromooffer->getComboProductDetails($param);
                } else {
                    $data['res'] = $this->liburl->pro_clerance_dtl($param);
                }
                if ($data['res']['discountPrice'] >= MIN_PRICE_FOR_EMI_5_BANK){
                    $data['res']['emi_month']  = $this->pmt(13, 1, $data['res']['discountPrice']);
                    $data['res']['idMinPrice'] = $data['res']['emi_month'];
                }
                //print_r($data); die;
                $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param);
                $data['similar_product'] = $this->libprodtl->getsimiliar_prod($param);
                $data['res2']            = $this->libprodtl->getrecently_viewed($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
                $data['sid']           = urldecode($_GET['sid']);
                if (!empty($mobile)) {
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/product_detail", $data);
                } else {
                    //print_r($data['res']); die;
                    //print_r($data['similar_product']); die;
                    $param['name']         = 'categorydetail';
                    $data['productId']     = $id;

                    $data['categorynames'] = $this->libmenu->getprodtlbredacrum($param);
                    $data['res2']          = $this->liburl->get_categorydetails($param);
                    $data['resp']          = $this->lib_home->get_clerance();

                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/category_detail", $data);
                    $this->load->view('web/include/footer', $data);
                }

            } else if ($getdtl['type'] == 'category'){
                $data['seoname'] = $this->uri->segment(1);
                if ($_REQUEST['start']) {
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }
                $param['percentage1'] = $param['percentage'];
                $param['cid']   = $param['id'];
                $data['seoid']  = $param['cid'];
                $param['count'] = true;
                $data['res']    = $this->liburl->get_categorydetails($param); //Cached with param
                $data['secondsubcat'] = $this->libmenu->get_dropdownmenu(); //Cached
                $data['getfilter'] = $this->liburl->getfilter($param); //Cached with param
                $data['getsubcat'] = $this->liburl->getsubcat($param); //Cached with param
                $data['getcat']    = $this->liburl->getcat($param); //Cached with param
                $data['start']     = $param['start'];
                $mobile            = $this->agent->is_mobile();
                $desktop           = $this->agent->is_browser();
                if ($param['start'] > 0) {
                    if (!empty($mobile)) {
                        $this->load->view("mobile/ajax_productlist", $data);
                    } else {
                        $this->load->view("web/include/ajax_productlist", $data);
                    }
                } else {

                    $mobile  = $this->agent->is_mobile();
                    $desktop = $this->agent->is_browser();

                    if (!empty($mobile)) {
                        $this->load->view('mobile/include/header', $data);
                        $this->load->view("mobile/category_listing", $data);
                    } else {
                        $param['id']              = intval($id);
                        $catName                 = $this->uri->segment(1);
                        $data['catname']         = $catName;
                        $catId                   = $this->uri->segment(2);
                        $data['catid']           = $catId;
                        $data['maincaturl']      = ADMINURL . $catName . '/' . $catId;
                        $param['categoryname']   = $data['catname'];
                        $param['categoryno']     = $catId;
                        $data['getbrandname']    = $this->libmenu->getbrandname($param); //Cached with param
                        $param['name']           = 'categorymain';
                        $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param); //Cached with param
                        $data['getcatname']      = $this->libmenu->getcatname($param); //Cached with param
                        $data['banner_category'] = $this->lib_home->get_home_dtl(); //Cached
                        $this->load->view('web/include/header', $data);
                        $this->load->view("web/category", $data);
                        $this->load->view('web/include/footer', $data);
                    }
                }
            } else if ($getdtl['type'] == 'brand'){
                if ($_REQUEST['start']) {
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }
                $data['res'] = $this->liburl->getbranddtl($param);
                if(!empty($mobile)){
                    $data['banner']   = $this->lib_home->get_home_dtl(); //Cached
                    $data['category'] = $data['banner'];

                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/category_listing", $data);
                }else{
                    $data['leftsidefalse']=true;
                    $data['webbanner']       = $this->lib_home->get_home_dtl(); //Cached
                    $data['banner_category'] = $data['webbanner'];
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/category", $data);
                    $this->load->view('web/include/footer', $data);
                }
            }
        } else {
            $data['clerance']        = $this->lib_home->get_clerance();//Cached
            $data['newarrivals']     = $this->lib_home->newarrival(); //Cached
            $param['module']         = 'homehotdeals';
            $data['hotdeals']        = $this->lib_home->hotdeals($param); //Cached NO param in cache query
            $data['signupstate']     = $this->liblogin->getsignupStates();
            $data['newarrivalbooks'] = $this->lib_home->newarrivalbooks(); //Cached
            $data['menus']           = $this->libmenu->get_dropdownmenu();//Cached
            $mobile                  = $this->agent->is_mobile();
            $desktop                 = $this->agent->is_browser();

            if(!empty($mobile)){
                $data['banner']   = $this->lib_home->get_home_dtl();//Cached
                //print_r($data['banner']); die;
                $data['category'] = $data['banner'];
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/home", $data);
            }else{
                $data['webbanner']= $this->lib_home->get_home_dtl();
                //print_r($data['webbanner']); die;
                $data['banner_category'] = $data['webbanner'];
                $this->load->view('web/include/header', $data);
                $this->load->view("web/index", $data);
                $this->load->view('web/include/footer', $data);
            }
        }
    }

    public function priceFormat($amt)
    {
        setlocale(LC_MONETARY, 'en_IN');
        return money_format('%!.0n', floor($amt)) . '/-';
    }

    public function pmt($apr, $term, $loan)
    {
        $term   = $term * 12;
        $apr    = $apr / 1200;
        $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
        return $amount;
    }
}
?>
