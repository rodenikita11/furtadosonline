<div class="body-content" id="top-banner-and-menu">
<div class="container-fluid">
<div class="row">
<!-- ============================================== CONTENT ============================================== -->
<div class="col-xs-12 col-sm-12 col-md-12 outer-top-150">
<div class="container">
   <div class="row">
    <!-- <div style="font-weight:bold;margin: 25px;/* width: 80%; */text-align: justify;">Deliveries are currently slower than usual. Due to regular revisions by our logistics service providers as well as in Government regulations and directives, delivery may be delayed depending on your location.</div>-->
	<div style="font-weight:bold;margin: 25px;/* width: 80%; */text-align: justify;">Please note: Due to Limited Quantities of Trinity College & ABRSM Books only 1 quantity, per book, per user/user address will be supplied. Any additional quantities will be cancelled and if advance money received will be refunded for the balance. Thank you for your understanding.</div>
      <div class='col-md-8 sidebar'>
        
         <div class="side-menu animate-dropdown outer-bottom-xs outer-top-ss">
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
               <ul id="myTab" class="nav nav-tabs nav-tabs-responsive" role="tablist">
                  <li role="presentation" class="active">
                     <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">
                     <span class="text">Cart view (<?echo $cnt?>)</span>
                     </a>
                  </li>
                  <li role="presentation" class=" disabled">
                     <a href="#profile" role="tab" id="profile-tab" aria-controls="profile" style="display:<?php echo !empty($res['bundlecnt']) ? 'block' : 'none' ;?>"<?/*onclick="getbundlecart();*/?>">
                     <span class="text">Bundle View (<?echo $bundlecnt;?>)</span>
                     </a>
                  </li>
               </ul>
               <div id="myTabContent" class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active owerflow " id="home" aria-labelledby="home-tab">
                     <?php if($resp){
                         //print_r($resp); die;
                             foreach ($resp as $key => $val) { //print '<pre>'; print_r($val);
                                include('include/list_cart.inc'); 
                             }
                             }else{?>
                             <div class='col-sm-6 col-md-10 product-info-block'>
                                 <div class="product-info">
                                    <h1>No Product Available In Cart..</h1>
                                 </div>
                             </div>       
                             <?}?>  
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                     <?php if($resp['bundlecart']){
                        foreach ($resp['bundlecart'] as $key => $val) {
                           include('bundlecart.php'); 
                        }
                        }?>  
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?if($cnt > 0){?>
      <div class='col-md-4 sidebar'>
         <div class="side-menu animate-dropdown outer-bottom-xs outer-top-ss height-500">
            <div class="col-sm-12 padding-0">
               <div class="leftsidebar">
                  <h2>Order Summary</h2>
               </div>
            </div>
            <div class="col-md-12 col-sm-12" style="padding: 0px;">
               <table class="table">
                  <thead>
                     <!--<tr>
                        <th style="border-bottom: 0;padding: 15px 30px;">
                           <div class="cart-sub-total">
                              Subtotal (<?echo $cnt?> items)<span class="inner-left-md-220">&#8377; <?echo $amount;?></span>
                           </div>
                        </th>
                     </tr>
                     <tr>
                        <th style="border-bottom: 0;    padding: 0px 30px;">
                           <div class="cart-grand-total">
                              Delivery<span class="inner-left-md-220">Free Shipping </span>
                           </div>
                        </th>
                     </tr>-->
                     <?php if(count($resp) > 0){
                        $rightsectionsamount=0;
                        foreach($resp as $productskey=>$productsval){
                              $rightsectionsamount+=$productsval['amount'];
                           ?>
                           <tr>
                              <th style="border-bottom: 0;padding: 8px 10px;">
                                 <div class="cart-sub-total">
                                    <b><?php echo substr($productsval['proname'], 0, 10); ?>...</b>(<?echo $productsval['qty']; ?> item<?php echo ($productsval['qty'] > 1) ? 's' : ''; ?>)<span class="inner-left-md-220"> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat($productsval['amount']);?></span>
                                 </div>
                                 <?php if(is_array($productsval['combo_product'])){ ?>
                                 <table class="table comboproductsincart">
                                    <?php foreach($productsval['combo_product'] as $combokey=>$comboval){
                                       $rightsectionsamount+=$comboval['dicountprice'];
                                       ?>
                                    <tr>
                                       <th style="border-bottom: 0;">
                                          <div class="cart-sub-total">
                                             <?php echo substr($comboval['proname'], 0, 10); ?>...(1 item)<span class="inner-left-md-220">
                                              <i class="fa fa-rupee" style="font-size:14px"></i>
                                              <?php echo $this->libsession->priceformat($comboval['dicountprice']); ?></span>
                                          </div>
                                       </th>
                                    </tr>
                                    <?php } ?>
                                 </table>
                                 <?php } ?>
                              </th>
                           </tr>
                     <?php } } ?>
                     <tr style="height: 20px;"></tr>
                     <tr>
                        <th style="    border-top: 1px solid #ccc ! important;    background: #f5f5f5;">
                           <div class="cart-grand-total">
                              <b> SubTotal</b><span class="inner-left-md-220"><b> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat($rightsectionsamount); ?></b></span>
                           </div>
                        </th>
                     </tr>
                  </thead>
                  <!-- /thead -->
               </table>
               <!-- /table -->
            </div>
            <!-- /.cart-shopping-total -->
            <div class="col-md-12">
               <?if($this->libsession->isSetSession('usrid')){ ?>
               <!--<a href="<?echo SITEMOBURL?>product/webcheckout">--><button type="button" onclick="checkStock();" class="btn orderbtn">Place order</button>
               <? }else{ ?>
               <a href="#login" class="dropdown-toggle" data-hover="dropdown" data-toggle="modal"><button type="button" class="btn orderbtn">Place order</button></a>
               <?}?>
            </div>
            <div class="col-md-12">
               <a href="<?echo SITEMOBURL?>">
                  <h4 class="shopping"> &#60; Continue shopping</h4>
               </a>
            </div>
         </div>
      </div>
      <?}?>
   </div>
</div>
<style>
.comboproductsincart span, .comboproductsincart div{font-size: 12px;}
.qtycart {
    padding: 6px 10px ! important;
}
.form-control {
    padding: 6px 10px ! important;
    
}
div#slide1 img {
    width: 100px;
    padding-top: 10px;
}
</style>
<script type="text/javascript">
	function checkStock(){
		var flag = 0;
		$(".value1").each(function() {
			if($(this).text() == "out of Stock"){
				flag++;
				console.log("in --"+flag);
			}
		});
		console.log("out --"+flag);
		if (flag > 0)
			alert('Please remove out of stock products from cart to continue shopping.');
		else{
			window.location.href = '<?echo SITEMOBURL?>product/webcheckout';
		}
   }
</script>