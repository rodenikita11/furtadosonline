        <section class="block-chess-banners">
            <div class="block">
                <div class="container">
                    <div class="header-for-dark">
                        <h1 style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="wow fadeInRight animated animated" data-wow-duration="2s">New <span>Collections</span></h1>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInLeft animated" data-wow-duration="2s">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="#"><img class="img-responsive" src="<?php echo SITEIMAGES;?>banner6.jpg" alt=""></a>

                                    </div>
                                    <div class="col-md-8">
                                        <div class="chess-caption-right">
                                            <a href="#" class="col-name">Modern collection</a>
                                            <p>
                                                Lorem ipsum dolor sit 
amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore 
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-3">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInRight animated" data-wow-duration="2s">
                                <a href="#"><img class="img-responsive" src="<?php echo SITEIMAGES;?>banner2.jpg" alt=""></a>
                            </article>
                        </div>
                    </div> 
                    <div class="row">

                        <div class="col-md-3">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInLeft animated" data-wow-duration="2s">
                                <a href="#"><img class="img-responsive" src="<?php echo SITEIMAGES;?>banner1.jpg" alt=""></a>
                            </article>
                        </div>
                        <div class="col-md-9">
                            <article style="visibility: visible;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="block-chess wow fadeInRight animated" data-wow-duration="2s">
                                <div class="row">

                                    <div class="col-md-8">
                                        <div class="chess-caption-left">
                                            <a href="#" class="col-name">Classic collection</a>
                                            <p>
                                                Lorem ipsum dolor sit 
amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="#"><img class="img-responsive" src="<?php echo SITEIMAGES;?>banner5.jpg" alt=""></a> 
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>