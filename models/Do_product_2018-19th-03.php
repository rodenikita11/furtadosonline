<?php
class Do_product extends CI_Model{

  function __construct(){
    parent:: __construct();
    $this->load->database();
  }

  public function getprodtl($param){
      $select="select pm.proname,pm.proid,pm.prothumbnail,pm.isAvailable,pm.onlinePrice,pm.embedCode,pm.discountPrice,pm.prodescription,pm.prolongspecification,pm.callforprice,pc.category_id, pm.isBook,cm.catname,cm.catparent_id, cm.minprice,cm.maxprice, count(rm.reviewid)as rvcnt,rm.overallrating ,rm.reviewauthor,rm.myreview,rm.reviewdate_added,rm.reviewtitle, bm.brandid, bm.brandname,bm.brandimage, bm.branddescription from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on  find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  where pm.proid='".$param['id']."' order by pm.isAvailable desc";
      //echo $select; die;
   $res=$this->db->query($select);
   if($res){
     $res1['prodata']=$res->result_array();
    }
    $select1="select pro_img_id ,proimagetitle,pro_image,pro_img_sort_order from product_image where proid='".$param['id']."'";

    $res2=$this->db->query($select1);
     if($res2){
     $res1['proimage']=$res2->result_array();
       }
      //$review="select reviewid, reviewauthor,reviewrating, reviewtext, overallrating, reviewtitle,reviewdate_modified,myreview,status  from review  where proid='".$param['id']."' order by reviewid desc limit 5";
      $review="select (select COUNT(reviewid) from review where proid='".$param['id']."')as totalreview,(select avg(reviewrating) from review where proid='".$param['id']."')as avgrating,reviewid, reviewauthor,reviewrating,experiencewithproduct ,mybackground ,myreview , reviewtext, overallrating, reviewtitle,reviewdate_modified,myreview,status  from review  where proid='".$param['id']."' and status=1 order by reviewid desc limit 5";

    $res3=$this->db->query($review);
    if($res3){
      $res1['rewview']=$res3->result_array();
    }
    return $res1;
  }

  public function insertsearchterm($param){
      $insert="INSERT INTO searchTerm SET searchTerm='".$param['search']."'"; 
      if($param['catid']){
          $insert.=", catid=".$param['catid']."";
      }
      if($param['level']){
          $insert.=", catlevel=".$param['level'].""; 
      }
      if($param['catname']){
          $insert.=", catname='".$param['catname']."'"; 
      }

      $insert.=", insertDateTime=NOW(), ipAddress='".$param['termip']."'";
      return $this->db->query($insert);
  }

  public function searchproductcount($param){
        $seachwords=explode(' ',$param['search']);
        $wherecond=false;
        //if($param['start'] ==0){
        if($param['sort'] && $param['sort']=='highprice'){
          $wherecond=", pm.discountprice DESC ";
        }else if($param['sort'] && $param['sort']=='lowprice'){
          $wherecond=", pm.discountprice ASC ";
        }
        $soundexAddons = array();
        //print_r($seachwords); die;
        foreach ($seachwords as $word){
          if(!is_int($word)){
              $soundexAddons[] = soundex($word);
          }else{
              //$soundexAddons=$word;
              $soundexAddons[] = soundex($word);
          }
        }

        $soundex=implode(' ',$soundexAddons);
      
      $select="SELECT COUNT(pm.proid) cnt FROM `product_master` pm LEFT join brand_master bm
            on pm.`brandid`=bm.brandid join product_to_category pc on pm.proid=pc.proid join category_master cm1 on
            pc.category_id=cm1.catid left join category_master cm2 on cm1.catparent_id=cm2.catid left
            join category_master cm3 on cm2.catparent_id=cm3.catid left join category_master cm4
            on cm3.catparent_id=cm4.catid left join category_master cm5 on cm4.catparent_id=cm5.catid
            join autosuggestkeywords ak on pm.proid=ak.proid where

            (Match (ak.proname, ak.proid_ft) AGAINST ('".$param['search']."' IN BOOLEAN MODE)

            OR MATCH(ak.proid_ft, pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch)

            AGAINST ('".$param['search']."' IN BOOLEAN MODE)

            OR MATCH(pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch )

            AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE))";
            $res=$this->db->query($select);
            $results=$res->result_array();
            return $results[0]['cnt'];
  }

  /*created by vikram 17/02/2018*/
  public function getbranddetail($param){
    //print_r($param);die;
    $select="SELECT bm.brandname,bm.brandid,(SELECT count(pm.proid) from product_master pm join product_to_category pc on pc.proid=pm.proid where brandid=bm.brandid ) as probrandcnt from brand_master bm join product_master pm on bm.brandid=pm.brandid join product_to_category pc on pm.proid=pc.proid where bm.brandname LIKE '%".$param['brandsearch']."%'  GROUP by bm.brandid";
     /*$select="SELECT brandname FROM brand_master where brandname LIKE '%".$param['brandsearch']."%'";*/
    $res=$this->db->query($select);
    if($res){
      return $res1=$res->result_array();
    }
   // return $res1;
  }
  public function addenquirydtl($param){
    //print_r($param);die;
     $select="INSERT INTO  informMe  SET productId='".$param['productId']."',emailAddress='".$param['email']."',mobileNo='".$param['mobile']."',fullName='".$param['fullname']."',landlineNo='".$param['landline']."',comments='".$param['comments']."',bestTime='".$param['time']."',ipAddress='".$param['ip']."',insertDate=now() ";
     $res=$this->db->query($select);
     //print_r($res);
     if($res){
      return $res;
     }
  }
  public function addpianoenquirydtl($param){
    //print_r($param);die('hiii');
   $select="INSERT INTO pianoRegistration SET
      fname='".$param['fullname']."',phone='".$param['landline']."',mobile='".$param['mobile']."',email='".$param['email']."',inter='".$param['interest']."',rtime='".$param['time']."',budget='".$param['budget']."',ipaddress='".$param['ip']."',insertdate=now() ";
     $res=$this->db->query($select);
     if($res){
      return $res;
     }
  }

  /*created by vikram 17/02/2018: End*/

  public function getcatdtl($param){
    //$param['count']=false;
    if($param['count']){
      $cat="select count(pm.proid) count from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on '".$param['id']."'=cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid ";

    //echo $cat; die;
    }else{
      //$cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid ";
      //$cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice,pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,(select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on '".$param['id']."' =cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid ";
      //$cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice,pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,(select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname, bm.branddescription, (SELECT cm2.catname from category_master cm1 join category_master cm2 on cm1.catparent_id=cm2.catid where cm1.catid='".$param['id']."' )as catparentname,(SELECT cm2.catid from category_master cm1 join category_master cm2 on cm1.catparent_id=cm2.catid where cm1.catid='".$param['id']."' )as catparentid from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on '".$param['id']."' =cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid";

      $cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice,pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.seo_metadesc, cm.seodescription, (SELECT seodescription FROM category_master WHERE catid='".$param['id']."') seodescription,  cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,(select catmetatitle from category_master where catid='".$param['id']."') as catmeta_title,(select catmeta_description from category_master where catid='".$param['id']."') as catmeta_description,(SELECT catmeta_keyword from category_master where catid='".$param['id']."' )as catmeta_keyword,(select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname, bm.branddescription, (SELECT cm2.catname from category_master cm1 join category_master cm2 on cm1.catparent_id=cm2.catid where cm1.catid='".$param['id']."' )as catparentname,(SELECT cm2.catid from category_master cm1 join category_master cm2 on cm1.catparent_id=cm2.catid where cm1.catid='".$param['id']."' )as catparentid from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on  pc.category_id=cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid";
    }


    if($param['id'] > 3){

      $cat.=" WHERE find_in_set('".$param['id']."',pc.category_path)";
    }else{
        $cat.=" WHERE cm.catparent_id='".$param['id']."'";
    }
   // $cat.=" where find_in_set('".$param['id']."',pc.category_path)";
    foreach($param['allattribute'] as $key=>$val){
      $qstr.=" and pa.attriid='".$val."'";

    }

    if($param['search']){
      $qstr.=" and (match(pm.proname) against ('".$param['search']."*' in boolean mode) OR match(pm.proid) against ('".$param['search']."*' in boolean mode))";
    }

      if($param['branddtl'] ){
       $param['branddtl'] =implode($param['branddtl'],',');
      $qstr.=" and bm.brandid in(".$param['branddtl'].")";

     }
      if($param['avaliable'] ){
       $param['avaliable'] =implode($param['avaliable'],',');
      $qstr.=" and pm.isAvailable in(".$param['avaliable'].")";

     }
     if($param['minprice'] and $param['maxprice']){
        $qstr.=" and pm.discountPrice BETWEEN  '".$param['minprice']."' AND    '".$param['maxprice']."'";
    }

    if(($param['start'] || $param['start']==0) && !$param['count']){
        $qstr.=" GROUP BY pm.proid ";
    }

     if($param['sort'] =='popular' ){
        $qstr .=" order by rm.overallrating desc ";

         }else if($param['sort'] =='lowprice' ){
      $qstr .=" order by pm.discountprice asc ";

         }
     else if($param['sort'] =='highprice'){

        $qstr .=" order by pm.discountprice desc ";
      }else{
        //$qstr .=" order by  pm.discountprice asc ";
         $qstr .=" order by rm.overallrating desc ";
     }


    if(($param['start'] || $param['start']==0) && !$param['count']){
        $qstr.=" limit ".$param['start'].",".LIMIT."  ";
    }

   /* if(empty($param['count']) || !empty($param['search'])){
     $cat=$cat.$qstr;
    }else{
       $cat=$cat;
    }*/
    $cat=$cat.$qstr; //newly added by sandeep On 2018-12-28 for pagination and above code was commented
    //echo $cat; die;

    //$cat=$cat.$qstr; //Added by Sandeep for ajax loaded product list//again commemted by viki 16/2/2018 bcoz of product not shows anything
    if($param['start']){
        //print_r($param); die;
        //echo $cat; die;
    }

    $res=$this->db->query($cat);
    if($res){
     $res1['product']=$res->result_array();
     //print_r($res1['product']); die;
       }
     // echo $param['count'];
     if($param['id']){

     //check if the search parameter is SKU

     //$select1="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname,ps.quantity,ps.price from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid join product_seller ps on ps.proid=pm.proid  left join product_attribute_mapping pa on pm.proid=pa.proid where cm.catid='".$param['id']."' and categoryFeatured='Yes'";

     // $select1="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname,ps.quantity,ps.price from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid join product_seller ps on ps.proid=pm.proid  left join product_attribute_mapping pa on pm.proid=pa.proid where cm.catid='".$param['id']."' and categorySpotlight='Yes'";

      $select1="select pm.proname, pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,(select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname, bm.branddescription, ps.quantity,ps.price from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_seller ps on pm.proid=ps.proid left join product_attribute_mapping pa on pm.proid=pa.proid where cm.catid='".$param['id']."' and categorySpotlight='Yes' ORDER BY pm.userReviewCnt ASC LIMIT 0, 12";

             //$select1.=$select1.$qstr;

       $resfeat=$this->db->query($select1);
             if($resfeat){
         $res1['featured_product']=$resfeat->result_array();
         }
        }//count close
      //} // id close

      if($param['id']){
      $banner="select img_path,img_link,img_order,id from banner_master where catid='".$param['id']."' and displaytype='MobileCategory' and status=1";
      $resbanner=$this->db->query($banner);
      if($resbanner){
      $res1['cat_banner']=$resbanner->result_array();
      }
      $bannerweb="select img_path,img_link,img_order,id from banner_master where FIND_IN_SET('".$param['id']."',catid) and displaytype='Category' and status=1 limit 1";
      $resbannerweb=$this->db->query($bannerweb);
       if($resbannerweb){
        $res1['cat_bannerweb']=$resbannerweb->result_array();
       }
      // print_r($res1['cat_bannerweb']); die;
      }
      return  $res1;

  }

  public function getbrnaddtl($param){
    if($param['count']){
      $brand="select count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid";
    }else{
       $brand="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,rm.overallrating,(SELECT count(reviewid) from review where proid=pm.proid) as rvcnt,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid";
    }

    $brand.=" where bm.brandid='".$param['id']."'";
     foreach($param['allattribute'] as $key=>$val){
      $brand.=" and pa.attriid='".$val."'";
     }

    $brand.=" group by pm.proid";
    $brand.=" limit ".$param['start'].",".LIMIT."  ";
    
    $res=$this->db->query($brand);
    if($res){
     return $res1=$res->result_array();
      }
  }

  public function getinfotype($param){
    if($param['id']){

      $select ="select proid, proname  from product_master where  proid='".$param['id']."'";

      $res=$this->db->query($select);


      if ($res->num_rows() > 0 ){

         $data['type']='product';
         return $data;

      }else{
        //$name=str_replace(array(' ', '  '), ' & ', $param['name']);
        //$cat="select catid,catname from category_master where catid='".$param['id']."' AND (REPLACE(catname, ' & ', ' ')='".$param['name']."' OR catname='".$name."')";

        $cat="select catid, catname from category_master where catid='".$param['id']."' AND MATCH(catname) AGAINST('".$param['name']."')";
       
        $query1=$this->db->query($cat);
        //echo $this->db->last_query(); die;
        
        if($query1->num_rows() > 0){

         $data['type']='category';
         return $data;
       }else{
                 $brand="select brandid,brandname from brand_master where brandid='".$param['id']."'";
                 $query2=$this->db->query($brand);
            if($query2->num_rows() > 0)
            {
               $data['type']='brand';
                return $data;
            }else{
               $data['type']='invalidtype';
                return $data;
            }
        }
      }
    }

  }


  public function getsimiliar_prod($param){
    if($param['id']){

         $select="select pm.proid,pm.proname,pm.prothumbnail,pm.discountPrice,pm.onlinePrice,pm.callforprice,pm.isAvailable,(select category_id from product_to_category where proid='".$param['id']."') as category_id from product_master pm  join product_to_category pc on pm.proid=pc.proid where pm.onlinePrice>=(select (onlinePrice-(onlinePrice*".SIMPER.")/100) from product_master where proid='".$param['id']."') and pm.onlinePrice<=(select (onlinePrice+(onlinePrice*".SIMPER.")/100) from product_master where proid='".$param['id']."') and FIND_IN_SET((select category_id from product_to_category where proid='".$param['id']."'),pm.categorypath) and pm.proactive='1'  LIMIT 6";

      $res=$this->db->query($select);
      if($res){
        return $res1['prodata']=$res->result_array();

       }
    }
  }

  public function insert_review($param){
    if($param){


      $insert="insert into  review SET proid='".$param['pid']."', customerid='".$param['usrid']."' , reviewtext='".$param['reviewmessage']."', reviewrating='".$param['ratingme']."',reviewauthor='".$param['usrname']."',author_email='".$param['usremail']."', reviewtitle='".$param['reviewtitle1']."', reviewdate_added=now(), status=0 ON DUPLICATE KEY UPDATE  reviewtext='".$param['reviewmessage']."',reviewauthor='".$param['usrname']."',reviewrating='".$param['ratingme']."',reviewtitle='".$param['reviewtitle1']."', status=0, reviewdate_added=now() ";
      //echo $insert; die;


      $res=$this->db->query($insert);
      if($res){
         $update="update product_master set userReviewCnt=userReviewCnt+1 where proid='".$param['pid']."'";
        // print $update; die;
         $res1=$this->db->query($update);
          if($res1){
            return true;
          }
        }
    }

  }
  //product auto suggest
  public function getproductautosuggect($param){
      if($param['type']=='cat'){
      $select="select b.id, d.catid as catid, a.proname as pn,a.proid, c.category_id, d.catname as cn , e.catname as pct, f.catname as rt ,e.catid as pid , f.catid as rid
        from product_master a
        join product_seller b
        on a.proid = b.proid
        join product_to_category c
        on a.proid = c.proid
        join category_master d
        on c.category_id = d.catid
        left join category_master e
        on d.catparent_id = e.catid
        left join category_master f
        on e.catparent_id = f.catid
        where Match (a.proname)
        AGAINST ('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) and proactive = 1 group by d.catparent_id limit 10;";
      }else if($param['type']=='pro'){
         $select="select a.proname as pn ,a.proid, f.id from product_master a join product_seller f on a.proid = f.proid  where Match (a.proname) AGAINST ('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) and proactive = 1 limit 5;";
      }
      else if($param['type']=='brand'){
         $select="SELECT brandname , brandid FROM brand_master where match(brandname) against('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) limit 5;";

      }else if($param['type']=='catlist'){

        $select = "SELECT d.catid as catid,d.catname as cn ,e.catid as pid,e.catname  as pct,f.catid  as rid , f.catname as rt FROM category_master d left join category_master e on d.catparent_id = e.catid left join category_master f on e.catparent_id = f.catid WHERE d.catname like '".$this->dbc->escape($param['kw'])."%' limit 5";

      }

      $res=$this->db->query($select);
        if($res){
         return  $res1=$res->result_array();

       }
  }
  //product search
public function productsearch($param){
    if(!empty($param['search'])){
        $seachwords=explode(' ',$param['search']);
        $wherecond=false;
        //if($param['start'] ==0){
            if($param['sort'] && $param['sort']=='highprice'){
              $wherecond=", pm.discountprice DESC ";
            }else if($param['sort'] && $param['sort']=='lowprice'){
              $wherecond=", pm.discountprice ASC ";
            }
            $soundexAddons = array();
            //print_r($seachwords); die;
            foreach ($seachwords as $word){
              if(!is_int($word)){
                  $soundexAddons[] = soundex($word);
              }else{
                  //$soundexAddons=$word;
                  $soundexAddons[] = soundex($word);
              }
            }

            $soundex=implode(' ',$soundexAddons);

            /*$select2="SELECT

            ((Match (ak.proname, ak.proid_ft) AGAINST ('".$param['search']."' IN BOOLEAN MODE) +

            MATCH(ak.proid_ft, pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch )

            AGAINST ('".$param['search']."' IN BOOLEAN MODE) ) *

            MATCH(pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch )

            AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) )

            as matchScore,

            pm.proid,pm.proname,pm.prothumbnail,pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable,
            bm.brandname,cm1.catname as 'first_category',cm2.catname as 'second_category',cm3.catname as 'third_category',
            cm4.catname as 'fourth_category', cm5.catname as 'fifth_category', cm1.catid firstcatid FROM `product_master` pm LEFT join brand_master bm
            on pm.`brandid`=bm.brandid join product_to_category pc on pm.proid=pc.proid join category_master cm1 on
            pc.category_id=cm1.catid left join category_master cm2 on cm1.catparent_id=cm2.catid left
            join category_master cm3 on cm2.catparent_id=cm3.catid left join category_master cm4
            on cm3.catparent_id=cm4.catid left join category_master cm5 on cm4.catparent_id=cm5.catid
            join autosuggestkeywords ak on pm.proid=ak.proid where

            (Match (ak.proname, ak.proid_ft) AGAINST ('".$param['search']."' IN BOOLEAN MODE)

            OR MATCH(ak.proid_ft, pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch)

            AGAINST ('".$param['search']."' IN BOOLEAN MODE)

            OR MATCH(pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch )

            AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE)) ";*/


            /*$select2="SELECT

            ((Match (ak.proname, ak.proid_ft) AGAINST ('".$param['search']."' IN BOOLEAN MODE) +

            MATCH(ak.proid_ft, pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch )

            AGAINST ('".$param['search']."' IN BOOLEAN MODE) ) )
            as matchScore,

            pm.proid,pm.proname,pm.prothumbnail,pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable,
            bm.brandname,cm1.catname as 'first_category',cm2.catname as 'second_category',cm3.catname as 'third_category',
            cm4.catname as 'fourth_category', cm5.catname as 'fifth_category', cm1.catid firstcatid FROM `product_master` pm LEFT join brand_master bm
            on pm.`brandid`=bm.brandid join product_to_category pc on pm.proid=pc.proid join category_master cm1 on
            pc.category_id=cm1.catid left join category_master cm2 on cm1.catparent_id=cm2.catid left
            join category_master cm3 on cm2.catparent_id=cm3.catid left join category_master cm4
            on cm3.catparent_id=cm4.catid left join category_master cm5 on cm4.catparent_id=cm5.catid
            join autosuggestkeywords ak on pm.proid=ak.proid where

            (Match (ak.proname, ak.proid_ft) AGAINST ('".$param['search']."' IN BOOLEAN MODE)

            OR MATCH(ak.proid_ft, pronamesearch,firstcatnamesearch,secondcatnamesearch,thirdcatnamesearch,fourthcatnamesearch,fifthcatnamesearch,brandnamesearch)

            AGAINST ('".$param['search']."' IN BOOLEAN MODE)) ";

            if($param['catid'] && $param['level']=='2'){
                $select2.=" AND ak.second_catid=".$param['catid']."";
            }else if($param['catid'] && $param['level']=='3'){
                 $select2.=" AND ak.third_catid=".$param['catid']."";
            }

            if($param['brandid'] || $param['branddtl']){
              if($param['branddtl']){
                  $rawbranddtl=ltrim($param['branddtl'], '[');
                  $param['brandid']=rtrim($rawbranddtl, ']');

                  //echo $param['brandid']; die;
              }
              //$select2.=" AND bm.brandid=".$param['brandid']."";//old query for brand filter
              $select2.=" AND bm.brandid IN (".$param['brandid'].")";
            }
            //echo $select2; die;
            $select2.=" order by cm1.catsort_order DESC, matchScore DESC ";
            if($wherecond){
                $select2.=$wherecond;
            }
            $select2.="limit ".$param['start'].", ".LIMIT."";*/

            /*$select2="SELECT
                        *
                          ,(
                              (
                                  MATCH(
                                      ak.proname,
                                      ak.proid_ft,
                                      pronamesearch
                                  ) AGAINST('LP Mini Timbales Pre Pack LP845' IN BOOLEAN MODE) 

                                  + MATCH(
                                      firstcatnamesearch,
                                      secondcatnamesearch,
                                      thirdcatnamesearch,
                                      fourthcatnamesearch,
                                      fifthcatnamesearch,
                                      brandnamesearch
                                  ) AGAINST('LP Mini Timbales Pre Pack LP845' IN BOOLEAN MODE)
                              )
                          ) AS matchScore
                          
                      FROM
                          autosuggestkeywords ak

                      WHERE
                          (
                              MATCH(
                                  proname,
                                  proid_ft,
                                  pronamesearch
                              ) AGAINST('LP Mini Timbales Pre Pack LP845' IN BOOLEAN MODE) 
                              
                              OR MATCH(
                                  
                                  firstcatnamesearch,
                                  secondcatnamesearch,
                                  thirdcatnamesearch,
                                  fourthcatnamesearch,
                                  fifthcatnamesearch,
                                  brandnamesearch
                              ) AGAINST('LP Mini Timbales Pre Pack LP845' IN BOOLEAN MODE)
                              
                          )
                      ORDER BY
                          matchScore
                      DESC
                      LIMIT 0, 24";
                      echo $select2; die;*/
             //$select2="SELECT ak.first_catname first_category,  ak.second_catname second_category, ak.third_catid, ak.third_catname third_category, ak.fourth_catname fourth_category, ak.fifth_catname fifth_category, pm.proid,pm.proname, pm.prothumbnail, pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable, ((MATCH(ak.proname,ak.proid_ft,pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) + MATCH(firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE))) AS matchScore FROM autosuggestkeywords ak LEFT JOIN product_master pm ON ak.proid=pm.proid WHERE (MATCH(ak.proname, ak.proid_ft, ak.pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) OR MATCH(firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE))";

            $matchagainst=($param['module']=='brandsfilters') ? "ak.brandname" : "firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch";
            $matchagainst1=($param['module']=='brandsfilters') ? "ak.brandname" : "ak.proname, ak.proid_ft, ak.pronamesearch";

            if($param['module']=='brandsfilters'){
                $select2="SELECT ak.first_catname first_category,  ak.second_catname second_category, ak.third_catid, ak.third_catname third_category, ak.fourth_catname fourth_category, ak.fifth_catname fifth_category, pm.proid,pm.proname, pm.prothumbnail, pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable, ((MATCH(ak.proname,ak.proid_ft,pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) + MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))) AS matchScore FROM autosuggestkeywords ak LEFT JOIN product_master pm ON ak.proid=pm.proid WHERE ak.brandid='".$param['brandid']."'";
            }else{
                $select2="SELECT ak.first_catname first_category,  ak.second_catname second_category, ak.third_catid, ak.third_catname third_category, ak.fourth_catname fourth_category, ak.fifth_catname fifth_category, pm.proid,pm.proname, pm.prothumbnail, pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable, ((MATCH(ak.proname,ak.proid_ft,pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) + MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))) AS matchScore FROM autosuggestkeywords ak LEFT JOIN product_master pm ON ak.proid=pm.proid WHERE (MATCH(".$matchagainst1.") AGAINST('".$param['search']."' IN BOOLEAN MODE) OR MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))";
            }

            if($param['catid'] && $param['level']=='2'){
                $select2.=" AND ak.second_catid=".$param['catid']."";
            }else if($param['catid'] && $param['level']=='3'){
                $select2.=" AND ak.third_catid=".$param['catid']."";
            }

            /*if($param['brandid'] || $param['branddtl']){
              if($param['branddtl']){
                  $rawbranddtl=ltrim($param['branddtl'], '[');
                  $param['brandid']=rtrim($rawbranddtl, ']');

                  //echo $param['brandid']; die;
              }
              //$select2.=" AND bm.brandid=".$param['brandid']."";//old query for brand filter
              $select2.=" AND ak.brandid IN (".$param['brandid'].")";
            }*/

            if($param['maincat']){
                $select2.=" AND ak.first_catid=".$param['maincat']."";
            }

             $select2.=" ORDER BY matchScore DESC ".$wherecond."";
             $select2.=" limit ".$param['pagestart'].", ".LIMIT."";

             if($_SERVER['REMOTE_ADDR']=='162.158.22.138'){
                //echo $select2; die;
             }

           // echo $select2; die;
            
            $res2=$this->db->query($select2);
            if($res2){
              //print_r($res2->result_array()); die;
              return $result=$res2->result_array();
            }
        //}
      }else{
        echo "search parameter missing"; die;
      }
}

      public function catfilter($param){
          if(!empty($param['search']) || $param['module']){
            //old query commented by Sandeep on 2018-06-29 because of results mismatch
              $select="SELECT DISTINCT(third_catid) third_catid, `third_catname`,`first_catid`,`second_catid`,`first_catname`,`second_catname` FROM autosuggestkeywords ak WHERE ";

              if($param['module'] && $param['module']=='clearancefilter'){
                  $wherecond="proid IN (".rtrim($param['product_ids'], ',').")";
              }else if($param['module'] && $param['module']=='brandsfilters'){
                  $wherecond="brandid=".$param['brandid']."";
              }else{
                $wherecond="( MATCH(ak.proname, ak.proid_ft) AGAINST('".$param['search']."' IN BOOLEAN MODE) OR MATCH( ak.proid_ft, pronamesearch, firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch ) AGAINST('".$param['search']."' IN BOOLEAN MODE))";
                if($param['maincat']){
                    $wherecond.=" AND ak.first_catid=".$param['maincat']."";
                }
                $wherecond.=" ORDER BY `first_catid` ASC";
              }

              $select=$select.$wherecond;
              //echo $select; die;
              /*$select="SELECT SQL_CACHE DISTINCT(ak.third_catid) third_catid, ak.third_catname, cm1.catid third_catid, cm1.catname third_catname, cm2.catid second_catid, cm2.catname second_catname, cm3.catid first_catid, cm3.catname first_catname FROM autosuggestkeywords ak LEFT JOIN category_master cm1 ON ak.third_catid=cm1.catid LEFT JOIN category_master cm2 ON cm1.catparent_id=cm2.catid LEFT JOIN category_master cm3 ON cm2.catparent_id=cm3.catid WHERE ( MATCH(ak.proname, ak.proid_ft) AGAINST('".$param['search']."' IN BOOLEAN MODE) OR MATCH( ak.proid_ft, pronamesearch, firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) ) ORDER BY `first_catid` ASC";
              echo $select; die;*/
              //echo '<pre>';
              $res=$this->db->query($select);
              //print_r($res->result_array()); die;
              if($res)
              return $res->result_array();
          }else{
              echo "search parameter missing"; die;
          }
      }


      public function searchtotalcount($param){
          if(!empty($param['search'])){
              $seachwords=explode(' ',$param['search']);
              $wherecond=false;
              //if($param['start'] ==0){
                  if($param['sort'] && $param['sort']=='highprice'){
                    $wherecond=", pm.discountprice DESC ";
                  }else if($param['sort'] && $param['sort']=='lowprice'){
                    $wherecond=", pm.discountprice ASC ";
                  }
                  $soundexAddons = array();
                  //print_r($seachwords); die;
                  foreach ($seachwords as $word){
                    if(!is_int($word)){
                        $soundexAddons[] = soundex($word);
                    }else{
                        //$soundexAddons=$word;
                        $soundexAddons[] = soundex($word);
                    }
                  }

                  $soundex=implode(' ',$soundexAddons);
                  $matchagainst=($param['module']=='brandsfilters') ? "ak.brandname" : "firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch";
                  $matchagainst1=($param['module']=='brandsfilters') ? "ak.brandname" : "ak.proname, ak.proid_ft, ak.pronamesearch";

                  if($param['module']=='brandsfilters'){
                      $select2="SELECT COUNT(pm.proid) cnt, ((MATCH(ak.proname,ak.proid_ft,pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) + MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))) AS matchScore FROM autosuggestkeywords ak LEFT JOIN product_master pm ON ak.proid=pm.proid WHERE ak.brandid='".$param['brandid']."'";
                  }else{
                      $select2="SELECT COUNT(pm.proid) cnt, ((MATCH(ak.proname,ak.proid_ft,pronamesearch) AGAINST('".$param['search']."' IN BOOLEAN MODE) + MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))) AS matchScore FROM autosuggestkeywords ak LEFT JOIN product_master pm ON ak.proid=pm.proid WHERE (MATCH(".$matchagainst1.") AGAINST('".$param['search']."' IN BOOLEAN MODE) OR MATCH(".$matchagainst.") AGAINST('".$param['search']."' IN BOOLEAN MODE))";
                  }

                  if($param['catid'] && $param['level']=='2'){
                      $select2.=" AND ak.second_catid=".$param['catid']."";
                  }else if($param['catid'] && $param['level']=='3'){
                      $select2.=" AND ak.third_catid=".$param['catid']."";
                  }

                  if($param['maincat']){
                      $select2.=" AND ak.first_catid=".$param['maincat']."";
                  }

                   $select2.=" ORDER BY matchScore DESC ".$wherecond."";
                   $select2.=" limit ".$param['start'].", ".LIMIT."";

                  $res2=$this->db->query($select2);
                  if($res2){
                    return $result=$res2->result_array();
                  }
            }else{
              echo "search parameter missing"; die;
            }
      }


      public function brandfilter($param){
          $select="SELECT DISTINCT(brandid), brandname FROM autosuggestkeywords WHERE ( MATCH(ak.proname, ak.proid_ft) AGAINST('guitar' IN BOOLEAN MODE) OR MATCH( ak.proid_ft, pronamesearch, firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch ) AGAINST('guitar' IN BOOLEAN MODE) OR MATCH( pronamesearch, firstcatnamesearch, secondcatnamesearch, thirdcatnamesearch, fourthcatnamesearch, fifthcatnamesearch, brandnamesearch ) AGAINST(CONCAT('', '*') IN BOOLEAN MODE) ) ORDER BY `first_catid` ASC";
      }


  public function getsearchprice($param){
    $select2="SELECT min(pm.discountprice) as 'minprice',max(pm.onlineprice) as 'maxprice' from `product_master` pm join brand_master bm on pm.`brandid`=bm.brandid join product_to_category pc on pm.proid=pc.proid join category_master cm1 on pc.category_id=cm1.catid left join category_master cm2 on cm1.catparent_id=cm2.catid left join category_master cm3 on cm2.catparent_id=cm3.catid left join category_master cm4 on cm3.catparent_id=cm4.catid left join category_master cm5 on cm4.catparent_id=cm5.catid where match(proname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) or match(brandname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) or match(cm1.catname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) or match(cm2.catname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) or match(cm3.catname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) or match(cm4.catname) AGAINST (concat('guitar','*') IN BOOLEAN MODE) or match(cm5.catname) AGAINST (concat('".$param['search']."','*') IN BOOLEAN MODE) order by pm.proid";
    $res2=$this->db->query($select2);

    if($res2){
      return $result=$res2->result_array();
    }
  }

  public function getofferfilter_old($param){
    //die('hiii');
    /*brand*/
    //echo '<pre>';
    //print_r($param); die;
    if($param['branddtl']){
      $param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
      $whr1.=" and pm.brandid in(".$param['branddtl'].")";
    }
    /*available*/
    if($param['avaliable']){
      $param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
      //  echo $param['avl'].'hiii1';
      $whr2.=" and pm.isAvailable in(".$param['avaliable'].")";
    }
    /*category*/  
    if($param['categoryid']) {
      $whr3 .= " and cm.catid = '".$param['categoryid']."'"; 
    }    
    /*price*/
    if($param['minprice'] and $param['maxprice']){
      $whr4.=" and pm.discountPrice >= '".$param['minprice']."' and pm.discountPrice <= '".$param['maxprice']."'";
    }
    /*category*/ 
    //$selectcat = "select  cm.catname,cm.catid as categoryid, count(pm.proid) as productcount from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id where pms.id='".$param['id']."' ".$whr1." ".$whr2." ".$whr3." group by cm.catid";

    //$selectcat = "SELECT cm.catname, cm.catid as categoryid, count(pm.proid) as productcount FROM category_master cm LEFT JOIN product_master pm ON FIND_IN_SET(cm.catid, pm.categorypath) WHERE pm.promotionId='".$param['id']."' ".$whr1." ".$whr2." ".$whr3." group by cm.catid";

    $selectcat="SELECT cm.catname, cm.catid as categoryid, cm.catparent_id FROM category_master cm LEFT JOIN product_master pm ON FIND_IN_SET(cm.catid, pm.categorypath) WHERE pm.promotionId=".$param['id']." ".$whr1." ".$whr2." ".$whr3." GROUP BY cm.catid";
    
    //print $selectcat; die;
    
    $res4= $this->db->query($selectcat);
    $res2['category']= $res4->result_array();
    /*brand*/
    $select = "select pm.brandid,COUNT(DISTINCT(bm.brandid)) as brandcount ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' ".$whr1." ".$whr2." ".$whr3."  GROUP BY pm.brandid";
    //print $select; die;

    $res  = $this->db->query($select);
    $res2['brand']=$res->result_array();
    /*available*/
    $select1 = "SELECT case pm.isAvailable when 1 then '1'   when 0 then '0'  end as 'offerid',count(pm.isAvailable) as 'count' FROM  product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' ".$whr1." ".$whr2." ".$whr3."  group by pm.isAvailable";
    $res1  = $this->db->query($select1);
    $res2['available']=$res1->result_array();
    /*price*/
    $select2 = "SELECT MIN(pm.discountPrice) as minamount ,MAX(pm.discountPrice) as maxamount FROM  product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' ".$whr1." ".$whr2." ".$whr3." ".$whr4." ";
    $res3  = $this->db->query($select2);
    $res2['price']=$res3->result_array();

    if($res2){
      //print_r($res2); die;
      //$res1=$res->result_array();
       return $res2;
    }else{
      echo "there is some problem while retriving the data"; die; 
     }  
    
  }

  public function getofferfilter($param){
    
    if($param['branddtl']){
      $param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
      $whr1.=" and brandid in(".$param['branddtl'].") ";
    }
    if($param['avaliable']){
      $param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
      //  echo $param['avl'].'hiii1';
      $whr2.=" and isAvailable in(".$param['avaliable'].") ";
    }  
    if($param['categoryid']) {
      //$whr3 .= " and cm.catid = '".$param['categoryid']."' "; 
      $whr3.=" AND FIND_IN_SET(".$param['categoryid'].", categorypath) ";
    }
    $whr4='';
    if($param['minprice'] and $param['maxprice']){
      $whr4.=" and discountPrice >= '".$param['minprice']."' and discountPrice <= '".$param['maxprice']."' ";
    }

    $selectcat="SELECT cm.catname, cm.catid as categoryid, cm.catparent_id, (SELECT catname FROM category_master WHERE catid=cm.catparent_id) catparentname FROM category_master cm LEFT JOIN product_master pm ON FIND_IN_SET(cm.catid, pm.categorypath) WHERE pm.promotionId=".$param['id']." GROUP BY cm.catid";
    
    //print $selectcat; die;
    
    $res4= $this->db->query($selectcat);
    $res2['category']= $res4->result_array();

    $select="SELECT bm.brandid, bm.brandname FROM brand_master bm LEFT JOIN product_master pm on bm.brandid=pm.brandid WHERE pm.promotionId=".$param['id']." GROUP BY bm.brandid";
    
    $res= $this->db->query($select);
    $res2['brand']=$res->result_array();
    

    $select1 = "SELECT CASE isAvailable WHEN 1 THEN '1' WHEN 0 THEN '0' END offerid FROM  product_master WHERE promotionId='".$param['id']."' GROUP BY isAvailable";

    $res1  = $this->db->query($select1);
    $res2['available']=$res1->result_array();
    /*price*/
    //$select2= "SELECT MIN(discountPrice) AS minamount ,MAX(discountPrice) AS maxamount FROM  product_master WHERE promotionId=".$param['id']." ".$whr1.$whr2.$whr4." ";

    $select2="SELECT MIN(pm.discountPrice) AS minamount ,MAX(pm.discountPrice) AS maxamount FROM  product_master pm LEFT JOIN product_to_category ptc ON pm.proid=ptc.proid LEFT JOIN category_master cm ON ptc.category_id=cm.catid WHERE pm.promotionId=".$param['id']." ".$whr1.$whr2.$whr4.$whr3."";
    //echo $select2; die;
    $res3= $this->db->query($select2);
    $res2['price']=$res3->result_array();

    $select3="SELECT DISTINCT(CEIL(((pm.onlinePrice-pm.discountPrice) / pm.onlinePrice) * 100)) discount FROM product_master pm WHERE promotionId=".$param['id']."";
    $res5=$this->db->query($select3);
    $res2['discount']=$res5->result_array();

    if($res2){
       return $res2;
    }else{
      echo "there is some problem while retriving the data"; die; 
     }  
    
  }
  // public function getofferfilter($param){
  //   $select = "select  pm.brandid,pc.category_id, cm.catname,COUNT(pm.brandid) as brandcount ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' GROUP BY pm.brandid";

  // }

  public function getproductpromotiondtl($param)
    {
      //$param['count']=false;
      $today = date('Y-m-d');
        if($param['count']){
          $select="select COUNT(DISTINCT(pm.proid)) cnt from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' ";
  
        }else{
        $select="select pm.proname,pm.proid, pm.isAvailable, pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid, pms.title,pms.name promotionname, pms.bannerImage,pms.logoImage,pms.topMenuImage,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."'";
      }
        $select.=" AND date(pms.endDate) >= '".$today."' AND date(pms.startDate) <= '".$today."' AND pms.status = 1 ";
      //print $select;die;
        if($param['branddtl'] ){
            $param['branddtl']   = str_replace( array('[',']') , ''  , $param['branddtl']);
            $select.=" and bm.brandid in(".$param['branddtl'].")";

         }
          if($param['avaliable'] ){
            $param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
            $select.=" and pm.isAvailable in(".$param['avaliable'].")";

         }
         if($param['minprice'] and $param['maxprice']){
          $select.=" and pm.discountPrice >= '".$param['minprice']."' and pm.discountPrice <= '".$param['maxprice']."'";

        }
        if($param['categoryid']) {
          $select .= " and cm.catid = '".$param['categoryid']."'"; 
        }

        if($param['discountdetail']){
            $discount=implode(',', $param['discountdetail']);
            if(!empty($discount))
            $select.=" AND CEIL(((pm.onlinePrice-pm.discountPrice) / pm.onlinePrice) * 100) IN (".$discount.")";
        }

        if($param['count']===false){
          $select.=" GROUP BY pm.proid ";
        }
        if($param['sort'] =='popular' ){
          $select .=" order by rm.overallrating desc ";

        }else if($param['sort'] =='lowprice' ){
          $select .=" order by pm.discountprice asc ";

        }
        else if($param['sort'] =='highprice'){
          $select .=" order by pm.discountprice desc ";
        }else{
          $select .=" order by rm.overallrating desc ";
        }
        if($param['count']===false){
          $select.="LIMIT ".$param['start'].", ".LIMIT."";
        }
        //echo $select; die;
       // error_log("this is test from do products".$select);  
        $res=$this->db->query($select);

        if($res){
          $res1['prodata']=$res->result_array();
        }
        return $res1;
    }

   public function getallpromotions()
    {
      $today = date('Y-m-d');

      $select="select id, title , bannerImage, startDate, endDate, promotionofferDisplay  from promotionMaster where  CURDATE() BETWEEN  date(startDate) AND date(endDate)  AND status = 1 order by promo_order asc ";     // print $select;
      $res=$this->db->query($select);
       if($res){
         $res1['prodata']=$res->result_array();
         }
         return $res1;

    }

    public function pro_clerance_dtl($param){
    /*$select="select cpm.productId, cpm.productName,cpm.onlinePrice,cpm.clearanceMRP,cpm. review,cpm.productImage1,cpm.productImage2,cpm.productImage3,cpm.productImage4, cpm.clearanceId,cpm.clearancePercentage,cpm.clearanceMRP,cpm.serialid,
    cpm.clearanceAvailable FROM clerance_product_master cpm left join product_master pm on cpm.productId=pm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_to_category pc on pm.proid=pc.proid where cpm.clearanceId='1' and cpm.productId=".$param['id']." and cpm.serialid='".$param['sid']."'"; */
    //query change by viki on 26/3/2018//
     $select="select cpm.productId, cpm.productName,cpm.onlinePrice,cpm.clearanceMRP,cpm. review,cpm.productImage1,cpm.productImage2,cpm.productImage3,cpm.productImage4, cpm.clearanceId,cpm.clearancePercentage,cpm.clearanceMRP,cpm.serialid,
    cpm.clearanceAvailable,count(rm.reviewid)as rvcnt,rm.overallrating ,rm.reviewauthor,rm.myreview,rm.reviewdate_added,rm.reviewtitle FROM clerance_product_master cpm left join product_master pm on cpm.productId=pm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_to_category pc on pm.proid=pc.proid left join review rm on cpm.productid=rm.proid where cpm.clearanceId='1' and cpm.productId=".$param['id']." and cpm.serialid='".$param['sid']."'";

       $res=$this->db->query($select);


    if($res){
      $res1=$res->result_array();
    }
    $review="select (select COUNT(reviewid) from review where proid='".$param['id']."')as totalreview,(select avg(reviewrating) from review where proid='".$param['id']."')as avgrating,reviewid, reviewauthor,reviewrating, reviewtext, overallrating, reviewtitle,reviewdate_modified,myreview,status  from review  where proid='".$param['id']."' order by reviewid desc limit 5";

    $res3=$this->db->query($review);
    if($res3){
     $res1['rewview']=$res3->result_array();
      }
       return $res1;

    }




  public function autosuggest($param){
    ///print_r($param); die;
     # print_R($param);exit;
  $seachwords = explode(' ', $param['search']);

      $searchtext=addslashes($param['search']);

       $select="SELECT proid,proname,first_catid,first_catname,second_catid,second_catname,third_catid,third_catname,fourth_catid,fourth_catname,fifth_catid,fifth_catname,brandid,brandname from autosuggestkeywords where Match (proname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (proid) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (first_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (second_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (third_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match(fourth_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or
     Match(fifth_catname)  AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match(brandname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) limit 0,20";
      $res=$this->db->query($select);

      if($res->num_rows>0){
        return $res1=$res->result_array();
      }else{
    $soundexAddons = array();
    foreach ( $seachwords as $word){
        $soundexAddons[] = soundex($word);
    }

    $soundex=implode(' ',$soundexAddons);

    $select2="SELECT proid,proname,first_catid,first_catname,second_catid,second_catname,third_catid,third_catname,fourth_catid,fourth_catname,fifth_catid,fifth_catname,brandid,brandname from autosuggestkeywords where Match (pronamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match (proid) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or or Match (firstcatnamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match (secondcatnamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match (thirdcatnamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match(fourthcatnamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match(fifthcatnamesearch)  AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) or Match(brandnamesearch) AGAINST (concat('".$soundex."','*') IN BOOLEAN MODE) limit 0,20";
    //echo $select2; die;

    $res2=$this->db->query($select2);

    if($res2){
      return $result=$res2->result_array();
    }
  }

    }

  public function savesearchterm($param){
        if(!empty($param['search'])){
          $searchterm=addslashes($param['search']);

          $savesearch="INSERT into keyword set kw_name='".$searchterm."',kw_noofresult='".$param['noofresults']."',kw_devicetype='".$param['device_type']."',kw_datetime=now()";
          $res=$this->db->query($savesearch);

          $res=$this->db->query($insert);
          if($res){
           return true;
          }
          else{
            return false;
          }
        }
  }
}
?>
