<?php
class Do_wishlist extends CI_Model{
	
	function __construct(){
		parent:: __construct();
		$this->load->database();
	}
	
	
	   public function addtowishlist($param){
		    $this->load->library('encryption');
		   $param['usrid'] = $this->encryption->decrypt($param['usrid']);
		   
		   $select="select  wishlist from customer where customer_id='".$param['usrid']."'";
		  $res= $this->db->query( $select);
		  if($res){
			  $result=$res->result_array();
			 // print_r($result);die;
			  }
		  if(!empty($result[0]['wishlist'])){
			 
			  $wishlist=explode(',',$result[0]['wishlist']);
			 
			  if(in_array($param['pid'],$wishlist)){
				  
				  return "duplicate";
			  }	  
			}
		  if(!empty($result[0]['wishlist'])){
			 $insert="update  customer set wishlist=CONCAT(wishlist,',','".$param['pid']."') where  customer_id='".$param['usrid']."'";
		  }else{
			  $insert="update customer set wishlist='".$param['pid']."' where  customer_id='".$param['usrid']."'";
			  
		   }
		   $res1= $this->db->query( $insert); 
		  // print_r($res1);die;
			  return $res1;
			  
		
		}	
			
			
			
			public function getwishlist($param){ 
			$this->load->library('encryption');
			$param['usrid'] = $this->encryption->decrypt($param['usrid']);
				//print_r($param);
		     $select="select  wishlist from customer where customer_id='".$param['usrid']."'";
		    //print $select;
		     $res= $this->db->query( $select);
		    if($res){
			$result1=$res->result_array();		
			$wishlist=$result1[0]['wishlist'];
			if($wishlist){
				$select="select proweight,proid,onlineprice,discountprice,proname,prothumbnail,isAvailable  from  product_master WHERE proid in (".$wishlist.") ";  
				$res1=$this->db->query($select);
				 
				if($res1){
				$res2=$res1->result_array();
               
				if($res2){
							foreach($res2 as $row){
							
								
							 $result[$row['proid']]['proid']=$row['proid'];
							 $result[$row['proid']]['proname']=$row['proname'];
							 $result[$row['proid']]['image']=$row['prothumbnail'];
							 $result[$row['proid']]['price']=$row['onlineprice'];
							 $result[$row['proid']]['weight']=$row['proweight'];
							 $result[$row['proid']]['stock']=$row['isAvailable'];
							 // $result[$key]['onlineprice']=$row['onlineprice'];
								
							  
							 
							 if($row['discountprice']!=''){
								 
								  $result[$row['proid']]['dicountprice']=$row['discountprice'];
								  $discount=$row['onlineprice']-$row['discountprice'];
								  $dis_per=($discount/$row['onlineprice'])*100;
								 
								  $dis_per= ceil($dis_per);
								  $result[$row['proid']]['precentage']=$dis_per;

							  }
							  
							  
					
			            }
			            
							  return $result; 
				    } 
		        }
			}
			
            }
		 }
		 
		 public function removefromwishlist($param){
			 $this->load->library('encryption');
			 $param['usrid'] = $this->encryption->decrypt($param['usrid']);
			 if($param){
		       $update="UPDATE  `customer`  SET  `wishlist` = TRIM(BOTH ',' FROM REPLACE(CONCAT(',', wishlist), ',".$param['pid']."', ''))  WHERE  customer_id=".$param['usrid']."";	 
		      $res= $this->db->query( $update);
			  return $res; 
		  }
		 }
}?>
