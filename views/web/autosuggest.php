<style>
    #note.columns2 { width: 450px}#note.columns2 #typeAheadInnerRow>ul { width: 49%; margin-right: 2%} #note.columns2 #typeAheadInnerRow>ul:last-child {margin-right: 0}
    #note.columns2 #typeAheadProducts {width: 49%}#note.columns3 {width: 590px}
    #note.columns3 #typeAheadInnerRow>ul {width: 26%;margin-right: 3%}
    #note.columns3 #typeAheadProducts {width: 40%}
    #note strong {font-weight: bold;font-size: 14px;line-height: 15px;color: #333;text-transform: capitalize}
    #note .highlight {font-weight: bold}
    #note #typeAheadInnerRow>ul {font-size: 12px;line-height: 13px;display: table-cell;background: #fff}
    #note #typeAheadInnerRow>ul#searchTypeAhead-list {padding: 0}
    #note li {float: none!important; display: block}
    #note a {font-size: inherit;padding: 0}#note a.searchTypeAhead-result {
        color: #686c6f; font-weight: bold;text-decoration: none;padding-left: 16px;padding: 2px 0;display: block}
    #note ul li.searchTypeAhead-item {color: #333;display: block;padding: 4px;float: none;cursor: pointer }
    #note ul li.searchTypeAhead-empty {font-size: 12px;padding-top: 8px;
        color: #414141;font-weight: bold;font-style: italic;width: 100%}
    #note ul li.searchTypeAhead-item a {
        color: inherit;text-decoration: inherit;font-size: inherit;font-weight: inherit;display: block}#note ul li.searchTypeAhead-item.active {color: #fff;background-color: #638fc3!important}#note ul li.searchTypeAhead-item.active a {color: #fff}
    #note #typeAheadDimensions { background: #fff;padding: 4px}
    #note #typeAheadDimensions a { color: #369; font-weight: inherit
    }#note #typeAheadDimensions>li {padding-bottom: 10px;float: none;display: block}#note #typeAheadDimensions>li li {padding: 4px}
    #note #typeAheadDimensions>li li:hover {color: #fff; background-color: #638fc3}
    #note #typeAheadDimensions>li li:hover a {color: #fff;text-decoration: none}
    #note #typeAheadProducts { background: #fff;overflow: hidden; padding: 4px; display: table-cell}
    #note #typeAheadProducts li {position: relative;min-height: 55px;float: none}
    #note #typeAheadProducts .imgLink {position: absolute;top: 0;left: 0}
    #note #typeAheadProducts .prodInfo {margin-left: 0px;margin-right: 30px;font-size: 12px}
    #note #typeAheadProducts .prodInfo a.titleLink {color: #369}
    #note #typeAheadProducts .titleLink {display: block;margin-bottom: 4px}
    #note #typeAheadProducts .stars { float: none;clear: both}
    #note #typeAheadProducts img { max-width: 90px}
    .blck{ display: block; }
</style>
<div id="note" class="columns3 blck">
    <div id="typeAheadInner">
        <div id="typeAheadInnerRow">
		<? //if($keywords_sql->num_rows > 0){ ?>
            
				<?

	$finalArr = array();
	$foundAll = 0;
	$cnt = 0;							// Amplifiers in Peavey:401|568
	
		//print_R($category); die;
		
?>
	<ul id="typeAheadDimensions">
              <?
                     			
					if($autosuggest){
						
				?>
	
				<?	foreach ($autosuggest as $key1=>$val1){ 
				//echo $brand[$k]; die;
				     
						?>
                        <li><a data-name="Sterling Audio"><span class="highlight"><?=$val1['categoryname'];?><!--</span>ling Audio (20)</a>-->
                        </li>
                     
				<?}}?>
                    
             
				
                
            </ul>
			
            <ul id="typeAheadDimensions">
                <li><strong>brand</strong>
                    <ul>
				
				<? //if($brand_sql->num_rows > 0){ 
                     			
					if($brand){
						
				?>
	
				<?	/*for($k=0,$m=0;$k<=count($brand),$m<=count($brand_id);$k++,$m++){ 
				        //echo $brand[$k]; die;
				       $brandname=preg_replace('/[^A-Za-z0-9\-]/', '-', $brand[$k]);
					   $brandname=str_replace("--","-",  $brandname) ;
					   $brandname=strtolower(rtrim( $brandname,'-')); 
					   $url=SITEMOBURL.'product/search?searchText='.$search;*/
                       foreach($brand as $brandkey=>$brandval){
					       //split brandid and brandname
                            $explodedbrands=explode(':',$brandval);
                                $brandname=preg_replace('/[^A-Za-z0-9\-]/', '-', $explodedbrands[1]);
                               $brandname=str_replace("--","-",  $brandname) ;
                               $brandname=strtolower(rtrim( $brandname,'-')); 
                               $url=SITEMOBURL.'product/search?searchText='.$brandname;
                       ?>
                        <!--<li><a data-name="" href="<?echo $url;?>"><span class="highlight"><?=$brand[$k]?>
                        </li>-->
                        <li><a data-name="" href="<?echo $url;?>"><span class="highlight"><?php echo $brandname; ?>
                        </li>
				<?}}?>
                    </ul>
                </li>
				
                <li> <strong>category</strong>
                    <ul>
					<? if($category){ ?>
	
	
				<?	for($k=0;$k<count($category[0]);$k++){ 
						$catarray=explode(',',$category[0][$k]);
						$catorgname=$catarray[0];
						$catname=preg_replace('/[^A-Za-z0-9\-]/', '-', $catorgname);
					 $catname=str_replace("--","-",  $catname) ;
					  $catname=strtolower(rtrim( $catname,'-')); 
						$catid=$catarray[1];
						$url=SITEMOBURL.$catname.'/'.$catid.'?search='.$search.'';
						?>
                        <li><a data-name="Pro Audio" href="<?echo $url;?>"><span class="highlight"><?=$catorgname?></span></a>
                        </li>
                       
				<?}}?>
                    </ul>
                </li>
            </ul>
			
            <div id="typeAheadProducts"> <strong>Products</strong>
                <ul>
				<? //if($product_sql->num_rows > 0){
					if($product){
						
					
				?>
				<?	for($i=0;$i<count($product[0]);$i++){ 
				      $proarray=explode(':',$product[0][$i]);
					 // print_R($proarray); die;
						$proorgname= $proarray[0];
						$proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $proorgname);
					 $proname=str_replace("--","-",  $proname) ;
					  $proname=strtolower(rtrim( $proname,'-')); 
						$proid=$proarray[1];
						$url=SITEMOBURL.$proname.'/'.$proid;?>
                    <li>
                        <div class="prodInfo"><a data-name="Sterling Audio ST55 / ST31 Condenser Mic Pa..." class="titleLink" href="<? echo $url;?>"><?=$proorgname;?></a><!--<span class="stars small rate-10">5.0</span>-->
                        </div>
                  
                    </li>
            
					<?}}?>
                </ul>
            </div>
        </div>
    </div>
</div>