<?php ?>
<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border: 1px solid #999;margin:  0px auto;">
    <tbody>
		<tr style="font-weight:bold;background-color: #e0e0e0;color: #000;">
		  <td style="padding: 5px;">EMI Tenure</td>
		  <td style="padding: 5px;">Bank Interest Rate</td>
		  <td align="right" style="padding: 5px;" >Monthly Installments</td>
		  <td align="right" style="padding: 5px;">Total Money</td>
		</tr>
		<tr id="emiId_3">
		  <td style="padding: 5px;"><b>3 Month</b></td>
		  <td align="center" style="padding: 5px;">13.9%</td>
		  <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
		     font-size: 12px ! important;
		     "></i> 15,535/-</td>
		  <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
		     font-size: 12px ! important;
		     "></i> 46,608/-</td>
		</tr>
		<tr id="emiId_6">
		  <td style="padding: 5px;"><b>6 Month</b></td>
		  <td align="center" style="padding: 5px;">13.9%</td>
		  <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
		     font-size: 12px ! important;
		     "></i> 7,902/-</td>
		  <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
		     font-size: 12px ! important;
		     "></i> 47,412/-</td>
		</tr>
    </tbody>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border: 1px solid #999;margin:  0px auto;">
    <tbody>
       <tr style="font-weight:bold;background-color: #e0e0e0;color: #000;">
          <td style="padding: 5px;">EMI Tenure</td>
          <td style="padding: 5px;">Bank Interest Rate</td>
          <td align="right" style="padding: 5px;" >Monthly Installments</td>
          <td align="right" style="padding: 5px;">Total Money</td>
       </tr>
       <tr id="emiId_3">
          <td style="padding: 5px;"><b>3 Month</b></td>
          <td align="center" style="padding: 5px;">13%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 15,512/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 46,539/-</td>
       </tr>
       <tr id="emiId_6">
          <td style="padding: 5px;"><b>6 Month</b></td>
          <td align="center" style="padding: 5px;">13%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 7,881/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 47,292/-</td>
       </tr>
       <tr id="emiId_9">
          <td style="padding: 5px;"><b>9 Month</b></td>
          <td align="center" style="padding: 5px;">13%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 5,338/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 48,051-</td>
       </tr>
       <tr id="emiId_12">
          <td style="padding: 5px;"><b>12 Month</b></td>
          <td align="center" style="padding: 5px;">13%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 4,068/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 48,816/-</td>
       </tr>
       <tr id="emiId_12">
          <td style="padding: 5px;"><b>18 Month</b></td>
          <td align="center" style="padding: 5px;">15%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 2,841/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 51,156/-</td>
       </tr>
       <tr id="emiId_12">
          <td style="padding: 5px;"><b>24 Month</b></td>
          <td align="center" style="padding: 5px;">15%</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 2,208/-</td>
          <td align="right" style="padding: 5px;"><i class="fa fa-inr" style="
             font-size: 12px ! important;
             "></i> 52,992/-</td>
       </tr>
    </tbody>
 </table>