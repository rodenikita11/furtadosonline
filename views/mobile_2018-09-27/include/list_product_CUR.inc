<div class="col-xs-6 box-left">
  <? if($value['price'] >  $value['discountprice']) {?>
    <div class="agile_top_brand_left_grid_pos_new"> 
      <img src="<?php echo SITEIMAGES.'mobimg/icon/offer.png';?>" alt=" " class="img-responsive">
    </div>
  <?}?>
    <div class="agile_top_brand_left_grid_pos_new_wishlist">
    <?if($value['wishlist'] =='0') {?>
      <p onclick="addtowishlist('<?echo $value['id'];?>');"><i class="fa fa-heart-o" style="font-size: 20px;color:#8b8b8b!important;padding-top: 10px;padding-left: 10px;"></i></p>
    <?} else {?>
      <p onclick="removefromwishlist('<?echo $value['id'];?>');"><i class="fa fa-heart" aria-hidden="true" style="font-size:17px;padding-right: 10px; padding-top:0px; float:right !important; color:red !important;"></i></p>
    <?}?>
  </div>
  <a href="<?php echo $value['url'];?>" style="text-decoration:none;">
    <img class="hot-deals-home" src="<?php echo $arrImg = false!==file($value['img']) ? $value['img'] : IMAGEDEFAULT ;?>" width="100%">
    <p class="overflow-ellipsis"><?php echo $value['name'];?></p>
    <!--div class="star">
      <?php
      $starNumber  = $value['review'][0]['avgrating'];
      if(strpos($starNumber, '.')) {
      $rawnumber = explode('.', $starNumber);
      $number    = $rawnumber[0];
      $decnumber = $rawnumber[1];
      } else {
      $number    = $starNumber;
      } ?>
      <? for ($i = 0; $i < 5 ; $i++) { ?>
      <?if($i < $number) { ?>
      <a href=""><i class="fa fa-star" style="color:#cd0029;"></i></a>
      <?} elseif($decnumber && $decnumber == 5) { ?>
      <a href=""><i class="fa fa-star-half-full " style="color:#cd0029;"></i></a>
      <?php 
      $decnumber++;
      } else {?>
      <a href=""><i class="fa fa-star-o"></i></a>
      <?}
      }
      ?>
      <!--  <?php if(empty($value['review'])){?>
      <span class="reviews">0 Reviews</span>
      <? }else{?>
      <span class="reviews"><?php echo $value['review'][0]['totalreview'];?> &nbsp <?php if($value['review'][0]['totalreview']== 1) { echo "Review"; } else{ echo "Reviews";} ?></span>
      <? }?> -->
    </div-->
    <div class="col-xs-12">
      <?if($value['callforprice'] == 'YES') {?>
      <div class="col-xs-8 prize-color">
        <a href='tel:02242875050'>Call for Price</a>
      </div>
      <?} else {?>
        <?if($value['price'] > $value['discountprice']) {?>
        <div class="col-xs-4 prize-color">
          <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $this->libsession->priceformat($value['discountprice']);?>
        </div>
        <span class="col-xs-4 percentage" >(<?php echo $value['percentage'];?>% off)</span>
        <?}?>
        <?if($value['price'] == $value['discountprice']){?>
        <div class="col-xs-6 prize-color">
          <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $this->libsession->priceformat($value['price']);?>
        </div>
        <?}else{?>
        <div class="col-xs-4 text">&nbsp;<i class="fa fa-inr" aria-hidden="true"></i> <?php echo $this->libsession->priceformat($value['price']);?></div>
        <?}
      }?>
    </div>
  </a>
</div>
