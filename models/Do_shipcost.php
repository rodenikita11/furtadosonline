<?php
class Do_shipcost extends CI_Model{
  
  function __construct(){
    parent:: __construct();
    $this->load->database();
  }
  

  /*public function getstate($param){
	  $select="SELECT state FROM `customer_shipping_address` where customer_id='".$param['usrid']."' order by address_id desc limit 1";
	  $res=$this->db->query($select);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
	  
	}*/
	
	 protected function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Do_shipcost.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . json_encode($query).PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/shipping_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	 }

   public function getstate($param){
	   $this->load->library('encryption');
	   $param['usrid'] = $this->encryption->decrypt($param['usrid']);
	   
	  $select="SELECT state, postcode FROM `customer_shipping_address` where customer_id='".$param['usrid']."' order by address_id desc limit 1";
		$res=$this->db->query($select);
		$this->log_fileDetails('getstate', $select);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
	  
  }
  
  public function getprodetail($param=false){
  	$param['proid']=($param['proid']) ? $param['proid'] : $param['pid'];
	$select="SELECT promotionId, clearanceId FROM product_master WHERE proid = ".$param['proid']." LIMIT 1";
		$res=$this->db->query($select);
		$this->log_fileDetails('getprodetail', $select);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
	  
  }
  
  public function getpromodetail($param){
	  $select="SELECT shippingstatus, startDate, endDate FROM promotionMaster WHERE id = ".$pid." AND endDate >= '".$today."' AND startDate <= '".$today."' AND status = 1";
		 $res=$this->db->query($select);
		 $this->log_fileDetails('getpromodetail', $select);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
	  
  }
  
  /*public function getzone($param=false){
  	
	  $selZoneSql = "SELECT DISTINCT(zoneId), pincode FROM pincodeMaster WHERE cityCode = (SELECT cityCode FROM cityMaster WHERE stateId = ".$param[0]['state']." LIMIT 1) LIMIT 1";
	  
      $res=$this->db->query($selZoneSql);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
  }*/

  public function getzone($param=false){
	  $selZoneSql = "SELECT DISTINCT(zoneId), pincode FROM pincodeMaster WHERE cityCode = (SELECT cityCode FROM cityMaster WHERE stateId = ".$param[0]['state']." LIMIT 1) LIMIT 1";
	  
			$res=$this->db->query($selZoneSql);
			$this->log_fileDetails('getzone', $selZoneSql);
	  if($res->num_rows() > 0){
		  return $res1=$res->result_array();
	  }else{
	  		$select="SELECT DISTINCT(zoneId), pincode FROM pincodeMaster WHERE pincode='".$param[0]['postcode']."'";
				$res=$this->db->query($select);
				$this->log_fileDetails('getzone', $select);
	  		return $res->result_array();
	  }
  }

  public function getproductprice($param=false){
  	
	  $selZoneSql = "SELECT onlinePrice,discountPrice,proweight FROM `product_master` where proid=".$param;
	  
			$res=$this->db->query($selZoneSql);
			$this->log_fileDetails('getproductprice', $selZoneSql);
	  if($res){
		  return $res1=$res->result_array();
		  
	  }
  }
  
   public function getpromotiondtl($pid=false){
  	$today = date('Y-m-d');
	  //$selZoneSql = "SELECT shippingstatus, startDate, endDate FROM promotionMaster WHERE id = 222 AND endDate >= '".$today."' AND startDate <= '".$today."' AND status = 1";
	$selZoneSql = "SELECT shippingstatus, startDate, endDate FROM promotionMaster WHERE id = ".$pid." AND endDate >= '".$today."' AND startDate <= '".$today."' AND status = 1";
	//echo $selZoneSql; die;
	  
		$res=$this->db->query($selZoneSql);
		$this->log_fileDetails('getpromotiondtl', $selZoneSql);
	if($res){
		return $res1=$res->result_array();
		  
	}
  }
  
} 
 ?> 