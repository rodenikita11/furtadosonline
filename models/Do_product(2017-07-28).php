<?php
class Do_product extends CI_Model{
  
  function __construct(){
    parent:: __construct();
    $this->load->database();
  }
  
  public function getprodtl($param){
      $select="select pm.proname,pm.proid,pm.prothumbnail,pm.isAvailable,pm.onlinePrice, pm.discountPrice,pm.prodescription,pm.prolongspecification,pm.callforprice,pc.category_id, cm.catname,cm.catparent_id, cm.minprice,cm.maxprice, count(rm.reviewid)as rvcnt,rm.overallrating ,rm.reviewauthor,rm.myreview,rm.reviewdate_added,rm.reviewtitle,bm.brandname,bm.brandimage from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on  find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid  where pm.proid='".$param['id']."'";  
   
   $res=$this->db->query($select);
   if($res){
     $res1['prodata']=$res->result_array();
     }
    $select1="select pro_img_id ,proimagetitle,pro_image,pro_img_sort_order from product_image where proid='".$param['id']."'";
   
    $res2=$this->db->query($select1);
     if($res2){
     $res1['proimage']=$res2->result_array();
       }
      //$review="select reviewid, reviewauthor,reviewrating, reviewtext, overallrating, reviewtitle,reviewdate_modified,myreview,status  from review  where proid='".$param['id']."' order by reviewid desc limit 5";  
      $review="select (select COUNT(reviewid) from review where proid='".$param['id']."')as totalreview,(select avg(reviewrating) from review where proid='".$param['id']."')as avgrating,reviewid, reviewauthor,reviewrating, reviewtext, overallrating, reviewtitle,reviewdate_modified,myreview,status  from review  where proid='".$param['id']."' order by reviewid desc limit 5";

    $res3=$this->db->query($review);
    if($res3){
     $res1['rewview']=$res3->result_array();
      } 
    return $res1;
  }
  
  public function getcatdtl($param){
    //print_R($param);
    if($param['count']){
      $cat="select count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on '".$param['id']."'=cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid";
    }else{
      //$cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid ";
      $cat="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice,pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,(select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on '".$param['id']."' =cm.catid left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid ";
    }   

    

    $cat.=" where find_in_set('".$param['id']."',pc.category_path)"; 
     foreach($param['allattribute'] as $key=>$val){
      $qstr.=" and pa.attriid='".$val."'"; 
       
     }
     
      if($param['branddtl'] ){
       $param['branddtl'] =implode($param['branddtl'],',');
      $qstr.=" and bm.brandid in(".$param['branddtl'].")"; 
       
     }
      if($param['avaliable'] ){
       $param['avaliable'] =implode($param['avaliable'],',');
      $qstr.=" and pm.isAvailable in(".$param['avaliable'].")"; 
       
     }
     if($param['minprice'] and $param['maxprice']){
      $qstr.=" and pm.discountPrice BETWEEN  '".$param['minprice']."' AND    '".$param['maxprice']."'";
      
        }
     
     if($param['sort'] =='popular' ){
        $qstr .=" order by rm.overallrating desc "; 
        
         }else if($param['sort'] =='lowprice' ){
      $qstr .=" order by pm.discountprice asc "; 
        
         }
     else if($param['sort'] =='highprice'){
        
        $qstr .=" order by pm.discountprice desc ";  
        
         }else{
        $qstr .=" order by  pm.discountprice asc ";  
     }
     
     
    if($param['start'] || $param['start']==0){
      
      $qstr.=" limit ".$param['start'].",".LIMIT."  ";
    }
    
    
    
    if(empty($param['count'])){
     $cat=$cat.$qstr;
    }else{
       $cat=$cat;
     }
   // echo $cat;
    $res=$this->db->query($cat);
    if($res){
     $res1['product']=$res->result_array(); 
       }
      // print_R($res1['product']);
     // echo $param['count']; 
     if($param['id']){
      
      $select1="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname,ps.quantity,ps.price from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid join product_seller ps on ps.proid=pm.proid  left join product_attribute_mapping pa on pm.proid=pa.proid where cm.catid='".$param['id']."' and categoryFeatured='Yes'";
      
             //$select1.=$select1.$qstr;
            
       $resfeat=$this->db->query($select1);
             if($resfeat){   
         $res1['featured_product']=$resfeat->result_array();
         }
        }//count close
      //} // id close
      
        if($param['id']){
       $banner="select img_path,img_link,id from banner_master where catid='".$param['id']."' and displaytype='MobileCategory'";
       $resbanner=$this->db->query($banner);
       if($resbanner){
       $res1['cat_banner']=$resbanner->result_array();
       }
      }
      return  $res1;  
    
  }
  
  public function getbrnaddtl($param){
    if($param['count']){
      $brand="select count(pm.proid) from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid join brand_master bm on pm.brandid=bm.brandid  left join product_attribute_mapping pa on pm.proid=pa.proid";
    }else{
       $brand="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice,rm.overallrating,(SELECT count(reviewid) from review where proid=pm.proid) as rvcnt,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid";     
    }   
    $brand.=" where bm.brandid='".$param['id']."'";
     foreach($param['allattribute'] as $key=>$val){
      $brand.=" and pa.attriid='".$val."'"; 
       
     }
    
    if($param['start']){
      $brand.=" limit '".$param['start']."',".LIMIT."  ";
    }
    $brand.=" group by pm.proid";
    
    $res=$this->db->query($brand);
    if($res){
     return $res1=$res->result_array();    
      }
  }
  
  public function getinfotype($param){
    
    if($param['id']){
      
      $select ="select proid, proname  from product_master where  proid='".$param['id']."'";
       
      $res=$this->db->query($select);
             
      
      if ($res->num_rows() > 0 )

      {

         $data['type']='product';
         return $data;

      }else{
       $cat="select catid,catname from category_master where catid='".$param['id']."'"; 
       $query1=$this->db->query($cat);
             if($query1->num_rows() > 0)
       {
        
         $data['type']='category';
         return $data;
      }else{
                $brand="select brandid,brandname from brand_master where brandid='".$param['id']."'"; 
        $query2=$this->db->query($brand);
           if($query2->num_rows() > 0)
          {
             $data['type']='brand';
              return $data; 
          }else{
             $data['type']='invalidtype';
              return $data;
          }       
        }
      }
    }
    
  }
  
  
  public function getsimiliar_prod($param){
    if($param['id']){
        $select="select pm.proid,pm.proname,pm.prothumbnail,pm.discountPrice,pm.onlinePrice,pm.callforprice from product_master pm  join product_to_category pc on pm.proid=pc.proid where pm.onlinePrice>=(select (onlinePrice-(onlinePrice*".SIMPER.")/100) from product_master where proid='".$param['id']."') and pm.onlinePrice<=(select (onlinePrice+(onlinePrice*".SIMPER.")/100) from product_master where proid='".$param['id']."') and FIND_IN_SET(pc.category_id,pm.categorypath) and pm.proactive='1'  LIMIT 4";  
      
      $res=$this->db->query($select);
      if($res){
        return $res1['prodata']=$res->result_array();
        
       }    
    }
  }
  
  public function insert_review($param){
    if($param){
      
      
      $insert="insert into  review SET proid='".$param['pid']."', customerid='".$param['usrid']."' , reviewtext='".$param['reviewmessage']."', reviewrating='".$param['ratingme']."',reviewauthor='".$param['usrname']."',author_email='".$param['usremail']."', reviewtitle='".$param['reviewtitle1']."', reviewdate_added=now(), status=0 ON DUPLICATE KEY UPDATE  reviewtext='".$param['reviewmessage']."',reviewauthor='".$param['usrname']."',reviewrating='".$param['ratingme']."',reviewtitle='".$param['reviewtitle1']."', status=0 "; 
      
      
      $res=$this->db->query($insert); 
      if($res){
         return true;
        }
    }
    
  }
  
  public function getproductautosuggect($param){
      if($param['type']=='cat'){
      $select="select b.id, d.catid as catid, a.proname as pn,a.proid, c.category_id, d.catname as cn , e.catname as pct, f.catname as rt ,e.catid as pid , f.catid as rid 
        from product_master a 
        join product_seller b
        on a.proid = b.proid 
        join product_to_category c  
        on a.proid = c.proid 
        join category_master d
        on c.category_id = d.catid 
        left join category_master e
        on d.catparent_id = e.catid 
        left join category_master f
        on e.catparent_id = f.catid 
        where Match (a.proname) 
        AGAINST ('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) and proactive = 1 group by d.catparent_id limit 10;";
      }else if($param['type']=='pro'){
         $select="select a.proname as pn ,a.proid, f.id from product_master a join product_seller f on a.proid = f.proid  where Match (a.proname) AGAINST ('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) and proactive = 1 limit 5;"; 
      }
      else if($param['type']=='brand'){
         $select="SELECT brandname , brandid FROM brand_master where match(brandname) against('".$this->dbc->escape($param['kw'])."*' IN BOOLEAN MODE) limit 5;"; 

      }else if($param['type']=='catlist'){
        
        $select = "SELECT d.catid as catid,d.catname as cn ,e.catid as pid,e.catname  as pct,f.catid  as rid , f.catname as rt FROM category_master d left join category_master e on d.catparent_id = e.catid left join category_master f on e.catparent_id = f.catid WHERE d.catname like '".$this->dbc->escape($param['kw'])."%' limit 5";
        
      }
      
      $res=$this->db->query($select); 
        if($res){
         return  $res1=$res->result_array();
	
       } 
  }
  
  public function productsearch($param){
    if(!empty($param['search'])){
       $select="SELECT pm.proid,pm.proname,pm.prothumbnail,pm.onlineprice,pm.discountprice,pm.callforprice,pm.isAvailable,brandname,cm1.catname as 'first_category',cm2.catname as 'second_category',cm3.catname as 'third_category',cm4.catname as 'fourth_category',
     cm5.catname as 'fifth_category' FROM `product_master` pm join brand_master bm on pm.`brandid`=bm.brandid join product_to_category pc on 
     pm.proid=pc.proid join category_master cm1 on pc.category_id=cm1.catid left join category_master cm2 on cm1.catparent_id=cm2.catid left join
      category_master cm3 on cm2.catparent_id=cm3.catid left join category_master cm4 on cm3.catparent_id=cm4.catid left join category_master cm5 on 
      cm4.catparent_id=cm5.catid where proname like '%".$param['search']."%' or brandname like '%".$param['search']."%' or cm1.catname like '%".$param['search']."%' or 
      cm2.catname like '%".$param['search']."%' or cm3.catname like '%".$param['search']."%' or cm4.catname like '%".$param['search']."%' or cm5.catname like '%".$param['search']."%' order by
       pm.proid limit ".$param['start'].", ".LIMIT.""; 
     
        $res=$this->db->query($select); 
        if($res){
       return $res1=$res->result_array(); 
       } 
     }else{
       echo "search parameter missing"; die; 
       
       }
    
    }

  public function getproductpromotiondtl($param)
    {
      $today = date('Y-m-d');
      
      $select="select pm.proname,pm.proid, pm.isAvailable,pm.prothumbnail,pm.onlinePrice, pm.discountPrice,pm.callforprice,pm.proshortspecification,pm.prolongspecification,pm.brandid,pms.title,pms.name,pms.bannerImage,pms.logoImage,pms.topMenuImage,pc.proid, pc.category_id, cm.catname,cm.catparent_id,cm.minprice,cm.maxprice, (select count(reviewid) from review where proid=pm.proid) as rvcnt,rm.overallrating ,bm.brandname from product_master pm join product_to_category pc on pm.proid=pc.proid join category_master cm on find_in_set(cm.catid,pc.category_path) left join review rm on pm.proid=rm.proid left join brand_master bm on pm.brandid=bm.brandid left join product_attribute_mapping pa on pm.proid=pa.proid join promotionMaster pms on pm.promotionId=pms.id  where pms.id='".$param['id']."' "; 
        $select.=" AND pms.endDate >= '".$today."' AND pms.startDate <= '".$today."' AND pms.status = 1 order by  pm.discountprice asc";
      $res=$this->db->query($select);
       if($res){
         $res1['prodata']=$res->result_array();
         }
         return $res1; 

    }
    
    public function pro_clerance_dtl($param){
     $select="select cpm.productId, cpm.productName,cpm.onlinePrice,cpm.clearanceMRP,cpm. review,cpm.productImage1,cpm.productImage2,cpm.productImage3,cpm.productImage4, cpm.clearanceId,cpm.clearancePercentage,cpm.clearanceMRP,cpm.serialid,
    cpm.clearanceAvailable FROM clerance_product_master cpm join product_master pm on cpm.productId=pm.proid join brand_master bm on pm.brandid=bm.brandid join product_to_category pc on pm.proid=pc.proid where cpm.clearanceId='1' and cpm.productId=".$param['id']." and cpm.serialid=".$param['sid'].""; 
       $res=$this->db->query($select); 
      
       
    if($res){
      $res1=$res->result_array();
       return $res1;
    }else{
      echo "there is some problem while retriving the data"; die; 
     }  
      
    }
  
  


  public function autosuggest($param){
     # print_R($param);exit;
      $searchtext=addslashes($param['search']);

       $select="SELECT proid,proname,first_catid,first_catname,second_catid,second_catname,third_catid,third_catname,fourth_catid,fourth_catname,fifth_catid,fifth_catname,brandid,brandname from autosuggestkeywords where Match (proname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (first_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (second_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match (third_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match(fourth_catname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match(fifth_catname)  AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) or Match(brandname) AGAINST (concat('".$searchtext."','*') IN BOOLEAN MODE) limit 0,100";
      
      $res=$this->db->query($select); 

          if($res){
       return $res1=$res->result_array();
        } 

    }

}
?>
