        <section>
            <div class="home-category color-scheme-2 p30-padding">
                <div class="container">
                    <div class="row" style="/*padding-top:36px;*/">
                        <div class="col-md-4 col-sm-6 col-xs-12 home-sub-categoreise">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-male"></i> <a href="<?echo SITEURL?>exclusive/men/38">Men</a> 
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/men-watches/43"><i class="fa fa-check"></i>Watches</a></li>
                                        <li><a href="<?echo SITEURL?>products/clothing-formal-shirts/149"><i class="fa fa-check"></i>Formal Shirts</a></li>
                                        <li><a href="<?echo SITEURL?>products/men-fragrances/46"><i class="fa fa-check"></i>Fragrances</a></li>
                                        <li><a href="<?echo SITEURL?>products/accessories-wallets/159"><i class="fa fa-check"></i>Wallets</a></li>
                                        <li><a href="<?echo SITEURL?>products/men-eyewear/40"><i class="fa fa-check"></i>Eye Wear</a></li>
                                        <li><a href="<?echo SITEURL?>products/men-fashion/47"><i class="fa fa-check"></i>Fashions</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>bg-w_1.jpg" alt="">
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 home-sub-categoreise">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-female"></i> <a href="<?echo SITEURL?>exclusive/women/187">Women</a>
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/personal-care-hair-care/240"><i class="fa fa-check"></i>Hair Care </a></li>
                                        <li><a href="<?echo SITEURL?>products/personal-care-skin-care/239"><i class="fa fa-check"></i>Skin Care</a></li>
                                        <li><a href="<?echo SITEURL?>products/women-footwear/188"><i class="fa fa-check"></i>Footwear</a></li>
                                        <li><a href="<?echo SITEURL?>products/women-fashion/196"><i class="fa fa-check"></i>Fashion</a></li>
                                        <li><a href="<?echo SITEURL?>products/women-clothing/190"><i class="fa fa-check"></i>Clothing</a></li>
                                        <li><a href="<?echo SITEURL?>products/women-accessories/191"><i class="fa fa-check"></i>Accessories</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>bg-w.jpg" alt="">

                                </div>
                            </article>  
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 home-sub-categoreise">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-child"></i> <a href="<?echo SITEURL?>products/kids/58">Kids</a>
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/kids-toys/68"><i class="fa fa-check"></i>Toys</a></li>
                                        <li><a href="<?echo SITEURL?>products/footwear-flip-fops/256"><i class="fa fa-check"></i>Flip-flops</a></li>
                                        <li><a href="<?echo SITEURL?>products/baby-care-baby-care-combos/300"><i class="fa fa-check"></i>Baby Care Combos</a></li>
                                        <li><a href="<?echo SITEURL?>products/baby-care-health-safety/305"><i class="fa fa-check"></i>Health & Safety</a></li>
                                        <li><a href="<?echo SITEURL?>products/toys-board-games/319"><i class="fa fa-check"></i>Board Games</a></li>
                                        <li><a href="<?echo SITEURL?>products/kids-watches/63"><i class="fa fa-check"></i>Watches</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>bg-w_2.jpg" alt="">
                                </div>
                            </article>  
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-home"></i> <a href="<?echo SITEURL?>products/home-kitchen/70">Home & Kitchen</a> 
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/furniture-dining-sets/381 "><i class="fa fa-check"></i>Dining Sets</a></li>
                                        <li><a href="<?echo SITEURL?>products/home-kitchen-cushions-cushion-covers/80"><i class="fa fa-check"></i>Cushion &amp; Covers</a></li>
                                        <li><a href="<?echo SITEURL?>products/small-appliances-microwave-ovens/333"><i class="fa fa-check"></i>Microwave &amp; Ovens</a></li>
                                        <li><a href="<?echo SITEURL?>products/home-kitchen-appliances/82"><i class="fa fa-check"></i>Appliances</a></li>
                                        <li><a href="<?echo SITEURL?>products/art-artificial-plants/394"><i class="fa fa-check"></i>Artifical Plants</a></li>
                                        <li><a href="<?echo SITEURL?>products/home-kitchen-home-decor/77"><i class="fa fa-check"></i>Home & Decor</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>home.jpg" alt="">
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-headphones"></i><a href="<?echo SITEURL?>products/electronics/86">Electronics</a> 
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/electronics-mobiles/87"><i class="fa fa-check"></i>Mobiles</a></li>
                                        <li><a href="<?echo SITEURL?>products/electronics-camera/91"><i class="fa fa-check"></i>Camera</a></li>
                                        <li><a href="<?echo SITEURL?>products/electronics-gadgets/96"><i class="fa fa-check"></i>Gadgets</a></li>
                                        <li><a href="<?echo SITEURL?>products/electronics-printer/94"><i class="fa fa-check"></i>Printer</a></li>
                                        <li><a href="<?echo SITEURL?>products/electronics-gaming-consoles/97"><i class="fa fa-check"></i>Gaming & Consoles</a></li>
                                        <li><a href="<?echo SITEURL?>products/home-appliances-air-conditioners/452"><i class="fa fa-check"></i>Air Conditioners</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>electronics.jpg" alt="">
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <article class="home-category-block">
                                <div class="home-category-title">
                                    <i class="fa fa-book"></i> <a href="<?echo SITEURL?>products/books-media/113">Book & Media</a> 
                                </div>
                                <div class="home-category-option">
                                    <ul class="list-unstyled home-category-list">
                                        <li><a href="<?echo SITEURL?>products/books-media-software/118"><i class="fa fa-check"></i>Software</a></li>
                                        <li><a href="<?echo SITEURL?>products/books-media-music/116"><i class="fa fa-check"></i>Music</a></li>
                                        <li><a href="<?echo SITEURL?>products/books-media-games/117"><i class="fa fa-check"></i>Games</a></li>
                                        <li><a href="<?echo SITEURL?>products/books-media-e-books/115"><i class="fa fa-check"></i>E Book</a></li>
                                        <li><a href="<?echo SITEURL?>products/books-media-books/114"><i class="fa fa-check"></i>Books</a></li>
                                        <li><a href="<?echo SITEURL?>products/software-operating-systems/531"><i class="fa fa-check"></i>Operating Systems</a></li>
                                    </ul>
                                    <img class="img-responsive" src="<?php echo SITEIMAGES;?>books.jpg" alt="">
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>