<?php
		/*
				FILE NAME : GoogleAPI
				CREATED : 17/Sep
				AUTHOR: SANJAY
		*/
		class GoogleAPI
		{

			function __construct()
			{
				$CI =& get_instance();
				$CI->load->library('google/LightOpenID');
				$this->getUrl();
			}
			
			public function getUrl()
			{
				$openid = new LightOpenID(SITEURL);
				$openid->identity = 'https://www.google.com/accounts/o8/id';
				$openid->required = array(
													'namePerson/first',
													'namePerson/last',
													'contact/email',
													'birthDate', 
													'person/gender',
													'contact/postalCode/home',
													'contact/country/home',
													'pref/language',
													'pref/timezone'
											);
				$openid->returnUrl = SITEURL.'customer/googlelogin';
				$url = $openid->authUrl(); #--Get google auth url.
				echo $url; die;
				return $url;
			}
		}
?>