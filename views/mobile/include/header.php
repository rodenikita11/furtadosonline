<?php include('head.php');
  $searchval=$_GET['search'];
  switch($_GET['maincat']){
      case '1':
        $search_concept='Musical Instruments';
        break;
      case '2':
        $search_concept='Music Books';
        break;
      case '3':
        $search_concept='Pro Audio';
        break;
      default:
        $search_concept='All';
  }
?> 
<style>
#stickyheader{display:none;}
.top-logo {width:100%;}
.badge
{
      background: #d32f2f !important;
    border-radius: 50% !important;
    color: #fff !important;
    height: 15px !important;
    width: 15px !important;
    position: absolute !important;
    bottom: 10px;
    right: 11px !important;
    font-size: 8px !important;
    text-align: center !important;
    line-height: 12.5px !important;
    border: 2px solid #fff !important;
   padding: 0px 0px !important;
}
.off {
    background: #12b312;
    color: #000;
    padding: 2px 3px;
    border-radius: 4px;
}
.ui-bar-a, .ui-page-theme-a .ui-bar-inherit, html .ui-bar-a .ui-bar-inherit, html .ui-body-a .ui-bar-inherit, html body .ui-group-theme-a .ui-bar-inherit {
    /* background-color: #e9e9e9; */
    /* border-color: #ddd; */
    /* color: #333; */
    /* text-shadow: 0 1px 0 #eee; */
    /* font-weight: 700; */
}
jquery.mobile-1.4.5.min.css:3
.ui-header-fixed {
    top: -1px;
    padding-top: 1px;
}
jquery.mobile-1.4.5.min.css:3
.ui-header-fixed, .ui-footer-fixed {
    left: 0;
    right: 0;
    width: 100%;
    position: fixed;
    z-index: 1000;
}
jquery.mobile-1.4.5.min.css:3
.ui-header, .ui-footer {
    border-width: 1px 0;
    border-style: solid;
    position: relative;
}

.formset{
	position:absolute;
	width:100%;
	z-index: 999;
}
.overlay {
    background: #e9e9e9;
    display: none;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.5;
    z-index: 9999999 !important;
    height: 100% !important;
}
#loading-img {
    background: url(https://www.furtadosonline.com/assets/images/ajax-loader.gif) center center no-repeat;
    height: 100%;
    z-index: 999999;
  }
</style>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MGRL7HV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="top-logo"  data-role="header" data-position="fixed">
<div class="container">
<div class="row">
<div class="col-md-12">
 <!--<marquee width="100%"><span style="color:#fff;font-weight:bold;">Dear Customer, please note due to the recent government directive of operational lockdown for all non-essential services in Mumbai there will be a delay in processing your online orders. We regret the inconvenience. </span></marquee>-->
 <span><center><a style="color:#fff! important;" href="tel: +91 9152004939 ">Customer Services: +91 9152004939</a>
<span><center><a style="color:#fff! important;" href="tel: 022 42875060">| +91 (022) 4287 5060/5000</a></center></span>
  
			 <!--<a style="color:#fff! important;" href="tel: +91 93206 44558"> +91 93206 44558</a>
  </center></span>
  <span><center><a style="color:#fff; ">9.30 AM to 5.30 PM (MON - SAT)</a></center></span>
  <!-- <ul class="list-unstyled" style="float: left;">
     <li><a href="tel: 022 - 4287 5050/60">Support:+91 91520 04939</a></li><span style="color:#fff">|</span> 
    <li><a>9.00 AM to 7.00 PM (MON - SAT)</a></li>
  </ul>-->
<div class="col-xs-2">
  <?php include('left_menu.php'); 
  
?>

</div>
<div class="col-xs-6 logo" style="padding:0;">

<a href="<?echo SITEMOBURL;?>"><img src="<?php echo SITEIMAGES.'mobimg/icon/logo.png';?>"></a>
</div>

<div class="col-xs-4 headpt">

 
 <div class="col-xs-12">
 
       <a href="<?echo SITEMOBURL?>product/viewcart"><div class="col-xs-4"  style="
    width: 34% !important;">  <div style="float: right; cursor:pointer;"><i class="fa fa-shopping-cart" aria-hidden="true"><span class="badge badge-notify my-cart-badge" id="currentitemcount"></span></i> </div></div></a> 
	
     
      <?if($this->libsession->isSetSession('usrid')){?>
      <a href="<?echo SITEMOBURL?>user/getwishlist"><div class="col-xs-4 lvhert"> <div style="float: right; cursor:pointer;"><i class="fa fa-heart" aria-hidden="true"></i><span class="badge badge-notify my-cart-badge" id="wishlistcount"></span></i> </div></div></a>  

   <div class="dropdown">
  <button class="dropbtn" style="margin-left:43px;margin-top: -17px;"><i style="float: right; cursor: pointer;" class="fa fa-user-o" aria-hidden="true"></i></button>
  <div class="dropdown-content">
    <a href="<?php echo SITEMOBURL?>user/myaccount">My Profile</a>
    <a href="<?php echo SITEMOBURL?>product/getorderbyuser">My Orders</a>
	<a href="<?php echo SITEMOBURL?>user/getwishlist">My Wishlist</a>
    <a href="<?php echo SITEMOBURL?>user/logout">Log Out</a>
  </div>
</div>
     <?}else{?>
     
      <a href="<?echo SITEMOBURL?>user/"><div class="col-xs-4" style="width: 30% !important;"> <div style="float: right; cursor:pointer;"><i class="fa fa-heart" aria-hidden="true"></i><span class="badge badge-notify my-cart-badge"></span></i> </div></div></a> 
    
    <a href="<?echo SITEMOBURL?>user/"><div class="col-xs-4"style="width: 28% !important;"><div style="float: right; cursor: pointer;"> <i class="fa fa-user-o" aria-hidden="true"></i></div></div></a>
      <?}?>
   
  
</div>
</div>
</div>
</div>
</div>
<div class="row">
  <?php 
    date_default_timezone_set('Asia/Calcutta');
    $date=date('Y-m-d H:i');
    $defdate='2019-01-05 10:00:00';
  
    if($date > $defdate){
  ?>
   <!--<marquee behavior="scroll" direction="left" style="color:#fff"><i><b>We are currently upgrading our website and the Shipping of Orders may be delayed. We are sorry for any inconveniences caused.</b></i></marquee>-->

 <?php } ?>
  <!--<marquee behavior="scroll" direction="left" style="color:#fff"><i><b>Due to a major technology upgrade on our website we will be unable to ship orders till Thursday, October 25, 2018. Thank you for your understanding and sorry for the inconvenience.</b></i></marquee>-->
	 <!-- <marquee behavior="scroll" direction="left" style="color:#fff"><i>"Due to the uncertainty regarding the All India transporter Strike scheduled to begin on 20th July dispatch & delivery of products may be delayed. We apologies for any inconvenience that this may cause and thank you for your understanding." </i></marquee> -->
</div>

<div class="container search-top">

<div class="col-xs-0"></div>
            <div class="col-xs-12">
            <div class="flipkart-navbar-search smallsearch">
                <div class="row">
          <form method="GET" action="<?php echo SITEMOBURL?>product/search">
            <div style="border-style: none;border-radius: 0px !important; background: #ededed;" class="search-panel col-xs-2">
              <button type="button" class="btn btn-default dropdown-toggle btnadjst" data-toggle="dropdown" aria-expanded="false" style="height: 32px;border-radius: 0px;">
                <span id="search_concept" style="width: 40px;overflow: hidden;text-overflow: ellipsis;display: block;"><?php echo $search_concept; ?></span><span class="caret"></span>
              </button>
              <ul class="dropdown-menu selectdrp" role="menu">
                <li value="0"><a href="">ALL</a></li>
                <li value="1"><a href="">Musical Instruments</a></li>
                <li value="2"><a href="">Music Books</a></li>
                <li value="3"><a href="">Pro Audio</a></li>
              </ul>
            </div>
            <input type="hidden" name="maincat" value="0" id="search_param">
            <input class="flipkart-navbar-input col-xs-8" style="height: 32px;border-radius: 0px;" type="" placeholder="Search Products" name="searchText" id="search">
            <button class="flipkart-navbar-button col-xs-2" name="submit" id="submit" type="submit">
          </form>
		  
                        <svg width="0px" height="15px">
                        
              <li class="fa fa-search" aria-hidden="true" style="color:#d4d6df;"></li>
                        </svg>
                    </button>
                </div>
            </div>
      </div>
      
      <div class="col-xs-0"></div>
            
<!--<marquee width="100%"><span style="color:#fff; font-weight:bold;">Dear Customer, please note due to the recent government directive of operational lockdown for all non-essential services in Mumbai there will be a delay in processing your online orders. We regret the inconvenience. </span></marquee>   --> 

</div>
</div>


<div class="top-logo" style="padding-left: 10px; position: fixed; z-index: 9999;    top: 0;" id="stickyheader">
	<div class="container">
		<div class="col-xs-2">
		  <?php include('left_menu.php');?>
		</div>
		<div class="col-xs-8">
			<div class="flipkart-navbar-search smallsearch">
				<div class="row">
				  <form method="GET" action="<?php echo SITEMOBURL?>product/search"  class="formset">
					<input class="flipkart-navbar-input col-xs-10" type="" placeholder="Search Products" name="searchText" id="search1">
					<button class="flipkart-navbar-button col-xs-2" name="submit" id="submit" type="submit">
				  </form> 
					  <svg width="0px" height="15px">	 
						<li class="fa fa-search" aria-hidden="true" style="color:#d4d6df;"></li>
					  </svg>
					</button>
				</div>
			</div>
		</div>
		<div class="col-xs-2">
			<a href="<?echo SITEMOBURL?>product/viewcart">
				<div class="col-xs-4" style="width: 90% !important;">  
					<div style="float: right; cursor:pointer; padding: 5px;">
						
							<i class="fa fa-shopping-cart" aria-hidden="true">
								<span class="badge badge-notify my-cart-badge" id="currentitemcount1"></span>
							</i>
						
					</div>
				</div>
			</a> 
		</div>
	</div>
</div>

  <div class="overlay">
  <div id="loading-img"></div>
</div>

<div id='GotoTop' style="font-size: 11px;">Go To Top <i class="fa fa-arrow-up" aria-hidden="true"></i></div>

    <script type="text/javascript">
    jQuery(document).ajaxStart(function(){
      jQuery(".overlay").show();
     });

    jQuery(document).ajaxComplete(function(){
       jQuery(".overlay").hide();
    });
  </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
               ajax_getCartCount();
               ajax_wishlistcount();
             var jssor_1_options = {
              $AutoPlay: true,
              $SlideWidth: 600,
              $Cols: 2,
              $Align: 50,
       $SlideSpacing: 30,
      
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
          
            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        
                if (refSize) {
                    refSize = Math.min(refSize, 2000);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
      
      
       
        });
    </script>

<script type="text/javascript">
  $(document).ready(function(e){
   $('.logout-menu').on('mouseover' , function(e) {
      e.preventDefault();
      var param = $(this).attr("href").replace("#","");
      var concept = $(this).text();
      $('.search-panel span#search_concept').text(concept);
      $('.input-group #search_param').val(param);
   });

   $('.search-panel .dropdown-menu').find('a').click(function(e) {
      e.preventDefault();
      var param = $(this).attr("href").replace("#","");
      var concept = $(this).text();
      $('.search-panel span#search_concept').text(concept);
      $('.input-group #search_param').val(param);
   });
   
});

  jQuery('.selectdrp li').on('click', function(){
      jQuery('#search_param').val(jQuery(this).val());
  });

  // dropdown menu cloing on mouse leave
      $(".logout-menu").mouseleave(function(){
      $(".dropdown").removeClass("open");
      });
</script>


   
<script>
jQuery( function() {
	
  /*  jQuery( "#search" ).autocomplete({
      minLength: 2,
      delay: 0,
        source: function(request, response) {
            $.getJSON(
                "<?php echo SITEMOBURL?>product/autosuggest",
                { term:jQuery('#search').val()}, 
                response
            );
		},
	  select: function (event, ui) { window.location = ui.item.url;  }
    }).autocomplete("instance")._renderItem = function( ul, item ){
      return $( "<li class='autosuggest' style='z-index:9999;'>" )
        .append( "<span class='autocategory'>" + item.category + "</span>" )
        .appendTo( ul );
    }; */
	/* jQuery( "#search1" ).autocomplete({
      minLength: 2,
      delay: 0,
      source: "<?php echo SITEMOBURL?>product/autosuggest",
	  select: function (event, ui) { window.location = ui.item.url;}
    }).autocomplete("instance")._renderItem = function( ul, item ){
      return $( "<li class='autosuggest'>" )
        .append( item.label + "<span class='autocategory'>"+ item.category + "</span>" )
        .appendTo( ul );
    }; */
 });
  
</script> 

<script type="text/javascript">

var getUrlParameter = function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

jQuery('#search').val(getUrlParameter('searchText').replace("+", " "));
jQuery('#search1').val(getUrlParameter('searchText').replace("+", " "));
jQuery('#submit11').click(function(){ 
    var search=jQuery('#search').val();
    
    jQuery.ajax({
        url:"<?php echo SITEMOBURL?>product/search",
        type:'POST',
        
        data:{searchText:search},
        success:function(result)
        {
         // window.location.href="<?php echo SITEMOBURL?>home/search1";
        }

    });
    return false;
  });

</script>

<!--search-->


<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "70%";
    // document.getElementById("flipkart-navbar").style.width = "50%";
    document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "rgba(0,0,0,0)";
}

</script>
<script>
function ajax_getCartCount(){
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>product/getcartcount', 
           type:'POST'
          
        }).done(function(resp){
			if(resp){
          var response = jQuery.parseJSON(resp);
            if(response.status==1)
            {
				 if(response.cartcount > 0){  
                   jQuery('#currentitemcount').html(response.cartcount); 
				   jQuery('#currentitemcount1').html(response.cartcount); 
                 }
          
            }
            else
            {
                  if(response.cartcount > 0){ 
					jQuery('#currentitemcount').html(response.cartcount); 
                     jQuery('#currentitemcount1').html(response.cartcount); 
                   } 
            }
         }   
          return false;
      });
}
function ajax_wishlistcount(){
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>user/getwishlistcount', 
           type:'POST'
          
        }).done(function(resp){
          if(resp){
          var response = jQuery.parseJSON(resp);
           // alert(response.cnt);
            if(response.error==false)
            {
             if(response.cnt > 0){  
                 jQuery('#wishlistcount').html(response.cnt); 
             }
            }
            else
            {
                if(response.cnt > 0){ 
                    jQuery('#wishlistcount').html(response.cnt); 
              } 
            }
         }   
          return false;
      });
}
</script>

 <!-- Bootstrap core JavaScript banner top
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
   
    <!-- jssor slider scripts-->
   
    <script>
        jQuery(document).ready(function ($) {

            var options = {
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                $AutoPlay: 1,                                    //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
                $Idle: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $Jease$.$OutQuint,          //[Optional] Specifies easing for right to left animation, default value is $Jease$.$OutQuad
                $SlideDuration: 800,                               //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0,                                   //[Optional] Space between each slide in pixels, default value is 0
                $Cols: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)
              
                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Rows: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 8,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 8,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1,                                //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    $Scale: false                                   //Scales bullets navigator or not while slider scale
                },

                $ArrowNavigatorOptions: {                           //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>

<script>
  jQuery(document).ready(function(){
      /*Top button Script*/
		 jQuery(window).scroll(function() {
			 // console.log($('body').scrollTop());
			if (jQuery(this).scrollTop()) {
				jQuery('#GotoTop').fadeIn();
			} 
			else {
				//console.log("fadeOut");
				jQuery('#GotoTop').fadeOut();
			}
		});
		
		jQuery("#GotoTop").click(function () {
			jQuery("html,body").animate({scrollTop: 0}, 1000);
			//document.body.scrollTop = 0;
		});
	})
</script>
   
