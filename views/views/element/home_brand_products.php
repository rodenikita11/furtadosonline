        <?php 
			$brandDetails = $this->Brand_master->getAllActiveBrand();
			if($brandDetails) { 
		?>

	<section class="partners">
        <div class="block color-scheme-dark-90">
		<div class="container">
                    <div class="header-for-dark">
                        <h1 style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-duration: 2s; -moz-animation-duration: 2s; animation-duration: 2s;" class="wow fadeInRight animated" data-wow-duration="2s">SHOP BY <span>BRAND</span></h1>
                    </div>
			</div>
          <div class="container">
          <div class="row">
            <div class="span12">
              <div id="owl-demo" class="owl-carousel">
			  <?php foreach($brandDetails as $brandKey=>$brandVal) { ?>
                <div class="item"> 
				<a href="<?php echo $brandVal['url'];?>">
				<img alt="<?php echo $brandVal['brandname'];?>" class="owl-imgb" src="<?php echo $this->lib_storeuri->getprothumbnail(FILEBASEURL.IMGBRANDPATH.$brandVal['brandthumbnail'],500);?>">
				</a>
				</div>
				<?php } ?>
              </div>
            </div>
          </div>
        </div>
</div>
</section>


    <style>
    #owl-demo .item{
		background-image:url('url("http://192.168.1.100/spidermall/spider-frontend/assets/images/white-bg.png")')#fff;
		background-repeat:repeat;
		background:#fff;
		opacity:0.6;
		/*height:170px;
		width: 275px;
		height:170px;
		line-height:170px;*/
		vertical-align:center;
    }
    #owl-demo .item img{
        display: block;
        max-height: 170px;
		opacity:none !important;
		vertical-align:center;
    }
	.owl-imgb{margin-right:20px !important; text-align:center; line-height:170px;}

    </style>

    <script type="text/javascript">
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
      });

    });
    </script>
<?php } ?>