
<div class="modal fade mt80" id="review" role="dialog"  data-sid="<?echo $_GET['sid'];?>">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!--<h4 class="modal-title"><?php //echo $_SESSION['usrname'];?></h4>-->
          <h4 class="modal-title writetr"> Write Review </h4>
        </div>
        <div class="modal-body hebg">
        <!--main box-->
           <div class="row">
              <div class="col-sm-12 col-md-12">
                    <div class="panel ">
                        <div class="panel-body" >                
                            <form  name="reviewform" id="reviewform">
                              <input type="hidden" name="hiddenrating" id="hiddenrating" value="2">
                              <input type="hidden" name="productid" id="productid" value="<?php echo $this->uri->segment(2);?>">

                              <div id="rateYo"></div>
                              <br>
                              <input type="text" class="form-control newfrm" value="" id="reviewtitle1" name="reviewtitle1" placeholder="Review title"/>
                             
                              <textarea class="form-control counted newfrm" id="reviewmessage" name="message" placeholder="Type in your message" rows="2"></textarea>
                              
                              <button type="button" class="btn btn-info postbtn" id="postreviewdtl" name="postreviewdtl">Post Review<img src="<?php echo base_url(); ?>assets/images/preloader.gif" class="imgsc"></button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
       
      </div>
    </div>
  </div>
  <style type="text/css">
    label#reviewtitle1-error {color: #cd0029;}.mt80{margin-top: 80px;}.writetr{
      line-height: 25px;padding:5px 17px;}.hebg{height: 200px; background: #ffffff;}
      .newfrm{margin-top: 15px; border: 1px solid #ccc;}.postbtn{width: 200px;box-shadow: none ! important; outline: none;}.imgsc{display: none;margin-left: 6px;width: 21px;}
  </style>
  <script type="text/javascript">
    jQuery('#reviewform').validate({
        rules:{
            reviewtitle1:{required:true}
        },
        messages:{
          name:"Please Enter Review Title"
        }
    })
        
    jQuery(function () {
		jQuery("#rateYo").rateYo({
		rating:0,
		fullStar: true,
		onSet: function (rating, rateYoInstance) {
	      jQuery('#hiddenrating').val(rating);

	    }
	  });
	});
  </script>
  <script type="text/javascript">
  jQuery('#postreviewdtl').click(function(){
     var success=jQuery("#reviewform").valid();
     if(success){
    jQuery('#postreviewdtl').children('img').show();
    jQuery('#postreviewdtl').attr('disabled',true);  
    sid=jQuery('#review').attr('data-sid');
    if(sid){
      module='cleranceproduct';
    }else{
      module='nocleranceproduct';
    }
    
    reviewmessage=jQuery('#reviewmessage').val();
    reviewtitle1=jQuery('#reviewtitle1').val();
    ratingme=jQuery('#hiddenrating').val();
    proid=jQuery('#productid').val();
    proname=jQuery('.proname').attr('data-id');
    
    jQuery('#myModal').modal('hide');
    jQuery(".overlay").show();
    jQuery.ajax({
      url:'<?php echo SITEMOBURL?>product/addreview',
      type:'POST',
      data:{proid:proid,reviewmessage:reviewmessage,ratingme:ratingme,reviewtitle1:reviewtitle1,proname:proname,module:module},
      success:function(data1)
      {   
        //jQuery(".overlay").show();
      jQuery('#postreviewdtl').children('img').hide();
      jQuery('#postreviewdtl').attr('disabled',false);
        var response = jQuery.parseJSON(data1);
        if(response.error=='false'){
             jQuery('input[type="submit"]').attr('disabled','disabled');
             jQuery('#review').modal('hide');
             bootbox.alert('Review submitted successfully', function(){  
             window.location.reload(); });
            // bootbox.alert();  
            
          }else{
           bootbox.alert('Error while adding review to product');  
            
          }
        
      }
  });
  }
});

</script>