<style>label#uname-error,label#state-error,label#remail-error,label#city-error,label#address1-error,label#rpassword-error,label#cpassword-error,label#phoneno-error {
    color: red;
    font-weight: 400;
}</style>
 <div id="signup" class="modal fade">
     <?php //print '<pre>';print_R($signupstate);?>
                 <div class="modal-dialog">
                     <div class="modal-content">
                             
                         <div class="modal-body">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <div class="col-lg-12 well">
                                 <div class="row">
                                          <form  method="post" name="registerform" id="registerform" style="padding:10px 25px;margin-left: 27px;" >
                                             <div class="col-sm-12">
                                                 <div class="omb_login">
                                                   <div class=" omb_row-sm-offset-2 omb_loginOr">
                                                      <div class="col-xs-12 col-sm-12">
                                                         <hr class="omb_hrOr">
                                                         <span class="omb_spanOr">Sign up</span>
                                                      </div>
                                                   </div>
                                                 </div>

                                                <div class="row">
                                                   <div class="col-sm-12 form-group">
                                                    <i class="fa fa-user iconab" style="color: #000;"></i>  <input type="text" name="uname" id="uname" placeholder="Full Name" class="form-control" onkeypress="return isNumber(event)" tabindex="1">
                                                   </div>
                                                   <!-- <div class="col-sm-6 form-group">
                                                     <i class="fa fa-globe iconab"></i> 
													                            <select name="signupstate" id="signupstate" class="form-control st">
                                                      
                                                   </select>
                                                   </div> -->
                                                </div> 

                                                <div class="row">
                                                   <div class="col-sm-12 form-group">
                                                    <i class="fa fa-envelope-o iconab" style="color: #000;"></i>  <input type="text" name="remail" id="remail" placeholder="Email ID" class="form-control" tabindex="3">
                                                   </div>
                                                   <!-- <div class="col-sm-6 form-group">
                                                     <i class="fa fa-map-marker iconab"></i> <select id="signupcity" name="signupcity" class="form-control st">
                                                      <option value="Select">Select City</option>
                                                      <input type="hidden" name="hiddencity" id="hiddencity" value="<?php echo $custdtl[0]['city'];?>">
                                                   </select>
                                                   </div> -->
                                                </div>   

                                                <div class="row">
                                                   <div class="col-sm-12 form-group">
                                                    <i class="fa fa-phone iconab"></i>  <input type="text" name="phoneno" id="phoneno" placeholder="Phone No." class="form-control" tabindex="5">
                                                   </div>


                                                   <!-- <div class="col-sm-6 form-group">
                                                     <i class="fa fa-address-card iconab"></i> <input type="text" name="address1" id="address1" placeholder="Address Line 1" class="form-control" tabindex="6">
                                                   </div> -->
                                                </div>   

                                             
                                                <div class="row">
                                                   <div class="col-sm-6 form-group">
                                                    <i class="fa fa-unlock iconab"></i>  <input type="password" placeholder="password" class="form-control" name="rpassword" id="rpassword" tabindex="7">
                                                   </div>
                                                   <div class="col-sm-6 form-group">
                                                     <i class=""></i> <input type="password" name="cpassword" id="cpassword" placeholder="Confirm password" class="form-control" tabindex="8">
                                                   </div>
                                                </div>                 
                                                  
                                                     

                                               <input type="button" class="btn btn-lg btn-info col-lg-offset-3" value="Sign Up" id="registeruser" style="outline:none;" tabindex="9"> 
                                               <img id='loader' src="<?php echo base_url(); ?>assets/images/preloader.gif" style='display: none;position: absolute;right: 246px;width: 21px;bottom: 10px;'>    

                                             </div>
                                          </form> 
                                          </div>
                                   </div>
                         </div> 
                     </div>
                 </div>
             </div>
 <script type="text/javascript">
  jQuery(document).ready(function(){
      jQuery.ajax({
          url:'<?php echo SITEMOBURL?>user/getsignupstates',
          dataType:'JSON'
      }).done(function(data){
          var str='<option value="Select">Select State</option>';
          stateslength=data.signupstate.length;
          for(var i=0; i < stateslength; i++){
              str+='<option value="'+data.signupstate[i].stateName+'"  data-id="'+data.signupstate[i].id+'">'+data.signupstate[i].stateName+'</option>';
          }
          jQuery('#signupstate').html(str);
      })
  })


  jQuery( "#signupstate" ).change(function() {
    var sid=jQuery('#signupstate option:selected').data('id');

     jQuery.ajax({
       type:'POST',
       dataType:'JSON',
       url:'<?php echo SITEMOBURL?>user/getsignupcities',
       data:{sid:sid},
       success:function(result)
       {
         res=eval(result);
         if(res.status== true)
         {
           var str='';
           var hidcity=jQuery('#hiddencity').val();

           for (var i = 0; i < (res.msg.length); i++) {

             var resultcity = res.msg[i]['cityName'].replace(/ /g, "_");
             if(hidcity != " " && hidcity== resultcity){
                 var sele='selected';
             }else{
                 var sele='';
             }

             str+='<option value='+resultcity+' '+ sele+'>'+res.msg[i]['cityName']+'</option>';
           };
          // console.log(str);
         jQuery('#signupcity').html(str);
         //jQuery('#city').html(str);
         }else{
           bootbox.alert(res.msg);
         }
       }
     });
     return false;
   
});
  
</script>            