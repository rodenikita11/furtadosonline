<?php 
class Mailclass{
	
	const EMMAPIKEY='fa09c83a9d9ad16668288e55e0691357';
	const EMMAPIURL='https://api.exacttouch.com/API/mailing/';
	const EMMFROMNAME='Furtados Music';
	const EMMFROMEMAIL='response@furtadosonline.com';
	const EMMCCEMAIL='';
	const LISTID='38'; //list id from EMM, its static 
		
	
	function __construct($to, $subject, $html){
		$this->to=$to;
		$this->html=$html;
		$this->subject=$subject;
		
		//$this->sendmail($this->to);
	}
	
	public function sendmail(){
		$this->apitocheckexists='<DATASET>
									<CONSTANT><ApiKey>'.self::EMMAPIKEY.'</ApiKey>
									</CONSTANT>
									<INPUT><Emailaddress>'.$this->to.'</Emailaddress>
									</INPUT></DATASET>';
									
		$this->poststoexists=array('type'=>'contact', 'activity'=>'Query', 'data'=>$this->apitocheckexists);
		
		$this->chtoexist=curl_init();
		curl_setopt($this->chtoexist, CURLOPT_URL, self::EMMAPIURL);
		curl_setopt($this->chtoexist, CURLOPT_POST, count($this->poststoexists));
		curl_setopt($this->chtoexist, CURLOPT_POSTFIELDS, $this->poststoexists);
		curl_setopt($this->chtoexist, CURLOPT_RETURNTRANSFER, true);
		$this->xmlstrtoquery=curl_exec($this->chtoexist);
		curl_close($this->chtoexist);
		//print_r($this->xmlstrtoquery); die;
		if(!empty($this->xmlstrtoquery)){
			$this->queryobj= new SimpleXMLElement($this->xmlstrtoquery);
			$this->existedlist=$this->queryobj->OUTPUT->ListDetails;
			
			if($this->existedlist==''){
				$sendtomail=$this->addnewtolist();
			}else{
				$sendtomail=$this->queryobj->OUTPUT->Emailaddress;
			}
			$campaignid=$this->addcampaign();
			$this->result=$this->sendcampaign($campaignid, $sendtomail);
			return $this->result;
		}
	}
	
	public function addnewtolist(){
		$this->apitoadd='<DATASET>
				<CONSTANT><ApiKey>'.self::EMMAPIKEY.'</ApiKey>
				</CONSTANT>
				<INPUT>
				<Unique_id></Unique_id>
				<AddEmail>'.$this->to.'</AddEmail> <AttributeValues></AttributeValues>
				<Attributecount></Attributecount>
				<ListMember>'.self::LISTID.'</ListMember><DoubleOptin></DoubleOptin><TriggerEmail></TriggerEmail>
				</INPUT></DATASET>';
		
		$this->poststoadd=array('type'=>'contact', 'activity'=>'Add', 'data'=>$this->apitoadd);
		
		$this->chtoadd=curl_init();	
		curl_setopt($this->chtoadd, CURLOPT_URL, self::EMMAPIURL);
		curl_setopt($this->chtoadd, CURLOPT_POST, count($this->poststoadd));
		curl_setopt($this->chtoadd, CURLOPT_POSTFIELDS, $this->poststoadd);
		curl_setopt($this->chtoadd, CURLOPT_RETURNTRANSFER, true);
		$this->xmlstradd =curl_exec($this->chtoadd);
		curl_close($this->chtoadd);
		
		$this->addobject=new SimpleXMLElement($this->xmlstradd);
		return $this->to;
	}
	
	public function addcampaign(){
		$this->apitoaddcampaign='<DATASET>
									<CONSTANT>
									<ApiKey>'.self::EMMAPIKEY.'</ApiKey>
									</CONSTANT>
									<INPUT><Subject>'.$this->subject.'</Subject>
									<FromName>'.self::EMMFROMNAME.'</FromName>
									<FromEmail>'.self::EMMFROMEMAIL.'</FromEmail><DYNAMIC></DYNAMIC><ReplyTo></ReplyTo><TemplateID></TemplateID><SetLimit></SetLimit><MessageHTML><![CDATA['.$this->html.' To unsubscribe [UNSUBSCRIBE]]]></MessageHTML><MessageMobile><![CDATA[]]></MessageMobile>
									<Attcount></Attcount>
									</INPUT></DATASET>';
									
		$this->poststoaddcampaign=array('type'=>'message', 'activity'=>'Add', 'data'=>$this->apitoaddcampaign);
		$this->chtoaddcampaign=curl_init();
		curl_setopt($this->chtoaddcampaign, CURLOPT_URL, self::EMMAPIURL);
		curl_setopt($this->chtoaddcampaign, CURLOPT_POST, count($this->poststoaddcampaign));
		curl_setopt($this->chtoaddcampaign, CURLOPT_POSTFIELDS, $this->poststoaddcampaign);
		curl_setopt($this->chtoaddcampaign, CURLOPT_RETURNTRANSFER, true);
		$this->xmltoaddcampaign=curl_exec($this->chtoaddcampaign);
		curl_close($this->chtoaddcampaign);
		
		$this->addcampaignobj=new SimpleXMLElement($this->xmltoaddcampaign);
		$this->campaignid=$this->addcampaignobj->OUTPUT->MID;
		
		return $this->campaignid;
	}
	
	public function sendcampaign($campaignid, $sendtomail){
		$this->apitosendcampaign='<DATASET>
									<CONSTANT><ApiKey>'.self::EMMAPIKEY.'</ApiKey>
									</CONSTANT>
									<INPUT><MID>'.$campaignid.'</MID><Emails>'.$sendtomail.'</Emails></INPUT>
									</DATASET>';
									
		$this->poststosendcampaign=array('type'=>'message', 'activity'=>'Quicktest', 'data'=>$this->apitosendcampaign);
		
		$this->chtosendcampaign=curl_init();
		curl_setopt($this->chtosendcampaign, CURLOPT_URL, self::EMMAPIURL);
		curl_setopt($this->chtosendcampaign, CURLOPT_POST, count($this->poststosendcampaign));
		curl_setopt($this->chtosendcampaign, CURLOPT_POSTFIELDS, $this->poststosendcampaign);
		curl_setopt($this->chtosendcampaign, CURLOPT_RETURNTRANSFER, true);
		$this->xmltosendcampaign=curl_exec($this->chtosendcampaign);
		curl_close($this->chtosendcampaign);
		
		$this->sendcampaignobj=new SimpleXMLElement($this->xmltosendcampaign);
		$this->sendresult=$this->sendcampaignobj->TYPE;
		return $this->sendresult;
	}
}
?>