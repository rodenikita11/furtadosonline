<div class="container conmt">
   <div  class="col-xs-12">
      <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
             <p class="title" style="font-weight: bold;">Pay by Credit Card <i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Pay using Credit Card</p>
            <p class="m1" style="font-size: 12px">
               By clicking on the "Pay by Credit Card " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
               <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
              <br>
               <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
               <p style="font-size: 12px;">After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</p>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
            Pay by Credit Card
            </button></a>
         </div>
      </div>

      <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay by Debit Card <i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Pay using Debit Card</p>
            <p class="m1" style="font-size: 12px">
              By clicking on the "Pay by Debit Card " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
              <br>
               <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
               <p style="font-size: 12px;">After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</p>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
            Pay by Debit Card
            </button></a>
         </div>
      </div>

      <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay using Net Banking<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Select from more than 25 banks.</p>
            <p class="m1" style="font-size: 12px">
              By clicking on the "Proceed to Payment" button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
              <br>
               <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
               <p style="font-size: 12px;">After clicking on the "Proceed to payment" button, you will be directed to a secure gateway for payment (Techprocess). After completing the payment process, you will be redirected back to Furtadosonline.com</p>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
           Proceed to Payment
            </button></a>
         </div>
      </div>

      <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">NEFT / RTGS<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Select from more than 25 banks.</p>
            <p class="m1" style="font-size: 12px">
                  The NEFT/ RTGS should be in favor of “Furtados Music India Pvt Ltd.”  
                  <br>
                  <b>Beneficiary A/c no:</b>05922560004715<br>
                  <b>Beneficiary Add :</b> 201, Town Centre II, Andheri – Kurla Road, Marol, <br>
                  Andheri (E), Mumbai – 400059<br>
                 <b> Beneficiary Bank:</b> HDFC Bank<br>
                  <b>Beneficiary IFSC Code :</b> HDFC0000143 <br>
                  By clicking on the "Order Now" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/getneftpayment"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
           Order Now
            </button></a>
         </div>
      </div>

      <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay by Cheque / Demand Draft<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="m1" style="font-size: 12px">
                  Cheque / DD for the complete value (including shipping cost) should be drawn in favour of "Furtados Music India Pvt Ltd." Cheque / DD should be payable at par in Mumbai. Outstation cheques will not be accepted. Cheque / DD should be posted / couriered to our office address as follows:<br>
                 <b>Furtados Music India Pvt Ltd,</b><br>
                  201, Town Centre II, Andheri – Kurla Road, Marol, Andheri (E)<br>
                  Mumbai – 400 059 <br>
                 <b> Tel:</b>(022) 42875050 / 42875060 <br>
                 <b> Fax:</b>(022) 42875012 <br>
                  By clicking on the "Order Now" button and placing this order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/getchequepayment"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
           Order Now
            </button></a>
         </div>
      </div>


       <!--<div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay in Installments (HDFC Bank)<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
             <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Pay in easy Installment <span style="font-size:12px;">(HDFC credit card holder only)</span></p>
            <p class="m1" style="font-size: 12px">
                <input type="radio" name="paymentType" id="paymentType" value="6" onclick="show_3_month_Emi();">   &nbsp; 3 Months EMI (<i class="fa fa-inr"></i><span id="amount_3_amount">
                     </span>  per month)<br/>
            </p>
               <div id="3_month_Emi" style="display:none;">
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                        <b>Tenure </b>: 3 Months
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                        <b> Easy EMI Finance Charge </b> : 14% P/a 
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;" >
                        <b>EMI Payable Every Month </b>: <i class="fa fa-inr"></i> <span id="amount_3_amount1">
                        </span> 
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;" >
                        <b>Total Amount Payable over tenure (with interest) : <i class="fa fa-inr"></i></b> 
                        <span id="amount_3_total"></span>
                     </li>
                  </div>
                  <p class="m1">
                     <input type="radio" name="paymentType" id="paymentType" value="7" onclick="show_6_month_Emi();">
                     &nbsp; 6 Months EMI (<i class="fa fa-inr"></i> <span id="amount_6_amount1">
                     </span>   per month)
                  </p>
                  <div id="6_month_Emi" style="display:none;">
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                        <b>Tenure </b>:6 Months
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                        <b>Easy EMI Finance Charge </b>: 14% P/a 
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;">
                        <b>EMI Payable Every Month : <i class="fa fa-inr"></i></b> 
                        <span id="amount_6_amount"></span>
                     </li>
                     <li style="padding-top: 3px; margin-left: 60px; font-size: 12px;padding-bottom: 10px">
                        <b>Total Amount Payable over tenure (with interest) : <i class="fa fa-inr"></i></b> 
                        <span id="amount_6_total"></span>
                     </li>
                  </div>
                  <p class="m1">
                     <input type="radio" name="paymentType" id="paymentType" value="16" onclick="paydirectly(this);">
                     &nbsp; Pay directly (<i class="fa fa-inr"></i>  <span id="total_amount">
                     </span>)
                  </p>

                  <p class="m1" style="font-weight:normal;font-size: 12px">By clicking on the "Pay by credit card / Debit card" button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a></p>
                  <p class="m1" style="font-weight:normal;font-size: 12px">Note: After clicking on "Pay" button you will be redirected to our HDFC payment gateway.  After completing the payment process, you will be redirected back to   Furtadosonline.com</p>

                 <a href="<?php echo SITEMOBURL; ?>product/hdfc_payment">
                  <button type="button" class="btn btn-warning pd" 
                   style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
                   Proceed to Payment
                </button>
              </a>
         </div>
      </div>-->

    <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay By EMI<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
        <div class="panel-content">
            <!--<a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
            Pay By EMI
            </button></a>-->
            <form action="distributepg" method="POST">
                <div class="form-group">
                   <label for="emibanks">Select Bank</label>
                   <select class="form-control" id="emibanks" name="emibanks">
                      <option value="">Select Bank</option>
                      <?php foreach($emibanks as $val){ ?>
                         <option value="<?php echo $val['eb_id']?>"><?php echo $val['eb_name']; ?></option>
                      <?php } ?>
                   </select>
					<p class="m1" style="font-size: 12px">
					  By clicking on the "Pay by EMI" button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
						  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
					  <br>
					   <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
					   <p style="font-size: 12px;">After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</p>
					</p>
                </div>
                <div class="form-group">
                   <button type="submit" name="submit" class="btn btn-warning pd" style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">PAY BY EMI</button>
                </div>
             </form>
		</div>
	</div>
	<div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay by Wallet<i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Pay using Wallet</p>
            <p class="m1" style="font-size: 12px">
              By clicking on the "Pay by Wallet " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
              <br>
               <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
               <p style="font-size: 12px;">After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</p>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
            Pay by Wallet
            </button></a>
         </div>
      </div>
	  <div class="accordion">
         <div class="panel" style="margin-bottom: 0px;">
            <p class="title" style="font-weight: bold;">Pay by UPI <i class="fa fa-angle-down" style="font-size:24px;float: right;"></i></p>
         </div>
         <div class="panel-content">
            <p class="title" style="font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Pay using UPI</p>
            <p class="m1" style="font-size: 12px">
              By clicking on the "Pay by UPI " button and placing this  order, you have read and agreed to furtadosonline.com  <a href="terms.php" class="bl_12">Terms of Use</a> and 
                  <a href="privacyPolicy.php" class="bl_12">Privacy Policy.</a>
              <br>
               <strong style="font-size: 14px;font-weight: bold;color: #cd0029;text-transform: uppercase;margin: 0px 0px 4px;">Note:</strong>
               <p style="font-size: 12px;">After clicking on "Pay" button you will be redirected to Razorpay payment gateway. After completing the payment process, you will be redirected back to Furtadosonline.com</p>
            </p>
            <a href="<?php echo SITEMOBURL; ?>product/distributepg"> <button type="button" class="btn btn-warning pd" 
               style="background: #cc0028;color: #fff;font-size: 14px;border: 1px solid #cc0028;text-transform: uppercase;    border-radius: 0px;margin: 8px 0px 10px;width: 100%;cursor: pointer;">
            Pay by UPI
            </button></a>
         </div>
      </div>

<style type="text/css">
.accordion {
    margin: 10px 0px;
}
   .conmt{
      padding-left: 0px;
    padding-right: 0px;
    margin-top: 5px;
    
   }
   .error {
   color: #cc0028;
   font-weight: 600;
   }
   div.bhoechie-tab-menu{
   padding-right: 0;
   padding-left: 0;
   padding-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group{
   margin-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group>a{
   margin-bottom: 0;
   }
   div.bhoechie-tab-menu div.list-group>a .glyphicon,
   div.bhoechie-tab-menu div.list-group>a .fa {
   color: #5A55A3;
   }
   div.bhoechie-tab-menu div.list-group>a:first-child{
   border-top-right-radius: 0;
   -moz-border-top-right-radius: 0;
   }
   div.bhoechie-tab-menu div.list-group>a:last-child{
   border-bottom-right-radius: 0;
   -moz-border-bottom-right-radius: 0;
   }
   div.bhoechie-tab-menu div.list-group>a.active,
   div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
   div.bhoechie-tab-menu div.list-group>a.active .fa{
   background-color: #cc0028;
   border-color: #cc0028;
   color: #ffffff ! important;
   }
   div.bhoechie-tab-menu div.list-group>a.active:after{
   content: '';
   position: absolute;
   left: 100%;
   top: 50%;
   margin-top: -13px;
   border-left: 0;
   border-bottom: 13px solid transparent;
   border-top: 13px solid transparent;
   border-left: 10px solid #cc0028;
   }
   div.bhoechie-tab-content{
   background-color: #ffffff;
   /* border: 1px solid #eeeeee; */
   padding-left: 20px;
   padding-top: 10px;
   }
   div.bhoechie-tab div.bhoechie-tab-content:not(.active){
   display: none;
   }
   .list-group-item{
   font-size:11px;
   }
   .delhe {font-size:10px;}
   *{margin:0; padding:0;}
   
   .panel-content{ 
      display:none;
      padding: 10px;
      border: 1px solid #eae4e4;
      min-height: 260px;
      height: 250px;
   }
   .panel p {
    background: #f5f5f5;
    padding: 10px 14px;
    border-bottom: 1px solid #ded8d8;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 500;
}
</style>
<script type="text/javascript">
   jQuery(document).ready(function() {
         jQuery("div.bhoechie-tab-menu>div.list-group>a").click(function(e){
         e.preventDefault();
         jQuery(this).siblings('a.active').removeClass("active");
         jQuery(this).addClass("active");
         var index = $(this).index();
         jQuery("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
         jQuery("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
      });
   });
   
   
   $(document).ready(function(){
    $(".panel").click(function(){
   $(this).next().slideToggle('slow');
   $(this).parent().siblings().find('.panel-content').slideUp();
   
    });
   });
   
</script>