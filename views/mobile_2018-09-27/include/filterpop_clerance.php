<?php //print '<pre>'; print_R($filter);?>
<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="btn btn-default" data-dismiss="modal"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></div>
        <p class="pricerange">
          <label for="price" class="pricepr">Price range:</label>
          <p id="amount2"></p>
          <p id="amount"></p>
        </p>
        <div id = "slider-3"></div>
        <input type="hidden" id="amount1" value="<?echo $_GET['pricestart']?>">
        <input type="hidden" id="amount2" value="<?echo $_GET['priceend']?>">
        <ul>
          <li class="_3d7x0n " onclick="optionshow('brand')">
            <p class="_2-yGHU">Brand</p>
            <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
            </svg>
          </li>
          <li class="_3d7x0n" onclick="optionshow('avaliable')">
            <p class="_2-yGHU">Availability</p>
            <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
            <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
            </svg>
          </li>
          <?foreach($getfilter['attgrp'] as $key => $val){?>  
            <li class="_3d7x0n" onclick="optionshow('<?echo str_replace(' ','',$val['name']);?>')">
              <p class="_2-yGHU"><?echo $val['name'];?></p>
              <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
              <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
              </svg>
            </li>
          <?}?>
        </ul>
        <div class="modal-footer">
          <div class="col-xs-6" style="margin-right: -75px;"><button type="button"  style=" background:#cd0029 !important;text-shadow: none;color: #fff; padding: .7em 1em;    border: none;" id="clearattr">Clear</button></div>
          <div class="col-xs-6" id="getfilter">
            <button type="button" style=" background:#cd0029 !important;text-shadow: none;color: #fff;  padding: .7em 1em;    border: none;">Apply</button>
          </div>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        </div>
      </div>
   </div>
</div>
<div id="myModaloption" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="Container container-fluid bg-1 text-center mainatt emi_1 brandlist overflow "  style="display:none;">
            <div class="row">
               <div class="col-xs-12 pricerange22" >
                  <div class="col-xs-2">BRAND</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <div class="overflowflit">   
               <?foreach($filter['brand'] as $key => $val){?>
               <div class="emi_2  bg-2 text-center   ">
                  <div class="row ">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 search"><?echo $val['brandname'];?>(<?echo $val['brandcount'];?>)</div>
                        <?if(in_array($val['brandid'],$param['branddtl'])){
                           $selected="checked";}else{
                           $selected='';
                           }?>
                        <div class="col-xs-2">
                           <b><input id="checkbox-<?echo $val['brandid']?>" class="checkbox-custom attributebrand" name="checkbox-2" type="checkbox" value="<?echo $val['brandid']?>" <?echo $selected;?>>
                           <label for="checkbox-2" class="checkbox-custom-label"></label>
                           </b>
                        </div>
                     </div>
                  </div>
               </div>
               
               <?}?>
               </div>
               <div class="Container container-fluid  fit">
                  <div class="row">
                     <div class="col-xs-6" >
                        <input type="button" value="Done" onclick="goback();"
                         style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                           padding:10px;width: 100%;margin: 0px -4px;border: none;">
                     </div>
                     <div class="col-xs-6" >
                        <input type="button" value="Close" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                          padding: 10px;border: none;width: 100%;">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--  <div class="Container container-fluid  bg-1 text-center mainatt emi_1 percentage "  style="display:none;">
            <div class="row">
                <div class="clear"></div>
                <div class="col-xs-12" >
                  <div class="col-xs-10 ">PERCENTAGE</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
                 </div>
                <div class="clear"></div> 
                <?php //print_r($getfilter['percentage']);?>
                <?//foreach($getfilter['respercentage'] as $key => $val){?>   
                    <div class="Container container-fluid  bg-1 text-center emi_1  ">
                      <div class="row">
                        <div class="col-xs-12" >
                            <div class="col-xs-10 ">% <?php //echo $val['percentage']?></div>
                          <?//if(in_array($val['percentage'],$param['percentage'])){
               //  $selected="checked";}else{
                 //$selected='';
               //}?>
                            <div class="col-xs-2">
                              <b><input id="checkbox-<?//echo $val['percentage']?>" class="checkbox-custom attributepercentage" name="checkbox-2" type="checkbox" value="<?//echo $val['percentage']?>" <?//echo $selected;?>>
                                <label for="checkbox-2" class="checkbox-custom-label"></label>
                              </b>
                            </div>
                        </div>
                      </div>
                    </div>
                <?//}?>
            
            
                <div class="Container container-fluid ">
                  <div class="row">
                    <div class="col-xs-12" >
                     <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                        padding: 1em 2em;">
                    </div>
                  </div>
                </div>
            </div>
            </div> -->
         <div class="Container container-fluid  bg-1 text-center mainatt emi_1 avaliable "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 ">Availability</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <?if(!$filter1){?>
                    <? if(in_array(1,$param['avaliable'])){
                      $selected1="checked";}else{
                      $selected1="";
                    }?>
                    <? if(in_array(2,$param['avaliable'])){
                      $selected2="checked";}else{
                      $selected2="";
                    }?>  
                    <?if($filter['available'][0]['clearanceid'] == 1 ){
                      $clearancename1 = 'Available';
                    } else {
                      $clearancename1 = 'Available';
                    }
                    if ($filter['available'][1]['clearanceid'] == 2) {
                      $clearancename2 = 'Sold Out';
                    } else {
                      $clearancename2 = 'Sold Out';
                    }?>
                  <?} else {?>
                    <? if(in_array(1,$param['avaliable'])){
                      $selected1="checked";}else{
                      $selected1="";
                    }?>
                    <? if(in_array(0,$param['avaliable'])){
                      $selected2="checked";}else{
                      $selected2="";
                    }?>  
                    <?if($filter['available'][1]['offerid'] == 1 ){
                      $clearancename1 = 'Available';
                    } else {
                      $clearancename1 = 'Available';
                    }
                    if ($filter['available'][0]['offerid'] == 0) {
                      $clearancename2 = 'Not Available';
                    } else {
                      $clearancename2 = 'Not Available';
                    }?>
                  <?}?>
               <div class="Container container-fluid  bg-1  text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 "><?php echo $clearancename1;?>(<?echo $filter['available'][0]['count'] ? $filter['available'][0]['count'] : 0 ;?>)</div>
                        <div class="col-xs-2"><b><input id="checkbox-1" class="checkbox-custom attributeavaliable" name="checkbox-3" type="checkbox" value="1" <?echo $selected1;?>>
                           <label for="checkbox-3" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 "><?php echo $clearancename2;?> (<?echo $filter['available'][1]['count'] ? $filter['available'][1]['count'] : 0 ;?>)</div>
                        <div class="col-xs-2"><b><input id="checkbox-0" class="checkbox-custom attributeavaliable" name="checkbox-4" type="checkbox" value="0" <?echo $selected2;?>>
                           <label for="checkbox-4" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;    border: none;
                  padding: 1em 2em;">
            </div>
         </div>
         <!---attribute-->
         <? 
            //print_R($getfilter['attr']);die;
          foreach($getfilter['attgrp'] as $key=>$val){?>
          <div class="Container container-fluid  bg-1 text-center mainatt emi_1 <?echo str_replace(' ','',$val['name']);?> "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 "><?echo $val['name']?></div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <?
              foreach ($val['att'] as $key1 =>$valatt){?>  
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 "><? echo $valatt['attname'];?></div>
                        <div class="col-xs-2"><b><input id="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom attribute" name="checkbox-<?echo $valatt['id'];?>" type="checkbox" value="<?echo $valatt['id'];?>" <? //echo $selected1;?>>
                           <label for="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <?}?>
               <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;    border: none;
                  padding: 1em 2em;">
            </div>
          </div>
         <?}?>  
         <!--- end---->
      </div>
   </div>
</div>
<script>
   jQuery(document).ready(function(){
     jQuery( "#amount" ).hide();
     jQuery( "#amount2" ).html( "Rs." + <?php echo $filter['minprice']?> +" - Rs." + <?php echo $filter['maxprice']?> );
    
   });
</script>
<script>
   jQuery(function() {
      jQuery( "#slider-3" ).slider({
        range: true,
        min:<?echo $filter['minprice']?>,
        max: <?echo $filter['maxprice']?>,
        values: [<?echo $filter['minprice']?>, <?echo $filter['maxprice']?> ],
        slide: function( event, ui ) {
          jQuery( "#amount2" ).hide();
          jQuery( "#amount" ).show();
          jQuery( "#amount" ).html( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
          jQuery( "#amount1" ).val(ui.values[ 0 ]);
          jQuery( "#amount2" ).val(ui.values[ 1 ]);
        }
      });
      jQuery( "#amount" ).html( "Rs." + jQuery( "#slider-3" ).slider( "values", 0 ) +
       " - Rs." + jQuery( "#slider-3" ).slider( "values", 1 ) ); 
    });
   
   var nodata=true
   var scrollval=800;
   jQuery(document).ready(function () {
      jQuery(window).scroll(function () {
          if (jQuery(this).scrollTop() > scrollval) {
              jQuery('.scrollup').fadeIn();
              scrollval+=500;
              if(nodata==true){
               showproducts();
       }
          } else {
              jQuery('.scrollup').fadeOut();
         }
      });
   }) 
   
   jQuery('.clearattr').bind('click',function(){
   groupid = jQuery(this).attr('data-id');
   
   jQuery('.attrgrp_'+groupid).prop("checked", false);
   });
   
   jQuery('#getfilter').bind('click',function(){
   curURI=window.location.pathname;
   
   var jstr = ''; 
   var jstr1='';
   jQuery(".attribute:checked").each(function() {
    if(jstr==''){
      jstr += '[' + jQuery(this).val();
    }else{
      jstr += ',' + jQuery(this).val();
    }
   });
      jQuery(".attributebrand:checked").each(function() {
    if(jstr==''){
      jstr += '&brand=[' + jQuery(this).val();
    }else{
      jstr += ',' + jQuery(this).val();
    }
   });
   
   
   jQuery(".attributeavaliable:checked").each(function() {
    
    if(jstr1==''){
      jstr1 += '&avaliable=[' + jQuery(this).val();
    }else{
      jstr1 += ',' + jQuery(this).val();
    }
    
   });  
   
   if(jstr!=''){ jstr += ']'; }
   if(jstr1!=''){   jstr1 += ']';}
   
   var min = jQuery("#amount1").val(); 
   var max = jQuery("#amount2").val();
   
   if(min < max)
   { 
    jstr += '&pricestart='+min; 
    jstr += '&pricend='+max; 
   } 
   	var sort = $( "#prosuctsort_opt option:selected").val();
    if(sort==undefined){sort = 'popular';}
    var moduleid='<?php echo $_GET['id'];?>';
	var offer = '<?php echo $this->uri->segment(2);?>';
	var promourl=window.location.href;
    if(offer == 'promotion'){ 
        //console.log(<?php echo $_GET['id'];?>); return false; 
         window.location.href=promourl+'&filter='+jstr+jstr1+"&sort="+sort;
     }else{
 		if(jstr && jstr1){
	        window.location.href=curURI+'?filter='+jstr+jstr1+"&sort="+sort;
	       } else if(jstr){
	         window.location.href=curURI+'?filter='+jstr+"&sort="+sort;

	       } else if(jstr1){
	         window.location.href=curURI+'?filter='+jstr1+"&sort="+sort;
	       }else {
	            window.location.href=curURI+"?sort="+sort;   
	       }
 	}
   // if(jstr && jstr1){
   //    window.location.href=curURI+'?filter='+jstr+jstr1;
   //    }else if(jstr){
   //    window.location.href=curURI+'?filter='+jstr;
    
   //  }else if(jstr1){
   //    window.location.href=curURI+'?filter='+jstr1;
   //  }else{
   //       window.location.href=curURI;
   //    }
   });
   

  function optionshow(ele){
   jQuery('#myModal').modal('hide');
   jQuery('#myModaloption').modal('show');
   jQuery('.mainatt').hide(); 
     if(ele=='brand'){
        // jQuery('.avaliable').hide();  
       jQuery('.brandlist').show();  
     }else if(ele=='avaliable'){
       //jQuery('.brandlist').hide();  
       jQuery('.avaliable').show();  
     }
     else if(ele=='percentage'){
       //jQuery('.brandlist').hide();  
       jQuery('.percentage').show();  
     }else{
     /// Query('.em_1').hide(); 
     jQuery('.'+ele+'').show();  
       //jQuery('.avaliable').hide();  
       //jQuery('.brandlist').hide();   
     } 
  }
  function goback(){
    jQuery('#myModaloption').modal('hide'); 
    jQuery('#myModal').modal('show');

  }
   // When the user scrolls down 20px from the top of the document, show the button
   window.onscroll = function() {scrollFunction()};
   
   function scrollFunction() {
       if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
           document.getElementById("myBtn").style.display = "block";
       } else {
           document.getElementById("myBtn").style.display = "none";
       }
   }
   
   // When the user clicks on the button, scroll to the top of the document
   function topFunction() {
       document.body.scrollTop = 0;
       document.documentElement.scrollTop = 0;
   }
</script>