<div class="main-quickview">
			<div class="three-box">
				<div class="product-img">
						<div class="new-img-slider left">
		<?php if(!empty($productImgDetail)) { ?>
	
	<?php if(count($productImgDetail)>3) { ?>
		<div class="btnprev" style="position:absolute; left: 3.5%; top:10px;"><i class="fa fa-sort-asc" style="color: #fc5602;cursor: pointer;font-size: 36px;"></i></div>
		<div class="btnnext" style="position:absolute; top: 332px; left: 3.5%;"><i class="fa fa-sort-desc" style="color: #fc5602;cursor: pointer;font-size: 36px;"></i></div>
			<?php } ?>
			
			
		<div style="float:left; margin-right:10px;" id="my_carousel">
			<ul style="list-style:none; padding:0px; margin-top:20px;">
				<?php foreach($productImgDetail as $imgKey=>$imgVal):?>
				<li class="hand" style="border-style: solid; border-color: rgb(238, 238, 238); border-width: 1px; margin: 1px; padding: 1px; text-align:center;"><img style="max-height:350px;" id="child_img_<?php echo $imgKey;?>" onclick="changeMirrorImg(<?php echo $imgKey;?>);" rel="<?php echo $this->lib_storeuri->getprothumbnail($imgVal,650);?>" src="<?php echo $this->lib_storeuri->getprothumbnail($imgVal,65);?>"></li>
				<?php endforeach; ?>
			</ul>
		</div>
			<?php } $arrayKeys = array_keys($productImgDetail); ?>
		<div style="float: left; vertical-align: middle; line-height: 310px; position: absolute; left:85px; top:20px; border:solid 1px #eee; width:380px; text-align:center;">
			<img id="target_mirror" class="big-image" style=" max-height:210px; vertical-align:middle; text-align:center;" src="<?php echo $this->lib_storeuri->getprothumbnail($productImgDetail[$arrayKeys[0]],650);?>">
		</div>
    </div>
				</div>
			<!--end product-->
			<!--product Detail-->
				<div class="qucik-view-pr-dt">
					<h1 id='promainid' data-seller='<?php echo $productDetail[0]['seller_id'];?>'><?php echo $productDetail[0]['proname'];?></h1>
					<div class="pr-code"><span>Product Code : </span><?php echo $productDetail[0]['prosku'];?></div>
					<div class="availble"><span>Availibility : </span><?php echo (($productDetail[0]['quantity']>0?"Available":"Out of stock"));?></div>
					<div class="price">
						<span >Best Price:</span>
						
						<?//print_R($productDetail); die;?>
							<?php if(!empty($productDetail['finalprice'])) { ?>
								<?php if($productDetail['marketprice'] > $productDetail['finalprice']) { ?>
									<p class="product-price"><span class="fS13 text-hides"><i class="fa fa-inr color2 fS13"></i><?php echo $productDetail['marketprice']?></span>&nbsp;&nbsp;<i class="fa fa-inr fS18 color1"></i><span class="fS24 color1"> <?php echo $productDetail['finalprice']?></span></p>
								<?php } else { ?>
									<p class="product-price">
										<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1"><?php echo $productDetail['finalprice']?></span>
									</p>
							<?php } ?>
							<?php $totalprice = $productDetail['finalprice']; ?>
							<? } else { ?>
							<p class="product-price">
								<i class="fa fa-inr fS18 color1"></i><span class="fS18 color1">Unable to retrieve price</span>
							</p>
							<?}?>
						
					
					</div>
					<?php if(!empty($productDetail['offerprice'])) { ?>
					<div class="price">
					
						<span >Offer Price:</span>
                          <p class="product-price"><span class="fS24 color1"><i class="fa fa-inr color2 fS13"></i><?php echo $productDetail['offerprice']?></span>&nbsp;&nbsp;</p> 
					</div>
					<?}?>
					<!--<div class="quantity"><span>Quantity : </span><input type="number" class="styler" name="" value="1" min="0"></div>-->
					<style type="text/css">
					.des-txt ul
					{
						padding-left:13px;
					}
					</style>
					<div class="description"><span>Description : </span><div class="des-txt"><?php echo $productDetail[0]['proshortspecification'];?></div></div>
					<div class="add-to-cart">
						<span><a class="product2cart" data-id="<?php echo $productDetail[0]['prosellerid'];?>" href="javascript:void(0);"><i class="fa fa-shopping-cart"></i> ADD TO CART</a></span>
						<a href="javascript:void(0);" title="Add to Wishlist" class="addtowishlist" data-id="<?php echo $productDetail[0]['prosellerid'];?>"><i class="fa fa-heart aws-1"></i></a>
						<a href="#" title="Share On Facebook"><i class="fa fa-facebook aws-1 social-icon1"></i></a>
						<a href="#" title="Share On Twitter"><i class="fa fa-twitter aws-1"></i></a>
					</div>
					<?php if($productDetail[0]['giftwrapping']=='yes') { #-- by sanjay, 8/Dec ?>
					<div>
					<input type="checkbox" class="is_giftwraped" name="is_giftwraped" value="Y" id="is_giftwraped_<?php echo $productDetail[0]['prosellerid'];?>"/><span>&nbsp;&nbsp;Gift wrap available for 
							<?php if($productDetail[0]['wrappingprice']>0) { ?>
					<i class="fa fa-inr ruppe1 color2" style="font-size:13px;"></i><?php echo $productDetail[0]['wrappingprice'];?>/quantity
							<?php } else { ?>
								free.
							<?php } ?>
					</span>
					</div>
					<?php } ?>
				</div>
			<!--end product Detail-->
			<!--product Brand-->
				<div class="quick-view-brand">
					<div onclick="hidepopup();"><i class="fa fa-times-circle close-icon pull-right"></i></div>
					<!--brand-->
					<div class="heading">BRAND</div>
					<div class="brand-img"><img src="<?php echo	$this->lib_storeuri->getprothumbnail(FILEBASEURL.IMGBRANDPATH.$productDetail[0]['brandthumbnail'],65);?>" />
					</div>
					<div class="brand-name"><?php echo $productDetail[0]['brandname'];?></div>
					<!--end barnd-->
					<!--seller-->
					<div class="seller-heading">SELLER NAME</div>
					<div class="seller-input">
						<div class="seller-name"><?php echo $productDetail[0]['name'];?></div>
						<div class="input-heading">Check Availability at</div>
						<span><input type="text" data-id='<?php echo $productDetail[0]['proid'];?>' placeholder="Pincode" id="buyers_pincode"> <button onclick="get_courier_charges();">CHECK</button></span>
						<div class="clear"></div>
						<input type="hidden" name="hidden_sell" id="hidden_sell" readonly="readonly" value="<?php echo $productDetail[0]['seller_id'];?>">
						<div class="input-heading" id="courier_charges_status"></div>
						<div class="input-heading" id="COD_status"></div>
					</div>
					<!--end seller-->
				</div>
			<!--End product Brand-->
			<div class="clear"></div>
			</div>
<div class="clear"></div>
			<!--related product-->
			<?php if(!empty($availableProducts)) { ?>
				<div class="related-pr">
					<h1>Also Available</h1>
					<div>
					<ul>
					  <?php foreach($availableProducts as $availableKey=>$availableVal) : ?>
						<li><a href="javascript:void(0);" onclick="quickview(<?php echo $availableVal['proid'];?>);"><img style="max-height:100px;" src="<?php echo $this->lib_storeuri->getprothumbnail($availableVal['prothumbnail'],120);?>" /></a></li>
			<?php endforeach;?>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
				<?php } ?>
			<!--end related product-->
		</div>

		<script type="text/javascript">
/*
		Script for carousel activity
		CREATED : 19/Sep
		AUTHOR : SANJAY
*/
jQuery(document).ready(function(){
		var step = 1; 
		var current = 0; 
		var maximum = jQuery('#my_carousel ul li').size(); 
		var visible = 3; 
		var speed = 100; 
		var liSize = 85;
		var carousel_height = 305;
		
		var ulSize = liSize * maximum;   
		var divSize = liSize * visible; 
	//-- Basic initialization.	
jQuery('#my_carousel ul').css("top", -(current * liSize)).css("position", "absolute");
jQuery('#my_carousel').css("width",'114px').css("height", carousel_height+"px").css("visibility", "visible").css("overflow", "hidden").css("position", "absolute").css('top','36px');
	//-- Navigation click activity
		jQuery('.btnnext').click(function() { 
			if(current + step < 0 || current + step > maximum - visible) {return; }
			else {
				current = current + step;
				jQuery('#my_carousel ul').animate({top: -(liSize * current)}, speed, null);
			}
			return false;
		});
 
		jQuery('.btnprev').click(function() { 
			if(current - step < 0 || current - step > maximum - visible) {return; }
			else {
				current = current - step;
				jQuery('#my_carousel ul').animate({top: -(liSize * current)}, speed, null);
			}
			return false;
		});
	
});

/*
	Used for chaing destination image, for selected image.
	SANJAY, 20/Sep
*/
changeMirrorImg = function(imgSelector){
		jQuery("#target_mirror").attr('src',jQuery("#child_img_"+imgSelector).attr('rel'));
};

  jQuery(document).ready(function(){
		addtowishlist();
  });
</script>
<style>			
a {
  color: #900;
  text-decoration: none;
}

a:hover {
  color: red;
  position: relative;
}

a[title]:hover:after {
  content: attr(title);
  padding: 0px 10px;
  height:40px;
  line-height:40px;
  color: #fc5602;
  position: absolute;
  font-size:12px;
  right: -1px;
  top:-30px;
  white-space: nowrap;
  z-index: 99999px;
  background:#fff;
  border:solid 1px #fc5602;
}
</style>
		