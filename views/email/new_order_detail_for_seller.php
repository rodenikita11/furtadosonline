<table id="summary" style="width:100%;border-collapse:collapse;"> 
           <tbody>
            <tr> 
             <td style="vertical-align:top;font-size:13px;line-height:18px;font-family:Arial, sans-serif;"> <h3 style="font-size:18px;color:rgb(204, 102, 0);font-weight:normal;">Hello,</h3> 
			 <p style="font:12px/ 16px Arial, sans-serif;">New Order received from SpidersMall.</p>
			 </td> 
            </tr>
            <tr> 
             <td style="vertical-align:top;font-size:13px;line-height:18px;font-family:Arial, sans-serif;"> 
				<p>Your order details for Order# <?php echo $current_cart_details[0]['om_invoicenum'];?></p>
				<table border="1">
					<tr>
						<th>#</th>
						<th style="width:250px;">Name</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Courier Charges</th>
						<th>Gift wrapped</th>
						<th>Sub-total</th>
					</tr>
					<?php  $counter=0;
					foreach($current_cart_details as $order=>$orderDetails) { ?>
						<tr>
							<td><?php echo ++$counter;?></td>
							<td style="width:250px;"><?php echo $orderDetails['op_product_name'];?></td>
							<td><?php echo $orderDetails['op_product_quantity'];?></td>
							<td><?php echo $orderDetails['op_product_price'];?></td>
							<td><?php echo $orderDetails['op_delivery_charges'];?></td>
							<td><?php echo (($orderDetails['op_gift_wrap']=="N"?"No":"Yes"));?></td>
							<td><?php echo $orderDetails['op_total_price'];?></td>
						</tr>
					<?php } ?>
				</table>
			 </td> 
            </tr>
			<tr>
				<td>
					<table>
						<tr>
							<td>
								<p><b>Seller Details</b></p>
								<p><?php echo $current_cart_details[0]['agent_seller_fname'].' '.$current_cart_details[0]['agent_seller_lname'];?></p>
								<p><?php echo $current_cart_details[0]['agent_seller_email1'];?></p>
								<p><?php echo $current_cart_details[0]['agent_seller_shopname'];?></p>
								<p><?php echo $current_cart_details[0]['agent_seller_shop_address'];?></p>
								<p><?php echo $current_cart_details[0]['agent_seller_shop_landmark'];?></p>
								<p><?php echo $current_cart_details[0]['agent_seller_shopcity'];?></p>
							</td>
							<td>
								<p><b>Shipping Details</b></p>
								<p><?php echo $current_cart_details[0]['shipping_contact_person'];?></p>
								<p><?php echo $current_cart_details[0]['shipping_address'];?></p>
								<p><?php echo $current_cart_details[0]['shipping_landmark'];?></p>
								<p><?php echo $current_cart_details[0]['shipping_state'];?></p>
								<p><?php echo $current_cart_details[0]['shipping_city'];?></p>
								<p><?php echo $current_cart_details[0]['shipping_postcode'];?></p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
           </tbody>
</table>