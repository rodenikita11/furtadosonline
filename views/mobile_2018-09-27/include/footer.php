<!-- find-stores Container -->
<script src="<?php echo SITEJS;?>newwishlist.js" type="text/javascript"></script>
<script src="<?php echo WEBJS;?>bootbox.min.js"></script>  
<div class="find-stores">
   <div class="row">
      <div class="col-xs-12">
         <a href="<?echo SITEMOBURL;?>user/storelisting"><img src="<?php echo SITEIMAGES.'mobimg/find_store.jpg';?>" width="100%"></a>
      </div>
   </div>
</div>
<!-- logo Container -->
<div class="Container container-fluid bg-1 text-center">
   <div class="row">
      <div class="col-xs-12">
         <div class="col-xs-6">
            <a href="http://www.highfurtados.com/" target="_blank">
            <img src="<?php echo SITEIMAGES.'mobimg/icon/high.jpg';?>" width="100%">
            </a>
         </div>
         <div class="col-xs-6">
            <a href="http://www.furtadosschoolofmusic.com/" target="_blank">
            <img src="<?php echo SITEIMAGES.'mobimg/icon/fsm.jpg';?>" width="100%">
            </a>
         </div>
      </div>
   </div>
</div>
<!-- blog img -->
<div class="Container container-fluid blog">
   <div class="row">
      <div class="col-xs-12 ">
         <a href="https://www.furtadosonline.com/blog/" target="_blank">
         <img src="<?php echo SITEIMAGES.'mobimg/icon/blog.jpg';?>" width="100%">
         </a>
      </div>
   </div>
</div>
<!-- music img -->
<div class="container container-fluid">
   <div class="col-md-12 content-grid-1 bg-one">
      <div class="content-grid-text">
         <h3><a href="tel:02242875050" style="color:#fff !important;">022 - 42875050 / 42875060</a></h3>
         <p> <a href="mailto:response@furtadosonline.com " target="_top" style="color:#fff !important;">response@furtadosonline.com </a> </p>
         <div class=" ser-grid">    
            <a href="#" class="hi-icon hi-icon-archive glyphicon glyphicon-headphones"> </a>
         </div>
      </div>
   </div>
   <div class="clearfix"> </div>
</div>
<!------------text---------------->
<div class="container form container-fluid">
   <div class="row">
      <div class="col-xs-12 ">
         <h3>Get exciting Furtados updates on email</h3>
         <br>
         <div class="header-search">
            <form action="#" id="newsletterform" method="post">
               <input type="search" name="email" placeholder="Enter Email ID" required="" id="email">
               <button type="submit" class="btn btn-default" aria-label="Left Align" style="background: #cd0029 ! important; ">
                  <p class="go"> Go</p>
               </button>
            </form>
         </div>
      </div>
   </div>
</div>
<!-------end------------->
<!-- Follow us on Container -->
<div class="container-fluid  text-center">
   <!--removed by sandesh bg-2-->
   <div class="row">
      <div class="col-xs-12 follow-us">
         <h3 >Follow us on</h3>
      </div>
   </div>
</div>
<!-- Follow us on icon -->
<div class="container container-fluid">
   <div class="row">
      <div class="col-xs-12" style="padding-left:0px;margin-left: 2px;">
         <div class="col-xs-2 social"><a href="https://www.facebook.com/FurtadosMusic" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/fb.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2 social"><a href="https://www.youtube.com/user/furtadosmusicindia" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/vi.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2 social"><a href="https://twitter.com/furtados" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/tw.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2  social"><a href="https://www.instagram.com/furtadosmusic" target="_blank"><img src="<?php echo SITEIMAGES.'mobimg/icon/ins.jpg';?>" width="100%"></a></div>
         <div class="col-xs-2  social"><a href="https://plus.google.com/u/0/+furtadosmusicindia" target="_blank"><img src="https://www.furtadosonline.com/assets/images/mobimg/icon/google-plus.png" width="100%"></a></div>
         <div class="col-xs-2  social"></div>
      </div>
   </div>
</div>
<!-- about us  Container -->
<div class="Container container-fluid about">
   <div class="row">
      <div class="col-xs-12 ">
         <div class="col-xs-5 about-bottom">
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/aboutus">About us</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/job">Careers</a></p>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/feedback">Feedback</a></p>
         </div>
         <div class="col-xs-1"></div>
         <div class="col-xs-6 furtados">
            <p><a class="footabout" href="http://furtadosartists.com/">Furtados Artists</a></p>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/termsandcondition">Terms and Conditions</a></p>
         </div>
      </div>
   </div>
</div>
<!-- Footer -->
<footer class="bg-4 text-center footer-copy">
   <p>Copyright @ 2018 Furtadosonline.com</p>
    <?require_once (__DIR__ .'/emi.php');?>
</footer>
</body>
</html>
<style type="text/css">label#email-error {
    color: #cd0029;
}</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16232700-1"></script>
<script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());
   
   gtag('config', 'UA-16232700-1');
</script> 
<script type="text/javascript">
   jQuery('#newsletterform').validate({
       rules:{
           email:{
               email:true,
               required:true
           }
       },
       messages:{
           email:{
               email:'Please Enter valid EmailID',
               required:'Please Enter EmailID'
           }
       },
       submitHandler:function(form){
           var valid=jQuery('#newsletterform').valid();
           
           var email=jQuery('#email').val();
           if(valid){
               jQuery.ajax({
                   url:'<?php echo SITEMOBURL?>user/newslettersubmit',
                   type:'POST',
                   dataType:'JSON',
                   data:{email:email},
                   success:function(result){
                     res=eval(result);
                        if(res.status== true)
                        {
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val('');
                              window.location.reload();  
                            });
                            
                        }else{
                            bootbox.alert(res.msg,function(){
                              jQuery('#email').val();  
                            });
                            
                        }
                   }
               });
           }
       }
   });
</script>
<script>
   jQuery(document).scroll(function(){
    curscrollpos=jQuery(document).scrollTop();
    if(curscrollpos < 10){
        jQuery('#stickyheader').hide();
        jQuery('#mainheader').show();
        return false;
    }else{
        jQuery('#mainheader').hide();
        jQuery('#stickyheader').show();
    }
   })
   function addtowishlist(ele, wishlistobj=false ){
  jQuery('.catallpro').unbind('click');
  pid=ele;
  addtowishlist=jQuery(wishlistobj).data('addtowishlist');
  jQuery.ajax({
    url:'<?php echo SITEMOBURL?>user/wishlist',
    type:'POST',
    data:{pid:pid},
    success:function(data)
    {   var response = jQuery.parseJSON(data);

      if(response.error=='false'){
           bootbox.alert('Product successfully added to shortlist', function(){
            window.location.reload();
           });
           if(addtowishlist){
           removefromcart(pid, addtowishlist);
         }
          // window.location.reload();
            ajax_getCartCount();
           ajax_wishlistcount();
        }else if(response.login=='false'){
           bootbox.alert('You need to login to create Shortlist',  function(){
              window.location.href="<?php echo SITEMOBURL?>user";
           });
        }else{
         bootbox.alert('Product Already In Shortlist');

        }

    }

  });
}
</script>
<style type="text/css">
  .modal-dialog { top: 24%;}.modal-footer {background: #f5f5f5;padding: 5px;}
  div#myModaloption .modal-dialog {top: 0;}
</style>