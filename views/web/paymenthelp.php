<style type="text/css">
   p.debit_txt_red {font-size: 14px;padding-left: 18px;color: #cc0028;clear: both;padding-top: 20px;}
   p.debit_txt_body,.enquiry_txt_body{padding-left: 18px;}p.debit_head {font-weight: 700;padding-left: 18px; font-size: 14px; color: #cc0028;}
</style>
<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150 inner-bottom-20">
            <div class="container">
               <div class="wide-banners wow fadeInUp">
                  <div class="row">
                     
                       
                          <div>
                              <div class="col-md-12">
                                  <h1 style="margin-left: 17px;color:#cc0028;font-size: 24px; text-align:center;">Payment Policy</h1>
                              </div>
                              <div class="col-md-3">
                                 <img src="https://www.furtadosonline.com/assets/images/mastercard.png">
                              </div>
                              <div class="col-md-9">
                                <h5>Shopping on Furtadosonline is 100% safe. The highest standard of online security has been implemented on Furtadosonline to keep your login details safe and secure. 
                                </h5>
                                <h5>All payments are processed through a secured encrypted payment gateway. No payment information, including card numbers, expiry dates, etc, is stored on Furtadosonline servers. All transactions are routed through Government authorised payment gateways.</h5>
                                
                              </div>
                               
                         </div>

                         <div style="clear: both;">
                         <div style="margin-bottom:10px">
                          <h1 style="margin-left: 17px;color:#cc0028;font-size: 24px;">Credit / Debit Card</h1>
                          <p class="enquiry_txt_body">Please provide your payment information as required. Kindly note, you can choose the EMI option. EMI is available on multiple tenures at a number of empaneled banks. Check the bank list to make EMI payments online.</p>
                           <p class="debit_head">Net Banking</p>
                            <!--<p class="enquiry_txt_body">Net Banking Payment transactions are carried out via a secured gateway for payment (Techprocess).</p>-->
                            <p class="enquiry_txt_body">Net Banking Payment transactions are processed using a secure payment gateway.</p>
                        </div>

                        <div style="margin-bottom:10px">
                           <p class="debit_head">Cheque / Demand Draft</p>
                            <p class="enquiry_txt_body">Cheque / DD for the complete value (including shipping cost) should be drawn in favour of 
                            <b>"Furtados Music India Pvt Ltd."</b></p>
                            <p class="debit_txt_body">Cheque / DD should be payable at par in Mumbai.</p>
                            <p class="debit_txt_body">Outstation cheques will not be accepted.</p>
                            <p class="debit_txt_body">Cheque / DD should be posted / couriered to our office address as follows.</p><br>
                            <p class="debit_head"><b>Furtados Music India Pvt Ltd,</b> </p>
                            <!--<p class="debit_txt_body">201, Town Centre II,</p> 
                            <p class="debit_txt_body">Andheri – Kurla Road,</p> 
                            <p class="debit_txt_body">Marol, Andheri (E)</p>
                            <p class="debit_txt_body">Mumbai – 400 059 </p>-->
							<p class="debit_txt_body">1st floor, Unit No.4, Dwarka Ashish,</p> 
                            <p class="debit_txt_body">6/8, Jambulwadi,</p> 
                            <p class="debit_txt_body">Kalbadevi Road</p>
                            <p class="debit_txt_body">Mumbai - 400 002 </p>
                            <br>
                            <p class="debit_txt_body">Contact Nos.:</p>
                            <p class="debit_txt_body">Tel: <a href="tel:02242875050">+91 22 42875050/ 42875060</a></p>
                        </div>
                        <div>
                           <p class="debit_head">NEFT / RTGS</p>
                            <p class="enquiry_txt_body">The NEFT/ RTGS should be in favor of <b>“Furtados Music India Pvt Ltd.”</b></p>
                            <p class="debit_txt_body">Beneficiary A/C No: <b>05922560004715</b></p>
                            <p class="debit_txt_body">Beneficiary Bank: <b>HDFC Bank</b></p>
                            <p class="debit_txt_body">Beneficiary IFSC Code : <b>HDFC0000143</b></p>
                            <p class="debit_txt_body">Beneficiary Address: <br> <b>1st floor, Unit No.4, Dwarka Ashish,<br> 6/8, Jambulwadi,<br> Kalbadevi Road,<br>
                            Mumbai - 400 002</b></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <!-- ============================================== CONTENT ============================================== -->

      </div>
   </div>
</div>