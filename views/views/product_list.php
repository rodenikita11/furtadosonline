
<?php include('includes/head.php');  ?>
<div class="content_wrapper">
				<!-- feature product hot deals -->
				<section class="inner_page_sec">
					<div class="container">
						<div class="row">
							<!-- bread crum -->
							<div class="col-lg-12 col-md-12 col-sm-12 bredcrum">
								<ul>
									<li><a href="">Home / </a></li>
									<li><a href=""> Guitars & Bass /</a></li>
									<li><a href=""> Electric Guitars </a></li>
								</ul>
								<div class="clear"></div>
							</div>
							<!-- end bread crum -->
							<!-- left side bar-->
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<!-- filter -->
								<div class="pr_filter">
									<h3>Refine Your Results</h3>
									<div class="select_cate">
										<p>Category :<span> Electric Guitars</span></p>
										<a href="#">Clear all filters</a>
									</div>
									<div class="brand">
										<h4>Brand</h4>
										<ul>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG4" class="css-checkbox">
												<label for="checkboxG4" class="css-label">Alhambra<span> (15)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG5" class="css-checkbox">
												<label for="checkboxG5" class="css-label">Alice<span> (4)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG6" class="css-checkbox">
												<label for="checkboxG6" class="css-label">AMT<span> (8)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG7" class="css-checkbox">
												<label for="checkboxG7" class="css-label">Applause<span> (8)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG8" id="checkboxG8" class="css-checkbox">
												<label for="checkboxG8" class="css-label">Ashton<span> (4)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG9" class="css-checkbox">
												<label for="checkboxG9" class="css-label">B C Rich<span> (2)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG10" class="css-checkbox">
												<label for="checkboxG10" class="css-label">Belcat<span> (1)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG11" class="css-checkbox">
												<label for="checkboxG11" class="css-label">Bespeco<span> (10)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG12" class="css-checkbox">
												<label for="checkboxG12" class="css-label">Blackstar<span> (9)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG13" class="css-checkbox">
												<label for="checkboxG13" class="css-label">Blade<span> (2)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG6" class="css-checkbox">
												<label for="checkboxG6" class="css-label">AMT<span> (8)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG7" class="css-checkbox">
												<label for="checkboxG7" class="css-label">Applause<span> (8)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG8" id="checkboxG8" class="css-checkbox">
												<label for="checkboxG8" class="css-label">Ashton<span> (4)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG9" class="css-checkbox">
												<label for="checkboxG9" class="css-label">B C Rich<span> (2)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG10" class="css-checkbox">
												<label for="checkboxG10" class="css-label">Belcat<span> (1)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG11" class="css-checkbox">
												<label for="checkboxG11" class="css-label">Bespeco<span> (10)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG12" class="css-checkbox">
												<label for="checkboxG12" class="css-label">Blackstar<span> (9)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG13" class="css-checkbox">
												<label for="checkboxG13" class="css-label">Blade<span> (2)</span></label>
											</li>
										</ul>
										<a href="#" class="more_check">MORE</a>
									</div>
									<div class="brand price_filter">
										<h4>Price</h4>
										<ul>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG14" class="css-checkbox">
												<label for="checkboxG14" class="css-label">` 7,490 to ` 20,550<span> (5)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG15" class="css-checkbox">
												<label for="checkboxG15" class="css-label">` 20,551 to ` 31,530<span> (10)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG16" class="css-checkbox">
												<label for="checkboxG16" class="css-label">` 31,531 to ` 46,900<span> (20)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG17" class="css-checkbox">
												<label for="checkboxG17" class="css-label">` 46,901 to ` 76,380<span> (10)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG8" id="checkboxG18" class="css-checkbox">
												<label for="checkboxG18" class="css-label">` 76,381 to ` 2,99,580<span> (6)</span></label>
											</li>
										</ul>
									</div>
									<div class="brand other_filter">
										<h4>Other</h4>
										<ul>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG19" class="css-checkbox">
												<label for="checkboxG19" class="css-label">Discounted <span> (45)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG20" class="css-checkbox">
												<label for="checkboxG20" class="css-label">Stock Available<span> (820)</span></label>
											</li>
											<li>
												<input type="checkbox" name="checkboxG4" id="checkboxG21" class="css-checkbox">
												<label for="checkboxG21" class="css-label">EMI Available<span> (480)</span></label>
											</li>
										</ul>
									</div>
								</div>
								<!-- end filter -->
								<!-- bundle -->
								<div class="bundle_banner">
									<a href="#"><img src="<?php echo ASSETURL; ?>images/img_36.jpg" alt=""></a>
								</div>
								<!-- end bundle -->
							</aside>
							<!-- end left side bar-->
							<!-- right side bar-->
							<article class="col-lg-9 col-md-9 col-sm-9">
								<!-- product lis -->
								<section class="product_list_sec">
									<div class="page_heading">
										<h2>Electric Guitars</h2>
									</div>
									<div class="price_select">
										<div class="col-lg-3 col-xs-3 showing_n0">
											<span>Showing <strong>364</strong></span>
										</div>
										<div class="col-lg-9 col-xs-9 select_price">
											<ul>
												<li class="active_pr"><i></i><a href="#">Popular </a></li>
												<li><a href="#">Low Price</a></li>
												<li><a href="#">High Price</a></li>
												<li><a href="#">New Arrivals</a></li>
											</ul>
										</div>
										<div class="clear"></div>
									</div>
									<div class="pr_list_in">
										<div class="pr_row">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="onsale">ONSale</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
														<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="free">free</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
														<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li class="last_child">
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
														<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
											</ul>
											<div class="clear"></div>
										</div>
										<div class="pr_row">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="onsale">ONSale</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
														<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="free">free</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
														<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li class="last_child">
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
														<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
											</ul>
											<div class="clear"></div>
										</div>
										<div class="pr_row">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="onsale">ONSale</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
														<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="free">free</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
														<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li class="last_child">
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
														<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
											</ul>
											<div class="clear"></div>
										</div>
										<div class="pr_row">
											<ul class="pr_list">
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img">
															<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
														</div>
														<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><strong>22</strong> Reviews</span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="onsale">ONSale</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
														<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li>
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
														<span class="free">free</span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
														<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
												<li class="last_child">
													<div class="offer_det">
														<span class="love"><a href="#"></a></span>
													</div>
													<a href="#">
														<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
														<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
														<div class="pr_price">
															<span class="or_price">19,350</span>
															<span class="discount_price">68,480</span>
														</div>
													</a>
													<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
												</li>
											</ul>
											<div class="clear"></div>
										</div>
									</div>
								</section>
								<!-- end product lis -->
							</article>
							<!--end  right side bar-->
						</div>
						<div class="clear"></div>
					</div>
				</section>
				<!-- end feature product hot deals -->

			</div>
<?php include('includes/footer.php'); ?>
