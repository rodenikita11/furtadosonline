<?php
			/*
				FILE NAME : ImageBo
				AUTHOR : SANJAY
				CREATED : 9/Sep
				DESC: Can be used throughout application for validating , uploading images.
				Main Function : validate_image(); uploadFile();
			*/
	class ImageBo
	{
			protected $img_type_allow=array('image/jpeg','image/gif','image/jpg','image/pjpeg','image/x-png','image/png');
			
			/*
					NAME : validate_image
					RETURN : True on Valid image, False on invalid image.
			*/
			public function validate_image($imgResource)
			{
				#Validate for empty image.
					if(!$this->isResourceEmpty($imgResource))
					{
						#--Validate for any error while sending file to server.
						if(!$this->isErrorInFile($imgResource))
						{
							if($this->isValidImageType($imgResource))
							{
								#-- Check for image size not to be greater than max_file_upload
								if($this->isValidSize($imgResource))
								{
									return true; #No error..
								}
							}
						}
					}
				return false; #-- On any error
			}
			
			/*
					NAME : uploadFile
					DESC : Used for uploading Image.
			*/
			public function uploadFile($resource,$upload_to_path)
			{
				#Check and create destination folder if not exist 
				if (!file_exists($upload_to_path) && !is_dir($upload_to_path)) {
							mkdir($upload_to_path,0777,true);         
					} 
				#-- Upload file start..
				$file_name = $resource['name'];
				$file_url = $upload_to_path.$file_name;
					if(!$this->isFileExist($file_url))
					{
						$uploadStatus = move_uploaded_file($resource["tmp_name"],$file_url);
					}
					else
					{
							#-- rename file.
							$file_name = time().$file_name;
							$file_url = $upload_to_path.$file_name;
							$uploadStatus = move_uploaded_file($resource["tmp_name"],$file_url);
					}
					
						if($uploadStatus)
						{
							$returnResponse['status']=1;
							$returnResponse['uploadStatus']=$uploadStatus;
							$returnResponse['file_name']=$file_name;
							$returnResponse['file_url']=$file_url;
						}
						else
						{
							$returnResponse['status']=0;
							$returnResponse['uploadStatus']=$uploadStatus;
							$returnResponse['file_name']=$file_name;
							$returnResponse['file_url']=$file_url;
						}
				return $returnResponse;
			}
			
			
			public function isFileExist($file_url)
			{
				if (file_exists($file_url))
						return true;
					return false;
			}
			/*
					NAME : isResourceEmpty
			*/
			public function isResourceEmpty($resorce)
			{
				if(empty($resorce)) return true;
				if(empty($resorce['name'])) return true;
				return false;
			}
			
			/*
					NAME : isErrorInFile
			*/
			public function isErrorInFile($resorce)
			{
				if(empty($resorce)) return true;
				if($resorce['error']>0) return true;
				return false;
			}
			
			/*
					NAME : isValidImageType
			*/
			public function isValidImageType($resource)
			{
				if(empty($resource)) return false;
				if(in_array($resource['type'],$this->img_type_allow))
					return true;
				return false;
			}
			
			/*
				NAME : isValidSize
			*/
			public function isValidSize($resource)
			{
				return true; #-- temp val.
				$max_upload = 1024 * (int)(ini_get('upload_max_filesize'));
				if($resource['size']>$max_upload)
					return false;
				return true;
			}
	}
?>