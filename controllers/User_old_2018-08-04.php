<?if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MY_Controller{
    function __construct()
    {
         parent::__construct();
         $this->load->library('Mailclass');
         $this->load->library('lib_loginfb'); 
         $this->load->library('user_agent');
         $this->load->library('Falconide');
        	
    }
    
    
    public function index(){
        //$this->mailclass->configuremail($to,$subject,$message);
        //$this->mailclass->sendmail();
        //$this->load->view('mobile/include/header');
        $this->load->view('mobile/login');
    }
    
    public function userlogin(){
      
        if(!empty($_POST)){
              
              $param['email']=$_POST['email'];
              $param['password']=$_POST['password'];
             $result = $this->liblogin->getuserdetail($param);
              $data['menus']=$this->libmenu->get_dropdownmenu();
                if($result)
                {
                    $data=array('status'=>true,'msg'=>'Logged Successfully','error'=>false);
                }else{
                    $data=array('status'=>false,'msg'=>'Invalid Username & Password','error'=>true);
                }
				
				
            echo json_encode($data); exit;


      }
    }
    /*viki 3/1/2018*/
    public function pianoenquiry(){
        $mobile =$this->agent->is_mobile();
        $desktop=$this->agent->is_browser();
	$data['res']['meta_title']='Buy Pianos Online | Buy Acoustic Pianos & Digital Pianos Online. India  | Furtados Online';
	$data['res']['meta_keyword']="Buy Piano Online, Buy Acoustic Pianos & Digital Pianos Online";
	$data['res']['meta_description']="Pianos are available in different types and sizes. Get the best deals on Acoustic Pianos & Digital Pianos right here on Furtados Online, India’s leading Piano retailer.";
        if (!empty($mobile))
          {
            $this->load->view('mobile/include/header',$data);
           $this->load->view('web/pionoenquiry',$data);
          }else{
            $this->load->view('web/include/header',$data); 
            $this->load->view('web/pionoenquiry'); 
            $this->load->view('web/include/footer',$data); 
          }
       
    }
    /*viki end 3/1/2018*/
    public function usersignup(){
       // print_r($_POST);die;
        if(!empty($_POST)){
             $param['name'] =$_POST['name'];
              $param['email']=$_POST['email'];      
              $param['password']=$_POST['password'];
              $param['state']=$_POST['state'];
              $param['city']=$_POST['city'];
              $param['phoneno']=$_POST['phoneno'];
              $param['address1']=$_POST['address1'];
              $data['resp'] = $this->liblogin->usersignup($param);
              if($data['resp']==1){
				          $res = $this->libmailer->registermail($param);
                  $data['error']='false';
              }else if($data['resp']=='3'){
                  $data['error']='true';
                  $data['message']='User already exists';
              }else if($data['resp']=="Invalid Email Format"){
                  $data['error']='true';
                  $data['message']=$data['resp'];
              }else{
                   $data['error']='true'; 
              }
              echo json_encode($data);
            
            }
        
        
        
        }
  
   
   

  public function myaccount(){
  	if(isset($_POST)){
  		$param['name']=$_POST['name'];
  		$param['address']=$_POST['address'];
  		$param['state']=$_POST['state'];
  	}
        if(!$this->libsession->isSetSession('usrid')) {
            
          $data['login']='false';
           echo json_encode($data);
           exit;         
         }else{
            $param['usrid']=$_SESSION['usrid'];
            $data['username']=$_SESSION['usrname'];
            $data['menus']=$this->libmenu->get_dropdownmenu();
            $data['custdtl']=$this->liblogin->getuserdata();
            //print_r($data['custdtl']);die;
            $data['shippingdtl']=$this->liblogin->getuseraddrdtl($param);
            //print_r($data['shippingdtl']); die;
            $data['states']=$this->liblogin->getStates();
                $mobile =$this->agent->is_mobile();
                $desktop=$this->agent->is_browser();
                if (!empty($mobile))
                  {
                    $data['usrid']=$_SESSION['usrid'];
                    $this->load->view('mobile/include/head'); 
                    $this->load->view('mobile/myaccount',$data);
                  }
                  else
                  {
                   //echo "hi"; die;             
                    $this->load->view('web/include/header',$data); 
                    $this->load->view('web/account',$data); 
                    $this->load->view('web/include/footer',$data); 
                  }
         }
    }
        
        public function logout(){
	 	$updsesshist=$this->libsession->sessionhistory($_SESSION);	
		session_destroy();  
	  	echo '<script type="text/javascript"> window.location = "'.SITEMOBURL.'"</script>';
        }

        public function logoutajax(){
        	session_destroy();
        	echo json_encode(array('error'=>false));
        }

        //for user acc dtl in customer table


        public function getcities()
        {

            if(!$this->libsession->isSetSession('usrid')) {
                
              $data['login']='false';
               echo json_encode($data); exit();             
                            
             }else{
                    if(!empty($_POST))
                    {
                            
                        $param['sid']=$_POST['sid'];
                        
                        $param['usrid']=$_SESSION['usrid'];
                        $resp = $this->liblogin->getcities($param);  
                        
                        if($resp){
                              $data= array('status' =>true ,'msg'=> $resp,'error'=>false);
                             
                          }else{
                             $data= array('status' =>false ,'msg'=>'No data found','error'=>true);
                          }
                          
                          
                          echo json_encode($data);
                    }else{
                        echo "there is some problem with parameter"; die;
                    }   
                 
             }
        }
         //created by viki on 26/3/2018
        public function getsignupcities()
        {

                    if(!empty($_POST))
                    {
                            
                        $param['sid']=$_POST['sid'];
                        
                        $param['usrid']=$_SESSION['usrid'];
                        $resp = $this->liblogin->getcities($param);  
                        
                        if($resp){
                              $data= array('status' =>true ,'msg'=> $resp,'error'=>false);
                             
                          }else{
                             $data= array('status' =>false ,'msg'=>'No data found','error'=>true);
                          }
                          
                          
                          echo json_encode($data);
                    }else{
                        echo "there is some problem with parameter"; die;
                    }   
           
        }
        //for user account 
        public function updateuseraccdtl()
        {	
			#print 'hi';exit;
            if(!$this->libsession->isSetSession('usrid')) {
                
              $data['login']='false';
               echo json_encode($data); exit();             
                            
             }else{
                    if(!empty($_POST))
                    {
                        //$param['id']=intval($_POST['id']);
                        if(!empty($_POST['formdata'])){
                        parse_str($_POST['formdata'],$param);
                       // echo '<pre>';print_R($param);  die;
                        $param['fname']=explode(' ',$param['name'])[0];
                        $param['lname']=explode(' ',$param['name'])[1];
                        //$param['gender']
                        
                        if ($param['title'] == 'Mr.'){
                          $param['gender'] = 'ma';
                        } else if($param['title'] == 'Mrs.') {
                          $param['gender'] = 'fe';  
                        }
                        $param['usrid']=$_SESSION['usrid'];
                       // echo '<pre>'; print_r($param); die;
                        $resp = $this->liblogin->updateuseraccdtl($param); 
                        }else{
                          //
                          //print_r($_POST);die;
                          $param['address']=$_POST['address'];
                          $param['landmark']=$_POST['landmark'];
                          $param['state2']=$_POST['state2'];
                          $param['city2']=$_POST['city2'];
                          $param['model']=$_POST['model'];
                          $param['pincode2']=$_POST['pincode2'];
                          $param['usrid']=$_SESSION['usrid'];
                          //print_r($param); die;
                          $resp = $this->liblogin->updateuseraccdtl($param); 
                        } 
                        //print_r($resp);die;
                        if($resp){
                              $this->libmailer->account_update($param);
                              $data = array('status' =>true ,'msg'=>'Details Saved','error'=>false );
                             
                          }else{
                             $data = array('status' =>false,'msg'=>'Details Not Saved','error'=>true );
                          }
                          
                          
                          echo json_encode($data);
                    }else{
                        echo "there is some problem with parameter"; die;
                    }   
                 
             }
        }
        //for shipping address
        public function saveuserdtl()
        {
          //print_r($_SESSION); die;
            if(!$this->libsession->isSetSession('usrid')) {
                
              $data['login']='false';
               echo json_encode($data); exit();             
                            
             }else{
                    if(!empty($_POST))
                    {
                        //$param['id']=intval($_POST['id']);

                        parse_str($_POST['formdata'],$param);
                        //print_r($param); die;  
                        $param['usrid']=$_SESSION['usrid'];
                        $data['resp'] = $this->liblogin->updateuserdetail($param); 
                        //print_r($data['resp']); die; 
                        if($data['resp']==1){
                              $data['error']='false';
                             
                          }else{
                               $data['error']='true'; 
                          }
                          
                          
                          echo json_encode($data);
                    }else{
                        echo "there is some problem with parameter"; die;
                    }   
                 
             }
        }
        
        public function wishlist()
       {
          // print_r($_POST);die;
             if(!$this->libsession->isSetSession('usrid')) {
                
             $data['login']='false';
            echo json_encode($data); exit();             
                            
            }else{
                    if(!empty($_POST))
                    {
                      
                        $param['pid']=intval($_POST['pid']);

                        $res = $this->libwishlist->addtowishlist($param);  
                        $allwishlist = $this->libsession->getSession('wishlist');
                        if($res==1){
                        $finalwishlist =  $allwishlist.','.$param['pid'];
                        $setwishlist=array('wishlist'=>$finalwishlist);
                        $this->libsession->setSession($setwishlist);
                          $data['error']='false'; 
                            
                        }else if($res=='duplicate'){
                           $data['error']='duplicate';
                            
                        }else{
                             $data['error']='true';
                            
                        }
                        
                    }
                     echo json_encode($data);exit;  
                 
             }  
        
            
       }
       
       function getwishlistcount(){
           
           $res = $this->libwishlist->getwishlistcount($param); 
           if($res){
                $data['cnt']=$res;
                $data['error']='false';
              }else{
                $data['error']='true'; 
                $data['cnt']=0;   
               }
            echo json_encode($data);exit; 
           
         }
            
            function getwishlist(){

              if($this->libsession->isSetSession('usrid') || $_POST['customerid']) {
                  if(!$_POST){  
                    $param['usrid']=$this->libsession->isSetSession('usrid');
                  }else{
                    $param['usrid']=$_POST['customerid']; 
                  }
                  $data['resp'] = $this->libwishlist->getwishlist($param); 
                  $data['menus']=$this->libmenu->get_dropdownmenu();
                  $mobile =$this->agent->is_mobile();
                  $desktop=$this->agent->is_browser();
                  //echo 'hiii';       
                  if (!empty($mobile))
                  {
                  //die('fhdaskfgsdk');
                    $this->load->view('mobile/include/head'); 
                    $this->load->view('mobile/wishlist',$data); 
                  } else {    
                    $this->load->view('web/mywishlist',$data);
                  }

              }else{
                echo "the user is not logged in"; die;

              }
            }  
         
         
             public function removefromwishlist(){
        
                if(!empty($_POST))
                    {
                        $param['pid'] = intval($_POST['pid']);
                        if($param['pid'])
                        {
                            $res = $this->libwishlist->removefromwishlist($param); 
                            $allwishlist = $this->libsession->getSession('wishlist');
                             if($res){
                                 $allwishlist=str_replace($param['pid'],'',$allwishlist) ;
                                 $setwishlist=array('wishlist'=>$allwishlist);
                                 $this->libsession->setSession($setwishlist);
                               $data['error']='false';   
                             }else{
                                 
                                 $data['error']='true'; 
                              }
                            
                        }
                        
                    
                echo json_encode($data); exit;
              }
         
        }

    public function saveforgotpassword(){
        $param['emailid']=$_POST['emailid'];
	$res=$this->liblogin->saveforgotpassword($param);
        
        if($res){
          //print_R($res);die;
            //$from = array(GENERIC_EMAIL =>DOMAIN_NAME);
            $to = $res['emailid'];
     	    $subject = "Password details of ".DOMAIN_NAME;      
            $body .= MAILER_HEADER;
      	    $body .= "<br>Dear ".strtoupper($res['name'])."," ;
      	    $body .= "<p> Greetings from ".DOMAIN_NAME."</p>";
      	    $body .= "<p>This mail has been sent to you as you have requested for a new password for your account.</p>";
      	    $body .= "Your temporary password is : <b>".$res['enccode']."</b>";
      	    $body .= "<p>Please <a href='".SITEMOBURL."user?emailid=".$res['emailid']."' target='_blank' style='color:#464f4b;'>login</a> and change your password.</p>";
      	    $body .= "<p style='color:#464F4B;'>If you have not requested to change your password and are receiving this mail in error we request you to inform us immediately by e-mailing us at   <a href='mailto:".RESPONSE_MAIL."' style='color:#464F4B;'>".RESPONSE_MAIL."</a></p>";
      	    $body .= MAILER_FOOTER;
          

          $Falconide=new Falconide();
          $result=$Falconide->sendmail($to,$subject,$body);
      //       $this->mailclass->configuremail($to,$subject,$message);
      // $mail=$this->mailclass->sendmail();

      #$this->mailclass->configuremail('allmails@furtadosonline.com',$subject,$message);
      #$mail2=$this->mailclass->sendmail();
             // print_r($result);die('hiiii123');         
          if($result>0){
              $data = array('status' =>true ,'msg'=>'email successfully sent','error'=>false);
          }
          else{
              $data = array('status' =>false ,'msg'=>'email not sent','error'=>true);
          }
        }
        else{
            $data = array('status' =>false ,'msg'=>'fail','error'=>true);
        }
        echo json_encode($data);
        
        
    }
    /*change password by viki 14/3/2018*/
    public function changepassword(){
      if($this->libsession->isSetSession('usrid')){
        if($_POST){
          $param['id']=$_SESSION['usrid'];
          $param['oldpassword']=$_POST['oldpassword'];
          $param['newpassword']=$_POST['newpassword'];
          $result=$this->liblogin->changepassword($param);
          if($result){
              //$updsesshist=$this->libsession->sessionhistory($_SESSION);  
              //session_destroy();  
              	$data = array('status' =>true ,'msg'=>'Password change successfully','error'=>false);

             	$this->libsession->deleteAllSession();
          }
          else{
              $data = array('status' =>false ,'msg'=>'Old password is wrong','error'=>true);
          }
        }
      }else{
        $data=array('msg'=>'User is not login. ');
      }
      echo json_encode($data);
      
    }

    public function setflasdata(){
    	$this->session->set_flashdata('changepassword','true');
    	echo json_encode(array('error'=>false));
    }
    /*End Chnage password */

public function newslettersubmit()
    {
        if(!empty($_POST)){
             // print_r($_POST);die;
              $param['email']=$_POST['email'];
              
              $param['ipaddress'] =$_SERVER['REMOTE_ADDR'];

              $result = $this->liblogin->insertnewsletter($param);
              //print_r($result);die;
                
                if($result[0]['email']){
                        $data=array('status'=>false,'msg'=>'Email Id Already Exists...','error'=>true);   
                }else{
                if($result)
                {
                  $to = $param['email'];
                  $subject = "Thankyou for subscribing us ".DOMAIN_NAME;
                  $body='<!DOCTYPE html><html><head><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script><style>* {box-sizing: border-box;}body {background-color: #red;padding: 20px;font-family: Arial;}.main {max-width: 1000px;margin: auto;}h1 {font-size: 50px;    word-break: break-all;}.row {margin: 8px -16px;}.row,.row > .column {padding: 8px;}.column {float: left;width: 25%;}.row:after {content: "";display: table;clear: both;}.content {background-color: white;padding: 10px;}@media screen and (max-width: 900px) {.column {width: 50%;}}@media screen and (max-width: 600px) {.column {width: 100%;}}</style></head><body><div class="main"><img src="https://tse4.mm.bing.net/th?id=OIP.vNI5CSwS8djnF_ghwUt3wQHaCx&pid=15.1&P=0&w=300&h=300" style="margin-left:23%;"><h2 class="text-center">Furtadosonline.com</h2><hr><h3 style="color:darkred;" class="text-center">Thank you for subscribing the Furtados newsletter</h3></div></body></html>';
                  $Falconide=new Falconide();
                  $res=$Falconide->sendmail($to,$subject,$body);
                  //print_r($res); die;
                  if($res>0){
                    $res1=$this->liblogin->updatenewsletter($result);
                    if($res1){
                    $data=array('status'=>true,'msg'=>'Thank you for registering !','error'=>false);
                    }
                  }
                }else{
                    $data=array('status'=>false,'msg'=>'Not added Successfully','error'=>true);
                }
              }
            echo json_encode($data);
            }
    }
    
    public function aboutus(){
            $data['menus']=$this->libmenu->get_dropdownmenu();
	    $data['res']['meta_title']="Buy Music Instruments Online | Piano, Guitar Stores in Mumbai | Furtados Online";
	    $data['res']['meta_keyword']="Buy Music Instruments Online, Guitar Shops in Mumbai, Music Stores Online India";
	$data['res']['meta_description']="With 20 music showrooms all over India, Furtados Online is one of India’s leading music stores company. Think Music, Think Furtados is apt description for the music house of Furtados. ";
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                $this->load->view('mobile/include/head'); 
                $this->load->view('mobile/about_us');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/about_us',$data);
                  $this->load->view('web/include/footer',$data); 
            }

        
    }
	
	 public function job(){
            $data['menus']=$this->libmenu->get_dropdownmenu();
	    $data['res']['meta_title']="Career Opportunities, Job Openings | Furtados Online";
	    $data['res']['meta_keyword']="Career Opportunities, Job Openings";
	    $data['res']['meta_description']="Learn more about job and career opportunities at Furtados, India’s leading music stores company.  Search our current openings today to find the best fit for you and your career goals.";
            $data['joblist'] = $this->liburl->getjobdetails(); 
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                $this->load->view('mobile/include/head'); 
                $this->load->view('mobile/jobs_listing');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/jobs_listing',$data);
                  $this->load->view('web/include/footer',$data); 
            }

        
    }
	
	
    public function contactus(){
          
          $data['menus']=$this->libmenu->get_dropdownmenu();
	  $data['res']['meta_title']="Music Instruments Shop Near Me  | Piano, Guitar Stores in Mumbai | Furtados Online";
	  $data['res']['meta_keyword']="Guitar Shops in Mumbai, Music Stores Online India";
	  $data['res']['meta_description']="Furtados Online: The numero uno among music stores in Mumbai, India. Looking for a music instruments shop near you? With music stores in Mumbai, Pune, Bangalore, Ahmedabad, Delhi, Goa, we are very close to you.";
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                 $this->load->view('mobile/include/head');
                  $this->load->view('mobile/contact_us');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/contact_us',$data);
                  $this->load->view('web/include/footer',$data); 
            }
        }

    public function DiwaliBlast2017(){
       $this->load->view('mobile/include/head');
       $this->load->view('mobile/DiwaliBlast2017');

    }
    public function checkpincode()
    {
      $param['pincode']=$_POST['pincode'];
         if(!empty($param['pincode'])){
              
              
              $result = $this->liblogin->checkpincode($param);
              //print_r($result); die;
                if($result)
                {
                    $data=array('status'=>true,'msg'=>$result[0],'error'=>false);
                }else{
                    $data=array('status'=>false,'msg'=>'Not available','error'=>true);
                }
              echo json_encode($data);
            } else {
                 $data=array('status'=>'wrong','msg'=>'Please Enter Pin Code');
                 //print_r($data); die;
                 echo json_encode($data);
            }
    }

     public function assuranceprogram(){
              $data['menus']=$this->libmenu->get_dropdownmenu();
	      $data['res']['meta_title']=PAGE_TITLE.'Assurance Program';
	    $data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
	    $data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";
              $mobile =$this->agent->is_mobile();
               $desktop=$this->agent->is_browser();
           //  echo 'hiii';       
          if (!empty($mobile))
            {
              //die('fhdaskfgsdk');
               $this->load->view('mobile/include/head'); 
              //$this->load->view('mobile/include/header');
              $this->load->view('mobile/assuranceprogram');
            }
            else
            {    
              //echo '123654';
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/assuranceprogram',$data);
                  $this->load->view('web/include/footer',$data); 
            }
        }

       public function termsandcondition(){
            $data['menus']=$this->libmenu->get_dropdownmenu();
		$data['res']['meta_title']=PAGE_TITLE.'Terms';
	    $data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
	    $data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
              $this->load->view('mobile/include/head'); 
              $this->load->view('mobile/termsandcondition');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/termsandcondition',$data);
                  $this->load->view('web/include/footer',$data); 
            }
           
        } 

        public function storelisting(){
            $data['menus']=$this->libmenu->get_dropdownmenu();
	    $data['res']['meta_title']="Online Music Stores India | Buy Musical Instruments Online | Music Instrument Shops Mumbai | Furtados Online";
	    $data['res']['meta_keyword']="Music instruments store in Mumbai, musical instruments online shopping, musical instruments store, musical instruments store in India, best musical instrument stores online";
	    $data['res']['meta_description']="Furtados Online one of the most popular and most played musical instruments all over in India, with the low price.";
            $data['map']=$this->liblogin->getmapdtl();
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile)){
               $this->load->view('mobile/include/head');
               $this->load->view('mobile/storelisting',$data);
            }else{
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/storelisting',$data);
                  $this->load->view('web/include/footer',$data); 
            }
        }

        public function trackorder(){
            $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
               // $this->load->view('mobile/include/head');
               // $this->load->view('mobile/storelisting',$data);
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/trackorder',$data);
                  $this->load->view('web/include/footer',$data); 
            }
          
        }

        public function storelocator($id=false){
            $data['menus']=$this->libmenu->get_dropdownmenu();
            $param['id']=$id;
          
            $data['map']=$this->liblogin->getmapdtl($param);
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
               $this->load->view('mobile/include/head');
               $this->load->view('mobile/storelocator',$data);
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/storelocator',$data);
                  $this->load->view('web/include/footer',$data); 
            }

          
        }

        public function map($id=false){
          $data['menus']=$this->libmenu->get_dropdownmenu();
          $param['id']=$id;
         // print_r($param['id']);
          $data['map']=$this->liblogin->getmapdtl($param);
		      $data['events']=$this->liblogin->getevento($param);
			//print_r($data['events']); die;
		  
       // print_r($data['id']);
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
               $this->load->view('mobile/include/head');
               $this->load->view('mobile/map',$data);
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/map',$data);
                  $this->load->view('web/include/footer',$data); 
            }

        }
		 public function paymenthelp(){
	$data['res']['meta_title']=PAGE_TITLE.'Payment Help';
		   $data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
		$data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";	

          $data['menus']=$this->libmenu->get_dropdownmenu();
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                $this->load->view('mobile/include/head');
                $this->load->view('mobile/paymenthelp');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/paymenthelp',$data);
                  $this->load->view('web/include/footer',$data); 
            }
          
        }
		 public function giftvoucher(){
          $data['menus']=$this->libmenu->get_dropdownmenu();
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                  $this->load->view('mobile/include/head');
                 $this->load->view('mobile/giftvoucher_m');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/giftvoucher',$data);
                  $this->load->view('web/include/footer',$data); 
            }
          
        }
		 public function feedback(){
          $data['menus']=$this->libmenu->get_dropdownmenu();
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                 $this->load->view('mobile/include/head');
                  $this->load->view('mobile/feedback');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/feedback',$data);
                  $this->load->view('web/include/footer',$data); 
            }
        }
		public function returnpolicy(){
     		   $data['menus']=$this->libmenu->get_dropdownmenu();
		   $data['res']['meta_title']=PAGE_TITLE.'Return Policy';
		   $data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
		$data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";
        $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                 $this->load->view('mobile/include/head');
                $this->load->view('mobile/returnpolicy');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/returnpolicy',$data);
                  $this->load->view('web/include/footer',$data); 
            }  
        
        }
        public function career(){
        $data['menus']=$this->libmenu->get_dropdownmenu();
        $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                //  $this->load->view('mobile/include/head');
                // $this->load->view('mobile/returnpolicy');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/career',$data);
                  $this->load->view('web/include/footer',$data); 
            }  
        
        }
        public function privacypolicy(){
        $data['menus']=$this->libmenu->get_dropdownmenu();
	$data['res']['meta_title']=PAGE_TITLE.'Privacy Policy';
	$data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
	$data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";
        $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                //  $this->load->view('mobile/include/head');
                // $this->load->view('mobile/returnpolicy');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/privacypolicy',$data);
                  $this->load->view('web/include/footer',$data); 
            }  
        
        }
        public function shippingpolicy(){
        $data['menus']=$this->libmenu->get_dropdownmenu();
	$data['res']['meta_title']=PAGE_TITLE.'Shipping Policy';
	$data['res']['meta_keyword']="Furtados,Music store,Musical Instruments,Indian Instruments,Pianos,Acoustic piano,Grand piano, Digital piano,Guitars,Acoustic guitars,Classical guitars,Electric guitars,Basses,Drums,Digital Drums,Percussion,Cymbals,Crash,Hi-hat,Ride,Keyboard,Synths,Processors,Trumpet,Cello,Viola,Violins,Harmonicas,Accordions,Brass,Flutes,Wind instrument,Bowed strings,Tablas,Strings,Mandolins,Guitars Amps,Effects,Metronomes,Saxophones,Clarinets,Vocal, World music,Workshop,Examination music,Sheet music,Music teachers,Ensemble,Fretted strings,Music notation,Indian classical vocal";
	$data['res']['meta_description']="India's leading retailers of all types of Musical Instruments & Sheet music - Buy Musical Instruments online - Indian instruments - Western instruments";
        $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile))
            {
                //  $this->load->view('mobile/include/head');
                // $this->load->view('mobile/returnpolicy');
            }
            else
            {    
                  $this->load->view('web/include/header',$data); 
                  $this->load->view('web/shippingpolicy',$data);
                  $this->load->view('web/include/footer',$data); 
            }  
        
        }
       
     public function savefeedback(){

       if(!empty($_POST)){
            
      
      $param['username']=$_POST['username'];
      $param['emailid']=$_POST['emailid'];
      $param['comment']=$_POST['comment'];
      $param['ip']=$_SERVER['REMOTE_ADDR'];
    
        $result = $this->liblogin->savefeedback($param);
      
          if($result)
          {
            $full_name=$param['username'];
            $emailid=$param['emailid'];
            $commentText=$param['comment'];
            $from = array($emailid => $full_name);
             //$to = array(GENERIC_EMAIL  => DOMAIN_NAME,$emailid =>$full_name,'feedback@furtadosonline.com' =>$full_name); 
           //$to = array(RESPONSE_MAIL => DOMAIN_NAME);   
           $to = array($param['emailid'] => $param['username'], 'feedback@furtadosonline.com'=>DOMAIN_NAME);
      
            $subject = "General Feedback from ".$full_name;
            $message_string = MAILER_HEADER;
            $message_string.= "<br>Dear, " ;
            $message_string.= "<p> General Feedback from : ".strtoupper($full_name);
            $message_string.= "<p> Name = ".$full_name."</p>";
            $message_string.= "<p> Email Id = ".$emailid."</p>";
            $message_string.= "<p> Feedback = ".str_replace('\n','<br>',$commentText)."</p></td></tr>";
            $message_string .= MAILER_FOOTER;
         
            $emailobj=new Mailclass($param['emailid'], $subject, $message_string);
            $recipients=$emailobj->sendmail();
            if ($recipients = 'success'){
              $data=array('status'=>true,'msg'=>'mail sent','error'=>false);
              }else{
                $data=array('status'=>false,'msg'=>'mail not sent','error'=>true);
              }
          }else{
              $data=array('status'=>false,'msg'=>'fail','error'=>true);
          }
        echo json_encode($data);
          }

      }
	  
	  public function checklogin(){
		  
		  $res=$this->liblogin->checklogin();
		  $data=$res;
		  echo json_encode($data);
		  
	  }
    public function flogin()
    {
         require  FILEBASEPATH.'vendor/facebook/php-sdk-v4/autoload.php';
         $fb_data = $this->lib_loginfb->facebook();
         //print_r($fb_data); die;
  
     if (isset($fb_data['me'])) {
          $userData['fb_oauth_provider'] = 'facebook';
            $userData['fb_oauth_uid']=$fb_data['me']['id'];
            $userData['firstname']=$fb_data['me']['first_name'];           
            $userData['lastname']=$fb_data['me']['last_name'];
            $userData['email'] = $fb_data['me']['email'];
            $userData['gender'] = $fb_data['me']['gender']; 
            
            $res = $this->Do_user->checkUser($userData);
            
            
                $userData['usrid']=$res['userID'];
                $userData['wishlist']=$res['wishlist'];
                $userData['usradd']=$res['usradd'];
                $userData['usrnum']=$res['usrnum'];
                $data['userData'] = $userData;
                $setdata=array('usrname'=>$userData['firstname']. $userData['lastname'],'usremail'=>$userData['email'],'usrid'=>$userData['usrid'],'wishlist'=>$userData['wishlist'],'usrnum'=>0,'usradd'=>$userData['usradd'],'usrnum'=>$userData['usrnum']);
                $this->libsession->setSession( $setdata);
        //print_R($_SESSION); die;
                echo '<script type="text/javascript">
                 window.location = "'.SITEURL.'"
                 </script>';
                

        } else {
             echo '<script type="text/javascript">
                 window.location = "'.$fb_data['loginUrl'].'"
                 </script>';
           
        }
    }

    public function gmaillogin(){
      include_once APPPATH."libraries/google-api-php-client-master/src/Google_Client.php";
      include_once APPPATH."libraries/google-api-php-client-master/src/Google/Service/autoload.php";
      include_once APPPATH."libraries/google-api-php-client-master/src/contrib/Google_Oauth2Service.php";

      // $google_client_id     = '603915754718-ajm4e8htj9lhaovcellfl8bt7eca49f1.apps.googleusercontent.com';
      $google_client_id=GOOGLECLIENTID;
      //$google_client_secret   = 'Q-leGS_OF0WygPQM7BKH3Jux';
      $google_client_secret=GOOGLECLIENTSECRET;
       $google_redirect_url  = SITEURL.'user/gmaillogin'; 
     //echo  $google_redirect_url; die;
     // $google_developer_key   = 'AIzaSyDQVnz1OtkG8U0AvqUa9QBmooQ0pjaJFXw';
       $google_developer_key=GOOGLEAPPKEY; 
              
        //start session
        session_start();

        $gClient = new Google_Client();
        //print $gClient;
        
        $gClient->setApplicationName('Login to discussdesk.com');
        $gClient->setClientId($google_client_id);
        $gClient->setClientSecret($google_client_secret);
        $gClient->setRedirectUri($google_redirect_url);
        $gClient->setDeveloperKey($google_developer_key);
       
        $google_oauthV2 = new Google_Oauth2Service($gClient);
 
        //If user wish to log out, we just unset Session variable
        if (isset($_REQUEST['reset'])) 
        {
          unset($_SESSION['token']);
          $gClient->revokeToken();
          //header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
           echo '<script type="text/javascript">
                 window.location = "'.filter_var($google_redirect_url, FILTER_SANITIZE_URL).'"
                 </script>';
        }


        if (isset($_GET['code'])) 
        { 
          $gClient->authenticate($_GET['code']);
          $_SESSION['token'] = $gClient->getAccessToken();
          //header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
          echo '<script type="text/javascript">
                 window.location = "'.filter_var($google_redirect_url, FILTER_SANITIZE_URL).'"
                 </script>';
          return;
        }


        if (isset($_SESSION['token'])) 
        { 	
            $gClient->setAccessToken($_SESSION['token']);
        }


        if ($gClient->getAccessToken()) 
        {
            $user=$google_oauthV2->userinfo->get();

            $userData['fb_oauth_provider']='google';
            $userData['fb_oauth_uid']= $user['id'];
            $userData['firstname']= $user['given_name'];
            $userData['lastname']= $user['family_name'];
            $userData['gender']= $user['gender'];
            
            $userData['email']= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
            
            $_SESSION['token']  = $gClient->getAccessToken();
        }
        else 
        {
          //get google login url
          $authUrl = $gClient->createAuthUrl();
          //print $authUrl; die;
        }
        
        if(isset($authUrl)) //user is not logged in, show login button
        {
          
          echo '<script type="text/javascript">
                 window.location = "'.$authUrl.'"
                 </script>';
        }else // user logged in 
        {

          $res = $this->Do_user->checkgmailUser($userData);
          $userData['usrid']=$res['userID'];
          $userData['wishlist']=$res['wishlist'];
          $userData['usradd']=$res['usradd'];
          $userData['usrnum']=$res['usrnum'];
          $data['userData'] = $userData;
          $setdata=array('usrname'=>$userData['firstname']. $userData['lastname'],'usremail'=>$userData['email'],'usrid'=>$userData['usrid'],'wishlist'=>$userData['wishlist'],'usrnum'=>0,'usradd'=>$userData['usradd'],'usrnum'=>$userData['usrnum']);
                
          $this->libsession->setSession($setdata);
          
          echo '<script type="text/javascript">
          window.location="'.base_url().'";
          </script>';
         }  
    }

    public function staticpage(){
		
		$param['id']=$_GET['id'];
		
		$data['resp']=$this->liburl->getstaticdata($param);
		
	   $this->load->view('mobile/include/head');
       $this->load->view('mobile/staticpage',$data);
		
		
	}	

  public function updateaddress(){
    $data=array();
    if($this->input->post()){
        $fullname=explode(' ', $this->input->post('name'));
        $param['firstname']=$fullname[0];
        if($fullname[1]){
            $param['lastname']=$fullname[1];
        }
        $param['address']=$this->input->post('address_1');
        $param['landmark']=$this->input->post('landmark');
        $param['postcode']=$this->input->post('postcode');
        $param['addressid']=$this->input->post('addressid');
        $res=$this->Do_user->updateaddress($param);
        if($res){
          $data['error']=false;
          $data['message']='Address updated successfully';
        }else{
          $data['error']=true;
          $data['message']='Error occurred';
        }
    }else{
      $data['error']=true;
      $data['message']='No parameters received';
    }
    echo json_encode($data);
  }

  public function deleteaddress(){
    $data=array();
      if($this->input->post()){
          $param['addressid']=$this->input->post('addressid');
          $param['customerid']=$this->input->post('customerid');
          $res=$this->Do_user->deleteaddress($param);
          if($res){
              $data['error']=false;
              $data['message']='Address deleted successfully';
          }else{
              $data['error']=true;
              $data['message']='Error occurred';
          }
    }else{
        $data['error']=true;
        $data['message']='No parameters received';
    }
    echo json_encode($data);
  }

}

?>
