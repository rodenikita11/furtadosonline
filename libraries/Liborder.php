<?php 
class Liborder{
             public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_order');
			 $this->ci = $CI;
             
         }
         
         
        public function createneworder($param){
			if(is_array($param)){
				$res=$this->ci->Do_order->createneworder($param); 
				 
				if($res){
					  
					return true;
					 
				}else{
					  
					return false;   
					  
				}
				 
			}
		}
		 
		public function additem($param){
			if(is_array($param)){
				 $res=$this->ci->Do_order->addorderitem($param); 
				if($res){
					return true;
				}else{
					return false;   
				} 
			}
		}
		 
		 public function getorderdetail($param){
			 if(is_array($param)){
				 
				 $res=$this->ci->Do_order->getOrderDetailByInvoice($param);  
				 
				 if($res){
					  
					 return $res;
					 
				  }else{
					  
					return false;   
					  
				  } 
				  
				  
				  
			  }
			 
		 }
		 
		 public function updatepayment($param){
			  if(is_array($param)){
				 $this->log_fileDetails('updatepayment', $param);
				 $res=$this->ci->Do_order->updatePaymentMethod($param);
				 
				 if($res){
					  
					 return true; 
					 
				  }else{
					  
					return false;   
					  
				  } 
				  
				  
				  
			  }
			 
		 }
	protected function log_fileDetails($fun_name, $query){
		$this->log = '------------------FileName: Liborder.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
		$this->log = $this->log . json_encode($query).PHP_EOL; // appending the query
		$this->logpath = APPPATH . 'logs/pre_updatepayment_queries-' . date('Y-m-d') . '.log';
		error_log($this->log, 3, $this->logpath);
	}

	public function log_data($name, $param){
		$this->log_fileDetails($name, $param);
	}

	public function updatefinalorderdetails($param){
		$this->log_fileDetails('updatefinalorderdetails', $param);
		if($param[orderinvoice_no]){
			 $res=$this->ci->Do_order->updateorderdetails($param);
			  if($res){
					  return true;
				}else{
					  return false;   
				} 
			 }else{
				echo "order number is missing";	 die;
		 }	 
	 }
		
		public function insert_tempforweb($param){
			if($param[orderinvoice_no]){
			
			 $res=$this->ci->Do_order->insert_tempforweb($param);
			  if($res){
					  return true;
				}else{
					  return false;   
				} 
			 }else{
				echo "order number is missing";	 die;
		  }	 
			
			
			
		}
		
		public function getmyorders($param){
			if($param['usrid']){
			 $res=$this->ci->Do_order->getmyorders($param);
			 
			 //print_R($res);
			  if($res){
				    foreach($res as $row){
						
						$orderdata[$row['orderinvoice_no']]['orderinvoice_no']=$row['orderinvoice_no'];
						$orderdata[$row['orderinvoice_no']]['order_date']=$row['created'];
						$orderdata[$row['orderinvoice_no']]['product'][$row['product_id']]['product_id']=$row['product_id'];
						$orderdata[$row['orderinvoice_no']]['product'][$row['product_id']]['name']=$row['proname']; 
						$orderdata[$row['orderinvoice_no']]['product'][$row['product_id']]['image']=$row['prothumbnail'];
						
					}
				  return $orderdata;
					 
				}else{
					  return false;   
				} 
			 }else{
				echo "Please login to check";	 die;
		  }	 
			
		}

		public function getweborderbyuser($param){
			if($param['usremail']){
			 $res=$this->ci->Do_order->getweborderbyuser($param);
			  if($res){
					  return $res;
				}else{
					  return false;   
				} 
			 }else{
				echo "Please login to check";	 die;
		  }	 
			
		}

			public function getorderdtl($param=false){
			
			 $res=$this->ci->Do_order->getorderdtl($param);
			 //print '<pre>';print_r($res); die;
			  if($res){
					  return $res;
				}else{
					  return false;   
				} 
			} 

			public function getallstate($param=false){
			
			 $res=$this->ci->Do_order->getallstate($param);
			  if($res){
					  return $res;
				}else{
					  return false;   
				} 
			}
			
			public function getlastinv($param=false){
			  
				 $res=$this->ci->Do_order->getlastinv(); 
				 
				 if($res){
					  
					 return $res; 
					 
				  }else{
					  
					return false;   
					  
				  } 
				  
				  
				  
			
			 
		 } 
		 
		 public function gettotalamount($id){
			  $res=$this->ci->Do_order->totalamountbyid($id); 
				 
				 if($res){
					  
					 return $res; 
					 
				  }else{
					  
					return false;   
					  
				  } 
			 
			 
			 
			 }
			 
			 public function getgiftcardorderdtl($param=false){
			  
			 $res=$this->ci->Do_order->getfinalorderdetails($param); 
			  if($res){
					  return $res;
				}else{
					  return false;   
				} 
			} 

            public function checkorderiswithgiftcard($orderid){
			//print_R($orderid);die;
			if($orderid){
				 $res=$this->ci->Do_order->checkorderiswithgiftcard($orderid); 
				 if($res){
					  
					 return $res; 
					 
				  }else{
					  
					return false;   
					  
				  } 
				
				
			}else{
				
				echo "Please pass order id as a parameter";exit;
			}
			
			
		}
		
		
		public function savegiftcarddetails($param=false){
			
			if($param){
				 $res=$this->ci->Do_order->savegiftcarddetails($param); 
				 if($res){
					  
					 return true; 
					 
				  }else{
					  
					return false;   
					  
				  } 
				
				
			}else{
				
				echo "Please pass all the parameter";exit;
			}
			
			
			
			
		}	

		public function saveproductfeedback($param){
			if($param){
				 $res=$this->ci->Do_order->saveproductfeedback($param); 
				 if($res){
					  
					 return true; 
					 
				  }else{
					  
					return false;   
					  
				  } 
				
				
			}else{
				
				echo "Please pass all the parameter";exit;
			}
		}

	public function getofferdetails($param){
		$res=$this->ci->Do_order->getofferdetails($param);
		return $res;
	}

	public function emibanks(){
		$res=$this->ci->Do_order->emibanks();
		return $res;
	}

	public function bankdetails($param){
		return $this->ci->Do_order->emibanks($param);
	}
}?> 