<?php
class Do_order Extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    public function createneworder($params){
        if ($_SESSION['bundleid']){
            $bundleId   = $_SESSION['bundleid'];
            $isbundleid = 1;
        } else {
            $bundleId   = 0;
            $isbundleid = 0;
        }
        if (empty($params))
            return false;
        $createOrder = "insert into order_master set orderinvoice_no='" . $params['orderinvoice_no'] . "', customer_id='" . $params['customer_id'] . "', shipping_master_id='" . $params['shipping_master_id'] . "', total_item_order='" . $params['total_item_order'] . "', order_pay_status='0', orderuser_agent='" . $_SERVER['HTTP_USER_AGENT'] . "', orderip='" . $_SERVER['REMOTE_ADDR'] . "', order_platform='".$params['platform']."', total_amount='" . $params['total_amount'] . "',total_courier_charges='" . $params['total_courier_charges'] . "', total_tax_amount=NULL, discount_amount=NULL, final_amount_to_pay='" . $params['final_amount_to_pay'] . "', order_status='Tranasction Pending', orderdate_added=now(),shipping_mode='" . $params['shipping_mode'] . "', order_session_json_details='" . json_encode($params) . "',isbundleid='" . $isbundleid . "',bundleId='" . $bundleId . "' ,created=now(), modified=now()";
        
        if ($params['giftcard_value']){
            $createOrder .= ",is_offer_used='Y',offer_type='GFT', offer_amounts='" . $params['giftcard_value'] . "',offer_pk_id='" . $cart_data['giftcode'] . "'";
        }
        /*if ($params['coupon_id']) {
            $createOrder .= ",is_offer_used='Y',offer_type='CPN', offer_amounts='" . $params['coupon_value'] . "',offer_pk_id='" . $cart_data['coupon_id'] . "'";
        }*/
        $createOrder .= "ON DUPLICATE KEY UPDATE total_item_order='" . $params['total_item_order'] . "', order_pay_status='0', total_amount='" . $params['total_amount'] . "',total_courier_charges='" . $params['total_courier_charges'] . "', total_tax_amount=NULL, discount_amount=NULL, final_amount_to_pay='".$params['final_amount_to_pay'] . "', order_session_json_details='" . json_encode($params) . "'";
        if ($params['giftcard_value']) {
            $createOrder .= ",is_offer_used='Y',offer_type='GFT', offer_amounts='" . $params['giftcard_value'] . "',offer_pk_id='" . $cart_data['giftcode'] . "'";
        }
        if ($params['coupon_id']) {
            $createOrder .= ",is_offer_used='Y',offer_type='CPN', offer_amounts='" . $params['offerdetails'][0]['discount'] . "', coupon_or_gift_code='".$params['offerdetails'][0]['code']."', offer_pk_id='" . $cart_data['coupon_id'] . "'";
        }
        //echo $createOrder; die;
        $isCreate = $this->db->query($createOrder);
        return $isCreate;
    }
    
    public function addorderitem($params){
        //print_R($params); die;
        if (empty($params))
            return false;
        $addItem   = "insert into order_product set order_id='" . $params['order_id'] . "', product_id='" . $params['product_id'] . "',  name='" . addslashes($params['name']) . "', quantity='" . $params['quantity'] . "', price='" . $params['price'] . "', total='" . $params['total'] . "', status=0,  order_status='" . $params['order_status'] . "', is_delivered='" . $params['is_delivered'] . "', is_promo='" . $params['is_promo'] . "', free_home_delivery='" . $params['free_home_delivery'] . "', delivery_charges='" . $params['delivery_charges'] . "', created=now(), modified=now(), timeline=now()";
        if($params['is_offer_used']=='Y'){
            $addItem.=" ,is_offer_used='".$params['is_offer_used']."', offer_type='".$params['offertype']."', coupon_or_gift_code='".$params['coupon_or_gift_code']."', offer_amounts=".$params['offer_amounts']."";
        }

        $addItem.=" ON DUPLICATE KEY UPDATE quantity='" . $params['quantity'] . "', price='" . $params['price'] . "', total='" . $params['total'] . "', status=0,  order_status='" . $params['order_status'] . "', is_delivered='" . $params['is_delivered'] . "', is_promo='" . $params['is_promo'] . "', free_home_delivery='" . $params['free_home_delivery'] . "', delivery_charges='" . $params['delivery_charges'] . "', created=now(), modified=now(), timeline=now()";

        if($params['is_offer_used']=='Y'){
            $addItem.=" ,is_offer_used='".$params['is_offer_used']."', offer_type='".$params['offertype']."', coupon_or_gift_code='".$params['coupon_or_gift_code']."', offer_amounts=".$params['offer_amounts']."";
        }

        $isItemAdd = $this->db->query($addItem);
        return $isItemAdd;
    }
    
    public function updatePaymentMethod($params){
        if (empty($params))
            return false;
        $update = "update order_master set orderpayment_method='" . $params['payment_method'] . "',total_amount='" . $params['total_amount'] . "',total_courier_charges='" . $params['total_courier_charges'] . "', final_amount_to_pay=".$params['final_amount_to_pay']." where orderinvoice_no='" . $params['orderinvoice_no'] . "'";
        
        if ($params['gift_card_id']){
            $update1= "update order_giftcard_details set gift_card_redemption='yes',redemption_date=now() where id='" . $params['gift_card_id'] . "'";
            $update_pay_status1 = $this->db->query($update1);
        }
        if ($params['coupon_id']){
            $update1 = "insert into coupon_history set  coupon_id='" . $params['coupon_id'] . "' ,order_id='" . $params['orderinvoice_no'] . "',  customer_id='" . $param['customer_id'] . "',
			    amount='" . $params['total_card_value'] . "' ,date_added=now();";
            $update_pay_status1 = $this->db->query($update1);
        }
        
        $update_pay_status = $this->db->query($update);
        return $update_pay_status;
    }
    public function getOrderDetailByInvoice($params){
        if (empty($params))
            return false;
        
        
        $select       = "SELECT om.*,csa.* FROM order_master om JOIN `customer_shipping_address` csa ON csa.address_id=om.shipping_master_id where om.orderinvoice_no='" . $params['orderinvoice_no'] . "' and om.customer_id='" . $params['customer_id'] . "'";
        // 		echo $select; die;
        $orderDetails = $this->db->query($select);
        if ($orderDetails) {
            $orderResultSet = $orderDetails->result_array();
            
            return $orderResultSet;
        }
    }
    
    public function updateorderdetails($params){
        if (empty($params))
            return false;
        if ($params['order_pay_status'] == 1) {
            $params['order_status'] = 'Inprocess';
        } else {
            $params['order_status'] = 'Transaction declined';
        }
        
        $update = "update order_master set order_pay_status='" . $params['order_pay_status'] . "', payment_gateway_response='" . $params['payment_gateway_response'] . "',order_status='" . $params['order_status'] . "' where orderinvoice_no='" . $params['orderinvoice_no'] . "'";
        
        $update_pay_status = $this->db->query($update);
        return $update_pay_status;
        
        
    }

    public function findStateCode($st){
	   #echo 'state'; echo $st;
		if($st != 0){
		 $findState_sql = "SELECT stateCode FROM stateMaster WHERE id = ".$st." LIMIT 1";
			$findState = $this->db->query($findState_sql);
			if(!empty($findState)){
				$results=$findState->result_array();
				#$findState_row = $findState->fetch_array();
				#print_R($results);exit;
			}
			return stripslashes($results[0]["stateCode"]);
		}
} 

    public function insert_tempforweb($param){
        $orderNumber = $param['orderinvoice_no'];
        $find_user_sql = "SELECT customer_id FROM `order_master` WHERE orderinvoice_no = " . $param['orderinvoice_no'] . " LIMIT 1";
        $find_user     = $this->db->query($find_user_sql);
        if ($find_user) {
            if ($find_user->num_rows() == 1) {
                $find_userRow = $find_user->result_array();
                $USER_ID      = $find_userRow[0]['customer_id'];
            }
            
            // Logged in user info
            $selectName_sql = "SELECT ud.title, ud.firstname, ud.lastname, ud.email, st.address_1, st.address_2, st.city, st.state,cm.cityName as 'cityname',sm.stateName as 'statename', st.postcode, st.contact_num  FROM customer AS ud join customer_billing_address
                AS st left join cityMaster cm on st.city=cm.id left join stateMaster sm on st.state=sm.id WHERE ud.customer_id = '" . $USER_ID . "' AND ud.status = 1 AND ud.customer_id = st.customer_id LIMIT 1";
            $selectName     = $this->db->query($selectName_sql);
            if ($selectName->num_rows > 0) {
                $selectNameRow = $selectName->result_array();
                
                $fname           = $selectNameRow[0]["firstname"];
                $lname           = $selectNameRow[0]["lastname"];
                $bill_userName   = $selectNameRow[0]["title"] . " " . $fname . " " . $lname;
                $emailId         = $selectNameRow[0]["email"];
                $bill_address1   = stripslashes($selectNameRow[0]["address_1"]);
                $bill_address2   = stripslashes($selectNameRow[0]["address_2"]);
                $city            = $selectNameRow[0]["city"];
                //$otherCity = $selectNameRow["otherCity"];
                //$stateCode = findStateCode($selectNameRow["state"], $mysqli);
		$stateCode = $this->findStateCode($selectNameRow[0]["state"]);
                $pincode         = $selectNameRow[0]["postcode"];
                $telephoneNumber = $selectNameRow[0]["contact_num"];
                //$faxNumber = $selectNameRow["faxNumber"];
                $cityname        = $selectNameRow[0]["cityname"];
		$statename	= $selectNameRow[0]["statename"];
                //echo  $fname; echo $lname; echo $bill_userName; die; 
            }
            
            
        }
        
 $selectOrderSql = "SELECT  om.customer_id,om.created,om.orderpayment_method,om.orderpayment_id,om.order_pay_status,om.total_courier_charges,om.final_amount_to_pay,om.total_amount,om.order_session_json_details,
                       csm.firstname,csm.lastname,csm.address_1,csm.city,csm.state,csm.contact_num,csm.	postcode from order_master om join customer_shipping_address csm on om.shipping_master_id =csm.address_id where  om.orderinvoice_no= " . $param['orderinvoice_no'] . " 
					   LIMIT 1";
        $selectOrder    = $this->db->query($selectOrderSql);
        if ($selectOrder) {
            if ($selectOrder->num_rows() == 1){
                $selectOrderRow = $selectOrder->result_array();
                //print_R($selectOrderRow); die;
                $status         = $selectOrderRow[0]['order_pay_status'];
                $orderDate      = $selectOrderRow[0]['created'];
                $paymentType    = $selectOrderRow[0]['orderpayment_method'];
                
                $customerName        = $fname . ' ' . $lname;
                $shippingAddress     = $selectOrderRow[0]['firstname'] . '<br>' . $selectOrderRow[0]['address_1'] . '<br>' . $selectOrderRow[0]['city'] . '<br>' . $selectOrderRow[0]['state'] . '-' . $selectOrderRow[0]['postcode'] . '<br>Telephone No.- ' . $selectOrderRow[0]['contact_num'];
                $subTotal            = $selectOrderRow[0]['total_amount'];
                $shippingCost        = $selectOrderRow[0]['total_courier_charges'];
                $shippingCostDisplay = ($shippingCost == '0.00') ? $freeShippingMessage1 : '<img src="' . $WEB_IMAGE_PATH . 'rupee.jpg" alt="Rs."/> ';
                $orderTotal          = $selectOrderRow[0]['final_amount_to_pay'];
		$line_no = 10000;
                
                $productDetailsArr = unserialize($selectOrderRow[0]['order_session_json_details']);
                
               // print_R($productDetailsArr);die;
                
            }
        }
        
        // Insert into tempforweb
        
       // $otherdb            = $this->load->database('temp', TRUE);
      $selectTempUser_sql = "SELECT `No.` 
							   FROM furtados_tempforweb.tempwebcustomer 
							   where `No.`= 'WC-" . $USER_ID . "' 
							   LIMIT 1";
        
       // print_r($otherdb); die;
        $selectTempUser = $this->db->query($selectTempUser_sql);        
        if ($selectTempUser->num_rows() == 0) {
            
            
        $insertCustomer_sql = "INSERT INTO furtados_tempforweb.tempwebcustomer  
									   SET	`No.` = 'WC-'" . $USER_ID . "',
											`Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
											`Search Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
											`Address` = '" . addslashes($bill_address1) . "',
											`Address 2` = '" . addslashes($bill_address2) . "',
											`City` = '" . addslashes($cityname) . "',
											`Post Code` = '" . $pincode . "',
											`State Code` = '" . $stateCode . "',
											`Country Code` = 'INDIA',
											`Contact` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
											`E-Mail` = '" . $emailId . "',
											`Phone No.` = '" . $telephoneNumber . "',
											`Fax No.` = '" . $faxNumber . "',
											`Customer Posting Group` = 'INTERNET',
											`Gen. Bus. Posting Group` = 'DOM',
											`VAT Bus. Posting Group` = 'VAT',
											`Structure` = 'MH',
											`Tax Liable` = 1,
											`imported` = 1";
            //$insertCustomer     = $this->db->query($insertCustomer_sql);
            
        } else {
            
            $selectTempUserRow = $selectTempUser->result_array();
            
            if ($selectTempUserRow['imported'] == 2) {
                $newStatus = 2;
            } else {
                $newStatus = 1;
            }
            
         $updateCustomer_sql = "UPDATE furtados_tempforweb.tempwebcustomer  
								   SET	`Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "', 
										`Search Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
										`Address` = '" . addslashes($bill_address1) . "',
										`Address 2` = '" . addslashes($bill_address2) . "',
										`City` = '" . addslashes($cityname) . "',
										`Post Code` = '" . $pincode . "',
										`State Code` = '" . $stateCode . "',
										`Country Code` = 'INDIA',
										`Contact` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
										`E-Mail` = '" . $emailId . "',
										`Phone No.` = '" . $telephoneNumber . "',
										`Fax No.` = '" . $faxNumber . "',
										`Customer Posting Group` = 'INTERNET',
										`Gen. Bus. Posting Group` = 'DOM',
										`VAT Bus. Posting Group` = 'VAT',
										`Structure` = 'MH',
										`Tax Liable` = 1,
										`imported` = " . $newStatus . " 
									 `No.`='WC-" . $USER_ID . "'  ";
           // $updateCustomer     = $this->db->query($updateCustomer_sql);
            
        }
       $insertInline_sql = "INSERT INTO furtados_tempforweb.tempwebsalesline 
								 SET 	`Document Type` = '1',
										`Document No.` = '".$param['platform']."-" . $orderNumber . "',
										`Sell-to Customer No.` = 'WC-" . $USER_ID . "',
										`Line No.` = '" . $line_no . "',
										`Type` = '2',
										`No.` = '" . $arrPid . "',
										`Description` = '" . addslashes($arrPidDetails['name']) . "',
										`Unit of Measure` = 'PCS',
										`Quantity` = '" . $arrPidDetails['quantity'] . "',
										`Amount Including Tax` = '" . $navPrice . "',
										`Location Code` = '310',
										`Shipping cost` = '" . $incShip . "', 
										`Discount_amount` = '" . $disPrice . "',
										`imported` = 1";
      //  $insertInline     = $this->db->query($insertInline_sql);
        $line_no += 10000;
        $oneTime_Ship = 1;
        $incShip      = 0;
        
        
        
        
        
        
        $shipping_arr = unserialize($selectOrderRow["shippingDetails"]);
        
        $address1 = substr($shipping_arr["Address"], 0, 55);
        $address2 = substr($shipping_arr["Address"], 55, strlen($shipping_arr["Address"]) - 1);
        
        $insertSalesH_sql = "INSERT INTO furtados_tempforweb.tempwebsalesheader
							SET `Document Type` = '1',
								`No.` = '".$param['platform']."-" . $orderNumber . "',
								`Sell-to Customer No.` = 'WC-" . $USER_ID . "',
								`Ship-to Name` = '" . strtoupper($shipping_arr["Name"]) . "',
								`Ship-to Address` = '" . $address1 . "',
								`Ship-to Address 2` = '" . $address2 . "',
								`Ship-to Post Code` = '" . $shipping_arr["Pincode"] . "',
								`Ship-to City` = '" . $shipping_arr["City"] . "',
								`Ship-to Contact` = '" . strtoupper($shipping_arr["Name"]) . "',
								`Bill-to Customer No.` = 'WC-" . $USER_ID . "',
								`Bill-to Name` = '" . strtoupper(strtolower($bill_userName)) . "',
								`Bill-to Address` = '" . addslashes($bill_address1) . "',
								`Bill-to Address 2` = '" . addslashes($bill_address2) . "',
								`Bill-to Post Code` = '" . $pincode . "',
								`Bill-to City` = '" . $cityname . "',
								`Bill-to Contact` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
								`Order Date` = '" . $selectOrderRow["insertDate"] . "',
								`Location Code` = '310',
								`Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
								`Search Name` = '" . strtoupper($fname) . " " . strtoupper($lname) . "',
								`Address` = '" . addslashes($bill_address1) . "',
								`Address 2` = '" . addslashes($bill_address2) . "',
								`City` = '" . $cityname . "',
								`Post Code` = '" . $pincode . "',
								`State Code` = '" . $stateCode . "',
								`Country Code` = 'INDIA',
								`Contact` = '" . strtoupper($fname) . " " . strtoupper($lname) . "', 
								`E-Mail` = '" . $emailId . "',
								`Phone No.` = '" . $telephoneNumber . "',
								`Fax No.` = '" . $faxNumber . "',
								`Customer Posting Group` = 'INTERNET',
								`Gen. Bus. Posting Group` = 'DOM',
								`VAT Bus. Posting Group` = 'VAT',
								`Structure` = 'MH',
								`Tax Liable` = 1,
								`imported` = 0, 
								`Payment Type` = '" . $paymentType . "', 
								`Payment Id` = '" . $selectOrderRow["paymentId"] . "', 
								`Promotion details` = '" . $promotionDetailsStr . "',
								`Shiping Mode` = '" . $tempShip . "'";

                              //  die;
       // $insertSalesH     = $this->db->query($insertSalesH_sql);
        
    }
    
    public function getmyorders($param)
    {
        $selectOrderSql = "SELECT om.orderinvoice_no, om.created,status,pm.proname,pm.prothumbnail,op.product_id  FROM order_master om join order_product op  on
	om.orderinvoice_no=op.order_id join product_master pm on pm.proid=op.product_id
  WHERE  om.order_status ='inprocess' and customer_id= " . $param['usrid'] . " group by op.order_product_id ORDER BY om.created DESC";
        //limit 0,5
        $res            = $this->db->query($selectOrderSql);
        if ($res) {
            
            $res1 = $res->result_array();
            return $res1;
            
        }
        
    }
    
    public function getweborderbyuser($param = false)
    {
        
        $params['email'] = $param['usremail'];
        if ($param['order_id']) {
            $params['order_id'] = $param['order_id'];
        }
        
        $ci = curl_init();
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ci, CURLOPT_HEADER, false);
        
        if ($param['order_id']) {
            $str = SITEURL . 'FMPcrons/Cronjobs/getweborderdetails.php?email=' . $params['email'] . '&order_id=' . $params['order_id'] . '';
        } else {
            $str = SITEURL . 'FMPcrons/Cronjobs/getweborderdetails.php?email=' . $params['email'];
        }
        //curl_setopt($ci, CURLOPT_URL,'http://furtadosonline.com/mobile/FMPcrons/Cronjobs/getweborderdetails.php');
        curl_setopt($ci, CURLOPT_URL, $str);
        //curl_setopt($ci, CURLOPT_POSTFIELDS, $params);
        
        $response = curl_exec($ci);
        
        if ($response) {
            return $response;
        } else {
            return false;
        }
        curl_close($ci);
        
    }
    
    
    public function updatebundle()
    {
        $update = "update userBundleMaster set status =2 where id='" . $_SESSION['bundleid'] . "'";
        $res    = $this->db->query($update);
        if ($res) {
            return true;
        }
    }
    
    public function getorderdtl($param){
        //$selectOrderSql = "SELECT om.orderinvoice_no as orderId,om.customer_id, concat(cs.firstname,' ',cs.lastname) as 'customer_name',cs.email as 'customer_email',op.product_id,pm.prothumbnail as image , op.name, op.quantity, pm.onlinePrice, pm.discountPrice, op.price,op.total,om.total_courier_charges, om.is_offer_used, om.offer_type, om.offer_amounts, om.total_amount, om.final_amount_to_pay, om.orderpayment_method,om.order_pay_status,om.order_status,om.shipping_mode,om.orderdate_added as 'insertDate',csa.contact_num,csa.address_1,csa.landmark,csa.city,csa.state,csa.postcode FROM order_master om join order_product op on om.orderinvoice_no=op.order_id join product_master pm on op.product_id=pm.proid join customer_shipping_address csa on om.shipping_master_id=csa.address_id join customer cs on csa.customer_id=cs.customer_id where om.orderinvoice_no=" . $param['order_no'] . " GROUP BY op.product_id";
        //print_r($selectOrderSql); die;
        
        //replace by vikram on 11 may 2018 because of shipping address_id issue in join 
    	$selectOrderSql= "SELECT om.orderinvoice_no as orderId,om.customer_id, concat(cs.firstname,' ',cs.lastname) as 'customer_name',cs.email as 'customer_email',op.product_id,pm.prothumbnail as image , op.name, op.quantity, pm.onlinePrice, pm.discountPrice, op.price,op.total,om.total_courier_charges, om.is_offer_used, om.offer_type, om.offer_amounts, om.total_amount, om.final_amount_to_pay, om.orderpayment_method,om.order_pay_status,om.order_status,om.shipping_mode,om.orderdate_added as 'insertDate',csa.contact_num,csa.address_1,csa.landmark,csa.city,csa.state,csa.postcode, om.coupon_or_gift_code, op.is_offer_used offersused, op.offer_type offertype, op.coupon_or_gift_code offercode, op.offer_amounts offeramount FROM order_master om join order_product op on om.orderinvoice_no=op.order_id join product_master pm on op.product_id=pm.proid left join customer_shipping_address csa on om.shipping_master_id=csa.address_id  join customer cs on om.customer_id=cs.customer_id where om.orderinvoice_no=" . $param['order_no'] . " GROUP BY op.product_id";

        //echo $selectOrderSql; die;

        $res = $this->db->query($selectOrderSql);
        if($res){
            $res1 = $res->result_array();
            return $res1;
        }
    }
    
    public function getproiddtl($param = false)
    {
        //print_r($param);die;
        $selectOrderSql = "SELECT promotionId, onlinePrice, clearanceId, clearanceMRP FROM product_master WHERE proid = " . $param . " AND isAvailable = 1 LIMIT 1";
        //	die;
        $res            = $this->db->query($selectOrderSql);
        if ($res) {
            $res1 = $res->result_array();
            return $res1;
        }
    }
    
    /*public function getclearencelogo($param=false)
    {
    echo $clSql = "SELECT logoImage FROM clearanceMaster WHERE id = ".$param." AND status = 1 LIMIT 1";
    die;
    $res=$this->db->query($clSql);
    if($res){
    $res1=$res->result_array();
    return $res1;
    }else{
    return false;
    }
    
    }
    
    public function getpromotiondtl($param=false)
    {
    $today = date('Y-m-d H:i:s');
    echo $clSql = "SELECT Name, logoImage, shippingstatus FROM promotionMaster WHERE id = ".$param." AND endDate >= '".$today."' AND startDate <= '".$today."' AND status = 1 LIMIT 1";
    die;
    $res=$this->db->query($clSql);
    if($res){
    $res1=$res->result_array();
    return $res1;
    }else{
    return false;
    }
    
    }*/
    
    public function getallstate($param = false)
    {
        $clSql = "SELECT id,stateName FROM `stateMaster`";
        
        $res = $this->db->query($clSql);
        if ($res) {
            $res1 = $res->result_array();
            return $res1;
        } else {
            return false;
        }
        
    }
    
    
    public function getlastinv()
    {
        $select= "select orderinvoice_no from order_master order by orderid desc limit 1";
        $res= $this->db->query($select);
        if ($res) {
            $res1 = $res->result_array();
            return $res1;
        } else {
            return false;
        }
        
    }
    
    public function totalamountbyid($id)
    {
        $select = "select total_amount  from  order_master  WHERE orderinvoice_no = " . $id . "";
        $res2   = $this->db->query($select);
        $res3   = $res2->result_array();
        return $res3;
        
    }
    
    
    public function checkorderiswithgiftcard($orderid)
    {
        $select = "select product_id from order_product where order_id=" . $orderid . "";
        $res2   = $this->db->query($select);
        if ($res2) {
            $res3 = $res2->result_array();
            if ($res3) {
                foreach ($res3 as $row) {
                    $checkcat = "SELECT category_id  from product_to_category where proid=" . $row['product_id'] . "";
                    $rescat   = $this->db->query($checkcat);
                    if ($rescat) {
                        $rescatid = $rescat->result_array();
                        //discuss with dev sir to make it dynamic on product base of take category id by name.// sneha(2018-03-03)/
                        if ($rescatid[0]['category_id'] == '100009') {
                            return $rescatid[0]['category_id'];
                            
                        } else {
                            
                            return false;
                        }
                        
                    }
                }
                
                
            }
        }
        
    }
    
    
    public function getfinalorderdetails($param)
    {
        
        $select = "select  om.orderinvoice_no,op.product_id,op.price,op.name,om.created,pm.prothumbnail from order_master om join  order_product op  on om.orderinvoice_no=op.order_id join product_master pm on  op.product_id =pm.proid where om.orderinvoice_no=" . $param['order_no'] . "";
        $res2   = $this->db->query($select);
        if ($res2) {
            $res3 = $res2->result_array();
            if ($res3) {
                return $res3;
                
            } else {
                return false;
            }
            
        }
        
    }
    
    public function savegiftcarddetails($param)
    {
        $select = "INSERT  into order_giftcard_details set order_id =" . $param['order_no'] . ", gift_card_id='" . $param['giftcard_name'] . "',gift_card_value='" . $param['price'] . "',user_email_id='" . $param['emailid'] . "',gift_mesg='" . $param['mesg'] . "',validity='" . $param['validity'] . "'";
        $res2   = $this->db->query($select);
        if ($res2) {
            return $res2;
        } else {
            return false;
            
        }
        
    }

    public function saveproductfeedback($param){
        //print_R($param); die;
        $insert = "INSERT INTO `feedbackMaster`(`full_name`, `emailid`, `commentText`, `flag`, `feedbackType`,  `status`, `insertDate`, `ipAddress`) VALUES ('" . $param['usrname'] . "','" . $param['user_email'] . "','" . $param['comment'] . "','regular','2','1',now(),'" . $param['ip'] . "')";
        $res2   = $this->db->query($insert);
        if ($res2) {
            return $res2;
        } else {
            return false;
        }
    }
    
    public function getofferdetails($param){
        $select="SELECT * FROM coupon WHERE code='".$param['code']."'";
        $res=$this->db->query($select);
        return $res->result_array();
    }
}
?>
