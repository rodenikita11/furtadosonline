<?php 
  include('head.php'); 
  $searchval=$_GET['search'];
  switch($_GET['maincat']){
      case '1':
        $search_concept='Musical Instruments';
        break;
      case '2':
        $search_concept='Music Books';
        break;
      case '3':
        $search_concept='Pro Audio';
        break;
      default:
        $search_concept='All';
  }
?>
<link rel="stylesheet" href="<?php echo WEBCSS;?>adjustment.css?v=123">

<body class="cnt-home scrollbar" data-scrollload="">
   <!-- ============================================== HEADER ============================================== -->
   <header class="header-style-1" data-flashmessage="<?php echo $this->session->flashdata('changepassword'); ?>" data-flashforfb="<?php echo $this->session->flashdata('flashmessage'); ?>">
      <!-- ============================================== TOP MENU ============================================== -->
      <div class="navbar navbar-fixed-top">
      <div class="top-bar animate-dropdown">
         <div class="container">
            <div class="header-top-inner">
               <div class="cnt-account">
                  <ul class="list-unstyled">
                     <li><a href="tel: 022 - 4287 5050/60">Customer Services :  022 - 4287 5050/60</a></li><span style="color:#fff">|</span> 
                     <li><a href="tel: 022 - 66225423">BOOKS ENQUIRY :  022 - 66225423</a></li><span style="color:#fff">|</span> 
                      <li>
                        <a>10 AM - 6:30 PM (Mon to Sat)</a>
                      </li>
                     <!--<li class="TO"><a href="<?php echo SITEMOBURL?>user/trackorder"><img class="Mr-6" src="<?php echo WEBIMG;?>track.png">Track Order</a></li>-->
                  </ul>
               </div>
               <!-- /.cnt-account -->
               <!-- /.cnt-cart -->
               <div class="clearfix"></div>
            </div>
            <!-- /.header-top-inner -->
         </div>
         <!-- /.container -->
      </div>

      <!-- /.header-top -->
      <div class="topmar animate-dropdown topbg">
         <div class="">
            <div class="header-top-inner">
            <?php 
              date_default_timezone_set('Asia/Calcutta');
              $date=date('Y-m-d H:i');
              $defdate='2019-01-05 10:00:00';
            
              if($date > $defdate){
            ?>
             <!--<marquee behavior="scroll" direction="left"><i><b>We are currently upgrading our website and the Shipping of Orders may be delayed. We are sorry for any inconveniences caused.</b></i></marquee>-->

           <?php } ?>
           <!--  <marquee behavior="scroll" direction="left"><i><b>Due to a major technology upgrade on our website we will be unable to ship orders till Thursday, October 25, 2018. Thank you for your understanding and sorry for the inconvenience.</i></b></marquee>-->

               <div class="clearfix"></div>
            </div>
         </div>
      </div>
      <!-- ============================================== TOP MENU : END ============================================== -->
      <div class="main-header">
         <div class="container">
            <div class="row ml">
               <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                  <!-- ============================================================= LOGO ============================================================= -->
                  <div class="logo"> <a href="<?php echo SITEURL;?>"> <img src="<?php echo WEBIMG;?>logo.png" alt="logo"> </a> </div>
                  <!-- /.logo -->
                  <!-- ============================================================= LOGO : END ============================================================= -->
               </div>
               <div class="col-xs-12 col-sm-6 col-md-6 logo-holder plrt">
                    <!--<form method="GET" action="<?php echo SITEMOBURL?>product/search"  class="formset">
              
               <div class="col-xs-12 col-sm-12 col-md-1 top-search-holder prl0 z-index9">
                <select name="maincat" class="selectdrp">
                              <option value="0" <?php echo ($_GET['maincat']==0) ? 'selected' : ''; ?>>All</option>
                              <option value="1" <?php echo ($_GET['maincat']==1) ? 'selected' : ''; ?>>Musical Instruments</option>
                              <option value="2" <?php echo ($_GET['maincat']==2) ? 'selected' : ''; ?>>Music Books</option>
                              <option value="3" <?php echo ($_GET['maincat']==3) ? 'selected' : ''; ?>>Pro Audio</option>
                           </select>
               </div>
               <div class="col-xs-12 col-sm-12 col-md-5 top-search-holder prl5 z-index9" style="padding-right: 0px;padding-left: 0px;" >
                
                 
                  <div class="search-area">
                     
                        <div class="control-group" style="width:518px !important">
                           <!--<input type="text"  value='<? echo $_GET["searchText"]?>' autocomplete="off" name="searchText" id="twotabsearchtextbox" placeholder="Search Products" onkeyup="suggestKeywords(this.value, 1);" value="<?php echo $searchText; ?>" oninput="test()"/>
                           
                           <input type="text"  value='<?php echo $_GET["searchText"]; ?>' autocomplete="off" name="searchText" id="twotabsearchtextbox" placeholder="Search Products" oninput="test()"/>
                          <div class="suggestionsBox" id="suggestions" style="display: none;" >
                              <div class="suggestionList" id="suggestionsList"></div>
                              <div style="clear:both;"></div>
                          </div>
                           <button class="search-button pd12ad" href="javascript:viod(0)" name="submit" id="submit" type="submit"></button>
                        </div>
                     </form>
                  </div>
                 
                 
               </div>
              
             </form>-->
                <form method="GET" action="<?php echo SITEMOBURL?>product/search"  class="formset">
                 <div class="input-group width615">
                    <div class="input-group-btn search-panel">
                       <button type="button" class="btn btn-default dropdown-toggle btnadjst" data-toggle="dropdown">
                       <span id="search_concept"><?php echo $search_concept; ?></span><span class="caret"></span>
                       </button>
                       <ul class="dropdown-menu selectdrp" role="menu">
                          <li value="0"><a href="">ALL</a></li>
                          <li value="1"><a href="">Musical Instruments</a></li>
                          <li value="2"><a href="">Music Books</a></li>
                          <li value="3"><a href="">Pro Audio</a></li>
                       </ul>
                    </div>
                    <input type="hidden" name="maincat" value="0" id="search_param">
                    <input type="text" class="form-control inputbtn whitead" value="<?php echo $_GET["searchText"]; ?>" name="searchText" id="twotabsearchtextbox" placeholder="Search Products" >
                    <span class="input-group-btn">
                    <button class="btn btn-default btntr"  href="javascript:viod(0)" name="submit" id="submit" type="submit" type="button" ><span class="glyphicon glyphicon-search"></span></button>
                    </span>
                 </div>
               </form>

              </div>


               <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
                  <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
                  <div class="cnt-block ml38">
                     <ul class="list-unstyled list-inline ml0" >
                        <li class="dropdown dropdown-small mr3">
                           <a href="<?echo SITEMOBURL?>product/viewcart" class="dropdown-toggle position" data-hover="dropdown" id="headercart">
                            <i class="icon fa fa-shopping-bag bgicon"></i>&nbsp;<span class=" margin-left value" id="currentitemcount"></span></a>
                        </li>
                        <li class="dropdown dropdown-small mr2">
                           <?if($this->libsession->isSetSession('usrid')){?>
                           <a href="<?echo SITEMOBURL?>user/myaccount?name=wishlist" class="dropdown-toggle position1" data-hover="dropdown"> <i class="icon fa fa-heart bgicon" id="heart"></i>&nbsp;<span class=" margin-left value" id="wishlistcount"></span></i></a>
                           <?}else{?>
                           <a href="#login" class="dropdown-toggle position1" data-hover="dropdown" data-toggle="modal">
                           <i class="icon fa fa-heart bgicon"></i>&nbsp;<span class=" margin-left value" id="wishlistcount"></span></i></a>
                           <?}?>
                        </li>
                        <li class="dropdown dropdown-small height44" id="mainloginicon" >
                           <?if(!$this->libsession->isSetSession('usrid')){?>
                           <p class="text-center"><a href="#login" class="dropdown-toggle" data-hover="dropdown" data-toggle="modal"><span class="value">Log In</span> &nbsp;&nbsp;<i class="fa fa-user bgicon" aria-hidden="true"></i></a></p>
                           <?}else{?>
                           <?$user=explode(' ', trim($this->libsession->getSession('usrname')));?>
                           <a href="#" class="dropdown-toggle mr14" data-hover="dropdown" data-toggle="dropdown" ><span class="value">Hi,<?php echo substr($user[0],0,8);?></span><i class="fa fa fa-sort-desc position2" aria-hidden="true" ></i></a>
                           <ul class="dropdown-menu">
                              <li> <a href="<?php echo SITEMOBURL?>user/myaccount">My Account</a></li>
                              <!-- <li> <a href="<?php echo SITEMOBURL?>product/getorderbyuser">My Order</a> -->
                              <li><a href="<?php echo SITEMOBURL?>user/logout" id="logoutbutton">Log Out</a></li>
                           </ul>
                           <?}?>
                        </li>
                     </ul>
                     <!-- /.list-unstyled -->
                  </div>
               </div>

              <input type="hidden" autocomplete="off" name="searchText" id="twotabsearchtextbox1" placeholder="Search Products" onkeyup="suggestKeywords(this.value, 1);" value="<?php echo $searchText; ?>" oninput="test1()"/>
               <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
               <!-- /.top-cart-row -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.main-header -->
      <!-- ============================================== NAVBAR ============================================== -->
      <?php include('left_menu.php'); ?>
      <!-- /.header-nav -->
      <!-- ============================================== NAVBAR : END ============================================== -->

</div>
   </header>
   <!-- Modal HTML Log IN -->
   <?//echo  __DIR__; //die;?>
   <?require_once ( __DIR__ .'/login.php');?>
   <!--Modal Log In:End HTML -->
   <!-- Modal SIGN UP HTML -->
   <?include (__DIR__ .'/signup.php');?>
   <!-- Modal SIGN UP:END HTML -->
   <!-- Modal emi HTML -->
   <?require_once (__DIR__ .'/emi.php');?>
   <!-- Modal emi:END HTML -->
   <!--modal popup of review-->
   <?include (__DIR__ .'/reviewpopup.php');?>
   <!--modal popup of review:end -->

   <!-- ============================================== HEADER : END ============================================== -->
  <style type="text/css">
    .whitead{
      background:#fff ! important;
    }
    .ml0{
      margin-left:0px
    }
  </style>
<script>
function suggestKeywords(inputString, type){
    if(inputString.length <= 1) {
      $('#suggestions').fadeOut();
    }else{


      //alert(search_type);
      $('#searchText').addClass('load');
      //var term=jQuery('#search').val()
      $.ajax({
        type: "GET",
        //url: PREFIX_RUN+'autosuggest.php',
        url: "<?php echo SITEMOBURL?>product/autosuggestweb",
        data:  {term:inputString},
        success: function(data){ //alert(data);
          if(data == ''){
            $('#suggestions').fadeOut();
            $('#suggestionsList').hide('fast');
          }else if(data.length > 0) {
            $('#suggestions').fadeIn();
            $('#suggestionsList').show('fast');
            $('#suggestionsList').html(data);
            //$('#searchText').removeClass('load');

            var temp = data.split('|');
            $('#catid').val(temp[0]);
            $('#brandid').val(temp[1]);

            //
          }
        }
      });

    }
  }//suggestKeywords()
</script>
<script type="text/javascript">
  $(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
    e.preventDefault();
    var param = $(this).attr("href").replace("#","");
    var concept = $(this).text();
    $('.search-panel span#search_concept').text(concept);
    $('.input-group #search_param').val(param);
  });
});
</script>
<script type="text/javascript">

  jQuery(document).ready(function(){
      /*Top button Script*/
 jQuery(window).scroll(function() {
    if (jQuery(this).scrollTop()) {
        jQuery('#toTop').fadeIn();
    } else {
        jQuery('#toTop').fadeOut();
    }
});

jQuery("#toTop").click(function () {
   jQuery("html, body").animate({scrollTop: 0}, 1000);
});
})

jQuery('.selectdrp li').on('click', function(){
    jQuery('#search_param').val(jQuery(this).val());
})
</script>