<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Furtados</title>
    <link rel="stylesheet" href="<?php echo WEBCSS;?>bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo WEBCSS;?>hapinesssalestyle.css">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MGRL7HV');</script>
	<!-- End Google Tag Manager -->
	<!-- Global site tag (gtag.js) - Google Ads: 863719530 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-863719530"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'AW-863719530');
	</script>
</head>

<body>

    <div class="container-fluid">
        <div class="header header-bg1 mt-lg-4 mt-3">
		</div>
        
        <div class="bg-red mb-3 px-4 pt-3">
            <h4>Terms & Conditions</h4>
            <div class="row">
                <div class="col-12 col-lg-12">
                    <ul style="list-style: disc;" class="pl-2">
                        <li>The Furtados NO Cost EMI Promotion is a special offer made available on select products and offers consumers the added advantage of extending their payment for the product, effectively affording them a higher value product that they would be ready to invest in.</li>
						<li>Your No Cost EMI payment will show in your credit card statement as a lower amount than the order amount (reduced amount is equal to the interest charged). The interest on this reduced amount will be charged extra by your bank. The bank may also charge you GST on the interest amount. The total amount you pay to your bank over the perios of the EMI will be equal to your order amount (not including the GST on interest, if applicable).</li>
						<li>You can decide which tenure of No Cost EMI you opt for – 3, 6 or 9 months. A longer tenure results in a smaller monthly payment.</li>
						<li>The approval for the EMI payment is at the sole discretion of the bank / Credit Card issuer.</li>
						<li>Most items above Rs 5,000/- (Rs 6000 for Amex credit card) up to a maximum of Rs 500,000/- are available for selecting this offer. Applicability of this offer will be displayed on the product page of the product.</li>
						<li>Please note that the amount may show as a lump-sum amount in your credit card statement immediately after the transaction but will be converted by your bank to EMIs in a few days (depending on bank to bank), based on your date of purchase and credit card billing cycle.</li>
						<li>Furtados reserves the right to withdraw this offer at its sole discretion and without any prior intimation.</li>
					</ul>
                </div>
                
            </div>
        </div>
    </div>   
</body>

</html>