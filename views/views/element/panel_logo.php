 <div class="header-main-block1">
                        <div class="container">
                            <div id="container-fixed">
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="<?php echo SITEURL;?>" class="header-logo" style="margin-top:0px !important;"> <img src="<?php echo SITEIMAGES;?>logo.png" alt="" width="170" /></a>        
                                    </div>
                                    <div class="col-md-5">
                                        <div class="top-search-form pull-left">
                                            <form id="search_form" action="<?php echo site_url('search/');?>" method="post" onsubmit="return validate_search_text();">
                                                <input style="border-style:solid;border-width:1px; border-color: grey;" id="search" name='query' placeholder="Search for products, categories, brands etc." class="form-control" type="text">
                                                <button type="submit"><i class="fa fa-search" style="color: grey;"></i></button>
                                            </form> 
<script type="text/javascript">											
make_search_url = function(){
		var searchThis = jQuery("#search").val();
			if(searchThis!='')
			{
				//searchThis = encodeURIComponent(searchThis);
				searchThis = searchThis.replace(/[^a-zA-Z0-9-_]/g, ' ');
				jQuery("#search_form").attr('action','<?php echo site_url('search/');?>'+'/'+searchThis);
			}
		console.log(searchThis);
};

validate_search_text = function()
{
	make_search_url();
	var text = jQuery("#search").val();
	if(text!='')
			{
				return true;
			}
		return false;
}
</script>
                                        </div>        
                                    </div>
							<div class="col-md-4">
										<?php $this->load->view('element/temp_cart'); ?>
										<div class="top-icons">
										  <div class="pull-right" id="account-menu">
												<a class="account-menu top-icon" href="javascript:void(0);" title="Wishlist" onmouseover="showwishlist();"><i class="fa fa-heart color5"></i><span class="mywishlistcnt">0</span></a>
												<span id="snapwish"></span>
											</div>
											<?php if(!$this->Do_customer->isUserLogin()){ ?>
												<a href="javascript:void(0)" onclick="get_login_signup_pops('L');"><span class="signin" style="margin-left: 86px; margin-top: 16px;">SIGN IN</span></a>
											<?php } ?>
											<?php if($this->Do_customer->isUserLogin()){ ?>
												<?php $this->load->view('element/user_short_cut_v2'); #-- Update, Sanjay 14/oct/2014?>
											<?php } ?>
										</div>
										
										
										<?
											$CI =& get_instance();
											$CI->load->model('promotional_model');
											if($CI->promotional_model->isMagiClickOn())
												echo '<div class="pull-right promonbanner" >Magic Click is<br>On</div>';
											else if($CI->promotional_model->isHappyHourStart())
												echo '<div class="pull-right promonbanner" >Happy Hour is<br>On</div>';
											else if($CI->promotional_model->isSpinAndRunStart())
												echo '<div class="pull-right promonbanner" >Spin N Run is<br>On</div>';
										?>
										
										
										
                            </div>
                    </div>
            </div>
        </div>
</div>

<style>
a[title]:hover:after {
  content: attr(title);
  padding: 0px 10px;
  height:40px;
  line-height:40px;
  color: #fc5602;
  position: absolute;
  font-size:12px;
  right: -1px;
  top: -44px;
  white-space: nowrap;
  z-index: 99999;
  background:#fff;
  border:solid 1px #fc5602;
}
.promonbanner{
	width: 74px;
	height: 74px;
	background-color: #fc5602;
	color: #ffffff;
	text-align: center;
	font-size: 15px;
	top: 11px;
	position: absolute;
	right: -64px;
	border-radius: 35px;
	z-index: 1;
	line-height: 18px;
	padding-top: 10px;
}
</style>
<script type="text/javascript">
	
	/*var clock;
	$(document).ready(function() {
		var clock;
		clock = $('.clock').FlipClock({
			clockFace: 'DailyCounter',
			autoStart: false,
			callbacks: {
				stop: function() {
					$('.message').html('The clock has stopped!')
				}
			}
		});
		clock.setTime(220880);
		clock.setCountdown(true);
		clock.start();
	});
	*/
	
</script>
