<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$httphost='https://'.$_SERVER['HTTP_HOST'].'/';
$httpshost='https://'.$_SERVER['HTTP_HOST'].'/';
$docroot=$_SERVER['DOCUMENT_ROOT'];
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

//define("ROOTPATH", "https://www.furtadosonline.com/");
define("ROOTPATH",$docroot); #--Comment on live
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define("PRED",'CS');

/*define('RAZORPAYKEY', 'rzp_test_rkISVOsj1qt5QS');
define('RAZORPAYSECRET', 'qtzB5Pi4765vzhtNA1c9fI8g'); */

define('RAZORPAYKEY', 'rzp_live_Gc72gTVp5ZF2XY');
define('RAZORPAYSECRET', 'JQWf6WmVtQbTrOgMrvliEIq5');
define('RAZORPAYDISPCURRENCY', 'INR');

define('PAYUKEY', 'gJ8LBs'); //LIVE PAYU KEY
define('PAYUSALT', 'dWkjqstQ'); //LIVE PAYU SALT

//define('PAYUKEY', 'gtKFFx'); //TEST PAYU KEY
//define('PAYUSALT', 'eCwWELxi'); //TEST PAYU SALT

 #Update by sanjay, global variable for live. UNCOMMENT IT BEFORE LIVE
		##-- on 7/sep
//echo $base_url; 

define("ROOTURL", "https://www.furtadosonline.com/");
//define("ROOTURL",$httphost); #-- Comment before Live. COL
#define("SITEURL",ROOTURL.'spider-frontend/'); #-- Comment before Live. COL
#define("ROOTURL",'http://spidersmall.comindex.php/'); #-- For LIVE
define("ADMINURL",ROOTURL);
define("GENERALURL",ROOTURL);
define("SITEURL",ROOTURL);
define("SITEMOBURL",ROOTURL);
define("SITEMOBILEURL", ROOTURL);
define("SITEMOBURLWITHHTTPS", ROOTURL);
define("SPIDER_ADMIN",ROOTURL.'spider-admin/');
define("SPIDER_SELLER",ROOTURL.'spider-seller/');
//define("SITEIMAGES",SITEURL.'assets/images/');
define("SITEIMAGES",GENERALURL.'assets/images/');
define("SITE_DEFAULT_IMAGES",SITEIMAGES.'default/');
define("SITECSS",SITEURL.'assets/css/');
define("WEBCSS",SITEURL.'assets/web/css/');
define("SITEJS",SITEURL.'assets/js/');
define("WEBJS",SITEURL.'assets/web/js/');
define("WEBIMG",SITEURL.'assets/web/images/');
define("THUMBPATH",ROOTURL.'img/');
define("FILEBASEURL",ROOTURL.'upload/');
define("CLEARANCE_IMAGE_DB",GENERALURL.'uploads/clerance');
define("PROMOTION_IMAGE_DB",SITEMOBURL.'upload/promotion');
define("SITESTORE",ROOTURL.'uploads/store');
define("FILEBASEPATH",$docroot);
define("HTTPSURL",$httpshost);
//define("FILEBASEPATH",'/home/stagingfurtados/public_html/'); #-- Uncomment on live..
#define("PG_KEY","/var/www/spidermall/spider-frontend/Key/96030119.key"); #-- local
define("PG_KEY","Key/96030119.key"); #Live
define("PAGE_TITLE", "furtadosonline.com : ");


	##-- on 7/sep
/*Upload path for category*/
define("IMGCATPATH",'images/category/');
define("IMGBRANDPATH",'images/brand/');
define("IMGPRODUCTPATH",'images/product/');
define("IMGSELLERPATH",'images/seller/');
define("IMGBANNERPATH",'images/banner/');
define("IMGHOMEBANNER",'images/home_banners/');
define("IMGOFFERPATH",'images/offers/');
define("INVOICE_STORAGE",'invoice/sellers');

define('REFUND_ORDER_IMG_PATH','images/refund/');



#List of default image name.
define("IMAGEDEFAULT",SITEIMAGES.'mobimg/noimage1.png');
define("IMGBRANDDEFAULT",'no_image.gif'); #For brand


define('LOGIN_PAGE','customer/dologin'); #add by sanjay.

#--- Facebook APP details, sanjay account
//define('FB_APPID','510913729091795');
//define('FB_APPSECREAT','5fb4ed8822344992289a99173fb60638'); 
#--- Facebook APP details, Sandeep account
define('FB_APPID','144763512593473');
define('FB_APPSECREAT','af6440e279eaefe143e3153e48b26714');

define('GOOGLECLIENTID','947370773198-3br5q0bnvij33np121gcf7gjtaflv0dh.apps.googleusercontent.com');
define('GOOGLECLIENTSECRET','IyqvOyPwAATtOFMSJIZkQHTH');
define('GOOGLEAPPKEY', 'AIzaSyAcqqgeRnrt8eJvgM3ZcJ2fR5MkVPeXggI');

//define('ENVIRONMENT','development');
define("SQLLOG",ROOTPATH.'/logs'); #--Comment on live


#---- Mail and sms config , 8/Dec
define('DUMMY_USER_EMAIL','spiderusers@mailcatch.com');
define('DUMMY_ADMIN_EMAIL','spideradmin@mailcatch.com');
define('DUMMY_SELLER_EMAIL','spiderseller@mailcatch.com');
define('NO_REPLY','no-reply@spidersmall.com');
define('SPIDER_FROM_EMAIL','support@spidersmall.com');
define('DUMMY_SMS_NUMBER','8689966528');
#define('SPIDER_ADMIN_EMAIL','dev.paul@lumeg.com'); #-- Always used in Cc
define('SPIDER_ADMIN_EMAIL','admin@spidermall.com');
define('ON_PRODUCTION_MODE',false); #-- Testing mode activated, deactivated.

#---- Mail and sms config , 8/Dec
#--SEO Contants--#####

define("metahome","Online shopping of musical instruments 
- Browse our range of electric, acoustic guitars and yamaha, casio 
keyboards, pianos and much more available in India at ✓Best Prices with 
✓Quick Delivery.");


##-----#####

#-- Combo product price limitation
define('COMBO_AMOUNT',50); #-- In percentage
/* End of file constants.php */
/* Location: ./application/config/constants.php */

/*MEMCACHED*/

define("MEMECACHE_IP", "127.0.0.1");
define("MEMECACHE_PORT", 11211);
define("MEMECAHCE_PREFIX", "LIVE_");

define('FPRO','FPRO'); #-- product caching	.
define('FCAT','FCAT'); #-- category caching
define('FATTR','FATTR'); #-- Attributes caching.
define('FOTH','FOTH'); #-- Other element.
define('HW',300); #-- LONG CACHING.
define('MW',1200); #-- MEDIUM CACHING.
define('LW',2400); #-- LOW CACHING.
define('DTime',date('Y-m-d H:i:s'));

define('ASSETURL', ''.SITEURL.'assets/');
define ('DWIMGLIST','185','193');
define ('DWIMGDTL','330','193');
define('DWBANNER','1200','193'); 
define('LIMIT','24'); 
define('SIMPER','20');
define('MIN_PRICE_FOR_EMI_5_BANK',5000);
define('MIN_PRICE_FOR_SHIPPING', 2000);
/*MEMCACHED*/
# Email
//DEFINE('SOMEIMG', '<img src="http://202.143.96.124/assets/images/emailer_images/logo_1.jpg">');
//echo SOMEIMG; die;
define("GENERIC_EMAIL", "response@furtadosonline.com");	
define("DOMAIN_NAME", "furtadosonline.com"); 
define("RESPONSE_MAIL", "response@furtadosonline.com");

define("DOMAIN_PATH", "furtadosonline.com");		
define("MAILER_HEADER","<table width='600' border='0' style='border:2px solid #9b9999;background-color:#ffffff;font-family:Calibri;' cellpadding='0' cellspacing='0'><tr bgcolor='#ffffff' style='border-bottom:5px solid #b20202;'><td width='252' align='left' style='padding:5px;height:100px;border-bottom:5px solid #b20202;'><a href='".SITEMOBURL."' target='_blank'>
	<img src='https://www.furtadosonline.com/assets/images/emailer_images/logo_1.jpg' border='0'  />
	</a></td><td width='348' align='right' valign='middle' style='border-bottom:5px solid #b20202;color:#5f5f5f;font-size:12px;font-weight:bold;padding-right:5px;'>&nbsp;</td></tr><tr bgcolor='#ffffff' style='border-bottom:5px solid #b20202;'><td colspan='2' align='right' style='padding-right:5px;'><a href='".SITEMOBURL."user/myaccount' target='_blank'><img src='https://www.furtadosonline.com/assets/images/emailer_images/mailer_h_my_ac.jpg' border='0'  /></a><a href='".SITEMOBURL."product/getorderbyuser' target='_blank'><img src='https://www.furtadosonline.com/assets/images/emailer_images/mailer_h__my_order.jpg' border='0'  /></a></td></tr><tr><td colspan='2' style='/*padding:5px 5px;*/color:#595959;font-family:Arial;font-size:12px;font-weight:normal'>");

define("MAILER_FOOTER", " </td>
							</tr>
							<tr>
							<td colspan='2' style='padding:5px 5px;color:#595959;font-family:Arial;font-size:12px;font-weight:normal; height=10px;' height='10'>&nbsp;</td>
						  </tr>
							<tr>
							<td colspan='2' style='padding:5px 5px;color:#595959;font-family:Arial;font-size:12px;font-weight:normal'>
								Regards,<br><br>
								Furtadosonline Team<br><br>
								
							</td>
						  </tr>
						 
						  <tr bgcolor='#f5f4f4' style='border-bottom:5px solid #b20202;'>
								<td colspan='2' align='left' height='65'>
									<table width='100%' border='0' cellspacing='0' cellpadding='0' style='font-size:14px;font-family:Calibri;'>
									  <tr>
										<td width='76%' valign='middle'>
											<table width='100%' border='0' cellspacing='0' cellpadding='0'  style='font-size:16px;font-family:Calibri;'>
											  <tr>
												<td height='30' align='center' style='border-bottom:1px dotted #aaaaaa;font-family:Calibri;'>
													&bull; <a href='".SITEMOBURL."privacyPolicy.php' style='font-size:14px;font-family:Calibri;color:#595959;text-decoration:none;'>Privacy Policy</a>  &nbsp; &bull; <a href='".SITEMOBURL."terms.php' style='font-size:14px;font-family:Calibri;color:#595959;text-decoration:none;'>Terms & Conditions</a>
												</td>
											  </tr>
											  <tr>
												<td align='center' height='30' style='font-size:14px;font-family:Calibri;color:#595959;text-decoration:none;'>For any query or assistance, feel free to <a href='".SITEMOBURL."contactUs.php' style='font-size:14px;font-family:Calibri;color:#595959;text-decoration:underline;'>Contact Us</a></td>
											  </tr>
											</table>
							
										</td>
										<td width='24%' align='right' valign='middle' style='padding-top:5px;padding-right:15px;font-size:14px;'>
											Follow us on<br /><br />
											<a href='https://www.facebook.com/FurtadosMusic' target='_blank'><img src='https://www.furtadosonline.com/assets/images/emailer_images/fb_mailer.jpg'' border='0' alt='Facebook' title='Facebook' /></a>&nbsp;
											<a href='https://www.youtube.com/user/furtadosmusicindia' target='_blank'><img src='https://www.furtadosonline.com/assets/images/emailer_images/youtube_mailer.jpg' border='0' alt='You Tube' title='You Tube' /></a>&nbsp;
											<a href='https://twitter.com/furtados' target='_blank'><img src='https://www.furtadosonline.com/assets/images/emailer_images/tw_mailer.jpg' border='0' alt='Twitter' title='Twitter' /></a>
										</td>
									  </tr>
									</table>
								</td>  
						  </tr>
						  <tr>
							<td height='20' colspan='2'><p style='padding:5px 5px;color:#595959;font-family:Arial; font-size:10px;'>Trouble viewing this email or any issue, please report this to <a href='mailto:".RESPONSE_MAIL."' style='color:#464F4B;font-size:10px;'>".RESPONSE_MAIL."</a></p>
</td>
						  </tr> 
						</table>");

?>
