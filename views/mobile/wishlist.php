<style type="text/css">
	.badge{background: #d32f2f !important;
    border-radius: 50% !important;
    color: #fff !important;
    height: 15px !important;
    width: 15px !important;
    position: absolute !important;
    top: -9px;
    right: 18px !important;
    font-size: 8px !important;
    text-align: center !important;
    line-height: 12.5px !important;
    border: 2px solid #fff !important;
    padding: 0px 0px !important;
}
</style>


<div class="top-logo">
	<div class="container" style="padding:0;">
		<div class="row">
			<div class="col-md-12">
				<div class="col-xs-2">
					<!--<a href="#" onclick="window.history.go(-1); return false;"><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>-->
					<a href="#" onclick="window.history.go(-1); return false;" >
					<!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
					<i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
					</a>
				</div>
				<?
				if($resp){
				$cnt =count($resp);
				}
				if($cnt >0){
				$cnt=$cnt;
				}else{
				$cnt=0;	 
				}?>
				<div class="col-xs-7 Favourite_list">Wishlist  (<?echo $cnt;?>)</div>
				<div class="col-xs-3" style="margin-top:10px;">
					<div class="col-xs-12">
						<div class="col-xs-4 ">
							<a href="<?php echo SITEMOBURL;?>product/viewcart"><i class="fa fa-shopping-cart" aria-hidden="true">
								<span class="badge badge-notify my-cart-badge" id="currentitemcount"></span></i></a>
						</div> 
						<div class="col-xs-4">  
							<div class="dropdown" style="float:right;">
								<button class="dropbtn"><i class="fa fa-power-off" aria-hidden="true"></i></button>
								<div class="dropdown-content">
									<a href="<?php echo SITEMOBURL?>user/myaccount">My Profile</a>
									<a href="<?php echo SITEMOBURL?>product/getorderbyuser">My Order</a>
									<a href="<?php echo SITEMOBURL?>user/logout">Log Out</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--reviews-->
<?if($resp){
	foreach($resp as $row){?>
<div class="listingtab">
   <div class="">
      <div class="ptc">
         <table width="100%" class="listtb">
            <tbody>
               <tr>
                  <td width="90px" class="listtd" style="border-right: 1px solid #ffffff">
                     <div class="listimg" style="width: 90px; height: 120px;">
                      <?if($row['image']){?>
								<img src="<?php echo $row['image'];?>" width="100%">
							<?}else{?>
								<img src="<?php echo SITEIMAGES;?>mobimg/noimage1.png" width="100%">
							<?}?>
                     </div>
                  </td>
                  <td style="border-right: 1px solid #ffffff">
                     <div class="dtext" style="-webkit-line-clamp: 2; -webkit-box-orient: vertical;">
                        <div>
                           <div class="titlecat" style="margin-top:0px"><?php echo $row['proname'];?></div>
                        </div>
                     </div>
                     <!--  hardcored rating 
					 <div class="txftdos ml" style="display: <?php echo ($value['rating'] && $value['rating'] > 0) ? '' : 'none'; ?>">
                        <span class="ratnig">
                           <div class="rates">
                              <div class="ratningbg" style="background-color: rgb(55, 190, 95);">
                                 <span>4.2</span>
                                 <span>
                                 <img class="starbg" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI1MTIiIGhlaWdodD0iNTEyIiB2aWV3Qm94PSIwIDAgMzA2IDMwNiI+PHBhdGggZmlsbD0iI2ZmZiIgZD0iTTE1MyAyMzAuNzc1bDk0LjM1IDY4Ljg1LTM1LjctMTEyLjIgOTQuMzUtNjYuM0gxOTEuMjVMMTUzIDYuMzc1bC0zOC4yNSAxMTQuNzVIMGw5NC4zNSA2Ni4zLTM1LjcgMTEyLjJ6Ii8+PC9zdmc+">
                                 </span>
                              </div>
                              <span class="f8RGEP"> (36)</span>
                           </div>
                        </span>
                     </div>
					 <br>-->
                     <div class="catdetails" style="margin-bottom: 5px;">
                        <span class="catsubt">
                        	<?if($row['dicountprice']){?>
									<i class="fa fa-rupee" style="font-size:12px;margin-right: 2px;"></i>
									<?php echo $this->libsession->priceformat($row['dicountprice'])?>
							<?}else{?>
							<?php echo $this->libsession->priceformat($row['price'])?>
							<?}?>
                        </span>
                     </div>
                        <span class="">
	                     	<?if($row['stock']=="1"){?>
									<p class="stock" style="padding-bottom: 8px;">In stock</p>
								<?}else{?>
									<p class="stock" style="padding-bottom: 8px;color: #cd0029;">Out of stock</p>
								<?}?>
						</span>

						<span class="">
								<?if($row['stock']=='1'){?>
										<p class="adcrt" onclick="addtocart(<?php echo $row['proid'];?>);">ADD TO CART</p>
									<?}else{?>
										<p class="add_to_card_color" disabled>ADD TO CART</p>
									<?}?>	
						</span>


                  </td>
                  <td width="35px" style="border-right: 1px solid #ffffff">
                     <div class="">
                        <div>
                           <div class="crtdelet">
                              <div class="col-xs-4 delete"  onclick="removefromwishlist(<?php echo $row['proid'];?>);">
									<img class="BfsEA4" alt="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjNjE2MTYxIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTQxMy43IDEzMy40Yy0yLjQtOS00LTE0LTQtMTQtMi42LTkuMy05LjItOS4zLTE5LTEwLjlsLTUzLjEtNi43Yy02LjYtMS4xLTYuNi0xLjEtOS4yLTYuOC04LjctMTkuNi0xMS40LTMxLTIwLjktMzFoLTEwM2MtOS41IDAtMTIuMSAxMS40LTIwLjggMzEuMS0yLjYgNS42LTIuNiA1LjYtOS4yIDYuOGwtNTMuMiA2LjdjLTkuNyAxLjYtMTYuNyAyLjUtMTkuMyAxMS44IDAgMC0xLjIgNC4xLTMuNyAxMy0zLjIgMTEuOS00LjUgMTAuNiA2LjUgMTAuNmgzMDIuNGMxMSAuMSA5LjggMS4zIDYuNS0xMC42bS0zNC4zIDQyLjZoLTI0Ni44Yy0xNi42IDAtMTcuNCAyLjItMTYuNCAxNC43bDE4LjcgMjQyLjZjMS42IDEyLjMgMi44IDE0LjggMTcuNSAxNC44aDIwNy4yYzE0LjcgMCAxNS45LTIuNSAxNy41LTE0LjhsMTguNy0yNDIuNmMxLTEyLjYuMi0xNC43LTE2LjQtMTQuNyIvPjwvc3ZnPg==" src="data:image/svg+xml;base64,PHN2ZyBmaWxsPSIjNjE2MTYxIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTQxMy43IDEzMy40Yy0yLjQtOS00LTE0LTQtMTQtMi42LTkuMy05LjItOS4zLTE5LTEwLjlsLTUzLjEtNi43Yy02LjYtMS4xLTYuNi0xLjEtOS4yLTYuOC04LjctMTkuNi0xMS40LTMxLTIwLjktMzFoLTEwM2MtOS41IDAtMTIuMSAxMS40LTIwLjggMzEuMS0yLjYgNS42LTIuNiA1LjYtOS4yIDYuOGwtNTMuMiA2LjdjLTkuNyAxLjYtMTYuNyAyLjUtMTkuMyAxMS44IDAgMC0xLjIgNC4xLTMuNyAxMy0zLjIgMTEuOS00LjUgMTAuNiA2LjUgMTAuNmgzMDIuNGMxMSAuMSA5LjggMS4zIDYuNS0xMC42bS0zNC4zIDQyLjZoLTI0Ni44Yy0xNi42IDAtMTcuNCAyLjItMTYuNCAxNC43bDE4LjcgMjQyLjZjMS42IDEyLjMgMi44IDE0LjggMTcuNSAxNC44aDIwNy4yYzE0LjcgMCAxNS45LTIuNSAxNy41LTE0LjhsMTguNy0yNDIuNmMxLTEyLjYuMi0xNC43LTE2LjQtMTQuNyIvPjwvc3ZnPg==" style="width: 20px; height: 20px;">
								</div>
                              <div class="lw3t_m">
                                 <div class="_3yFKkI"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </td>
               </tr>
            </tbody>
         </table>
      </div>
   </div>
</div>
<?}}else{?> 
	<div class="Favourite">
		<div class="Favourite-no" style="background: white;height: 0; margin: 200px 25px;">
			<center><p>There are currently no products in the Wishlist !!</p></center>
		</div> 
	</div>
	<!--<div class="col-xs-12">
	No product present in the wishlist !!
	</div>-->
<? }?>





<script>
function addtocart(ele){
	proid=ele;
	jQuery.ajax({
		url:'<?php echo SITEMOBURL?>product/storeproid',
		type:'POST',
		data:{proid:proid},
		success:function(data)
		{   var response = jQuery.parseJSON(data);
		   
			if(response.error=='false'){
			     alert('Product successfully added to cart');
			      removefromwishlist(proid); 
			     ajax_getCartCount();
			      window.location.reload();
				}else{
				 alert('error in adding product to cart');	
					
				}
			
		}

	});
	
}

function removefromwishlist(ele) {
	var pid=ele;
   
    jQuery.ajax({
		url:'<?php echo SITEMOBURL?>user/removefromwishlist',
		type:'POST',
		data:{pid:pid},
		success:function(data)
		{   var response = jQuery.parseJSON(data);
		  
			if(response.error=='false'){
			     alert('Product  removed successfully from Wishlist'); 
			     window.location.reload();
				}else{
				 alert('Error in removing  product from Wishlist');	
				}
			
		}

	});
	
    
}
</script>
<script>
jQuery(document).ready(function () {
ajax_getCartCount();
});  

function ajax_getCartCount(){
	
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>product/getcartcount', 
           type:'POST'
          
        }).done(function(resp){
        	if (resp.trim().length > 0) {
        		
          var response = jQuery.parseJSON(resp);
            if(response.status==1)
            {
				 if(response.cartcount > 0){  
                   jQuery('#currentitemcount').html(response.cartcount); 
                 }
          
            }
            else
            {
                  if(response.cartcount > 0){  
                     jQuery('#currentitemcount').html(response.cartcount); 
                   } 
            }
          }  
          return false;

      });
}	
</script>