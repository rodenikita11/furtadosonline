<?php 
if(is_array($hotdeals)){
	$resp['product']=$hotdeals['product'];
	$name = 'Hot Deals';
} else if(is_array($newarrivalbooks)){
	$resp['product']=$newarrivalbooks['product'];
	$name = 'Newly Arrival Books';
} else if(is_array($newarrivals)){
	$resp['product']=$newarrivals['product'];
	$name = 'New Arrivals';
}else {
	$resp['product']=$resp['product'];
	$name = 'Featured Products';
}
?>

<div class="Container container-fluid bg-1 text-center search-res">
	<div class="col-xs-12 Drums_and_bass_top">
		<div class="col-xs-7 Drums_and_bass">
			<p class="view"><?php echo $name;?></p>
		</div>
		<div class="col-xs-5">
			<?if($resp['product']){?>
				<p class="item"><?echo count($resp['product']);?> items</p>
			<?}?>
		</div>
	</div>
</div>
<div class="Electric  bg-1 text-center">
	<div class="row">
		<div class="col-xs-12 flex-container" id="listdata">
			<?if($resp['product']){
				foreach ($resp['product'] as $key=>$value){ 
					include('include/list_product.inc');
				}
			} else { ?>
				<div class="">No Products Found...</div>
			<?php } ?>
		</div>
	</div>
</div>
<style type="text/css">
   p.view {
    text-transform: uppercase;
    font-size: 14px;
    margin-top: 1px;
   }
</style>