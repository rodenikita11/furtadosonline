<!DOCTYPE html>
<html>
    <head>
    <title> :: Furtatose ::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="<?php echo ASSETURL; ?>css/bootstrap.css" rel="stylesheet" media="screen">		
		<link href="<?php echo ASSETURL; ?>css/font-awesome.min.css" rel="stylesheet" media="screen">		
		<link href="<?php echo ASSETURL; ?>css/flexslider.css" rel="stylesheet" media="screen">
		<link href="<?php echo ASSETURL; ?>css/owl.carousel.css" rel="stylesheet" media="screen">
		<link href="<?php echo ASSETURL; ?>css/style.css" rel="stylesheet" media="screen">
		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1819704241636062');
		fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=1819704241636062&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
    </head>
    <body>
		<!-- main wrapper -->
		<div class="main_wrapper">
			<!-- HEADER -->
			<header class="header_sec">
				<section class="logo_cart">
					<div class="container">
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3">
								<div class="logo">
									<a href="#"><img src="<?php echo ASSETURL;  ?>images/logo.jpg" alt=""></a>								             
								</div>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-9">
								<div class="login_cart_in">
									<ul>
										<li>
											<div class="top_seach">
												<input id="search" class="T_input" type="search" name="search" placeholder="Enter your search term...">
												<input class="T_search" type="submit" value="">												
											</div>
										</li>
										<li>
											<div class="login_acc">
												<a href="javascript:void(0)">
													<span></span>
													<strong><i>Login</i> My Account</strong>
													<em></em>
												</a>
											</div>
										</li>
										<li>
											<div class="myList">
												<a href="javascript:void(0)">
													<span></span>
													<strong><i>My</i> List</strong>
													<em></em>
												</a>
											</div>
										</li>
										<li>
											<div class="my_cart">
												<a href="javascript:void(0)">
													<span>3</span>
													<strong><i>My</i> Cart</strong>
													<em></em>
												</a>
											</div>
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<section class="navigation">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<nav class="main_navigation">
									<ul>
										<li><a href="">Home</a></li>
										<li><a href="">Musical Instruments <em></em></a></li>
										<li><a href="">Music Books <em></em></a></li>
										<li><a href="">Pro Audio <em></em></a></li>
										<li><a href="">Clearance Sale</a></li>
										<li><a href="">Our Stores</a></li>
									</ul>
								</nav>
								<div class="nav_telephone">
									<a href="tel:2242875050"> +91 22 - 42875050/60</a>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</section>
			</header>