<?php 
class Libshippingcost{
             public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_shipcost');
             $this->ci = $CI;

			// $this->price_master_array = array("1"=>array("18","98.25","75.60"), 
														 // "2"=>array("12","98.25","75.60"), 
														 // "3"=>array("12","52.4","75.60"), 
														 // "4"=>array("12","52.4","75.60"), 
														 // "5"=>array("11","58.95","75.60"), 
														 // "6"=>array("11","58.95","75.60"), 
														 // "7"=>array("","",""), 
														 // "8"=>array("","",""), 
														 // "9"=>array("","",""), 
														 // "10"=>array("","","")
												  	// );
			 $this->price_master_array = array("1"=>array("20","122.81","95.00"), 
									 "2"=>array("22.5","122.81","95.00"), 
									 "3"=>array("15","65.5","95.00"), 
									 "4"=>array("15","65.5","95.00"), 
									 "5"=>array("13.75","73.68","95.00"), 
									 "6"=>array("22.5","73.68","95.00"), 
									 "7"=>array("","",""), 
									 "8"=>array("","",""), 
									 "9"=>array("","",""), 
									 "10"=>array("","","")
								);
             $this->freight_master_array = array("Normal"=>array("min_weight"=>"10",
																			 "max_weight"=>"10000",
																			 "airway_bill"=>"100",
																			 "fuel_surcharge"=>"29",
																			 "freight_on_value"=>"0.2",
																			  "service_tax"=>"18"
																		),
															  "Express"=>array("min_weight"=>"10",
																			 "max_weight"=>"10000",
																			 "airway_bill"=>"100",
																			 "fuel_surcharge"=>"70",
																			 "freight_on_value"=>"0.2",
																			  "service_tax"=>"18"
																		), 
															  "DP"=>array("min_weight"=>"0.500",
																		  "max_weight"=>"3",
																		  "airway_bill"=>"0",
																		  "fuel_surcharge"=>"70",
																		  "freight_on_value"=>"0",
																		  "service_tax"=>"18"
																		 )
														  );

		 }
		 
		 protected function log_fileDetails($fun_name, $query){
			$this->log = '------------------FileName: Libshippingcost.php - Function Name: '.$fun_name.'-----'.date("F j, Y, g:i a").'--------------------'.PHP_EOL;
			$this->log = $this->log . json_encode($query).PHP_EOL; // appending the query
			$this->logpath = APPPATH . 'logs/shipping_queries-' . date('Y-m-d') . '.log';
			error_log($this->log, 3, $this->logpath);
		 }
		 
		 public function calculateshippingcost($param){
			if($param['orderinvoice_no']){
				$this->log_fileDetails('calculateshippingcost-orderid', $param['orderinvoice_no']);
			}
		 	if(!$param['shippingmode'])
		 		return false;
			//$postState, 0, 0, $_SESSION['CART_PRODUCTS'], 'DP',, $MIN_PRICE_FOR_SHIPPING
			$param['cart']=$_SESSION['cart'];
			$param['usrid']=$_SESSION['usrid'];
			$param['shippingmode']=$param['shippingmode'];
			$postState = $this->ci->Do_shipcost->getstate($param);
			/*
			check to see if any coupon code is applied
			if yes then considered discountprice from the session cart	
			to calculate the final amount
			*/
			$resp['postState']=$postState[0]['state'];
			$shippingMode=$param['shippingmode'];
				if($zone == 0){ // buggy code
						
						$reszone = $this->ci->Do_shipcost->getzone($postState);
						
						if($reszone){
							//$selZoneRow = $selZone->fetch_array();
							$zone = $reszone[0]['zoneId'];
							//print_R($zone); die; 
							if($pincode == 0){  $pincode = $reszone[0]['pincode']; }
						}
					}
				

				// IF regular Product Price > 1000 (Free Shipping)
			$today = date('Y-m-d H:i:s');
			
			$incSC = 0;
			$applicableP = 0; $applicableW = 0;  // Promotional Product (include)
			$notApplicableP = 0; $notApplicableW = 0;   // Regular Product + Promotional Product (exclude)
			$totalPrice = 0; $totalWeight = 0;
			$cartprice=0;
			
			foreach($param['cart'] as $key => $value){
				//print_r($value);	
				$selPPSql = $this->ci->Do_shipcost->getprodetail($value);
				//print_r($selPPSql); die;
				if(!empty($selPPSql)){
					//$selPPRow = $selPP->fetch_array();
					
					$pid = $selPPSql[0]['promotionId'];
					$cid = $selPPSql[0]['clearanceId'];
				}else{
					$pid = 0; $cid = 0;
				}


					
				$productid=($value['pid']) ? $value['pid'] : $value['proid'];
				
				$productprice = $this->ci->Do_shipcost->getproductprice($productid);
				

				$onlinePrice=($_SESSION['coupon_code_applied']) ? $value['price'] : $productprice[0]['onlinePrice'];
				$discountPrice=($_SESSION['coupon_code_applied']) ? $value['dicountprice'] : $productprice[0]['discountPrice'];
				$weight=$productprice[0]['proweight'];
				
				if($onlinePrice > $discountPrice){
					$price=($_SESSION['coupon_code_applied']) ? $value['dicountprice'] : $discountPrice;
				}else{
					$price=($_SESSION['coupon_code_applied']) ? $value['price'] : $onlinePrice;
				}
				$cartprice += ($price * $value['qty']);
				
					if($cid != 0){
							if($cid == 1){
								$applicableP += round($price * $value['qty']);
								$applicableW += $weight * $value['qty'];	
							}else if($selPRow['shippingstatus'] == 2){
								$notApplicableP += round($price * $value['qty']);	
								$notApplicableW += $weight * $value['qty'];							
							}	
						
						}else if($pid != 0){
							
							$getpromotiondtl = $this->ci->Do_shipcost->getpromotiondtl($pid);
							
							if(!empty($getpromotiondtl)){
								
								if($getpromotiondtl[0]['shippingstatus'] == 1){
									
									$applicableP += round($price * $value['qty']);	
									$applicableW += $weight * $value['qty'];	
								}else if($getpromotiondtl[0]['shippingstatus'] == 2){
									
									$notApplicableP += round($price * $value['qty']);	
									$notApplicableW += $weight * $value['qty'];								
								}
							}else if($shippingMode == 'Express'){
									$applicableP += round($price * $value['qty']);	
									$applicableW += $weight * $value['qty'];		
							}else if($shippingMode == 'Normal' || $shippingMode == 'DP'){
								$applicableP += round($price * $value['qty']);	
								$applicableW += $weight * $value['qty'];
							}
						}else{
							$notApplicableP += round($price * $value['qty']);
							$notApplicableW += $weight * $value['qty'];	
							
						}
					
				} // for ends here
				if($notApplicableW > $this->freight_master_array['DP']['max_weight']){
					$resp['showexp']=1;
				}else{
					$resp['showexp']=0;
				}
				$min_price_for_shipping=MIN_PRICE_FOR_SHIPPING;
				//echo $min_price_for_shipping; die;
				$shippingMode=$param['shippingmode'];
				if($cartprice < MIN_PRICE_FOR_SHIPPING && ($shippingMode == 'Normal' || $shippingMode == 'DP')){
					if($shippingMode == 'Normal' || $shippingMode == 'DP'){
						if($notApplicableP < $min_price_for_shipping){
							//$totalPrice	= ceil($applicableP);
							$totalPrice	= ceil($applicableP + $notApplicableP);
							$totalWeight = $applicableW + $notApplicableW;
						}else{
							//die('here');
							$totalPrice= ceil($applicableP);
							$totalWeight= $applicableW;
							//$totalWeight = $applicableW + $notApplicableW;
						//echo $totalPrice.'<br>'.$totalWeight; die;
						
						}
					}else if($shippingMode == 'Express'){
						$totalPrice	= ceil($applicableP + $notApplicableP);
						$totalWeight = $applicableW + $notApplicableW;
						//$totalWeight = $applicableW;
	                  // echo 	$totalPrice .'<br>'. 	$totalWeight; die;			
					}
					//echo $totalPrice.'<br>'.$totalWeight; die;
					if($totalWeight > 0 && $totalWeight <= 0.499){ $totalWeight = 0.5; }
					if($totalWeight > 0.5 && $totalWeight <= 0.999){ $totalWeight = 1; }
					if($applicableW > 0 && $applicableW <= 0.499){ $applicableW = 0.5; }
					if($applicableW > 0.5 && $applicableW <= 0.999){ $applicableW = 1; }
	               
				   	if($shippingMode != 'Express'){
						if($totalWeight == 0 ){
							$shippingMode = 0;
						}else if($totalWeight <= $this->freight_master_array['DP']['max_weight']){
							$shippingMode = 'DP';
						}else{
							$shippingMode = 'Normal';
						}
				   	}else{
					   $shippingMode = 'Express';
					}
				    
					$resp['totalWeight']=$totalWeight;
					
					if($totalPrice == 0 || $totalWeight == 0){
						
					}
					
					switch($shippingMode){
						case "Normal" : $key_val = 0; 	break;
						case "Express" : $key_val = 1; 	break;
						case "DP" : $key_val = 2; 		break;
					}
					
					$rate = $this->price_master_array[$zone][$key_val];
					
					switch($shippingMode){
						case 'Normal' : 
						case 'Express' :  //$applicableW = ($applicableW < 10) ? 10 : $applicableW; break;		// Min 10 Kg
											if($applicableW < 10 && $applicableW != 0){
												$applicableW = 10;
											}else{
												$applicableW = 	$totalWeight;
											}
											break;	
												// Min 10 Kg
						case 'DP' : //$applicableW = ($applicableW < 1) ? 1 : $applicableW; 
									if($applicableW >= 0 && $applicableW <= 0.499){ $applicableW = 0.5; } 
									if($applicableW > 0.5 && $applicableW <= 0.999){ $applicableW = 1; } 
									break;	// Min 1 Kg
					}
				
					$final_amount = 0;
					
					$tot = $applicableW * $rate;
					//echo $tot; die;
					$ab = $this->freight_master_array[$shippingMode]["airway_bill"];
					//echo $ab; die;				
					$freight_cost = $tot + $ab;
					//echo $freight_cost; die;
					if($this->freight_master_array[$shippingMode]["fuel_surcharge"] == 0){
						$fuel_surcharge = 0;
					}else{
						$fuel_surcharge = round($freight_cost * ($this->freight_master_array[$shippingMode]["fuel_surcharge"] / 100));
					} 
		
					if($this->freight_master_array[$shippingMode]["freight_on_value"] == 0){
						$freight_on_value = 0;	
					}else{
						
						$freight_on_value = $applicableP * ($this->freight_master_array[$shippingMode]["freight_on_value"] / 100);
						$freight_on_value = round($freight_on_value,2);
						
						if($freight_on_value < 100){ $freight_on_value = 100; }
					}

					$t_freight = $freight_cost + $fuel_surcharge + $freight_on_value;

					//echo 'ggiuagsfiua'.$t_freight; die;
					
					if($this->freight_master_array[$shippingMode]["service_tax"] == 0){
						$service_tax = 0;
					}else{
						$service_tax = $t_freight * ($this->freight_master_array[$shippingMode]["service_tax"] / 100);
						$service_tax = round($service_tax,2);
					}
					
					$resp['total_shipping_cost']=ceil($t_freight + $service_tax);
				}else if($cartprice > MIN_PRICE_FOR_SHIPPING && ($shippingMode == 'Express')){
					$totalPrice	= ceil($applicableP + $notApplicableP);
					$totalWeight = $applicableW + $notApplicableW;

					if($totalWeight == 0){
						$shippingMode = 0;
					}else{
						$shippingMode = 'Express';
					}
				    
					$resp['totalWeight']=$totalWeight;
					
					if($totalPrice == 0 || $totalWeight == 0){
						
					}
					
					$key_val = 1;
						
					$rate = $this->price_master_array[$zone][$key_val];
					
					switch($shippingMode){
						case 'Normal' : 
						case 'Express' :  //$applicableW = ($applicableW < 10) ? 10 : $applicableW; break;		// Min 10 Kg
											if($applicableW < 10 && $applicableW != 0){
												$applicableW = 10;
											}else{
												$applicableW = 	$totalWeight;
											}
											break;	
												// Min 10 Kg
						case 'DP' : //$applicableW = ($applicableW < 1) ? 1 : $applicableW; 
									if($applicableW >= 0 && $applicableW <= 0.499){ $applicableW = 0.5; } 
									if($applicableW > 0.5 && $applicableW <= 0.999){ $applicableW = 1; } 
									break;	// Min 1 Kg
					}
				//echo 'applicableW='.$applicableW; die;
					$final_amount = 0;
					
					$tot = $applicableW * $rate;
					//echo 'tot---'.$tot; die;
					$ab = $this->freight_master_array[$shippingMode]["airway_bill"];
					//echo $ab; die;				
					$freight_cost = $tot + $ab;
					//echo $freight_cost; die;
					if($this->freight_master_array[$shippingMode]["fuel_surcharge"] == 0){
						$fuel_surcharge = 0;
					}else{
						$fuel_surcharge = round($freight_cost * ($this->freight_master_array[$shippingMode]["fuel_surcharge"] / 100));
					} 
		
					if($this->freight_master_array[$shippingMode]["freight_on_value"] == 0){
						$freight_on_value = 0;	
					}else{
						//$freight_on_value = $totalPrice * ($freight_master_array[$shippingMode]["freight_on_value"] / 100);
						$freight_on_value = $applicableP * ($this->freight_master_array[$shippingMode]["freight_on_value"] / 100);
						$freight_on_value = round($freight_on_value,2);
						
						if($freight_on_value < 100){ $freight_on_value = 100; }
					}
					
					$t_freight = $freight_cost + $fuel_surcharge + $freight_on_value;

					//echo 'ggiuagsfiua'.$t_freight; die;
					
					if($this->freight_master_array[$shippingMode]["service_tax"] == 0){
						$service_tax = 0;
					}else{
						$service_tax = $t_freight * ($this->freight_master_array[$shippingMode]["service_tax"] / 100);
						$service_tax = round($service_tax,2);
					}

					$resp['total_shipping_cost']=ceil($t_freight + $service_tax);

				}else if($cartprice < MIN_PRICE_FOR_SHIPPING && ($shippingMode == 'Express')){
					$totalPrice	= ceil($applicableP + $notApplicableP);
					$totalWeight = $applicableW + $notApplicableW;

					if($totalWeight == 0){
						$shippingMode = 0;
					}else{
						$shippingMode = 'Express';
					}
				    
					$resp['totalWeight']=$totalWeight;
					
					if($totalPrice == 0 || $totalWeight == 0){
						
					}
					
					$key_val = 1;
						
					$rate = $this->price_master_array[$zone][$key_val];
					
					switch($shippingMode){
						case 'Normal' : 
						case 'Express' :  //$applicableW = ($applicableW < 10) ? 10 : $applicableW; break;		// Min 10 Kg
											if($applicableW < 10 && $applicableW != 0){
												$applicableW = 10;
											}else{
												$applicableW = 	$totalWeight;
											}
											break;	
												// Min 10 Kg
						case 'DP' : //$applicableW = ($applicableW < 1) ? 1 : $applicableW; 
									if($applicableW >= 0 && $applicableW <= 0.499){ $applicableW = 0.5; } 
									if($applicableW > 0.5 && $applicableW <= 0.999){ $applicableW = 1; } 
									break;	// Min 1 Kg
					}
				//echo 'applicableW='.$applicableW; die;
					$final_amount = 0;
					
					$tot = $applicableW * $rate;
					//echo 'tot---'.$tot; die;
					$ab = $this->freight_master_array[$shippingMode]["airway_bill"];
					//echo $ab; die;				
					$freight_cost = $tot + $ab;
					//echo $freight_cost; die;
					if($this->freight_master_array[$shippingMode]["fuel_surcharge"] == 0){
						$fuel_surcharge = 0;
					}else{
						$fuel_surcharge = round($freight_cost * ($this->freight_master_array[$shippingMode]["fuel_surcharge"] / 100));
					} 
		
					if($this->freight_master_array[$shippingMode]["freight_on_value"] == 0){
						$freight_on_value = 0;	
					}else{
						//$freight_on_value = $totalPrice * ($freight_master_array[$shippingMode]["freight_on_value"] / 100);
						$freight_on_value = $applicableP * ($this->freight_master_array[$shippingMode]["freight_on_value"] / 100);
						$freight_on_value = round($freight_on_value,2);
						
						if($freight_on_value < 100){ $freight_on_value = 100; }
					}

					$t_freight = $freight_cost + $fuel_surcharge + $freight_on_value;

					//echo 'ggiuagsfiua'.$t_freight; die;
					
					if($this->freight_master_array[$shippingMode]["service_tax"] == 0){
						$service_tax = 0;
					}else{
						$service_tax = $t_freight * ($this->freight_master_array[$shippingMode]["service_tax"] / 100);
						$service_tax = round($service_tax,2);
					}
					
					$resp['total_shipping_cost']=ceil($t_freight + $service_tax);
				}else{
					$resp['total_shipping_cost']=0;
					$resp['totalWeight']=$applicableW + $notApplicableW;
					if($totalWeight > 0 && $totalWeight <= 0.499){ $totalWeight = 0.5; }
					if($totalWeight > 0.5 && $totalWeight <= 0.999){ $totalWeight = 1; }
					if($applicableW > 0 && $applicableW <= 0.499){ $applicableW = 0.5; }
					if($applicableW > 0.5 && $applicableW <= 0.999){ $applicableW = 1; }
				}
				$this->log_fileDetails('calculateshippingcost-final_response', $resp);
				return $resp;
						
		 }
		 
		 
		
	}
?>