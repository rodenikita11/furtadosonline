﻿<!DOCTYPE HTML>
<?header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0");?>
<html>
<head>
<title>FMP- Login</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- Custom Theme files -->
<link rel="stylesheet" href="<?php echo SITECSS?>loginstyle.css">
<!-- for-mobile-apps -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<meta name="keywords" content="Classy Login form Responsive, Login form web template, Sign up Web Templates, Flat Web Templates, Login signup Responsive web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- //for-mobile-apps -->
<!--Google Fonts-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo SITECSS?>loginstyle1.css">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,500,800' rel='stylesheet' type='text/css'>

</head>
<body>
<!--header start here-->
<div class="header">

    <div class="header-main">
        
        <h1><a href="#"><img src="<?php echo SITEIMAGES;?>mobimg/logo1.png"></a></h1>
               <h1>Login</h1>
            <div class="header-bottom">
                <div class="header-right w3agile">
                    
                    <div class="header-left-bottom agileinfo">
                        
                     <form action="" name="loginform" id="loginform">
                        <input type="text"  name="email" placeholder="example@gmail.com" id="email"/>
                        <input type="password" placeholder="password" name="password" id="password" >
                        <div class="clear"> </div>
                        <br/>
                        <button  type="button" id="submit" name="login" value="Login" >Login</button>
                    </form> 
                    <center><div class="failmsg" style="margin-top: 10px;color: #847e80;"></div></center>
                    <div class="header-left-top">
                    <div class="hr-sect">Login via</div>
                        <!--<div class="sign-up"> <h2>or</h2> </div>-->
                        
                    </div>
                    <div class="header-social wthree">
                            <a href="<?echo SITEMOBURL?>user/flogin" class="face"><h5>Facebook</h5></a>
                            <br>
                         <a href="<?echo SITEMOBURL?>user/gmaillogin" class="face" style="width: 78%;background: #DD4B39;border: 1px solid #DD4B39;margin-top: 11px;outline: none;"><h5>Gmail</h5></a>
                        </div>
                    <br/>
                        <div class="remember">
                         <span class="checkbox1" style="color:#fff;" onclick="displayPassDiv();">
                         Forgot Password?
                         </span>
                         <div class="forgot">
                            <span class="checkbox1"><a href="#small-dialog" class="sign-in popup-top-anim" style="
    color: #fff;
">Register Now </a></span>
                         </div>
                        <div class="clear"> </div>
                      </div>
                <!--<div id="idFormInform" style="display:none;">
                    <form name="formInformMe" id="inform">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top" style="padding-top:5px;">
                           <input name="emailid" id="emailid" type="text" maxlength="100" value="" size="30" placeholder="Enter your email address" class="sign_in_form" />
                        </td>
						</tr>
						<tr>
                      <td valign="top"> <a href="javascript:void(0);"><button type="button" id="forgotpassword" style="width: 25%; float: right;    font-size: 15px; margin-top: -12px;padding-left: 16px; padding-bottom: 4px;padding-top: 4px;"class="btn btn-danger grooverbtn text-center"><Send/button></a></td>
                      </tr>
                    </table>
                    </form>
                </div>-->
                        
                </div>
                </div>
              
            </div>
        </div>
        
        <div id="small-dialog" class="mfp-hide">
        
        <h1><a href="#"><img src="<?php echo SITEIMAGES;?>mobimg/logo1.png"></a></h1>
        
            <h5 class="w3ls-title">Sign Up</h5>
            <p style="color:#fff; text-align:center; padding-top:20px;
            padding-bottom:20px;">Personal information</p>
            <div class="header-bottom">
                <div class="header-right w3agile">
                    
                    <div class="header-left-bottom agileinfo">
                        
                     <form  method="post" name="registerform" id="registerform">
                        <input type="text" placeholder="Your Name" name="name" id="name" required="" onkeypress="return isNumber(event)">
                        <input type="text" placeholder="example@gmail.com" name="snemail" id="snemail" required="">
                        <input type="password"  placeholder="Password" name="snpassword" id="snpassword" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'password';}"/>
                        
                        <!--<div class="remember">
                         <span class="checkbox1">
                               <label class="checkbox"><input type="checkbox" name="" checked=""><i> </i>Remember me</label>
                         </span>
                         <div class="forgot">
                            <h6><a href="#">Forgot Password?</a></h6>
                         </div>
                        <div class="clear"> </div>
                      </div>-->
                        <div class="clear"> </div>
                        <br/>
                        <input type="button" class="signupbtn" value="Sign Up" id="registeruser">
                    </form>
                    <br/>
                </div>
                </div>
              
            </div>
        </div>
</div>
<!--header end here-->
<!--pop-up for forget password start-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3class="modal-title">Forgot your password?</h3>
      </div>
      <div class="modal-body">
	  <p>In order to receive your access code by email, please enter the email address you provided during the registration process. <p>
        <form name="formInformMe" id="inform">
           <input name="emailid" id="emailid" type="text" maxlength="100" value="" size="30" placeholder="Enter your email address" class="sign_in_form" style=" margin-top: 14px; width:300px;
    padding: 3px 9px 5px 33px;
" />
           
        </form>
      </div>
      <div class="modal-footer">
	   <button type="button" id="forgotpassword" class="btn btn-danger grooverbtn text-center">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--pop-up for forget password end-->
<script src="<?php echo SITEJS;?>jquery.magnific-popup.js"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
    $.validator.addMethod(
            "regex",
            function(value, element, regexp) 
            {
                if (regexp.constructor != RegExp)
                    regexp = new RegExp(regexp);
                else if (regexp.global)
                    regexp.lastIndex = 0;
                return this.optional(element) || regexp.test(value);
            },
            "Please check your input."
    ); 

    jQuery('#registerform').validate({
        rules:{
            name:{
                required:true
            },
            snemail:{
                email:true,
                required:true
            },
            snpassword:{
                required:true,
                regex: /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/, 
            }
        },
        messages:{
            name:{
                required:"Please enter Name"
            },
            snemail:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            },
            snpassword:{
                required:"Please enter Password",
                regex: "Passwords must contain at least 8 characters, including uppercase, lowercase, special characters and numbers.",
            }
        },
    });

    jQuery('#registeruser').click(function() {        
        var success=jQuery("#registerform").valid();
             if(success){
                    name=jQuery('#name').val();
                    email=jQuery('#snemail').val();
                    password=jQuery('#snpassword').val();
                jQuery.ajax({
                     url:'<?php echo SITEMOBURL?>user/usersignup',
                    type:'POST',
                    data:{name:name,email:email,password:password},
                    success:function(result){
                        var response = jQuery.parseJSON(result);
                        if(response.error=='false'){
                             alert('user registered successfully'); 
                             window.location.reload();
                        // }else if(response.error=='true'){
                             // alert('error in adding  user');    
                        }else{
                            alert(response.message);  
                        }
                    }
                });
            }
        });
    
    });

</script>
</body>
</html>
<style type="text/css">
    #submit{
    background: #fff;
    color: #000;
    font-size: 26px;
    padding: 0.3em 1.2em;
    width: 100%;
    font-weight: 500;
    transition: 0.5s all;
    -webkit-transition: 0.5s all;
    -moz-transition: 0.5s all;
    -o-transition: 0.5s all;
    display: inline-block;
    cursor: pointer;
    outline: none;
    border: none;
    border-radius: 30px;
    font-family: 'Roboto Condensed', sans-serif;

    }
</style>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#loginform').validate({
        rules:{
            email:{
                email:true,
                required:true
            },
            password:{
                required:true
            }
        },
        messages:{
            email:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            },
            password:{
                required:"Please enter Password"
            }
        },
    });

    jQuery('#submit').click(function() {        
        var success=jQuery("#loginform").valid();
             if(success){
        var backurl=document.referrer;
                jQuery.ajax({
                    url:'<?php echo SITEMOBURL?>user/userlogin',
                    type:'POST',
                    dataType:'JSON',
                    data:jQuery('#loginform').serialize(),
                    success:function(result){
                        res=eval(result);

                        if(res.status == true)
                        {
							   if(backurl=='<?php echo SITEMOBURL?>product/buynow' || backurl=='<?php echo SITEMOBURL?>product/checkoutuserdtl'){
								 window.location.href="<?php echo SITEMOBURL?>product/viewcart"; 
							   }else{
								  window.location.href="<?php echo SITEMOBURL?>";
							   }
                        }else{
                            jQuery('.failmsg').html(res.msg);
                        }
                    }
                });
            }
        });
    
    });

</script>
<script type="text/javascript">
    jQuery(document).ready(function(){

    jQuery('#inform').validate({
        rules:{
            emailid:{
                email:true,
                required:true
            }
        },
        messages:{
            emailid:{
                email:"Please enter valid Email Id",
                required:"Please enter your Email Id"
            }
        },
    });

    jQuery('#forgotpassword').click(function() {      
        var success=jQuery("#inform").valid();
        if(success){
        var emailid = document.getElementById('emailid').value;
        var formdata = $("#inform").serialize();
        var baseurl = '<?echo SITEMOBURL?>';
            jQuery.ajax({
                
                type: "POST",
                url: baseurl+'user/saveforgotpassword',
                data:{emailid},
                success:function(result){

                    res=jQuery.parseJSON(result);
                    //console.log(res);
                    if(res.status == true){
                        //alert('Thank you. You will receive a mail for password change shortly');
						alert('You will receive an email for change of password shortly in your inbox or spam folder. Thank you.');
                        jQuery('#emailid').val(' ');
                        jQuery('#myModal').modal('hide');
                   }else if(res.status == false){
                     alert('Sorry you are not registered with us or kindly enter the valid email Id & Password.');
                     jQuery('#emailid').val(' ');
                     jQuery('#myModal').modal('hide');

                    }
                }
            });
        }
    });
});
</script>
<script>
    jQuery(document).ready(function() {
        jQuery('.popup-top-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });                                                                                         
    }); 

function displayPassDiv(){
    jQuery('#myModal').modal('show');
//document.getElementById("idFormInform").style.display = "block";
}

function validateEmail(email){
        var RE_EMAIL = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$/
        if (!RE_EMAIL.test(email)) {
            //alert("You must enter a valid email address."); 
            return false;   
        }
    }


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode > 96 && charCode < 123) || (charCode > 64 && charCode < 91) || (charCode == 32)) {
        return true;
    }
    return false;
}

</script>
