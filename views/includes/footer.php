<!-- footer -->
			<footer class="main_footer">
				<!-- top link-->
				<section class="top_link">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 ml_ink">
								<ul>
									 <li class="first"><a href="<?php echo SITEMOBURL?>user/aboutus" title="About Us">About Us</a></li>
              <li><a href="<?php echo SITEMOBURL?>user/contactus" title="Contact Us">Contact Us </a></li>
              <li><a href="<?php echo SITEMOBURL?>user/job" title="Career">Career</a></li>
              <li><a href="<?php echo SITEMOBURL?>user/pianoenquiry" title="Piano Enquiry">Piano Enquiry</a></li>								                  
								</ul>
							</div>
							<div class="col-lg-6 mail_right">
								<p>For any query write us on :<a href="mailto:response@furtadosonline.com"> response@furtadosonline.com </a></p>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end top link-->
				<!-- ft link -->
				<section class="ft_link_sec">
					<div class="container">
						<div class="row">
							<article class="col-lg-9 col-xs-9">
								<div class="ft_link">
									<ul>
										<li>
											<h5>Musical Instruments<span></span></h5>
											<ol>
												<li><a href="#">Pianos </a></li>
												<li><a href="#">Guitars & Bass</a></li>
												<li><a href="#">Keyboards & Dmi</a></li>
												<li><a href="#">Drums & Percussion </a></li>
												<li><a href="#">Harmonicas & Accordions </a></li>
												<li><a href="#">Bowed Strings </a></li>
												<li><a href="#">Wind Instruments </a></li>
												<li><a href="#">Education </a></li>
												<li><a href="#">Indian Instruments </a></li>
												<li><a href="#">Musician Essentials</a></li>
												<li><a href="#">Other Fretted Instruments </a></li>
												<li><a href="#">Computer Music </a></li>
											</ol>
										</li>
										<li>
											<h5>Music Books<span></span></h5>
											<ol>
												<li><a href="#">Examination Music </a></li>												
												<li><a href="#">Keyboard </a></li>												
												<li><a href="#">Fretted Strings</a></li>												
												<li><a href="#">Bowed Strings </a></li>												
												<li><a href="#">Drums & Percussion</a></li>		
												<li><a href="#">Voice </a></li>
												<li><a href="#">Repertoire & Albums</a></li>
												<li><a href="#">Wind</a></li>
												<li><a href="#">General </a></li>
												<li><a href="#">Educational</a></li>
												<li><a href="#">Ensemble </a></li>
												<li><a href="#">English Speech/drama</a></li>
												<li><a href="#">Miscellaneous Books </a></li>
												<li><a href="#">Indian Instruments </a></li>
												<li><a href="#">Music Magazines</a></li>
											</ol>
										</li>
										<li>
											<h5>Pro Audio<span></span></h5>
											<ol>
												<li><a href="#">Desktop Monitors </a></li>
												<li><a href="#">Computer Interfaces </a></li>
												<li><a href="#">Keyboard Controllers </a></li>
												<li><a href="#">Microphones </a></li>
												<li><a href="#">Headphones </a></li>
												<li><a href="#">Cables & Accessories </a></li>
												<li><a href="#">Home Sound </a></li>
												<li><a href="#">Portable Recording Devices </a></li>
												<li><a href="#">Software</a></li>
												<li><a href="#">Speakers</a></li>
												<li><a href="#">Dj</a></li>
												<li><a href="#">Live Sound</a></li>
												<li><a href="#">Home Studio </a></li>
											</ol>
										</li>
										<li>
											<h5>Payment Help<span></span></h5>
											<ol>
												<li><a href="#">Credit / Debit Card </a></li>
												<li><a href="#">Net Banking</a></li>
												<li><a href="#">Cheque / Demand Draft</a></li>
												<li><a href="#">NEFT / RTGS</a></li>
											</ol>
										</li>
										<li class="f_mt_15">
											<h5>Help<span></span></h5>
											<ol>
												<li><a href="#">Contact us </a></li>
												<li><a href="#">Feedback</a></li>
												<li><a href="#">Sitemap</a></li>										
											</ol>
										</li>
										<li class="f_mt_15">
											<h5>Policies<span></span></h5>
											<ol>
												<li><a href="#">Shipping Policy </a></li>
												<li><a href="#">Return Policy</a></li>
												<li><a href="#">Privacy Policy</a></li>
												<li><a href="#">Assurance Program</a></li>
												<li><a href="#">Terms</a></li>												
											</ol>
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</article>
							<aside class="col-lg-3 col-md-3 col-xs-3">
								<div class="ft_search">
									<span>Subscribe For Our Newsletter</span>
									<div class="find_se" id="custom-search-input_1">
										<div class="input-group">
											<input type="text" placeholder="Buscar" class="form-control input-lg">
											<span class="input-group-btn">
												<button type="button" class="btn btn-info btn-lg">
													
												</button>
											</span>
										</div>
									</div>
								</div>
								<div class="ft_paymebt">
									<h5/>Payment Method</h5>
									<ul>
										<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/img_25.jpg" alt=""></a></li>
										<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/img_26.jpg" alt=""></a></li>
										<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/img_27.jpg" alt=""></a></li>
										<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/img_28.jpg" alt=""></a></li>
									</ul>
									<div class="clear"></div>
								</div>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end ft link -->
				<!-- red sec -->
				<section class="red_ft">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<p>Your one stop shop for everything musical online</p>
							</div>
						</div>
					</div>
				</section>
				<!-- end red sec -->
				<!-- copy right -->
				<section class="copy_right_sec">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-xs-6 copy_right">
								<p>© 2016 <strong>Furtadosonline.com</strong> all rights reserved.<br><a href="#">Design & Developed by Lumeg</a></p>
							</div>
							<div class="col-lg-6 col-xs-6 ft_social">
								<ul>
									<li><a href="">Follow us on</a></li>
									<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/fb.jpg" alt=""></a></li>
									<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/tw.jpg" alt=""></a></li>
									<li><a href="#"><img src="<?php echo ASSETURL;  ?>images/g+.jpg" alt=""></a></li>
								</ul>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end copy right -->
			</footer>
			<!-- end footer -->
			<!-- end content wrapper -->
		</div>	
		<!-- end main wrapper -->
		
        <script src="<?php echo ASSETURL; ?>js/jquery.min.js"></script>
        <script src="<?php echo ASSETURL; ?>js/bootstrap.min.js"></script>		
		<script src="<?php echo ASSETURL; ?>js/jquery.flexslider.js"></script>	
		<script src="<?php echo ASSETURL; ?>js/owl.carousel.js"></script>	
		<script src="<?php echo ASSETURL; ?>js/main.js"></script>	
    </body>
</html>