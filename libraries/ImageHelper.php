<?php
		/*
			FILE NAME : ImageHelper
			CREATED : 15/Sep
			AUTHOR : SANJAY
			DESC : This library file is used to get Image from resource to front-end, in case resource file miss, it will get default image for such missing resource.
			NOTE : Don't modify this file, it works..
		*/
		class ImageHelper
		{
			protected $imgFor = array('BNR');
			
			#-- Master function, can be used directly from view.
			public function get_image($fileName,$imgFor)
			{
					$resource['imgPath'] = FILEBASEURL;
					$resource['fileLoc'] = $this->getImageFor($imgFor);
					$resource['fileName'] = $fileName;
					$fileUrl = $this->getResourceFileURL($resource,$imgFor);
					return $fileUrl;
			}
			
			
			#-- used for getting target folder path.
			public function getImageFor($imgFor)
			{
				switch($imgFor)
					{
						case 'BRN': #Brand
						$fileLoc = IMGBRANDPATH;
						break;
						
						default:
						$fileLoc = '';
					}
				return $fileLoc;
			}
			
			#--Try to fetch, image file.. if not found then get default image resource.
			public function getResourceFileURL($resourceOption,$imgFor)
			{
					$imgPath = null; 
					if(file_exists(FILEBASEPATH.$resourceOption['fileLoc'].$resourceOption['fileName']) && !empty($resourceOption['fileName']))
						{
							$imgPath = $resourceOption['imgPath'].$resourceOption['fileLoc'].$resourceOption['fileName'];
							return $imgPath;
						}
						else
						{
							switch($imgFor)
							{
								case 'BRN': #Brand
								$imgPath = $resourceOption['imgPath'].IMAGEDEFAULT.IMGBRANDDEFAULT;
								break;
							}
						}
				return $imgPath;
			}
		}
?>