<div id="account-menu" class="pull-left acc_list1">
    <a href="<?php echo site_url('customer/profileSetting');?>" class="account-menu-title desk_acc"><i class="fa fa-user"></i>&nbsp; Account <i class="fa fa-angle-down"></i> </a>
	<a href="javascript:void(0)" class="account-menu-title mob_acc"><i class="fa fa-user"></i>&nbsp; Account <i class="fa fa-angle-down"></i> </a>
        <ul class="list-unstyled account-menu-item">
            <li><a href="<?php echo site_url('customer/profileSetting');?>"><i class="fa fa-user"></i>&nbsp;Profile</a></li>
            <li><a href="<?php echo site_url('customer/mainorder');?>"><i class="fa fa-th-list"></i>&nbsp;Order</a></li>
            <li><a href="<?php echo site_url('customer/wishlist');?>"><i class="fa fa-heart"></i>&nbsp; Wishlist</a></li>
			<li><a href="javascript:void(0);" onclick="logout_session();"><i class="fa fa-power-off"></i>&nbsp;logout</a></li>
        </ul>
</div>
<script type="text/javascript">
logout_session = function(){
		jQuery.cookie('mywishlist','',{ path:'/'});
		window.location = APIURL+'customer/logout';
};
</script>