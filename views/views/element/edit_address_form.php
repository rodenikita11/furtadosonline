<style>
.shipping-edi{background: none repeat scroll 0 0 #fff; margin: 0 auto; position: relative;  width: 550px !important;}
.mrt10{margin-top:10px;}

@media (max-width: 600px) {
	
.shipping-edi					{width:90% !important;}
.shipping-edi i.pull-right		{width:auto !important;}
#edit_shipaddress				{width:100% !important; margin-left:0px !important;}

}
</style>
<div class="shipping-edi" style="width:60%;padding:10px 25px;" id="edit_shiping_details">   
<div onclick="hidepopup();"><i class="fa fa-times-circle close-icon pull-right"></i></div>           
          <div><h3>Edit Shipping Address</h3></div>
            <div>
<div class="form-group">
    <label for="edit_shipname" class="col-sm-4">Name:</label>
    <div class="col-sm-8">
        <input value="<?php echo $addressDetails['contact_person'];?>" class="form-control" type="text" name="edit_shipname" id="edit_shipname"> 
		<div id="edit_shipname_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
<div class="form-group mrt10">
    <label for="edit_shippincode" class="col-sm-4">Pincode:</label>
    <div class="col-sm-8">
       <input class="form-control" type="text" name="edit_shippincode" value="<?php echo $addressDetails['postcode'];?>" id="edit_shippincode"> 
		<div id="edit_shippincode_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
 <div class="form-group mrt10">
  <label for="edit_shipaddress" class="col-sm-1">Address:</label>
   <textarea class="form-control" style="width:300px; float:left; margin-left:140px; margin-bottom:10px;" name="edit_shipaddress" rows="3" id="edit_shipaddress" class="shipping-input-form-textarea"><?php echo $addressDetails['address_1'];?></textarea>
	<div id="edit_shipaddress_alert" class="cl_error"></div>
</div>

<div class="form-group mrt10">
    <label for="edit_shiplandmark" class="col-sm-4">Land Mark:</label>
    <div class="col-sm-8">
       <input class="form-control" type="text" name="edit_shiplandmark" value="<?php echo $addressDetails['landmark'];?>" id="edit_shiplandmark"> 
		<div id="edit_shiplandmark_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
<div class="form-group mrt10">
    <label for="edit_shipcity" class="col-sm-4">City:</label>
    <div class="col-sm-8">
       <input class="form-control" type="text" name="edit_shipcity" value="<?php echo $addressDetails['city'];?>" id="edit_shipcity"> 
		<div id="edit_shipcity_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
<div class="form-group mrt10">
    <label for="edit_shipstate" class="col-sm-4">State:</label>
    <div class="col-sm-8">
	   <input class="form-control" type="text" name="edit_shipstate" value="<?php echo $addressDetails['state'];?>" id="edit_shipstate"> 
		<div id="edit_shipstate_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
<div class="form-group mrt10">
    <label for="edit_shipphone" class="col-sm-4">Phone:</label>
    <div class="col-sm-8">
       <input type="text" name="edit_shipphone" value="<?php echo $addressDetails['contact_num'];?>" placeholder="+91" id="edit_shipphone" class="shipping-input-form"> </div>
			<div id="edit_shipphone_alert" class="cl_error"></div>
    </div>
</div>
   <div class="clearfix"></div>
<div class="form-group mrt10">
    <label for="inputFirstName" class="col-sm-4"></label>
    <div class="col-sm-8">
       <button class="co-btn" onclick="editshipping(<?php echo $addressDetails['address_id'];?>);" id="edit_shiping">UPDATE</button>
	   <button class="co-btn" onclick="hidepopup();" id="cancel_shiping">CANCEL</button>
    </div>
</div>
   <div class="clearfix"></div>
</div>
