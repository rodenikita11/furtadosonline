<div class="top-logo">
<div class="container" style="padding:0;">
<div class="row">
<div class="col-md-12">

<div class="col-xs-2">
<a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>

</div>
<div class="col-xs-7 Favourite_list">Gift Voucher</div>

<div class="col-xs-3" style="padding:0;">
  
 </div>
  </div>
</div>
</div>
</div>
<div class="container-fluid bg-1 text-center">
<div class="col-sm-1"></div>

<div class="col-sm-10" style="text-align:center;">
  <img src="<?php echo SITEIMAGES;?>mobimg/icon/gift_vouccher_poster_cc-1.jpg" class="img-responsive margin" style="display:inline;margin-right:30px;" alt="gift">
  <h3>We at Furtados Music offer you the unique power of gifting! Whether it’s a birthday or even just a Thank You, make someone feel special.

It’s the best gift a musician can get. With a wide range of musical instruments to choose from, your loved one can now pick what they like instead of you wondering what to buy!</h3>
</div>
<div class="col-sm-1"></div>
</div>

<!-- Second Container -->
<div class="container-fluid bg-2 text-center">
<div class="col-sm-1"></div>

<div class="col-sm-10">
  <h2 class="margin">NOW AVAILABLE AT FURTADOS STORES ACROSS INDIA!</h2 >
  <p>You can now purchase Gift Vouchers worth Rs. 100, Rs. 500 and Rs. 1000 at any Furtados Music store. The voucher can be redeemed at Furtados and Musee Musical stores across India.</p>
  
  <p>When you buy a gift voucher,
Make sure the date of issue, place of issue (Store name), Stamp and Signature is added to the Gift Voucher.<p>
  </div>
  <div class="col-sm-1"></div>
</div>


<div class="container-fluid bg-3 text-center">    
  <h3 class="">Terms and Conditions</h3><br>
  <div class="row">
    
  <div class="col-sm-2"></div>
    <div class="col-sm-8 Terms"> 
      *The Value of the Gift Voucher is mentioned on the front of the voucher.<br>
*The voucher is to be presented to the cashier at the time of purchase.<br>
*The voucher is valid for 1 year from the date of issue. If the voucher is not utilized within that time, the value of the voucher will be forfeited. Under no circumstances will expired vouchers be accepted or refunded.<br>
*The voucher is valid only if stamped and signed by the issuing store.<br>
*The voucher cannot be refunded or redeemed in cash.<br>
*In case the voucher is lost or stolen, Furtados does not take responsibility to issue a replacement<br>
*Furtados Gift Vouchers can be used only at Furtados and Musee Musical outlets<br>
*Furtados Gift vouchers cannot be redeemed online at www.furtadosonline.com<br>
     
    </div>
	  <div class="col-sm-2"></div>
  </div>
</div>




<!-- Third Container (Grid) -->
<div class="container-fluid bg-4 text-center">    
 
  <div class="row">
  <div class="col-sm-1"></div>
  <div class="col-sm-10">
   <h2 class="margin">List of locations</h2>
  <p>You can now reedem your gift vouchers at the following locations: </p><br><br>
    <div class="col-sm-4 footer-last" style="text-align:center;">
	
      <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/65">Furtados Music - Ahmedabad</a></p>
	  
    <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/7">Furtados Music - Bangalore </a></p>
	
	<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/41">Furtados Music - Chandigarh
</a></p>
	<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/71">Furtados Music - Mangalore</a></p>

<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/29">Furtados Music - Margao
<br/>(Grace Estate)</a></p>

<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/15">Furtados Music - Margao 
<br/>(Reliance Centre)</a></p>

<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/25">Furtados Music - Panjim</a></p>
	
    </div>
    <div class="col-sm-4 footer-last" style="text-align:center;"> 
     <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/101
">Furtados Music - Mumbai 
<br/>(Borivali - SVP Road)</a></p>  

	 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/67">Furtados Music - Mumbai<br/> 
(Powai - Galleria Mall)</a></p>
	 
	 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/21
">Furtados Music - Mumbai 
<br/>(Dhobitalao)</a></p>
	 
	 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/5">Furtados Music - Mumbai 
<br/>(Juhu - JVPD scheme)</a></p>
	 
	 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/27">Furtados Music - Mumbai 
<br/>(Kalbadevi)</a></p>
	 
	 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/1">Furtados Music - New Delhi 
<br/>(Lajpat Nagar II)</a></p>
     
    </div>
    <div class="col-sm-4 footer-last" style="text-align:center;"> 
     
	  <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/49">Furtados Music - New Delhi 
<br/>(Saket)</a></p>
	  
	  
	   <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/0">Furtados Music - Pune 
<br/>(Camp- Nucleus Mall)
</a></p>
	   
	    <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/102">Furtados Music - Pune 
<br/>(Baner) </a></p>
		
		<p><a href="https://www.furtadosonline.com/mobile/user/storelocator/39">Musee Musical - Hyderabad </a></p> 
		 
		 <p><a href="https://www.furtadosonline.com/mobile/user/storelocator/31">Musee Musical - Chennai</a></p>
		  
		
	 
    </div>
  </div>
  
    <div class="col-sm-1"></div>
</div>
</div>
 <div class="sn-group sn-social">
                <center><h4 class="mt">Follow us on</h4></center>
                <div><a class="facebook" href="https://www.facebook.com/FurtadosMusic" target="_blank">Facebook</a></div>
                
                <div><a class="youtube" href="https://www.youtube.com/user/furtadosmusicindia" target="_blank">YouTube</a></div>
                <div><a class="twitter" href="https://twitter.com/furtados" target="_blank">Twitter </a></div>
                <div><a class="instagram" href="https://www.instagram.com/furtadosmusic" target="_blank">Instagram</a></div>
                
              </div>
<style>
  body {
      font: 20px Montserrat, sans-serif;
      line-height: 1.8;
      color: #f5f6f7;
	 
  }
  p {font-size: 16px;}
  .margin {margin-bottom: 45px;}
  .bg-1 { 
      background-color: #fff; /* Green */
      color: #000;
  }
  .bg-2 { 
      background-color: #ab2327; /* Dark Blue */
      color: #ffffff;
  }
  .bg-3 { 
      background-color: #ffffff; /* White */
      color: #555555;
  }
  .bg-4 { 
      background-color: #ab2327; /* Black Gray */
      color: #fff;
  }
  .container-fluid {
      padding-top: 30px;
      padding-bottom: 30px;
  }
  .navbar {
      padding-top: 15px;
      padding-bottom: 15px;
      border: 0;
      border-radius: 0;
      margin-bottom: 0;
      font-size: 12px;
      letter-spacing: 5px;
  }
  .navbar-nav  li a:hover {
      color: #1abc9c !important;
  }
  h3{    margin: 20px 0 10px;
    color: #ab2327;
    font-weight: 500;
    font-size: 17px;}
	
	.Terms
	{ font-size:14px;
	}
	a{color:#fff;}
	a:hover{color:#000;}
	
	.footer-last
	{text-align:left;
	}
	.mt{margin-top:20px !important;}
  </style>
	

    
	
 <?php include("include/footer2.php"); ?>