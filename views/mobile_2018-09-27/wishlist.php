<style type="text/css">
	.badge{background: #d32f2f !important;
    border-radius: 50% !important;
    color: #fff !important;
    height: 15px !important;
    width: 15px !important;
    position: absolute !important;
    top: 0px;
    right: 28px !important;
    font-size: 8px !important;
    text-align: center !important;
    line-height: 12.5px !important;
    border: 2px solid #fff !important;
    padding: 0px 0px !important;
}
</style>


<div class="top-logo">
	<div class="container" style="padding:0;">
		<div class="row">
			<div class="col-md-12">
				<div class="col-xs-2">
					<!--<a href="#" onclick="window.history.go(-1); return false;"><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>-->
					<a href="#" onclick="window.history.go(-1); return false;" >
					<!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
					<i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
					</a>
				</div>
				<?
				if($resp){
				$cnt =count($resp);
				}
				if($cnt >0){
				$cnt=$cnt;
				}else{
				$cnt=0;	 
				}?>
				<div class="col-xs-7 Favourite_list">Wishlist  (<?echo $cnt;?>)</div>
				<div class="col-xs-3" style="margin-top:10px;">
					<div class="col-xs-12">
						<div class="col-xs-4 ">
							<a href="<?php echo SITEMOBURL;?>product/viewcart"><i class="fa fa-shopping-cart" aria-hidden="true">
								<span class="badge badge-notify my-cart-badge" id="currentitemcount"></span></i></a>
						</div> 
						<div class="col-xs-4">  
							<div class="dropdown" style="float:right;">
								<button class="dropbtn"><i class="fa fa-power-off" aria-hidden="true"></i></button>
								<div class="dropdown-content">
									<a href="<?php echo SITEMOBURL?>user/myaccount">My Profile</a>
									<a href="<?php echo SITEMOBURL?>product/getorderbyuser">My Order</a>
									<a href="<?php echo SITEMOBURL?>user/logout">Log Out</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--reviews-->
<?if($resp){
	foreach($resp as $row){?>
		<div class="Favourite">
			<div class="Container container-fluid text-center Pearl">
				<div class="row">
					<div class="col-xs-12" >
						<div class="col-xs-4">
							<?if($row['image']){?>
								<img src="<?php echo $row['image'];?>" width="100%">
							<?}else{?>
								<img src="<?php echo SITEIMAGES;?>mobimg/noimage1.png" width="100%">
							<?}?>
						</div>
						<div class="col-xs-1"></div>

						<div class="col-xs-7 Pearl_Drum"><p><?php echo $row['proname'];?></p>
							<?if($row['dicountprice']){?>
								<p class="amount"><i class="fa fa-inr" aria-hidden="true"></i><b><?php echo $this->libsession->priceformat($row['dicountprice'])?></b></p>
							<?}else{?>
								<p class="amount"><b><?php echo $this->libsession->priceformat($row['price'])?></b></p>
							<?}?>
							<?if($row['stock']=="1"){?>
								<p class="stock">In stock</p>
							<?}else{?>
								<p class="stock">Out of stock</p>
							<?}?>
							<div class="clear"></div>
							<div class="col-xs-12">
								<div class="col-xs-8">
									<?if($row['stock']=='1'){?>
										<p class="add_to_card" style="text-align:center;padding: 10px;" onclick="addtocart(<?php echo $row['proid'];?>);">ADD TO CART</p>
									<?}else{?>
										<p class="add_to_card_color" style="text-align:center;padding: 10px;" disabled>ADD TO CART</p>
									<?}?>
								</div>
								<div class="col-xs-4 delete"  onclick="removefromwishlist(<?php echo $row['proid'];?>);">
									<img src="<?php echo SITEIMAGES;?>mobimg/icon/delete.png">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?}}else{?> 
	<div class="Favourite">
		<div class="Favourite-no">
			<p>	There are currently no products in the Wishlist !!</p> 
		</div> 
	</div>
	<!--<div class="col-xs-12">
	No product present in the wishlist !!
	</div>-->
<? }?>

<script>
function addtocart(ele){
	proid=ele;
	jQuery.ajax({
		url:'<?php echo SITEMOBURL?>product/storeproid',
		type:'POST',
		data:{proid:proid},
		success:function(data)
		{   var response = jQuery.parseJSON(data);
		   
			if(response.error=='false'){
			     alert('Product successfully added to cart');
			      removefromwishlist(proid); 
			     ajax_getCartCount();
			      window.location.reload();
				}else{
				 alert('error in adding product to cart');	
					
				}
			
		}

	});
	
}

function removefromwishlist(ele) {
	var pid=ele;
   
    jQuery.ajax({
		url:'<?php echo SITEMOBURL?>user/removefromwishlist',
		type:'POST',
		data:{pid:pid},
		success:function(data)
		{   var response = jQuery.parseJSON(data);
		  
			if(response.error=='false'){
			     alert('Product  removed successfully from Wishlist'); 
			     window.location.reload();
				}else{
				 alert('Error in removing  product from Wishlist');	
				}
			
		}

	});
	
    
}
</script>
<script>
jQuery(document).ready(function () {
ajax_getCartCount();
});  

function ajax_getCartCount(){
	
    jQuery.ajax({
            url :'<?php echo SITEMOBURL?>product/getcartcount', 
           type:'POST'
          
        }).done(function(resp){
        	if (resp.trim().length > 0) {
        		
          var response = jQuery.parseJSON(resp);
            if(response.status==1)
            {
				 if(response.cartcount > 0){  
                   jQuery('#currentitemcount').html(response.cartcount); 
                 }
          
            }
            else
            {
                  if(response.cartcount > 0){  
                     jQuery('#currentitemcount').html(response.cartcount); 
                   } 
            }
          }  
          return false;

      });
}	
</script>