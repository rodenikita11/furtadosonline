    <!-- Place this asynchronous JavaScript just before your </body> tag -->
    <script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
    </script>
<span id="signinButton">
  <span
    class="g-signin"
    data-callback="signinCallback"
    data-clientid="<?php echo GOOGLEAPPID;?>"
    data-cookiepolicy="single_host_origin"
    data-requestvisibleactions="http://schema.org/AddAction"
    data-scope="https://www.googleapis.com/auth/plus.login">
  </span>
</span>
<script type="text/javascript">
function signinCallback(authResult) {
  if (authResult['status']['signed_in']) {
				console.log(authResult);
		gapi.client.load('plus','v1', function(){
			var request = gapi.client.plus.people.list({
			'userId': 'me',
			'collection': 'visible'
			});
			request.execute(function(resp) {
			console.log('Num people visible:' + resp.totalItems);
			});
		});
		
		// Update the app to reflect a signed in user
    // Hide the sign-in button now that the user is authorized, for example:
    document.getElementById('signinButton').setAttribute('style', 'display: none');
	alert('User Login Success');
  } else {
    // Update the app to reflect a signed out user
    // Possible error values:
    //   "user_signed_out" - User is signed-out
    //   "access_denied" - User denied access to your app
    //   "immediate_failed" - Could not automatically log in the user
    console.log('Sign-in state: ' + authResult['error']);
	alert('Fail to login');
  }
}

</script>