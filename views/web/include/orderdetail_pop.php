<button type="button" class="close" data-dismiss="modal" aria-hidden="false" onclick='closeorderdetails();'>&times;</button>
<div class="col-lg-12 well">
   <div class="row">
      <?if($result){ //print_r($result);?>
      <?if($result[0]['orderId']){?>
      <div class="row">
         <div class="deatils">
            <h1 class="col-lg-10 orderh1"> <b style="color:#000"> Order No: </b> <?echo $result[0]['orderId'];?></h1>
            <?if($result[0]['insertDate']){?>
            <? $date1= date('d-m-Y',strtotime($result[0]['insertDate'])); 
               $time=date_format($result[0]['insertDate'],  'G:ia'); ?>
            <h1 class="col-lg-2 orderh1"> <b style="color:#000">Date: </b> <?echo $date1;?></h1>
            <?}?>
         </div>
      </div>
      <?}?>
      <div class="row">
      <div class="col col-sm-8 col-md-8 col-xs-12">
         <div class="col col-sm-8 col-md-12 col-xs-12 deatilsbg1"><!--deatilsbg-->
            <?for ($i=0; $i < count($result); $i++) { //print_r($result);?>
            <? $r1=array(',',' ');
               $r2=array('-','-');
               $url=ADMINURL.str_replace($r1, $r2, $result[$i]['name']).'/'.$result[$i]['product_id'];
               $totalamount+=$result[$i]['discountPrice'];
               switch($result[$i]['offertype']){
                  case 'CPN':
                     $offertype='Coupon';
                     break;

                  case 'GFT':
                     $offertype='Gift Card';
                     break;

                  default:
                     $offertype='unknown';
               }

               ?>
            <div class="col-xs-12 col-sm-6 col-md-2 gallery-holder gh">
               <div class="product-item-holder size-big single-product-gallery small-gallery">
                  <div class="single-product-gallery-item" id="slide1">
                     <a href="<?php echo $url;?>">
                     <img src="<?php if(!isset($result[$i]['image'])){ echo SITEIMAGES.'mobimg/noimage1.png';}else{echo $result[$i]['image'];}?>" alt="<? echo  $result[$i]['name'];?>" title='<? echo  $result[$i]['name'];?>'>
                     </a>
                  </div>
                  <!-- /.single-product-gallery-item -->
               </div>
            </div>
            <div class=' col-sm-6 col-md-10 product-info-block pib'>
               <div class="product-info">
                  <h1 class="name"><a href="<?php echo $url;?>"><? echo substr($result[$i]['name'], 0,40);?></a></h1>
                  <div class="price-container info-container m-t-40">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="price-box">
                              <span class="price m-r-12"> <i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat(abs($result[$i]['total']))?></span>
                              <span class="value1 red">Date:<?php echo $date1.$time;?></span>
                           </div>
                        </div>
                        <?php if($result[$i]['offersused']=='Y' && $result[$i]['offeramount'] > 0){ ?>
                           <div class="col-sm-6">
                              <div class="price-box">
                                 <span class="price m-r-12">Offer Amount<i class="fa fa-rupee" style="font-size:14px"></i> <?php echo $this->libsession->priceformat(abs($result[$i]['offeramount'])); ?></span>
                                 <span class="value1 red"><?php echo $offertype.' '.$result[$i]['offercode'].' is applied on this product'; ?></span>
                              </div>
                           </div>
                        <?php } ?>
                     </div>
                     <!-- /.row -->
                  </div>
                  <!-- /.price-container -->
               </div>
               <!-- /.product-info -->
            </div>
            <?}?>
            <!-- /.col-sm-7 -->
         </div>
      </div>
       

       <div class="col col-sm-4 col-md-4 col-xs-12" >
         <div class="col col-sm-12 col-md-12 col-xs-12" >
            <div class="price-container info-container m-t-10">
               <div class="col-sm-12">
                  <table style="width: 100%; border: 1px solid #ccc;">
                     <thead>
                        <th colspan="2" style="background: #d20e0e;border-bottom: 1px solid #ccc;">
                           <h5 style=" text-align:  center; font-size:  16px;text-transform: uppercase;font-weight: 600;
                              color: #ffffff;">Order Summary </h5>
                        </th>
                     </thead>
                     <tbody>
                        <tr>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;"> 
                              Total Amount: 
                           </td>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;">  <i class="fa fa-inr" 
                              style="font-size: 12px"></i>
                              <?php echo $this->libsession->priceformat($totalamount); ?> 
                           </td>
                        </tr>
                        <tr>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;">Shipping Amount:</td>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;"> 
                              <i class="fa fa-inr" style="font-size: 12px"></i> <?php echo $this->libsession->priceformat($result[0]['total_courier_charges'])?>
                              <?$finalamount=$result[0]['total_amount']+$result[0]['total_courier_charges'];?>
                           </td>
                        </tr>
                        <?php if($result[0]['is_offer_used']=='Y'){ ?>
                        <tr>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc; vertical-align:top" >Offer Applied</td>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;"> 
                           <?php 
                                 switch($result[0]['offer_type']){
                                    case 'CPN':
                                       $offertype='Coupon';
                                       break;
                                    case 'GFT':
                                       $offertype='Gift Card';
                                       break;
                                    default:
                                       $offertype='Offer';
                                       break;
                                 }
                                 echo $offertype;
                              ?>
                              <p><span>Coupon Amount: </span><?php echo $result[0]['offer_amounts']; ?></p>
                           </td>
                        </tr>
                        <?php } ?>
                        <tr>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;"> Final Amount:</td>
                           <td style="padding: 8px 4px;border-bottom: 1px solid #ccc;"> <i class="fa fa-inr" style="font-size: 12px"></i> <?php echo $this->libsession->priceformat($finalamount)?></td>
                        </tr>
                        <tr>
                           <td colspan="2">
                              <span class="col-sm-12" style="color: #e90734;text-align: center;padding: 8px 5px; font-weight: 600;text-transform: uppercase;">Status: <?php if(isset($result[0]['status'])) { echo $result[0]['status'];}else{ echo "Order placed"; }?></span>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>
      <?}else{?>
      <div class="col-xs-12 "  >
         <h1 style="font-size: 16px !important;color: red;" class="col-xs-8" >Sorry unable to fetch the Order Detail !!</h1>
      </div>
      <?}?>
      <?//print_r($result);die;?>
   </div>
</div>
<style type="text/css">
.pib{border-bottom: 1px solid #ccc;
    padding-bottom: 3px;
    border-top: 1px solid #ccc;}
   i.fa.fa-inr{
   color: #181818;
   }
   .proorder{
   border: 1px solid #ccc;
   margin-top: 10px;
   min-height: 315px;
   }
   div#proorder1 {
   border-bottom: 1px solid #9a9898 !important;
   border-top: 1px solid #9a9898 !important;
   }
   .gh{border-bottom: 1px solid #ccc;
    padding-bottom: 4px;
    border-top: 1px solid #ccc;
    padding-top: 5px;
    clear: both;
 }
</style>