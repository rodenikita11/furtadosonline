<?
class Liblogin{
          public $ci;
          public function __construct() {
             $CI = & get_instance();
             $CI->load->model('Do_user');
             $CI->load->model('Do_common');
			 $CI->load->library('Libsession'); 
			 $CI->load->library('encryption');
             $this->ci = $CI;
             
         }
         
       public  function getuserdetail($param){
            // print_R($param); die;
             if(isset($param['password']) && isset($param['email'])){
                 if (!filter_var($param['email'], FILTER_VALIDATE_EMAIL)) { 
                      //echo  "Invalid email format"; exit;
                    return false;
                    }else{
                        $param['password']=md5($param['password']);
                        $res=$this->ci->Do_user->getusrdtl($param);
                       // print_R($res); die;  
                        if(empty($res))
                        {
                            return false;
                        }
						
						$encrypted_customer_id = $this->ci->encryption->encrypt($res[0]['customer_id']);
                        $sessiondata= array('usrid'=>$encrypted_customer_id,'usrname'=>$res[0]['firstname'].' '.$res[0]['lastname'],'usremail'=>$res[0]['email'],'usrnum'=>$res[0]['telephone'],'wishlist'=>$res[0]['wishlist'],'usradd'=>$res[0]['useraddress'].$res[0]['city'].$res[0]['state'],'pincode'=>$res[0]['pincode']);
						
                    
                        $this->ci->libsession->setSession($sessiondata);
                    // print_R($_SESSION); die;
                        return true;
                        
                    }
             }else{
                 echo "please enter the email and  password"; exit; 
             }
             
             
         }
         
         
         public function usersignup($param){
              if(isset($param)){
                 if (!filter_var($param['email'], FILTER_VALIDATE_EMAIL)) { 
                      //echo  "Invalid email format"; exit;
                      return 'Invalid Email Format';
                    }else{
                        $param['password']=md5($param['password']);
                        //print_r($param);die;
                        $res=$this->ci->Do_user->insertuser($param); 
                        //print_r($res);die;
                        if($res){
                         return $res;   
                            
                        }
                        
                    }
             }else{
                 echo "please enter the email and  password"; exit; 
             }
        }

        /*changepassword by viki 14/3/2018*/
        
        public function changepassword($param){
          if(isset($param)){
           // print_r($param); die;
            $res=$this->ci->Do_user->changepassword($param);
            if($res){
              return $res;
            }else{
              return false;
            }
          }
        }
        /*End changepassword */

        public function getcities($param)
        {
            if(isset($param))
            {
                $res=$this->ci->Do_user->getcities($param); 
                    if($res){
                     return $res;   
                        
                    }else{
                        return false;   
                    }
            }
        }

        //FOR USER ACCOUNT
        
        public function updateuseraccdtl($param)
        {
         //print_r($param);die;
            if(isset($param))
            {
                $res=$this->ci->Do_user->updateuseraccdtl($param); 
                    if($res){
                     return $res;   
                        
                    }else{
                        return false;   
                    }
            }   
        }

        //for shipping address
        public function updateuserdetail($param)
        {
            if(isset($param))
            {
             
                $res=$this->ci->Do_user->updateuser($param); 
                    if($res){
                     return $res;   
                        
                    }else{
                        return false;   
                    }
            }   
        }

        public function saveshippingaddress($param)
        {
            if(isset($param))
            {
                $res=$this->ci->Do_user->addshippingaddress($param); 
                    if($res){
                     return $res;   
                        
                    }else{
                        return false;   
                    }
            }   
        }

        
        public function savebillingaddress($param)
        {
            if(isset($param))
            {
                $res=$this->ci->Do_user->savebillingaddress($param); 
                    if($res){
                     return $res;   
                        
                    }else{
                        return false;   
                    }
            }   
        }

        public function getuseraddrdtl($param)
        {
            if(isset($param))
            {
                $res=$this->ci->Do_user->getuseraddrdtl($param); 
                    if($res){
                        
                     return $res;   
                        
                    }else{
                    return false;   
                        
                    }
            }
        }
        
        public function getuserdata()
        {
            if($_SESSION['usrid'])
            {
                //print_R($_SESSION);
                $param['usrid']=$_SESSION['usrid'];
                $res=$this->ci->Do_user->getuserdata($param); 
                //print_r($res);
                    if($res){
                        
                     return $res;   
                        
                    }else{
                     return false;  
                    }
            }
        }
        
        
        public function getStates()
        {
            if($_SESSION['usrid'])
            {
                $res=$this->ci->Do_user->getStates(); 
                    if($res){
                        
                     return $res;   
                        
                    }else{
                     return false;  
                    }
            }
        }

        //created by viki on 26/3/2018
        public function getsignupStates(){
          $res=$this->ci->Do_user->getStates(); 
            if($res){
                return $res;   
            }else{
                return false;  
            }
        }

        public function getshippingid($param=false){
            
         if($_SESSION['usrid'])
            {
                $param['usrid']=$_SESSION['usrid'];
                $res=$this->ci->Do_user->getshippingid($param); 
                    if($res){
                        
                     return $res;   
                        
                    }else{
                    return false;   
                        
                    }
            }   
        
        }

        public function saveforgotpassword($param){
            //print 'zz';print_R($param);die;
            
            $res=$this->ci->Do_user->saveforgotpassword($param); 
            
            return $res;

        }

        public function insertnewsletter($param=false)
        {
          //print_r($param);die;
             if(isset($param)){
                 if (!filter_var($param['email'], FILTER_VALIDATE_EMAIL)) { 
                      //echo  "Invalid email format"; exit;
                    return false;
                    }else{
                        if(!empty($_SESSION['usrid'])){
                          $param['usrid']=$_SESSION['usrid'];
                        }
                        $param['ipaddress']=$param['ipaddress'];
                        $res=$this->ci->Do_user->insertnewsletter($param); 
                       if($res[0]['email']){
                          return $res;
                        }else{
                          if($res){
                           return $res;   
                          }else{
                              return false;
                          }
                      }
                        
                    }
             }else{
                 echo "please enter the email"; exit; 
             }
        }
        function updatenewsletter($id){
           $res=$this->ci->Do_user->updatenewsletter($id); 
           if($res){
               return true;   
           }else{
               return false;
           }
        }

        public function checkpincode($param=false){
            if(isset($param))
            {
                $res=$this->ci->Do_user->checkpincode($param); 
                //print_r($res);die;
                    if($res){
                        
                     return $res;   
                        
                    }else{
                    return false;   
                        
                    }
            }
        }

         public function getmapdtl($param=false){
            
          $res=$this->ci->Do_user->getmapdtl($param); 
              if($res){
                  
               return $res;   
                  
              }else{
              return false;   
                  
              }
            
        }
		
		 public function getevento($param=false)
        {
            
          $res=$this->ci->Do_user->getevento($param); 
              if($res){
                  
               return $res;   
                  
              }else{
              return false;   
                  
              }
            
        }

           public function getmap($param=false)
        {
            
          $res=$this->ci->Do_user->getmap($param); 
        //  print_r($res);
              if($res){
                  
               return $res;   
                  
              }else{
              return false;   
                  
              }
            
        }

        
          public function savefeedback($param=false)
        {
            
          $res=$this->ci->Do_user->savefeedback($param); 
          
              if($res){
               return true;
                  
              }else{
              return false;   
                  
              }
            
        }
		
		public function checklogin(){
			
			if($this->ci->libsession->isSetSession('usrid')){
				$data['login']=true;
				$data['error']=false;
				
			}else{
				$data['login']=false;
				$data['error']=true;
				
			}
			return $data;
		}


    public function pincodavailabilitycheck($param){
        if($param['pincode']){
            $res=$this->ci->Do_user->pincodavailabilitycheck($param);
            if($res){
                $data=array('status'=>true, 'msg'=>$res, 'error'=>false);
            }else{
                $data=array('status'=>false, 'msg'=>$res, 'error'=>true);
            }
        }else{
          $data=array('status'=>true, 'msg'=>'No parameters received', 'error'=>false);
        }
        return $data;
    }

    public function newsletterunsub($param){
        if($param['email']){
            if($this->Do_user->newsletterunsub($param)){
                return true;
            }
        }else{
          return false;
        }
    }
}?>