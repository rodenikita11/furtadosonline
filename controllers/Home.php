<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MY_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->library('user_agent');
        // echo "please hold! while we are work on your request";
        // ob_flush();flush(); 
    }
    
    public function index($param1=false, $param2=false){
        
      //  echo "<img src='https://www.furtadosonline.com/assets/web/images/OFFLINE.jpg' />"
      //  exit;
    	date_default_timezone_set('Asia/Kolkata');
        //ob_flush();flush(); 
        //sleep(20); 
        //die('here');
        /*echo '<pre>';
    	print_r($this->libsession->getallsessions()); die;*/
        //error_log("\n befor calling libmenu->get_dropdownmenu() ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
        $name1= $this->uri->segment('1');
        $name= str_replace("-", " ", $name1);
        //print $name; die;
        $id=$this->uri->segment('2');
        //$param['catid']=intval(206);
        //print_R($id);
        if ($name && $id){
        	// error_log("\n befor calling libmenu->get_dropdownmenu() ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['menus'] = $this->libmenu->get_dropdownmenu();
            // error_log("\n befor calling liblogin->getsignupStates() ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
                     
            $data['signupstate']= $this->liblogin->getsignupStates();
            $param['id'] = intval($id);
            $param['name']=$name;
            
            if($_GET['filter']){
                $param['filterdtl'] = $_GET['filter'];
            }
            if($_GET['search']){
                $param['search'] = $_GET['search'];
            }
            if($_GET['brand']){
                $param['branddtl'] = $_GET['brand'];
            }
            if($_GET['avaliable']){
                $param['avaliable'] = $_GET['avaliable'];
            }
            if($_GET['sort']){
                $param['sort'] = $_GET['sort'];
            }
            if($_GET['pricestart']){
                $param['minprice'] = $_GET['pricestart'];
            }
            if($_GET['pricend']){
                $param['maxprice'] = $_GET['pricend'];
            }
            if($_GET['percentage']){
                $param['percentage'] = $_GET['percentage'];
            }
            //print_r($param); die;
            // print_R($catname);
            //error_log("\n befor calling liburl->getinfotype ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
            $getdtl = $this->liburl->getinfotype($param);
            //error_log("\n after calling liburl->getinfotype ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
            //echo '<pre>';
            //print_r($getdtl); die;
            if($_SERVER['REMOTE_ADDR']=='162.158.50.90'){
                //die($getdtl['type']);
            }

            if ($_GET['sid']){
                $getdtl['type'] = 'product';
            }
            
            if ($name1 == 'giftcard'){
                if ($_REQUEST['start']){
                    $param['start'] = $_REQUEST['start'] + LIMIT;
                } else {
                    $param['start'] = '0';
                }

                $param['cid']            = intval($id);
                $data['banner_category'] = $this->lib_home->get_home_dtl();
                $data['giftcarddetail']  = $this->liburl->get_categorydetails($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
               //echo $mobile; die;
                if (!empty($mobile)) {
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view("mobile/offercard", $data);
                } else {
                    
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/offercard", $data);
                    $this->load->view('web/include/footer', $data);
                }
            } else if ($name1 == 'promotion'){
                $param['id']= intval($id);
                $data['promotionproduct'] = $this->libprodtl->getproductpromotiondtl($param);
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/promotion_products", $data);
            } else if ($getdtl['type'] == 'product'){
                $data['seomodule'] = 'product';
                $data['seoname']   = $this->uri->segment(1);
                $data['seoid']     = $this->uri->segment(2);
                
                $param['sid'] = urldecode($_GET['sid']);
                // print_r($param['sid']); echo 'hiii123456';
                if (empty($param['sid'])){
                    $data['res']= $this->liburl->get_productdetail($param);
                    $param['catid'] = $data['res']['catid'];
                    // print '<pre>';print_r($data['res']); die;
                    $data['comboproducts']= $this->libpromooffer->getComboProductDetails($param);
                    //print_R($data['comboproducts']); die;
                    
                } else {
                    $data['res'] = $this->liburl->pro_clerance_dtl($param);
					// print_r($data['res']); die;
                }
                if ($data['res']['discountPrice'] >= MIN_PRICE_FOR_EMI_5_BANK){
                    $data['res']['emi_month']  = $this->pmt(12, 2, $data['res']['discountPrice']);
                    $data['res']['idMinPrice'] = $data['res']['emi_month'];
                }
                //print_r($data); die;
                $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param);
                $data['similar_product'] = $this->libprodtl->getsimiliar_prod($param);
                $data['res2']            = $this->libprodtl->getrecently_viewed($param);
                $mobile                  = $this->agent->is_mobile();
                $desktop                 = $this->agent->is_browser();
                $data['sid']           	 = urldecode($_GET['sid']);


                if (!empty($mobile)){
                    $this->load->view('mobile/include/header', $data);
                    if($data['res']['proid'] || $param['sid']){
                        $this->load->view("mobile/product_detail", $data);
                    }else{
                        $this->load->view("mobile/not_available", $data);
                    }
                } else {
                    $param['name']         = 'categorydetail';
                    $data['productId']     = $id;
                    
                    $data['categorynames'] = $this->libmenu->getprodtlbredacrum($param);
                    $data['res2']          = $this->liburl->get_categorydetails($param);
                    $data['resp']          = $this->lib_home->get_clerance();
            
                    $this->load->view('web/include/header', $data);
                    if($data['res']['proid'] || $param['sid']){
                        $this->load->view("web/category_detail", $data);
                    }else{
                        $this->load->view("web/not_available", $data);
                    }
                    $this->load->view('web/include/footer', $data);
                }
                
            }else if ($getdtl['type'] == 'category'){
                $data['seoname'] = $this->uri->segment(1);
                if ($_REQUEST['start']){
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }
                $param['percentage1'] = $param['percentage'];
                $param['cid']   = $param['id'];
                $data['seoid']  = $param['cid'];
                $param['count'] = true;

                if($_REQUEST['start']){
                          $param['start']=$_REQUEST['start'];
                          $paramx['start']='/'.$_REQUEST['start'];
                  }else{
                      $param['start']=0;
                      $paramx['start']='/0';
                  }
                  if($_REQUEST['catid']){
                      $param['catid']=$_REQUEST['catid'];
                      $paramx['catid']= '&scat='.$param['catid'];
                  }
                  if($_REQUEST['level']){
                      $param['level']=$_REQUEST['level'];
                  }
                  if($_REQUEST['searchText']){
                      $param['search']=addslashes($_REQUEST['searchText']);
                      $paramx['search']='/'.addslashes($_REQUEST['searchText']);
                  }else{
                      $param['search']=addslashes($_REQUEST['search']);
                      $paramx['search']='/'.addslashes($_REQUEST['search']);
                  }
                  if($_REQUEST['catname']){
                      $param['catname']=$_REQUEST['catname'];
                  }
                  if($_REQUEST['maincat']){
                      $param['maincat']=$_REQUEST['maincat'];
                      $paramx['itemtype']= '&itemtype='.$param['maincat'];
                  }
                  if($_REQUEST['filter']){
                      $param['filterdtl']=$_REQUEST['filter'];
                  }
                  if($_REQUEST['brand']){
                      $param['branddtl']=$_REQUEST['brand'];
                      $paramx['branddtl'] = str_replace( array('[',']') , ''  , $param['branddtl']);
                      $param['branddtl'] = explode(',',$paramx['branddtl']);
                      //$paramx['branddtl']='&brandid='.$paramx['branddtl'].'.0';
					  $paramx['branddtl']='&brandid=';
						foreach ($param['branddtl'] as $value) {
						 $paramx['branddtl'] .=$value.'.0,';
						}
                      // var_dump($param['branddtl'], $paramx['branddtl']); exit;
                  }
                  if($_REQUEST['discount']){
                    $temp =explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']));
                    $temp_len = count($temp) - 1;
                    $param['discountdetail'] = array( 0 => $temp[0]); 
                        // $paramx['discount'] = '&min_perc='. ($temp[$temp_len] - 1); // as api needs one less
                      if(($temp[0] == $temp[1]) && $temp[1] > 1){
                        $temp[0] = $temp[0] - 1; // as min - max requires point 1 difference
                      } 
                        $paramx['discount'] = '&min_perc='. $temp[0] . '&max_perc='. $temp[1]; // as api needs one less
                        //echo '<pre>';
                        // print_r($param['discountdetail']); die;
                  }
                      
                  if(isset($_REQUEST['avaliable'])){
                      $param['avaliable']=$_REQUEST['avaliable'];
        
                      $avaliable   = str_replace( array('[',']') , ''  , $param['avaliable']);
                      $avaliable = explode(',', $avaliable);
                      $param['avaliable']= $avaliable;
        
                      if( count($avaliable) == 1 ){
                        $paramx['available']='&isavailable='.$avaliable[0];
                      }
                  }
            /* 
                      if($param['avaliable']){
                        $param['avaliable']   = str_replace( array('[',']') , ''  , $param['avaliable']);
                        //  echo $param['avl'].'hiii1';
                        $whr2.=" and pm.isAvailable in(".$param['avaliable'].")";
                      } */
            
                      if($_REQUEST['sort']){
                          $param['sort']=$_REQUEST['sort'];
                      }
                     if($_REQUEST['pricestart']){
                         // echo 'under this';
                          $param['minprice'] =$_REQUEST['pricestart'];
                          $paramx['minprice'] ='&rmin='.$_REQUEST['pricestart'];
                      }
                      if($_REQUEST['pricend']){
                          $param['maxprice']=$_REQUEST['pricend'];
                          $paramx['maxprice'] ='&rmax='.$_REQUEST['pricend'];
                          if(!isset($paramx['minprice'])){
                            $paramx['minprice'] ='&rmax=1';
                          }
                      }
            
                      if($pagestart){
                          $param['pagestart']=$pagestart;
                          $paramx['pagestart']='/'.$pagestart;
                      }else{
                          $param['pagestart']=0;
                          $paramx['pagestart']='/0';
                      }
            
                      if($_REQUEST['sort']){
                        // the api format currenty is 
                        //http://3.210.70.215:8080/search/api/v1.0/product/piano/0/2?itemtype=1&min_perc=5.0&orderby=discountprice&ordertype=asc
                          $param['sort']=$_REQUEST['sort'];
                          // lets create a sort too
            
                          if(isset($temp[0])){
                            $mindis = $temp[0];
                          }
                          else{
                            $mindis = 0;
                          }
            
                          // if we got the high price and nothing
                          if( $param['sort'] == 'highprice'){
                              $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discountprice&ordertype=desc';
                          }
                          // if we got the low price and nothing
                          else if( $param['sort'] == 'lowprice'){
                              $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discountprice&ordertype=asc';
                          }            
                          // if we got the max discount
                          else if( $param['sort'] == 'maxdiscount' ){
                              $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discpercent&ordertype=desc';
            
                          }
                          // if we got the min discount                
                          else if( $param['sort'] == 'mindiscount' ){
                              $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discpercent&ordertype=asc';
            
                          }
                    }
                //print_r($param);die;
                //print_r($param); die;
                if($this->uri->segment(3)){
                    $param['start']=$this->uri->segment(3);
                }else{
                    $param['start']=0;
                }

                //$data['res']= $this->liburl->get_categorydetails($param);
                $data['res']= $this->liburl->get_categorydetailsX($param, $paramx);
                $data['secondsubcat']= $this->libmenu->get_dropdownmenu();
                $data['discountdetailer'] = $param['discountdetail'];
                //echo '<pre>';
                //print_r($data['res']['percentage']); die;
                
                //$data['getfilter'] = $this->liburl->getfilter($param);
                  $data['getfilter']['brand'] = $data['res']['brandfilter'];
                   
                  $data['getfilter']['minprice'] = $data['res']['minprice'];
                  $data['getfilter']['maxprice'] = $data['res']['maxprice'];
                  $data['getfilter']['mindisc'] = $data['res']['mindiscount'];
                  $data['getfilter']['maxdisc'] = $data['res']['maxdiscount'];
                  $data['filter'] = $data['res']['catfilter'];
                  
                  $data['param'] = $param;
                  // $data['getcatname']      = $this->libmenu->getcatname($param);
                $data['getsubcat'] = $this->libmenu->getsubcatname($param);
                // var_dump('<pre>', $data['getfilter']['brand'],$data['getfilter']['brand'][0]['brandname']);
                if(empty($data['getsubcat'])){
                   $data['getsubcat'] = $this->libmenu->getcatname($param);
                   foreach ($data['filter'] as $key => $cat) {
                    foreach ($cat as $catname => $subcat) {
                      if($subcat['sub']){
                          unset($data['filter'][$key][$catname]['sub'][0]);
                          foreach ($data['getsubcat']["subcategory"] as $index => $value) {
                           $data['filter'][$key][$catname]['sub'][$index]['name'] =  $value['catname'];
                           $data['filter'][$key][$catname]['sub'][$index]['url'] = SITEMOBURL.str_replace(array(' ','&',',','--'),'-',strtolower($value['catname'])).'/'.$value['catid'];
                          }                       
                      }
                    }
                  }
                }
                else{
                  foreach ($data['filter'] as $key => $cat) {
                    foreach ($cat as $catname => $subcat) {
                      if($subcat['sub']){
                          unset($data['filter'][$key][$catname]['sub'][0]);
                          foreach ($data['getsubcat'] as $index => $value) {
                           $data['filter'][$key][$catname]['sub'][$index]['name'] =  $value['catname'];
                           $data['filter'][$key][$catname]['sub'][$index]['url'] = SITEMOBURL.str_replace(array(' ','&',',','--'),'-',strtolower($value['catname'])).'/'.$value['catid'];
                          }                       
                      }
                    }   
                  }
                }
                // if(empty($data['filter'])){
                //     $data['filter']=
                //   }


                // print_r($data['getsubcat'],$data['filter']);// die;
                // var_dump('<pre>',$data['filter']);

                /*$data['getfilter']['brand']=$this->liburl->getfilterbrand($param);
                $filteravailable=$this->liburl->getfilteravailable($param);
                $data['getfilter']['avalibale']['cnt']=$filteravailable[0]['proavailablecnt'];
                $filternotavailable=$this->liburl->getfilternotavailable($param);
                $data['getfilter']['notavaliable']['cnt']=$filternotavailable[0]['pronotavailablecnt'];
                $filterprocnt=$this->liburl->getfilterprocnt($param);
                $data['getfilter']['procnt']['cnt']=$filterprocnt[0]['procnt'];
                $filterminprice=$this->liburl->getfilterminprice($param);
                $data['getfilter']['minprice']=$filterminprice[0]['minprice'];
                $filtermaxprice=$this->liburl->getfiltermaxprice($param);
                $data['getfilter']['maxprice']=$filtermaxprice[0]['maxprice'];*/
                
                // $data['getsubcat'] = $this->liburl->getsubcat($param);
                $data['getcat']    = $this->liburl->getcat($param);

                $data['start']     = $param['start'];
                $mobile            = $this->agent->is_mobile();
                $desktop           = $this->agent->is_browser();

                $brands=array();
                foreach($data['res']['product'] as $resval){
                    $brands[$resval['brandid']]=$resval['brandname'];
                }
                $brandsstring=implode(',', $brands);
                //$catbrands=from here it is pending
                $catseoname=str_replace('-', ' ', $this->uri->segment(1));
                //$data['metadescription']="Find your next ".$catseoname." from our extensive selection online with all the major brands like ".$brandsstring." in India. Best Prices , Quick Delivery & 100% genuine ".$catseoname."";
                $data['metadescription']=$data['res']['meta_description'];
                //echo $data['res']['meta_description']; die;

                //echo '<pre>';
                //print_r($data); die;
                //if ($param['start'] > 0){
                    
                    /*if(!empty($mobile)){
                        $this->load->view("mobile/ajax_productlist", $data);
                    } else {
                        $this->load->view("web/include/ajax_productlist", $data);
                    }*/
                //} else {
                    
                    $mobile  = $this->agent->is_mobile();
                    $desktop = $this->agent->is_browser();
                    
                    if (!empty($mobile)){
                        $this->load->view('mobile/include/header', $data);
                        $this->load->view("mobile/category_listing", $data);
                    } else {
                        //print_r($data); die;
                        $param['id']             = intval($id);
                        $catName                 = $this->uri->segment(1);
                        $data['catname']         = $catName;
                        $catId                   = $this->uri->segment(2);
                        $data['catid']           = $catId;
                        $data['maincaturl']      = ADMINURL . $catName . '/' . $catId;
                        //echo "hi"; die;    
                        //print '<pre>'; print_r($data);die; 
                        //print_r($param);
                        $param['categoryname']   = $data['catname'];
                        $param['categoryno']     = $catId;
                        $data['getbrandname']    = $this->libmenu->getbrandname($param);
                        $param['name']           = 'categorymain';
                        $data['categorynames']   = $this->libmenu->getprodtlbredacrum($param);
                        $data['getcatname']      = $this->libmenu->getcatname($param);
                        //print_r($data['categorynames']);
                        $data['banner_category'] = $this->lib_home->get_home_dtl();
                        $this->load->view('web/include/header', $data);
                        $this->load->view("web/category", $data);
                        $this->load->view('web/include/footer', $data);
                    }
               // }
            }else if($getdtl['type'] == 'brand'){
            	$param['brandid']=(int)$id;
				$data['brandid']=$param['brandid'];
				$param['search']=$param['brandname']=$data['brandname']=$name1;
				
				if($_REQUEST['start']){
				  $param['start']=$_REQUEST['start'];
				  $paramx['start']='/'.$_REQUEST['start'];
				}else{
				  $param['start']=0;
				  $paramx['start']='/0';
				}
				if($_REQUEST['catid']){
				  $param['catid']=$_REQUEST['catid'];
				  $paramx['catid']= '&scat='.$param['catid'];
				}
				if($_REQUEST['level']){
				  $param['level']=$_REQUEST['level'];
				}
				if($_REQUEST['searchText']){
				  $param['search']=addslashes($_REQUEST['searchText']);
				  $paramx['search']='/'.addslashes($_REQUEST['searchText']);
				}else{
				  $param['search']=addslashes($_REQUEST['search']);
				  $paramx['search']='/'.addslashes($_REQUEST['search']);
				}
				if($_REQUEST['catname']){
				  $param['catname']=$_REQUEST['catname'];
				}
				if($_REQUEST['maincat']){
				  $param['maincat']=$_REQUEST['maincat'];
				  $paramx['itemtype']= '&itemtype='.$param['maincat'];
				}
				if($_REQUEST['filter']){
				  $param['filterdtl']=$_REQUEST['filter'];
				}
				if($_REQUEST['brand']){
				  $param['branddtl']=$_REQUEST['brand'];
				  $paramx['branddtl'] = str_replace( array('[',']') , ''  , $param['branddtl']);
				  $param['branddtl'] = explode(',',$paramx['branddtl']);
				  $paramx['branddtl']='&brandid=';
					foreach ($param['branddtl'] as $value) {
					 $paramx['branddtl'] .=$value.'.0,';
					}
				}
				if($_REQUEST['discount']){
				$temp =explode(',', str_replace(array('[', ']', '"'), '', $_REQUEST['discount']));
				$temp_len = count($temp) - 1;
				$param['discountdetail'] = array( 0 => $temp[0]); 
				  if(($temp[0] == $temp[1]) && $temp[1] > 1){
					$temp[0] = $temp[0] - 1; // as min - max requires point 1 difference
				  } 
					$paramx['discount'] = '&min_perc='. $temp[0] . '&max_perc='. $temp[1]; // as api needs one less
				}
				  
				if(isset($_REQUEST['avaliable'])){
				  $param['avaliable']=$_REQUEST['avaliable'];
				  $avaliable   = str_replace( array('[',']') , ''  , $param['avaliable']);
				  $avaliable = explode(',', $avaliable);
				  $param['avaliable']= $avaliable;

				  if( count($avaliable) == 1 ){
					$paramx['available']='&isavailable='.$avaliable[0];
				  }
				}
				if($_REQUEST['sort']){
					$param['sort']=$_REQUEST['sort'];
				}
				if($_REQUEST['pricestart']){
				  $param['minprice'] =$_REQUEST['pricestart'];
				  $paramx['minprice'] ='&rmin='.$_REQUEST['pricestart'];
				}
				if($_REQUEST['pricend']){
				  $param['maxprice']=$_REQUEST['pricend'];
				  $paramx['maxprice'] ='&rmax='.$_REQUEST['pricend'];
				  if(!isset($paramx['minprice'])){
					$paramx['minprice'] ='&rmax=1';
				  }
				}
				if($pagestart){
				  $param['pagestart']=$pagestart;
				  $paramx['pagestart']='/'.$pagestart;
				}else{
				  $param['pagestart']=0;
				  $paramx['pagestart']='/0';
				}
				if($_REQUEST['sort']){
					$param['sort']=$_REQUEST['sort'];
					if(isset($temp[0])){
						$mindis = $temp[0];
					}
					else{
						$mindis = 0;
					}
					// if we got the high price and nothing
					if( $param['sort'] == 'highprice'){
					  $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discountprice&ordertype=desc';
					}
					// if we got the low price and nothing
					else if( $param['sort'] == 'lowprice'){
					  $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discountprice&ordertype=asc';
					}            
					// if we got the max discount
					else if( $param['sort'] == 'maxdiscount' ){
					  $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discpercent&ordertype=desc';
					}
					// if we got the min discount                
					else if( $param['sort'] == 'mindiscount' ){
					  $paramx['sort'] = '&min_perc='.$mindis.'&orderby=discpercent&ordertype=asc';
					}
				}
				if($this->uri->segment(3)){
					$param['start']=$this->uri->segment(3);
				}else{
					$param['start']=0;
				}
				$data['module']=$param['module']='brandsfilters';
				//$data['param']=$param;
				$data['res']=$this->lib_home->brandsX($param, $paramx);
				$data['secondsubcat']=$this->libmenu->get_dropdownmenu();
				
				$brandsmeta=$this->liburl->brandsmeta($param);
                $data['res']['meta_title']=$brandsmeta[0]['seo_title'];
                $data['res']['meta_description']=$brandsmeta[0]['seo_metadesc'];
                $data['res']['meta_keyword']=$brandsmeta[0]['seo_metakeyword'];
                $data['res']['seo_footerdescription']=$brandsmeta[0]['seo_footerdescription'];

				//var_dump($data['res']); die();
				$data['getfilter']['brand'] = $data['res']['brandfilter'];
				$data['getfilter']['minprice'] = $data['res']['minprice'];
				$data['getfilter']['maxprice'] = $data['res']['maxprice'];
				$data['getfilter']['mindisc'] = $data['res']['mindiscount'];
				$data['getfilter']['maxdisc'] = $data['res']['maxdiscount'];
				$data['filter'] = $data['res']['catfilter'];
				
				
				if (!empty($mobile)){
					$this->load->view('mobile/include/header',$data);
					$this->load->view('mobile/category_listing',$data);
				}else{
					$data['leftsidefalse']=true;
					$data['banner_category']=$data['webbanner']=$this->lib_home->get_home_dtl();
					$this->load->view('web/include/header',$data);
					$this->load->view("web/category",$data);
					$this->load->view('web/include/footer',$data);
				}
				
				
				// error_log("\n In brand else ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                /*if ($_REQUEST['start']){
                    $param['start'] = $_REQUEST['start']; //+LIMIT;
                } else {
                    $param['start'] = '0';
                }
                
                if($_REQUEST['catid']){
                  $param['catid']=$_REQUEST['catid'];
                }
                if($_REQUEST['level']){
                  $param['level']=$_REQUEST['level'];
                }
                if($_REQUEST['searchText']){
                    $param['search']=addslashes($_REQUEST['searchText']);
                }else{
                    $param['search']=addslashes($_REQUEST['search']);
                }
                if($_REQUEST['catname']){
                    $param['catname']=$data['catname']=$_REQUEST['catname'];
                }
                if($_REQUEST['maincat']){
                    $param['maincat']=$_REQUEST['maincat'];
                }
                if($_REQUEST['filter']){
                    $param['filterdtl']=$_REQUEST['filter'];
                }
                if($_REQUEST['brand']){
                    $param['branddtl']=$_REQUEST['brand'];
                }
                if($_REQUEST['avaliable']){
                    $param['avaliable']=$_REQUEST['avaliable'];
                }
                if($_REQUEST['sort']){
                    $param['sort']=$_REQUEST['sort'];
                }
                if($_REQUEST['pricestart']){
                    $param['minprice'] =$_REQUEST['pricestart'];
                }
                if($_REQUEST['pricend']){
                    $param['maxprice']=$_REQUEST['pricend'];
                }

                if($this->uri->segment(3)){
                	$param['pagestart']=$this->uri->segment(3);
                }else{
                	$param['pagestart']=0;
                }
                
                $param['brandid']=intval($id);
                //$param['search']=$param['brandname']=$name1;
                $param['search']=$param['brandname']=$data['brandname']=$name1;
                //echo $param['start']; die;
                $data['module']=$param['module']='brandsfilters';
                // error_log("\n before calling libsearch->catfilter ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                $catfilter=$this->libsearch->catfilter($param);
                //echo '<pre>';
                //print_r($catfilter); die;
                $data['filter']=$catfilter;
                // error_log("\n before calling libsearch->prouctsearch ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                $data['res']= $this->libsearch->prouctsearch($param);
                // error_log("\n after calling libsearch->prouctsearch ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                //print_r($data['res']); die;
                $data['resp']['product']=$data['res']['product'];
                $data['resp']['links']=$data['res']['links'];

                $metacats=array();
                foreach($data['resp']['product'] as $brandmeta){
                    $metacats[$brandmeta['third_catid']]=$brandmeta['third_category'];
                }
                // error_log("\n before calling liburl->brandsmeta ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                $brandsmeta=$this->liburl->brandsmeta($param);
                // error_log("\n after calling liburl->brandsmeta ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            
                $data['res']['meta_title']=$brandsmeta[0]['seo_title'];
                $data['res']['meta_description']=$brandsmeta[0]['seo_metadesc'];
                $data['res']['meta_keyword']=$brandsmeta[0]['seo_metakeyword'];
                $data['res']['seo_footerdescription']=$brandsmeta[0]['seo_footerdescription'];

                $metacatsstring=implode(',', $metacats);

                //$data['metadescription']="Wide range of  musical instruments including ".$metacatsstring." from ".$param['brandname']." is now avaliable at upto 35% discount with Best Prices , Quick Delivery & 100% genuine product";
                /*if(!empty($mobile)){
                    //print '<pre>';print_r($data['hotdeals']); die;
                    $data['banner']   = $this->lib_home->get_home_dtl();
                    $data['category'] = $data['banner'];//$this->lib_home->get_home_dtl();
            
                    $this->load->view('mobile/include/header', $data);
                    $this->load->view('mobile/product_searching',$data);
                }else{
                    //echo "hi"; die;
                    $data['leftsidefalse']=true;
                    $data['webbanner']       = $this->lib_home->get_home_dtl();
                    $data['banner_category'] = $data['webbanner'];   // $this->lib_home->get_home_dtl();
                    //print_r($data); die;
                    $this->load->view('web/include/header', $data);
                    $this->load->view("web/searchproduct",$data);
                    $this->load->view('web/include/footer', $data);
                }*/

                /*if($param['start'] > 0){
                    if(!empty($mobile)){
                         $this->load->view('mobile/ajax_productsearch',$data);
                    }else{
                        $this->load->view("web/include/ajaxsearchproductlist", $data);
                    }
                }else{*/
                    /*if (!empty($mobile)){
                        $this->load->view('mobile/include/header',$data);
                        $this->load->view('mobile/product_searching',$data);
                    }else{
                        $data['leftsidefalse']=true;
                        $data['banner_category']=$data['webbanner']=$this->lib_home->get_home_dtl();
                        $this->load->view('web/include/header',$data);
                        $this->load->view("web/searchproduct",$data);
                        $this->load->view('web/include/footer',$data);
                    }*/
                //}
            }
        } else {
            $param['module']         = 'homehotdeals';
            //error_log("\n befor calling clearance ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['clerance']        = $this->lib_home->get_clerance();
            // $data['newarrivals']     = $this->lib_home->newarrivalX($param);
            //error_log("\n befor calling new arrival ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['newarrivals']     = $this->lib_home->newarrival($param);
            // $data['hotdeals']        = $this->lib_home->hotdealsX($param);
            //error_log("\n befor calling hot deal ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['hotdeals']        = $this->lib_home->hotdeals($param);
           // error_log("\n Hotdeal". date('Y-m-d H:i:s').print_r($data['hotdeals'],1), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['signupstate']     = $this->liblogin->getsignupStates();
            //print '<pre>'; print_r($data['states']); die('hiii');
            // $data['newarrivalbooks'] = $this->lib_home->newarrivalbooksX($param);
            //error_log("\n befor calling newarrivalbooks ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['newarrivalbooks'] = $this->lib_home->newarrivalbooks($param);
            // print_R($data['newarrivals']); die('hiiii123');
            //error_log("\n befor calling menu ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
            $data['menus']           = $this->libmenu->get_dropdownmenu();
            //echo '<pre>';
            //print_r($data['menus']); die;
            $mobile                  = $this->agent->is_mobile();
            $desktop                 = $this->agent->is_browser();
            //$mobile=true;
            //echo '<pre>';
            //print_r($data['menus']); die;
            $data['res']['meta_description']='Furtados – Musical Instruments & Equipment Retailer in India for pianos,keyboards,guitars,drums & more.✓Best Prices✓Best Brands✓Quick Delivery  | Online Store';
            
            $data['res']['meta_title']='Buy Musical Instruments & Equipment Online store in India, Furtados';
            if(!empty($mobile)){
                //print '<pre>';print_r($data['hotdeals']); die;
                $data['banner']   = $this->lib_home->get_home_dtl();
                $data['category'] = $data['banner'];//$this->lib_home->get_home_dtl();
        
                $this->load->view('mobile/include/header', $data);
                $this->load->view("mobile/home", $data);
            }else{
                //echo "hi"; die;
                //error_log("\n befor calling banner ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
                $data['webbanner']       = $this->lib_home->get_home_dtl();
                $data['banner_category'] = $data['webbanner'];   // $this->lib_home->get_home_dtl();
                //error_log("\n After calling Banner ".date('Y-m-d H:i:s'), 3, "/webapps/furtadosonline/logs/my-errors.log");
                //echo '<pre>';
                //print_r($data['banner_category']); die;
                $this->load->view('web/include/header', $data);
                $this->load->view('web/index', $data);
                $this->load->view('web/include/footer', $data);
            }
        }
    }
    
    public function priceFormat($amt){
        setlocale(LC_MONETARY, 'en_IN');
        return money_format('%!.0n', floor($amt)) . '/-';
    }
    
    public function pmt($apr, $term, $loan)
    {
        $term   = $term * 12;
        $apr    = $apr / 1200;
        $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
        return $amount;
    }
}
?>
