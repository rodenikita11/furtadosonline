<style type="text/css">
   h2.offerh2 {
    font-size: 18px;
    text-align: center;
    margin: 8px 0px 10px;
   }
   .m-b-10{
    margin-bottom: 20px;
   }
   
</style>


<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
   <?if($getpromotiondtl){?>
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150 inner-top-md-20 ">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
               <div class="wide-banners wow fadeInUp outer-bottom-xs">
                  <div class="row">
                     <h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Offer Zone</h2>
                     <!--div class="col-md-12 col-sm-12 m-b-10">
                        <div class="wide-banner cnt-strip">
                           <div class="image"> <img class="img-responsive" src="<?php echo WEBIMG?>banners/add.png" alt="" > </div>
                        </div>
                     </div-->
                  </div>
                  <?foreach($getpromotiondtl as $banner){?>
                     <div class="clear">
                     <div class="col-sm-4 col-md-4 col-xs-12 m-b-10 edate" id="eDate" style="padding-left: 2px; padding-right: 2px;" data-date='<?php echo $banner['endDate'];?>'>
                        <div class="products">
                           <div class="product" style="border: 1px solid #d5d8e0;padding: 0px">
                              <div class="product-image">
                                 <div class="image" > <a href="<?php echo $banner['url'];?>"><img  <?if(isset($banner['img'])){?> src="<?php echo $banner['img'];?>" <?}else{?> src="<?php echo WEBIMG?>banners/Noimage (1).jpg" <?}?> alt="" style="max-height: 186px;"></a> </div>
                                 <!-- /.image -->
                              </div>
                              <!-- /.product-image -->
                              <div class="product-info text-left bgtr" style="height: 94px;">
                                 <div class="product-price"> 
                                    <h1 class="offerh1" style="font-size: 16px; font-weight: 600;line-height: 1.2; margin-top: 5px;margin-bottom: 5px;text-transform: capitalize;text-align: center;"><a style="color: #cd2327 !important; font-size: 14px;" href="<?php echo $banner['url'];?>"><?php echo strtoupper($banner['title']);?></a></h1>
                                 </div>
                                 <div class="product-price2" style=" margin-top: 14px;text-align: center;">
                                    <?if($banner['promotionofferDisplay'] == 1 ) {?>
                                    <span style="color: #cc0028;font-size: 16px;font-weight: 600; text-transform: UPPERCASE;">
                                       <?php $date1=$banner['endDate'];
                                             $date=date_create($date1);
                                             echo 'OFFER ENDS ON '.date_format($date,"jS F Y");
                                       ?>
                                    </span>
                                    <?} else if($banner['promotionofferDisplay'] == 2 ){?>
                                    <p id="demo" class="time" style="color: #cc0028;font-size: 16px;font-weight: 600;text-align: center;"></p>
                                    <?}?>
                                 </div>
                              </div>
                              <!-- /.product-info -->
                           </div>
                           <!-- /.product --> 
                        </div>
                        <!-- /.products --> 
                     </div>
                  </div>
                  <?}?>

                  
                  
                  
            </div>
         </div>
         <!-- ========================================= SECTION – HERO : END ========================================= --> 
      </div>
   </div>
   <?}else{?>
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder inner-top-md-20 ">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
               <div class="wide-banners wow fadeInUp outer-bottom-xs">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 m-b-10">
                        <div class="wide-banner cnt-strip outer-bottom nooffer">
                          <img src="<?php echo WEBIMG;?>staticpage/nooffer.jpg" style="width:50%">
                        </div>
                     </div>
                  </div>
            </div>
         </div>
         <!-- ========================================= SECTION – HERO : END ========================================= --> 
      </div>
   </div>
   <?}?>
</div>
</div>
<!-- Display the countdown timer in an element --> 

<style type="text/css">
   .clear:nth-child(17) {
    clear: both
}
</style>
<script type="text/javascript">
   setInterval(function(){
      edatele = jQuery('.edate');
      edatele.each(function(){
         enddate = jQuery(this).data('date');
         var countDownDate = new Date(enddate);
         var now           = new Date().getTime();
         // Find the distance between now an the count down date
         var distance = countDownDate - now;
         // Time calculations for days, hours, minutes and seconds
         var days     = Math.floor(distance / (1000 * 60 * 60 * 24));
         var hours    = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
         var minutes  = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
         var seconds  = Math.floor((distance % (1000 * 60)) / 1000);
         // Display the result in the element with id="demo"
         str=" Remaining "+days + " Days " + hours + " Hours "
           + minutes + " Minutes " + seconds + " Seconds ";
         jQuery(this).find('p').html(str);  
         // If the count down is finished, write some text 
         if (distance < 0){
           jQuery(this).find('p').html("OFFER EXPIRED");
         }

      })
   }, 1000)
</script> 