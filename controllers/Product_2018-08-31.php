<?php
  if (!defined('BASEPATH')) exit('No direct script access allowed');
  require('razorpay/razorpay-php/Razorpay.php');
  use Razorpay\Api\Api;
  use Razorpay\Api\Errors\SignatureVerificationError;
  /**
  *
  */
  class Product extends MY_Controller{
    function __construct(){
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->library('Falconide');
    }

    public function index(){
        $this->load->view('mobile/product_listing');
    }

    public function storeproid(){
      $mobile=$this->agent->is_mobile();
      $desktop=$this->agent->is_browser();

        if($_POST){
            $param['pid']=$_POST['proid'];
          if($_POST['sid']){
             $param['sid']=$_POST['sid'];
          }
          if($_POST['comobo_product']==1){
             $param['selected_combo_pro_id']=$_POST['selected_combo_pro'];
          }
          //print_r($param); die;
          $data['res']=$this->libcart->addtocart($param);
          //print_r($data['res']); die;
          if(is_array($data['res'])){
           if(!empty($desktop)){
              //$data['currentcartwithcombo']=$this->load->view('web/include/currentcartwithcombo', $data, true);
           }
            if(!empty($data['res'])){
                 $data['error']= 'false';
              }else{
                  $data['error']='true';
               }

             echo  json_encode($data);
           }
        }
    }


        /*featuredproducts*/
        public function featuredproducts(){
             if($_REQUEST['filter']){
              $param['filterdtl']=$_REQUEST['filter'];
            }
            if($_REQUEST['brand']){
                $param['branddtl']=$_REQUEST['brand'];
            }
            if($_REQUEST['avaliable']){
                $param['avaliable']=$_REQUEST['avaliable'];
            }
            if($_REQUEST['sort']){
                $param['sort']=$_REQUEST['sort'];
            }
            if($_REQUEST['pricestart']){
                $param['minprice'] =$_REQUEST['pricestart'];
            }
            if($_REQUEST['pricend']){
                $param['maxprice']=$_REQUEST['pricend'];
            }
            if($_REQUEST['categoryid']){
                $param['categoryid']=$_REQUEST['categoryid'];
            }
           $data['menus']=$this->libmenu->get_dropdownmenu();
           $data['filter'] = $this->lib_home->get_featuredproductfilter($param);
           //print '<pre>';print_r($data['filter']); die;
          // $data['menus']=$this->libmenu->get_dropdownmenu();
           $data['resp']=$this->lib_home->getfeaturedproduct($param);
          // print '<pre>';print_r($data['resp']); die;
           $data['banner_category']=$this->lib_home->get_home_dtl();
           //  $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
              $desktop=$this->agent->is_browser();
          //  echo 'hiii';
           if (!empty($mobile))
               {
                  //die('fhdaskfgsdk');
                  
                   $this->load->view('mobile/include/header',$data);
                   $this->load->view('mobile/featured_products',$data);
               }
               else
               {
                  //echo '123654';
                  //print_R($data['menus']);
                  $data['banner_category']=$this->lib_home->get_home_dtl();
                   $this->load->view('web/include/header',$data);
                   $this->load->view('web/categorylist',$data);
                   $this->load->view('web/include/footer',$data);
               }

        }

        /*hotdeals*/
        public function hotdeals(){
            if ($_REQUEST['start']){
                $param['start'] = $_REQUEST['start'];
            }else{
                $param['start'] = '0';
            }
            $data['start']=$param['start'];
            if($this->input->post('module') && $this->input->post('module')=='loadhotdeals'){
              $param['module']=$this->input->post('module');
            }
            $data['hotdeals']=$this->lib_home->hotdeals($param);
            $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
            if(!empty($mobile)){
                  //die('fhdaskfgsdk');
                   $this->load->view('mobile/include/header',$data);
                   $this->load->view('mobile/featured_products',$data);
               }else{
                  if($param['module'] && $param['module']=='loadhotdeals'){
                    $data['hotdealsproducts']=$this->load->view('web/hotdealslist', $data, true);
            echo $data['hotdealsproducts']; exit;
                  }else{
                    $data['banner_category']=$this->lib_home->get_home_dtl();
                      $this->load->view('web/include/header',$data);
                      $this->load->view('web/categorylist',$data);
                      $this->load->view('web/include/footer',$data);
                  }
               }
    }

        /*newarrivals*/
         public function newarrivals(){
            $data['newarrivals']=$this->lib_home->newarrival();
              $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
              $desktop=$this->agent->is_browser();
          //  echo 'hiii';
           if (!empty($mobile))
               {
                  //die('fhdaskfgsdk');
                   $this->load->view('mobile/include/header',$data);
                   $this->load->view('mobile/featured_products',$data);
               }
               else
               {
                  //echo '123654';
                  $data['banner_category']=$this->lib_home->get_home_dtl();
                   $this->load->view('web/include/header',$data);
                   $this->load->view('web/categorylist',$data);
                   $this->load->view('web/include/footer',$data);
               }

        }


        /*newarrivalbooks*/
         public function newarrivalbooks(){
            $data['newarrivalbooks']=$this->lib_home->newarrivalbooks();
              $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
              $desktop=$this->agent->is_browser();
          //  echo 'hiii';
           if (!empty($mobile))
               {
                  //die('fhdaskfgsdk');
                   $this->load->view('mobile/include/header',$data);
                   $this->load->view('mobile/featured_products',$data);
               }else{
                  //echo '123654';
                  $data['banner_category']=$this->lib_home->get_home_dtl();
                   $this->load->view('web/include/header',$data);
                   $this->load->view('web/categorylist',$data);
                   $this->load->view('web/include/footer',$data);
               }

        }


         public function promotion(){
            $param['id']=(int)$_GET['id'];
            if($_REQUEST['filter']){
              $param['filterdtl']=$_REQUEST['filter'];
            }
            if($_REQUEST['brand']){
                $param['branddtl']=$_REQUEST['brand'];
            }
            if($_REQUEST['avaliable']){
                $param['avaliable']=$_REQUEST['avaliable'];
            }
            if($_REQUEST['sort']){
                $param['sort']=$_REQUEST['sort'];
            }
            if($_REQUEST['pricestart']){
                $param['minprice'] =$_REQUEST['pricestart'];
            }
            if($_REQUEST['pricend']){
                $param['maxprice']=$_REQUEST['pricend'];
            }
            if($_REQUEST['categoryid']){
              $param['categoryid']=$_REQUEST['categoryid'];
            }
           // print_R($param['id']);
           $data['menus']=$this->libmenu->get_dropdownmenu();
           $data['promotionproduct']=$this->libprodtl->getproductpromotiondtl($param);
           //echo '<pre>';
           //print_r($data['promotionproduct']); die;
           $data['filter'] = $this->libprodtl->get_offerfilter($param);

           //echo '<pre>';
           //print_r($data['filter']); die;
           $data['filter1']='offerzone';

           //print '<pre>'; print_R($data['promotionproduct']); print '</br>'; print_r($data['filter']); die;
            $mobile  = $this->agent->is_mobile();
            $desktop = $this->agent->is_browser();
           if (!empty($mobile))
               {
                //print '<pre>';  print_r($data['filter']); die;
                  $this->load->view('mobile/include/header',$data);
               $this->load->view('mobile/promotion_products',$data);

               }
               else
               {
                  $data['banner_category']=$this->lib_home->get_home_dtl();

                   $this->load->view('web/include/header',$data);
                   $this->load->view('web/offerzonedetails',$data);
                   $this->load->view('web/include/footer',$data);
               }

        }
        
        public function offerzone(){
          $data['menus']=$this->libmenu->get_dropdownmenu();
         $data['res']['meta_title']="Best Deals and Offers on Guitars, Keyboards, Pianos | Deals on Music Instruments Online | Furtados Online";
         $data['res']['meta_keyword']="Best Deals and Offers on Guitars, Pianos and Keyboards";
         $data['res']['meta_description']="Furtados Online: Get the best deals and offers on Guitars, Keyboards, Pianos from one of India’s leading music stores company.  With music stores in Mumbai, Pune, Bangalore, Ahmedabad, Delhi, Goa, we are very close to you.";
  
           $data['getpromotiondtl']=$this->libprodtl->getpromotiondtl();
            $mobile =$this->agent->is_mobile();
              $desktop=$this->agent->is_browser();
               if (!empty($mobile)){
                   $this->load->view('mobile/include/header',$data);
                   $this->load->view('mobile/offerzone',$data); // to be created
               }
               else
               {
                  //echo '123654';
                  //print_r($data['promotionproduct']);//die('sus');
                  //$data['hotdeals']=$this->lib_home->hotdeals();
                  //print_r($data['hotdeals']);
                   $data['banner_category']=$this->lib_home->get_home_dtl();

                   $this->load->view('web/include/header',$data);
                   $this->load->view('web/offerzone',$data);
                   $this->load->view('web/include/footer',$data);
               }


        }

        public function saveshippingaddress(){
            if($_POST){
               $data['selectedaddr']=$_POST['selectedaddr'];
               $data['addressid']=$_POST['addressid'];
            }
               $data['usrid']=$_SESSION['usrid'];

               $res = $this->liblogin->saveshippingaddress($data);
               if($res){
                   $data=array('status'=>true,'msg'=>'Address set','error'=>false);

                  }else{
                     $data=array('status'=>false,'msg'=>'insertion fail','error'=>true);

                  }
                 echo  json_encode($data);
           }

           public function autosuggest(){
          if($_GET){
           $param['search']=$_GET['term'];
            $res = $this->libsearch->autosuggest($param);
                 $i=0;
            //print_R($res); die;
            foreach($res as $key =>$val){
               if($val['type']=='brand'){
                  $val['name']=$val['name'].'(brand)';

               }else{

                  $val['name']=$val['name'];

               }
              $dataset[$i]['label'] =  ucfirst($param['search']);
              $dataset[$i]['category'] = '<a href="'.$val['url'].'">'.ucfirst($param['search']).' '.'in'.' '.ucwords(strtolower($val['name'])).'</a>';
              $i++;
            }
            echo json_encode($dataset);exit;
          }
       }

    public function autosuggestweb(){
        if($_GET){
          $param['search']=$_GET['term'];
          $res1 = $this->libsearch->autosuggest($param);
          //echo '<pre>';
          //print_r($res1); die;
            $i=0;
            foreach($res1 as $key =>$val){
                   if($val['type']=='brand'){
                      $val['name']=$val['name'].'(brand)';

                   }else{

                      $val['name']=$val['name'];

                   }
                  $res2[$i]['label'] =  ucfirst($param['search']);
                  $res2[$i]['categoryname'] = '<a href="'.$val['url'].'">'.ucfirst($param['search']).' '.'in'.' '.ucwords(strtolower($val['name'])).'</a>';
                  $i++;
            }
                //echo '<pre>';
                //print_r($res2); die;
                $res = $this->libsearch->websearch($param);
                $res['autosuggest']=$res2;
                $res['search']=$param['search'];
                $i=0;
               $this->load->view('web/autosuggest',$res);
               //echo json_encode($dataset);exit;
      }
    }

     //VIEW
           public function addbillingaddr(){

            //print_r($_SESSION); die('bbb');
            if($_SESSION['usrid'] = '' && $_SESSION['usrname'] = '' || $_POST['customerid'] = '') {
               //redirect('mobile/user');
               echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'user"
                    </script>';
            }else{
               if($_POST['customerid']){
                  $data['usrid']=$_POST['customerid'];
               }else{
               $data['usrid']=$_SESSION['usrid'];
               }
               $data['usrname']=$_SESSION['usrname'];
               //$data['resp'] = $this->liblogin->getuseraddrdtl($data);
               $data['state']=$this->liborder->getallstate();

               $mobile =$this->agent->is_mobile();
                    $desktop=$this->agent->is_browser();
                 if (!empty($mobile))
                   {
                 $this->load->view('mobile/include/head');
               $this->load->view('mobile/billingaddress',$data);

               }else{
                   $this->load->view('web/billingaddress',$data);

               }
            }
           }

       public function addbannercount(){
         if($_POST){

           $param['id']=$_POST['bannerid'];
          // print_r($param); die;
           $res=$this->liburl->addbannercount($param);
           if($res)
             {
              $data = array('status' =>true ,'msg'=>$res,'erorr'=>false );

             }else{
              $data = array('status' =>false ,'msg'=>'No Result Found' ,'erorr'=>true );
          }
          echo json_encode($data);
         }

       }

           public function savebillingaddress()
           {
               if(!$this->libsession->isSetSession('usrid')) {

              $data['login']='false';
               echo json_encode($data); exit();

             }else{
                  if(!empty($_POST))
                  {
                     //$param['id']=intval($_POST['id']);
                     parse_str($_POST['formdata'],$param);

                     $param['usrid']=$_SESSION['usrid'];
                     $res = $this->liblogin->savebillingaddress($param);
                     if($res==1){
                          $data=array('status'=>true,'msg'=>'address set','error'=>false);

                       }else{
                           $data=array('status'=>false,'msg'=>'insertion fail','error'=>true);
                       }


                       echo json_encode($data);
                  }else{
                     echo "there is some problem with parameter"; die;
                  }

             }
           }

        public function addnewaddress()
           {

            if(empty($_SESSION['usrid']) && empty($_SESSION['usrname']))
            {
               //redirect('mobile/user');
               echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'user"
                    </script>';

            }else{
               $data['usrid']=$_SESSION['usrid'];
               $data['usrname']=$_SESSION['usrname'];
               //$data['resp'] = $this->liblogin->getuseraddrdtl($data);
               $data['state']=$this->liborder->getallstate();
               $this->load->view('mobile/include/head');
               $this->load->view('mobile/checkout',$data);
            }
           }

        public function getcartcount()
            {
                  $current_cart_item_count=0;
               if($this->libsession->isSetSession('cart'))
                  {
                     $current_cart_item_count = count($this->libsession->getSession('cart'));
                     $resp['status']=1;
                     $resp['cartcount'] = $current_cart_item_count;
                     echo json_encode($resp); exit;
                  }else if($this->libsession->isSetSession('final_bundle')){
                     $current_cart_item_count = count($this->libsession->getSession('final_bundle'));
                     $resp['status']=1;
                     $resp['cartcount'] = $current_cart_item_count;
                     echo json_encode($resp); exit;

                  }


            }

         public function viewcart(){
            $data['resp'] = $this->libcart->getcurrentcart();

            //print_r($data['resp']); die;

            $data['amount']=0;
               if($data['resp']){
                  //print_R($data['resp']); die;
                  foreach($data['resp'] as $key=>$val){
                     if($val['combo_product']){
                       foreach($val['combo_product'] as $key_combo=>$val_combo){
                          //echo $val_combo['dicountprice'];
                          $data['amount']=$data['amount']+$val_combo['dicountprice'];
                       }

                     }
                      $data['amount']=$data['amount']+$val['amount'];

                    //echo   $data['amount'];
                  }

               } //die;
            $data['cnt']=count($data['resp']);
            $data['bundlecnt']=count($this->libsession->getSession('final_bundle'));
            $data['bundlecart'] = $this->libcart->getbundlecart();

            $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
            //print_r($_SESSION); die;
            //print_r($data); die;
            if (!empty($mobile))
               {
                  $this->load->view('mobile/include/head');
               if($data){
                  $this->load->view('mobile/mycart',$data);
                  }else{
                      $this->load->view('mobile/mycart');
                  }
                  }
               else
               {

                  $this->load->view('web/include/header',$data);
                     $this->load->view('web/cart_view',$data);
                     $this->load->view('web/include/footer',$data);
               }



         }

    public function buynow(){

           if(empty($_SESSION['usrid']) && empty($_SESSION['usrname'])){
            //redirect('mobile/user');
            echo '<script type="text/javascript">
               window.location = "'.SITEMOBURL.'user"
               </script>';
              /* $data['error']=true;
               $data['message']='not signed in';
               echo json_encode($data);
               exit;*/
         }else{
            $data['resp'] = $this->libcart->getcurrentcart();
            //print_r($data['resp']); die;
            $data['amount']=0;
            if($data['resp']){
               foreach($data['resp'] as $key=>$val){
                 $data['amount']=$data['amount']+$val['amount'];
               }
            }
            $data['cnt']=count($data['resp']);
            $this->load->view('mobile/include/head');
            $data['menus']=$this->libmenu->get_dropdownmenu();
            $mobile =$this->agent->is_mobile();
               $desktop=$this->agent->is_browser();
               if($mobile){
                  if($data){
                  $this->load->view('mobile/mycart',$data);
               }else{
                  $this->load->view('mobile/mycart');
               }
               }else{
                  $this->load->view('web/include/header',$data);
                     $this->load->view('web/cart_view',$data);
                     $this->load->view('web/include/footer',$data);
               }
         }
      }


        public function checkoutuserdtl()
           {
            //print_r($_SESSION); die;
            //print_R($_SESSION); die;
            if(!$this->libsession->isSetSession('usrid'))
            {
               //redirect('mobile/user');
               echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'user"
                    </script>';
            }else{
               $data['usrid']=$_SESSION['usrid'];

               $data['resp'] = $this->liblogin->getuseraddrdtl($data);
                 $data['menus']=$this->libmenu->get_dropdownmenu();
                $mobile =$this->agent->is_mobile();
                    $desktop=$this->agent->is_browser();
                 if (!empty($mobile))
                   {
                  $this->load->view('mobile/include/head');

                  $this->load->view('mobile/choose_address',$data);
               }else{
                  $data['state']=$this->liborder->getallstate();
                  $this->load->view('web/include/shipping_address',$data);
               }
               //$this->load->view('mobile/checkout',$data);
            }
           }
           //done by sneha for web single page order details on 27-01-2018/////

            public function webcheckout(){
            /*$orderinvoice_no=intval($this->libsession->getSession('orderinvoice_no'));
        if($this->libsession->getSession('setpgsession')=='set'){
          $this->libsession->deleteSession('setpgsession');
        }
            if($orderinvoice_no && $this->libsession->getsession('setpgsession')==){
                if(0==$orderinvoice_no%2){
                    $this->razorpay();
                 }else{
                    $this->razorpay();
                 }
            }*/
            $mobile =$this->agent->is_mobile();
            $data['menus']=$this->libmenu->get_dropdownmenu();
            if(!$this->libsession->isSetSession('usrid')){
               //redirect('mobile/user');
               echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'user"
                    </script>';
            }else{
               $data['usrid']=$_SESSION['usrid'];

               $data['resp'] = $this->liblogin->getuseraddrdtl($data);

                 if (!empty($mobile))
                   {
                   echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'product/finalcart"
                    </script>';
                }else{
                  $this->load->view('web/include/header',$data);
                  $this->load->view('web/order',$data);
                  $this->load->view('web/include/footer',$data);
                }
            }
           }

           public function updatecart(){
              ///print_R($_POST); die;
              if($_POST){

                  $param['qtychange']=$_POST['qty'];
                  $param['pid']=$_POST['pid'];
                  $data['resp']= $this->libcart->updatecart($param);
                  if($data['resp']){
                   $data['error']='false';
                   foreach ($data['resp'] as $key => $value) {
                     $data['user']=$value;
                   }


                  }else{
                      $data['error']='true';

                  }
                     echo  json_encode($data); exit;
               }else{

                 echo "there is some problem with parameter"; die;

                 }


       }
        public function updatebundlecart(){
              //print_R($_POST);
              if($_POST){

                  $param['qtychange']=$_POST['qty'];
                  $param['pid']=$_POST['pid'];
                  $data['resp']= $this->libcart->updatebundlecart($param);
                  //print_R($data); die;
                  //print_R($_SESSION); die;

                  if($data['resp']){
                   $data['error']='false';

                  }else{
                      $data['error']='true';

                  }
                     echo  json_encode($data); exit;
               }else{

                 echo "there is some problem with parameter"; die;

              }


       }

        public function removefromcart(){
      if(!empty($_POST)){
                if($_POST['pid']){
                  $param['pid']=$_POST['pid'];
                  $param['orderinvoice_no']=$this->libsession->getSession('orderinvoice_no');
                  $data=$this->libcart->removefromcart($param);
                }
            }
      echo json_encode($data); exit;
        }


      public function removefromcombocart(){

               if(!empty($_POST))
               {
                  if($_POST['pid']){
                     $param['pid']=$_POST['pid'];
                     $param['combo_id']=$_POST['combo_id'];
                     $data=$this->libcart->removefromcombocart($param);
                  }
               }


               echo json_encode($data); exit;


      }
      public function removefrombundlecart(){

               if(!empty($_POST))
                  {
                     $pid = $_POST['pid'];
                     if(isset($_SESSION['final_bundle'][$pid]))
                     {
                        unset($_SESSION['final_bundle'][$pid]);
                        $data['error']='false';

                     }
                     else
                     {
                        $data['error']='true';

                     }

                      echo json_encode($data); exit;
                    }

      }

    public function search(){
      $mobile =$this->agent->is_mobile();
      $desktop=$this->agent->is_browser();
      $data['res']['meta_title']='Search Result - '.$_REQUEST['searchText'];
      $data['res']['meta_keyword']="";
      $data['res']['meta_description']="";
        if($_REQUEST){
            if($_REQUEST['start']){
                $param['start']=$_REQUEST['start'];
            }else{
                $param['start']=0;
            }
            if($_REQUEST['catid']){
              $param['catid']=$_REQUEST['catid'];
            }
            if($_REQUEST['level']){
              $param['level']=$_REQUEST['level'];
            }
            if($_REQUEST['searchText']){
                $param['search']=addslashes($_REQUEST['searchText']);
            }else{
                $param['search']=addslashes($_REQUEST['search']);
            }
            if($_REQUEST['filter']){
                $param['filterdtl']=$_REQUEST['filter'];
            }
            if($_REQUEST['brand']){
                $param['branddtl']=$_REQUEST['brand'];
            }
            if($_REQUEST['avaliable']){
                $param['avaliable']=$_REQUEST['avaliable'];
            }
            if($_REQUEST['sort']){
                $param['sort']=$_REQUEST['sort'];
            }
            if($_REQUEST['pricestart']){
                $param['minprice'] =$_REQUEST['pricestart'];
            }
            if($_REQUEST['pricend']){
                $param['maxprice']=$_REQUEST['pricend'];
            }

            //echo '<pre>';
            //print_r(var_dump($param)); die;
            $res= $this->libsearch->prouctsearch($param);
            $catfilter=$this->libsearch->catfilter($param);
            $data['getfilter']['brand']=$this->libsearch->brandfilter($param);
            $param['termip']=$_SERVER['REMOTE_ADDR'];
            $ressearchterm=$this->libsearch->insertsearchterm($param);
            $res['cnt']=$this->libsearch->getcount($param);
            //print '<pre>'; print_r($res); die;
            if($res){
              //get filters for searched term
                $i=0;
                foreach($res['product'] as $val){
                    $param['cid']=intval($val['firstcatid']);
                    if($i < 2)
                    //$rawfilter[]=$this->liburl->getfilter($param);
                    $i++;
                }
                //echo json_encode($rawfilter); die;
                //fiter the raw filter data
                $filterarray=array();
                /*foreach($rawfilter as $key=>$val){
                    foreach($val as $innerkey=>$innerval){
                        if(!empty($innerval) && is_array($innerval)){
                            foreach($innerval as $index=>$value){
                                $filterarray[$innerkey][$index]=$value;
                            }
                        }
                    }
                }*/
                //echo $res['cnt']; die;
                $data['cnt']=$res['cnt'];
                $data['resp']=$res;
                $data['filter']=$catfilter;
                $data['start']=$param['start'];
                $data['keyword']=$param['search'];
            }
        }
        //$data['filter'] = $this->libsearch->get_searchfilter($param);

        $data['menus']=$this->libmenu->get_dropdownmenu();
        if($param['start'] > 0){
            if (!empty($mobile)){
                 $this->load->view('mobile/ajax_productsearch',$data);
            }else{
                $this->load->view("web/include/ajaxsearchproductlist", $data);
            }
        }else{
            if (!empty($mobile)){
                $this->load->view('mobile/include/header',$data);
                $this->load->view('mobile/product_searching',$data);
            }else{
              //echo $data['start']; die;
                $data['banner_category']=$this->lib_home->get_home_dtl();
                $this->load->view('web/include/header',$data);
                $this->load->view("web/searchproduct",$data);
                $this->load->view('web/include/footer',$data);
            }
        }
    }
           /* Created By Viki 28/2/2018 */
           public function brandsearch(){
            //print_r($_POST);die;
            $param['brandsearch']=$_POST['brandsearch1'];
            $res=$this->libsearch->brandsearch($param);
            if($res)
               {
                  $data = array('status' =>true ,'msg'=>$res,'erorr'=>false );

               }else{
                  $data = array('status' =>false ,'msg'=>'No Result Found' ,'erorr'=>true );
            }
            echo json_encode($data);
           }
           public function addenquirydtl(){
               $this->form_validation->set_rules('fullname','Full Name','required');
               $this->form_validation->set_rules('mobile','Mobile Number','required');
               $this->form_validation->set_rules('email','Email Address','required');
               $this->form_validation->set_rules('time','Best Time to contact','required');
                  if($this->form_validation->run()== FALSE){
                        $error=validation_errors();
                        $data=array('status'=>'erroroccured','error'=>$error);
                        echo json_encode($data);
                     }else{
                        $param['fullname']=$_POST['fullname'];
                        $param['mobile']=$_POST['mobile'];
                        $param['email']=$_POST['email'];
                        $param['time']=$_POST['time'];
                        $param['comments']=$_POST['comments'];
                        $param['landline']=$_POST['landline'];
                        $param['productId']=$_POST['productId'];
                        $param['productName']=$_POST['productName'];
                        //print_R($param);
                        $res=$this->libprodtl->addenquirydtl($param);
                        if($res){
                           $mes='We assure you of our best service at all time.<br>Thank you for your interest. <br>We will inform you once the product is available online.';
                           $data = array('status' =>true ,'msg'=>$mes,'erorr'=>false );
                        }else{
                           $data = array('status' =>false ,'msg'=>'Error Occur While Adding Data!!','erorr'=>true );
                        }
                        echo json_encode($data);
                     }
            }

            public function pianoenquirydtl(){
               $this->form_validation->set_rules('fullname','Full Name','required');
               $this->form_validation->set_rules('mobile','Mobile Number','required');
               $this->form_validation->set_rules('email','Email Address','required');
               $this->form_validation->set_rules('time','Best Time to contact','required');
                  if($this->form_validation->run()== FALSE){
                        $error=validation_errors();
                        $data=array('status'=>'erroroccured','error'=>$error);
                        echo json_encode($data);
                     }else{
                        $param['fullname']=$_POST['fullname'];
                        $param['mobile']=$_POST['mobile'];
                        $param['email']=$_POST['email'];
                        $param['time']=$_POST['time'];
                        $param['landline']=$_POST['landline'];
                        $param['interest']=$_POST['interest'];
                        $param['budget']=$_POST['budget'];
                        $res=$this->libprodtl->pianoenquirydtl($param);
                        if($res){
                           $mes='Thank you for your interest on furtadosonline.com.<br>Your request has been recorded. Our Piano Specialist will contact you shortly.';
                           $data = array('status' =>true ,'msg'=>$mes,'erorr'=>false );
                        }else{
                           $data = array('status' =>false ,'msg'=>'Error Occur While Adding Data!!','erorr'=>true );
                        }
                        echo json_encode($data);
                     }
            }
           /* Created By Viki 28/2/2018 : End */

         public function finalcart(){
            $data['resp'] = $this->libcart->getcurrentcart();
            //echo '<pre>';
            //print_r($data['resp']); die;
            //create an hash from the session to compare cart data
            $haststring='';
            $rawstring='';
            $res=$this->liblogin->getshippingid();
            $session_data['shipping_address_key']=$res[0]['address_id'];
            //print_r($session_data); die;
            $this->libsession->setSession($session_data);
            //print_r($_SESSION); die;
            $data['amount']=0;
               if($data['resp']){
                //echo '<pre>';
                //print_r($data['resp']); die;
                //calculate the shipping charges ---new module added by Sandeep on 2018-06-26
                $param['shippingmode']='Normal'; //by default calculate for the Normal shipping mode
                $resshipping=$this->checkshippingcost($param);
                $data['total_courier_charges']=$resshipping['msg']['total_shipping_cost'];

                  foreach($data['resp'] as $key=>$val){//print_r($val);die;
                   if(is_array($val['combo_product'])){
                    //print_r($val['combo_product']);die;
                      foreach($val['combo_product'] as $key_combo=>$val_combo){
                        $data['amount']+=$val_combo['dicountprice'];
                      }

                   }
                    $data['carttotalprice']+=$val['price'] * $val['qty'];
                    $data['cartmaindiscount']+=($val['price'] - $val['dicountprice']) * $val['qty'];
                    $data['amount']+=$val['amount'];
                    $data['totalcart']+=$val['amountcart'];
                    $data['totaldiscountedamount']+=$val['amountdeducted'] * $val['qty'];
                    $rawstring.=$val['proid'].'-'.$val['qty'].'|';
                  }
                  //add shipping charges to the final amount
                    $data['amount']=$data['amount']+$data['total_courier_charges'];
                    $haststring=md5($rawstring);
                    //get the cart products from the database to compare with the hashstring
                    $param['orderinvoiceno']=$this->libsession->getSession('orderinvoice_no');
                    //$orderproducts=
               }
            $giftcode=$this->libsession->getSession('gift_code_applied');
            $coupon_code=$this->libsession->getSession('coupon_code_applied');
    //echo $coupon_code; die;
      /*if($giftcode){
       $giftcard_value=$this->libsession->getSession('gift_code_value');
         if($giftcard_value < $data['amount']){
           $data['amount']=$data['amount']-$giftcard_value;
          }
      }else if($coupon_code){
      //echo $coupon_code; die;
     $coupon_value=$this->libsession->getSession('coupon_code_value');
         if($coupon_value < $data['amount']){
           $data['amount']=$data['amount']-$coupon_value;
         }
      }*/
    //echo $data['amount']; die;
    if($giftcode){
      $data['giftcode']=$giftcode;
    }else if($coupon_code){
      $data['giftcode']=$coupon_code;
    }

    
    //echo '<pre>';
    //print_r($res); die;

            //echo '<pre>';
            //print_r($data); die;
            //echo $data['giftcode']; die;
            //echo $data['amount']; die;
            $data['cnt']=count($data['resp']);
            $data['menus']=$this->libmenu->get_dropdownmenu();

             $mobile =$this->agent->is_mobile();
                    $desktop=$this->agent->is_browser();
                 if (!empty($mobile))
                   {
                 $this->load->view('mobile/include/header',$data);
                $this->load->view('mobile/finalcart',$data);

               }else{
                   $this->load->view('web/include/finalcart',$data);
               }

         }

         public function checkshippingcost($param=false){
            //print_r($_POST);die;
            //if($_POST){
               $param['shippingmode']=($_POST['shippingmode']) ? $_POST['shippingmode'] : $param['shippingmode'];
               $res=$this->libshippingcost->calculateshippingcost($param);
               //print_r($res); die;
               $session_data['shipping_mode'] =  $param['shippingmode'];
               $this->libsession->setSession($session_data);
               // changes for setting shipping mode in session end
               if($res){
                  $data= array('status' =>true ,'msg'=>$res,'erorr'=>false );
                  $_SESSION['shipping_cost']=$res;
               }else{
                  $data = array('status' =>false ,'msg'=>$res,'erorr'=>true );
               }
            //}
            //echo json_encode($data);
              return $data;
         }

         public function getPaymentStatus($currentcart,$offerStatus=null){
          //echo 'asads';
          //print_r($currentcart); die;
         if(empty($currentcart)) return false;
         //print_r($currentcart); die;
         $amnt = null;
         if($this->libsession->getSession('final_bundle')){
            $data['resp'] = $this->libsession->getSession('final_bundle');
         }else{
            $data['resp'] = $this->libcart->getcurrentcart();
            //echo '<pre>';
      //print_r($data['resp']);
         }
         //echo 'the';
        //print_r($data['resp']); die;
         $data['amount']=0;
               if($data['resp']){
                  foreach($data['resp'] as $key=>$val){
                    $data['amount']=$data['amount']+$val['amount'];
                  }
              }
            $data['cnt']=count($data['resp']);
       //print_r($data); die;
       return $data;
      }

      public function addOrderItem($param){
          //print_r($param); die;
          $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
         if($this->libsession->getSession('final_bundle')){
            $data['resp'] = $this->libsession->getSession('final_bundle');
         }else{
            $data['resp'] = $this->libcart->getcurrentcart();
         }
         //echo '<pre>';
         //print_R($data['resp']); die;
         foreach($data['resp']as $key=>$val)
               {

                  $cart_item_detail['order_id'] = $orderinvoice_no;
                  $cart_item_detail['product_id'] = $val['proid'];
                  $cart_item_detail['name'] = $val['proname'];
                  $cart_item_detail['quantity'] = $val['qty'];
                  $cart_item_detail['price'] = $val['dicountprice'];
                  $cart_item_detail['total'] = $val['amount'];
                  $cart_item_detail['order_status'] = 'Transaction Pending';
                  $cart_item_detail['is_delivered'] = 'N';
                  $cart_item_detail['delivery_charges'] =0;

                  if($val['amountdeducted'] > 0){
                      $cart_item_detail['is_offer_used']='Y';
                      switch($val['offertype']){
                          case 'coupon':
                            $cart_item_detail['offertype']='CPN';
                            break;

                          case 'giftcard':
                            $cart_item_detail['offertype']='GFT';
                            break;

                          default: 
                            $cart_item_detail['offertype']='UKN';
                      }
                      $cart_item_detail['coupon_or_gift_code']=$val['couponcode'];
                      $cart_item_detail['offer_amounts']=$val['amountdeducted'];
                  }else{
                    $cart_item_detail['offer_amounts']=0;
                  }

                  $isOrderItemCreated = $this->liborder->additem($cart_item_detail);


            if($isOrderItemCreated){
                     $flag=1;continue;
               }else {
                     $flag=0;break;
               }
            }
               if($flag!=0)
               {
                  return true;
               }
            return false;


        }

        public function placeFinalOrder(){
          //print_r($this->libsession->getallsessions()); die;
          $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
            
          $module=$this->input->post('module');
          if($this->libsession->getSession('final_bundle')){
            $cart_data = $this->libsession->getSession('final_bundle');
          }else{
            $cart_data = $this->libsession->getSession('cart');
          }
            //print_r($cart_data); die;
            $res=$this->liblogin->getshippingid();
            $session_data['shipping_address_key'] = $res[0]['address_id'];
            $this->libsession->setSession($session_data);
            //print_r($cart_data); die;
          if(!empty($cart_data))
          {
            $total_item_order = count($cart_data);
            $currentcart      = $cart_data;
          if(!$this->libsession->getSession('orderinvoice_no')){
            $getlatinv_no = $this->liborder->getlastinv();
            //print_R($getlatinv_no); die;
            $getlatinv_no = $getlatinv_no[0]['orderinvoice_no']+1;
          }else{
            $getlatinv_no = $this->libsession->getSession('orderinvoice_no');
          }
          
          $cart_data['orderinvoice_no']    = $getlatinv_no;#--Generate Invoice unique code
          $cart_data['customer_id']        = $this->libsession->getSession('usrid');
          $cart_data['total_item_order']   = $total_item_order;
          $cart_data['shipping_master_id'] = $this->libsession->getSession('shipping_address_key');
          $cart_data['shipping_mode']      = $this->libsession->getSession('shipping_mode');
      
          if($mobile){
              $cart_data['platform']='WOM';
          }else{
              $cart_data['platform']='WO';
          }

          //calculate the shipping charges ---new module added by Sandeep on 2018-06-26
          $param['shippingmode']=($_POST['shippingmode']) ? $_POST['shippingmode'] : 'Normal'; //by default calculate for the Normal shipping mode
          $resshipping=$this->checkshippingcost($param);
          $cart_data['total_courier_charges']=$resshipping['msg']['total_shipping_cost'];
          
          //echo $cart_core_json['customer_id']; die;
    //print_R($_SESSION);

          $paymentStatus = $this->getPaymentStatus($currentcart,$offerStatus); #-- update on
          //echo 'heer';
          //print_r($paymentStatus); die;
          //print_r($paymentStatus); die;
          if(!empty($paymentStatus)){
            if($this->libsession->getSession('shipping_cost')){
              $cart_data['all_shiiping_dtl']      = $this->libsession->getSession('shipping_cost');
              $cart_data['total_courier_charges'] = $cart_data['all_shiiping_dtl']['total_shipping_cost'];
            }else{
              $cart_data['total_courier_charges'] = 0;
            } 
            $cart_data['total_amount'] = 0;
            
            foreach($paymentStatus['resp'] as $cartprokey=>$cartproval){
              $cart_data['total_amount'] += $cartproval['amount'];
              if(is_array($cartproval['combo_product'])){
                foreach($cartproval['combo_product'] as $combokey=>$comboval){
                  $cart_data['total_amount'] += $comboval['dicountprice'];
                }
              }
            }

            $cart_data['giftcode']= $this->libsession->getSession('gift_code_applied');
            $cart_data['coupon_id']= $this->libsession->getSession('coupon_code_applied');

            if($cart_data['giftcode'] || $cart_data['coupon_id']){
              $param['code']= ($cart_data['giftcode']) ? $cart_data['giftcode'] : $cart_data['coupon_id'];
              $offerdetails= $this->liborder->getofferdetails($param);
              $cart_data['offerdetails'] = $offerdetails;
            }
            /*
            Commened by Sandeep becuase of change in algo
            if($cart_data['giftcode']){
            $cart_data['giftcard_value']=$this->libsession->getSession('gift_code_value');
            if($cart_data['giftcard_value'] <  $cart_data['total_amount'] ){
            $cart_data['total_amount']=$cart_data['total_amount']-$cart_data['giftcard_value'];

            }
            }else if( $cart_data['coupon_id']){
            $cart_data['coupon_value']=$this->libsession->getSession('coupon_code_value');
            if( $cart_data['coupon_value'] < $cart_data['total_amount'] ){
            $cart_data['total_amount']=$cart_data['total_amount']-$cart_data['coupon_value'];

            }

            }*/
            
            $session_data['total_amount'] =  $paymentStatus['amount'];
            $this->libsession->setSession($session_data);
            //echo $paymentStatus['amount'].'<br>'.$cart_data['total_courier_charges']; die;
            //print_r($cart_data['total_courier_charges']); die;
            $cart_data['final_amount_to_pay'] = $paymentStatus['amount'] + $cart_data['total_courier_charges'];
            $cart_data['total_item_count']    = $paymentStatus['cnt'];
          }
            //echo '<pre>';
            //print_r($cart_data); die;

            $total_amount =  $cart_data['final_amount_to_pay'];
            if($total_amount  >= MIN_PRICE_FOR_EMI ){
              $resp['total_amount'] = $total_amount ;
              $resp['emi_amount_3_interestRate'] = 2.34;
              $resp['emi_amount_6_interestRate'] = 4.12;

              $resp['emi_amount_3_interest'] = round(($total_amount/100) * $resp['emi_amount_3_interestRate']);
              $resp['emi_amount_6_interest'] = round(($total_amount/100) * $resp['emi_amount_6_interestRate']);

              $resp['emi_amount_3_total'] = $total_amount + $resp['emi_amount_3_interest'];
              $resp['emi_amount_6_total'] = $total_amount + $resp['emi_amount_6_interest'];

              $resp['emi_amount_3_amount'] = round($resp['emi_amount_3_total'] / 3);
              $resp['emi_amount_6_amount'] = round($resp['emi_amount_6_total'] / 6);

              $resp['emi_amount_3_total'] = $resp['emi_amount_3_amount'] * 3;
              $resp['emi_amount_6_total'] = $resp['emi_amount_6_amount'] * 6;

            }

            $isOrderCreated = $this->liborder->createneworder($cart_data);
            if($isOrderCreated){
              $session_data['orderinvoice_no'] = $cart_data['orderinvoice_no'];
              $this->libsession->setSession($session_data);
              $itemAdded = $this->addOrderItem($currentcart);
              //echo '<pre>';
              //print_R($currentcart ); die;
              if($itemAdded){
                if(!empty($mobile)){
                  //die('here');
                  $resp['status']=1;
                  $resp['msg']='Order master set, success';
                  $resp['error']=false;
                  if(!$module){
                      echo '<script type="text/javascript">
                      window.location = "'.SITEMOBURL.'product/finalcart"
                      </script>'; exit;
                  }else if($module=='coupon'){
                    echo json_encode($resp); exit;
                  }
                } else {
                  $resp['status']=1;
                  $resp['msg']='Order master set, success';
                  $resp['error']=false;
                  echo json_encode($resp); exit;
                }
                //$this->securePayU();
              } else {
                $resp['status']=0;
                $resp['msg']='Fail to set order product.';
                $resp['error']=false;
              }
            } else {
              $resp['status']=0;
              $resp['msg']='Fail to set order.';
              $resp['error']=false;
            }
          } else {
            $resp['status']=2;
            $resp['msg']='Session Expired, No item in cart.';
            $resp['error']=false;
          }
          echo json_encode($resp);
          exit;
        }

      public function securePayU(){
         if(!$this->libsession->isSetSession('usrid')) { echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'"
                    </script>';} #-- User Login
         if(!$this->libsession->isSetSession('orderinvoice_no')){
          echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'"
                    </script>';
                    } #-- If No purchase.
         #-- Now update Payment method.

         $update['payment_method'] = 'Pay u';
         $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
         $update['orderinvoice_no'] = $orderinvoice_no;

         $finalamount=$this->liborder->gettotalamount($update['orderinvoice_no']);
         //print_r($finalamount); die;
         $update['total_amount']=$finalamount[0]['total_amount'];
          //print_R($update); die;

         if($this->libsession->getSession('shipping_cost')){
         $update['all_shiiping_dtl']=$this->libsession->getSession('shipping_cost');
         $update['total_courier_charges']=$update['all_shiiping_dtl']['total_shipping_cost'];
           }else{
            $update['total_courier_charges']=0;
         }

           //echo $paymentStatus['amount'].'<br>'.$cart_data['total_courier_charges']; die;
           //print_r($cart_data['total_courier_charges']); die;
         $update['final_amount_to_pay'] = $update['total_amount'] + $update['total_courier_charges'];
          //   $update['total_item_count'] = $paymentStatus['cnt'];
      if($this->libsession->getSession('gift_card_id')){
      $update['gift_card_id']=$this->libsession->getSession('gift_card_id');
      $update['total_card_value']=$this->libsession->getSession('total_card_value');
      $update['code']=$this->libsession->getSession('code');

         }else if($this->libsession->getSession('coupon_id')){
      $update['coupon_id']=$this->libsession->getSession('coupon_id');
      $update['total_card_value']=$this->libsession->getSession('coupon_code_value');
      $update['code']=$this->libsession->getSession('code');
      $update['customer_id']=$this->libsession->isSetSession('usrid');
     }


         $this->liborder->updatepayment($update);
         #-- Now get current transaction details..
         $orderParams['orderinvoice_no'] = $orderinvoice_no;
         $orderParams['customer_id'] = $this->libsession->getSession('usrid');
         $orderDetails = $this->liborder->getorderdetail($orderParams);
         //print_r($orderDetails); die;
         $userdetails = $this->liblogin->getuserdata();
         $userdetails[0]['telephone']=$orderDetails[0]['contact_num'];
         //$orderDetails[0]['final_amount_to_pay']=1.00;
         if(!empty($orderDetails))
         {
            // die('test');
            $pgarray = array ('key' => PAYUKEY, 'txnid' => $orderinvoice_no, 'amount' =>  $update['final_amount_to_pay'],'firstname' => $userdetails[0]['firstname'].' '.$userdetails[0]['lastname'], 'email' => $userdetails[0]['email'], 'phone' => $userdetails[0]['telephone'],'productinfo' => $orderDetails[0]['orderid'], 'surl' => 'success', 'furl' => 'failed');
            //print_R($pgarray); die;
            require_once dirname( __FILE__ ) . '/payu/payu.php';
            $result = pay_page($pgarray , PAYUSALT);
            // print_R($_POST); die;
            if($_POST){
               //$result['status']=1; $result['data']="success";
               $this->payment_success($result);
            }
         } exit;
         echo '<script type="text/javascript">
                    //window.location = "'.SITEMOBURL.'product/webcheckout"
                    //</script>';
         exit;
      }

      public function razorpay(){
         $keyId=RAZORPAYKEY;
         $keySecret=RAZORPAYSECRET;
         $displayCurrency=RAZORPAYDISPCURRENCY;

         $api = new Api($keyId, $keySecret);
         $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
         $finalamountarray=$this->liborder->gettotalamount($orderinvoice_no);

         $param['payment_method']='razorpay';
         if($this->libsession->getSession('shipping_cost')){
            $param['all_shiiping_dtl']=$this->libsession->getSession('shipping_cost');
            $param['total_courier_charges']=$param['all_shiiping_dtl']['total_shipping_cost'];
         }else{
            $param['total_courier_charges']=0;
         }
         $param['total_amount']=$finalamountarray[0]['total_amount'];
         $finalamount=$finalamountarray[0]['total_amount'] + $param['total_courier_charges'];

         $orderData = [
             'receipt'         => $orderinvoice_no,
             'amount'          => $finalamount * 100, //rupees in paise
             'currency'        => 'INR',
             'payment_capture' => 1 // auto capture
         ];
         //print_r($orderData); die;
         $razorpayOrder = $api->order->create($orderData);
         $razorpayOrderId = $razorpayOrder['id'];
         $razorpayOrderIdsession=array('razorpay_order_id'=>$razorpayOrderId);
         $this->libsession->setSession($razorpayOrderIdsession);
         $displayAmount = $amount = $orderData['amount'];
         $displayCurrency='INR'; //change the currency accordingly if needed
         if($displayCurrency !== 'INR'){
              $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
              $exchange = json_decode(file_get_contents($url), true);
              $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
         }
         //echo $displayAmount; die;
         $checkout = 'automatic';//change the checkout method accordingly if needed
         if (isset($_GET['checkout']) and in_array($_GET['checkout'], ['automatic', 'manual'], true)){
             $checkout = $_GET['checkout'];
         }
         $param['usrid']=$param['customer_id']=$this->libsession->getSession('usrid');
         $param['orderinvoice_no']=$orderinvoice_no;
         $customerdata=$this->liblogin->getuserdata();
         $orderdata=$this->liborder->getorderdetail($param);
         //$param['final_amount_to_pay']=$param['total_amount']+ $param['total_courier_charges'];
         $param['final_amount_to_pay']=$finalamount;
         if($this->libsession->getSession('gift_card_id')){
            $param['gift_card_id']=$this->libsession->getSession('gift_card_id');
            $param['total_card_value']=$this->libsession->getSession('total_card_value');
            $param['code']=$this->libsession->getSession('code');

         } else if($this->libsession->getSession('coupon_id')){
             $param['coupon_id']=$this->libsession->getSession('coupon_id');
             $param['total_card_value']=$this->libsession->getSession('coupon_code_value');
             $param['code']=$this->libsession->getSession('code');
            //$param['customer_id']=$this->libsession->getSession('usrid');
         }
        $this->liborder->updatepayment($param);

         $customeraddress=$orderdata[0]['address_1'].', '.$orderdata[0]['landmark'].', '.$orderdata[0]['address_2'].', '.$orderdata[0]['cityname'].', '.$orderdata[0]['statename'];

        $data = [
             "key"               => $keyId,
             "amount"            => $displayAmount,
             "name"              => $customerdata[0]['firstname'].' '.$customerdata[0]['lastname'],
             "description"       => $customerdata[0]['firstname'].' '.$customerdata[0]['lastname'],
             "image"             => SITEURL.'assets/images/mobimg/icon/logo_12.png',
             "prefill"           => [
             "name"              => $customerdata[0]['firstname'].' '.$customerdata[0]['lastname'],
             "email"             => $customerdata[0]['email'],
             "contact"           => $customerdata[0]['telephone'],
             ],
             "notes"             => [
             "address"           => $customeraddress,
             "merchant_order_id" => $orderdata[0]['orderinvoice_no'],
             ],
             "theme"             => [
             "color"             => "#F37254"
             ],
             "order_id"          => $razorpayOrderId,
        ];

        if ($displayCurrency !== 'INR'){
             $data['display_currency']= $displayCurrency;
             $data['display_amount']= $displayAmount;
        }
        //print_r($data); die;
        $this->load->view('mobile/razorpay', $data);
      }


    public function distributepg(){
        $orderinvoice_no=intval($this->libsession->getSession('orderinvoice_no'));
        if(0==$orderinvoice_no%2){
          if($_SERVER['SERVER_NAME']=='furtadosv2.lumeg.in'){
              $this->razorpay();
          }else{
              $this->securePayU();
          }
        }else{
            $this->razorpay();
        }
        $this->load->view('web/distributepg');
    }

    public function hdfc_payment(){
      //print_R($_POST); die;
      if($_POST['paymentType']){
        $orderinvoice_no=$this->libsession->getSession('orderinvoice_no');
        $param['usrid']=$param['customer_id']=$this->libsession->getSession('usrid');
        $param['orderinvoice_no']=$orderinvoice_no;
        $paytype='4427';
         $orderdata=$this->liborder->getorderdetail($param);
         $customeraddress=$orderdata[0]['address_1'].', '.$orderdata[0]['landmark'].', '.$orderdata[0]['address_2'].', '.$orderdata[0]['cityname'].', '.$orderdata[0]['statename'];
         //echo $customeraddress; die;
          $customerdata=$this->liblogin->getuserdata();
       // header('Location:'.SITEURL.'hdfc_payment/SendPerformREQuesthdfc.php?payment_type='.$paytype.'');
         echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'hdfc_payment/SendPerformREQuesthdfc.php?order_no='.$orderinvoice_no.'&payment_type='.$paytype.'&udf2='.$customerdata[0]['telephone'].'&udf3='.$customerdata[0]['email'].' &udf4='.$customeraddress.'"
                    </script>';
      }
    }

      function payment_success($result=false){
         if($result){

            if($result['status']==1 && $result['data']=="success")
            {
               $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
               $update_order['order_pay_status'] = 1;
               $update_order['payment_gateway_response'] = json_encode($_POST);
               $update_order['orderinvoice_no'] = $orderinvoice_no;
               $isFinalUpdate = $this->liborder->updatefinalorderdetails($update_order);
               //echo $isFinalUpdate; die;
               if($isFinalUpdate)
               {
                  $this->_compelteOrder();
                  echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'product/thanksCustomer"
                    </script>';

               }else{
                  //redirect(SITEMOBURL);
                  echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'product/webcheckout"
                    </script>';
               }
            }else{
               $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
               $update_order['order_pay_status'] = 0;
               $update_order['payment_gateway_response'] = json_encode($_POST);
               $update_order['orderinvoice_no'] = $orderinvoice_no;
               $isFinalUpdate = $this->liborder->updatefinalorderdetails($update_order);
               //redirect('product/finalcart');
                        echo '<center><h3>Your payment is failed</h3><br><div class="col-xs-12 Place_Order"><a href="'.SITEMOBURL.'product/webcheckout">Retry Payment</a></div></center>';
            }

         }
      }

      function payment_success_razorpay($param=false){
         $success=false;
         $error='Payment Failed';
         $keyId=RAZORPAYKEY;
         $keySecret=RAZORPAYSECRET;
         $displayCurrency=RAZORPAYDISPCURRENCY;
         $api=new Api($keyId, $keySecret);

         if(empty($_POST['razorpay_payment_id']) === false){
             $api = new Api($keyId, $keySecret);
              $attributes = array(
                     'razorpay_order_id' => $this->libsession->getSession('razorpay_order_id'),
                     'razorpay_payment_id' => $_POST['razorpay_payment_id'],
                     'razorpay_signature' => $_POST['razorpay_signature']
                 );
              //integrated razorpay capture API on 2018-06-13 by Sandeep
              $paymentget=$api->payment->fetch($_POST['razorpay_payment_id']);
              $capture=$paymentget->capture(array('amount' => $paymentget['amount']));
              $success=true;
         }

         if($success===true){
            $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
            $param['order_pay_status'] = 1;
            $param['payment_gateway_response'] = json_encode(array('result'=>'success', 'attributes'=>$attributes));
            $param['orderinvoice_no'] = $orderinvoice_no;
            if($this->liborder->updatefinalorderdetails($param)){
               $this->_compelteOrder();
               //header('location:'.SITEMOBURL.'product/thanksCustomer');
               /*echo '<script type="text/javascript">
                    window.location = "'.SITEMOBURL.'product/thanksCustomer"
                    </script>';*/
                    $data['error']=false;
                    $data['message']='Payment successfully captured';
                    $data['status']=1;
            }else{
                    $data['error']=true;
                    $data['message']='failed to update order';
                    $data['status']=2;

            }
         }else{
            $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
            $param['order_pay_status'] = 0;
            $param['payment_gateway_response'] = json_encode(array('results'=>'failure', 'attributes'=>$attributes));
            $param['orderinvoice_no'] = $orderinvoice_no;
            $this->liborder->updatefinalorderdetails($param);

            $data['error']=true;
            $data['message']='failed to capture payment';
            $data['status']=3;
         }
         echo json_encode($data);
      }

      private function _compelteOrder(){
         $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
         $params['orderinvoice_no'] = $orderinvoice_no;
          $mobile =$this->agent->is_mobile();
          $desktop=$this->agent->is_browser();
          if (!empty($mobile)){
              $param['platform']='WOM';
          }else{
              $params['platform']='WO';
          }
       #print_r($params); die;
         $res = $this->liborder->insert_tempforweb($params);
         if($_SESSION['bundleid']){
            $res = $this->liborder->updatebundle();
         }
      }

      public function thanksCustomer($param=false){
          $data['menus']=$this->libmenu->get_dropdownmenu();

          $data['codsuccess']=($param) ? 'Order successfully placed' : '';

          $data['order_no']=$this->libsession->getSession('orderinvoice_no');

          $data['final_amount']=$this->libsession->getSession('final_cart_amount');

          $oredreresult=$this->liborder->getorderdtl($data);
          $result=$this->libmailer->orderdetails($oredreresult);
          $giftcard= $this->liborder->checkorderiswithgiftcard($data['order_no']);

    if($giftcard){
      $data['giftcard'] =1;
    }else{
     $data['giftcard'] =0;
    }
         $mobile =$this->agent->is_mobile();
           $desktop=$this->agent->is_browser();
           if (!empty($mobile))
             {
             $this->load->view('mobile/include/header',$data);
         $this->load->view('mobile/thanks_customer', $data);
             }
             else
             {
              // $data['banner_category']=$this->lib_home->get_home_dtl();
               $this->load->view('web/include/header',$data);
             $this->load->view("web/thanks_customer",$data);
             $this->load->view('web/include/footer',$data);

              }


         $this->libsession->deleteSession('cart');
         $this->libsession->deleteSession('final_bundle');
         #-- remove extra cart details, after successfully.
         $removeSession = array('shipping_address_key','final_cart_amount','orderinvoice_no','bundleid', 'coupon_code_applied', 'coupon_code_value', 'coupon_id');
         $this->libsession->deleteSession($removeSession);
      }

      public function getorderbyuser(){
         $data['menus']=$this->libmenu->get_dropdownmenu();

         if($_POST['customerid']){
            $params['usrid']=$_POST['customerid'];

         }else{
            $userid = $this->libsession->getSession('usrid');
            $params['usrid'] = $userid ;
         }
         $data['resp'] = $this->liborder->getmyorders($params);
         //print_R($data); die;
         $usremail = $this->libsession->getSession('usremail');
         $params['usremail'] = $usremail ;
         $data['respweb'] = $this->liborder->getweborderbyuser($params);
         #print_R($data); die;
         $mobile =$this->agent->is_mobile();
           $desktop=$this->agent->is_browser();
           if (!empty($mobile))
             {
              $this->load->view('mobile/include/head');
            $this->load->view('mobile/myorders', $data);
             }
             else
             {
              $this->load->view("web/myorderview", $data);
              }
      }


      public function getcleranceproduct(){
       // die('We will be back soon');
  $data['res']['meta_title']="Clearance Sale on Guitars, Pianos, Keyboards, Drums Online | Furtados Online";
      $data['res']['meta_keyword']="Clearance Sale on Guitars, Pianos";
      $data['res']['meta_description']="Check out our Clearance Sale on Furtados Online, India’s leading online music instruments retailer. Get upto 40% off on Guitars, Pianos, Keyboards, Drums etc.";
        //print_r($_GET); 
          if($_REQUEST['filter']){
            $param['filterdtl']=$_REQUEST['filter'];
          }
          if($_REQUEST['brand']){
              $param['branddtl']=$_REQUEST['brand'];
          }
          if($_REQUEST['avaliable']){
              $param['avaliable']=$_REQUEST['avaliable'];
          }
          if($_REQUEST['sort']){
              $param['sort']=$_REQUEST['sort'];
          }
          if($_REQUEST['pricestart']){
              $param['minprice'] =$_REQUEST['pricestart'];
          }
          if($_REQUEST['pricend']){
              $param['maxprice']=$_REQUEST['pricend'];
          }
          if($_REQUEST['categoryid']){
              $param['categoryid']=$_REQUEST['categoryid'];
          }
          
          $data['menus']  = $this->libmenu->get_dropdownmenu();
          $data['resp']   = $this->lib_home->get_clerance($param);
          //echo '<pre>';
          //print_r($data['resp']); die;
          //$data['filter'] = $this->lib_home->get_clerancefilter($param);
          //print_R($data['filter']); die;
          $product_ids='';
          foreach($data['resp']['product'] as $key=>$val){
              $product_ids.=$val['id'].',';
          }
          $param['product_ids']=$product_ids;
          //echo $param['product_ids']; die;
          $param['module']='clearancefilter';
          $data['filter']=$this->libsearch->catfilter($param);
          //echo '<pre>';
          //print_r($data['catfilter']); die;
          $mobile  = $this->agent->is_mobile();
          $desktop = $this->agent->is_browser();
            if (!empty($mobile)){
              $this->load->view('mobile/include/header',$data);
              $this->load->view('mobile/clearnce', $data);
            }else{
               $data['banner_category']=$this->lib_home->get_home_dtl();
               $this->load->view('web/include/header',$data);
               $this->load->view("web/cleranceproduct",$data);
               $this->load->view('web/include/footer',$data);
            }

      }


      public function createbundle(){
          $data['menus']=$this->libmenu->get_dropdownmenu();
         //$data['resp'] = $this->lib_home->get_clerance()  ;
          $this->load->view('mobile/include/header',$data);

         $this->load->view('mobile/createbundle', $data);


      }


      public function addtobundle(){
         if($_POST){
                $param['pid']=$_POST['proid'];

                $data['res']=$this->libcart->addtobundle($param);
                if(is_array($data['res'])){

                if(!empty($data['res'])){
                     $data['error']= 'false';
                  }else{
                      $data ['error']='true';
                   }

                 echo  json_encode($data);
               }
            }


      }

      public function getbundlecartcount()
            {
                  $current_cart_item_count=0;
               if($this->libsession->isSetSession('final_bundle'))
                  {
                     $current_cart_item_count = count($this->libsession->getSession('final_bundle'));
                     $resp['status']=1;
                     $resp['cartcount'] = $current_cart_item_count;
                     echo json_encode($resp); exit;
                  }


            }
      public function viewbundlecart(){


            $data['resp'] = $this->libcart->getbundlecart();

            $data['amount']=0;
               if($data['resp']){
                  foreach($data['resp'] as $key=>$val){
                    $data['amount']=$data['amount']+$val['amount'];

                  }

               }
            $data['cnt']=count($data['resp']);
            $data['bundlecnt']=count($this->libsession->getSession('final_bundle'));
            $mobile =$this->agent->is_mobile();
            $desktop=$this->agent->is_browser();
             $this->load->view('mobile/include/head');
            if($data){
                   if(!$mobile){
                       $this->load->view('web/bundlecart',$data);

                    }else{
                       $this->load->view('mobile/bundlecart',$data);
                    }
             }else{
                   $this->load->view('mobile/bundlecart');

               }


           }

          public function getquote(){
            if($_POST){
             $param['comment']=$_POST['comment'];
              $res=$this->libcart->addquote($param);

              $res2=$this->libmailer->budleenquirymail($param);
              //print_R($_SESSION); die;
                  if($res){
                 $data['error']= 'false';
                 $this->libsession->deleteSession('final_bundle');

               }else{
                $data['error']= 'true';
               }
                  echo json_encode($data); exit;

            }

         }

    public function getbundleshopcart(){
      if(!$this->libsession->isSetSession('usrid')) {
        echo '<script type="text/javascript">
        window.location = "'.SITEMOBURL.'user"
        </script>';
      }else{
        $param['bundleid']=$_GET['bundleId'];
        $param['bundleid'] = base64_decode(str_replace('__','&',$param['bundleid']));
        $param['bundleid'] = intval($param['bundleid']);
        $_SESSION['bundleid']=$param['bundleid'];
        $data['resp']=$this->libcart->getbundledetail($param);
        $data['amount']=0;
        if($data['resp']){
          foreach($data['resp'] as $key=>$val){
            $data['amount']=$data['amount']+$val['amount'];
          }
        }
        $data['cnt']=count($data['resp']);
        $this->load->view('mobile/include/head');
        // print_R($data);
        if($data){
          $this->load->view('mobile/bundleshopcart',$data);
        }else{
          $this->load->view('mobile/bundleshopcart');
        }
      }
    }

    public function finalbundlecart(){
      $data['resp'] = $this->libsession->getSession('final_bundle');
      $res=$this->liblogin->getshippingid();
      $session_data['shipping_address_key'] =  $res[0]['address_id'];
      $this->libsession->setSession($session_data);
      //print_r($_SESSION); die;
      $data['amount']=0;
      if($data['resp']){
        foreach($data['resp'] as $key=>$val){
          $data['amount']=$data['amount']+$val['amount'];
        }
      }
      $data['cnt']=count($data['resp']);
      $data['menus']=$this->libmenu->get_dropdownmenu();
      $this->load->view('mobile/include/header',$data);
      if($data){
        $this->load->view('mobile/finalbundlecart',$data);
      }else{
        $this->load->view('mobile/finalbundlecart',$data);
      }
    }

    public function checkloginforeview(){
      if(!$this->libsession->isSetSession('usrid'))
      {
        $data = array('status' =>false,'msg'=>'Not Logged In', 'error'=>true );
      }else{
        $data = array('status' =>true ,'msg'=>'Logged In', 'error'=>false );
      }
      echo json_encode($data);
    }

    public function addreview(){
      if($_POST){
        $param['module']   = $_POST['module'];
        $param['pid']      = $_POST['proid'];
        $param['proname']  = $_POST['proname'];
        $param['reviewmessage'] = $_POST['reviewmessage'];
        $param['ratingme'] = $_POST['ratingme'];
        $param['reviewtitle1']  = $_POST['reviewtitle1'];
        $param['usrid']    = $_SESSION['usrid'];
        $param['usrname']  = $_SESSION['usrname'];
        $param['usremail'] = $_SESSION['usremail'];


        $res=$this->libprodtl->addreview($param);
        //$res2=$this->libmailer->postreview($param);

      if($res){
      $data['error'] = 'false';
      }else{
      $data['error'] ='true';
      }

      echo  json_encode($data);
      }

    }

    public function getorderdtl(){
      if($_GET || $_POST){
        if($_GET['order_no']){
          $param['order_no'] = $_GET['order_no'];
        }else{
          $param['order_no'] = $_POST['orderid'];
        }

        $res=$this->liborder->getorderdtl($param);
        //echo '<pre>';
        //print_r($res); die;
        
        #$this->load->view('mobile/include/head');
        if($res){
            $data['resp'] = $res;
            $mobile  = $this->agent->is_mobile();
            $desktop = $this->agent->is_browser();
            $data['menus'] = $this->libmenu->get_dropdownmenu();
          if (!empty($mobile))
          {
            $this->load->view('mobile/include/head');
            $this->load->view('mobile/order_detail',$data);
          }
          else
          {
            $data['result'] = $data['resp'];
            $this->load->view("web/include/orderdetail_pop", $data);
          }
        }
      }
    }

    public function getweborderdtl(){
      if($_GET){
        $param['order_id'] = $_GET['order_no'];
        $usremail = $this->libsession->getSession('usremail');
        $param['usremail'] = $usremail ;
        $res  = $this->liborder->getweborderbyuser($param);
        $res1 = json_decode($res,true);

        foreach ($res1 as $key => $value) {
          foreach ($value['product'] as $keypro => $valuepro) {
            $res2[$keypro]['orderId']       = $value['orderId'];
            $res2[$keypro]['order_created'] = $value['insertDate'];
            $res2[$keypro]['status']        = $value['status'];

            $res2[$keypro]['total']         = $value['total_amount'];
            $res2[$keypro]['total_courier_charges'] = $value['shipping_cost'];
            $res2[$keypro]['final_amount_to_pay']   = $value['final_amount'];

            $res2[$keypro]['product_id']  = $valuepro['productid'];
            $res2[$keypro]['quantity']    = $valuepro['quantity'];
            $res2[$keypro]['name']        = $valuepro['name'];
            $res2[$keypro]['onlinePrice'] = $valuepro['orgprice'];
            $res2[$keypro]['price'] = $valuepro['price'];
            $res2[$keypro]['image'] = $valuepro['image'];
          }
        }

        $this->load->view('mobile/include/head');
        if($res){
          $data['resp'] = $res2;
          $this->load->view('mobile/order_detail',$data);
        }
      }
    }

    public function giftcart(){
      $data['menus']=$this->libmenu->get_dropdownmenu();
      $mobile =$this->agent->is_mobile();
      $desktop=$this->agent->is_browser();
      if (!empty($mobile)){
          //  $this->load->view('mobile/include/head');
          // $this->load->view('mobile/giftvoucher_m');
      }
      else
      {
        $this->load->view('web/include/header',$data);
        $this->load->view('web/giftcart',$data);
        $this->load->view('web/include/footer',$data);
      }

    }

    public function getdetail(){
      // print_r($_POST['pid']); die;
      $param['pid']=$_POST['pid'];
      $res=$this->liburl->get_productdetail($param);
      $data['name']=$res['name'];
      $data['proid']=$res['proid'];
      //print_R($res);
      $view=$this->load->view('web/include/enquiry',$data, true);
      echo $view;
    }

    public function storegiftcarddetails(){
      if($_POST){
        $param['order_no'] =$_POST['orderno'];
        $param['emailid'] =$_POST['emailid'];
        $param['mesg'] =$_POST['mesg'];
        $res=$this->liborder->getgiftcardorderdtl($param);
        if($res){
          $param['product_id']=$res[0]['product_id'];
          $param['price']=$res[0]['price'];
          $length=6;
          $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $charactersLength = strlen($characters);
          $randomString = '';

          for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
          }

          $param['giftcard_name']='GIF'.$randomString;
          $param['giftcard_image']=$res[0]['prothumbnail'];
          $date = date($res[0]['created']);
          $newdate = strtotime ( '+1 year' , strtotime ( $date ) ) ;
          $newdate = date ( 'Y-m-j' , $newdate );
          $param['validity']= $newdate;
          $res1=$this->liborder->savegiftcarddetails($param);
          if($res1==1){
            $to = $param['emailid'];
            $subject = "Giftcard From ".DOMAIN_NAME;
            $body .= MAILER_HEADER;
            $body .= "<!DOCTYPE html><html><head><style>div.container {width: 100%;border: 1px solid gray;}header, footer {padding: 1em;color: #cc0028; background-color: #ffffff;clear: left;text-align: center;}nav {float: left;max-width: 160px; margin: 0;padding: 1em;}nav ul {list-style-type: none;padding: 0;}nav ul a {text-decoration: none;}p{color:grey;}article {margin-left: 10px;padding: 1em;overflow: hidden;text-align:center;}</style></head><body><div class='container'><header><h3>Your egift card is ready</h3><img src='https://www.furtadosonline.com/images/logo_1.png' ></header><article><p>Hii, Congratulation,</p><p>Here is your Gift Cart. <br>
              Here is the code which you can use while reedemption <b>".$param['giftcard_name']."</b> on ".DOMAIN_NAME." <br>".$param['mesg']."</p><img src='".$param['giftcard_image']."' alt='giftcard' title='giftcard'></article></div></body></html>" ;
            $body .= MAILER_FOOTER;
            $Falconide=new Falconide();
            $result=$Falconide->sendmail($to,$subject,$body);
            if($result){
              $data = array('status' =>true ,'msg'=>'Giftcard  details saved successfully', 'error'=>false );
            }
          }else{
          $data = array('status' =>false ,'msg'=>'Giftcard  details are not saved, Please try again', 'error'=>true );
          }
          echo json_encode($data);
        }
      }
    }

    public function checkgiftcarddtl(){
      if($_POST){
        $param['promocode']  = $_POST['promocode'];
        $param['user_email'] = $this->libsession->getSession('usremail');
        //print_R($param); die;
        //$res=$this->libcart->promocodecheck($param);// This is the old method, new method has been defined in below called library
        $res=$this->giftcoupon_validator->validate($param);
        //print_r($res); die;
     
        /*if(is_array($res)){
          if($res ['gift_card_id']) {
            $giftcard['gift_code_applied'] = $res['code'];
            $giftcard['gift_code_value']   = $res['total_card_value'];
            $giftcard['gift_card_id']      = $res['gift_card_id'];
          }else if($res['coupon_code_id']){
            $giftcard['coupon_code_applied'] = $res['code'];
            $giftcard['coupon_code_value']   = $res['total_card_value'];
            $giftcard['coupon_id']           = $res['coupon_code_id'];
            $giftcard['validity']            = $res['validity'];
          }
        //print_r($giftcard); die;
        $this->libsession->setSession($giftcard);
        //print_R($_SESSION); die;
        //print_r($data); die;

        $data = array('status' =>true,'msg'=>'Giftcard or Couponcode applied', 'error'=>false );
        }else{

        $data = array('status' =>false,'msg'=>'Giftcard or Couponcode is not valid', 'error'=>true );
        //print_R($data);die;

        }
        echo json_encode($data);*/
        $giftcard=array();
        if($res['error']==false){
             $giftcard['coupon_code_applied'] = $res['coupon_code_applied'];
            $giftcard['coupon_code_value']   = $res['coupon_code_value'];
            $giftcard['coupon_id']           = $res['coupon_id'];
            $giftcard['validity']            = $res['validity'];

             $this->libsession->setSession($giftcard);
             //$data = array('status' =>true, 'msg'=>$res['message'], 'error'=>false);
             $data['status']=true;
             $data['msg']=$res['message'];
             $data['error']=false;
        }else{
             //$data = array('status' =>false, 'msg'=>$res['message'], 'error'=>true);
              $data['status']=false;
             $data['msg']=$res['message'];
             $data['error']=true;
        }
        //print_r($data); die;
        //die('here');
        echo json_encode($data);
      }
    }

    public function cancelcoupon(){
        $param['module']='cancelcoupon';
        $res=$this->giftcoupon_validator->cancelcoupon($param);
        if($res){
            echo '<script type="text/javascript">
            window.location = "'.SITEMOBURL.'product/webcheckout"
            </script>';
        }
    }

    public function getchequepayment(){
      $update['payment_method']  = 'Cheque';
      $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
      $update['orderinvoice_no'] = $orderinvoice_no;

      $finalamount=$this->liborder->gettotalamount($update['orderinvoice_no']);
      //print_r($finalamount); die;
      $update['total_amount']=$finalamount[0]['total_amount'];
      //print_R($update); die;

      if($this->libsession->getSession('shipping_cost')){
        $update['all_shiiping_dtl']      = $this->libsession->getSession('shipping_cost');
        $update['total_courier_charges'] = $update['all_shiiping_dtl']['total_shipping_cost'];
      }else{
        $update['total_courier_charges'] = 0;
      }

      //echo $paymentStatus['amount'].'<br>'.$cart_data['total_courier_charges']; die;
      //print_r($cart_data['total_courier_charges']); die;
      $update['final_amount_to_pay'] = $update['total_amount']+ $update['total_courier_charges'];
      //   $update['total_item_count'] = $paymentStatus['cnt'];
      if($this->libsession->getSession('gift_card_id')){
        $update['gift_card_id']      = $this->libsession->getSession('gift_card_id');
        $update['total_card_value']  = $this->libsession->getSession('total_card_value');
        $update['code'] = $this->libsession->getSession('code');

      }


      $this->liborder->updatepayment($update);
      #-- Now get current transaction details..
      $orderParams['orderinvoice_no'] = $orderinvoice_no;
      $orderParams['customer_id']     = $this->libsession->getSession('usrid');
      $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
      $update_order['order_pay_status'] = 1;
      $update_order['payment_gateway_response'] = '';
      $update_order['orderinvoice_no']  = $orderinvoice_no;

      $isFinalUpdate = $this->liborder->updatefinalorderdetails($update_order);
      //echo $isFinalUpdate; die;
      if($isFinalUpdate)
      {
        $this->_compelteOrder();
        echo '<script type="text/javascript">
        window.location = "'.SITEMOBURL.'product/thanksCustomer"
        </script>';

      }else{
      //redirect(SITEMOBURL);
        echo '<script type="text/javascript">
        window.location = "'.SITEMOBURL.'product/webcheckout"
        </script>';
      }
    }

    public function getneftpayment(){
      $update['payment_method']  = 'NEFT';
      $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
      $update['orderinvoice_no'] = $orderinvoice_no;

      $finalamount=$this->liborder->gettotalamount($update['orderinvoice_no']);
      //print_r($finalamount); die;
      $update['total_amount'] = $finalamount[0]['total_amount'];
      //print_R($update); die;

      if($this->libsession->getSession('shipping_cost')){
        $update['all_shiiping_dtl']      = $this->libsession->getSession('shipping_cost');
        $update['total_courier_charges'] = $update['all_shiiping_dtl']['total_shipping_cost'];
      }else{
      $update['total_courier_charges']=0;
      }

      //echo $paymentStatus['amount'].'<br>'.$cart_data['total_courier_charges']; die;
      //print_r($cart_data['total_courier_charges']); die;
      $update['final_amount_to_pay'] = $update['total_amount'] + $update['total_courier_charges'];
      //   $update['total_item_count'] = $paymentStatus['cnt'];
      if($this->libsession->getSession('gift_card_id')){
      $update['gift_card_id']     = $this->libsession->getSession('gift_card_id');
      $update['total_card_value'] = $this->libsession->getSession('total_card_value');
      $update['code']=$this->libsession->getSession('code');

      }
      $this->liborder->updatepayment($update);
      #-- Now get current transaction details..
      $orderParams['orderinvoice_no'] = $orderinvoice_no;
      $orderParams['customer_id']     = $this->libsession->getSession('usrid');
      $orderinvoice_no = $this->libsession->getSession('orderinvoice_no');
      $update_order['order_pay_status'] = 1;
      $update_order['payment_gateway_response'] = '';
      $update_order['orderinvoice_no']  = $orderinvoice_no;

      $isFinalUpdate = $this->liborder->updatefinalorderdetails($update_order);
      //echo $isFinalUpdate; die;
      if($isFinalUpdate)
      {
        $this->_compelteOrder();
        echo '<script type="text/javascript">
        window.location = "'.SITEMOBURL.'product/thanksCustomer"
        </script>';

      }else{
        //redirect(SITEMOBURL);
        echo '<script type="text/javascript">
        window.location = "'.SITEMOBURL.'product/webcheckout"
        </script>';
      }


    }

    public function saveproductfeedback(){
      if($this->libsession->getSession('usrid')){
        $param['user_email'] = $this->libsession->getSession('usremail');
        $param['comment']    = $_POST['data'];
        $param['ip']         = $_SERVER['REMOTE_ADDR'];
        $param['usrname']    = $_SESSION['usrname'];
        $res = $this->liborder->saveproductfeedback($param);
      if($res){
        $data['error']   = false;
        $data['message'] = 'Product Feedback Submitted Successfully';
      }else{
        $data['error']   = true;
        $data['message'] = 'Error Occur While Adding Product Feedback';
      }
      }else{
        $data['error']   = 'needlogin';
        $data['message'] = 'You Need To Login To Save Product Feedback';
      }
      //print_r($data); die;
      echo json_encode($data);
      // print_R($param); die;

    }

    public function payment(){
        $this->load->view('mobile/include/header');
        $this->load->view('mobile/payment');
    }

    public function checkip(){
      print_r($_SERVER['REMOTE_ADDR']);
    }

    //==================================Emi Calculation=====================

    public function productwiseemi(){
      $bank   = intval($_POST['bank']);
      $amount = intval($_POST['amount']);
      if($bank == 0 || $amount == 0){ 
        echo 0; 
        exit(); 
      }
      $output = '<table width="100%" border="1" cellspacing="0" cellpadding="5" style="border: 1px solid #999;">
                <tr style="font-weight:bold;background-color: #666;color: white;">
                <td>EMI Tenure</td>
                <td>Bank Interest Rate</td>
                <td align="right">Monthly Installments</td>
                <td align="right">Total Money</td>
                </tr>';

      $barr  = 'b'.$bank;
      //print $barr.'....................'.$amount; die;
      if($barr == 'b1'){
      $carr = array("3"=>"12", "6"=>"12", "9"=>"12", "12"=>"12");
      }
      if($barr == 'b2'){
      $carr = array("3"=>"13", "6"=>"13", "9"=>"13", "12"=>"13");
      }
      if($barr == 'b3'){
      $carr = array("3"=>"12", "6"=>"12", "9"=>"12", "12"=>"12");
      }
      if($barr == 'b4'){
      $carr = array("3"=>"12.50", "6"=>"12.50", "9"=>"12.50", "12"=>"12.50");
      }
      if($barr == 'b5'){
      $carr = array("3"=>"13", "6"=>"13", "9"=>"13", "12"=>"13", "18"=>"15", "24"=>"15");
      }
      if($barr == 'b6'){
      $carr = array('3' =>"13.9" ,"6"=>"13.9"); 
      }

      if($barr){
        foreach($carr as $month_key => $int){
          //print $month_key.'===='.$int; print '<br>'; 
          $emi_month      = $this->pmt($amount, $int, $month_key);
          $t              = number_format($emi_month,0,'','');
          $total_with_int = $t * $month_key;
          $output .= '<tr id="emiId_'.$month_key.'">
                      <td><b>'.$month_key.' Month</b></td>
                      <td align="center">'.$int.'%</td>
                      <td align="right"><i class="fa fa-inr"></i> '.$this->libsession->priceformat($emi_month).'</td>
                      <td align="right"><i class="fa fa-inr"></i> '.$this->libsession->priceformat($total_with_int).'</td>
                      </tr>';
        }
      }else{
        $output .='<tr><td><b>No Record Found!!</b></td></tr>';
      }
        $output .= '</table>';
      echo $output;
    }
    
    //==================================Emi Calculation:End=====================

    //======================EMI Formula=========================================
    function pmt($pAmount, $annualInterestper, $duration) {
      
      $amount = false;

    if($pAmount && $annualInterestper && $duration) { 
      $MonthlyInterest = ($annualInterestper/(12*100)); 
      $commanVal = ((1 + $MonthlyInterest) ** $duration);
      $amount = ceil(( $pAmount * $MonthlyInterest * $commanVal ) / ( $commanVal - 1)) ;
    }
    return $amount;
    }
    //======================EMI Formula:End=========================================
}
?>
