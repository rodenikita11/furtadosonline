<style>
   p {
   font-size: 14px;
   text-align: left;
   }
   .panel-body {
    padding: 15px;
    min-height: 162px;
}
.panel-heading {
    padding: 1px 15px;
}
.panel-heading h4 {
    font-size: 16px;
}
.panel-default>.panel-heading {
    font-weight: 600;
    text-transform: capitalize;
}
</style>
<div class="body-content" id="top-banner-and-menu">
<div class="container-fluid">
<div class="row">
   <!-- ============================================== CONTENT ============================================== -->
   <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150">
      <!-- ========================================== SECTION – HERO ========================================= -->
       <h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Our Stores</h2>
	   <h5 style="text-align: center;margin-bottom: 20px;">Please call to check showroom operational days as these change in adherence to local government guidelines.</h5>
      <div class="container">
         <div class="wide-banners wow fadeInUp outer-bottom-xs">
            <div class="row">
               <div class="col-xs-12">
                  <!-- <img src="<?php echo WEBIMG;?>staticpage/comingsoon.jpg" style="width:100%"> -->
                  <div class="w100per">

                    <div class='row'>
                     <?php for ($i=0; $i <count($map) ; $i++){
                          $storename=strtolower(str_replace(array('----', '---', '--'), '-', preg_replace('/(\,|\s|\-)/i', '-', $map[$i]['storeName'])));

                      ?>
                     <div class="col-md-4 col-lg-4 clearfix" >
                         <div class="panel panel-default" style="height: 224px;">
                             <div class="panel-heading" style="background: #cc0028;color: #fff;text-transform: uppercase;"> <h4> <?php echo $map[$i]['storeName'];?></h4></div>
                             <div class="panel-body">
                                <p style="font-size: 12px !important;"><?php echo str_replace(array(',,',', ,'),',',ucfirst($map[$i]['storeAddress']));?></p>
    								            <? if($map[$i]['telePhone1'] && $map[$i]['telePhone2']){?>
                                <p style="font-size: 12px !important;">Phone : <?php echo $map[$i]['telePhone1'].','.$map[$i]['telePhone2'];?> </p>
                                <?php } else if($map[$i]['telePhone2']){ ?>
                                <p style="font-size: 12px !important;">Phone : <?php echo $map[$i]['telePhone2'];?></p>
                                <?} else if ($map[$i]['telePhone1'] ){?>
                                <p style="font-size: 12px !important;">Phone : <?php echo $map[$i]['telePhone1'];?></p>
                                <?} else {?>  
                                <p style="font-size: 12px !important;">Phone : Not Avaliable</p>
                                <? } ?>


                          <? if($map[$i]['time']){?>
                           <p style="font-size: 12px !important;">Timing :<?php echo $map[$i]['time'];?></p>
                         <?}else{?>
                           <p style="font-size: 12px !important;">Timing :10:00AM - 10:00PM | Open 7 days a week</p>
                         <?}?>
                           <p style="font-size: 12px !important;">Closed On : <?php echo $map[$i]['closedOn'];?></p> 
                           <p class="last-row">
                              <?php if(!empty($map[$i]['googleMapLink'])){ ?>
                              <span class="pull-left"><a href="<?php echo SITEMOBURL;?>store/<?php echo $storename; ?>/<?php echo $map[$i]['storeId'];?>" class="viewbtn">View On Map</a></span>
                              <?php } ?>
                              <!-- <span class="pull-right mord"><a href="<?php echo SITEMOBURL;?>user/storelocator/<?php echo $map[$i]['id'];?>" class="text-uppercase"> MORE DETAILS <i class="fa fa-angle-right"></i> </a></span>-->
                           </p>
                             </div>
                         </div>
                     </div>
                     <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>