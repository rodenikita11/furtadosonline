<section>
            <div class="second-page-container" style="padding-top:130px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <?php $this->load->view('element/breadcrum');?>
                            <div class="block-product-detail">
                                <div class="row">
                                    <?php 
	
										$data['result1'] = $result1;
										$this->load->view('element/product_gallery_panel.php',$data);
									?>
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<input type="hidden" id="current_pro_id" value="<?php echo $result1[0]['prosellerid'];?>" readonly="readonly">
									    <div class="product-detail-section">
                                            <h3><?echo $result1[0]['proname']?><!--<span>(Bright Green)</span>--></h3>
                                            
											
                                            <div class="product-information" style="padding-left: 40px;">
                                                <div class="clearfix">
                                                    <label class="pull-left">Product Code:</label><?echo $result1[0]['prosku']?>
                                                </div>
									
                                                <div class="clearfix">
                                                    <label class="pull-left">Availability:</label>
                                                    <p>Available</p>
                                                </div>
                                                <div class="clearfix">
                                                    <label class="pull-left">Description:</label>
                                                    <div class="description" title="" style="padding-left:90px;"><?php echo $result1[0]['proshortspecification'];?></div>
                                                </div>
                                                <div class="clearfix">
                                                    <label class="pull-left">Points:</label>
													<span class="fS18 color1"><?php echo $result1[0]['pro_points']?></span>
													</p>
												
                                                </div>
                                                <div class="shopping-cart-buttons">
                                             	   <a href="javascript:void(0);" id='addtopoints' data-id='<? echo $result1[0]['prosellerid']?>' class="shoping"><i class="fa"></i>GET IT NOW</a>
												</div>
												<div  class="shopping-cart-buttons">
                                             	   <a href="../productlist" id='redeemmore' class="shoping" style='display:none' ><i class="fa"></i>REDEEM MORE</a>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
						
						<div class="clear"></div>
                         </div>
                    </div>
                </div>  
            </div>
        </section>
		<script type="text/javascript">
			addProductToCart = function(){
				var proId = jQuery("#current_pro_id").val();
				var quantity = jQuery("#pro_quantity").val();
				var product_size = '';
				//-- update by sanjay, 10-Sep
				if(jQuery("#selected_option").length>0)
				{
					 product_size = jQuery("#selected_option").val();
						if(product_size=="" || typeof product_size=='undefined')
									{
						alert("Please select product option, before adding.");
						return false;
								}
			}
			 //--update by sanjay, 10-Sep
				var is_gift_wraped = isGiftWraped(proId); //-- update on 8/Dec
				jQuery.ajax({
					url:APIURL+'spiderCart/ajax_addtocart?_'+Math.random(),
					type:'POST',
					data:{new_product_id:proId,quantity:quantity,is_gift_wraped:is_gift_wraped,product_size:product_size},
			}).done(function(resp){
					//return false; //--Temp
					var response = jQuery.parseJSON(resp);
					if(response.status==1)
					{
						injectpopup(response.add_to_cart_html);
						ajax_getCartCount();
					}
			});
		};
		
		get_amount_by_quantity = function(){
				var proPrice = jQuery("#current_pro_price").val();
				var quantity = jQuery("#pro_quantity").val();
					var max_quantity = jQuery("#pro_quantity").attr('max');
						if(parseInt(quantity)>parseInt(max_quantity))
						{
							quantity = max_quantity;
							jQuery("#pro_quantity").val(quantity);
						}
				total_amount = proPrice*quantity;
				jQuery("#f_amnt").html(total_amount);
				return false;
		};
		
		jQuery(document).ready(function(){
			jQuery("#buyers_pincode").live("keypress", function(e) {
							var code = (e.keyCode ? e.keyCode : e.which);
								if (code == 13) { jQuery("#get_availibility_trigger").click();}
						});
		});
		</script>
<style>
a:hover {
  color: red;
  position: relative;
}

a[title]:hover:after {
  content: attr(title);
  padding: 0px 10px;
  height:40px;
  line-height:40px;
  color: #fc5602;
  position: absolute;
  font-size:12px;
  right: -1px;
  top: -44px;
  white-space: nowrap;
  z-index: 99999px;
  background:#fff;
  border:solid 1px #fc5602;
}
</style>