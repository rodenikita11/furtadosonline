<div class="top-logo">
   <div class="container" style="padding:0;">
      <div class="row">
         <div class="col-md-12">
            <div class="col-xs-2">
               <a href="#" onclick="window.history.go(-1); return false;" >
                  <!--<img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;">-->
                  <i class="fa fa-angle-left" aria-hidden="true" style="color: #fff;padding-left: 20px;font-size: 40px;"></i>
               </a>
            </div>
            <div class="col-xs-9 Favourite_list">
               <span class="mycartnew">My Cart (<?echo $cnt?>) </span> 
               <!-- | 
               <span class="mubundlenew" style="opacity: 0.5;"><a href="<?echo SITEMOBURL;?>product/viewbundlecart" class="Favourite_list">Bundle Cart (<?echo $bundlecnt;?>)</a> </span> -->
            </div>
            <div class="col-xs-1" style="padding:0;">

               <!--<div class="col-xs-12">
                  <div class="col-xs-4 hidden-xs"></div>
                    <div class="col-xs-4 "><a href="#"><img src="img/icon/go.png" width="100%" ></a></div>
                   <div class="col-xs-4  hidden-xs"></div>
                  </div>-->
            </div>
         </div>
      </div>
   </div>
</div>
<div class="Favourite">
   <?php
      if($resp){
       foreach($resp as $key=>$val){ ?> 
   <div class="text-center">
      <div class="Container container-fluid addtocard row" id="<?echo $key;?>">
         <div class="col-xs-12 ">
            <div class="col-xs-4"><img src="<? if(!isset($val['image'])) { echo SITEIMAGES."mobimg/noimage1.png"; } else { echo $val['image'];} ?>" width="100%"></div>
            <div class="col-xs-8 Pearl_Drum" data-id="<?php echo $val['proid']?>">
               <p class="Pearl_Drum-text"><?echo $val['proname'];?></p>
               <p class="amount"><i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<b><?echo $this->libsession->priceformat($val['dicountprice']);?></b></p>
               <?if($val[stock]=='0'){?>
               <p class="stock">Not Avaliable</p>
               <?}else{?>
               <p class="stock">In Stock</p>
               <?}?>
               <div class="clear"></div>
               <div class="col-xs-12">
                  <div class="col-xs-2" style="margin-left: 13px;">Qty</div>
                  <div class="col-xs-2">
                  	<?$qunty=!empty($val['qty']) ? $val['qty'] : '' ;?>
                     <select id="qtychange_<?echo $key;?>" name="qtychange" class="qtych"  value="<?echo $qunty;?>"  style="width:110%;" onchange="updatecart(<?echo $key;?>);" >
                        <option <?php if($qunty == 1) echo "selected"; ?> value="1">1</option>
                        <option <?php if($qunty == 2) echo "selected"; ?> value="2">2</option>
                        <option <?php if($qunty == 3) echo "selected"; ?> value="3">3</option>
                        <option <?php if($qunty == 4) echo "selected"; ?> value="4">4</option>
                        <option <?php if($qunty == 5) echo "selected"; ?> value="5">5</option>
                        <option <?php if($qunty == 6) echo "selected"; ?> value="6">6</option>
                        <option <?php if($qunty == 7) echo "selected"; ?> value="7">7</option>
                        <option <?php if($qunty == 8) echo "selected"; ?> value="8">8</option>
                        <option <?php if($qunty == 9) echo "selected"; ?> value="9">9</option>
                        <option <?php if($qunty == 10) echo "selected"; ?> value="10">10</option>
                        <option <?php if($qunty == 11) echo "selected"; ?> value="11">11</option>
                        <option <?php if($qunty == 12) echo "selected"; ?> value="12">12</option>
                        <option <?php if($qunty == 13) echo "selected"; ?> value="13">13</option>
                        <option <?php if($qunty == 14) echo "selected"; ?> value="14">14</option>
                        <option <?php if($qunty == 15) echo "selected"; ?> value="15">15</option>
                     </select>
                  </div>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-2 delete" >
                     <img src="<?php echo SITEIMAGES;?>mobimg/icon/delete.png" onclick="removefromcart(<?echo $key;?>);"> 
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?}?>
   </div>
   <div class="clear"></div>
   <!-- First Container -->
   <div style="padding-right:0px !important;padding-left:0px !important;">
      <div class="row">
         <div class="col-xs-12" >
            <div class="flex-container ">
               <div class="col-xs-6 Change_Address">Total &nbsp;<i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<?echo $this->libsession->priceformat($amount);?></div>
               <div class="col-xs-6 Place_Order"><a href="<?php echo SITEMOBURL;?>product/checkoutuserdtl">CONTINUE</a></div>
            </div>
         </div>
      </div>
   </div>
   <div class="clear"></div>
   <?}
      else{?>
   <div class="Favourite-no">
      <p>No products present in the cart</p>
   </div>
   <?}?>
   <!--<h4 class="shownow">SHOP NOW!</h4>-->
</div>
<script>
   function updatecart(ele) {
   	var pid=ele;
       var qty= jQuery('#qtychange_'+ele).val();
      // console.log(pid+' next '+qty); return false;
       jQuery.ajax({
   		url:'<?php echo SITEMOBURL?>product/updatecart',
   		type:'POST',
   		data:{qty:qty,pid:pid},
   		//alert(data); 
   		success:function(data) {
   		//console.log(data); return false;  
   		 var response = jQuery.parseJSON(data); 
   		  
   			if(response.error=='false'){ 
   			     alert('Cart updated successfully'); 
   			     window.location.reload();
   				}else{
   				 alert('Error in updating  product to cart');	
   				}
   			
   		}
   
   	});
   	
       
   }
   function removefromcart(ele) {
   	var pid=ele;
      
       jQuery.ajax({
   		url:'<?php echo SITEMOBURL?>product/removefromcart',
   		type:'POST',
   		data:{pid:pid},
   		success:function(data)
   		{   var response = jQuery.parseJSON(data);
   		  
   			if(response.error=='false'){
   			     alert('Product  removed successfully'); 
   			     window.location.reload();
   				}else{
   				 alert('Error in removing  product from cart');	
   				}
   			
   		}
   
   	});
   	
       
   }
</script>
<script type="text/javascript">
   jQuery('.shownow').click(function(){
   	jQuery('.sh-btn-menu').click();
   })
</script>
<script>
   // jQuery( document ).ready(function() {
   //  jQuery('.qtych').val('<?echo $val['qty']?>');
   // });
</script>