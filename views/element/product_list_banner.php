	<?php
		$param['category']	= $this->catid;
		$result=$this->Do_category->getcatbanner($param);
	
	?>
		<?php if($result['catimage']){ 
		$img_attribute = getimagesize($result['catimage']);
				if(!is_array($img_attribute))
				{
					#-- Image is missing for category, get default image.
					$catimage = FILEBASEURL.IMAGEDEFAULT.'default_cat.jpg';
				}
				else
				{
					$catimage = $result['catimage'];
				}
			} else 
			{
				$catimage = FILEBASEURL.IMAGEDEFAULT.'default_cat.jpg';
			}
		
		?>
		
		<section class="home_banner in_page_banner">
			<div class="home_banner_in">
				<div style="height:300px;overflow:hidden;">
					<img src="<?php echo $this->lib_storeuri->getprothumbnail($catimage,1800);?>" class="img-responsive"  alt="slidebg1">
				</div>
			</div>
		</section>
		
		<section>
            <div class="revolution-container">
                <div class="">
                    <ul class="list-unstyled" style="list-style:nome;text-align:center;">	<!-- SLIDE  -->
                        <li>
							<div style="height:300px;overflow:hidden;">
								<img src="<?php echo $this->lib_storeuri->getprothumbnail($catimage,1800);?>" class="img-responsive"  alt="slidebg1">
							</div>
						</li>
                    </ul>
                    <div class="revolutiontimer"></div> 
                </div>
            </div>
		</section>