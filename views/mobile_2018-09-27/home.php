<div class="top-header-padding" style="min-height: 50px;">
   <!-- Jssor Slider Begin -->
   <!-- ================================================== -->
   <div id="slider1_container" style="visibility: hidden; position: relative; margin: 0 auto;
      top: -1px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
      <!-- Loading Screen -->
      <div u="loading" style="position: absolute; top: 0px; left: 0px;">
         <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
            top: 0px; left: 0px; width: 100%; height: 100%;">
         </div>
         <div style="position: absolute; display: block; background: url(../img/loading.gif) no-repeat center center;
            top: 0px; left: 0px; width: 100%; height: 100%;">
         </div>
      </div>
      <!-- Slides Container -->
      <div u="slides" style="position: absolute; left: 0px; top: 0px; width: 1300px; height: 500px; overflow: hidden;">
         <?php 
            $j=0;
               foreach ($banner['banner'] as $key => $value) { ?>
         <div>
            <a href="<?php echo $value['url'];?>" onclick="getbannercount('<?echo $value['url'];?>','<?echo $key ;?>')"><img u="image" src="<?php echo $value['imgpath'];?>" /></a>
         </div>
         <?php  $j++; 
            } ?>
      </div>
      <!--#region Bullet Navigator Skin Begin -->
      <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-no-jquery.html -->
      <!--#endregion Bullet Navigator Skin End -->
      <!--#region Arrow Navigator Skin Begin -->
      <!-- Help: http://www.jssor.com/development/slider-with-arrow-navigator-no-jquery.html -->
      <style>
         /* jssor slider bullet navigator skin 21 css */
         /*
         .jssorb21 div           (normal)
         .jssorb21 div:hover     (normal mouseover)
         .jssorb21 .av           (active)
         .jssorb21 .av:hover     (active mouseover)
         .jssorb21 .dn           (mousedown)
         */
         .jssorb21 {
         position: absolute;
         }
         .jssorb21 div, .jssorb21 div:hover, .jssorb21 .av {
         position: absolute;
         /* size of bullet elment */
         width: 19px;
         height: 19px;
         text-align: center;
         line-height: 19px;
         color: white;
         font-size: 12px;
         background: url(<?php echo SITEIMAGES;?>b21.png) no-repeat;
         overflow: hidden;
         cursor: pointer;
         }
         .jssorb21 div { background-position: -5px -5px; }
         .jssorb21 div:hover, .jssorb21 .av:hover { background-position: -35px -5px; }
         .jssorb21 .av { background-position: -65px -5px; }
         .jssorb21 .dn, .jssorb21 .dn:hover { background-position: -95px -5px; }
      </style>
      <!-- bullet navigator container -->
      <div u="navigator" class="jssorb21" style="bottom: 26px; right: 6px;">
         <!-- bullet navigator item prototype -->
         <div u="prototype"></div>
      </div>
   </div>
   <!-- Jssor Slider End -->
</div>

<div class="container container-fluid Browse" style="padding-left: 4px;">
   <h3> BROWSE BY CATEGORY </h3>
</div>
<? //print_r($category['category']);die; ?>
<!-- First Container img -->
<div class="maincat">
   <div class="row">
      <div class="col-xs-12  flex-container">
         <? foreach ($category['category'] as $key => $value) {?>
         <div class="col-xs-4 topimg">
            <a href="<?php echo $value['url']?>">
               <img src="<?php echo $value['img'] ? $value['img'] : SITEIMAGES.'mobimg/noimage1.png'?>">
               <? $name= str_replace('--', " & ", $value['name']);?>
               <?//$name= str_replace('-', " ", $value['name']);?>
               <p class="overflow-ellipsis"><?php echo ucfirst(strtolower($name));?></p>
            </a>
         </div>
         <?php  } ?>
         <div class="col-xs-4 topimg">
            <img src="<?php echo SITEIMAGES.'mobimg/icon/img06.png';?>" onclick="openmenu()">
            <p>more</p>
         </div>
      </div>
   </div>
</div>
<div class="instruments-top">
   <div class="col-xs-8 instruments">
      <p class='name'>Featured Products</p>
   </div>
   <?if(count($banner['product'])>3){?>
   <div class="col-xs-4 viewallhome">
      <p class="view"><a href="<?echo SITEMOBURL?>/product/featuredproducts">VIEW ALL</a></p>
   </div>
   <?}?>
</div>
<!-- instruments Container img -->
<div class="Electric bg-1 text-center ">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <?php 
            $count=count($banner['product']);
            $incre=0;
            foreach ($banner['product'] as $key => $value) { 
             
            include('include/list_product.inc');
            
            if($incre >=3){
            break;
            }else{
            $incre++;} 
            }
            
                     ?>
      </div>
   </div>
</div>
<!-- instruments Container -->
<div class="instruments-top">
   <div class="col-xs-8 instruments">
      <p class='name'>Hot Deal</p>
   </div>
   <?if(count($hotdeals['product'])>3){?>
   <div class="col-xs-4 viewallhome">
      <p class="view"><a href="<?echo SITEMOBURL?>product/hotdeals">VIEW ALL</a></p>
   </div>
   <?}?>
</div>
<!-- instruments Container img -->
<div class="Electric bg-1 text-center ">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <?php 
            $count=count($hotdeals['product']);
            $incre=0;
            foreach ($hotdeals['product'] as $key => $value) { 
             
            include('include/list_product.inc');
            
            if($incre >=3){
            break;
            }else{
            $incre++;} 
            }
            
                     ?>
      </div>
   </div>
</div>
<!-- instruments Container -->
<div class="instruments-top">
   <div class="col-xs-8 instruments">
      <p class='name'>New Arrivals</p>
   </div>
   <?if(count($newarrivals['product'])>3){?>
   <div class="col-xs-4 viewallhome">
      <p class="view"><a href="<?echo SITEMOBURL?>product/newarrivals">VIEW ALL</a></p>
   </div>
   <?}?>
</div>
<!-- instruments Container img -->
<div class="Electric bg-1 text-center ">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <?php 
            $count=count($newarrivals['product']);
            $incre=0;
            foreach ($newarrivals['product'] as $key => $value) { 
             
            include('include/list_product.inc');
            
            if($incre >=3){
            break;
            }else{
            $incre++;} 
            }
            
                     ?>
      </div>
   </div>
</div>
<!-- instruments Container -->
<div class="instruments-top">
   <div class="col-xs-8 instruments">
      <p class='name'>Newly launched books</p>
   </div>
   <?if(count($newarrivalbooks['product'])>3){?>
   <div class="col-xs-4 viewallhome">
      <p class="view"><a href="<?echo SITEMOBURL?>product/newarrivalbooks">VIEW ALL</a></p>
   </div>
   <?}?>
</div>
<!-- instruments Container img -->
<div class="Electric bg-1 text-center ">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <?php 
            $count=count($newarrivalbooks['product']);
            $incre=0;
            foreach ($newarrivalbooks['product'] as $key => $value) { 
             
            include('include/list_product.inc');
            
            if($incre >=3){
            break;
            }else{
            $incre++;} 
            }
            
                     ?>
      </div>
   </div>
</div>
<!-- <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:800px;height:300px;overflow:hidden;visibility:hidden;">
   <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
      <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%; "></div>
      <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
   </div>
   <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:300px;overflow:hidden;">
      <?php 
         //print_R($banner); die;  
         foreach ($banner['hotdeal_banner'] as $key => $value) { ?>
      <div>
         <a href="<?php echo $value['url'];?>"> <img data-u="image" src="<?php echo $value['imgpath'];?>" style="border-radius:30px;" /></a>
      </div>
      <?php  }
         ?>
   </div>
   <span data-u="arrowleft" class="jssora13l" style="top:0px;left:30px;width:40px;height:50px;" data-autocenter="2"></span>
   <span data-u="arrowright" class="jssora13r" style="top:0px;right:30px;width:40px;height:50px;" data-autocenter="2"></span>
</div> -->
<!-- instruments Container -->

<div class="stripbanner">
   <div class="row">
      <div class="col-xs-12"> 
         <a href="javascript:void(0)">
         <img src="<?php echo SITEIMAGES.'mobimg/kross_2_Mobile.jpg';?>" width="100%">
         </a>
      </div>
   </div>
</div>
<div class="bg-1 text-center Accessories">
   <div class="col-xs-8 instruments">
      <p class='name'>Clearance Products</p>
   </div>
   <?if($clerance){
      $incre=0;
      //if(count($clerance)>3){?>
   <div class="col-xs-4 viewallhome pull-right">
      <p class="view"><a href="<?echo SITEMOBURL?>/product/getcleranceproduct">VIEW ALL</a></p>
   </div>
   <?//}?>
</div>
<!-- accessories Container img -->
<div class="Electric  bg-1 text-center">
   <div class="row">
      <div class="col-xs-12 flex-container">
         <? foreach($clerance['product'] as $key => $value){ //print '<pre>';print_r($value);
            include('include/list_product.inc');
            
            if($incre >=3){
            break;
            }else{
            $incre++;} }
            
            }?>
      </div>
   </div>
</div>
<!-- Second img -->
<!-- First Container img -->
<div class="delivery">
   <div class="row">
      <div class="col-xs-12  flex-container">
         <div class="col-xs-3 topimg">
            <a href="#">
               <img src="<?php echo SITEIMAGES.'mobimg/icon/free_delivery.jpg';?>" 
                  width="100%" style='max-height:150px'>
               <p>Home <br>Delivery</p>
            </a>
         </div>
         <div class="col-xs-3 topimg">
            <a href="<?php echo ROOTURL; ?>user/assuranceprogram">
               <img src="<?php echo SITEIMAGES.'mobimg/icon/emi.jpg';?>" 
                  width="100%" style='max-height:150px'>
               <p>Assurance<br>Programme</p>
            </a>
         </div>
         <div class="col-xs-3 topimg">
            <a href="<?php echo ROOTURL; ?>user/paymenthelp">
               <img src="<?php echo SITEIMAGES.'mobimg/icon/options.jpg';?>" 
                  width="100%" style='max-height:150px'>
               <p>Payment<br>Options</p>
            </a>
         </div>
         <div class="col-xs-3 topimg">
            <a href="<?php echo ROOTURL; ?>user/returnpolicy">
               <img src="<?php echo SITEIMAGES.'mobimg/icon/easy.jpg';?>" 
                  width="100%" style='max-height:150px'>
               <p>Return<br>Policy</p>
            </a>
         </div>
      </div>
   </div>
</div>
<?php include('include/footer.php');?>
<script type="text/javascript">
   function openmenu () {
       jQuery('.sh-btn-menu').click();
   }
   function getbannercount(bannerurl,id){
      var bannerid=id;
      var bannerurl=bannerurl;
      var  url="<?php echo SITEMOBURL?>product/addbannercount";
          jQuery.ajax({
          type:"post",
          url:url,
          data:{bannerid:bannerid},
          success: function(data){
            //alert(data);

           // console.log(data); return false;
           window.location.href=bannerurl;
          }
        });
   }
</script>
<script></script>
<script type="text/javascript">
   jQuery(".firstrow0").addClass('active');
</script>
<style type="text/css">
   p.name {
    text-transform: uppercase;
    font-size: 16px;
   }
   .name{font-weight:bold;}
</style>