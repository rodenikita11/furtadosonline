<?php
		/*
				NAME : lib_communicator
				CREATED : 21/Nov
				DESC : Used for managing sms , email
		*/
 class Lib_communicator {

		public function __construct()
			{
				$this->CI =& get_instance();
				$this->CI->load->model('do_communicator');
			}
	
	function createmail($params){
		$this->CI->do_communicator->addtomailer($params);
	 }

	 function createsms($params){
		$this->CI->do_communicator->setSMS($params);
     }
}
?>