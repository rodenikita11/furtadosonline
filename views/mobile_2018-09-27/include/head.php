<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
   <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,500,800'
 rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/css/bootstrap-slider.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo SITECSS?>mobstyle.css">
  <link rel="stylesheet" href="<?php echo SITECSS?>mainmenu2.css">

  <link rel="stylesheet" href="<?php echo SITECSS?>main.2.0.css">
    <link rel="stylesheet" href="<?php echo SITECSS?>owl.carousel.min.css">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link href="<?php echo SITECSS?>mobstyle1.css" rel="stylesheet" type="text/css" media="all">
 

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <!--<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.8.0/bootstrap-slider.min.js"></script>
  	
  

  <script src="<?php echo SITEJS;?>jssor.slider-22.2.8.mini.js" type="text/javascript"></script>
  <script src="<?php echo SITEJS;?>jssor.slider-22.2.8.min.js" type="text/javascript"></script>
  <script src="<?php echo SITEJS;?>jssor.slider-22.2.8.debug.js" type="text/javascript"></script>  
  <script src="<?php echo SITEJS;?>jquery.magnific-popup.js" type="text/javascript"></script>
   <script src="<?php echo SITEJS;?>owl.carousel.min.js" type="text/javascript"></script>
  <script src="<?php echo SITEJS;?>header.js.download"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" rel="stylesheet" type="text/css" media="all">  
	 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
  <!-- product detail new design css and js--->
    
     
     <link rel="stylesheet" type="text/css" href="<?php echo SITECSS?>custom.css">
     
  
  <!--- end  product detail---->

     <script src="<?php echo SITEJS;?>jquery.magnific-popup.js"></script>
<!-----sign up---->
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- font files  -->
<link href='//fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
<link rel="canonical" href="<?php echo 'http://furtadosonline.com/' ?><?php echo (isset($seomodule)) ? 'product/' : ''; ?><?php echo ($seoname) ? $seoname.'-'.$seoid : ''; ?>">
<!--end-->
</head>
<style>
.overlay {
    background: #e9e9e9;
    display: none;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    opacity: 0.5;
    z-index: 9999999 !important;
    height: 100% !important;
}
#loading-img {
    background: url(http://furtadosonline.com/mobile/assets/images/ajax-loader.gif) center center no-repeat;
    height: 100%;
    z-index: 999999;
  }
</style>
<script type="text/javascript">
    jQuery(document).ajaxStart(function(){
      jQuery(".overlay").show();
     });

    jQuery(document).ajaxComplete(function(){
       jQuery(".overlay").hide();
    });
  </script>
  <div class="overlay">
  <div id="loading-img"></div>
</div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_IN/sdk.js#xfbml=1&version=v3.0&appId=239126193508543&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
