<?
class Lib_filters{
    public $ci;
    public function __construct() {
        $CI = & get_instance();
        $CI->load->library('libsession');
        $CI->load->library('libapi');
        $this->ci = $CI;
    }
    
	
    public function getFormattedProducts($res){
        $wishlist=$this->ci->libsession->getSession('wishlist');
        foreach($res['data'] as $row){
            if($wishlist){
                $arrwishlist=explode(',',$wishlist);
            if(in_array($row['proid'],$arrwishlist)){
                    $prodata['product'][$row['proid']]['wishlist']='1';
                }else{
                    $prodata['product'][$row['proid']]['wishlist']='0';
                }
            }else{
                $prodata['product'][$row['proid']]['wishlist']='0';
            }
            $proname=preg_replace('/[^A-Za-z0-9\-]/', '-', $row['proname']);
            $proname=str_replace("--","-", $proname) ;
            $proname=strtolower(rtrim($proname,'-'));
            $proid=$row['proid'];
            $prodata['product'][$row['proid']]['id']= $proid;
        // $prodata['product'][$row['proid']]['url']=''.SITEURL.''.$proname.'/'.$proid.'';
            $prodata['product'][$row['proid']]['url']=''.SITEMOBURL.''.$proname.'/'.$proid.'';
            $prodata['product'][$row['proid']]['name']=$row['proname'];
            $prodata['product'][$row['proid']]['catname']=$row['catname'];
            $prodata['maincatname']=$row['catname'];
            if($row['prothumbnail']){
                //echo SITEURL; echo FILEBASEPATH; echo $row['prothumbnail'];  die;

                //$image1=str_replace("http://192.168.1.100/FMP",FILEBASEPATH,$row['prothumbnail']);
                $image1=str_replace("http://furtadosonline.com/",ROOTURL,$row['prothumbnail']);
                    $image1=str_replace("uploads","upload",$image1);
                // echo 	$image1; die;
            // if(file_exists($image1)){
                    //echo "hi"; die;
                $prodata['product'][$row['proid']]['img']=(stripos($row['prothumbnail'], 'http:')!==false) ? str_replace('http:', 'https:', $row['prothumbnail']) : $row['prothumbnail'];
                // }else{
                // $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';
                // }
            }else{

            $prodata['product'][$row['proid']]['img']=SITEIMAGES.'mobimg/noimage1.png';

            }
            //echo $prodata[$row['proid']]['img'];die;
            $prodata['product'][$row['proid']]['price']=$row['onlineprice'];
            $prodata['product'][$row['proid']]['onlinePrice']=$row['onlineprice'];
            $prodata['product'][$row['proid']]['discountprice']=$row['discountprice'];
            $prodata['product'][$row['proid']]['avaliable']=$row['isavailable'];
            $setprice=$row['onlineprice']-$row['discountprice'];
            $prodata['product'][$row['proid']]['percentage']= round($setprice/$row['onlineprice']*100);
        //@todo below records are expected but not returned by elastic search
        $prodata['product'][$row['proid']]['callforprice']=$row['callforprice'];
        $prodata['product'][$row['proid']]['qty']=$row['quantity'];
        $prodata['product'][$row['proid']]['shortspecification']=$row['proshortspecification'];
        $prodata['product'][$row['proid']]['longdesc']=$row['prolongspecification'];
        $prodata['product'][$row['proid']]['review']=$row['rvcnt'];
        $prodata['product'][$row['proid']]['rating']=$row['overallrating'];
        // ends
            $prodata['product'][$row['proid']]['brandid']=$row['brandid'];
        }

        $res_prodata = $prodata;
        return $res_prodata;
    }

    public function getCommonFilters($param){
        if($param['isFilters'] && $param['url']){
            $url = $param['url'];
			$url_filters = $url."&filters=true";
			// {"third_catid":"1127","third_catname":"WOODWIND ACCESSORIES","first_catid":"2","second_catid":"111","first_catname":"Music Books","second_catname":"MISCELLANEOUS BOOKS"}
			$get_data_filters = $this->ci->libapi->callAPI('GET', $url_filters , false);
			$res_filters = json_decode($get_data_filters, true);
			$cat_len = count($res_filters['data']['unique_cat']);
			// structuring categories to fit UI
			$x=0;
			for( $i=0; $i < $cat_len; $i++ ){
				$newarray['id']=$res_filters['data']['unique_cat'][$i]['third_catid'];
				$newarray['name']=$res_filters['data']['unique_cat'][$i]['third_catname'];

				if($param['module']=='brandsfilters'){
					$newarray['url']=SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
				
				}else if($param['module']=='featuredproducts' || $param['module']=='newarrivalbooks' || $param['module']=='newarrivals' || $param['module']=='hotdeals'){
						$newarray['url']=SITEURL.'product/'.$param['module'].'?x=&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'];
						
				}else{
					if($param['bookofthemonth']==1){
						$newarray['url']=SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
					}else{
						$newarray['url']=SITEURL.'product/search/?maincat='.$res_filters['data']['unique_cat'][$i]['first_catid'].'&searchText='.$param['search'].'&level=3&catid='.$res_filters['data']['unique_cat'][$i]['third_catid'].'&catname='.$res_filters['data']['unique_cat'][$i]['third_catname'];
					}
				}

				if($param['module']=='brandsfilters'){
					$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.$param['brandname'].'/'.$param['brandid'].'/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
				}else if($param['module']=='promotionsfilters'){
					//---no need to create anything----//
				}else{
					if($param['bookofthemonth']==1){
						$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/bookofthemonth/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];
					}else{
						// realx for now
						if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
							$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['url'] = SITEURL.'product/search/?maincat=&searchText='.$param['search'].'&catid='.$val['second_catid'].'&level=2&catname='.$res_filters['data']['unique_cat'][$i]['second_catname'];	
						}
					}
				}
				if(!empty($res_filters['data']['unique_cat'][$i]['first_catname']) && !empty($res_filters['data']['unique_cat'][$i]['second_catname'])){
					$newfilterarray[$res_filters['data']['unique_cat'][$i]['first_catname']][$res_filters['data']['unique_cat'][$i]['second_catname']]['sub'][] = $newarray;
				}

				$x++;
			}

			// Structuring Brand to fit UI
			$brand_len = count($res_filters['data']['unique_brand']);
			for( $m=0; $m < $brand_len; $m++ ){
				$res_filters['data']['unique_brand'][$m]['brandid'] = $res_filters['data']['unique_brand'][$m]['id'];
			}

			$res_data['brandfilter'] = $res_filters['data']['unique_brand'];
            $res_data['catfilter'] = $newfilterarray;
            
            return $res_data;
        }
        else{
            return false;
        }
    }
        
}