<div class="top-logo">
   <div class="container" style="padding:0;">
      <div class="row">
         <div class="col-md-12">
            <div class="col-xs-2">
               <a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>
            </div>
            <div class="col-xs-7 Favourite_list">Career</div>
            <div class="col-xs-3"style="padding:0;">
            </div>
         </div>
      </div>
   </div>
</div>
<div class="Container container-fluid about-us">
   <div class="row">
      <div class="col-xs-12">
         <div class="panel-group" id="accordion">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> <b> Sales Representative- Lajpat Nagar (Delhi)</b></a>
                  </h4>
               </div>
               <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                     <table cellpadding="10" cellspacing="10" width="100%" style="border: 1px solid #ddd">
                        <tr>
                           <td style="font-size: 14px;"><b>Profile</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Understand the given target for the month and plan an achievement process in terms of handling the walk ins Understand the customers needs Try and up sell where ever necessary or cross sell. Explain the product to the Customer Should possess excellent communication and sales skills.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;"><b>Qualification</b></td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;">
                              <p>
                                 HSC/ Any Graduate
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Experience</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 1-2 years experience. Freshers can also apply. Playing any instrument will be an added advantage
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Location</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 New Delhi
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Salary</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 As per Industry Standards
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Post Date</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Dec 11, 2017
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <a href="mailto:careers@furtadosonline.com?subject=Application for post of Sales Representative- Lajpat Nagar (Delhi)" class="btn" style="background: #cc0028;color: #fff ! important;padding: 8px;font-size: 14px;"><strong>Apply for this Job</strong></a>   
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><b>Store Manager- Pune</b></a>
                  </h4>
               </div>
               <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                     <table cellpadding="10" cellspacing="10" width="100%" style="border: 1px solid #ddd">
                        <tr>
                           <td style="font-size: 14px;"><b>Profile</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Ensure smooth functioning of day to day operations of the store. 2. Develop business strategies to raise our customers’ pool, expand store traffic and optimize profitability. 3. Meet sales goals by training, motivating, mentoring and providing feedback to sales staff. 4. Ensure high levels of customers satisfaction through excellent service. 5. Complete store administration and ensure compliance with policies and procedures. 6. Maintain outstanding store condition and visual merchandising standards. 7. Report on buying trends, customer needs, profits etc. 8. Conduct regular store meetings.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;"><b>Qualification</b></td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;">
                              <p>
                                 HSC/ Any Graduate
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Experience</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Above 3 Years
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Location</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Pune
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Salary</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 As per Industry Standards
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Post Date</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Mar 24, 2017
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <a href="mailto:careers@furtadosonline.com?subject=Application for post of Sales Representative- Lajpat Nagar (Delhi)" class="btn" style="background: #cc0028;color: #fff ! important;padding: 8px;font-size: 14px;"><strong>Apply for this Job</strong></a>   
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><b>Sales Representative- Baner (Pune)</b></a>
                  </h4>
               </div>
               <div id="collapse3" class="panel-collapse collapse">
                  <div class="panel-body">
                     <table cellpadding="10" cellspacing="10" width="100%" style="border: 1px solid #ddd">
                        <tr>
                           <td style="font-size: 14px;"><b>Profile</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Understand the given target for the month and plan an achievement process in terms of handling the walk ins Understand the customers needs Try and up sell where ever necessary or cross sell. Explain the product to the Customer Should possess excellent communication and sales skills.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;"><b>Qualification</b></td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;">
                              <p>
                                 HSC/ Any Graduate
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Experience</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 1-2 years experience. Freshers can also apply. Playing any instrument will be an added advantage.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Location</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Pune
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Salary</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 As per Industry Standards
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Post Date</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Feb 20, 2017
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <a href="mailto:careers@furtadosonline.com?subject=Application for post of Sales Representative- Lajpat Nagar (Delhi)" class="btn" style="background: #cc0028;color: #fff ! important;padding: 8px;font-size: 14px;"><strong>Apply for this Job</strong></a>   
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><b>Sales Representative</b></a>
                  </h4>
               </div>
               <div id="collapse4" class="panel-collapse collapse">
                  <div class="panel-body">
                     <table cellpadding="10" cellspacing="10" width="100%" style="border: 1px solid #ddd">
                        <tr>
                           <td style="font-size: 14px;"><b>Profile</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Understand the given target for the month and plan an achievement process in terms of handling the walk ins Understand the customers needs Try and up sell where ever necessary or cross sell. Explain the product to the Customer Should possess excellent communication and sales skills.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;"><b>Qualification</b></td>
                        </tr>
                        <tr>
                           <td style="font-size: 14px;">
                              <p>
                                 HSC/ Any Graduate
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Experience</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 1-2 years experience. Freshers can also apply. Playing any instrument will be an added advantage.
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Location</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Pune
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Salary</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 As per Industry Standards
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td><b>Post Date</b></td>
                        </tr>
                        <tr>
                           <td>
                              <p>
                                 Feb 20, 2017
                              </p>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <a href="mailto:careers@furtadosonline.com?subject=Application for post of Sales Representative- Lajpat Nagar (Delhi)" class="btn" style="background: #cc0028;color: #fff ! important;padding: 8px;font-size: 14px;"><strong>Apply for this Job</strong></a>   
                           </td>
                        </tr>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-------2 img--------------->
<?php include('include/footer.php');?>