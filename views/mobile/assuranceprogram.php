<div class="top-logo">
<div class="container" style="padding:0;">
<div class="row">
<div class="col-md-12">

<div class="col-xs-2">
<a href="#" onclick="window.history.go(-1); return false;" ><img src="<?php echo SITEIMAGES;?>mobimg/icon/arrow03.png" style="padding:6px;"></a>

</div>
<div class="col-xs-7 Favourite_list">Assurance Program</div>

<div class="col-xs-3" style="padding:0;">
  
 </div>
	</div>
</div>
</div>
</div>
<style>ul.b {list-style-type: square;
margin: 5px 0 0 17px;}
.greathp{padding-top: 10px;}
li{    margin-bottom: 6px;}</style>

<div class="Container container-fluid about-us">
<div class="row">
	<div class="col-xs-12">
	
<h4 class="greathp" style="color:#cc0028;">Piano Tuning</h4>
	<ul class="b"><li>We will provide 2 FREE piano tuning sessions within the first 12 months of purchase.</li></ul>
	
   <h4 class="greathp" style="color:#cc0028;">Terms and conditions of Warranty (This warranty is valid in India only)</h4>
	<ul class="b">
	<li>Any new product purchased has a warranty to be free from defects in material and workmanship for the number of years specified on the warranty, subject to limitations contained in the warranty.</li>
	
	<li>The warranty is extended to the original retail purchaser only and may not be transferred or assigned to subsequent owners.</li>
	
	<li>The customer has to produce the original Furtados Assurance Certificate which should have the date of purchase and/or the invoice, failing which the warranty would not be honoured.</li>
	
	<li>If at any time the product purchased malfunctions as a result of faulty material or workmanship, Furtados will repair the defect/s or replace the product as it deems appropriate at its sole discretion.</li>
	
	<li>The product will have to be produced at the retail showroom for repair or shipped to our service centre, freight pre-paid by the customer. If the product is to be shipped back to the owner after repair the freight charges will need to be borne by the owner. Insurance for the product during transit – to and fro – is highly recommended. Furtados will not be liable for any damage that may occur in transit and delivery of the product.</li>
	
	<li>Furtados reserves the right to use substitute materials at the time of repair in the event that the original materials are no longer available.</li>
	
	<li>In the case of an unlikely event where the product is destroyed, lost or damaged beyond repair, while in the possession of Furtados, it will be replaced with one of the same or most similar style of a value not in excess of the original purchase price of the product. Any insurance covering the product including but not limited to the Collector’s Item value insurance, must be carried by the owner at the owner’s expense.</li>
	
	<li>Furtados makes no other express warranty of any kind whatsoever. All implied warranties, including warranties of merchantability and fitness for a particular purpose, exceeding the specific provisions of this warranty are hereby disclaimed and excluded from this warranty.</li>
	<li>Furtados shall not be liable for any special, indirect, consequential, incidental or other similar damages suffered by the purchaser or any third party, including without limitation, damages for loss of profits or business or damages resulting from use or performance of the product, whether in contract or in tort, even if Furtados or its authorized representative has been advised of the possibility of such damages. Furtados shall not be liable for any expenses, claims, or suits arising out of or relating to any of the foregoing.</li>
	<li>Furtados will not be liable for any irregularities in service in the case of a warranty which is dependent on a third party.</li>
	<li>Furtados shall not be liable or be deemed to be in default for any delay or failure in performance resulting directly or indirectly from causes beyond its reasonable control including delay in repairing due to non-availability of component and/or accessory especially vis-a-vis the import of supplies or if the company is prevented from performing its functions under the warranty.</li>
	<li>All disputes will be subject to the exclusive jurisdiction of Mumbai, India, only.</li>
	</ul>
	
	
<h4 class="greathp" style="color:#cc0028;">The warranty will not apply in the following situations (Limitations of Warranty)</h4>
<ul class="b">
		<li>Any product that has been altered or modified in any way or upon which the serial number has been tampered with or altered.</li>
		
		<li>Any product whose warranty form has been altered or upon which false information has been given.</li>
		
		<li>Any product that has been damaged due to misuse, negligence, accident or improper operation.</li>
		
		<li>The subjective issue of tonal characteristics.</li>
		
		<li>Any repair work carried out/ tampered by unauthorized personnel/agencies.</li>
		
		<li>Shipping damages of any kind.</li>
		
		<li>Any product that has been subjected to extremes of humidity or temperature.</li>
		
		<li>Normal wear and tear (For e.g. keypads, springs for wind instruments, drum lugs and pedals, worn frets, worn machine heads, worn plating, string replacement, scratched pick guards, LCD display, casing, moving parts, tube valves or damages to or discoloration of the product finish for any reason)</li>
		
		<li>Essential consumables required for the proper functioning of the product including but not limited to guitar strings, wind instrument reeds and drum heads supplied with/ as part of the product are not covered under any warranty.</li>
		
		<li>More than one replacement of rubber pads and switches. Rubber pads and switches will be replaced free of charge for one time within the warranty period.</li>
		
		<li>Any factory installed electronics in acoustic instruments, after a period of 1 year following the original date of purchase.
		</li>
		
		<li>Any product damaged by exposure to insects, pests or like organisms.</li>
		
		<li>Any form of damage resulting from fire, water, unregulated or defective power supply, riot, mishandling, commercial use or any act of nature and use contrary to operating instructions specified in the user manual supplied with the equipment.</li>
		
		<li>Damage resulting out of excessive force and/or repetitive non-standard actions.</li>
		
		<!--<li>Furtados makes no other express warranty of any kind whatsoever. All implied warranties, including warranties of merchantability and fitness for a particular purpose, exceeding the specific provisions of this warranty are hereby disclaimed and excluded from this warranty.</li>
		
		<li>Furtados shall not be liable for any special, indirect, consequential, incidental or other similar damages suffered by the purchaser or any third party, including without limitation, damages for loss of profits or business or damages resulting from use or performance of the product, whether in contract or in tort, even if Furtados or its authorized representative has been advised of the possibility of such damages. Furtados shall not be liable for any expenses, claims, or suits arising out of or relating to any of the foregoing.</li>
		
		<li>All disputes will be subject to Mumbai (India) Jurisdiction only.</li>
		
		<li>Furtados will not be liable for any irregularities in service in the case of a warranty which is dependent on a third party.</li>
		
		<li>Furtados shall not be liable or be deemed to be in default for any delay or failure in performance resulting directly or indirectly from causes beyond its reasonable control including delay in repairing due to non availability of component and/or accessory specially vis a vis the import of supplies or if the company is prevented from performing its functions under the warranty.</li>-->

</ul>

	





	</div>
 </div>
 </div>
 <!-------2 img--------------->
 
<?php include('include/footer.php');?>