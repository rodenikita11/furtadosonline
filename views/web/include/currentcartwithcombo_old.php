<?php
if(count($res) > 0){
	foreach($res as $key=>$val){
		?>

	<div class="col-xs-12 col-sm-6 col-md-3 gallery-holder">
   
   <div class='col-sm-6 col-md-9 product-info-block'>
		<div class="product-item-holder size-big single-product-gallery small-gallery">
         <div class="single-product-gallery-item col-sm-3 col-md-3" id="slide1">
            <a data-lightbox="image-1" data-title="Gallery">
            <img class="img-responsive" alt="" src="<?php if(!isset($val['image'])){ echo SITEIMAGES.'mobimg/noimage1.png';}else{echo $val['image'];}?>"/>
            
            </a>
         </div>
         <!-- /.single-product-gallery-item -->
      </div>
      <div class="product-info">
         <h1 class="name"><?php echo $val['proname'];?></h1>
         <div class="rating-reviews">
            <div class="row">
               <div class="col-sm-12">
                  <!-- <div class="reviews">
                     <a href="#" class="lnk">Item No.130034</a>
                  </div> -->
               </div>
            </div>
            <!-- /.row -->    
         </div>
         <!-- /.rating-reviews -->
         <div class="price-container info-container m-t-20" style='margin-left: 129px;margin-top: 7px;'>
            <div class="row">
               <div class="col-sm-9">
                  <div class="price-box">
                     <span class="price m-r-12">&#8377; <?php echo $val['dicountprice'];?></span>
                     <span class="price-strike m-r-12 ">&#8377; <?php echo $val['price'];?></span>
                     <?if($val[stock]=='0'){?>
                      <span class="value1 red">out of Stock</span>
                       <?}else{?>
                      <span class="value1 "> In Stock</span>
                       <?}?>
					    <a href="javascript:void(0);" class="btn btn-inverse delet" onclick="removefromcombocart(<?echo $key;?>,44);"><i class="glyphicon glyphicon-trash"></i></a>
                  </div>
               </div>
            </div>
            <!-- /.row -->
         </div>
         <!-- /.price-container -->
      </div>
      <!-- /.product-info -->
   </div>
   		<?php if(is_array($val['combo_product'])){
   			foreach($val['combo_product'] as $combokey=>$comboval){ ?>
   				<div class='col-sm-6 col-md-9 product-info-block'>
					<div class="product-item-holder size-big single-product-gallery small-gallery">
			         <div class="single-product-gallery-item col-sm-3 col-md-3" id="slide1">
			            <a data-lightbox="image-1" data-title="Gallery">
			            <img class="img-responsive" alt="" src="<?php if(!isset($comboval['image'])){ echo SITEIMAGES.'mobimg/noimage1.png';}else{echo $comboval['image'];}?>"/>
			            
			            </a>
			         </div>
			         <!-- /.single-product-gallery-item -->
			      </div>
			      <div class="product-info">
			         <h1 class="name"><?php echo $comboval['proname'];?></h1>
			         <div class="rating-reviews">
			            <div class="row">
			               <div class="col-sm-12">
			                  <!-- <div class="reviews">
			                     <a href="#" class="lnk">Item No.130034</a>
			                  </div> -->
			               </div>
			            </div>
			            <!-- /.row -->    
			         </div>
			         <!-- /.rating-reviews -->
			         <div class="price-container info-container m-t-20" style='margin-left: 129px;margin-top: 7px;'>
			            <div class="row">
			               <div class="col-sm-9">
			                  <div class="price-box">
			                     <span class="price m-r-12">&#8377; <?php echo $comboval['dicountprice'];?></span>
			                     <span class="price-strike m-r-12 ">&#8377; <?php echo $comboval['price'];?></span>
			                     <?if($comboval[stock]=='0'){?>
			                      <span class="value1 red">out of Stock</span>
			                       <?}else{?>
			                      <span class="value1 "> In Stock</span>
			                       <?}?>
								    <a href="javascript:void(0);" class="btn btn-inverse delet" onclick="removefromcombocart(<?echo $key;?>,44);"><i class="glyphicon glyphicon-trash"></i></a>
			                  </div>
			               </div>
			            </div>
			            <!-- /.row -->
			         </div>
			         <!-- /.price-container -->
			      </div>
			      <!-- /.product-info -->
			   </div>
   		<?php } } ?>
</div>
<?php } } ?>