<style>
   p {
   font-size: 14px;
   text-align: justify;
   }
</style>
<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row">
         <div class="col-lg-12 outer-top-150">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;text-align: center;">Return Policy</h2>
            <p> We always endeavour to supply the ordered products in good condition. Kindly verify that the item ordered by you is as per your requirement at the time of placing your order. We are unable to accept returns from customers under any circumstances other than manufacturing defects. In the unlikely event that the product is received with a manufacturing defect or is damaged in transit, we request you to inform us via email on response@furtadosonline.com or call us on +91 22 42875050/ 60.</p>
            <p> Please read the details below to have a seamless hassle-free experience. We will do our best to replace approved claims for any damaged in-transit product or defective product. Only if we do not have a replacement to offer will a refund be processed.</p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;"> Non-returnable/Nonrefundable items:</h2>
            <p> All items purchased on Furtadosonline are non-returnable/ non-refundable. The only exception is made for products received in damaged or defective condition and this information is sent to us by e-mail within the stipulated period.</p>
            <p> Please do not accept any packages which are damaged or tampered with. In such a case, please return the product to the courier delivery person immediately. All customers are requested to inform us of any wrongly delivered products, products damaged in transit and products with manufacturing defects within 48 hours from their delivery at the customer's address. Claims beyond 48 hours of delivery will not be entertained.</p>
            <p>*Clearance products once sold cannot be returned or replaced.</p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;"> Grievance Redressal</h2>
            <p> Please send us an email on response@furtadosonline.com with the full description of the issue being faced with the product. The inclusion of a few photographs or short video of the problem will greatly help to arrive at a speedy resolution. You can also call us on +91 22 42875050/ 60. We will investigate the issue and revert with the solution. You are requested to kindly send back the product only after receiving a return authorization from us. Items sent without a return authorisation will not be accepted/ or processed for refund. </p>
            
         </div>

          <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">When will you receive your refund?</h2>
            <p>1.Refunds will reflect in the customer bank account within 15 working days as per the bank/credit card issuer/payment gateway policy. The refund will happen through the same mode that the payment was made. FMIL will try their best to assist as much as they can, however, we will not be responsible for any delays from the bank/credit card issuer/payment gateway. </p>
            <p>2. The refund will be processed once the product has been received by us in intact condition and after proper examination by our after-sales team.
</p>
            
         </div>

      </div>
   </div>
</div>