<?php include('includes/head.php'); ?>
<div class="content_wrapper">
				<!-- banner -->
				<section class="banner_home">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="flexslider">
									<ul class="slides">
										<li>
											<div class="banner_text">
												<span>Guitars</span>
												<a href="#">browse collection</a>
											</div>
											<img src="<?php echo ASSETURL; ?>images/banner.jpg" alt="">
										</li>
										<li>
											<div class="banner_text">
												<span>Guitars</span>
												<a href="#">browse collection</a>
											</div>
											<img src="<?php echo ASSETURL; ?>images/banner.jpg" alt="">
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end banner -->
				<!-- feature product hot deals -->
				<section class="feature_pr_hot_deals">
					<div class="container">
						<div class="row">
							<article class="col-lg-9 col-md-9 col-sm-9">
								<div class="featur_pr">
									<h3 class="co_heading">Most Selling</h3>
									<a href="#" class="more_pr">MORE</a>
									<ul class="pr_list">
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
											</div>
											<a href="#">
												<div class="pr_img">
													<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
												</div>
												<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><strong>22</strong> Reviews</span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
												<span class="onsale">ONSale</span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
												<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
												<span class="free">free</span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
												<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li class="last_child">
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
												<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</article>
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<div class="hot_deals">
									<h3 class="co_heading">Hot Deals</h3>
									<div class="owl-carousel">
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_6.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_7.jpg" alt=""></a></li>
												<li><a href="#"><img src="<?php echo ASSETURL; ?>images/img_5.jpg" alt=""></a></li>
											</ul>
										</div>
									</div>
								</div>
							</aside>
						</div>
					</div>
				</section>
				<!-- end feature product hot deals -->
				<!-- categorioe advertise -->
				<section class="cate_advertise">
					<div class="container">
						<div class="row">
							<article class="col-lg-9 col-md-9 col-sm-9">
								<div class="in_categories">
									<h3 class="co_heading">Category</h3>
									<ul>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_15.jpg" alt="">
										</li>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_8.jpg" alt="">
										</li>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_9.jpg" alt="">
										</li>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_10.jpg" alt="">
										</li>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_11.jpg" alt="">
										</li>
										<li>
											<div class="hover_color">
												<a href="#" class="more">More</a>
											</div>
											<span class="cate_name">Pianos</span>
											<img src="<?php echo ASSETURL; ?>images/img_12.jpg" alt="">
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</article>
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<div class="advertise">
									<div class="find_search">
										<h3 class="co_heading">Find Our Stores</h3>
											<div id="custom-search-input" class="find_se">
												<div class="input-group">
													<input type="text" class="form-control input-lg" placeholder="Buscar" />
													<span class="input-group-btn">
														<button class="btn btn-info btn-lg" type="button">

														</button>
													</span>
												</div>
											</div>
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
									</div>
									<div class="in_add">
										<a href="#"><img src="<?php echo ASSETURL; ?>images/img_14.jpg" alt=""></a>
									</div>
								</div>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end categorioe advertise -->
				<!-- banner add sec -->
				<section class="banner_add">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<a href="#"><img src="<?php echo ASSETURL; ?>images/img_16.jpg" alt=""></a>
							</div>
						</div>
					</div>
				</section>
				<!-- end banner add sec -->
				<!-- most selling and add -->
				<section class="most_sellin_add">
					<div class="container">
						<div class="row">
							<article class="col-lg-9 col-md-9 col-sm-9">
								<div class="featur_pr">
									<h3 class="co_heading">Most Selling</h3>
									<a href="#" class="more_pr">MORE</a>
									<ul class="pr_list">
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
											</div>
											<a href="#">
												<div class="pr_img">
													<img src="<?php echo ASSETURL; ?>images/img_1.jpg" alt="">
												</div>
												<h4 class="pr_name">Hohner, Acoustic-Electric Ukulele, Concert, ULU21CES / With Pick Up</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><strong>22</strong> Reviews</span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
												<span class="onsale">ONSale</span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_2.jpg" alt=""></div>
												<h4 class="pr_name">Yamaha, Electronic Keyboard PSR S670</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li>
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
												<span class="free">free</span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_3.jpg" alt=""></div>
												<h4 class="pr_name">Epiphone, Electric Guitar, Les Paul Ultra III -Midnight Sapphire ENU3MSNH1</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
										<li class="last_child">
											<div class="offer_det">
												<span class="love"><a href="#"></a></span>
											</div>
											<a href="#">
												<div class="pr_img"><img src="<?php echo ASSETURL; ?>images/img_4.jpg" alt=""></div>
												<h4 class="pr_name">Cort, Electric Guitar, X-11, H-S-H -Black Cherry Sunburst</h4>
												<div class="pr_price">
													<span class="or_price">19,350</span>
													<span class="discount_price">68,480</span>
												</div>
											</a>
											<span class="review"><a href="#"><strong>22</strong> Reviews</a></span>
										</li>
									</ul>
									<div class="clear"></div>
								</div>
							</article>
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<div class="piano_add">
									<a href="#"><img src="<?php echo ASSETURL; ?>images/img_17.jpg" alt=""></a>
								</div>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end most selling and add -->
				<!-- discount cate add -->
				<section class="discount_categories_add">
					<div class="container">
						<div class="row">
							<article class="col-lg-9 col-md-9 col-sm-9">
								<div class="discount_cate_list">
									<h3 class="co_heading">Discounted Categories</h3>
									<div class="owl-carousel_1">
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Education</span>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_18.jpg" alt=""></a>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Musician Essentials </span>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_19.jpg" alt=""></a>
												</li>
											</ul>
										</div>
										<div class="item">
											<ul>
												<li>
													<span class="cate_name">Computer Music</span>
													<a href="#"><img src="<?php echo ASSETURL; ?>images/img_20.jpg" alt=""></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</article>
							<aside class="col-lg-3 col-md-3 col-sm-3">
								<div class="bundle">
									<a href="#"><img src="<?php echo ASSETURL; ?>images/img_21.jpg" alt=""></a>
								</div>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end discount cate add -->
				<!-- education learning banner -->
				<section class="education_learning">
					<div class="container">
						<div class="row">
							<article class="col-lg-6 col-md-6 col-sm-6">
								<div class="education_throw">
									<a href="#"><img src="<?php echo ASSETURL; ?>images/img_22.jpg" alt=""></a>
								</div>
							</article>
							<aside class="col-lg-6 col-md-6 col-sm-6">
								<div class="learning_Book">
									<a href="#"><img src="<?php echo ASSETURL; ?>images/img_23.jpg" alt=""></a>
								</div>
							</aside>
							<div class="clear"></div>
						</div>
					</div>
				</section>
				<!-- end education learning banner -->
				<!-- about sec -->
				<section class="about_sec">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="about_in">
									<h3 class="co_heading">About Us</h3>
									<div class="col-lg-2 col-xs-2 no-pad about_img">
										<img src="<?php echo ASSETURL; ?>images/img_24.jpg" alt="">
									</div>
									<div class="col-lg-10 col-xs-10 no-pad about_text">
										<h4>Furtados India's leading online store for Musical Instruments, Printed Sheet Music, Music Books and Music Accessories.</h4>
										<p>Furtados is a leader in music retail in India with over 20 physical stores across the country. Established in 1865, Furtados makes available all the leading brands in Guitars, Pianos, Keyboards, Drums, Harmonicas and other musical instruments.</p>
										<p>Top brands like Yamaha, Korg, Gibson, Fender, Mapex, Pearl Drums, Takamine, Zildjian, Taylor Guitars, D'Addario Strings and many more are all available in-store and online. A new section for Apple computers is also available online for sale across India. Regularly updated news, concert listings and music events keeps you abreast with the latest in the music world. A product review section makes choosing an instrument more interactive and engaging. Welcome to the world of Furtados...because you love music.</p>
										<a href="#">MORE</a>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- end about sec -->
			</div>
<?php include('includes/footer.php'); ?>
