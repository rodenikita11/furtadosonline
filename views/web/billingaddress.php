 <form method="post" id="userdtlform" name="userdtlform" action="">
   <div class="tab-pane active" style="display:none;" id="tab_d2">
      <div class="col-sm-12">
         <div class="col-sm-8">
            <div class=" panel-default">
               <div class="panel-body">
                  <div class="table-responsive">
                     <table class="table ">
                        <thead>
                           <tr>
                              <th colspan="3">
                                 <h5 class="tbh5">Home</h5>
                              </th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td width="25%" class="td">Name</td>
                              <td width="1%">:</td>
                              <td width="65%" class="adtd"><input type="text" class="form-control" value="<?php echo $customer['firstname'].' '.$customer['lastname'];?>" name="name" id="name" readonly></td>
                           </tr>
                           <tr>
                              <td class="td">Mobile No.</td>
                              <td>:</td>
                              <td class="adtd"><input type="text" class="form-control" value="<?php echo $customer['telephone'];?>" name="telephone" id="telephone" readonly></td>
                           </tr>
                           <tr>
                              <td class="td">Address</td>
                              <td>:</td>
                              <td class="adtd"><textarea class="form-control" name="address" id="address"><?php echo $customer['useraddress'];?></textarea></td>
                           </tr>
                           <tr>
                              <td class="td">Landmark</td>
                              <td>:</td>
                              <td class="adtd"><input type="text" class="form-control" value="<?php echo $customer['landmark'];?>" name="landmark" id="landmark"><?php echo $customer['landmark'];?></td>
                           </tr>
                           <tr>
                              <td class="td">State</td>
                              <td>:</td>
                              <td class="adtd"><input type="text" class="form-control" value="<?php echo $customer['state'];?>" name="state" id="state"></td>
                           </tr>
                           </tr>
                           <tr>
                              <td class="td">City</td>
                              <td>:</td>
                              <td class="adtd"><input type="text" class="form-control" value="<?php echo $customer['city'];?>" name="city" id="city"></td>
                           </tr>
                           </tr>
                           <tr>
                              <td class="td">Pin Code</td>
                              <td>:</td>
                              <td class="adtd"><input type="text" class="form-control" value="<?php echo $customer['pincode'];?>" name="pincode" id="pincode"></td>
                           </tr>
                        </tbody>
                     </table>
                     <div class="col-sm-10">
                        <input type="button" value="Cancel" class="btn btn-danger btn-cancle" style="float: right;">
                        <input type="submit" id="submit" name="submit" value="Save" class="btn btn-danger">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</form>