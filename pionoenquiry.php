<style type="text/css">
   h2.offerh2 {
   font-size: 18px;
   text-align: center;
   margin: 8px 0px 10px;
   }
   .m-b-10{
   margin-bottom: 20px;
   }
</style>
<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">
         <!-- ============================================== CONTENT ============================================== -->
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-150">
            <!-- ========================================== SECTION – HERO ========================================= -->
            <div class="container">
               <h1 class="well">Piano Enquiry</h1>
               <p class="enquiry_txt">Thank you for your interest in Pianos.</p>
               <br>
               <p class="enquiry_txt_body">Furtados is India's leading Piano retailer with a range that encompasses beginner student models to the exclusive Steinway range.
                  Pianos are available in different types and sizes. The main types are Acoustic Pianos &amp; Digital Pianos. Acoustic Pianos is also available in Grand and Upright models.
               </p>
               <br>
               <p class="enquiry_txt_body">Please enter the details in the below form &amp; our Piano Specialist will contact you for your requirement shortly. We assure you of our best service at all time.</p>
               <br>
               <div class="col-lg-12 well">
                  <div class="row">
                     <form>
                        <div class="col-sm-12">
                           <div class="form-group">
                                 <label>First Name</label>
                                 <input type="text" placeholder="Enter First Name Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Landline Number</label>
                                 <input type="text" placeholder="Enter Landline Number Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Mobile Number</label>
                                 <input type="text" placeholder="Enter Mobile Number Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Email Address</label>
                                 <input type="text" placeholder="Enter Email Address Here.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Which is the best time we can contact you?</label>
                                 <input type="text" placeholder="Best Time.." class="form-control">
                              </div>
                              <div class="form-group">
                                 <label>Type of Piano interested in:</label>
                                 <select class="enquiry_form" id="intesrt" name="intesrt" >
                                    <option value="Not Selected">Please Select</option>
                                    <option value="Grand Piano">Grand Piano</option>
                                    <option value="Upright Piano">Upright Piano</option>
                                    <option value="Digital Piano">Digital Piano</option>
                                 </select>
                              </div>
                              <div class="form-group">
                                 <label>Budget:</label>
                                 <select class="enquiry_form" id="intesrt" name="intesrt" >
                                    <option value="Not Selected">Please Select</option>
                                    <option value="Upto 1 lakh">Upto 1 lakh</option>
                                    <option value="1 lakh - 2 lakhs">1 lakh - 2 lakhs</option>
                                    <option value="2 lakhs - 5 lakhs">2 lakhs - 5 lakhs</option>
                                    <option value="5 lakhs &amp; above">5 lakhs &amp; above</option>
                                 </select>
                              </div>
                           
                           <button type="button" class="btn btn-lg btn-info">Submit</button>                   
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <!-- ========================================= SECTION – HERO : END========================--> 
         </div>
      </div>
   </div>
</div>