<footer class="mt0">
     <div class="container"> 
      <div class="row">
      <div class="col-xs-12 ">
         <div class="col-xs-3 about-bottom">
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/aboutus">About us</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/job">Careers</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/contactus" title="Contact Us">Contact Us </a></p>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/feedback">Feedback</a></p>
            
         </div>
         <div class="col-xs-4">
           <p><a class="footabout" href="<?php echo SITEMOBURL?>user/pianoenquiry" title="Piano Enquiry">Piano Enquiry</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/privacypolicy" title="Privacy Policy ">Privacy Policy </a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/assuranceprogram" title="Assurance Program">Assurance Program</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/shippingpolicy" title="Shipping Policy">Shipping Policy </a></p>
         </div>
         <div class="col-xs-5 furtados">
            <p><a class="footabout" href="http://furtadosartists.com/">Furtados Artists</a></p>
            <p><a class="footabout" href="<?echo SITEMOBURL;?>/user/termsandcondition">Terms and Conditions</a></p>
            <p><a class="footabout" href="https://www.furtadosonline.com/blog/" title="Blog">Blog</a></p>
            <p><a class="footabout" title="Furtados School Of Music" href="http://www.furtadosschoolofmusic.com/" title="Furtados School Of Music">Furtados School Of Music</a></p>
            <p><a class="footabout" href="<?php echo SITEMOBURL?>user/returnpolicy" title="Return Policy ">Return Policy </a></p>
         </div>
      </div>
   </div>
     </div>
    </footer>
    <div class="copyright text-center w100per"> Copyright &copy; 2018 Furtadosonline.com</div>
       <!-- footer finsih -->
   </div>
  </div>

  <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
   <!--  <script src="js/bootstrap.min.js"></script> -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../mobile-site/js/ie10-viewport-bug-workaround.js"></script>
  <script src="../mobile-site/js/custom.js"></script>
    <!-- for swatches popup starts -->
    <!-- JavaScript Includes -->
  <script src="../mobile-site/js/transition.js"></script>
  <script src="..../mobile-site/js/modal.js"></script>
  </body>
</html>
