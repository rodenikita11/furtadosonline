<div class="body-content" id="top-banner-and-menu">
   <div class="container-fluid">
      <div class="row">			
         <div class="col-xs-12 col-sm-12 col-md-12 outer-top-200" style="background: #f5f5f5; margin-bottom: 60px;">
            <div class="container">
               <div class="wide-banners wow fadeInUp outer-bottom-xs">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="second-page-container" style="padding:0px;">
                           <div style="text-align:center;" class="thank_you_div">
								<?php echo $message; ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
     </div>
  </div>
</div>