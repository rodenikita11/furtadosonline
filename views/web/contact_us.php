<div class="container">
   <div class="wide-banners wow fadeInUp outer-bottom-xs">
      <div class="row">
         <div class="col-xs-12 outer-top-150">
              <h2 style="text-align: center;margin-bottom: 20px;color: #cc0028;text-transform: uppercase;font-weight: 600;">Contact Us</h2>
            <div class="contact-page">
			    <!--<div class="contact-title">
                        <h4>Information</h4>
                     </div>
				-->
               <div class="row">
                  <div class="col-md-6 contact-info">
				      <h2 style="margin: -14px 0px 14px;font-size: 20;"> <b>FURTADOS MUSIC INDIA PVT LTD,</b></h2>
                     <div class="clearfix address">
                        <span class="contact-i"><i class="fa fa-map-marker" style="font-size:  21px;padding-top: 5px;"></i></span>
                        <!--<span class="contact-span">201, Town Centre II,Andheri - Kurla Road, Marol,Andheri (East) <br>Mumbai - 400 059 INDIA</span>-->
						<span class="contact-span">1st floor, Unit No.4, Dwarka Ashish,  6/8, Jambulwadi, Kalbadevi Road<br>Mumbai - 400 002 INDIA</span>
                     </div>
                     <div class="clearfix phone-no">
                        <span class="contact-i"><i class="fa fa-mobile" style="font-size:  21px;padding-top: 5px;"></i></span>
                        <!-- <span class="contact-span"> <a href="tel: 022 - 42875050">91 (022) 42875050 / 42875060</a></span> -->
                        <span class="contact-span"> 
							<a href="tel: +91 91520 04939"> +91 91520 04939 </a>
							<a href="tel: 022 42875060">| +91 (022) 4287 5060/5000 </a>
							<!--<a href="tel: +91 93206 44558">| +91 93206 44558</a>-->
							</span>
                     </div>
                     <div class="clearfix email">
                        <span class="contact-i"><i class="fa fa-envelope"></i></span>
                        <span class="contact-span"><a href="mailto:response@furtadosonline.com">response@furtadosonline.com</a></span>
                     </div><br>
					 <div class="clearfix email">
                        <span class="contact-i"><i class="fa fa-hourglass"></i></span>
                        <span class="contact-span">Monday - Saturday : 10.00 AM to 5.30 PM </span>
                     </div>
					 
                  </div>
               </div>
               <!-- /.contact-page -->
            </div>
         </div>
      </div>
   </div>
</div>