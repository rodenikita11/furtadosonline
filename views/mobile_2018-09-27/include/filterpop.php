<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="btn btn-default" data-dismiss="modal"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a></div>
         <p class="pricerange">
            <label for="price" class="pricepr">Price range:</label>
         <p id="amount2"></p>
         <p id="amount"></p>
         <!--<input type = "text" id = "price" style = "border:0; color:#b9cd6d; font-weight:bold;">-->
         </p>
         <div id = "slider-3"></div>
         <input type="hidden" id="amount1" value="<?echo $_GET['pricestart']?>">
         <input type="hidden" id="amount2" value="<?echo $_GET['priceend']?>">
         <ul>
            <!--<li class="_3d7x0n" onclick="optionshow('percentage')"><p class="_2-yGHU">Percentage</p><svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path></svg></li>-->
            <li class="_3d7x0n " onclick="optionshow('brand')">
               <p class="_2-yGHU">Brand</p>
               <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
               </svg>
            </li>
            <li class="_3d7x0n" onclick="optionshow('avaliable')">
               <p class="_2-yGHU">Availability</p>
               <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
               </svg>
            </li>
            <?foreach($getfilter['attgrp'] as $key => $val){?>  
            <li class="_3d7x0n" onclick="optionshow('<?echo str_replace(' ','',$val['name']);?>')">
               <p class="_2-yGHU"><?echo $val['name'];?></p>
               <svg class="_2Zylr6" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                  <path d="M7.5 4.5L6.44 5.56 9.88 9l-3.44 3.44L7.5 13.5 12 9z"></path>
               </svg>
            </li>
            <?}?>
         </ul>
         <div class="modal-footer">
            <div class="col-xs-6" style="margin-right: -110px;"><button type="button"  style=" background:#cd0029 !important;text-shadow: none;color: #fff; padding: .7em 1em;    border: none;" id="clearattr">Clear</button></div>
            <div class="col-xs-6" id="getfilter">
               <button type="button" style=" background:#cd0029 !important;text-shadow: none;color: #fff;  padding: .7em 1em;    border: none;
     margin-right: -12px;">Apply</button>
            </div>
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
         </div>
      </div>
   </div>
</div>
<div id="myModaloption" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="Container container-fluid bg-1 text-center mainatt emi_1 brandlist overflow "  style="display:none;">
            <div class="row">
               <div class="col-xs-12 pricerange22" >
                  <div class="col-xs-2">BRAND</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <div class="overflowflit">   
               <?foreach($getfilter['brand'] as $key => $val){?>
               <div class="emi_2  bg-2 text-center   ">
                  <div class="row ">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 search"><?echo $val['name'];?>(<?echo $val['probrandcnt'];?>)</div>
                        <?if(in_array($val['id'],$param['branddtl'])){
                           $selected="checked";}else{
                           $selected='';
                           }?>
                        <div class="col-xs-2">
                           <b><input id="checkbox-<?echo $val['id']?>" class="checkbox-custom attributebrand" name="checkbox-2" type="checkbox" value="<?echo $val['id']?>" <?echo $selected;?>>
                           <label for="checkbox-2" class="checkbox-custom-label"></label>
                           </b>
                        </div>
                     </div>
                  </div>
               </div>
               
               <?}?>
               </div>
               <div class="Container container-fluid  fit">
                  <div class="row">
                     <div class="col-xs-6" >
                        <input type="button" value="Done" onclick="goback();"
                         style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                           padding:10px;width: 100%;margin: 0px -4px;border: none;">
                     </div>
                     <div class="col-xs-6" >
                        <input type="button" value="Close" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                          padding: 10px;border: none;width: 100%;">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--  <div class="Container container-fluid  bg-1 text-center mainatt emi_1 percentage "  style="display:none;">
            <div class="row">
                <div class="clear"></div>
                <div class="col-xs-12" >
                  <div class="col-xs-10 ">PERCENTAGE</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
                 </div>
                <div class="clear"></div> 
                <?php //print_r($getfilter['percentage']);?>
                <?//foreach($getfilter['respercentage'] as $key => $val){?>   
                    <div class="Container container-fluid  bg-1 text-center emi_1  ">
                      <div class="row">
                        <div class="col-xs-12" >
                            <div class="col-xs-10 ">% <?php //echo $val['percentage']?></div>
                          <?//if(in_array($val['percentage'],$param['percentage'])){
               //  $selected="checked";}else{
                 //$selected='';
               //}?>
                            <div class="col-xs-2">
                              <b><input id="checkbox-<?//echo $val['percentage']?>" class="checkbox-custom attributepercentage" name="checkbox-2" type="checkbox" value="<?//echo $val['percentage']?>" <?//echo $selected;?>>
                                <label for="checkbox-2" class="checkbox-custom-label"></label>
                              </b>
                            </div>
                        </div>
                      </div>
                    </div>
                <?//}?>
            
            
                <div class="Container container-fluid ">
                  <div class="row">
                    <div class="col-xs-12" >
                     <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;
                        padding: 1em 2em;">
                    </div>
                  </div>
                </div>
            </div>
            </div> -->
         <div class="Container container-fluid  bg-1 text-center mainatt emi_1 avaliable "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 ">Availability</div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <div class="Container container-fluid  bg-1  text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 ">Available(<?echo $getfilter['avalibale']['cnt'];?>)</div>
                        <? if(in_array(1,$param['avaliable'])){
                           $selected1="checked";}else{
                           $selected1='';
                           }?>
                        <div class="col-xs-2"><b><input id="checkbox-1" class="checkbox-custom attributeavaliable" name="checkbox-3" type="checkbox" value="1" <?echo $selected1;?>>
                           <label for="checkbox-3" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 ">Not Available (<?echo $getfilter['notavaliable']['cnt'];?>)</div>
                        <?  if(in_array(0,$param['avaliable'])){
                           $selected2="checked";}else{
                             $selected2='';
                             }?>
                        <div class="col-xs-2"><b><input id="checkbox-0" class="checkbox-custom attributeavaliable" name="checkbox-4" type="checkbox" value="0" <?echo $selected2;?>>
                           <label for="checkbox-4" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;    border: none;
                  padding: 1em 2em;">
            </div>
         </div>
         <!---attribute-->
         <? 
            //print_R($getfilter['attr']);die;
          foreach($getfilter['attgrp'] as $key=>$val){?>
          <div class="Container container-fluid  bg-1 text-center mainatt emi_1 <?echo str_replace(' ','',$val['name']);?> "  style="display:none;">
            <div class="row">
               <div class="col-xs-12" >
                  <div class="col-xs-10 "><?echo $val['name']?></div>
                  <div class="col-xs-2 close1"><button type="button" class="close" data-dismiss="modal">×</button></div>
               </div>
               <div class="clear"></div>
               <?
              foreach ($val['att'] as $key1 =>$valatt){?>  
               <div class="Container container-fluid  bg-1 text-center emi_1  ">
                  <div class="row">
                     <div class="col-xs-12" >
                        <div class="col-xs-10 "><? echo $valatt['attname'];?></div>
                        <div class="col-xs-2"><b><input id="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom attribute" name="checkbox-<?echo $valatt['id'];?>" type="checkbox" value="<?echo $valatt['id'];?>" <? //echo $selected1;?>>
                           <label for="checkbox-<?echo $valatt['id'];?>" class="checkbox-custom-label"></label></b>
                        </div>
                     </div>
                  </div>
               </div>
               <?}?>
               <input type="button" value="Done" onclick="goback();" style=" background: #cd0029 !important; text-shadow: none; color: #fff;    border: none;
                  padding: 1em 2em;">
            </div>
          </div>
         <?}?>  
         <!--- end---->
      </div>
   </div>
</div>
<script type="text/javascript">
  function optionshow(ele){
   jQuery('#myModal').modal('hide');
   jQuery('#myModaloption').modal('show');
   jQuery('.mainatt').hide(); 
     if(ele=='brand'){
        // jQuery('.avaliable').hide();  
       jQuery('.brandlist').show();  
     }else if(ele=='avaliable'){
       //jQuery('.brandlist').hide();  
       jQuery('.avaliable').show();  
     }
     else if(ele=='percentage'){
       //jQuery('.brandlist').hide();  
       jQuery('.percentage').show();  
     }else{
     /// Query('.em_1').hide(); 
     jQuery('.'+ele+'').show();  
       //jQuery('.avaliable').hide();  
       //jQuery('.brandlist').hide();   
     } 
  }
  function goback(){
    jQuery('#myModaloption').modal('hide'); 
    jQuery('#myModal').modal('show');

  }
</script>