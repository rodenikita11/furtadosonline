<style>
   p {
   font-size: 14px;
   text-align: justify;
   }
</style>
<div class="container">
   <div class="wide-banners wow fadeInUp outer-top-xs">
      <div class="row" style="margin-left: -10px">
         <div class="col-lg-12 outer-top-150">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;text-align: center;">Terms and Conditions</h2>
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;"> Your Account</h2>
            <p> If you use this site you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and, you agree to accept responsibility for all activities that occur under your account or password. Furtadosonline and its affiliates reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders at their sole discretion. </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Acceptance</h2>
            <p>By purchasing products and/or services from this site, you agree to be bound by and accept this agreement. Orders are not binding upon Furtadosonline until accepted by Furtados. We reserve the right to refuse service to anyone. Furtadosonline will indicate its acceptance of an order by acknowledging your order by reply, email or by shipping the ordered items to you. Furtadosonline reserves the right to cancel or part ship orders as per our convenience and necessity.</p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Pricing</h2>
            <p>Product descriptions, pricing, and availability can change quickly, Furtadosonline does not warrant the accuracy or completeness of the information provided on the site. If there are any changes in pricing, availability or transportation charges the same will be intimated to the user and requisite approval obtained, by e-mail or any other accepted means of communication, before the product is dispatched.

            </p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Limited Quantities</h2>
            <p>Some products available on promotion or as special deal may be limited in stock and are offered on a first come, first serve basis. Furtadosonline reserves the right to amend or discontinue any promotion that may be applicable without requirement to provide a reason for the same. 
            </p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Shipping Charges—Taxes and Title</h2>
            <p>Separate charges for shipping and handling will be shown on your order form, if applicable. Orders placed on Saturdays, Sundays, or holidays will be processed on the next business day. As a security precaution, initial orders and orders shipping to alternate addresses may be held for extended verification. We reserve the right to make partial shipments, which will not relieve you of your obligation to pay for the remaining deliveries. All items purchased from Furtadosonline.com are made pursuant to a shipment contract. This means that the risk of loss and title for such items passes to you upon our delivery to the carrier. For more information on our shipping policies, including rates, delivery times, and delays, please refer to our Shipping Policy or call us at our customer service numbers on<a href="tel:02242875050"> +91 22 42875050/60.</a>
            </p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Billing & Payments</h2>
            <p>All items supplied will be accompanied by an official GST invoice. If payment for your order has been made in advance the order will be dispatched after receipt of the same. 
  
            </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Delivery Mode</h2>
            <p>Generally all orders are processed and shipped within 3-4 working days, once payment is confirmed and the product is in stock. However, certain categories of products have different lead-times for delivery due to the nature of the product. If the delivery of a product is delayed for an extended period of time the same will be informed to the user. Kindly note, due to certain regulatory requirements or carrier availability it may not be possible to supply products to certain areas and locations. We will advise you if the area is not serviceable after you have placed your order. Transit times vary depending on your location and mode of dispatch opted for. 
            </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Modes of Dispatch</h2>
            <p>All shipments are delivered to your doorstep. If for any reason we are unable to deliver to your doorstep you will be informed by us or the logistic/courier/postal company handling the delivery. Please note that while we may endeavour to have your shipment delivered at your door-step certain mitigating circumstances and occurrences may necessitate that the same may be required to be collected by you from a designated location. 
            </p>
         </div>
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Custom Delivery Option
</h2>
            <p>This mode is applicable for oversized and large items like Acoustic Pianos and Organs. A quote will be provided for the item keeping in mind the location of dispatch, etc. Only on acceptance of this quote and completion of the shopping process will the product be delivered.  
            </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Delivery Time</h2>
            <p>Transit times vary depending on the location and mode of dispatch opted for. To form an estimate of the amount of time you can expect to receive your items please add: Time to dispatch (varies from product to product and displayed on the product page) + Transit time. The Transit time is displayed (depending on your mode of shipment) in your shopping cart. Please note that transit time is based on best effort estimates and may be exceeded due to operational or logistical reasons which may or may not be within our control.
            </p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Cancellation / Refund Policy</h2>
            <p>We at Furtados make every effort to fulfil all the orders placed with the help of all our group stores under the corporate entity - Furtados Music India Pvt Ltd. However, please note that there may be certain orders that we are unable to process and must cancel. Some situations that may result in your order being cancelled include limitations on quantities available for purchase, inaccuracies or errors in product or pricing information, or problems identified by our credit and fraud avoidance department. Furtados will communicate to you if all or any portion of your order is cancelled. In the unlikely event that your order is cancelled after your credit card has been charged, the said amount will be reversed in your Card Account. 
            </p>
         </div>
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Refunds</h2>
            <p>In the event that a refund is necessitated the amount will be refunded in the same mode as the payment was made. If the payment was by Credit Card or Net-Banking, we will refund the Credit Card or Net-Banking account. Typically refunds are processed within 15 working days. Cheques will be sent only to the billing address of the customer.</p>
         </div>
         
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Returns Policy</h2>
            <p>We always endeavour to supply product in pristine condition. However, if, in the unlikely event that the product is received in a defective condition we request you to send back the product to us for replacement. Kindly send back the product only after receiving a return authorisation from us. Items sent without a return authorisation will not be accepted by us. We are unable to accept returns from customers/users under any other circumstances. Kindly verify that the item ordered by you is what you require when placing your order.  
            </p>
         </div>
       
        
         <div class="col-lg-12 outer-top-xs">
            <h2 style="color:#cc0028;text-transform: capitalize;font-size: 24px;margin: 0px 0px 5px;">Limited Warranty & Service</h2>
            <p>All the new products are sold with the manufacturer's limited warranty. The warranty period and service varies by manufacturer and product. Please see our Furtados Assurance Program for our service policy. We value your opinions. If you have comments or questions about our Terms & Privacy Policy, please send a mail to 
               <a href="mailto:response@furtadosonline.com" class="bl_12">response@furtadosonline.com </a> or write to us at the following address.   
            </p>
            <p> 
               <br/><b> Furtados Music (India) Pvt Ltd </b> <br>
               1st floor, Unit No.4, Dwarka Ashish, 6/8, Jambulwadi, Kalbadevi Road
Mumbai - 400 002 INDIA<br>
               Telephone : +91 91520 04939 | +91 (022) 4287 5060/5000
            </p>
         </div>
      </div>
   </div>
</div>
