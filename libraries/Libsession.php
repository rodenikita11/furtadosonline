<?php

	class Libsession
	{






		public $ci, $sessionlib, $another,$mem_var;

    public function __construct(){
         $CI = & get_instance();
          session_start();
         $CI->load->model('Do_user');
         $this->ci = $CI;
				 $this->mem_var = new Memcache;
	 $this->mem_var->addServer(MEMECACHE_IP, MEMECACHE_PORT);
	// var_dump($this->mem_var,MEMECACHE_IP,MEMECACHE_PORT);
    }

		public function setSession($sessionParams)
		{
				foreach($sessionParams as $key=>$val)
				{
					$_SESSION[$key]=$val;
				}

		}

		/*
				NAME : getSession
				DESC : Used for getting session value
		*/
		public function getSession($key)
		{

				if(isset($_SESSION[$key]))
					return $_SESSION[$key];
				return null;
		}

		/*
				NAME : deleteSession
				DESC : Remove Session data.
		*/
		public function deleteSession($removeSession)
		{
			if(empty($removeSession)) return false;
				if(is_array($removeSession))
				{
			foreach($removeSession as $key=>$val)
			{
				unset($_SESSION[$val]); #For collection of array..
			}
				}
				else
				{
					unset($_SESSION[$removeSession]); #For Single Session.
				}
			return true;
		}

		/*
				NAME : deleteAllSession
				DESC : Clear all session data throughout application..
		*/
		public function deleteAllSession()
		{
			unset($_SESSION);
			session_destroy();
		}

		/*
				NAME : isSetSession

				RETURN : true on success, false on fail.
		*/
		public function isSetSession($sessionKey)
		{
			  	//print_R($_SESSION); die;
				if(isset($_SESSION[$sessionKey]))
				return true;
				return false;
		}
		public function sessionhistory($param){
			//print_r($this->ci);die;
			$res=$this->ci->Do_user->savesessionhistory($param);

			return $res;

		}

		public function priceformat($price){
			$amount = round($price);
			setlocale(LC_MONETARY, 'en_IN');
			$amount = money_format('%!.0i', $amount);
			return $amount;
		}

		public function getallsessions(){
			if($_SESSION){
				return $_SESSION;
			}else{
				return 'No session availabel';
			}
		}

		/*MEMCACHE CODE*/

		public function getMemcache($key){
			//return false; die;
			//echo MEMECAHCE_PREFIX.$key;
			if($this->mem_var->get(MEMECAHCE_PREFIX.$key)){ // bypass done
				$date = $this->mem_var->get(MEMECAHCE_PREFIX.$key);
				$cacheret=json_decode($date, TRUE);
				return $cacheret;
			}else{
				return null;
			}
		}

		public function setMemcache($key,$value,$expiry=86400){
			// return false; // bypass done
			//return false; die;
			$encode = json_encode($value);
			return $this->mem_var->add(MEMECAHCE_PREFIX.$key,$encode,false,$expiry);
		}

	}
?>
